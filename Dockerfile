FROM nginx:stable-alpine
ARG DIST_FOLDER

COPY nginx/default.conf /etc/nginx/conf.d
RUN chmod 644 /etc/nginx/conf.d/default.conf
COPY $DIST_FOLDER /usr/share/nginx/html

EXPOSE 80/tcp
EXPOSE 443/tcp

CMD ["nginx", "-g", "daemon off;"]
