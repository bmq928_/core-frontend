#!/usr/bin/env sh

# variables
habor_project=$CI_ENV_HARBOR_PROJECT
harbor_uri=$CI_ENV_HARBOR_URI
harbor_username=$CI_ENV_HARBOR_USERNAME
harbor_password=$CI_ENV_HARBOR_PASSWORD

img_tag='latest'
img_name='core-frontend'
image=$harbor_uri/$habor_project/$img_name:$img_tag

yarn 
yarn build
docker login $harbor_uri -u$harbor_username -p$harbor_password
docker build --compress --build-arg DIST_FOLDER=build -t $image .
docker push $publish_img