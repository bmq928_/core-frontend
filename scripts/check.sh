#!/bin/bash
set -e

[ "$(git rev-parse --abbrev-ref HEAD)" = "master" ] && HEAD="HEAD^" || HEAD="origin/master"

yarn

if [ -z "$1" ]; then
  yarn lint --quiet -c .eslintrc.push.js
  yarn tsc -p tsconfig.json
fi

FILES=$(git diff --name-only $HEAD --diff-filter=ACMR "*.js" "*.tsx" "*.ts" "*.json" "*.mdx" | sed 's| |\\ |g')
if [ ! -z "$FILES" ]; then
  echo "$FILES" | xargs ./node_modules/.bin/prettier --write
fi

if [[ -z "$1" && `git status --porcelain` ]]; then
  echo Lint problem: Please commit fixed files
  exit 255
fi
