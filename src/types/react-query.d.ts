import {
  MutationFunction,
  QueryFunction,
  QueryKey,
  UseInfiniteQueryOptions,
  UseInfiniteQueryResult,
  UseMutationOptions,
  UseMutationResult,
  UseQueryOptions,
  UseQueryResult,
} from 'react-query'
import { ErrorResponse } from '../services/api'

declare module 'react-query' {
  export declare function useQuery<
    TQueryFnData = unknown,
    TError = ErrorResponse,
    TData = TQueryFnData,
    TQueryKey extends QueryKey = QueryKey
  >(options: UseQueryOptions<TQueryFnData, TError, TData, TQueryKey>): UseQueryResult<TData, TError>
  export declare function useQuery<
    TQueryFnData = unknown,
    TError = ErrorResponse,
    TData = TQueryFnData,
    TQueryKey extends QueryKey = QueryKey
  >(
    queryKey: TQueryKey,
    options?: UseQueryOptions<TQueryFnData, TError, TData, TQueryKey>,
  ): UseQueryResult<TData, TError>
  export declare function useQuery<
    TQueryFnData = unknown,
    TError = ErrorResponse,
    TData = TQueryFnData,
    TQueryKey extends QueryKey = QueryKey
  >(
    queryKey: TQueryKey,
    queryFn: QueryFunction<TQueryFnData, TQueryKey>,
    options?: UseQueryOptions<TQueryFnData, TError, TData, TQueryKey>,
  ): UseQueryResult<TData, TError>

  export declare function useMutation<TData = unknown, TError = ErrorResponse, TVariables = void, TContext = unknown>(
    options: UseMutationOptions<TData, TError, TVariables, TContext>,
  ): UseMutationResult<TData, TError, TVariables, TContext>
  export declare function useMutation<TData = unknown, TError = ErrorResponse, TVariables = void, TContext = unknown>(
    mutationFn: MutationFunction<TData, TVariables>,
    options?: UseMutationOptions<TData, TError, TVariables, TContext>,
  ): UseMutationResult<TData, TError, TVariables, TContext>
  export declare function useMutation<TData = unknown, TError = ErrorResponse, TVariables = void, TContext = unknown>(
    mutationKey: MutationKey,
    options?: UseMutationOptions<TData, TError, TVariables, TContext>,
  ): UseMutationResult<TData, TError, TVariables, TContext>
  export declare function useMutation<TData = unknown, TError = ErrorResponse, TVariables = void, TContext = unknown>(
    mutationKey: MutationKey,
    mutationFn?: MutationFunction<TData, TVariables>,
    options?: UseMutationOptions<TData, TError, TVariables, TContext>,
  ): UseMutationResult<TData, TError, TVariables, TContext>

  export declare function useInfiniteQuery<
    TQueryFnData = unknown,
    TError = ErrorResponse,
    TData = TQueryFnData,
    TQueryKey extends QueryKey = QueryKey
  >(
    options: UseInfiniteQueryOptions<TQueryFnData, TError, TData, TQueryFnData, TQueryKey>,
  ): UseInfiniteQueryResult<TData, TError>
  export declare function useInfiniteQuery<
    TQueryFnData = unknown,
    TError = ErrorResponse,
    TData = TQueryFnData,
    TQueryKey extends QueryKey = QueryKey
  >(
    queryKey: TQueryKey,
    options?: UseInfiniteQueryOptions<TQueryFnData, TError, TData, TQueryFnData, TQueryKey>,
  ): UseInfiniteQueryResult<TData, TError>
  export declare function useInfiniteQuery<
    TQueryFnData = unknown,
    TError = ErrorResponse,
    TData = TQueryFnData,
    TQueryKey extends QueryKey = QueryKey
  >(
    queryKey: TQueryKey,
    queryFn: QueryFunction<TQueryFnData, TQueryKey>,
    options?: UseInfiniteQueryOptions<TQueryFnData, TError, TData, TQueryFnData, TQueryKey>,
  ): UseInfiniteQueryResult<TData, TError>
}
