import { Theme } from '../components/GlobalStyle'

declare module 'styled-components' {
  export interface DefaultTheme extends Theme {}
}
