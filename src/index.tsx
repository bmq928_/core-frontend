import * as React from 'react'
import ReactDOM from 'react-dom'
import App from './App'

console.info('Build version', process.env.REACT_APP_VERSION || 'development')

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root'),
)
