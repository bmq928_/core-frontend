import debounce from 'lodash/debounce'
import * as React from 'react'
import { useHistory, useLocation } from 'react-router'
import { SortingRule } from 'react-table'
import { SortOrder } from '../services/api/request'

export type TableConfigs = {
  searchText?: string
  page?: number
  sortBy?: string
  sortOrder?: SortOrder
}

const DEFAULT_CONFIGS = { timeout: 300, method: 'replace' }

export function useTableConfigs(
  defaultValues: TableConfigs = { page: 1 },
  options?: { timeout?: number; method?: 'replace' | 'push' },
): [
  TableConfigs & { page: number; searchValue?: string },
  {
    onChangePage(page: number): void
    onChangeSearch(search: string): void
    onChangeSort(sortByArray: SortingRule<any>[]): void
  },
] {
  const configs = { ...DEFAULT_CONFIGS, ...options }
  const location = useLocation()
  const history = useHistory()

  const [searchValue, setSearchValue] = React.useState<string>('')

  const { current: urlSearchParams } = React.useRef(new URLSearchParams(location.search))

  React.useEffect(() => {
    const searchValue = urlSearchParams.get('searchText')
    setSearchValue(searchValue || '')
  }, [])

  const navigate = () => {
    const search = urlSearchParams.toString()
    // @ts-ignore
    history[configs.method]({ pathname: location.pathname, search })
  }

  const handleChangeSearchText = React.useCallback(
    debounce((searchText?: string) => {
      const value = searchText?.trim()
      if (!value) {
        urlSearchParams.delete('searchText')
      } else {
        urlSearchParams.set('searchText', value)
      }
      urlSearchParams.delete('page')

      navigate()
    }, configs.timeout),
    [configs.timeout],
  )

  const onChangeSearch = React.useCallback((newValue: string) => {
    setSearchValue(newValue)
    handleChangeSearchText(newValue)
  }, [])

  const onChangePage = React.useCallback((page: number) => {
    if (page <= 1) {
      urlSearchParams.delete('page')
    } else {
      urlSearchParams.set('page', `${page}`)
    }
    navigate()
  }, [])

  const onChangeSort = React.useCallback((sortByArray: SortingRule<any>[]) => {
    const sortBy = sortByArray[0]
    if (!sortBy) {
      urlSearchParams.delete('sortBy')
      urlSearchParams.delete('sortOrder')
    } else {
      urlSearchParams.set('sortBy', sortBy.id)
      urlSearchParams.set('sortOrder', sortBy.desc ? SortOrder.Desc : SortOrder.Asc)
    }
    navigate()
  }, [])

  const params = transformParams(urlSearchParams, defaultValues)

  return [
    { searchValue, ...params },
    { onChangePage, onChangeSearch, onChangeSort },
  ]
}

function transformParams(urlSearchParams: URLSearchParams, defaultValues: TableConfigs) {
  const page = Number(urlSearchParams.get('page') || 1)
  const sortBy = urlSearchParams.get('sortBy') || undefined
  const sortOrder = urlSearchParams.get('sortOrder') || undefined
  const searchText = urlSearchParams.get('searchText') || undefined
  return Object.assign(defaultValues, { searchText, page, sortBy, sortOrder })
}
