import * as React from 'react'
import { SortingRule } from 'react-table'
import { SortType } from '../routes/AppViewer/model'
import { SortOrder } from '../services/api/request'

export function useLocalTableConfigs(): [
  { sort?: SortType; currentPage: number },
  { onChangeSort: (sortByArray: SortingRule<any>[]) => void; onChangePage: (page: number) => void },
] {
  const [sort, setSort] = React.useState<SortType>()
  const [currentPage, onChangePage] = React.useState(1)

  const onChangeSort = React.useCallback((sortByArray: SortingRule<any>[]) => {
    const sortBy = sortByArray[0]
    if (!sortBy) {
      setSort(undefined)
      return
    }
    setSort({ sortBy: sortBy.id, sortOrder: sortBy.desc ? SortOrder.Desc : SortOrder.Asc })
  }, [])

  return React.useMemo(
    () => [
      { sort, currentPage },
      { onChangeSort, onChangePage },
    ],
    [sort, onChangeSort, currentPage],
  )
}
