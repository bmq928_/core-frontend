import { useTranslation } from 'react-i18next'
import { isEmptyStr } from '../utils/utils'

export function useValidateString(
  isRequired?: boolean,
): {
  validateFunction: (checkStr?: string) => string | boolean
} {
  const { t } = useTranslation()

  return {
    validateFunction: (str?: string) => {
      if (isRequired === undefined || !isRequired) {
        return true
      }
      return isEmptyStr(str) ? (t('common.error.required') as string) || 'Required' : true
    },
  }
}
