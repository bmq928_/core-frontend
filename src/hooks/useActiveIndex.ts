import * as React from 'react'

type Actions = {
  next(): void
  previous(): void
  goTo(to: number): void
}

export function useActiveIndex(length: number, initialIndex = 0): [number, Actions] {
  const [activeIndex, updateActiveIndex] = React.useState(initialIndex)

  const handleChangeIndex = React.useCallback(
    (step = 1) => {
      updateActiveIndex(prev => {
        if (Math.abs(step) > length) {
          throw Error(`Step should be ${length} >= step >= -${length}`)
        }
        if (step !== 0) {
          return Math.abs((prev + step + length) % length)
        }
        return prev
      })
    },
    [length],
  )

  const handleGoTo = React.useCallback(
    (to: number) => {
      if (Math.abs(to) > length) {
        throw Error(`Step should be <= ${length}`)
      }
      updateActiveIndex(prev => {
        return prev !== to ? to : prev
      })
    },
    [length],
  )

  const actions = React.useMemo(
    () => ({
      next() {
        handleChangeIndex()
      },
      previous() {
        handleChangeIndex(-1)
      },
      goTo(to: number) {
        handleGoTo(to)
      },
    }),
    [handleChangeIndex, handleGoTo],
  )

  return [activeIndex, actions]
}
