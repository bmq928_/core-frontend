import { useEffect } from 'react'

export const useClickOutside = <T extends HTMLElement, E extends HTMLElement>(
  handler: (event: Event) => void,
  ref?: React.RefObject<T> | React.MutableRefObject<T>,
  excludes?: Array<E | undefined | null>,
) => {
  useEffect(() => {
    const listener = (event: Event) => {
      const target = event.target as T
      let excluded = false
      if (excludes) {
        excluded = excludes.reduce((result, item) => {
          if (result || item?.contains(target)) {
            return true
          }
          return result
        }, false as boolean)
      }
      if (!ref || !ref.current || ref.current.contains(target) || excluded) {
        return
      }

      handler(event)
    }

    document.addEventListener('mousedown', listener)
    document.addEventListener('touchstart', listener)
    return () => {
      document.removeEventListener('mousedown', listener)
      document.removeEventListener('touchstart', listener)
    }
  }, [ref, handler, excludes])
}
