import React from 'react'
import ResizeObserver from 'resize-observer-polyfill'

export type ElementRect = Omit<DOMRect, 'toJSON'>

export function useMeasure<Element extends HTMLElement>(ref: React.RefObject<Element>) {
  const [bounds, set] = React.useState<ElementRect>({
    bottom: 0,
    height: 0,
    left: 0,
    right: 0,
    top: 0,
    width: 0,
    x: 0,
    y: 0,
  })

  const resizeObserver = React.useRef(
    new ResizeObserver(([entry]) => {
      set(entry.target.getBoundingClientRect())
    }),
  )

  React.useEffect(() => {
    const ro = resizeObserver.current
    if (ref.current) {
      ro.observe(ref.current)
      return () => {
        ro.disconnect()
      }
    }
  }, [ref])

  return bounds
}
