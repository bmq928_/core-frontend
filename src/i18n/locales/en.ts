export default {
  translation: {
    common: {
      ok: 'Ok',
      error: {
        required: 'Required',
        invalidEmail: 'Invalid email address',
        other: 'Something went wrong',
      },
    },
  },
}
