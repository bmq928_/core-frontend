export default {
  translation: {
    common: {
      ok: 'Ok',
      error: {
        required: 'Bắt buộc',
        invalidEmail: 'Địa chỉ email không hợp lệ',
        other: 'vi:Something went wrong',
      },
    },
  },
}
