import { createRef } from 'react'
import { I18nextProvider } from 'react-i18next'
import { QueryCache, QueryClient, QueryClientProvider, setLogger } from 'react-query'
import { Provider } from 'react-redux'
import 'react-toastify/dist/ReactToastify.css'
import { ThemeProvider } from 'styled-components'
import { GlobalStyle, theme } from './components/GlobalStyle'
import { ToastContainer } from './components/NToast'
import i18n from './i18n/i18n'
import { store } from './redux'
import { Main } from './routes/Main'

const queryCache = new QueryCache()
const queryClient = new QueryClient({
  queryCache,
  defaultOptions: {
    queries: {
      staleTime: 15000,
      // stop refetch when refocus back to window
      refetchOnWindowFocus: false,
      retry: 1,
    },
    mutations: {
      retry: 1,
    },
  },
})

setLogger({
  log: console.info,
  warn: console.info,
  error: console.info,
})

export const rootPortal = createRef<HTMLDivElement>()

function App() {
  return (
    <I18nextProvider i18n={i18n}>
      <Provider store={store}>
        <ThemeProvider theme={theme}>
          <QueryClientProvider client={queryClient}>
            <GlobalStyle />
            <Main />
          </QueryClientProvider>
          <ToastContainer />
        </ThemeProvider>
      </Provider>
    </I18nextProvider>
  )
}

export default App
