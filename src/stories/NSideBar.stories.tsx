import { Meta, Story } from '@storybook/react/types-6-0'
import React from 'react'
import { ThemeProvider } from 'styled-components'
import { GlobalStyle, theme } from '../components/GlobalStyle'
import { NSideBar, NSideBarProps } from '../components/NLayout/NSideBar'

export default {
  title: 'Example/NSideBar',
  component: NSideBar,
} as Meta

const Template: Story<NSideBarProps> = args => {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <NSideBar {...args}>
        <p>Content 1</p>
        <p>Content 2</p>
        <p>Content 3</p>
      </NSideBar>
    </ThemeProvider>
  )
}

export const Default = Template.bind({})
Default.args = {}

export const ThemeLightCollasped = Template.bind({})
ThemeLightCollasped.args = {
  mode: 'light',
  collapsed: true,
}
