// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Meta, Story } from '@storybook/react/types-6-0'
import React from 'react'
import { NCheckbox, NCheckboxProps } from '../components/NCheckbox/NCheckbox'

export default {
  title: 'Input/NCheckbox',
  component: NCheckbox,
} as Meta

const Template: Story<NCheckboxProps> = args => <NCheckbox {...args} />

export const Default = Template.bind({})
Default.args = {
  title: 'Title',
  subtitle: 'Subtitle',
}

export const Intermidiate = Template.bind({})
Intermidiate.args = {
  title: 'Title',
  subtitle: 'Subtitle',
  indeterminate: true,
}

export const Only_checkbox = Template.bind({})
Only_checkbox.args = {}

export const Disable_unchecked = Template.bind({})
Disable_unchecked.args = {
  title: 'Title',
  subtitle: 'Subtitle',
  disabled: true,
}

export const Disable_checked = Template.bind({})
Disable_checked.args = {
  title: 'Title',
  subtitle: 'Subtitle',
  checked: true,
  disabled: true,
}

export const Disable_indeterminate = Template.bind({})
Disable_indeterminate.args = {
  title: 'Title',
  subtitle: 'Subtitle',
  indeterminate: true,
  disabled: true,
}
