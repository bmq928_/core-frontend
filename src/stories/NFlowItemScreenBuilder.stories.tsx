import { Meta } from '@storybook/react/types-6-0'
import React, { useState } from 'react'
import { BrowserRouter } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'
import { GlobalStyle, theme } from '../components/GlobalStyle'
import { NButton } from '../components/NButton/NButton'
import { NModal } from '../components/NModal/NModal'
import { ScreenBuilder } from '../routes/FlowBuilder/FlowBuilderItem/ScreenBuilder/ScreenBuilder'
import { NodeType } from '../routes/FlowBuilder/utils/types'

export default {
  title: 'DevFlowItem/NFlowItemScreenBuilder',
  component: ScreenBuilder,
} as Meta

export const Default = () => {
  const [showModal, setShowModal] = useState(false)

  return (
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <GlobalStyle />
        <NButton onClick={() => setShowModal(true)}>show modal</NButton>
        <NModal visible={showModal} setVisible={setShowModal} size="x-large">
          <ScreenBuilder
            nodeModal={{ type: NodeType.Screen, position: { x: 0, y: 0 } }}
            onCancel={() => setShowModal(false)}
            onSubmit={() => {
              console.log('submit')
              setShowModal(false)
            }}
          />
        </NModal>
      </ThemeProvider>
    </BrowserRouter>
  )
}
