// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Meta, Story } from '@storybook/react/types-6-0'
import React from 'react'
import { theme } from '../components/GlobalStyle'
import { NSpinner, NSpinnerProps } from '../components/NSpinner/NSpinner'

export default {
  title: 'Example/NSpinner',
  component: NSpinner,
} as Meta

const Template: Story<NSpinnerProps> = args => <NSpinner {...args} />

export const Spinner = Template.bind({})
Spinner.args = {
  color: theme.color.Primary700,
  size: 20,
  strokeWidth: 5,
  sizeUnit: 'px',
}
