import { Meta } from '@storybook/react/types-6-0'
import React from 'react'
import { NButton } from '../components/NButton/NButton'
import { CreateLayoutModal } from '../routes/Dashboard/routes/Layout/CreateLayoutModal'
import { AskForPublishModal } from '../routes/LayoutBuilder/components/AskForPublishModal'
import { RecordSelection as RecordSelect } from '../routes/LayoutBuilder/components/ZoneItems/Components/ObjectList/RecordSelection'
import { FieldResponse } from '../services/api/models'
import FIELDS from './assets/fields.json'

export default {
  title: 'Example/Modals',
} as Meta

export const CreateLayout = () => {
  const [show, set] = React.useState(false)
  const [data, setData] = React.useState<any>()
  return (
    <>
      <NButton onClick={() => set(true)}>Show modal</NButton>
      {data && <pre>{JSON.stringify(data, null, 2)}</pre>}
      <CreateLayoutModal visible={show} setVisible={set} onSubmit={setData} />
    </>
  )
}

export const RecordSelection = () => {
  const [data, setData] = React.useState<any>()
  return (
    <React.Fragment>
      <RecordSelect
        data={
          // @ts-ignore
          FIELDS as FieldResponse[]
        }
        fields={data}
        onSubmit={setData}>
        <NButton>Record Selection</NButton>
      </RecordSelect>
      {data && <pre>{JSON.stringify(data, null, 2)}</pre>}
    </React.Fragment>
  )
}

export const AskForPublish = () => {
  const [show, set] = React.useState(false)
  return (
    <>
      <NButton onClick={() => set(true)}>Show Modal</NButton>
      <AskForPublishModal visible={show} setVisible={set} name="App Page">
        <NButton
          size="small"
          type="ghost"
          onClick={() => {
            set(false)
          }}>
          Not yet
        </NButton>
        <NButton
          size="small"
          type="primary"
          onClick={() => {
            set(false)
          }}>
          Publish
        </NButton>
      </AskForPublishModal>
    </>
  )
}
