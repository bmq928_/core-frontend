// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Meta, Story } from '@storybook/react/types-6-0'
import React, { useState } from 'react'
import { NSingleSelect, NSingleSelectProps } from '../components/NSelect/NSingleSelect'

export default {
  title: 'Select/NSingleSelect',
  component: NSingleSelect,
} as Meta

const Template: Story<NSingleSelectProps> = args => {
  const [value, setValue] = useState<string | undefined>()

  return <NSingleSelect {...args} value={value} onValueChange={setValue} />
}

export const SingleSelect = Template.bind({})

SingleSelect.args = {
  options: [
    { label: 'Option1', value: 'value1' },
    { label: 'Option2', value: 'value2' },
    { label: 'Option3', value: 'value3' },
  ],
}
