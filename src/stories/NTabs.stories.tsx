import { Meta } from '@storybook/react/types-6-0'
import React from 'react'
import styled from 'styled-components'
import { NTabs } from '../components/NTabs/NTabs'

const Content = styled('div')`
  padding: 16px 0;
`

export default {
  title: 'Example/NTabs',
} as Meta

export const Default = () => {
  const [active, setActive] = React.useState(2)
  return (
    <NTabs active={active} onChangeTab={setActive}>
      <NTabs.Tab title="Title 1">
        <Content>Tab 1</Content>
      </NTabs.Tab>
      <NTabs.Tab title="Title 2">Tab 2</NTabs.Tab>
      <NTabs.Tab title="Title 3">Tab 3</NTabs.Tab>
    </NTabs>
  )
}
