import { Meta, Story } from '@storybook/react/types-6-0'
import * as React from 'react'
import { NDatePicker, NDatePickerProps } from '../components/DateTime/NDatePicker'

export default {
  title: 'DateTime/DatePicker',
} as Meta

const Template: Story<NDatePickerProps> = args => {
  const [innerValue, setInnerValue] = React.useState<Date>()

  const { value, onChangeValue, ...restArgs } = args
  return <NDatePicker value={value || innerValue} onChangeValue={onChangeValue || setInnerValue} {...restArgs} />
}

export const DatePicker = Template.bind({})
DatePicker.args = { placeholder: 'Select a date' }

export const Test = () => {
  const [value, setValue] = React.useState<Date>()
  return (
    <>
      <NDatePicker label="With label" value={value} onChangeValue={setValue} />
      <NDatePicker label="With label" caption="Caption" value={value} onChangeValue={setValue} />
      <NDatePicker label="With label" error="Error?" value={value} onChangeValue={setValue} />
    </>
  )
}
