// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Meta } from '@storybook/react/types-6-0'
import React from 'react'
import { NTextInput } from '../components/NTextInput/NTextInput'

const Padded = () => <div style={{ height: 32 }} />

export default {
  title: 'Input/NTextInput',
} as Meta

export const TextInput = () => {
  return (
    <>
      <NTextInput label="Label" caption="Caption" placeholder="Enter your name" />
      <Padded />
      <NTextInput label="No Caption" placeholder="Enter your name" />
      <Padded />
      <NTextInput placeholder="Enter your name" />
      <Padded />
      <NTextInput error="What?" placeholder="Enter your name" />
      <Padded />
      <NTextInput caption="caption" error="but error" placeholder="Enter your name" />
      <Padded />
      <NTextInput disabled caption="caption" placeholder="Enter your name" />
      <Padded />
      <NTextInput label="Custom Left" left={<div style={{ width: 24, height: 24, backgroundColor: '#999' }} />} />
    </>
  )
}
