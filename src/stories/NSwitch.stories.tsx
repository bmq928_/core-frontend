// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Meta, Story } from '@storybook/react/types-6-0'
import React from 'react'
import { NSwitch, NSwitchProps } from '../components/NSwitch/NSwitch'

export default {
  title: 'Input/NSwitch',
  component: NSwitch,
} as Meta

const Template: Story<NSwitchProps> = args => <NSwitch {...args} />

export const Default = Template.bind({})
Default.args = {
  title: 'Title',
  subtitle: 'Subtitle',
}

export const OnlySwitch = Template.bind({})
OnlySwitch.args = {}

export const Small = Template.bind({})
Small.args = {
  title: 'Title',
  subtitle: 'Subtitle',
  size: 'small',
}
