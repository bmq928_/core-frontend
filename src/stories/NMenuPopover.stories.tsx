import { Meta, Story } from '@storybook/react/types-6-0'
import React from 'react'
import { ThemeProvider } from 'styled-components'
import { GlobalStyle, theme } from '../components/GlobalStyle'
import { NMenuPopover, NMenuPopoverItem, NMenuPopoverProps } from '../components/NMenuPopover'

export default {
  title: 'Example/NMenuPopover',
  component: NMenuPopover,
} as Meta

const Template: Story<NMenuPopoverProps> = args => {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <div style={{ margin: '200px' }}>
        <NMenuPopover {...args}>
          <NMenuPopoverItem onClick={() => alert('hi!')}>Hi</NMenuPopoverItem>
        </NMenuPopover>
      </div>
    </ThemeProvider>
  )
}

export const Default = Template.bind({})
