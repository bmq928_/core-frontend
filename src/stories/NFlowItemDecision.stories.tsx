import { Meta } from '@storybook/react/types-6-0'
import React, { useState } from 'react'
import { BrowserRouter } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'
import { GlobalStyle, theme } from '../components/GlobalStyle'
import { NButton } from '../components/NButton/NButton'
import { NModal } from '../components/NModal/NModal'
import { DecisionModal } from '../routes/FlowBuilder/FlowBuilderItem/FlowItemDecision/DecisionModal'
import { NodeType } from '../routes/FlowBuilder/utils/types'

export default {
  title: 'DevFlowItem/NFlowItemDecision',
  component: DecisionModal,
} as Meta

export const Default = () => {
  const [showDecision, setShowDecision] = useState(false)

  return (
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <GlobalStyle />
        <NButton onClick={() => setShowDecision(true)} style={{ marginLeft: '1rem' }}>
          Show Decision
        </NButton>
        <NModal visible={showDecision} setVisible={setShowDecision} size="x-large">
          <DecisionModal
            nodeModal={{
              position: { x: 0, y: 0 },
              type: NodeType.Decision,
            }}
            onCancel={() => setShowDecision(false)}
            onSubmit={data => {
              console.log(data)
              setShowDecision(false)
            }}
          />
        </NModal>
      </ThemeProvider>
    </BrowserRouter>
  )
}
