import { Meta, Story } from '@storybook/react/types-6-0'
import { useState } from 'react'
import { NMultiSelect, NMultiSelectProps } from '../components/NSelect/NMultiSelect'

export default {
  title: 'Select/NMultiSelect',
  component: NMultiSelect,
} as Meta

const Template: Story<NMultiSelectProps> = args => {
  const [values, setValues] = useState<string[]>([])
  return <NMultiSelect {...args} values={values} onValuesChange={setValues} />
}

export const MultiSelect = Template.bind({})

MultiSelect.args = {
  options: [
    { label: 'Option1', value: 'value1' },
    { label: 'Option2', value: 'value2' },
    { label: 'Option3', value: 'value3' },
    { label: 'Option4', value: 'valu4e' },
    { label: 'Option5', value: 'value33' },
  ],
  creatable: true,
}
