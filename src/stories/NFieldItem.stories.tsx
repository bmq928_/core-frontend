// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Meta } from '@storybook/react/types-6-0'
import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import styled, { ThemeProvider } from 'styled-components'
import { GlobalStyle, spacing, theme } from '../components/GlobalStyle'
import { NButton } from '../components/NButton/NButton'
import { NDivider } from '../components/NDivider'
import { FieldItemMode, NFieldItem } from '../components/NFieldItem/NFieldItem'
import { NRow } from '../components/NGrid/NGrid'
import { NTypography } from '../components/NTypography'
import { FieldSchema } from '../services/api/models'

const Grid = styled.div`
  display: grid;
  width: 80vw;
  grid-template-columns: 1fr 1fr;
  grid-gap: ${spacing('md')};
`

export default {
  title: 'Example/NFieldItem',
  component: NFieldItem,
} as Meta

export const ColumnDefault = () => {
  const [mode, setMode] = useState(FieldItemMode.View)
  const [submitResult, setSubmitResult] = useState('')
  const formMethods = useForm({
    defaultValues: TestFieldValue,
  })

  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <NRow>
        <NButton
          type={mode === FieldItemMode.View ? 'primary' : 'default'}
          onClick={() => {
            setMode(FieldItemMode.View)
            setSubmitResult('')
          }}>
          {FieldItemMode.View}
        </NButton>
        <NButton
          type={mode === FieldItemMode.Form ? 'primary' : 'default'}
          onClick={() => {
            setMode(FieldItemMode.Form)
          }}>
          {FieldItemMode.Form}
        </NButton>
      </NRow>
      <NDivider size="md"></NDivider>
      <Grid>
        {TestFields.map(field => (
          <NFieldItem mode={mode} field={field} contents={TestFieldValue} />
        ))}
      </Grid>
      {mode === FieldItemMode.Form && (
        <>
          <NDivider size="md" />
          <NButton
            onClick={() => {
              formMethods.handleSubmit(data => {
                setSubmitResult(JSON.stringify(data))
              })()
            }}>
            Submit
          </NButton>
          <NDivider size="md" />
          <NTypography.Headline>{submitResult}</NTypography.Headline>
        </>
      )}
    </ThemeProvider>
  )
}

const TestFieldValue: Record<string, any> = {
  age: 24,
  gender: 'other',
  guid: '00b_001rQ6zmBBDb98h1mSCkFZ3ym',
  is_alive: true,
  name: 'Bin',
  objName: 'test_object_2',
  tags: ['tag 1', 'tag 2'],
  test_date: '1970-01-01T16:00:00.000Z',
}

const TestFields: FieldSchema[] = [
  {
    name: 'name',
    displayName: 'Name',
    description: undefined,
    isRequired: false,
    subType: 'short',
    isSearchable: true,
    isSortable: true,
    value: undefined,
    typeName: 'text',
  },
  {
    name: 'age',
    displayName: 'age',
    description: undefined,
    isRequired: false,
    subType: 'integer',
    isSearchable: true,
    isSortable: true,
    value: undefined,
    typeName: 'numeric',
  },
  {
    name: 'gender',
    displayName: 'gender',
    description: undefined,
    isRequired: false,
    subType: 'single',
    isSearchable: true,
    isSortable: true,
    value: '["male","female","other"]',
    typeName: 'pickList',
  },
  {
    name: 'is_alive',
    displayName: 'Is alive',
    description: undefined,
    isRequired: true,
    defaultValue: 'true',
    isSearchable: false,
    isSortable: true,
    value: undefined,
    typeName: 'boolean',
  },
  {
    name: 'test_date',
    displayName: 'Test date',
    description: undefined,

    isRequired: false,

    defaultValue: '2021-03-03T04:14:40.000Z',
    subType: 'date-time',
    isSearchable: true,
    isSortable: true,
    value: undefined,
    typeName: 'dateTime',
  },
  {
    name: 'tags',
    displayName: 'Tags',
    description: undefined,
    isRequired: false,
    subType: 'multi',
    isSearchable: true,
    isSortable: true,
    value: '["tag 1","tag 2","tag 3"]',
    typeName: 'pickList',
  },
]
