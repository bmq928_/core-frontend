import * as React from 'react'
import { NPopoverPlacement } from '../components/NPopover'
import { NSingleSelect } from '../components/NSelect/NSingleSelect'

export default {
  title: 'Example/NPopover',
}

const OPTIONS = [
  { value: 'auto', label: 'auto position' },
  { value: 'top', label: 'top' },
  { value: 'topLeft', label: 'topLeft' },
  { value: 'topRight', label: 'topRight' },
  { value: 'right', label: 'right' },
  { value: 'rightTop', label: 'rightTop' },
  { value: 'rightBottom', label: 'rightBottom' },
  { value: 'bottom', label: 'bottom' },
  { value: 'bottomLeft', label: 'bottomLeft' },
  { value: 'bottomRight', label: 'bottomRight' },
  { value: 'left', label: 'left' },
  { value: 'leftTop', label: 'leftTop' },
  { value: 'leftBottom', label: 'leftBottom' },
]

export const Default = () => {
  const [placement, setPlacement] = React.useState<string | undefined>('auto')
  const [value, setValue] = React.useState<string | undefined>('item-0')
  return (
    <>
      <NSingleSelect options={OPTIONS} value={placement} onValueChange={setPlacement} placeholder="Auto Position" />
      <div
        style={{
          display: 'flex',
          height: 'calc(100vh - 200px)',
          width: '100vw',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <NSingleSelect
          value={value}
          onValueChange={setValue}
          placement={placement !== 'auto' ? (placement as NPopoverPlacement) : undefined}
          options={Array.from({ length: 30 }, (_, i) => ({ label: `Item ${i + 1}`, value: `item-${i}` }))}
        />
      </div>
    </>
  )
}
