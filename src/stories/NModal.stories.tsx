// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Meta, Story } from '@storybook/react/types-6-0'
import React, { useState } from 'react'
import { NButton } from '../components/NButton/NButton'
import { NModal, NModalProps } from '../components/NModal/NModal'

export default {
  title: 'Example/NModal',
  component: NModal,
} as Meta

const Template: Story<NModalProps> = args => {
  const [show, setShow] = useState(false)

  return (
    <>
      <NButton type="primary" onClick={() => setShow(true)}>
        Show Modal
      </NButton>
      <NModal {...args} visible={show} setVisible={setShow}>
        <NModal.Header title="Modal test" onClose={() => setShow(false)} />
        <NModal.Body>
          <div style={{ height: '300px' }}>Test modal</div>
        </NModal.Body>
        <NModal.Footer onCancel={() => setShow(false)} />
      </NModal>
    </>
  )
}

export const Modal = Template.bind({})

Modal.args = {
  size: 'default',
}
