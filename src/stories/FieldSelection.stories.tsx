import { Meta } from '@storybook/react/types-6-0'
import React from 'react'
import { DndProvider, useDrop } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'

export default {
  title: 'Example/FieldSelection',
} as Meta

const ACTIONS = Array.from({ length: 11 }, (_, i) => ({ id: `action-${i}`, displayName: `Action ${i + 1}` }))

const FIELDS = Array.from({ length: 29 }, (_, i) => ({ id: `field-${i}`, displayName: `Field ${i + 1}` }))

const findItem = (type: string, id: string) => (type === 'ACTION_TYPE' ? ACTIONS : FIELDS).find(i => i.id === id)

export const Default = () => {
  return (
    <DndProvider backend={HTML5Backend}>
      <div style={{ display: 'flex' }}>
        <TestZone accept="ACTION_TYPE" />
        <TestZone accept="FIELD_TYPE" />
      </div>
    </DndProvider>
  )
}

type TestZoneProps = {
  accept: string | string[]
}

const TestZone = ({ accept }: TestZoneProps) => {
  const [dropped, setDropped] = React.useState<any[]>([])
  const [{ isOver, canDrop }, drop] = useDrop({
    accept,
    drop(item: { type: string; id: string }) {
      const droppedItem = findItem(item.type, item.id)
      setDropped(prev => [...prev, droppedItem])
    },
    collect(monitor) {
      return {
        isOver: monitor.isOver(),
        canDrop: monitor.canDrop(),
      }
    },
  })

  return (
    <div
      ref={drop}
      style={{
        flex: 1,
        minHeight: 200,
        border: '1px solid #999',
        background: isOver && canDrop ? 'green' : !isOver ? 'white' : 'red',
      }}>
      <div>Only allow {accept.toString()} type</div>
      {dropped.map(i => (
        <div key={i.id}>{i.displayName}</div>
      ))}
    </div>
  )
}
