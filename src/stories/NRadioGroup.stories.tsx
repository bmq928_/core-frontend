// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Meta, Story } from '@storybook/react/types-6-0'
import React from 'react'
import { NRadio, NRadioProps } from '../components/NRadio/NRadio'

export default {
  title: 'Input/NRadio',
  component: NRadio,
} as Meta

const Template: Story<NRadioProps> = args => <NRadio {...args} />

export const Default = Template.bind({})
Default.args = {
  title: 'NRadio Title',
  subtitle: 'NRadio Subtitle',
  checked: true,
  disabled: false,
}

export const RadioOnly = () => <NRadio />
export const Disabled = () => <NRadio title="NRadio Disabled" disabled />
export const DisableChecked = () => <NRadio title="NRadio disabled" subtitle="but checked" disabled checked />

const ALL_STATE = [
  {
    name: 'ALL_STATE',
    title: 'Normal Radio',
  },
  {
    name: 'ALL_STATE',
    title: 'Default Checked',
    defaultChecked: true,
  },
  {
    name: 'ALL_STATE',
    title: 'Disabled',
    disabled: true,
  },
]

export const AllState = () => (
  <div>
    {ALL_STATE.map((i, idx) => (
      <React.Fragment key={`radio-${idx}`}>
        <NRadio {...i} />
        <div style={{ height: 16 }} />
      </React.Fragment>
    ))}
  </div>
)
