import { Meta } from '@storybook/react/types-6-0'
import React, { useState } from 'react'
import { BrowserRouter } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'
import { GlobalStyle, theme } from '../components/GlobalStyle'
import { NButton } from '../components/NButton/NButton'
import { NModal } from '../components/NModal/NModal'
import { LoopBuilder } from '../routes/FlowBuilder/FlowBuilderItem/LoopBuilder/LoopBuilder'
import { NodeType } from '../routes/FlowBuilder/utils/types'

export default {
  title: 'DevFlowItem/NFlowItemLoop',
  component: LoopBuilder,
} as Meta

export const Default = () => {
  const [showModal, setShowModal] = useState(false)

  return (
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <GlobalStyle />
        <NButton onClick={() => setShowModal(true)} style={{ marginLeft: '1rem' }}>
          Show Loop Modal
        </NButton>
        <NModal visible={showModal} setVisible={setShowModal} size="x-large">
          <LoopBuilder
            nodeModal={{
              position: { x: 0, y: 0 },
              type: NodeType.Loop,
            }}
            onCancel={() => setShowModal(false)}
            onSubmit={data => {
              console.log(data)
              setShowModal(false)
            }}
          />
        </NModal>
      </ThemeProvider>
    </BrowserRouter>
  )
}
