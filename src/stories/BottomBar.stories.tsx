import { Meta } from '@storybook/react/types-6-0'
import React from 'react'
import styled from 'styled-components'
import { NTabs } from '../components/NTabs/NTabs'
import { BottomBar } from '../routes/LayoutBuilder/components/BottomBar'

const FieldWrapper = styled('div')`
  display: grid;
  grid-template: 1fr / repeat(5, 1fr);
`

export default {
  title: 'Example/BottomBar',
} as Meta

export const Default = () => {
  const [show, set] = React.useState(false)

  return (
    <>
      <button onClick={() => set(prev => !prev)}>Toggle</button>
      <BottomBar visible={show}>
        <NTabs>
          <NTabs.Tab title="Fields">
            <FieldWrapper>
              {Array.from({ length: 50 }, (_, i) => i + 1).map(i => (
                <div key={i}>Item {i}</div>
              ))}
            </FieldWrapper>
          </NTabs.Tab>
          <NTabs.Tab title="Actions">Actions?</NTabs.Tab>
        </NTabs>
      </BottomBar>
    </>
  )
}
