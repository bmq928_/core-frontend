import { Meta } from '@storybook/react/types-6-0'
import React from 'react'
import { Calendar } from '../components/Calendar/Calendar'
import { GlobalStyle } from '../components/GlobalStyle'

export default {
  title: 'DateTime/Calendar',
} as Meta

export const SingleCalendar = () => {
  return (
    <>
      <GlobalStyle />
      <Calendar />
    </>
  )
}
