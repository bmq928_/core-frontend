import { Meta, Story } from '@storybook/react/types-6-0'
import React from 'react'
import { ThemeProvider } from 'styled-components'
import { GlobalStyle, theme } from '../components/GlobalStyle'
import { NAvatar, NAvatarProps } from '../components/NAvatar/NAvatar'

export default {
  title: 'Example/NAvatar',
  component: NAvatar,
} as Meta

const Template: Story<NAvatarProps> = args => {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <NAvatar {...args} />
    </ThemeProvider>
  )
}

export const Default = Template.bind({})
Default.args = {
  name: 'Test ',
  size: 200,
}
