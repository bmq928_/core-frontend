import { Meta, Story } from '@storybook/react/types-6-0'
import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'
import { GlobalStyle, theme } from '../components/GlobalStyle'
import { NSideBar } from '../components/NLayout'
import { NContainer } from '../components/NLayout/NContainer'
import { NLayout, NLayoutProps } from '../components/NLayout/NLayout'
import { NMenu } from '../components/NMenu/NMenu'
import { NNavBar } from '../components/NNavBar/NNavBar'

export default {
  title: 'Example/NLayout',
  component: NLayout,
} as Meta

const Template: Story<NLayoutProps> = args => {
  return (
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <GlobalStyle />
        <NLayout {...args}>
          <NNavBar></NNavBar>
          <NSideBar mode="dark" title="Object">
            <NMenu type="primary">
              <NMenu.Item to="/object/name_0">Users</NMenu.Item>
              <NMenu.Item to="/object/name_1">Permission</NMenu.Item>
              <NMenu.Item to="/object/name_2">Branches</NMenu.Item>
            </NMenu>
          </NSideBar>
          <NContainer>
            <NContainer.Title>Test Title</NContainer.Title>
            <NContainer.Description>Test Description</NContainer.Description>
            <NContainer.Content>Test Content</NContainer.Content>
          </NContainer>
        </NLayout>
      </ThemeProvider>
    </BrowserRouter>
  )
}

export const Default = Template.bind({})
Default.args = { isRow: true }
