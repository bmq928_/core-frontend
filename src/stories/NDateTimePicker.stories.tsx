import { Meta, Story } from '@storybook/react/types-6-0'
import * as React from 'react'
import { NDateTimePicker, NDateTimePickerProps } from '../components/DateTime/NDateTimePicker'

export default {
  title: 'DateTime/DateTimePicker',
} as Meta

const Template: Story<NDateTimePickerProps> = args => {
  const [innerValue, setInnerValue] = React.useState<Date>(new Date())

  const { value, onValueChange, ...restArgs } = args
  return (
    <>
      <div style={{ width: '500px' }}>
        <NDateTimePicker value={value || innerValue} onValueChange={onValueChange || setInnerValue} {...restArgs} />
      </div>
    </>
  )
}

export const DateTimePicker = Template.bind({})
DateTimePicker.args = {}
