import { Meta, Story } from '@storybook/react/types-6-0'
import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'
import { ReactComponent as Nuclent } from '../assets/nuclent-small.svg'
import { GlobalStyle, theme } from '../components/GlobalStyle'
import { SidebarIcons } from '../components/Icons'
import { NNavBar, NNavBarProps } from '../components/NNavBar/NNavBar'

export default {
  title: 'Example/NNavBar',
  component: NNavBar,
} as Meta

const Template: Story<NNavBarProps> = args => {
  return (
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <GlobalStyle />
        <NNavBar {...args}>
          <NNavBar.Item
            to="/object"
            icon={<SidebarIcons.ObjectOutlined />}
            activeIcon={<SidebarIcons.ObjectFilled />}
          />
          <NNavBar.Item
            to="/people"
            icon={<SidebarIcons.PeopleOutlined />}
            activeIcon={<SidebarIcons.PeopleFilled />}
          />
        </NNavBar>
        <NNavBar />
      </ThemeProvider>
    </BrowserRouter>
  )
}

export const Default = Template.bind({})
Default.args = {
  logo: <Nuclent />,
}
