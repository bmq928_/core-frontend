// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Meta, Story } from '@storybook/react/types-6-0'
import React from 'react'
import styled, { ThemeProvider } from 'styled-components'
import { GlobalStyle, theme } from '../components/GlobalStyle'
import { NColumn, NRow, NRowProps } from '../components/NGrid/NGrid'

export default {
  title: 'Example/NRow',
  component: NRow,
} as Meta

const TestDiv = styled.div`
  height: 50px;
  border: solid black 1px;
  justify-content: center;
  align-items: center;
`

const Template: Story<NRowProps> = args => (
  <ThemeProvider theme={theme}>
    <GlobalStyle />
    <NRow {...args}>
      <NColumn span={4}>
        <TestDiv>Span 4</TestDiv>
      </NColumn>
      <NColumn span={5}>
        <TestDiv style={{ height: 75 }}>Span 5</TestDiv>
      </NColumn>
      <NColumn span={6}>
        <TestDiv>Span 6</TestDiv>
      </NColumn>
      <NColumn span={6} offset={6}>
        <TestDiv>Span 7</TestDiv>
      </NColumn>
    </NRow>
  </ThemeProvider>
)

export const RowDefault = Template.bind({})
RowDefault.args = {}
