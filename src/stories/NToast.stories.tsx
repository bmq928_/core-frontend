import React from 'react'
import 'react-toastify/dist/ReactToastify.css'
import { GlobalIcons } from '../components/Icons'
import { NToast, ToastContainer } from '../components/NToast'

export default {
  title: 'Example/Notification',
}

export const Default = () => {
  return (
    <div>
      <button
        onClick={() => {
          NToast.success({ title: 'Title', subtitle: 'Subtitle', icon: <GlobalIcons.Add /> }, { autoClose: false })
          NToast.info({ title: 'Title', subtitle: 'Subtitle', icon: <GlobalIcons.Add /> }, { autoClose: false })
          NToast.dark(
            {
              title: 'Title',
              icon: <div style={{ height: 16, width: 16, backgroundColor: 'white', borderRadius: '50%' }} />,
            },
            { autoClose: false },
          )
          NToast.error({
            title: 'Title',
            subtitle: 'Subtitle',
            icon: <GlobalIcons.Add />,
            actions: [
              {
                label: 'Close',
                onClick() {
                  alert('What?')
                },
              },
              {
                label: 'What?',
                onClick() {
                  alert('What?')
                },
              },
            ],
          })
          NToast.warning({ title: 'Title', subtitle: 'Subtitle', icon: <GlobalIcons.Add /> })
          NToast.warning({ subtitle: 'Subtitle', icon: <GlobalIcons.Add /> })
          NToast.warning({ title: 'Title', subtitle: 'Subtitle' })
          NToast.warning({ subtitle: 'Subtitle' })
        }}>
        Toast
      </button>
      <ToastContainer />
    </div>
  )
}
