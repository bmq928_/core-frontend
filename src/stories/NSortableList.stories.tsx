import { Meta } from '@storybook/react/types-6-0'
import React from 'react'
import styled from 'styled-components'
import { NCheckbox } from '../components/NCheckbox/NCheckbox'
import { NModal } from '../components/NModal/NModal'
import { moveItem, NSortableList } from '../components/NSortableList/NSortableList'

const Item = styled('div')`
  padding: 10px;
  background: white;
  &:not(:last-child) {
    margin-bottom: 10px;
  }
  border: 1px solid #999;
  &.is-dragging {
    opacity: 0;
  }
  &:not(.is-dragging):hover {
    border-style: dashed;
  }
`

export default {
  title: 'Example/NSortableList',
  component: NSortableList,
} as Meta

const DATA = Array.from({ length: 100 }, (_, i) => `Item ${i + 1}`)

export const Default = () => {
  const [data, setData] = React.useState(DATA)

  const handleSort = moveItem(setData)

  return (
    <NSortableList onSort={handleSort}>
      {data.map(item => (
        <Item key={item}>{item}</Item>
      ))}
    </NSortableList>
  )
}

export const RecordSelection = () => {
  const [data, setData] = React.useState(DATA)
  const handleSort = moveItem(setData)
  const [selected, setSelected] = React.useState<string[]>([])
  const handleSelect = (item: string) => {
    setSelected(prev => {
      if (prev.includes(item)) {
        return prev.filter(i => i !== item)
      }
      return [...prev, item]
    })
  }
  return (
    <NModal visible>
      <NModal.Header title="Record Selection" />
      <NModal.Body>
        <div style={{ maxHeight: 200, overflow: 'auto' }}>
          <NSortableList onSort={handleSort}>
            {data.map(datum => (
              <div
                style={{ display: 'flex', padding: 8 }}
                onClick={() => {
                  handleSelect(datum)
                }}
                key={datum}>
                <div style={{ marginRight: 8 }}>H</div>
                <div style={{ flex: 1 }}>{datum}</div>
                <NCheckbox checked={selected.includes(datum)} onChange={() => handleSelect(datum)} />
              </div>
            ))}
          </NSortableList>
        </div>
      </NModal.Body>
      <NModal.Footer onFinish={() => console.log('finished')} />
    </NModal>
  )
}
