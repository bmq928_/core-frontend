import { Meta, Story } from '@storybook/react/types-6-0'
import * as React from 'react'
import { NTimePicker, NTimePickerProps } from '../components/DateTime/NTimePicker'

export default {
  title: 'DateTime/TimePicker',
} as Meta

const Template: Story<NTimePickerProps> = args => {
  const [innerValue, setInnerValue] = React.useState<Date>()

  const { value, onChangeValue, ...restArgs } = args

  return (
    <>
      <div style={{ width: '200px' }}>
        <NTimePicker value={value || innerValue} onChangeValue={onChangeValue || setInnerValue} {...restArgs} />
      </div>
    </>
  )
}

export const TimePicker = Template.bind({})
TimePicker.args = {
  placeholder: 'Select a time',
  use12HourFormat: false,
  hourPicker: true,
  minutePicker: true,
  secondPicker: true,
  showControl: true,
}
