// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Meta, Story } from '@storybook/react/types-6-0'
import React, { useCallback, useMemo, useState } from 'react'
import { SortingRule } from 'react-table'
import styled from 'styled-components'
import { classnames } from '../common/classnames'
import { color } from '../components/GlobalStyle'
import { NPagination } from '../components/NPagination'
import { NTable, NTableColumnType, NTableInterface } from '../components/NTable/NTable'
import { NTableActions } from '../components/NTable/NTableActions'
import { NTableCollapsedCellClassName } from '../components/NTable/NTableStyledContainer'

const NameCell = styled.div`
  display: flex;
  align-items: center;
  margin-top: 12px;
  margin-bottom: 12px;
`

const TempAvatar = styled.div`
  width: 32px;
  height: 32px;
  border-radius: 16px;
  margin-right: 8px;
  background: ${color('Neutral300')};
`

type DataType = {
  name: string
  age: number
  visits: number
  progress: number
  status: string
  actions?: string[]
}

const newPerson = (index: number) => {
  const statusChance = Math.random()
  return {
    name: `name_${index}`,
    age: Math.floor(Math.random() * 30),
    visits: Math.floor(Math.random() * 100),
    progress: Math.floor(Math.random() * 100),
    status: statusChance > 0.66 ? 'relationship' : statusChance > 0.33 ? 'complicated' : 'single',
  } as DataType
}

function makeData(lens: number) {
  return Array.from(Array(lens).keys()).map(d => {
    return {
      ...newPerson(d),
    }
  }) as DataType[]
}

const data = makeData(20)

const columns: NTableColumnType<DataType> = [
  {
    Header: 'Name',
    accessor: 'name',
    Cell: ({ value }) => (
      <NameCell>
        <TempAvatar />
        {value}
      </NameCell>
    ),
  },
  {
    Header: 'Age',
    accessor: 'age',
    defaultCanSort: true,
  },
  {
    Header: 'Visits',
    accessor: 'visits',
  },
  {
    Header: 'Status',
    accessor: 'status',
  },
  {
    Header: 'Profile Progress',
    accessor: 'progress',
  },
  {
    Header: '',
    accessor: 'actions',
    Cell: () => {
      const [showDropList, setShowDropList] = useState(false)
      return (
        <NTableActions
          showDropList={showDropList}
          setShowDropList={setShowDropList}
          options={[{ title: 'Duplicate' }, { title: 'Delete' }]}
        />
      )
    },
    id: classnames([NTableCollapsedCellClassName]),
  },
]

//////////////////////////////////////
export default {
  title: 'Example/NTable',
  component: NTable,
} as Meta

const Template: Story<Pick<NTableInterface, 'className' | 'rowSelectionConfig' | 'onChangeSort'>> = args => (
  <NTable {...args} columns={columns} data={data.slice(0, 5)} />
)

export const Default = Template.bind({})
Default.args = {}

export const Sort = () => {
  const [transData, setTransData] = useState(data.slice(0, 5))

  const handleSort = useCallback((sortBy: SortingRule<DataType>[]) => {
    if (!sortBy[0]) {
      return setTransData(data.slice(0, 5))
    }
    const { id, desc } = sortBy[0]
    let sort = data.slice(0, 5)
    if (id === 'age') {
      sort.sort((a, b) => (desc ? b.age - a.age : a.age - b.age))
    }
    setTransData(sort)
  }, [])

  return <NTable columns={columns} data={transData} onChangeSort={handleSort} />
}

export const Checkboxes = () => {
  const testOnSelect = () => {}
  return <NTable columns={columns} data={data.slice(0, 5)} rowSelectionConfig={{ onChange: testOnSelect }} />
}

export const Pagination = () => {
  const [currentPage, setCurrentPage] = useState(1)
  const pageData = useMemo(() => {
    return data.slice((currentPage - 1) * 5, currentPage * 5)
  }, [currentPage])

  return (
    <NTable
      columns={columns}
      data={pageData}
      pageSize={5}
      pagination={<NPagination total={4} current={currentPage} onChangePage={setCurrentPage} />}
    />
  )
}
