// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Meta, Story } from '@storybook/react/types-6-0'
import React from 'react'
import styled, { ThemeProvider } from 'styled-components'
import { GlobalStyle, theme } from '../components/GlobalStyle'
import { NColumn, NColumnProps, NRow } from '../components/NGrid/NGrid'

export default {
  title: 'Example/NColumn',
  component: NColumn,
} as Meta

const TestDiv = styled.div`
  height: 50px;
  border: solid black 1px;
  justify-content: center;
  align-items: center;
`

const Template: Story<NColumnProps> = args => (
  <ThemeProvider theme={theme}>
    <GlobalStyle />
    <NRow gutter={[10, 10]}>
      <NColumn {...args}>
        <TestDiv>Col 1</TestDiv>
      </NColumn>
      <NColumn {...args}>
        <TestDiv>Col 2</TestDiv>
      </NColumn>
      <NColumn {...args}>
        <TestDiv>Col 3</TestDiv>
      </NColumn>
      <NColumn {...args}>
        <TestDiv>Col 4</TestDiv>
      </NColumn>
    </NRow>
  </ThemeProvider>
)

export const ColumnDefault = Template.bind({})
ColumnDefault.args = {}
