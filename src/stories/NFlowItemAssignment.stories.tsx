import { Meta } from '@storybook/react/types-6-0'
import React, { useState } from 'react'
import { BrowserRouter } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'
import { GlobalStyle, theme } from '../components/GlobalStyle'
import { NButton } from '../components/NButton/NButton'
import { NModal } from '../components/NModal/NModal'
import { AssignmentModal } from '../routes/FlowBuilder/FlowBuilderItem/FlowItemAssignment/AssignmentModal'
import { NodeType } from '../routes/FlowBuilder/utils/types'

export default {
  title: 'DevFlowItem/NFlowItemAssignment',
  component: AssignmentModal,
} as Meta

export const Default = () => {
  const [showAssignment, setShowAssignment] = useState(false)

  return (
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <GlobalStyle />
        <NButton onClick={() => setShowAssignment(true)} style={{ marginLeft: '1rem' }}>
          Show Assignment
        </NButton>
        <NModal visible={showAssignment} setVisible={setShowAssignment} size="x-large">
          <AssignmentModal
            nodeModal={{
              position: { x: 0, y: 0 },
              type: NodeType.Assignment,
            }}
            onCancel={() => setShowAssignment(false)}
            onSubmit={data => {
              console.log(data)
              setShowAssignment(false)
            }}
          />
        </NModal>
      </ThemeProvider>
    </BrowserRouter>
  )
}
