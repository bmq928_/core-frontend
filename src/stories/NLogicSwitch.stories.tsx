// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Meta, Story } from '@storybook/react/types-6-0'
import React, { useState } from 'react'
import { NLogicSwitch } from '../components/NLogicSwitch/NLogicSwitch'

export default {
  title: 'Input/NLogicSwitch',
  component: NLogicSwitch,
} as Meta

const Template: Story = () => {
  const [operator, setOperator] = useState<string | undefined>('AND')
  return <NLogicSwitch activeOperator={operator} onOperatorChange={setOperator} operators={['AND', 'OR']} />
}

export const Logic = Template.bind({})
