import { Meta } from '@storybook/react/types-6-0'
import React, { useState } from 'react'
import { BrowserRouter } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'
import { GlobalStyle, theme } from '../components/GlobalStyle'
import { NButton } from '../components/NButton/NButton'
import { FlowSelectObjectModal } from '../routes/FlowBuilder/FlowConfigModal/FlowSelectObjectModal'

export default {
  title: 'DevFlowItem/NFlowSelectObject',
  component: FlowSelectObjectModal,
} as Meta

export const Default = () => {
  const [show, set] = useState(false)

  return (
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <GlobalStyle />
        <NButton onClick={() => set(true)}>Show Select Object Modal</NButton>
        <FlowSelectObjectModal showModal={show} setShowModal={set} />
      </ThemeProvider>
    </BrowserRouter>
  )
}
