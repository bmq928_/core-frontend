import { Meta, Story } from '@storybook/react/types-6-0'
import React from 'react'
import { MemoryRouter } from 'react-router-dom'
import { FlowBuilder } from '../routes/FlowBuilder/FlowBuilder'

export default {
  title: 'Example/FlowBuilder',
  component: FlowBuilder,
  parameters: {
    layout: 'fullscreen',
  },
} as Meta

const Template: Story = () => {
  return (
    <MemoryRouter>
      <FlowBuilder />
    </MemoryRouter>
  )
}

export const Default = Template.bind({})
Default.args = {}
