// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Meta, Story } from '@storybook/react/types-6-0'
import React from 'react'
import { ThemeProvider } from 'styled-components'
import { GlobalStyle, theme } from '../components/GlobalStyle'
import { NButton, NButtonProps } from '../components/NButton/NButton'

export default {
  title: 'Example/NButton',
  component: NButton,
} as Meta

const Template: Story<NButtonProps> = args => {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <NButton {...args} />
    </ThemeProvider>
  )
}

export const Primary = Template.bind({})
Primary.args = {
  type: 'primary',
  children: 'Primary',
}

export const Default = Template.bind({})
Default.args = {
  children: 'Default',
  type: 'default',
}

export const Ghost = Template.bind({})
Ghost.args = {
  children: 'Ghost',
  type: 'ghost',
}

export const Outline = Template.bind({})
Outline.args = {
  children: 'Outline',
  type: 'outline',
}

export const Outline2 = Template.bind({})
Outline2.args = {
  children: 'Outline2',
  type: 'outline-2',
}

export const Link = Template.bind({})
Link.args = {
  children: 'Link',
  type: 'link',
}
