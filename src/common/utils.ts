import { ErrorResponse } from '../services/api'

export const AlertErrorMsg = (title: string, error: ErrorResponse) => {
  if (!error.response) {
    return
  }
  const { statusCode, message } = error.response.data
  alert(`${title}\nCode: ${statusCode}\n${message}`)
}
