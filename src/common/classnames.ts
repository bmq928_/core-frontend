export function classnames(inputs: (string | boolean | undefined | null)[]) {
  return [...new Set(inputs)].filter(Boolean).join(' ')
}
