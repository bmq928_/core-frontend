import InputNumber, { InputNumberProps } from 'rc-input-number'
import * as React from 'react'
import styled from 'styled-components'
import { classnames } from '../common/classnames'
import { color } from './GlobalStyle'
import { Caption, InputWrapper, Label, Wrapper } from './NTextInput/NTextInput'
import { RequiredIndicator } from './NTypography'

const Input = styled(InputNumber)`
  flex: 1;
  width: 100%;
  input {
    border: none;
    font-size: 14px;
    line-height: 16px;
    padding: 11px 8px;
    flex: 1;
    width: 100%;
    color: ${color('Neutral900')};

    &:focus {
      outline: 0;
    }

    ::placeholder {
      font-size: inherit;
      color: ${color('Neutral500')};
    }
  }
`

type NCurrencyInputProps = {
  label?: React.ReactNode
  caption?: string
  left?: React.ReactNode
  right?: React.ReactNode
  error?: string
} & InputNumberProps

// TODO: This is not support SSG
let inputId = 1
const getInputId = () => {
  return `currency-input-${inputId++}`
}

export const NCurrencyInput = React.memo(
  React.forwardRef<HTMLInputElement, NCurrencyInputProps>(function Node(
    { className, left, right, error, label, caption, id, required, ...inputProps },
    ref,
  ) {
    const classname = classnames([
      className || false,
      (inputProps?.disabled || false) && 'is-disabled',
      !!error && 'is-error',
    ])
    const inputId = React.useMemo(() => id || getInputId(), [id])
    return (
      <Wrapper className={classname} data-testid="NTextInput-Wrapper">
        {label && (
          <Label htmlFor={inputId} data-testid="NTextInput-Label">
            {required && <RequiredIndicator>*</RequiredIndicator>}
            {label}
          </Label>
        )}
        <InputWrapper data-testid="NTextInput-InputWrapper">
          {left}
          <Input data-testid="NTextInput-Input" ref={ref} id={inputId} {...inputProps} />
          {right}
        </InputWrapper>
        {(error || caption) && <Caption data-testid="NTextInput-Caption">{error || caption}</Caption>}
      </Wrapper>
    )
  }),
)

NCurrencyInput.displayName = 'NCurrencyInput'
