import * as React from 'react'
import styled from 'styled-components'
import { useClickOutside } from '../../hooks/useClickOutside'
import { color, spacing } from '../GlobalStyle'
import { elevation } from '../NElevation'
import { NPerfectScrollbar } from '../NPerfectScrollbar'
import { NPopover, NPopoverPlacement } from '../NPopover'
import { typography } from '../NTypography'

const TriggerWrapper = styled('div')``

const ItemsWrapper = styled(NPerfectScrollbar)`
  ${elevation('Elevation300')};
  border-radius: 4px;
  background: ${color('white')};
  position: relative;
`

export type NMenuPopoverProps = {
  children: React.ReactNode
  trigger: React.ReactNode
  initialOpen?: boolean
  placement?: NPopoverPlacement
  offsetX?: number
  offsetY?: number
  closeOnSelect?: boolean
  header?: React.ReactNode
  footer?: React.ReactNode
  disabled?: boolean
} & React.HTMLAttributes<HTMLDivElement>

export const NMenuPopover = React.memo(function Node({
  trigger,
  children,
  initialOpen = false,
  placement,
  offsetX,
  offsetY,
  closeOnSelect = true,
  onClick,
  footer,
  header,
  disabled = false,
  ...divProps
}: NMenuPopoverProps) {
  const triggerRef = React.useRef<HTMLDivElement>(null)
  const popoverRef = React.useRef<HTMLDivElement>(null)

  const [visible, setVisible] = React.useState(initialOpen)

  useClickOutside(() => setVisible(false), popoverRef, [triggerRef.current])

  const triggerElement = React.isValidElement(trigger) ? trigger : <div>{trigger}</div>

  return (
    <React.Fragment>
      <TriggerWrapper ref={triggerRef}>
        {React.cloneElement(triggerElement, {
          ...triggerElement.props,
          onClick(e: React.MouseEvent) {
            if (!disabled) {
              if (triggerElement.props?.onClick) {
                triggerElement.props.onClick(e)
              }
              setVisible(true)
            }
          },
        })}
      </TriggerWrapper>
      {visible && (
        <NPopover targetRef={triggerRef} ref={popoverRef} placement={placement} offsetX={offsetX} offsetY={offsetY}>
          <ItemsWrapper {...divProps}>
            {header}
            <div
              onClick={e => {
                if (onClick) {
                  onClick(e)
                }
                if (closeOnSelect) {
                  setVisible(false)
                }
              }}>
              {children}
            </div>
            {footer}
          </ItemsWrapper>
        </NPopover>
      )}
    </React.Fragment>
  )
})

NMenuPopover.displayName = 'NMenuPopover'

const ItemWrapper = styled('div')`
  cursor: pointer;
  box-sizing: border-box;
  transition: all 0.3s ease-in-out;
  &:hover {
    background: ${color('Neutral200')};
  }
  display: flex;
  align-items: center;
  height: 40px;
  ${typography('x-small-ui-text')};
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  padding: ${spacing('md')};
`

type NMenuPopoverItemProps = {} & React.HTMLAttributes<HTMLDivElement>

export const NMenuPopoverItem = React.memo(function Node(props: NMenuPopoverItemProps) {
  return <ItemWrapper {...props} />
})

NMenuPopoverItem.displayName = 'NMenuPopoverItem'
