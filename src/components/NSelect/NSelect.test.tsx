import '@testing-library/jest-dom'
import { cleanup, fireEvent, render, screen } from '@testing-library/react'
import React from 'react'
import { ThemeProvider } from 'styled-components'
import { theme } from '../GlobalStyle'
import { NMultiSelect } from './NMultiSelect'
import { NSingleSelect } from './NSingleSelect'

describe('NSingleSelect', () => {
  afterEach(cleanup)
  test('all options should be rendered', () => {
    const options = [
      { label: 'Option1', value: 'value1' },
      { label: 'Option2', value: 'value2' },
      { label: 'Option3', value: 'value3' },
    ]
    render(
      <ThemeProvider theme={theme}>
        <NSingleSelect label="Label" caption="Some helper text" options={options} value="value1" />
      </ThemeProvider>,
    )
    fireEvent.click(screen.getByTestId('NSingleSelect'))
    // return value and 1st option with Option 1 text
    expect(screen.getAllByText('Option1')).toHaveLength(2)
    expect(screen.getByText('Option2')).toBeInTheDocument()
    expect(screen.getByText('Option3')).toBeInTheDocument()
  })
  test('disable select should not show dropdown', () => {
    const options = [
      { label: 'Option1', value: 'value1' },
      { label: 'Option2', value: 'value2' },
      { label: 'Option3', value: 'value3' },
    ]
    render(
      <ThemeProvider theme={theme}>
        <NSingleSelect label="Label" caption="Some helper text" options={options} value="value1" disabled={true} />
      </ThemeProvider>,
    )
    fireEvent.click(screen.getByTestId('NSingleSelect'))
    expect(screen.queryAllByText('Option2')).toHaveLength(0)
    expect(screen.queryAllByText('Option3')).toHaveLength(0)
  })
})

describe('NMultiSelect', () => {
  afterEach(cleanup)
  test('all options should be rendered', () => {
    const options = [
      { label: 'Option1', value: 'value1' },
      { label: 'Option2', value: 'value2' },
      { label: 'Option3', value: 'value3' },
    ]
    const selected = ['value1', 'value2']
    render(
      <ThemeProvider theme={theme}>
        <NMultiSelect label="Label" caption="Some helper text" options={options} values={selected} />
      </ThemeProvider>,
    )
    fireEvent.click(screen.getByTestId('NMultiSelect'))
    expect(screen.getAllByText('Option1')).toHaveLength(2)
    expect(screen.getAllByText('Option2')).toHaveLength(2)
    expect(screen.getByText('Option3')).toBeInTheDocument()
  })
  test('disable select should not show dropdown', () => {
    const options = [
      { label: 'Option1', value: 'value1' },
      { label: 'Option2', value: 'value2' },
      { label: 'Option3', value: 'value3' },
    ]
    const selected = ['value1', 'value2']
    render(
      <ThemeProvider theme={theme}>
        <NMultiSelect disabled label="Label" caption="Some helper text" options={options} values={selected} />
      </ThemeProvider>,
    )
    fireEvent.click(screen.getByTestId('NMultiSelect'))
    expect(screen.getAllByText('Option1')).toHaveLength(1)
    expect(screen.getAllByText('Option2')).toHaveLength(1)
    expect(screen.queryAllByText('Option3')).toHaveLength(0)
  })
})
