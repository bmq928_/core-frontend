import styled from 'styled-components'
import { color, spacing } from '../GlobalStyle'
import { elevation } from '../NElevation'
import { NPerfectScrollbar } from '../NPerfectScrollbar'
import { NTypography } from '../NTypography'

export const SelectContainer = styled.div`
  border: 1px solid ${color('Neutral200')};
  background: ${color('white')};
  box-sizing: border-box;
  border-radius: 4px;
  color: ${color('Neutral700')};
  height: 40px;
  cursor: pointer;
  padding-left: ${spacing('md')};
  padding-right: ${spacing('md')};
  &:hover {
    border: 1px solid ${color('Neutral400')};
  }
  &.disabled {
    border: 1px solid ${color('Neutral200')};
    color: ${color('Neutral400')};
    cursor: not-allowed;
  }
  transition: all 0.3s ease-in-out;
  &.opened {
    border: 1px solid ${color('Primary700')};
  }
`
export const SelectLabel = styled(NTypography.InputLabel)`
  margin-bottom: ${spacing('xs')};
`
export const SelectCaption = styled(NTypography.InputCaption)`
  margin-top: ${spacing('xxs')};
  &.is-error {
    color: ${color('Red700')};
  }
`
export const SelectDropdown = styled.div`
  position: absolute;
  background: ${color('white')};
  ${elevation('Elevation200')};
  border-radius: 4px;
  z-index: 99999;
`

export const NoOption = styled.div`
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: center;
`

export const OptionsContainer = styled(NPerfectScrollbar)`
  transition: all 0.3s ease-in-out;
  padding: ${spacing('xxs')} 0;
`
