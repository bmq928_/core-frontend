import { FC, useMemo, useRef, useState } from 'react'
import styled from 'styled-components'
import { classnames } from '../../common/classnames'
import { useClickOutside } from '../../hooks/useClickOutside'
import { color, spacing } from '../GlobalStyle'
import { Icons } from '../Icons'
import { NPopover } from '../NPopover'
import { RequiredIndicator } from '../NTypography'
import {
  NoOption,
  OptionsContainer,
  SelectCaption,
  SelectContainer,
  SelectDropdown,
  SelectLabel,
} from './CommonStyledSelect'
import { CommonSelectProps, SelectOption } from './model'
import { NOptionTag } from './NOptionTag'

const Wrapper = styled.div`
  width: 240px;
  &.full-width {
    width: 100%;
  }
`

const MultiSelectContainer = styled(SelectContainer)`
  width: 100%;
  min-height: 40px;
  height: auto;
  align-items: center;
  display: inline-flex;
  padding-left: ${spacing('xxs')};
  padding-right: ${spacing('xl')};
  position: relative;
`

const ValuesContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding: 0 ${spacing('xxs')};
  align-items: center;
  width: 100%;
`

const TextLabel = styled.p`
  color: ${color('Neutral700')};
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  max-width: 100%;
`

const Option = styled.div`
  height: 40px;
  cursor: pointer;
  box-sizing: border-box;
  transition: all 0.3s ease-in-out;
  &.selected {
    background: ${color('Primary200')};
    ${TextLabel} {
      font-weight: 500;
    }
  }
  &:hover {
    background: ${color('Neutral200')};
  }
  align-items: center;
  padding-left: ${spacing('md')};
  padding-right: ${spacing('md')};
  display: flex;
  justify-content: space-between;
  max-width: 100%;
`

const CheckIconContainer = styled.div`
  margin-left: ${spacing('md')};
  color: ${color('Primary700')};
`

const ValueInput = styled.input`
  outline: none;
  border: 0px;
  max-width: 100%;
  height: 32px;
  line-height: 32px;

  font-size: 14px;
  font-weight: 400;
`

const OptionTag = styled(NOptionTag)`
  margin-right: ${spacing('xxs')};
  margin-bottom: ${spacing(2)};
  margin-top: ${spacing(2)};
`
const IconContainer = styled.div`
  color: ${color('Neutral500')};
  &.disabled {
    opacity: 0.6;
  }
  position: absolute;
  right: ${spacing('md')};
  min-width: 8px;
  min-height: 8px;
`

export type NMultiSelectProps = CommonSelectProps & {
  values: string[]
  onValuesChange?: (values: string[]) => void
  options: SelectOption[]
  creatable?: boolean
  showArrow?: boolean
  onInputValueChange?: (value?: string) => void
}

export const NMultiSelect: FC<NMultiSelectProps> = ({
  label,
  caption,
  disabled = false,
  required,
  error,
  fullWidth = false,
  placeholder = 'Select some options',
  className = '',
  placement = 'bottom',
  values,
  emptyComponent,
  onValuesChange,
  options,
  creatable,
  onInputValueChange,
  showArrow = true,
  allowClear = false,
  ...rest
}) => {
  const selectRef = useRef<HTMLDivElement>(null)
  const dropdownRef = useRef<HTMLDivElement>(null)
  const inputValueRef = useRef<HTMLInputElement>(null)
  const [showDropdown, setShowDropdown] = useState(false)
  const [inputValue, setInputValue] = useState('')
  const [localOptions, setLocalOptions] = useState<SelectOption[]>([])
  const [showClearIcon, setShowClearIcon] = useState(false)

  const handleClick = () => {
    if (showDropdown || disabled) {
      return
    }
    inputValueRef.current && inputValueRef.current.focus()
    setShowDropdown(prev => !prev)
  }

  const mergedOptions = useMemo(() => {
    return [...options, ...localOptions]
  }, [options, localOptions])

  const selectedOptions = useMemo(() => {
    const allOptions = new Map(mergedOptions.map(i => [i.value, i]))
    return values.reduce((options: SelectOption[], val) => {
      const option = allOptions.get(val)
      if (option) {
        options.push(option)
      }
      return options
    }, [])
  }, [values, mergedOptions])

  const removeValue = (value: string) => {
    onValuesChange && onValuesChange([...values].filter(val => val !== value))
    inputValueRef.current && inputValueRef.current.focus()
  }

  const addValue = (value: string) => {
    onValuesChange && onValuesChange([...values, value])
    inputValueRef.current && inputValueRef.current.focus()
  }

  const addLocalOption = (value: string) => {
    addValue(value)
    setLocalOptions(prev => [...prev, { label: value, value: value, isLocal: true }])
    setInputValue('')
    inputValueRef.current && inputValueRef.current.focus()
  }

  const removeLocalOption = (value: string) => {
    removeValue(value)
    setLocalOptions(prev => [...prev].filter(val => val.value !== value))
    inputValueRef.current && inputValueRef.current.focus()
  }

  const removeLastValue = () => {
    if (values.length > 0) {
      removeLocalOption(values[values.length - 1])
      inputValueRef.current && inputValueRef.current.focus()
    }
  }

  useClickOutside(() => setShowDropdown(false), dropdownRef, selectRef.current ? [selectRef.current] : undefined)

  return (
    <Wrapper className={classnames([fullWidth && 'full-width'])}>
      {label && (
        <SelectLabel>
          {required && <RequiredIndicator>*</RequiredIndicator>}
          {label}
        </SelectLabel>
      )}
      <MultiSelectContainer
        {...rest}
        className={classnames([showDropdown && 'opened', disabled && 'disabled', className])}
        data-testid="NMultiSelect"
        onClick={handleClick}
        ref={selectRef}>
        <ValuesContainer>
          {selectedOptions.map(opt => (
            <OptionTag
              key={opt.value}
              closable
              option={opt}
              onClose={() => {
                if (opt.isLocal) {
                  removeLocalOption(opt.value)
                } else {
                  removeValue(opt.value)
                }
              }}
            />
          ))}

          <ValueInput
            autoFocus
            ref={inputValueRef}
            placeholder={values.length > 0 ? '' : placeholder}
            size={inputValue.length || (values.length <= 0 && placeholder.length) || 1}
            value={inputValue}
            onChange={e => {
              setInputValue(e.target.value)
              onInputValueChange && onInputValueChange(e.target.value)
            }}
            onKeyDown={event => {
              const newValue = (event.target as HTMLInputElement).value

              if (event.key === 'Enter' || event.key === 'Tab') {
                if (newValue.trim() !== '' && !options.some(opt => opt.value === newValue && creatable)) {
                  addLocalOption(newValue)
                }
              }
              if (event.key === 'Backspace' && newValue === '') {
                removeLastValue()
              }
            }}
          />
        </ValuesContainer>
        <IconContainer
          onMouseEnter={() => setShowClearIcon(true)}
          onMouseLeave={() => setShowClearIcon(false)}
          onClick={e => {
            if (allowClear && onValuesChange) {
              e.stopPropagation()
              onValuesChange([])
              setLocalOptions([])
            }
          }}
          className={disabled ? 'disabled' : ''}>
          {allowClear && showClearIcon ? <Icons.Close8 /> : showArrow && <Icons.Down />}
        </IconContainer>
      </MultiSelectContainer>
      {showDropdown && (
        <NPopover targetRef={selectRef} ref={dropdownRef} placement={placement} offsetX={0} offsetY={4} fullWidth>
          <SelectDropdown>
            <OptionsContainer style={{ maxHeight: 270 }}>
              {options.length <= 0 && (emptyComponent ? emptyComponent : <NoOption>No option</NoOption>)}
              {mergedOptions.length > 0 &&
                mergedOptions.map(opt => {
                  const selected = values.includes(opt.value)
                  return (
                    <Option
                      className={classnames([selected && 'selected'])}
                      onClick={e => {
                        e.stopPropagation()
                        if (selected) {
                          if (opt.isLocal) {
                            removeLocalOption(opt.value)
                          } else {
                            removeValue(opt.value)
                          }
                        } else {
                          addValue(opt.value)
                        }
                      }}
                      key={opt.value}>
                      {opt.label ? (
                        typeof opt.label === 'string' ? (
                          <TextLabel>{opt.label}</TextLabel>
                        ) : (
                          opt.label
                        )
                      ) : (
                        <TextLabel>{opt.value}</TextLabel>
                      )}
                      <CheckIconContainer>{selected && <Icons.Check />}</CheckIconContainer>
                    </Option>
                  )
                })}
            </OptionsContainer>
          </SelectDropdown>
        </NPopover>
      )}
      {(error || caption) && (
        <SelectCaption className={classnames([!!error && 'is-error'])}>{error || caption}</SelectCaption>
      )}
    </Wrapper>
  )
}
