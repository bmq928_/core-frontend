import React, { ChangeEvent, useRef } from 'react'
import styled from 'styled-components'
import { color, spacing } from '../GlobalStyle'
import { NTypography } from '../NTypography'

const BigSize = { width: '48px', height: '24px', indicator: '20px', offset: '24px' }
const SmallSize = { width: '32px', height: '16px', indicator: '12px', offset: '16px' }

const Container = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`

const SwitchContainer = styled.div<{ size: 'default' | 'small' }>`
  position: relative;

  display: flex;
  flex-direction: column;
  justify-content: center;

  width: ${props => (props.size === 'small' ? SmallSize.width : BigSize.width)};
  height: ${props => (props.size === 'small' ? SmallSize.height : BigSize.height)};
  border-radius: calc(${props => (props.size === 'small' ? SmallSize.height : BigSize.height)} / 2);

  background-color: ${color('Neutral200')};
  transition: background-color 0.25s;
`

const SwitchIndicator = styled.div<{ size: 'default' | 'small' }>`
  position: absolute;
  top: 2px;
  left: 2px;

  width: ${props => (props.size === 'small' ? SmallSize.indicator : BigSize.indicator)};
  height: ${props => (props.size === 'small' ? SmallSize.indicator : BigSize.indicator)};
  border-radius: calc(${spacing('lg')} / 2);
  background-color: ${color('white')};

  transform: translateX(0px);
  transition: transform 0.25s;
`

const SwitchInput = styled.input<{ containerSize: 'default' | 'small' }>`
  pointer-events: none;
  opacity: 0;
  width: 0;
  height: 0;

  :checked + ${SwitchContainer} {
    background-color: ${color('Primary700')};
    ${SwitchIndicator} {
      transform: translateX(${props => (props.containerSize === 'small' ? SmallSize.offset : BigSize.offset)});
    }
  }
`

const TextContainer = styled.div`
  margin-left: ${spacing('md')};
`

export type NSwitchProps = {
  // style
  className?: string
  size?: 'default' | 'small'
  //props
  disabled?: boolean
  // data
  name?: string
  title?: string
  subtitle?: string
  checked?: boolean
  onChange?: (newVal: boolean) => void
}

export const NSwitch: React.FC<NSwitchProps> = ({
  className,
  size = 'default',
  disabled,
  name,
  title,
  subtitle,
  checked,
  onChange,
}) => {
  const checkboxRef = useRef<HTMLInputElement>(null)

  const handleOnChange = (e: ChangeEvent<HTMLInputElement>) => {
    onChange && onChange(e.target.checked)
  }

  return (
    <Container
      data-testid="Container"
      className={className}
      onClick={() => {
        !disabled && checkboxRef.current?.click()
      }}>
      <SwitchInput
        data-testid="SwitchInput"
        containerSize={size}
        ref={checkboxRef}
        type="checkbox"
        name={name}
        disabled={disabled}
        checked={checked}
        onChange={handleOnChange}
      />
      <SwitchContainer size={size}>
        <SwitchIndicator size={size} />
      </SwitchContainer>
      {(!!title || !!subtitle) && (
        <TextContainer>
          {title && <NTypography.Headline className={disabled ? 'disabled' : ''}>{title}</NTypography.Headline>}
          {subtitle && <NTypography.Caption className={disabled ? 'disabled' : ''}>{subtitle}</NTypography.Caption>}
        </TextContainer>
      )}
    </Container>
  )
}
