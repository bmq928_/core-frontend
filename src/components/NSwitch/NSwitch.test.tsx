import '@testing-library/jest-dom'
import { cleanup, render } from '@testing-library/react'
import { ThemeProvider } from 'styled-components'
import { theme } from '../GlobalStyle'
import { NSwitch } from './NSwitch'

describe('NSwitch', () => {
  afterEach(cleanup)

  test('NSwitch clickable', () => {
    const { getByTestId } = render(
      <ThemeProvider theme={theme}>
        <NSwitch />
      </ThemeProvider>,
    )

    const container = getByTestId('Container')
    container.click()
    const SwitchInput = getByTestId('SwitchInput')
    expect(SwitchInput).toBeChecked()
  })

  test('disabled NSwitch should be disabled', () => {
    const { getByTestId } = render(
      <ThemeProvider theme={theme}>
        <NSwitch disabled />
      </ThemeProvider>,
    )

    const container = getByTestId('Container')
    container.click()
    const SwitchInput = getByTestId('SwitchInput')
    expect(SwitchInput).not.toBeChecked()
    expect(SwitchInput).toBeDisabled()
  })
})
