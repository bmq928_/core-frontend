import styled from 'styled-components'
import { color, spacing } from '../GlobalStyle'

export type NTabBarUnderlineProps = {
  selectedElement?: Element
  width: number
  translateX: number
}

export const NTabBarUnderline = styled.div<{ width?: number; translateX?: number }>`
  position: absolute;
  bottom: 0;
  width: ${props => (props.width ? spacing(props.width) : '0px')};
  height: 2px;
  background-color: ${color('Primary700')};
  transform: translateX(${props => (props.translateX ? spacing(props.translateX) : '0px')});

  transition: width 0.25s, transform 0.25s;
`
