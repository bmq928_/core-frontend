import { forwardRef } from 'react'
import styled from 'styled-components'
import { classnames } from '../../common/classnames'
import { color, spacing } from '../GlobalStyle'
import { typography } from '../NTypography'
import { TabType } from './model'

export const NTabBarItemClassname = 'NTabBarItem'

const CounterContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  pointer-events: none;

  margin-left: ${spacing('sm')};

  width: 26px;
  height: 20px;
  border-radius: 4px;

  &.default {
    color: ${color('Neutral700')};
    background-color: ${color('Neutral200')};
  }
`

const Container = styled.li`
  display: flex;
  flex-direction: row;
  align-items: center;

  border-radius: 3px;
  cursor: pointer;
  transition: color 0.25s;

  &.default {
    height: 54px;
    color: ${color('Neutral500')};
    margin: 0 ${spacing('lg')};

    &:last-of-type {
      margin: 0;
      margin-left: ${spacing('md')};
    }

    &:first-of-type {
      margin: 0;
      margin-right: ${spacing('md')};
    }

    :hover {
      color: ${color('Neutral700')};
      ${CounterContainer} {
        color: ${color('Primary700')};
        background-color: ${color('Primary200')};
      }
    }

    &.is-active {
      color: ${color('Neutral900')};
      ${CounterContainer} {
        color: ${color('white')};
        background-color: ${color('Primary700')};
      }
    }
  }

  &.filled {
    height: 40px;
    padding: 0 ${spacing('md')};

    ${typography('h500')};

    background-color: ${color('Neutral200')};
    color: ${color('Neutral500')};

    :hover {
      background-color: ${color('Neutral300')};
      color: ${color('Neutral700')};
      ${CounterContainer} {
        color: ${color('Primary700')};
        background-color: ${color('Primary200')};
      }
    }

    &.is-active {
      background-color: ${color('white')};
      color: ${color('Neutral900')};
      ${CounterContainer} {
        color: ${color('white')};
        background-color: ${color('Primary700')};
      }
    }
  }
`

export type NTabBarItemProps = {
  counter?: number
  selectedValue?: string
  type?: TabType
} & React.HTMLAttributes<HTMLLIElement>

export const NTabBarItem = forwardRef<HTMLLIElement, NTabBarItemProps>(
  ({ counter, children, type = TabType.Default, className, ...restProps }: NTabBarItemProps, ref) => {
    return (
      <Container data-testid="TabBarItem" ref={ref} className={classnames([type, className])} {...restProps}>
        {children}
        {counter !== undefined && (
          <CounterContainer data-testid="TabBarItemCounter" className={type}>
            {counter}
          </CounterContainer>
        )}
      </Container>
    )
  },
)
