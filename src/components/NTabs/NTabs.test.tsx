import '@testing-library/jest-dom'
import { cleanup, render } from '@testing-library/react'
import { ThemeProvider } from 'styled-components'
import { GlobalStyle, theme } from '../GlobalStyle'
import { NTabs } from './NTabs'

describe('NTabs', () => {
  afterEach(cleanup)

  test('click on tab bar item', () => {
    const mockOnClick = jest.fn()
    const values = ['0', '1', '2']

    const { getAllByTestId } = render(
      <ThemeProvider theme={theme}>
        <GlobalStyle />
        <NTabs onChangeTab={mockOnClick}>
          {values.map(v => (
            <NTabs.Tab key={v} title="Action 1" />
          ))}
        </NTabs>
        ,
      </ThemeProvider>,
    )

    const TabBarItems = getAllByTestId('TabBarItem')
    //
    TabBarItems[1].click()
    expect(mockOnClick.mock.calls.length).toBe(1)
    expect(mockOnClick.mock.calls[0][0]).toBe(1)
  })

  test('tab bar item with counter', () => {
    const values = ['1', '2', '3']

    const { getAllByTestId } = render(
      <ThemeProvider theme={theme}>
        <GlobalStyle />
        <NTabs>
          {values.map((v, i) => (
            <NTabs.Tab key={v} title={`Action ${i.toString()}`} counter={i} />
          ))}
        </NTabs>
        ,
      </ThemeProvider>,
    )

    const TabBarCounters = getAllByTestId('TabBarItemCounter')
    expect(TabBarCounters).toHaveLength(3)
  })
})
