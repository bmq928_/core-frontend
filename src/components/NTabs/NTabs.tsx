import * as React from 'react'
import styled from 'styled-components'
import { classnames } from '../../common/classnames'
import { useActiveIndex } from '../../hooks/useActiveIndex'
import { color, spacing } from '../GlobalStyle'
import { TabType } from './model'
import { NTabBarItem } from './NTabBarItem'
import { NTabBarUnderline } from './NTabBarUnderline'

export const NTabBarUnderLineBackground = styled.div`
  position: absolute;
  bottom: 0;
  width: 100%;
  height: 2px;
  background-color: ${color('Neutral200')};
`

const Wrapper = styled('div')`
  width: 100%;
  display: grid;
  grid-template: 1fr / 1fr;
  grid-template-areas:
    'tablist'
    'tabcontent';

  &.default {
    background-color: ${color('white')};
  }
`

const TabList = styled('ul')`
  position: relative;
  grid-area: tablist;
  display: flex;
  align-items: center;

  li {
    cursor: pointer;
  }

  &.default {
    background-color: ${color('white')};
  }

  &.filled {
    background-color: ${color('transparent')};
    li {
      &:not(:last-child) {
        margin-right: ${spacing('xxs')};
      }
    }
  }
`

type NTabsProps = {
  defaultActive?: number
  active?: number
  onChangeTab?(index: number): void
  children: ReturnType<typeof Tab>[] | ReturnType<typeof Tab>
  leftElement?: React.ReactNode
  rightElement?: React.ReactNode
  showUnderlineBg?: boolean
  type?: TabType
} & React.HTMLAttributes<HTMLDivElement>
export function NTabs({
  defaultActive = 0,
  active,
  onChangeTab,
  children,
  leftElement,
  rightElement,
  showUnderlineBg,
  type = TabType.Default,
  className,
  ...restProps
}: NTabsProps) {
  //
  const tabListRef = React.useRef<(HTMLLIElement | null)[]>([])
  tabListRef.current = []
  const addToRefs = (node: HTMLLIElement | null) => {
    if (node && !tabListRef.current.includes(node)) {
      tabListRef.current.push(node)
    }
  }
  const [allStyled, setAllStyled] = React.useState<{ translateX: number; width: number }[]>([])

  // render tab list items
  const allChild = Array.isArray(children) ? children : [children]
  const [localActive, { goTo }] = useActiveIndex(allChild.length, defaultActive)

  const index = active !== undefined ? active : localActive

  const tabList = React.Children.map(allChild, (child, childIndex) => {
    if (React.isValidElement(child)) {
      const { title, counter, onClick } = child.props as TabProps
      const className = classnames([childIndex === index && 'is-active'])

      const handleClick = (e: React.MouseEvent) => {
        if (onClick) {
          onClick(e)
        }
        if (childIndex === index) {
          return
        }
        if (onChangeTab) {
          onChangeTab(childIndex)
        }
        goTo(childIndex)
      }

      return (
        <NTabBarItem
          ref={addToRefs}
          className={className}
          key={`tab-${index}`}
          counter={counter}
          type={type}
          onClick={handleClick}>
          {title}
        </NTabBarItem>
      )
    }
  })

  React.useEffect(() => {
    const result = tabListRef.current.map(ref => {
      if (!ref) {
        return {
          width: 0,
          translateX: 0,
        }
      }
      const selectedElementRect = ref.getBoundingClientRect()
      const parentRect = ref.parentElement?.getBoundingClientRect()
      return {
        width: selectedElementRect?.width || 0,
        translateX: parentRect && selectedElementRect ? selectedElementRect.left - parentRect.left : 0,
      }
    })
    setAllStyled(result)
  }, [children])

  return (
    <Wrapper className={classnames([className, type])} {...restProps}>
      <TabList className={classnames(['tab-list', type])}>
        {showUnderlineBg && <NTabBarUnderLineBackground className="underline-background" />}
        {type === 'default' && (
          <NTabBarUnderline className="tabbar-underline" data-testid="TabBarUnderline" {...allStyled[index]} />
        )}
        {leftElement}
        {tabList}
        {rightElement}
      </TabList>
      {allChild[index]}
    </Wrapper>
  )
}

NTabs.Tab = Tab

type TabProps = {
  className?: string
  children?: React.ReactNode
  title: React.ReactNode
  counter?: number
  onClick?(e: React.MouseEvent): void
  style?: any
}

function Tab({ className, children, style }: TabProps) {
  return (
    <div className={className} style={style}>
      {children}
    </div>
  )
}

Tab.displayName = 'NTabs.Tab'
