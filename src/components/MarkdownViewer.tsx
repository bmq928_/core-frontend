import * as React from 'react'
import ReactMarkdown from 'react-markdown'
import { Link } from 'react-router-dom'
import remarkGfm from 'remark-gfm'
import styled from 'styled-components'
import { color, spacing } from './GlobalStyle'

function Anchor({ href, rel, ...props }: React.AnchorHTMLAttributes<HTMLAnchorElement>) {
  if (!href) {
    return null
  }
  if (href?.startsWith('http')) {
    // eslint-disable-next-line jsx-a11y/anchor-has-content
    return <a href={href} target="_blank" {...props} rel={`noopener noreferrer ${rel || ''}`} />
  }
  return <Link to={href}>{props.children}</Link>
}

const Wrapper = styled('div')`
  width: 100%;
  table {
    width: 100%;
    border-collapse: none;
    border-spacing: 0;
    th {
      text-transform: uppercase;
    }
    th,
    td {
      border-bottom: 1px solid ${color('Neutral200')};
      padding: ${spacing('xs')};
    }
  }

  a {
    color: ${color('Primary700')};
    text-decoration: underline;
  }

  > *:not(:last-child) {
    margin-bottom: ${spacing('xs')};
  }
`

type MarkdownViewerProps = {
  children: string
}

export const MarkdownViewer = React.memo(function Node({ children }: MarkdownViewerProps) {
  return (
    <Wrapper>
      <ReactMarkdown remarkPlugins={[remarkGfm]} components={{ a: Anchor }}>
        {children}
      </ReactMarkdown>
    </Wrapper>
  )
})

MarkdownViewer.displayName = 'MarkdownViewer'
