import * as React from 'react'
import styled from 'styled-components'
import { classnames } from '../../common/classnames'
import { color, spacing } from '../GlobalStyle'
import { NTypography, RequiredIndicator } from '../NTypography'

const Label = styled(NTypography.InputLabel)`
  margin-bottom: ${spacing('xs')};
`

const Caption = styled(NTypography.InputCaption)`
  margin-top: ${spacing('xxs')};
`

const TextArea = styled.textarea<NTextAreaProps>`
  width: 100%;
  padding: ${spacing('sm')};
  border-radius: 3px;
  border-color: ${color('Neutral200')};
  ::placeholder {
    font-size: inherit;
    color: ${color('Neutral500')};
  }
  &:hover {
    border-color: ${color('Neutral400')};
  }
  // no value will show N200
  &:placeholder-shown {
    border-color: ${color('Neutral200')};
  }
  &:focus-within {
    outline: 0;
    border-color: ${color('Primary700')};
  }

  resize: ${props => props.resize};
  transition: border-color 0.2s ease-in-out;
  font-family: inherit;
`

const TextAreaWrapper = styled.div`
  max-width: 100%;
`

const Wrapper = styled.div`
  &.is-error {
    ${Caption} {
      color: ${color('Red700')};
    }
    ${TextArea} {
      border-color: ${color('Red700')};
    }
  }
  &.is-disabled {
    ${TextArea} {
      cursor: not-allowed;
      background: ${color('Neutral200')};
      &:hover {
        border-color: ${color('Neutral200')};
      }
    }
  }
`

export type NTextAreaProps = {
  className?: string
  label?: string
  caption?: string
  error?: string
  resize?: 'none' | 'both' | 'horizontal' | 'vertical' | 'initial' | 'inherit'
}

export const NTextArea = React.forwardRef<
  HTMLTextAreaElement | null,
  NTextAreaProps & React.TextareaHTMLAttributes<HTMLTextAreaElement>
>(({ className, resize = 'none', label, caption, error, required, id, ...textAreaProps }, ref) => {
  const classname = classnames([
    className || false,
    (textAreaProps?.disabled || false) && 'is-disabled',
    !!error && 'is-error',
  ])
  const inputId = React.useMemo(() => id || getTextInputId(), [id])
  return (
    <Wrapper className={classname}>
      {label && (
        <Label htmlFor={inputId} data-testid="NTextArea-Label">
          {required && <RequiredIndicator>*</RequiredIndicator>}
          {label}
        </Label>
      )}
      <TextAreaWrapper>
        <TextArea ref={ref} required={required} id={inputId} {...textAreaProps} resize={resize} />
      </TextAreaWrapper>
      {(error || caption) && <Caption data-testid="NTextArea-Caption">{error || caption}</Caption>}
    </Wrapper>
  )
})

// TODO: This is not support SSG
let textInputId = 1
const getTextInputId = () => {
  return `text-area-${textInputId++}`
}
