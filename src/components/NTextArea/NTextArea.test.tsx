import '@testing-library/jest-dom'
import { cleanup, render } from '@testing-library/react'
import { ThemeProvider } from 'styled-components'
import { theme } from '../GlobalStyle'
import { NTextArea } from './NTextArea'

describe('NTextArea', () => {
  afterEach(cleanup)

  test('NTextArea show label', () => {
    const TestLabel = 'Test Label'

    const { getAllByTestId } = render(
      <ThemeProvider theme={theme}>
        <NTextArea label={TestLabel} />
      </ThemeProvider>,
    )

    const Label = getAllByTestId('NTextArea-Label')
    expect(Label.length === 1)
    expect(Label[0].nodeValue === TestLabel)
  })

  test('NTextArea show caption', () => {
    const TestLabel = 'Test Label'

    const { getAllByTestId } = render(
      <ThemeProvider theme={theme}>
        <NTextArea caption={TestLabel} />
      </ThemeProvider>,
    )

    const Caption = getAllByTestId('NTextArea-Caption')
    expect(Caption.length === 1)
    expect(Caption[0].nodeValue === TestLabel)
  })

  test('NTextArea show error', () => {
    const TestLabel = 'Test Label'
    const TestError = 'Test error'

    const { getAllByTestId } = render(
      <ThemeProvider theme={theme}>
        <NTextArea caption={TestLabel} error={TestError} />
      </ThemeProvider>,
    )

    const Caption = getAllByTestId('NTextArea-Caption')
    expect(Caption.length === 1)
    expect(Caption[0].nodeValue === TestError)
  })
})
