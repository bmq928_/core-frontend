import { addDays, differenceInDays, endOfMonth, endOfWeek, startOfMonth, startOfWeek } from 'date-fns'

export type WeekStartsOn = 0 | 1 | 2 | 3 | 4 | 5 | 6

export function generateCalendar(startDate: Date | number, weekStartsOn: WeekStartsOn = 0) {
  const firstDayOfMonth = startOfMonth(startDate)
  const firstDayOfWeek = startOfWeek(firstDayOfMonth, {
    weekStartsOn,
  })

  const lastDayOfMonth = endOfMonth(firstDayOfMonth)
  const lastDayOfWeek = endOfWeek(lastDayOfMonth, { weekStartsOn })

  const totalDays = Math.abs(differenceInDays(firstDayOfWeek, lastDayOfWeek)) + 1

  return Array.from({ length: totalDays }, (_, day) => {
    return addDays(firstDayOfWeek, day)
  })
}

export const hour24Values = Array.from({ length: 24 }, (_, i) => `${i}`)

export const hour12Values = ['12', '1', '2', '3', '4', '6', '7', '8', '9', '10', '11']

export const minuteOrSecondValues = (interval: number = 1) => {
  if (interval > 0) {
    return Array.from({ length: 60 / interval }, (_, i) => `${i * interval}`)
  }
  return []
}

export const periodValues = ['AM', 'PM']
