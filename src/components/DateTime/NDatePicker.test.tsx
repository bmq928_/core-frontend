import '@testing-library/jest-dom'
import { cleanup, fireEvent, render } from '../../utils/test-utils'
import { NDatePicker } from './NDatePicker'

describe('NDatePicker', () => {
  afterEach(cleanup)
  test('show calendar on focus', () => {
    const value = new Date('01/01/2021')
    const mockOnChange = jest.fn()

    const { getByTestId, queryByTestId } = render(<NDatePicker value={value} onChangeValue={mockOnChange} />)

    expect(queryByTestId('CalendarContainer')).not.toBeInTheDocument()

    const Input = getByTestId('NTextInput-Input')
    Input.focus()

    expect(queryByTestId('CalendarContainer')).toBeInTheDocument()
  })

  test('press enter valid date change value', async () => {
    const value = new Date('01/01/2021')
    const mockOnChange = jest.fn()

    const { getByTestId } = render(<NDatePicker value={value} onChangeValue={mockOnChange} />)

    const Input = getByTestId('NTextInput-Input')
    Input.focus()
    fireEvent.keyPress(Input, { key: 'Enter', code: 13, charCode: 13 })

    expect(mockOnChange).toBeCalledTimes(1)
  })

  test('press enter non valid, change input value back', async () => {
    const value = new Date('01/01/2021')
    const mockOnChange = jest.fn()

    const { getByTestId } = render(<NDatePicker value={value} onChangeValue={mockOnChange} />)

    const Input = getByTestId('NTextInput-Input')
    Input.focus()
    fireEvent.change(Input, { target: { value: 'test' } })
    fireEvent.keyPress(Input, { key: 'Enter', code: 13, charCode: 13 })

    expect(mockOnChange).toBeCalledTimes(0)
    //@ts-ignore
    expect(Input.value).toBe('01/01/2021')
  })
})
