import { format, getHours, getMinutes, getSeconds, isValid, parse, setHours, setMinutes, setSeconds } from 'date-fns'
import { FC, useEffect, useRef, useState } from 'react'
import styled from 'styled-components'
import { useClickOutside } from '../../hooks/useClickOutside'
import { usePopoverPosition } from '../../hooks/usePopoverPosition'
import { color, spacing } from '../GlobalStyle'
import { GlobalIcons } from '../Icons'
import { NButton } from '../NButton/NButton'
import { elevation } from '../NElevation'
import { NTextInput, NTextInputProps } from '../NTextInput/NTextInput'
import { Portal } from '../Portal'
import { TimePickerModule } from './TimePickerModule'
import { hour12Values, hour24Values, minuteOrSecondValues, periodValues } from './utils'

const Dropdown = styled.div`
  position: absolute;
  border-radius: 3px;
  z-index: 99999;
  ${elevation('Elevation200')};
  background: ${color('white')};
`
const PickersWrapper = styled.div`
  display: flex;
  max-height: 210px;
`
const ControlFooter = styled.div`
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding-left: ${spacing('sm')};
  padding-right: ${spacing('sm')};
  border-top: 1px solid ${color('Neutral200')};
`

export type NTimePickerProps = {
  value?: Date
  onChangeValue(time: Date): void
  placeholder?: string
  use12HourFormat?: boolean
  placement?: 'top' | 'bottom'
  scrollYOffset?: number
  hourPicker?: boolean
  minutePicker?: boolean
  secondPicker?: boolean
  showControl?: boolean
  onOkBtn?: () => void
  secondInterval?: number
  minuteInterval?: number
  hourInterval?: number
} & NTextInputProps &
  Omit<React.InputHTMLAttributes<HTMLInputElement>, 'value'>

export const NTimePicker: FC<NTimePickerProps> = ({
  value,
  onChangeValue,
  placeholder,
  use12HourFormat = false,
  placement = 'bottom',
  scrollYOffset = window.pageYOffset,
  hourPicker = true,
  minutePicker = true,
  secondPicker = true,
  showControl = false,
  secondInterval,
  minuteInterval,
  onOkBtn,
  ...inputProps
}) => {
  const timeFormat = use12HourFormat ? 'hh:mm:ss aaa' : 'HH:mm:ss'
  const [showDropdown, setShowDropdown] = useState(false)
  const inputRef = useRef<HTMLInputElement>(null)
  const dropdownRef = useRef(null)
  const { top, left } = usePopoverPosition(inputRef, dropdownRef, placement === 'bottom' ? 'bottomLeft' : 'topLeft')

  const [input, setInput] = useState<string>((value && format(value, timeFormat)) || '')

  const handleNowBtn = () => {
    onChangeValue(new Date())
    setShowDropdown(false)
  }

  const setHour = (hour: string) => {
    const curVal = value || new Date().setHours(0, 0, 0, 0)
    const newValue = setHours(curVal, Number(hour))
    onChangeValue(newValue)
  }

  const setMinute = (minute: string) => {
    const curVal = value || new Date().setHours(0, 0, 0, 0)
    const newValue = setMinutes(curVal, Number(minute))
    onChangeValue(newValue)
  }

  const setSecond = (second: string) => {
    const curVal = value || new Date().setHours(0, 0, 0, 0)
    const newValue = setSeconds(curVal, Number(second))
    onChangeValue(newValue)
  }

  const setPeriod = (period: string) => {
    //AM / PM
    const curVal = value || new Date().setHours(0, 0, 0, 0)
    const newDateString = `${format(curVal, 'hh:mm:ss')} ${period}`
    const newValue = parse(newDateString, 'hh:mm:ss aaa', new Date())
    onChangeValue(newValue)
  }

  // when value change update input text
  useEffect(() => {
    if (!value) {
      return
    }

    setInput(format(value, timeFormat))
  }, [value])

  useClickOutside(
    () => {
      checkInputFormat()
      setShowDropdown(false)
    },
    dropdownRef,
    (inputRef.current && [inputRef.current]) || undefined,
  )

  const checkInputFormat = () => {
    const newDate = parse(input, timeFormat, new Date())
    if (isValid(newDate)) {
      onChangeValue(newDate)
      return
    }

    setInput(value ? format(value, timeFormat) : '')
  }

  const onKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      inputRef.current?.blur()
      checkInputFormat()
      setShowDropdown(false)
    }
  }

  return (
    <>
      <NTextInput
        placeholder={placeholder || timeFormat}
        right={<GlobalIcons.Time />}
        {...inputProps}
        ref={inputRef}
        value={input}
        autoComplete="off"
        onFocus={() => setShowDropdown(true)}
        onChange={e => setInput(e.target.value)}
        onKeyPress={onKeyPress}
      />
      <Portal>
        {showDropdown && (
          <Dropdown
            ref={dropdownRef}
            style={{
              minWidth: inputRef.current?.parentElement?.offsetWidth || 0,
              top: placement === 'bottom' ? scrollYOffset + top + 4 : scrollYOffset + top - 4,
              left: left - 8,
            }}>
            <PickersWrapper>
              {hourPicker && (
                <TimePickerModule
                  value={value && (use12HourFormat ? format(value, 'h') : getHours(value).toString())}
                  onValueChange={setHour}
                  valueList={use12HourFormat ? hour12Values : hour24Values}
                />
              )}
              {minutePicker && (
                <TimePickerModule
                  value={value && getMinutes(value).toString()}
                  onValueChange={setMinute}
                  valueList={minuteOrSecondValues(minuteInterval)}
                />
              )}
              {secondPicker && (
                <TimePickerModule
                  value={value && getSeconds(value).toString()}
                  onValueChange={setSecond}
                  valueList={minuteOrSecondValues(secondInterval)}
                />
              )}
              {hourPicker && use12HourFormat && (
                <TimePickerModule
                  value={value && format(value, 'aaa')}
                  onValueChange={setPeriod}
                  valueList={periodValues}
                />
              )}
            </PickersWrapper>
            {showControl && (
              <ControlFooter>
                <NButton type="link" size="small" onClick={handleNowBtn}>
                  Now
                </NButton>
                <NButton
                  type="primary"
                  size="small"
                  onClick={() => {
                    onOkBtn && onOkBtn()
                    setShowDropdown(false)
                  }}>
                  Ok
                </NButton>
              </ControlFooter>
            )}
          </Dropdown>
        )}
      </Portal>
    </>
  )
}
