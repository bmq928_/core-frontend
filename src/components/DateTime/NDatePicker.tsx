import { format, isValid, parse } from 'date-fns'
import * as React from 'react'
import styled from 'styled-components'
import { rootPortal } from '../../App'
import { useClickOutside } from '../../hooks/useClickOutside'
import { Calendar } from '../Calendar/Calendar'
import { GlobalIcons } from '../Icons'
import { elevation } from '../NElevation'
import { NTextInput, NTextInputProps } from '../NTextInput/NTextInput'
import { Portal } from '../Portal'

const CalendarWrapper = styled('div')`
  position: absolute;
  top: 0;
  left: 0;
  border-radius: 3px;
  overflow: hidden;
  z-index: 99999;
  ${elevation('Elevation200')};
`

export type NDatePickerProps = {
  children?: React.ReactNode
  dateFormat?: string
  value?: Date
  onChangeValue(date: Date): void
  renderCurrentMonthOnly?: boolean
  canSelectPastDay?: boolean
  mountNode?: React.RefObject<HTMLDivElement>
} & NTextInputProps &
  Omit<React.InputHTMLAttributes<HTMLInputElement>, 'value'>

export function NDatePicker({
  dateFormat = 'dd/MM/yyyy',
  value,
  onChangeValue,
  renderCurrentMonthOnly,
  canSelectPastDay,
  mountNode = rootPortal,
  ...inputProps
}: NDatePickerProps) {
  const [showCalendar, setShowCalendar] = React.useState(false)
  const [inputValue, setInputValue] = React.useState((value && format(value, dateFormat)) || '')

  const inputRef = React.useRef<HTMLInputElement>(null)
  const dropdownRef = React.useRef(null)

  useClickOutside(
    () => {
      checkInputFormat()
      setShowCalendar(false)
    },
    dropdownRef,
    (inputRef.current && [inputRef.current]) || undefined,
  )

  const { top, left } = React.useMemo(() => {
    if (showCalendar) {
      const containerSizes = inputRef.current?.parentElement?.getBoundingClientRect()
      return {
        top: (containerSizes?.top || 0) + (containerSizes?.height || 0) + 4,
        left: containerSizes?.left || 0,
      }
    }
    return { top: 0, left: 0 }
  }, [showCalendar])

  React.useEffect(() => {
    setInputValue((value && format(value, dateFormat)) || '')
  }, [value, dateFormat])

  const checkInputFormat = () => {
    const testInput = inputValue.replace(/\s/g, '')
    const newDate = parse(testInput, dateFormat, 0)
    if (isValid(newDate)) {
      onChangeValue(newDate)
      return
    }
    setInputValue((value && format(value, dateFormat)) || '')
  }

  const onKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      inputRef.current?.blur()
      checkInputFormat()
      setShowCalendar(false)
    }
  }

  return (
    <>
      <NTextInput
        right={<GlobalIcons.Calendar />}
        placeholder={dateFormat}
        {...inputProps}
        ref={inputRef}
        value={inputValue}
        autoComplete="off"
        onChange={event => setInputValue(event.target.value)}
        onKeyPress={onKeyPress}
        onFocus={() => setShowCalendar(true)}
      />
      <Portal mountNode={mountNode}>
        {showCalendar && (
          <CalendarWrapper ref={dropdownRef} style={{ top, left }}>
            <Calendar
              value={value}
              onChangeValue={date => {
                onChangeValue(date)
                setShowCalendar(false)
              }}
              renderCurrentMonthOnly={renderCurrentMonthOnly}
              canSelectPastDay={canSelectPastDay}
            />
          </CalendarWrapper>
        )}
      </Portal>
    </>
  )
}
