import { FC } from 'react'
import styled from 'styled-components'
import { color, spacing } from '../GlobalStyle'
import { NDivider } from '../NDivider'
import { NColumn } from '../NGrid/NGrid'
import { NTypography, RequiredIndicator } from '../NTypography'
import { NDatePicker } from './NDatePicker'
import { NTimePicker } from './NTimePicker'

const DateTimeWrapper = styled.div`
  display: flex;
`
const Label = styled(NTypography.InputLabel)`
  margin-bottom: ${spacing('xs')};
`
const Caption = styled(NTypography.InputCaption)`
  margin-top: ${spacing('xxs')};
  &.error {
    color: ${color('Red700')};
  }
`

const Wrapper = styled('div')`
  display: flex;
  flex-direction: column;
  &.is-error {
    ${Caption} {
      color: ${color('Red700')};
    }
  }
`

export type NDateTimePickerProps = {
  className?: string
  value?: Date
  required?: boolean
  onValueChange: (date: Date) => void
  disabled?: boolean
  dateFormat?: string
  use12HourFormat?: boolean
  label?: string
  caption?: string
  error?: string
}

export const NDateTimePicker: FC<NDateTimePickerProps> = ({
  className,
  value,
  onValueChange,
  disabled,
  required,
  dateFormat = 'dd/MM/yyyy',
  use12HourFormat = false,
  label,
  caption,
  error,
}) => {
  const handleChangeDate = (newDate: Date) => {
    const nowDate = new Date()
    nowDate.setHours(0, 0, 0, 0)

    const result = value ? new Date(value) : nowDate
    result.setFullYear(newDate.getFullYear(), newDate.getMonth(), newDate.getDate())

    onValueChange(result)
  }

  const handleChangeTime = (newDate: Date) => {
    const result = value ? new Date(value) : new Date()
    result.setHours(newDate.getHours(), newDate.getMinutes(), newDate.getSeconds())

    onValueChange(result)
  }

  return (
    <Wrapper className={className}>
      {label && (
        <Label>
          {required && <RequiredIndicator>*</RequiredIndicator>}
          {label}
        </Label>
      )}
      <DateTimeWrapper className={className}>
        <NColumn flex={1}>
          <NDatePicker dateFormat={dateFormat} value={value} onChangeValue={handleChangeDate} disabled={disabled} />
        </NColumn>
        <NDivider vertical size="xs" />
        <NColumn flex={1}>
          <NTimePicker
            use12HourFormat={use12HourFormat}
            value={value}
            onChangeValue={handleChangeTime}
            disabled={disabled}
          />
        </NColumn>
      </DateTimeWrapper>
      <Caption className={error ? 'error' : undefined}>{error || caption}</Caption>
    </Wrapper>
  )
}
