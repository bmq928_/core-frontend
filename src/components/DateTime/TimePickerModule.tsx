import { FC, useEffect, useRef, useState } from 'react'
import styled from 'styled-components'
import { color } from '../GlobalStyle'

const Wrapper = styled.div`
  width: 100%;
  min-width: 50px;
  background: ${color('white')};
  flex: 1;
`
const Scroll = styled.div`
  overflow: auto;
  /* -ms-overflow-style: none; 
  scrollbar-width: none;  */

  ::-webkit-scrollbar-track {
    background: transparent;
    border-right: 1px solid ${color('Neutral200')};
  }
  ::-webkit-scrollbar {
    width: 3px;
  }
  ::-webkit-scrollbar-thumb {
    background: ${color('Neutral400')};
    border-radius: 2px;
  }
  &:not(:hover)::-webkit-scrollbar {
    background: transparent;
  }
  &:not(:hover)::-webkit-scrollbar-thumb {
    background: transparent;
  }
  height: 100%;
`

const ValueWrapper = styled.div`
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  box-sizing: border-box;
  &:hover {
    background: ${color('Neutral200')};
  }
  &.selected {
    background: ${color('Primary200')};
  }
`

// To make all elements be able to scroll to top
const ExtraDiv = styled.div`
  height: 170px; //dropdown = 210 - value = 40 => enough to scroll
`

export const TimePickerModuleClassname = 'TimePickerModule'

type TimePickerModuleProps = {
  value?: string
  valueList: string[]
  onValueChange?: (value: string) => void
  className?: string
}

export const TimePickerModule: FC<TimePickerModuleProps> = ({ value, valueList, onValueChange, className }) => {
  const [isFirstRender, setIsFirstRender] = useState(true)

  const refs = useRef<(HTMLDivElement | null)[]>([])
  refs.current = []

  const addToRefs = (valueWrapper: HTMLDivElement | null) => {
    if (valueWrapper && !refs.current.includes(valueWrapper)) {
      refs.current.push(valueWrapper)
    }
  }

  useEffect(() => {
    if (value) {
      const valueIndex = valueList.indexOf(value)
      valueIndex &&
        refs.current[valueIndex]?.scrollIntoView({
          block: 'start',
          behavior: isFirstRender ? 'auto' : 'smooth',
        })
    }
  }, [value])

  useEffect(() => {
    setIsFirstRender(false)
  }, [])

  return (
    <Wrapper className={className}>
      <Scroll>
        {valueList.map(val => {
          return (
            <ValueWrapper
              className={val === value ? 'selected' : ''}
              ref={addToRefs}
              onClick={() => {
                onValueChange && onValueChange(val)
              }}
              key={val}>
              {val.length === 2 ? val : `0${val}`}
            </ValueWrapper>
          )
        })}
        <ExtraDiv />
      </Scroll>
    </Wrapper>
  )
}
