import React from 'react'
import { toast, ToastContainer as BaseToastContainer, ToastOptions } from 'react-toastify'
import styled from 'styled-components'
import { classnames } from '../common/classnames'
import { ErrorResponse } from '../services/api'
import { color, spacing } from './GlobalStyle'
import { NDivider } from './NDivider'

export const ToastContainer = styled(BaseToastContainer)`
  .Toastify__toast-container {
  }
  .Toastify__toast {
    padding: 12px;
    box-shadow: 0px 18px 28px rgba(9, 30, 66, 0.15), 0px 0px 1px rgba(9, 30, 66, 0.31);
    border-radius: 8px;
  }
  .Toastify__toast-body {
    display: flex;
  }
  .Toastify__toast--title {
    font-weight: 500;
  }
  .Toastify__toast--actions {
    display: flex;
    align-items: center;
  }
  .Toastify__toast--action {
    cursor: pointer;
    color: ${color('white')};
    &:hover {
      color: ${color('Neutral500')};
    }
  }
  .Toastify__toast--subtitle {
    color: ${color('white')};
  }
  .Toastify__toast--info {
    background: ${color('white')};
    color: ${color('black')};
    .Toastify__toast--action {
      color: ${color('white')};
    }
  }
  .Toastify__toast--error {
    background: ${color('danger')};
  }
  .Toastify__toast--warning {
    background: ${color('warning')};
  }
  .Toastify__toast--success {
    background: ${color('Primary700')};
  }
  .Toastify__toast--dark {
    background: ${color('Neutral900')};
    color: ${color('white')};
  }
  .Toastify__progress-bar {
  }
`

type ToastFunction = (options: ToastProps, toastOptions?: Omit<ToastOptions, 'type'>) => string

type ToastType = 'success' | 'error' | 'warning' | 'info' | 'dark'

function createToast(type: ToastType) {
  const fn: ToastFunction = (props, toastOptions) => {
    const autoClose = props.actions ? false : toastOptions?.autoClose ?? 5000
    const toastId = toast(<Toast {...props} />, { ...toastOptions, autoClose, type }) as string
    return toastId
  }
  return fn
}

export const NToast: Record<ToastType, ToastFunction> & { dismiss(toastId: string): void } = {
  success: createToast('success'),
  error: createToast('error'),
  warning: createToast('warning'),
  info: createToast('info'),
  dark: createToast('dark'),
  dismiss: toast.dismiss,
}

type Action = {
  label: string
  onClick(e: React.MouseEvent): void
}

type ToastProps = { title?: React.ReactNode; subtitle?: React.ReactNode; icon?: React.ReactNode; actions?: Action[] }

const ToastContent = styled('div')`
  flex: 1;
  &.has-icon {
    margin-left: ${spacing('md')};
  }
`

const Dot = styled('div')`
  display: flex;
  margin: 0 9px;
  height: 3px;
  justify-content: center;
  div {
    width: 3px;
    height: 3px;
    background: currentColor;
  }
`

export const Toast = ({ title, subtitle, icon, actions }: ToastProps) => {
  return (
    <React.Fragment>
      {icon}
      <ToastContent className={classnames([!!icon && 'has-icon'])}>
        {title && <div className="Toastify__toast--title">{title}</div>}
        {title && subtitle && <NDivider size="sm" />}
        {subtitle && <div className="Toastify__toast--subtitle">{subtitle}</div>}
        {actions && actions.length > 0 && (
          <>
            <NDivider size="sm" />
            <div className="Toastify__toast--actions">
              {actions.map((action, actionIndex) => {
                return (
                  <React.Fragment key={action.label}>
                    <div className="Toastify__toast--action" onClick={action.onClick}>
                      {action.label}
                    </div>
                    {actionIndex + 1 < actions.length && (
                      <Dot>
                        <div />
                      </Dot>
                    )}
                  </React.Fragment>
                )
              })}
            </div>
          </>
        )}
      </ToastContent>
    </React.Fragment>
  )
}

export function getRequestError(title: string, error: ErrorResponse) {
  if (error.response) {
    return {
      title,
      subtitle: `${error.response.data.statusCode} - ${error.response.data.message}`,
    }
  }
  return {
    title,
    subtitle: 'Something went wrong!',
  }
}
