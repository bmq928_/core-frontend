import React, { FC, useMemo } from 'react'
import styled, { css } from 'styled-components'

export type NColumnProps = { flex?: number; span?: number; offset?: number; className?: string }
export const NColumn = styled.div<NColumnProps>`
  display: flex;
  flex-direction: column;
  overflow: hidden;

  ${props => {
    const { flex, span = 24, offset = 0 } = props
    return css`
      ${flex && `flex: ${props.flex};`}
      ${!flex && `width: calc(100% / 24 * ${span});`}
      margin-left: calc(100% / 24 * ${offset});
    `
  }}
`

////
type RowContainerType = {
  isWrap: boolean
  horizontalGap: string
  verticalGap: string
  justify: string
  align: string
}
const RowContainer = styled.div<RowContainerType>`
  display: flex;
  flex-direction: row;
  flex-wrap: ${props => (props.isWrap ? 'wrap' : 'nowrap')};

  justify-content: ${props => props.justify};
  align-items: ${props => props.align};

  margin-left: -${props => props.horizontalGap};
  margin-right: -${props => props.horizontalGap};
  margin-top: -${props => props.verticalGap};
  margin-bottom: -${props => props.verticalGap};

  ${NColumn} {
    padding-left: ${props => props.horizontalGap};
    padding-right: ${props => props.horizontalGap};
    padding-top: ${props => props.verticalGap};
    padding-bottom: ${props => props.verticalGap};
  }
`
export type NRowProps = {
  isWrap?: boolean
  align?: 'flex-start' | 'center' | 'flex-end' | 'stretch'
  justify?: 'flex-start' | 'flex-end' | 'center' | 'space-around' | 'space-between'
  // TODO: responsive gap
  gutter?: number | number[]
  className?: string
}
export const NRow: FC<NRowProps> = ({
  isWrap = false,
  align = 'flex-start',
  justify = 'flex-start',
  gutter = 0,
  children,
  className,
}) => {
  const { horizontalGap, verticalGap } = useMemo(() => {
    if (typeof gutter === 'number') {
      return { horizontalGap: `${gutter / 2}px`, verticalGap: `0px` }
    }

    return { horizontalGap: `${gutter[0] / 2}px`, verticalGap: `${gutter[1] / 2}px` }
    // TODO: responsive gap
  }, [gutter])

  return (
    <RowContainer
      isWrap={isWrap}
      justify={justify}
      align={align}
      horizontalGap={horizontalGap}
      verticalGap={verticalGap}
      className={className}>
      {children}
    </RowContainer>
  )
}
