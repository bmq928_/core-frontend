import { FC } from 'react'
import styled from 'styled-components'
import { color } from '../GlobalStyle'

const Container = styled.div`
  display: inline-flex;
  height: 24px;
  align-items: center;
  box-sizing: border-box;
  border: 1px solid ${color('Neutral200')};
  padding: 3px;
  border-radius: 4px;
  cursor: pointer;
  /* font-family: 'Space Grotesk'; */ //TODO: Add font for this
`
const Operator = styled.div`
  width: 48px;
  display: flex;
  justify-content: center;
  align-items: center;
  color: ${color('Neutral500')};
  font-size: 12px;
  font-weight: 400;
  line-height: 15.31px;
  border-radius: 3px;
  height: 18px;
  &.active {
    color: ${color('white')};
  }
  z-index: 100;
`
const ActiveIndicator = styled.div<{ activeIndex: number }>`
  background: ${color('Primary700')};
  height: 18px;
  width: 48px;
  position: absolute;
  border-radius: 3px;
  transition: transform 0.3s;
  transform: ${props => `translateX(${props.activeIndex * 48}px)`};
`

export type NLogicSwitchProps = {
  activeOperator?: string
  onOperatorChange?: (value?: string) => void
  operators: string[]
}

export const NLogicSwitch: FC<NLogicSwitchProps> = ({
  activeOperator = 'AND',
  onOperatorChange,
  operators = ['AND', 'OR'],
}) => {
  return (
    <Container>
      {operators.map(operator => (
        <Operator
          key={operator}
          className={activeOperator === operator ? 'active' : ''}
          onClick={() => {
            onOperatorChange && onOperatorChange(operator)
          }}>
          {operator}
        </Operator>
      ))}
      <ActiveIndicator activeIndex={operators.indexOf(activeOperator)} />
    </Container>
  )
}
