import '@testing-library/jest-dom'
import { cleanup, render, screen } from '@testing-library/react'
import React from 'react'
import { ThemeProvider } from 'styled-components'
import { theme } from '../GlobalStyle'
import { NLogicSwitch } from './NLogicSwitch'

describe('NLogicSwitch', () => {
  afterEach(cleanup)
  test('NLogicSwitch should render both operator', () => {
    render(
      <ThemeProvider theme={theme}>
        <NLogicSwitch activeOperator="AND" operators={['AND', 'OR']} />
      </ThemeProvider>,
    )
    expect(screen.queryAllByText('AND')).toHaveLength(1)
    expect(screen.queryAllByText('OR')).toHaveLength(1)
  })
  test('NLogicSwitch should render active class operator', () => {
    render(
      <ThemeProvider theme={theme}>
        <NLogicSwitch activeOperator="AND" operators={['AND', 'OR']} />
      </ThemeProvider>,
    )
    expect(screen.getByText('AND')).toHaveClass('active')
  })
})
