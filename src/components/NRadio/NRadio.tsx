import * as React from 'react'
import styled from 'styled-components'
import { color, spacing } from '../GlobalStyle'
import { NTypography } from '../NTypography'

const RadioContainer = styled('label').attrs((props: { disabled?: boolean }) => {
  if (props.disabled) {
    return {
      style: {
        cursor: 'not-allowed',
      },
    }
  }
})<{ disabled?: boolean }>`
  display: flex;
  align-items: center;
  cursor: pointer;
`

const RadioSelectOuter = styled.div<{ isSelected?: boolean; disabled?: boolean }>`
  display: flex;
  justify-content: center;
  align-items: center;

  min-width: 16px;
  height: 16px;

  border-radius: 50%;
  border-style: solid;
  border-width: 1px;

  border-color: ${color('Neutral200')};
  background-color: ${color('white')};

  transition: background-color, border-color 0.25s;
`

const RadioSelectInner = styled.div`
  width: 4px;
  height: 4px;
  background-color: ${color('white')};
  border-radius: 2px;
  display: none;
`

const RadioInput = styled.input`
  pointer-events: none;
  opacity: 0;
  width: 0;
  height: 0;

  :checked ~ ${RadioSelectOuter} {
    background-color: ${color('Primary700')};
    border-color: ${color('transparent')};
    ${RadioSelectInner} {
      display: block;
    }
  }

  :disabled ~ ${RadioSelectOuter} {
    background-color: ${color('Neutral100')};
    border-color: ${color('transparent')};
  }
`

const TextContainer = styled.div`
  margin-left: ${spacing('md')};
`

let inputId = 1

export type NRadioProps = {
  title?: string
  subtitle?: string
} & React.InputHTMLAttributes<HTMLInputElement>

export const NRadio: React.FC<NRadioProps> = React.forwardRef<HTMLInputElement, NRadioProps>(
  ({ id, title, subtitle, className, disabled, ...inputProps }, ref) => {
    const { current: radioId } = React.useRef(id || `input-${++inputId}`)

    return (
      <RadioContainer className={className} data-testid="NRadioContainer" disabled={disabled} htmlFor={radioId}>
        <RadioInput data-testid="NRadioInput" disabled={disabled} ref={ref} type="radio" {...inputProps} id={radioId} />
        <RadioSelectOuter>
          <RadioSelectInner />
        </RadioSelectOuter>
        <TextContainer>
          <NTypography.Headline className={disabled ? 'disabled' : ''}>{title}</NTypography.Headline>
          {subtitle && <NTypography.Caption className={disabled ? 'disabled' : ''}>{subtitle}</NTypography.Caption>}
        </TextContainer>
      </RadioContainer>
    )
  },
)
