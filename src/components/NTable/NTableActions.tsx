import React, { FC, useMemo, useRef } from 'react'
import styled from 'styled-components'
import { rootPortal } from '../../App'
import { useClickOutside } from '../../hooks/useClickOutside'
import { color, spacing } from '../GlobalStyle'
import { Icons } from '../Icons'
import { NButton } from '../NButton/NButton'
import { elevation } from '../NElevation'
import { typography } from '../NTypography'
import { Portal } from '../Portal'

const DropListContainer = styled.div`
  position: absolute;
  display: flex;
  flex-direction: column;
  z-index: 10000;
  padding: ${spacing('xxs')} 0;
  background: ${color('white')};
  border-radius: 4px;
  ${elevation('Elevation200')}
`
const DropListItem = styled(NButton)`
  padding: ${spacing('md')} ${spacing('sm')};
  width: 180px;
  justify-content: flex-start;
  ${typography('ui-text')}

  &:hover {
    background-color: ${color('Neutral200')};
  }
`

type OptionType = {
  title: string
  onClick?: () => void
  isLoading?: boolean
}

export type NTableActionsProps = {
  showDropList: boolean
  setShowDropList: (newState: boolean) => void

  options: OptionType[]
  mountNode?: React.RefObject<HTMLDivElement>
}

export const NTableActions: FC<NTableActionsProps> = ({
  showDropList,
  setShowDropList,
  options,
  mountNode = rootPortal,
}) => {
  const buttonRef = useRef<HTMLButtonElement>(null)
  const dropdownRef = useRef(null)

  useClickOutside(() => {
    setShowDropList(false)
  }, dropdownRef)

  const { top, left } = useMemo(() => {
    if (showDropList) {
      const containerSizes = buttonRef.current?.getBoundingClientRect()
      return {
        top: (containerSizes?.top || 0) + (containerSizes?.height || 0) + 4,
        left: containerSizes?.left ? containerSizes.left - 156 : 0,
      }
    }
    return { top: 0, left: 0 }
  }, [showDropList])

  return (
    <>
      <NButton
        ref={buttonRef}
        type="ghost"
        icon={<Icons.More />}
        style={{ width: 24, height: 24, padding: 0 }}
        onClick={e => {
          e.stopPropagation()
          setShowDropList(true)
        }}
      />
      <Portal mountNode={mountNode}>
        {showDropList && (
          <DropListContainer ref={dropdownRef} style={{ top, left }}>
            {options.map(op => {
              return (
                <DropListItem
                  key={`droplist_item_${op.title}`}
                  loading={op.isLoading}
                  type="ghost"
                  onClick={e => {
                    e.stopPropagation()
                    op.onClick && op.onClick()
                  }}>
                  {op.title}
                </DropListItem>
              )
            })}
          </DropListContainer>
        )}
      </Portal>
    </>
  )
}
