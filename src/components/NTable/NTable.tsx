import React, { PropsWithChildren, ReactElement, useEffect } from 'react'
import {
  CellProps,
  Column,
  HeaderProps,
  Hooks,
  SortingRule,
  TableOptions,
  usePagination,
  useRowSelect,
  useSortBy,
  useTable,
} from 'react-table'
import { useTheme } from 'styled-components'
import { SortOrder } from '../../services/api/request'
import { color } from '../GlobalStyle'
import { Icons } from '../Icons'
import { NCheckbox } from '../NCheckbox/NCheckbox'
import { NSpinner } from '../NSpinner/NSpinner'
import {
  checkboxClassName,
  NTableEmptyIcon,
  NTableHeaderCell,
  NTableHeaderText,
  NTableLoadingContainer,
  NTableSize,
  NTableStyledContainer,
  PaginationWrapper,
  sortSymbolClassName,
} from './NTableStyledContainer'

const selectionHook = (hooks: Hooks<any>) => {
  hooks.allColumns.push(columns => [
    // Let's make a column for selection
    {
      id: checkboxClassName,
      disableResizing: true,
      disableGroupBy: true,
      // to render a checkbox for header
      Header: ({ getToggleAllRowsSelectedProps }: HeaderProps<any>) => {
        const { checked, indeterminate, onChange } = getToggleAllRowsSelectedProps()
        return <NCheckbox checked={checked} indeterminate={indeterminate} onChange={onChange} />
      },
      // to the render a checkbox fro each cell
      Cell: ({ row }: CellProps<any>) => {
        const { checked, onChange } = row.getToggleRowSelectedProps()
        return <NCheckbox checked={checked} onChange={onChange} />
      },
    },
    ...columns,
  ])
}

export type NTableColumnType<T extends object> = Column<T>[]

type RowSelectionConfigType<T extends object> = {
  onChange?: (selectedRows: T[]) => void
}

export interface NTableInterface<T extends object = {}> extends TableOptions<T> {
  columns: NTableColumnType<T>
  isLoading?: boolean

  className?: string
  rowSelectionConfig?: RowSelectionConfigType<T>

  defaultSortBy?: string
  defaultSortOrder?: string
  onChangeSort?: (sortBy: SortingRule<T>[]) => void

  onClickRow?: (data: T) => void

  noDataComponent?: React.ReactNode
  pagination?: React.ReactNode
  pageSize?: number
}

export function NTable<T extends object>(props: PropsWithChildren<NTableInterface<T>>): ReactElement {
  const {
    isLoading,
    className,
    rowSelectionConfig,

    defaultSortBy,
    defaultSortOrder,
    onChangeSort,
    onClickRow,

    columns,
    data,
    initialState,

    noDataComponent,
    pagination,
    pageSize = 0,
    ...tableProps
  } = props
  const theme = useTheme()

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    selectedFlatRows,

    // gotoPage,
    // pageCount,
    setPageSize,

    state: { sortBy },
  } = useTable<T>(
    {
      manualSortBy: !!onChangeSort,
      manualPagination: true,
      columns,
      data,
      initialState: {
        ...initialState,
        sortBy: defaultSortBy
          ? [
              {
                id: defaultSortBy,
                desc: defaultSortOrder === SortOrder.Desc,
              },
              ...(initialState?.sortBy || []),
            ]
          : initialState?.sortBy || [],
      },
      ...tableProps,
    },
    useSortBy,
    usePagination,
    useRowSelect,
    selectionHook,
  )

  // on selected changed
  useEffect(() => {
    rowSelectionConfig?.onChange && rowSelectionConfig?.onChange(selectedFlatRows.map(d => d.original))
  }, [selectedFlatRows])

  // on sort
  useEffect(() => {
    onChangeSort && onChangeSort(sortBy)
  }, [columns, sortBy, onChangeSort])

  // update pageSize
  useEffect(() => {
    if (pageSize > 0) {
      setPageSize(pageSize)
    }
  }, [pageSize, setPageSize])

  return (
    <NTableStyledContainer className={className}>
      <NTableSize minHeight={pageSize > 0 ? pageSize * 40 : 200}>
        <table {...getTableProps()}>
          <thead>
            {headerGroups.map(headerGroup => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map(column => {
                  if (!rowSelectionConfig && column.id === checkboxClassName) {
                    return undefined
                  }
                  return (
                    <th
                      className={column.id === checkboxClassName ? checkboxClassName : column.id}
                      {...column.getHeaderProps(column.defaultCanSort ? column.getSortByToggleProps() : undefined)}>
                      <NTableHeaderCell>
                        <NTableHeaderText>{column.render('Header')}</NTableHeaderText>
                        {column.defaultCanSort && (
                          <div className={sortSymbolClassName}>
                            {column.isSorted ? (
                              column.isSortedDesc ? (
                                <Icons.ArrowDown />
                              ) : (
                                <Icons.ArrowUp />
                              )
                            ) : (
                              <NTableEmptyIcon />
                            )}
                            <Icons.Sort />
                          </div>
                        )}
                      </NTableHeaderCell>
                    </th>
                  )
                })}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {data.length > 0 &&
              rows.map(row => {
                prepareRow(row)
                return (
                  <tr {...row.getRowProps()}>
                    {row.cells.map(cell => {
                      if (!rowSelectionConfig && cell.column.id === checkboxClassName) {
                        return undefined
                      }
                      return (
                        <td
                          className={cell.column.id === checkboxClassName ? checkboxClassName : cell.column.id}
                          {...cell.getCellProps()}
                          onClick={() => {
                            onClickRow && cell.column.id !== checkboxClassName && onClickRow(row.original)
                          }}>
                          {cell.render('Cell')}
                        </td>
                      )
                    })}
                  </tr>
                )
              })}
          </tbody>
        </table>
        {data.length <= 0 && noDataComponent}
        {isLoading && (
          <NTableLoadingContainer>
            <NSpinner size={30} strokeWidth={2} color={color('Primary900')({ theme })} />
          </NTableLoadingContainer>
        )}
      </NTableSize>
      {pagination && <PaginationWrapper>{pagination}</PaginationWrapper>}
    </NTableStyledContainer>
  )
}
