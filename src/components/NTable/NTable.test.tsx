import '@testing-library/jest-dom'
import { cleanup, render } from '@testing-library/react'
import { ThemeProvider } from 'styled-components'
import { GlobalStyle, theme } from '../GlobalStyle'
import { NTable, NTableColumnType } from './NTable'

describe('NTable', () => {
  afterEach(cleanup)

  test('render pagination', () => {
    const columns: NTableColumnType<any> = [
      {
        Header: 'Display Name',
        accessor: 'displayName',
      },
      {
        Header: 'Name',
        accessor: 'name',
      },
      {
        Header: 'Description',
        accessor: 'description',
      },
    ]

    const { getAllByTestId } = render(
      <ThemeProvider theme={theme}>
        <GlobalStyle />
        <NTable columns={columns} data={[]}></NTable>,
      </ThemeProvider>,
    )

    const Pagination = getAllByTestId('NPagination')

    expect(Pagination).toHaveLength(1)
  })
})
