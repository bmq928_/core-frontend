import React, { ChangeEvent, ReactElement } from 'react'
import styled from 'styled-components'
import { color, spacing } from '../GlobalStyle'
import { GlobalIcons } from '../Icons'
import { NButton } from '../NButton/NButton'
import { NDivider } from '../NDivider'
import { NTextInput } from '../NTextInput/NTextInput'
import { typography } from '../NTypography'

const Container = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: ${spacing('md')};
  height: 42px;

  .input {
    display: flex;
    flex: 1;
    div {
      flex: 1;
    }

    svg {
      width: 24px;
      height: 24px;
      path {
        fill: ${color('Neutral500')};
      }
    }
  }

  .deselected-btn {
  }
`

const Flex = styled.div`
  display: flex;
  flex: 1;
  justify-content: stretch;
  align-items: center;
`

const Text = styled.p`
  ${typography('ui-text')};
  margin-right: ${spacing('lg')};
`

export type NTableToolbarProps<T extends object> = {
  searchConfig: {
    value?: string
    onChange: (e: ChangeEvent<HTMLInputElement>) => void
  }
  // sortByConfig: {
  //   sortByValue?: string
  //   sortByOptions: { label: string; value: string }[]
  //   onChange: Dispatch<SetStateAction<string | undefined>>
  // }
  selectedConfig: {
    selected: T[]
    onDeSelectAll: () => void
    onDelete: (selected: T[]) => void
  }
}

export function NTableToolbar<T extends object>(props: NTableToolbarProps<T>): ReactElement {
  const { selectedConfig, searchConfig } = props

  return (
    <Container>
      <Flex>
        {selectedConfig.selected.length > 0 ? (
          <>
            <Text>{selectedConfig.selected.length} object selected</Text>
            <NButton className="deselected-btn" type="default" onClick={selectedConfig.onDeSelectAll}>
              Deselected All
            </NButton>
            <NDivider vertical size="md" />
            <NButton
              type="outline"
              icon={<GlobalIcons.Trash />}
              onClick={() => selectedConfig.onDelete(selectedConfig.selected)}>
              Delete
            </NButton>
          </>
        ) : (
          <NTextInput
            className="input"
            left={<GlobalIcons.Search />}
            placeholder="Search"
            value={searchConfig.value}
            onChange={searchConfig.onChange}
          />
        )}
      </Flex>
      {/* <Text>Sort by</Text>
      <NSingleSelect
        value={sortByConfig.sortByValue}
        options={sortByConfig.sortByOptions}
        onValueChange={sortByConfig.onChange}
      /> */}
    </Container>
  )
}
