import React, { ReactNode, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { useDispatch } from 'react-redux'
import styled from 'styled-components'
import { ReactComponent as NuclentCoreIcon } from '../../assets/icons/nuclent-core.svg'
import { authActions } from '../../redux/slices/authSlice'
import { sessionActions, useSession } from '../../redux/slices/sessionSlice'
import { APIServices } from '../../services/api'
import { AppQueryKey } from '../../services/api/endpoints/app'
import { AppViewerResponse } from '../../services/api/models'
import { color, spacing } from '../GlobalStyle'
import { GlobalIcons } from '../Icons'
import { NAvatar } from '../NAvatar/NAvatar'
import { NButton } from '../NButton/NButton'
import { NDivider } from '../NDivider'
import { NMenuPopover, NMenuPopoverItem } from '../NMenuPopover'
import { NSpinner } from '../NSpinner/NSpinner'
import { NTextInput } from '../NTextInput/NTextInput'
import { NToast } from '../NToast'
import { typography } from '../NTypography'
import { AppLauncher } from './AppLauncher'
import { ReactComponent as AppDrawerIcon } from './icons/app-drawer.svg'
import { ReactComponent as UnionIcon } from './icons/union-icon.svg'

const Wrapper = styled('div')`
  display: flex;
  align-items: center;
  background-color: ${color('white')};
  min-height: 56px;
  box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.05), 0px 1px 2px rgba(0, 0, 0, 0.1);
  padding-left: ${spacing('md')};
  padding-right: ${spacing('md')};
  z-index: 10;
`

const LeftElement = styled('div')`
  flex: 1;
  display: flex;
  align-items: center;
`

const Title = styled('div')`
  margin-left: ${spacing('xs')};
  ${typography('h500')}
  color:  ${color('Neutral700')};
`
const TitleText = styled.span`
  color: ${color('Neutral500')};
`

const RightElement = styled('div')`
  flex: 1;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  margin-right: ${spacing('sm')};
`

const MenuIcon = styled(AppDrawerIcon)`
  cursor: pointer;
`

const AppMenu = styled(NMenuPopover)`
  width: 300px;
  padding: ${spacing('md')};
  padding-top: ${spacing('xl')};
  padding-bottom: ${spacing('lg')};
  z-index: 10;
`

const UserMenu = styled(NMenuPopover)`
  width: 300px;
  padding: ${spacing('md')};
  z-index: 10;
`
const AvatarWithPointer = styled(NAvatar)`
  cursor: pointer;
`
const UserInfoWrapper = styled.div`
  height: 56px;
  padding-left: ${spacing('md')};
  padding-right: ${spacing('md')};
  display: flex;
  align-items: center;
`
const UserInfo = styled.div`
  margin-left: ${spacing('sm')};
`

const ProfileTitle = styled.p`
  ${typography('button')}
  margin-bottom: ${spacing('xxs')};
`
const ProfileEmail = styled.p`
  ${typography('x-small-ui-text')}
`

const AppMenuHeader = styled.div`
  padding: ${spacing('xs')} ${spacing('md')};
  ${typography('overline')}
  color: ${color('Neutral500')};
`

const CustomMenuItem = styled(NMenuPopoverItem)`
  padding-left: ${spacing('md')};
  padding-right: ${spacing('md')};
  display: flex;
  ${typography('small-bold-text')};
  align-items: center;
  height: 48px;
`

const CenterElement = styled('div')`
  min-width: 40%;
`

const FooterContainer = styled.div`
  display: flex;
  align-items: center;
  padding-left: ${spacing('md')};
`

const CurrentApp = styled.div`
  display: flex;
  margin-left: ${spacing('md')};
  margin-right: ${spacing('md')};
  border-bottom: 1px solid ${color('Neutral200')};
  padding-bottom: ${spacing('sm')};
  margin-bottom: ${spacing('sm')};
`

const CurrentAppName = styled.p`
  ${typography('button')}
  font-weight: bold;
  color: ${color('Neutral700')};
  width: 180px;
`

const CurrentAppDesc = styled.p`
  ${typography('small-ui-text')}
  color: ${color('Neutral400')};
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
  width: 180px;
`

const AppItemName = styled.div`
  width: 220px;
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
`

const APP_LIMIT = 5

type AppHeaderProps = {
  icon?: React.ReactNode
  left?: ReactNode
  right?: ReactNode
  onSelectApp: (appId: string) => void
  selectedApp?: AppViewerResponse
} & React.HTMLAttributes<HTMLDivElement>

export function AppHeader({ left, onSelectApp, right, selectedApp, ...restProps }: AppHeaderProps) {
  const dispatch = useDispatch()
  const { t } = useTranslation()
  const [showAppLauncher, setShowAppLauncher] = useState(false)
  const queryClient = useQueryClient()
  const session = useSession()

  // console.log(session)

  const { mutate: logout } = useMutation(APIServices.Auth.logout, {
    onSuccess: () => {
      dispatch(authActions.removeToken())
      queryClient.clear()
      dispatch(sessionActions.clearSession())
    },
    onError: error => {
      NToast.error({ title: 'Logout unsuccessful', subtitle: error.response?.data.message })
    },
  })

  const { data: appsData, isLoading } = useQuery(
    [AppQueryKey.getApps, { limit: APP_LIMIT }],
    APIServices.Apps.getApps,
    {
      select(res) {
        return res.data
      },
    },
  )

  const handleLogout = () => {
    logout()
  }

  return (
    <>
      <Wrapper {...restProps}>
        <LeftElement>
          <AppMenu
            offsetY={12}
            header={
              <>
                {selectedApp && (
                  <CurrentApp>
                    <NAvatar name={selectedApp.name} size={40} variant="rounded" />
                    <NDivider vertical size="md" />
                    <div>
                      <CurrentAppName>{selectedApp.name}</CurrentAppName>
                      <NDivider size="xxs" />
                      <CurrentAppDesc>{selectedApp.description}</CurrentAppDesc>
                    </div>
                  </CurrentApp>
                )}
                <AppMenuHeader>SWITCH TO</AppMenuHeader>
              </>
            }
            disabled={isLoading}
            trigger={(isLoading && <NSpinner size={24} />) || <MenuIcon />}>
            {(appsData?.data || []).map(app => (
              <CustomMenuItem onClick={() => onSelectApp(app.id)} key={app.id}>
                <NAvatar name={app.name} size={32} variant="rounded" />
                <NDivider vertical size="sm" />
                <AppItemName>{app.name}</AppItemName>
              </CustomMenuItem>
            ))}
            <FooterContainer>
              <UnionIcon />
              <NDivider vertical size="sm" />
              <NButton
                size="small"
                type="link"
                onClick={() => {
                  setShowAppLauncher(true)
                }}>
                All apps
              </NButton>
            </FooterContainer>
          </AppMenu>
          {left || (
            <>
              <NDivider vertical size="xs" />
              <NuclentCoreIcon />
              <Title>
                core <TitleText>setup</TitleText>
              </Title>
            </>
          )}
        </LeftElement>
        <CenterElement>
          <NTextInput left={<GlobalIcons.Search />} placeholder="Search Nuclent Core" />
        </CenterElement>
        <RightElement>
          {right}
          <NDivider size="md" vertical />
          <UserMenu
            offsetY={12}
            placement="bottomRight"
            header={
              <UserInfoWrapper>
                <NAvatar name={session.user_name || ''} variant="circle" size={32} />
                <UserInfo>
                  <ProfileTitle>{t('dashboard.profile')}</ProfileTitle>
                  <ProfileEmail>{session.user_name}</ProfileEmail>
                </UserInfo>
              </UserInfoWrapper>
            }
            trigger={<AvatarWithPointer name={session.user_name || ''} variant="circle" size={32} />}>
            <NMenuPopoverItem>{t('dashboard.profile')}</NMenuPopoverItem>
            <NMenuPopoverItem onClick={handleLogout}>{t('dashboard.logout')}</NMenuPopoverItem>
          </UserMenu>
        </RightElement>
      </Wrapper>
      {showAppLauncher && (
        <AppLauncher
          showAppLauncher={showAppLauncher}
          setShowAppLauncher={setShowAppLauncher}
          setSelectedApp={onSelectApp}
        />
      )}
    </>
  )
}
