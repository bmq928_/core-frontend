import React, { FC } from 'react'
import { useQuery } from 'react-query'
import styled from 'styled-components'
import { useSearchText } from '../../hooks/useSearchText'
import { LoadingView } from '../../routes/AppViewer/components/LoadingView'
import { APIServices } from '../../services/api'
import { AppQueryKey } from '../../services/api/endpoints/app'
import { color, spacing } from '../GlobalStyle'
import { GlobalIcons } from '../Icons'
import { NAvatar } from '../NAvatar/NAvatar'
import { NDivider } from '../NDivider'
import { NModal } from '../NModal/NModal'
import { NPerfectScrollbar } from '../NPerfectScrollbar'
import { NTextInput } from '../NTextInput/NTextInput'
import { typography } from '../NTypography'

const AppListWrapper = styled.div`
  padding: ${spacing('xl')};
  padding-bottom: 0;
`

const AppListGrid = styled(NPerfectScrollbar)`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: ${spacing('xl')};
  padding: ${spacing('xl')} 0;
`

const AppItem = styled.div`
  display: flex;
  height: 80px;
  border-radius: 4px;
  border: 1px solid ${color('Neutral200')};
  &:hover {
    border-color: ${color('Primary700')};
  }
  align-items: center;
  padding: ${spacing('lg')};
  transition: background-color 0.25s;
  cursor: pointer;
`

const AppName = styled.p`
  ${typography('button')}
  margin-bottom: ${spacing('xs')};
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
  width: 266px;
`

const AppDesc = styled.p`
  ${typography('x-small-ui-text')}
  color: ${color('Neutral400')};
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
  width: 266px;
`

const AppListScrollArea = styled.div`
  height: 600px;
`

const LoadingViewContainer = styled.div`
  display: flex;
  height: 100%;
  align-items: center;
  justify-content: center;
`

type AppLauncherProps = {
  showAppLauncher: boolean
  setShowAppLauncher: (show: boolean) => void

  setSelectedApp: (selectedAppId: string) => void
}

export const AppLauncher: FC<AppLauncherProps> = ({ showAppLauncher, setShowAppLauncher, setSelectedApp }) => {
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText()

  const { data: appsData, isLoading: isLoadingAppsData } = useQuery(
    [
      AppQueryKey.getApps,
      {
        searchText: searchText,
      },
    ],
    APIServices.Apps.getApps,
    { keepPreviousData: true },
  )

  return (
    <NModal size="x-large" setVisible={setShowAppLauncher} visible={showAppLauncher}>
      <NModal.Header onClose={() => setShowAppLauncher(false)}>App Launcher</NModal.Header>
      <NModal.Body>
        <AppListWrapper>
          <NTextInput
            className="input"
            left={<GlobalIcons.Search />}
            placeholder="Search"
            value={searchValue}
            onChange={e => {
              handleSearchChange(e.target.value)
            }}
          />
          <AppListScrollArea>
            {isLoadingAppsData ? (
              <LoadingViewContainer>
                <LoadingView />
              </LoadingViewContainer>
            ) : (
              <AppListGrid>
                {appsData?.data &&
                  appsData?.data.data.map(app => (
                    <AppItem
                      key={app.id}
                      onClick={() => {
                        setSelectedApp(app.id)
                        setShowAppLauncher(false)
                      }}>
                      <NAvatar name={app.name} size={40} variant="rounded" />
                      <NDivider vertical size="lg" />
                      <div>
                        <AppName>{app.name}</AppName>
                        <AppDesc>{app.description}</AppDesc>
                      </div>
                    </AppItem>
                  ))}
              </AppListGrid>
            )}
          </AppListScrollArea>
        </AppListWrapper>
      </NModal.Body>
    </NModal>
  )
}
