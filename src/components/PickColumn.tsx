import { FC, ReactNode, useCallback, useMemo, useState } from 'react'
import styled, { useTheme } from 'styled-components'
import { classnames } from '../common/classnames'
import { color, spacing } from './GlobalStyle'
import { GlobalIcons, Icons } from './Icons'
import { NButton } from './NButton/NButton'
import { NDivider } from './NDivider'
import { NPerfectScrollbar } from './NPerfectScrollbar'
import { NSortableList } from './NSortableList'
import { NSpinner } from './NSpinner/NSpinner'
import { NTextInput } from './NTextInput/NTextInput'
import { typography } from './NTypography'

const Wrapper = styled.div`
  display: flex;
  align-items: center;
`
const ListWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  height: 400px;
  border: 1px solid ${color('Neutral200')};
  border-radius: 4px;
`
const ListHeader = styled.div`
  padding: ${spacing('sm')};
  border-bottom: 1px solid ${color('Neutral200')};
  ${typography('overline')}
`

const ButtonWrapper = styled.div`
  margin-right: ${spacing('xl')};
  margin-left: ${spacing('xl')};
  display: flex;
  flex-direction: column;
`

const RightIcon = styled(GlobalIcons.LeftLarge)`
  transform: rotate(180deg);
`

const SearchInput = styled(NTextInput)`
  margin: ${spacing('xs')};
`

const Option = styled.div`
  padding: ${spacing('md')};
  height: 48px;
  box-sizing: border-box;
  border: 1px solid transparent;
  border-bottom: 1px solid ${color('Neutral200')};
  cursor: pointer;
  &.selected {
    background: ${color('Primary100')};
    border: 1px solid ${color('Primary700')};
  }
  &:hover {
    background: ${color('Neutral200')};
  }
  display: flex;
  align-items: center;
  justify-content: space-between;
  &.sortable {
    cursor: move;
  }
`

const TextLabel = styled.p`
  ${typography('x-small-ui-text')}
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  max-width: 100%;
`

const LoadingContainer = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
`

export type PickColumnProps = {
  selectedOptions?: PickListOption[]
  options: PickListOption[]
  onOptionsChange: (options: PickListOption[]) => void
  leftHeader?: string
  rightHeader?: string
  onSearchValueChange?: (value: string) => void
  searchValue?: string
  isLoading?: boolean
  sortableSelected?: boolean
}

export type PickListOption = {
  label?: string | ReactNode
  value: string
}

export const PickColumn: FC<PickColumnProps> = ({
  selectedOptions = [],
  options,
  onOptionsChange,
  leftHeader,
  rightHeader,
  onSearchValueChange,
  searchValue = '',
  isLoading = false,
  sortableSelected = false,
}) => {
  const [selected, setSelected] = useState<PickListOption[]>([])
  const [deselected, setDeselected] = useState<PickListOption[]>([])
  const theme = useTheme()

  const displayOptions: PickListOption[] = useMemo(() => {
    // remove selected ones from all options
    return options.filter((opt: PickListOption) => {
      return !selectedOptions.some(selected => selected?.value === opt.value)
    })
  }, [selectedOptions, options])

  const addToSelected = (option: PickListOption) => {
    setSelected(prev => [...prev, option])
  }

  const removeFromSelected = (option: PickListOption) => {
    setSelected(prev => prev.filter(opt => opt.value !== option.value))
  }

  const addToDeselected = (option: PickListOption) => {
    setDeselected(prev => [...prev, option])
  }

  const removeFromDeselected = (option: PickListOption) => {
    setDeselected(prev => prev.filter(opt => !opt || opt.value !== option.value))
  }

  //Real update to parent
  const addOptions = () => {
    onOptionsChange([...selectedOptions, ...selected])
    setSelected([])
  }

  const removeOptions = () => {
    onOptionsChange(selectedOptions.filter(i => !!i && !deselected.some(j => !j || j.value === i.value)))
    setDeselected([])
  }

  const addAllOptions = () => {
    onOptionsChange(options)
    setSelected([])
    setDeselected([])
  }

  const removeAllOptions = () => {
    onOptionsChange([])
    setSelected([])
  }

  const moveSelected = useCallback(
    (dragIndex: number, hoverIndex: number) => {
      const dragOption = selectedOptions[dragIndex]
      const newOptions = [...selectedOptions]
      newOptions[dragIndex] = newOptions[hoverIndex]
      newOptions[hoverIndex] = dragOption
      onOptionsChange(newOptions)
    },
    [onOptionsChange, selectedOptions],
  )

  return (
    <Wrapper>
      <ListWrapper>
        <ListHeader>{leftHeader}</ListHeader>
        {onSearchValueChange && (
          <SearchInput
            left={<GlobalIcons.Search />}
            placeholder="Search"
            value={searchValue}
            onChange={e => {
              onSearchValueChange(e.target.value)
            }}
          />
        )}
        {isLoading ? (
          <LoadingContainer>
            <NSpinner size={20} strokeWidth={2} color={color('Primary900')({ theme })} />
          </LoadingContainer>
        ) : (
          <NPerfectScrollbar>
            {displayOptions.map(opt => {
              const isSelected = selected.includes(opt)
              return (
                <Option
                  key={opt.value}
                  className={classnames([isSelected && 'selected'])}
                  onClick={() => {
                    if (isSelected) {
                      removeFromSelected(opt)
                    } else {
                      addToSelected(opt)
                    }
                  }}>
                  {opt.label ? (
                    typeof opt.label === 'string' ? (
                      <TextLabel>{opt.label}</TextLabel>
                    ) : (
                      opt.label
                    )
                  ) : (
                    <TextLabel>{opt.value}</TextLabel>
                  )}
                </Option>
              )
            })}
          </NPerfectScrollbar>
        )}
      </ListWrapper>
      <ButtonWrapper>
        <NButton icon={<GlobalIcons.MoreLeft />} onClick={removeAllOptions} type="outline" />
        <NDivider size="xs" />
        <NButton icon={<GlobalIcons.LeftLarge />} onClick={removeOptions} type="outline" />
        <NDivider size="xs" />
        <NButton icon={<RightIcon />} onClick={addOptions} type="outline" />
        <NDivider size="xs" />
        <NButton icon={<GlobalIcons.MoreRight />} onClick={addAllOptions} type="outline" />
      </ButtonWrapper>
      <ListWrapper>
        <ListHeader>{rightHeader}</ListHeader>
        <NPerfectScrollbar>
          <NSortableList disabled={!sortableSelected} onSort={moveSelected}>
            {selectedOptions.map((opt, index) => {
              const isSelected = deselected.includes(opt)
              return (
                <Option
                  key={opt?.value || `missing_field_${index}`}
                  className={classnames([isSelected && 'selected', sortableSelected && 'sortable'])}
                  onClick={() => {
                    if (isSelected) {
                      removeFromDeselected(opt)
                    } else {
                      addToDeselected(opt)
                    }
                  }}>
                  {opt?.label ? (
                    typeof opt.label === 'string' ? (
                      <TextLabel>{opt.label}</TextLabel>
                    ) : (
                      opt.label
                    )
                  ) : (
                    <TextLabel>{opt?.value || '{Missing field}'}</TextLabel>
                  )}
                  {sortableSelected && <Icons.Reorder />}
                </Option>
              )
            })}
          </NSortableList>
        </NPerfectScrollbar>
      </ListWrapper>
    </Wrapper>
  )
}
