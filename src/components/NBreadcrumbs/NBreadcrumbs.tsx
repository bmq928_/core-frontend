import { FC } from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import { color } from '../GlobalStyle'
import { typography } from '../NTypography'

const BreadcrumbsWrapper = styled('ul')`
  display: flex;
  li {
    ${typography('button')};
    a {
      color: ${color('Neutral400')};
      &:after {
        content: '/';
        margin: 0 4px;
        color: ${color('Neutral400')};
      }
      &:hover {
        color: ${color('Neutral900')};
      }
    }
  }
`

export type Breadcrumb = { path?: string; label: string }

export type NBreadcrumbsProps = {
  breadcrumbs: Breadcrumb[]
}

export const NBreadcrumbs: FC<NBreadcrumbsProps> = ({ breadcrumbs }) => {
  return (
    <BreadcrumbsWrapper>
      {breadcrumbs.map((i, index) => (
        <li key={`item-${index}`}>{(i.path && <Link to={i.path}>{i.label}</Link>) || i.label}</li>
      ))}
    </BreadcrumbsWrapper>
  )
}
