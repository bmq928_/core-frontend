import * as React from 'react'
import styled from 'styled-components'
import { color } from './GlobalStyle'
import { Icons } from './Icons'
import { NButton } from './NButton/NButton'

const Wrapper = styled('div')`
  display: flex;
  button {
    width: 32px;
    height: 32px;
    padding: 0px;
  }
  & > *:not(:last-child) {
    margin-right: 2px;
  }
`

const Separator = styled('div')`
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 3px;

  width: 32px;
  height: 32px;
`

const CurrentPage = styled(Separator)`
  color: ${color('white')};
  background: ${color('Primary700')};
`

const SEPARATOR = 'PAGE_SEPARATOR'
const MAGIC_NUMBER = 3

function calculatePage(total: number, current: number, padding: number = 1): (number | string)[] {
  const length = MAGIC_NUMBER + padding * 2
  if (total <= length + 2) {
    return Array.from({ length: total }, (_, i) => i + 1)
  }

  const isOnlyLeft = current < length
  if (isOnlyLeft) {
    return (Array.from({ length }, (_, i) => i + 1) as (string | number)[]).concat([SEPARATOR, total])
  }

  const isOnlyRight = current > total - length + 1
  if (isOnlyRight) {
    return [1, SEPARATOR].concat(Array.from({ length }, (_, i) => i + total - length + 1))
  }

  const center = Array.from({ length }, (_, i) => {
    if (i === 0 || i + 1 === length) {
      return SEPARATOR
    }
    return i + current - padding
  })

  return ([1] as (string | number)[]).concat(center).concat([total])
}

type NPaginationProps = {
  total: number
  current: number
  onChangePage(page: number): void
  padding?: number
} & React.HTMLAttributes<HTMLDivElement>

// current start from 1
export function NPagination({ total, current, onChangePage, padding, ...divProps }: NPaginationProps) {
  const pages = calculatePage(total, current, padding)
  const canClickPrevious = current - 1 > 0
  const handlePreviousClick = () => {
    if (canClickPrevious) {
      onChangePage(current - 1)
    }
  }

  const canClickNext = current < total
  const handleNextClick = () => {
    if (canClickNext) {
      onChangePage(current + 1)
    }
  }
  if (total <= 1) {
    return null
  }

  return (
    <Wrapper {...divProps}>
      <NButton
        type="ghost"
        size="small"
        icon={<Icons.Left />}
        disabled={!canClickPrevious}
        onClick={handlePreviousClick}
      />
      {pages.map((page, pageIndex) => {
        if (typeof page === 'string') {
          return <Separator key={`sep-${pageIndex}`}>...</Separator>
        }
        const isCurrent = current === page
        if (isCurrent) {
          return <CurrentPage key="current-page">{page}</CurrentPage>
        }
        return (
          <NButton key={page} type="ghost" size="small" onClick={() => onChangePage(page)}>
            {page}
          </NButton>
        )
      })}
      <NButton type="ghost" size="small" icon={<Icons.Right />} disabled={!canClickNext} onClick={handleNextClick} />
    </Wrapper>
  )
}
