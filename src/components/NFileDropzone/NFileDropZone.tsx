import React, { FC } from 'react'
import { DropzoneOptions, useDropzone } from 'react-dropzone'
import styled, { useTheme } from 'styled-components'
import { classnames } from '../../common/classnames'
import { color, getAlpha, spacing } from '../GlobalStyle'
import { GlobalIcons } from '../Icons'
import { NButton } from '../NButton/NButton'
import { NDivider } from '../NDivider'
import { elevation } from '../NElevation'
import { NSpinner } from '../NSpinner/NSpinner'
import { NTypography, typography } from '../NTypography'
import { NFileType } from './NFileType'

const Label = styled(NTypography.InputLabel)`
  margin-bottom: ${spacing('xs')};
`

const Dropzone = styled.div<{ highlight: boolean }>`
  flex: 1;
  display: flex;
  flex-direction: column;
  border-radius: 4px;
  background-color: ${props => (props.highlight ? color('Primary200') : color('Neutral100'))};
  justify-content: center;
  align-items: center;
  cursor: pointer;
`

const Caption = styled(NTypography.InputCaption)`
  margin-top: ${spacing('xxs')};
`

const IconContainer = styled.div`
  display: flex;
  width: 32px;
  height: 32px;
  border-radius: 16px;
  background-color: ${color('Primary700')};
  color: ${color('white')};
  justify-content: center;
  align-items: center;
`

const DropzoneText = styled.div`
  color: ${color('Neutral900')};
  ${typography('small-bold-text')};
`
const DropzoneSubText = styled.span`
  color: ${color('Neutral500')};
`

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  min-height: 200px;
  &.is-error {
    ${Caption} {
      color: ${color('Red700')};
    }
    ${IconContainer} {
      background-color: ${color('Red700')};
    }
    ${Dropzone} {
      background-color: ${color('Red700')}${getAlpha(10)};
    }
  }
`
////////////////////////////
const FilePreviewWrapper = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  ${elevation('Container100')};
`

const FileTop = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: ${spacing('xl')};
  flex: 1;
`

const FileImg = styled.img`
  max-height: 144px;
  width: auto;
`

const FileTypeText = styled.p`
  ${typography('h800')};
  color: ${color('Neutral300')};
  text-transform: uppercase;
`

const FileBottom = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: ${spacing('md')};
  border-top: 1px ${color('Neutral200')} solid;
`

const FileNameText = styled.p`
  ${typography('x-small-bold-text')};
`
const FileInfoText = styled.p`
  ${typography('x-small-ui-text')};
  color: ${color('Neutral400')};
`

type NFileDropzoneProps = {
  className?: string
  label?: string
  caption?: string
  error?: string
  file?: NFileType
  isUploading?: boolean
  onRemove: () => void
} & DropzoneOptions

export const NFileDropzone: FC<NFileDropzoneProps> = ({
  className,
  label,
  caption,
  error,
  file,
  isUploading = false,
  onRemove,
  disabled,
  ...dropzoneOptions
}) => {
  const theme = useTheme()
  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    maxFiles: 1,
    disabled: disabled || isUploading,
    ...dropzoneOptions,
  })

  const classname = classnames([className || false, !!error && 'is-error'])

  if (file) {
    return (
      <Wrapper className={classname}>
        <Label>{label}</Label>
        <FilePreviewWrapper>
          <FileTop>
            {file.url ? <FileImg src={file.url} alt=""></FileImg> : <FileTypeText>{file.mimeType}</FileTypeText>}
          </FileTop>
          <FileBottom>
            <div>
              <FileNameText>{file.name}</FileNameText>
              <FileInfoText>
                {file.size}・{file.mimeType}
              </FileInfoText>
            </div>
            <NButton
              type="ghost"
              icon={<GlobalIcons.Trash />}
              onClick={() => {
                onRemove()
              }}>
              Remove
            </NButton>
          </FileBottom>
        </FilePreviewWrapper>
      </Wrapper>
    )
  }

  return (
    <Wrapper className={classname}>
      <Label>{label}</Label>
      <Dropzone {...getRootProps()} highlight={isUploading || isDragActive}>
        <input {...getInputProps()} />
        {(() => {
          if (isUploading) {
            return (
              <>
                <IconContainer>
                  <NSpinner size={15} strokeWidth={2} color={color('white')({ theme })} />
                </IconContainer>
                <NDivider size="md" />
                <DropzoneText>Uploading...</DropzoneText>
              </>
            )
          }
          if (isDragActive) {
            return (
              <>
                <IconContainer>
                  <GlobalIcons.DownArrow />{' '}
                </IconContainer>
                <NDivider size="md" />
                <DropzoneText>Release file</DropzoneText>{' '}
              </>
            )
          }

          return (
            <>
              <IconContainer>
                <GlobalIcons.Plus />
              </IconContainer>
              <NDivider size="md" />
              <DropzoneText>
                Add file <DropzoneSubText>or drop here</DropzoneSubText>
              </DropzoneText>
            </>
          )
        })()}
      </Dropzone>
      {(error || caption) && <Caption data-testid="NTextInput-Caption">{error || caption}</Caption>}
    </Wrapper>
  )
}
