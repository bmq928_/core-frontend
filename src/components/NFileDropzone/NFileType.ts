export type NFileType = {
  id: string
  name: string
  tagName?: string
  mimeType: string
  size: number
  url?: string
}
