import PerfectScrollbar from 'perfect-scrollbar'
import 'perfect-scrollbar/css/perfect-scrollbar.css'
import * as React from 'react'
import styled from 'styled-components'
import { useCombinedRefs } from '../hooks/useCombinedRefs'

const Scrollable = styled('div')`
  position: relative;
  overflow: hidden;
`

type NPerfectScrollbarProps = {
  options?: PerfectScrollbar.Options
} & React.HTMLAttributes<HTMLDivElement>

export const NPerfectScrollbar = React.memo(
  React.forwardRef<HTMLDivElement, NPerfectScrollbarProps>(function Node({ options, ...divProps }, forwardRef) {
    const scrollRef = React.useRef<HTMLDivElement>(null)
    const ref = useCombinedRefs([forwardRef, scrollRef])

    const [ps, setPs] = React.useState<PerfectScrollbar>()

    React.useLayoutEffect(() => {
      setPs(prev => {
        if (prev) {
          return prev
        }
        return new PerfectScrollbar(scrollRef.current!, options)
      })
    }, [ps, options])

    React.useEffect(() => {
      if (ps) {
        ps.update()
      }
    }, [ps])

    return <Scrollable ref={ref} {...divProps} />
  }),
)

NPerfectScrollbar.displayName = 'NPerfectScrollbar'
