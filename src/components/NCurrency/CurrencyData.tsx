import { FC } from 'react'
import { useSession } from '../../redux/slices/sessionSlice'
import { formatCurrencyValue } from '../../utils/currency-utils'

type Props = {
  dataCurrencyCode: string
  value: number
  convertedValue?: number
}

export const CurrencyData: FC<Props> = ({ dataCurrencyCode, value, convertedValue }) => {
  const { currencyLocale, defaultCurrencyCode } = useSession()
  const currencyValue = currencyLocale ? formatCurrencyValue(currencyLocale, dataCurrencyCode, value) : value.toString()
  const convertedCurrencyValue =
    dataCurrencyCode !== defaultCurrencyCode && currencyLocale && defaultCurrencyCode && convertedValue
      ? formatCurrencyValue(currencyLocale, defaultCurrencyCode, convertedValue)
      : convertedValue?.toString()
  return (
    <>{dataCurrencyCode !== defaultCurrencyCode ? `${currencyValue} (${convertedCurrencyValue})` : currencyValue}</>
  )
}
