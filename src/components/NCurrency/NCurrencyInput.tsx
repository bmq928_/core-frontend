import React, { useMemo, useState } from 'react'
import styled from 'styled-components'
import { classnames } from '../../common/classnames'
import { useSession } from '../../redux/slices/sessionSlice'
import { color, spacing } from '../GlobalStyle'
import { NTextInputProps } from '../NTextInput/NTextInput'
import { NTypography, RequiredIndicator } from '../NTypography'

const Label = styled(NTypography.InputLabel)`
  margin-bottom: ${spacing('xs')};
`

const InputWrapper = styled('div')`
  display: flex;
  align-items: center;
  padding: 0 ${spacing('xs')};
  transition: border-color 0.3s ease-in-out;
  border-radius: 4px;
  border: 1px solid ${color('Neutral200')};
  &:hover {
    border-color: ${color('Neutral400')};
  }
  &:focus-within {
    border-color: ${color('Primary700')};
  }
`

const InputContainer = styled.div`
  display: flex;
  flex: 1;
  position: relative;
  align-items: center;
`

const Input = styled('input')`
  border: none;
  font-size: 14px;
  line-height: 16px;
  padding: 11px 8px;
  flex: 1;
  width: 100%;
  color: ${color('transparent')};

  &:focus {
    outline: 0;
    color: ${color('Neutral900')};
  }

  ::placeholder {
    font-size: inherit;
    color: ${color('Neutral500')};
  }
`

const MaskedText = styled.div`
  flex: 1;
  position: absolute;
  left: 10px;
  font-size: 14px;
  pointer-events: none;
  color: ${color('Neutral900')};
`

const Caption = styled(NTypography.InputCaption)`
  margin-top: ${spacing('xxs')};
`

const Wrapper = styled('div')`
  display: flex;
  flex-direction: column;
  &.is-error {
    ${InputWrapper} {
      border-color: ${color('Red700')};
    }
    ${Caption} {
      color: ${color('Red700')};
    }
  }
  &.is-disabled {
    ${InputWrapper} {
      cursor: not-allowed;
      background: ${color('Neutral200')};
      &:hover {
        border-color: ${color('Neutral200')};
      }
    }
    ${Input} {
      color: ${color('Neutral500')};
    }
  }
`
// TODO: This is not support SSG
let currencyInputId = 1
const getCurrnecyInputId = () => {
  return `currnecy-input-${currencyInputId++}`
}

type NCurrencyInputProps = NTextInputProps & {
  currencyCode: string
  value: number | string
  onChange: React.ChangeEventHandler<HTMLInputElement>
}

export const NCurrencyInput = React.forwardRef<
  HTMLInputElement | null,
  NCurrencyInputProps & Omit<React.InputHTMLAttributes<HTMLInputElement>, 'value' | 'onChange'>
>(
  (
    {
      className,
      left,
      right,
      error,
      label,
      caption,
      id,
      required,
      disabled,
      currencyCode,
      onFocus,
      onBlur,
      onKeyPress,
      value,
      ...inputProps
    }: NCurrencyInputProps & Omit<React.InputHTMLAttributes<HTMLInputElement>, 'value' | 'onChange'>,
    ref,
  ) => {
    const { currencyLocale } = useSession()
    const [isFocused, setIsFocused] = useState(false)

    const inputId = useMemo(() => id || getCurrnecyInputId(), [])

    const classname = classnames([className || false, (disabled || false) && 'is-disabled', !!error && 'is-error'])

    const { formatter, canEnterDecimal } = useMemo(() => {
      const tempFormatter = Intl.NumberFormat(currencyLocale, {
        style: 'currency',
        currency: currencyCode,
      })

      const tempFormatSchema = tempFormatter.formatToParts(100000.01)

      const tempCanEnterDecimal = !!tempFormatSchema.find(part => {
        return part.type === 'decimal'
      })

      return {
        formatter: tempFormatter,
        formatSchema: tempFormatSchema,
        canEnterDecimal: tempCanEnterDecimal,
      }
    }, [currencyCode, currencyLocale])

    return (
      <Wrapper className={classname} data-testid="NCurrencyInput-Wrapper">
        {label && (
          <Label htmlFor={inputId} data-testid="NCurrencyInput-Label">
            {required && <RequiredIndicator>*</RequiredIndicator>}
            {label}
          </Label>
        )}
        <InputWrapper data-testid="NCurrencyInput-InputWrapper">
          {left}
          <InputContainer>
            <Input
              ref={ref}
              data-testid="NCurrencyInput-Input"
              id={inputId}
              type="number"
              disabled={disabled}
              value={value}
              onFocus={e => {
                setIsFocused(true)
                onFocus && onFocus(e)
              }}
              onBlur={e => {
                setIsFocused(false)
                onBlur && onBlur(e)
              }}
              onKeyPress={e => {
                if (e.key === 'e') {
                  e.preventDefault()
                  return
                }
                if (!canEnterDecimal && e.key === '.') {
                  e.preventDefault()
                  return
                }
                onKeyPress && onKeyPress(e)
              }}
              {...inputProps}
            />
            {!isFocused && <MaskedText>{formatter.format(+value)}</MaskedText>}
          </InputContainer>
          {right}
        </InputWrapper>
        {(error || caption) && <Caption data-testid="NCurrencyInput-Caption">{error || caption}</Caption>}
      </Wrapper>
    )
  },
)
