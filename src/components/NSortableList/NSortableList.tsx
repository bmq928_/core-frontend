import * as React from 'react'
import { useDrag, useDrop } from 'react-dnd'
import { classnames } from '../../common/classnames'

const SORTABLE_ITEM = 'SORTABLE_ITEM'

type NSortableListProps = {
  children: JSX.Element | JSX.Element[]
  onSort(fromIndex: number, toIndex: number): void
  itemType?: string
  disabled?: boolean
} & React.HTMLAttributes<HTMLDivElement>

export function NSortableList({
  children,
  onSort,
  itemType = SORTABLE_ITEM,
  disabled = false,
  ...restProps
}: NSortableListProps) {
  const [, drop] = useDrop({ accept: itemType })
  return (
    <div ref={drop} {...restProps}>
      {React.Children.map(children, (child: JSX.Element, childIndex) => (
        <SortableItem disabled={disabled} index={childIndex} onSort={onSort} itemType={itemType}>
          {child}
        </SortableItem>
      ))}
    </div>
  )
}

type SortableItemProps = { index: number; itemType: string; children: JSX.Element; disabled: boolean } & Pick<
  NSortableListProps,
  'onSort'
>

function SortableItem({ index, children, onSort, itemType, disabled }: SortableItemProps) {
  const [, drop] = useDrop({
    accept: itemType,
    canDrop() {
      return false
    },
    hover(item: { index: number }) {
      if (item.index !== index) {
        onSort(item.index, index)
        item.index = index
      }
    },
  })
  const [{ isDragging }, drag] = useDrag({
    type: itemType,
    item: { index, id: children },
    canDrag: !disabled,
    collect(monitor) {
      return {
        isDragging: monitor.isDragging(),
      }
    },
  })

  const child = React.cloneElement(children, {
    ref(node: HTMLElement) {
      drag(drop(node))
    },
    ...children.props,
    style: children.props.style,
    className: classnames([children.props.className, isDragging && 'is-dragging']),
  })

  return <>{child}</>
}

export function moveItem<T>(set: React.Dispatch<React.SetStateAction<T[]>>) {
  return function (fromIndex: number, toIndex: number) {
    set(previous => {
      const next = Array.from(previous) as T[]
      ;[next[toIndex], next[fromIndex]] = [next[fromIndex], next[toIndex]]
      return next
    })
  }
}
