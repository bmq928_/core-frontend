import ColorHash from 'color-hash'
import * as React from 'react'
import styled from 'styled-components'
import { color } from '../GlobalStyle'

export type IconVariant = 'rounded' | 'circle' | 'default'

const Wrapper = styled.div<{ size: number; sizeUnit: string; rounded: boolean; variant: IconVariant; color: string }>`
  width: ${props => `${props.size}${props.sizeUnit}`};
  height: ${props => `${props.size}${props.sizeUnit}`};
  display: flex;
  justify-content: center;
  align-items: center;
  background: ${props => props.color};
  border-radius: ${props => (props.variant === 'circle' ? '50%' : props.variant === 'rounded' ? '4px' : '0')};
`
const Text = styled.p<{ size: number; sizeUnit: string }>`
  color: ${color('white')};
  font-size: ${props => `${props.size * 0.4}${props.sizeUnit}`};
  line-height: ${props => `${props.size * 0.4}${props.sizeUnit}`};
  text-transform: uppercase;
`

export type NAvatarProps = {
  name: string
  size: number
  sizeUnit?: string
  rounded?: boolean
  variant?: IconVariant
  className?: string
} & React.HTMLAttributes<HTMLDivElement>

//TODO: support url as well
export const NAvatar = React.memo(
  React.forwardRef<HTMLDivElement, NAvatarProps>(function Node(
    { size, sizeUnit = 'px', name, rounded = false, variant = 'default', ...divProps },
    ref,
  ) {
    return (
      <Wrapper
        ref={ref}
        size={size}
        sizeUnit={sizeUnit}
        rounded={rounded}
        variant={variant}
        color={new ColorHash().hex(name)}
        {...divProps}>
        <Text size={size} sizeUnit={sizeUnit}>
          {name.length > 0 ? name[0] : 'N'}
        </Text>
      </Wrapper>
    )
  }),
)

NAvatar.displayName = 'NAvatar'
