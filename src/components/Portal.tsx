import { useEffect, useRef } from 'react'
import ReactDOM from 'react-dom'

function getOwnerDocument<T extends HTMLElement = HTMLElement>(element: T) {
  return element.ownerDocument || document
}

type Props<T> = {
  mountNode?: React.RefObject<T | null>
  children: React.ReactNode
}

export function Portal<T extends HTMLElement | SVGElement>({ children, mountNode }: Props<T>) {
  const element = useRef<HTMLDivElement>(document.createElement('div'))

  useEffect(() => {
    const childNode = element.current
    const ownerDocument = getOwnerDocument(childNode)
    ownerDocument.body.appendChild(childNode)
    return () => {
      ownerDocument.body.removeChild(childNode)
    }
  }, [mountNode])

  if (mountNode?.current) {
    return ReactDOM.createPortal(children, mountNode.current)
  }

  return ReactDOM.createPortal(children, element.current)
}
