import React, { CSSProperties, FC } from 'react'
import styled, { css } from 'styled-components'
import { color, spacing } from '../GlobalStyle'
import { typography } from '../NTypography'

type BaseSideBarProps = {
  mode: 'light' | 'dark'
  collapsed?: boolean
}
const BaseSideBar = styled.div<BaseSideBarProps>`
  display: flex;
  flex-direction: column;
  align-items: ${props => (props.collapsed ? 'center' : 'flex-start')};

  margin: ${props => (props.mode === 'light' ? 0 : spacing('xs'))} 0;
  border-radius: 8px;

  ${props => {
    if (props.mode === 'light') {
      return css`
        background-color: ${props.theme.color.white};
      `
    }
    return css`
      background-image: linear-gradient(180deg, #f5f6f7 0%, rgba(245, 246, 247, 0) 100%);
    `
  }}

  width: ${props => (props.collapsed ? '56px' : '255px')};
  min-height: 100vh;

  padding: ${spacing('xl')} ${spacing('xs')};

  transition: width, background-color, border-right 0.25s;
`

const Title = styled.h1<{ mode: string }>`
  padding-left: ${spacing('sm')};
  padding-right: ${spacing('sm')};
  margin-bottom: ${spacing('md')};

  color: ${props => (props.mode === 'light' ? color('black') : color('Neutral900'))};
  ${typography('h600')}
  text-transform: none;
`

export type NSideBarProps = {
  style?: CSSProperties
  mode?: 'light' | 'dark'
  collapsed?: boolean
  title?: string
  className?: string
}

export const NSideBar: FC<NSideBarProps> = ({ style, mode = 'dark', collapsed, title, children, className }) => {
  return (
    <BaseSideBar className={className} mode={mode} collapsed={collapsed} style={style}>
      {!collapsed && title && <Title mode={mode}>{title}</Title>}
      {children}
    </BaseSideBar>
  )
}
