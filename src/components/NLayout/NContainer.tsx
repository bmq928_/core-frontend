import React, { CSSProperties, FC } from 'react'
import styled from 'styled-components'
import { color, spacing } from '../GlobalStyle'
import { typography } from '../NTypography'

const Title = styled.h1`
  margin-bottom: ${spacing('xxl')};
  ${typography('h800')};
`

const Description = styled.p`
  margin-bottom: ${spacing('xxl')};
  color: ${color('Neutral500')};
  ${typography('ui-text')};
`

const Content = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
`

////////////////////////////
type NContainerBaseProps = {
  isRow?: boolean
}
const NContainerBase = styled.div<NContainerBaseProps>`
  display: flex;
  flex-direction: ${props => (props.isRow ? 'row' : 'column')};
  flex: 1;
  overflow: auto;

  padding: ${spacing('xxl')};
`

export type NContainerProps = {
  isRow?: boolean
  className?: string
  style?: CSSProperties
}

export const NContainer: FC<NContainerProps> & {
  Title: FC
  Description: FC
  Content: FC
} = ({ isRow, className, style, children }) => {
  return (
    <NContainerBase className={className} style={style} isRow={isRow}>
      {children}
    </NContainerBase>
  )
}

NContainer.Title = Title
NContainer.Description = Description
NContainer.Content = Content
