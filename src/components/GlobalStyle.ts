import { createGlobalStyle } from 'styled-components'

export const theme = {
  color: {
    // TODO: Should follow best practice
    // Ref: https://refactoringui.com/previews/building-your-color-palette/
    white: '#FFFFFF',
    transparent: 'transparent',
    black: 'black',

    Neutral100: '#F9F9FA',
    Neutral200: '#EDEEF0',
    Neutral300: '#CED3D8',
    Neutral400: '#A3ABB5',
    Neutral500: '#828B94',
    Neutral700: '#525C66',
    Neutral900: '#0D1826',

    Primary100: '#F0F9F8',
    Primary200: '#E0F1F0',
    Primary300: '#B2DCD9',
    Primary400: '#84C8C3',
    Primary700: '#66BBB5',
    Primary900: '#35938B',

    Secondary900: '#408DA2',

    Tertiary900: '#233655',

    warning: '#FF9202',
    danger: '#E8442F',

    Red700: '#EC6140',
  },
  boxShadow: {},
  borderRadius: {},
  fontSize: {
    xxs: '9px',
    xs: '12px',
    sm: '14px',
    md: '16px',
    lg: '18px',
    xl: '24px',
    xxl: '32px',
  },
  spacing: {
    xxs: '4px',
    xs: '8px',
    sm: '12px',
    md: '16px',
    lg: '20px',
    xl: '24px',
    xxl: '32px',
  },
  breakpoints: {
    sm: '576px',
    md: '768px',
    lg: '992px',
    xl: '1200px',
  },
  fontWeight: {
    ExtraBold: '800',
    Bold: 'bold',
    SemiBold: '600',
    Medium: '500',
    Regular: 'normal',
    Light: '300',
  },
}
export type ThemeSpaceType = keyof typeof theme.spacing

/**
 * xxs: 4px; xs: 8px; sm: 12px; md: 16px
 * lg: 20px; xl: 24px; xxl: 32px;
 */
export function spacing(size: keyof typeof theme.spacing | number) {
  return function (props: { theme: Theme }) {
    if (typeof size === 'number') {
      return `${size}px`
    }
    return props.theme.spacing[size] || theme.spacing.md
  }
}

export type ThemeColorType = keyof typeof theme.color
export function color(type: ThemeColorType) {
  return function (props: { theme: Theme }) {
    return props.theme.color[type] || theme.color.Primary700
  }
}
export function fontWeight(fontWeight: keyof typeof theme.fontWeight) {
  return function (props: { theme: Theme }) {
    return props.theme.fontWeight[fontWeight] || theme.fontWeight.Regular
  }
}
/**
 * xxs: 9px; xs: 12px; sm: 14px; md: 16px; lg: 18px; xl: 24px; xxl: 32px
 */
export function fontSize(fontSize: keyof typeof theme.fontSize) {
  return function (props: { theme: Theme }) {
    return props.theme.fontSize[fontSize] || theme.fontSize.sm
  }
}

export function getAlpha(alpha: number) {
  const value = Math.max(0, Math.min(alpha, 100))
  return Math.round((value / 100) * 255)
    .toString(16)
    .padStart(2, '0')
}

export type Theme = typeof theme

export const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }
  html,
  body {
    font-size: 14px;
    font-family: "Neurial Grotesk", sans-serif;
    color: ${color('Neutral700')};
  }
  html,
  body,
  #root {
    min-height: 100vh;
  }
  button {
    cursor: pointer;
  }
  input,button,textarea {
    font-family: inherit;
    &:focus {
      outline: 0;
    }
  }
  img {
    display: block;
  }
  a {
    text-decoration: none;
    cursor: pointer;
  }
  ul,
  ol {
    list-style: none;
  }
  h1,
  .h1,
  h2,
  .h2,
  h3,
  .h3,
  h4,
  .h4,
  h5,
  .h5,
  h6,
  .h6 {
    font-family: "Neurial Gotesk", sans-serif;
    font-weight: bold;
  }
  h1,
  .h1 {
    font-size: 32px;
  }

  h2,
  .h2 {
    font-size: 24px;
  }

  h3,
  .h3 {
    font-size: 16px;
  }

  h4,
  .h4 {
    font-size: 14px;
  }

  h5,
  .h5 {
    font-size: 12px;
  }

  h6,
  .h6 {
    font-size: 12px;
  }

  p {
    line-height: 14px;
  }

`
