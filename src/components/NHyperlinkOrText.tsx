import { FC, HTMLAttributes } from 'react'

type Props = HTMLAttributes<HTMLAnchorElement>

export const NHyperlinkOrText: FC<Props> = ({ children, ...restProps }) => {
  const hyperlinkRegex = new RegExp('^https?://', 'g')
  if (typeof children === 'string' && hyperlinkRegex.test(children)) {
    return (
      <a href={children} {...restProps} target="_blank" rel="noopener noreferrer">
        {children}
      </a>
    )
  }

  return <>{children}</>
}
