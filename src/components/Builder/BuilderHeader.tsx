import * as React from 'react'
import { useHistory } from 'react-router-dom'
import styled from 'styled-components'
import { color, spacing } from '../GlobalStyle'
import { GlobalIcons } from '../Icons'
import { typography } from '../NTypography'

const Wrapper = styled('div')`
  display: flex;
  align-items: center;
  background-color: ${color('white')};
  margin-bottom: 2px;
`

const LeftElement = styled('div')`
  flex: 1;
  display: flex;
  align-items: center;

  .back-icon {
    width: 40px;
    height: 40px;

    display: flex;
    align-items: center;
    justify-content: center;
  }
`

const IconWrapper = styled('div')`
  width: 40px;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  &:first-child {
    cursor: pointer;
    border-right: 1px solid ${color('Neutral200')};
  }
`

const Title = styled('div')`
  ${typography('button')}
`

const RightElement = styled('div')`
  flex: 1;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  margin-right: ${spacing('sm')};
`

const CenterElement = styled('div')`
  height: 100%;
  display: flex;
  align-items: center;
`

type BuilderHeaderProps = {
  children?: React.ReactNode
  icon?: React.ReactNode
  title: string
  rightElement?: React.ReactNode
} & React.HTMLAttributes<HTMLDivElement>

export function BuilderHeader({ icon, title, rightElement, children, ...restProps }: BuilderHeaderProps) {
  const history = useHistory()
  return (
    <Wrapper {...restProps}>
      <LeftElement>
        <IconWrapper>
          <GlobalIcons.LeftLarge onClick={() => history.goBack()} />
        </IconWrapper>
        <IconWrapper>{icon}</IconWrapper>
        <Title>{title}</Title>
      </LeftElement>
      <CenterElement>{children}</CenterElement>
      <RightElement>{rightElement}</RightElement>
    </Wrapper>
  )
}
