import { color, spacing, theme } from './GlobalStyle'

describe('GlobalStyle utils', () => {
  test('spacing should return predefined value based on key', () => {
    const xs = '8px'
    const sm = '12px'
    const md = '16px'
    const lg = '20px'
    const xl = '24px'

    expect(spacing('xs')({ theme })).toBe(xs)
    expect(spacing('sm')({ theme })).toBe(sm)
    expect(spacing('md')({ theme })).toBe(md)
    expect(spacing('lg')({ theme })).toBe(lg)
    expect(spacing('xl')({ theme })).toBe(xl)
  })

  test('spacing should return custom value as a string', () => {
    const value = spacing(8)({ theme })
    expect(value).toBe('8px')
  })

  test('spacing should return default md value', () => {
    // @ts-ignore
    const value = spacing()({ theme })
    expect(value).toBe(theme.spacing.md)
  })

  test('color should return predefined value based on key', () => {
    Object.keys(theme.color).map(key => {
      const k = key as keyof typeof theme.color
      const v = color(k)({ theme })
      expect(v).toBe(theme.color[k])
    })
  })

  test('color should return default primary value', () => {
    // @ts-ignore
    const value = color()({ theme })
    expect(value).toBe(theme.color.Primary700)
  })
})
