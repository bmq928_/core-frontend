import { addMonths, format, isFuture, isPast, isSameDay, isSameMonth, isToday } from 'date-fns'
import * as React from 'react'
import styled from 'styled-components'
import { classnames } from '../../common/classnames'
import { generateCalendar } from '../DateTime/utils'
import { color, spacing } from '../GlobalStyle'
import { Icons } from '../Icons'
import { typography } from '../NTypography'

const Wrapper = styled('div')`
  max-width: 312px;
  background: ${color('white')};
  border-radius: 3px;
`

const ListWrapper = styled('div')`
  padding: 0 ${spacing('md')};
  display: grid;
  grid-template: 1fr / repeat(7, 40px);
`

const Item = styled('div')`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: ${spacing('xs')} 0;
  color: ${color('Neutral700')};
`

const DateItem = styled(Item)`
  cursor: auto;
  font-weight: bold;
  font-size: 12px;
  text-transform: uppercase;
`

const DayItem = styled(Item)`
  font-size: 14px;
  position: relative;
  border-radius: 3px;
  border: none;
  background: ${color('transparent')};
  &.is-today {
    color: ${color('Primary900')};
  }
  &.is-today:after {
    position: absolute;
    content: '';
    left: 4px;
    right: 4px;
    bottom: 4px;
    height: 2px;
    background-color: ${color('Primary900')};
  }
  &:not(.is-disabled):not(.is-selected).is-current-month:hover {
    cursor: pointer;
    background: ${color('Neutral200')};
  }
  &.is-disabled,
  &.is-previous-month,
  &.is-next-month {
    color: ${color('Neutral200')};
  }
  &.is-selected {
    background-color: ${color('Neutral700')};
    color: ${color('white')};
  }
`

const CalendarFooter = styled('button')`
  border: none;
  border-top: 1px solid ${color('Neutral200')};
  padding: ${spacing('xs')} ${spacing('md')};
  display: flex;
  justify-content: center;
  align-items: center;
  background: ${color('transparent')};
  color: ${color('Primary700')};
  width: 100%;
  &:hover {
    background: ${color('Neutral200')};
  }
`

export type CalendarProps = {
  date?: Date
  renderCurrentMonthOnly?: boolean
  canSelectPastDay?: boolean
  className?: string
  weekStarsOn?: 'Sunday' | 'Monday'
  value?: Date
  onChangeValue?(date: Date): void
}

export function Calendar({
  className,
  value = new Date(),
  onChangeValue,
  renderCurrentMonthOnly,
  canSelectPastDay,
}: CalendarProps) {
  const [fromDate, setFromDate] = React.useState(value)
  const daysOfMonth = generateCalendar(fromDate)
  return (
    <Wrapper data-testid="CalendarContainer" className={className}>
      <CalendarHeader
        onPreviousClick={() => setFromDate(prev => addMonths(prev, -1))}
        onNextClick={() => setFromDate(prev => addMonths(prev, 1))}>
        <div className="h400">{format(fromDate, 'MMMM yyyy')}</div>
      </CalendarHeader>
      <ListWrapper>
        <DateList data={daysOfMonth.slice(0, 7)} />
        <DayList
          data={daysOfMonth}
          date={fromDate}
          selected={value}
          renderCurrentMonthOnly={renderCurrentMonthOnly}
          canSelectPastDay={canSelectPastDay}
          onDayClick={onChangeValue}
        />
      </ListWrapper>
      <CalendarFooter onClick={() => setFromDate(new Date())}>Today</CalendarFooter>
    </Wrapper>
  )
}

type DateListProps = {
  data: Date[]
}

function DateList({ data }: DateListProps) {
  return (
    <>
      {data.map(i => (
        <DateItem key={`date-${i.toISOString()}`}>{format(i, 'EEE')}</DateItem>
      ))}
    </>
  )
}

type DayListProps = {
  data: Date[]
  date: Date
  selected?: Date
  renderCurrentMonthOnly?: boolean
  canSelectPastDay?: boolean
  onDayClick?(date: Date): void
}

function DayList({
  data,
  date,
  selected,
  canSelectPastDay = true,
  renderCurrentMonthOnly = false,
  onDayClick,
}: DayListProps) {
  return (
    <>
      {data.map(i => {
        const isCurrentMonth = isSameMonth(i, date)
        const isPastDay = isPast(i)
        const isCurrentDay = isToday(i)
        const isDisabled = isPastDay && !isCurrentDay && !canSelectPastDay
        const className = classnames([
          isCurrentDay && 'is-today',
          isPastDay && 'is-past',
          isFuture(i) && 'is-future',
          isCurrentMonth && 'is-current-month',
          isPast(i) && !isCurrentDay && !isCurrentMonth && 'is-previous-month',
          isFuture(i) && !isCurrentMonth && 'is-next-month',
          isDisabled && 'is-disabled',
          !!selected && isSameDay(i, selected) && 'is-selected',
        ])
        const handleDayClick = () => {
          if (isPastDay && !canSelectPastDay) {
            return
          }
          if (onDayClick) {
            onDayClick(i)
          }
        }
        return (
          <DayItem
            as={(!isDisabled && isCurrentMonth && 'button') || undefined}
            className={className}
            key={`day-${i.toISOString()}`}
            onClick={handleDayClick}>
            {((isCurrentMonth && renderCurrentMonthOnly) || !renderCurrentMonthOnly) && i.getDate()}
          </DayItem>
        )
      })}
    </>
  )
}

const HeaderWrapper = styled('div')`
  position: relative;
  display: flex;
  padding: ${spacing('md')} ${spacing('xl')} ${spacing('xs')};
  button {
    width: ${spacing('xl')};
    height: ${spacing('xl')};
    display: flex;
    justify-content: center;
    align-items: center;
    background: ${color('transparent')};
    color: ${color('Neutral400')};
    position: absolute;
    border: none;
    border-radius: 3px;
    bottom: 4px;
    &.button-previous {
      left: ${spacing('md')};
    }
    &.button-next {
      right: ${spacing('md')};
    }
    &:hover {
      background: ${color('Neutral200')};
      color: ${color('Neutral700')};
    }
  }
  & > *:not(button) {
    flex: 1;
    display: flex;
    justify-content: center;
    align-items: center;
    text-transform: uppercase;
    ${typography('h400')};
  }
`

type CalendarHeaderProps = {
  onPreviousClick(): void
  onNextClick(): void
  children?: React.ReactNode
}

function CalendarHeader({ onPreviousClick, onNextClick, children }: CalendarHeaderProps) {
  return (
    <HeaderWrapper>
      <button className="button-previous" onClick={onPreviousClick}>
        <Icons.Left />
      </button>
      <button className="button-next" onClick={onNextClick}>
        <Icons.Right />
      </button>
      {children}
    </HeaderWrapper>
  )
}
