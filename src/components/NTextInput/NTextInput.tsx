import * as React from 'react'
import styled from 'styled-components'
import { classnames } from '../../common/classnames'
import { color, spacing } from '../GlobalStyle'
import { NTypography, RequiredIndicator } from '../NTypography'

export const Label = styled(NTypography.InputLabel)`
  margin-bottom: ${spacing('xs')};
`

export const InputWrapper = styled('div')`
  display: flex;
  align-items: center;
  padding: 0 ${spacing('xs')};
  transition: border-color 0.3s ease-in-out;
  border-radius: 4px;
  border: 1px solid ${color('Neutral200')};
  &:hover {
    border-color: ${color('Neutral400')};
  }
  &:focus-within {
    border-color: ${color('Primary700')};
  }
`

const Input = styled('input')`
  border: none;
  font-size: 14px;
  line-height: 16px;
  padding: 11px 8px;
  flex: 1;
  width: 100%;
  color: ${color('Neutral900')};

  &:focus {
    outline: 0;
  }

  ::placeholder {
    font-size: inherit;
    color: ${color('Neutral500')};
  }
`

export const Caption = styled(NTypography.InputCaption)`
  margin-top: ${spacing('xxs')};
`

export const Wrapper = styled('div')`
  display: flex;
  flex-direction: column;
  &.is-error {
    ${InputWrapper} {
      border-color: ${color('Red700')};
    }
    ${Caption} {
      color: ${color('Red700')};
    }
  }
  &.is-disabled {
    ${InputWrapper} {
      cursor: not-allowed;
      background: ${color('Neutral200')};
      &:hover {
        border-color: ${color('Neutral200')};
      }
    }
    ${Input} {
      color: ${color('Neutral500')};
    }
  }
`

export type NTextInputProps = {
  className?: string
  label?: React.ReactNode
  caption?: string
  left?: React.ReactNode
  right?: React.ReactNode
  error?: string
}

// TODO: This is not support SSG
let textInputId = 1
const getTextInputId = () => {
  return `text-input-${textInputId++}`
}

export const NTextInput = React.forwardRef<
  HTMLInputElement | null,
  NTextInputProps & React.InputHTMLAttributes<HTMLInputElement>
>(
  (
    {
      className,
      left,
      right,
      error,
      label,
      caption,
      type = 'text',
      id,
      required,
      ...inputProps
    }: NTextInputProps & React.InputHTMLAttributes<HTMLInputElement>,
    ref,
  ) => {
    const classname = classnames([
      className || false,
      (inputProps?.disabled || false) && 'is-disabled',
      !!error && 'is-error',
    ])
    const inputId = React.useMemo(() => id || getTextInputId(), [id])
    return (
      <Wrapper className={classname} data-testid="NTextInput-Wrapper">
        {label && (
          <Label htmlFor={inputId} data-testid="NTextInput-Label">
            {required && <RequiredIndicator>*</RequiredIndicator>}
            {label}
          </Label>
        )}
        <InputWrapper data-testid="NTextInput-InputWrapper">
          {left}
          <Input
            data-testid="NTextInput-Input"
            ref={ref}
            id={inputId}
            required={required}
            type={type}
            {...inputProps}
          />
          {right}
        </InputWrapper>
        {(error || caption) && <Caption data-testid="NTextInput-Caption">{error || caption}</Caption>}
      </Wrapper>
    )
  },
)
