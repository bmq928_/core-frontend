import rehypePrism from '@mapbox/rehype-prism'
import rehype from 'rehype'

export function processHTML(html: string) {
  return rehype()
    .data('settings', { fragment: true })
    .use(rehypePrism, { ignoreMissing: true })
    .processSync(html)
    .toString()
}

export function htmlEncode(sHtml: string) {
  return sHtml.replace(
    /[<>&"]/g,
    (c: string) => (({ '<': '&lt;', '>': '&gt;', '&': '&amp;', '"': '&quot;' } as Record<string, string>)[c]),
  )
}

const STYLE_PROPERTIES = [
  'fontFamily',
  'fontSize',
  'fontWeight',
  'wordWrap',
  'whiteSpace',
  'borderLeftWidth',
  'borderTopWidth',
  'borderRightWidth',
  'borderBottomWidth',
  'paddingTop',
  'paddingBottom',
  'paddingRight',
  'paddingLeft',
  'marginTop',
  'marginLeft',
  'marginRight',
  'marginBottom',
  'lineHeight',
]

export type CaretXY = {
  x: number
  y: number
  height: string
}

export function getCaretXY(element: HTMLTextAreaElement | HTMLInputElement): CaretXY {
  var start = element.selectionStart || 0
  var end = element.selectionEnd || 0
  var copy = document.createElement('div')
  copy.textContent = element.value
  var style = getComputedStyle(element)
  // Copy style to fake element
  STYLE_PROPERTIES.forEach(function (key) {
    // @ts-ignore
    copy.style[key] = style[key]
  })
  copy.style.overflow = 'auto'
  copy.style.width = element.offsetWidth + 'px'
  copy.style.height = element.offsetHeight + 'px'
  copy.style.position = 'absolute'
  copy.style.left = element.offsetLeft + 'px'
  copy.style.top = element.offsetTop + 'px'
  // we want to render the fake element
  copy.style.visibility = 'visible'
  copy.style.opacity = '0'
  copy.style.userSelect = 'none'
  document.body.appendChild(copy)
  var range = document.createRange()
  if (copy.firstChild) {
    range.setStart(copy.firstChild, start)
    range.setEnd(copy.firstChild, end)
  }
  var rect = range.getBoundingClientRect()
  document.body.removeChild(copy)
  const elementRect = element.getBoundingClientRect()
  return {
    x: rect.left - element.scrollLeft + elementRect.left,
    y: rect.top - element.scrollTop + elementRect.top,
    height: copy.style.lineHeight !== 'normal' ? copy.style.lineHeight : copy.style.fontSize,
  }
}

export function createTrigger(trigger: string) {
  /**
   * Search text pattern
   * Format: {$object.nested.key}
   *
   * Matched with cases:
   * incompleted suggestion in middle of sentence
   * input: "{$var1 sometext here {$var2} {$var3}"
   * output: "{$var2 " with 3 groups ["{$var1 ", "var1", " "]
   *
   * incompleted suggestion in middle of sentence
   * input: "Hello world {$var1} {$var2 {$var3}"
   * output: "{$var2 " with 3 groups ["{$var2 ", "var2", " "]
   *
   * incompleted suggestion in the end of sentence
   * input: "Hello world {$var1} {$var2} {$var3"
   * output: "{$var3" with 3 groups ["{$var3", "var3", ""]
   *
   * group[0] should be used for search and replace selected variable
   * group[1] should be used for filter list variables
   * group[2] should be used for concat the last whitespace or not
   */
  const escapedTrigger = escapeRegExp(trigger)
  return new RegExp(`${escapedTrigger}([$a-zA-Z0-9_.]*)(\\s|$)`, 'gm')
}

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions
export function escapeRegExp(str: string) {
  return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&')
}

export const EXIT_CODES = ['Escape']
export const SELECT_CODES = ['Tab', 'Enter']
export const ARROW_CODES = ['ArrowDown', 'ArrowUp']
