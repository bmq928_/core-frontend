import { FC } from 'react'
import { NavLink, NavLinkProps, useLocation } from 'react-router-dom'
import styled from 'styled-components'
import { classnames } from '../../common/classnames'
import { spacing } from '../GlobalStyle'
import { typography } from '../NTypography'

export const NMenuItemClassName = 'NMenuItem'

const Container = styled(NavLink)<{ selected: boolean }>`
  height: 40px;

  display: flex;
  flex-direction: row;
  align-items: center;
  padding-left: ${spacing('md')};
  padding-right: ${spacing('md')};

  border-radius: 6px;
  cursor: pointer;
  transition: background-color 0.25s;

  ${typography('button')}
`

export type NMenuItemProps = {
  to: string

  className?: string
  children?: string
} & NavLinkProps

export const NMenuItem: FC<NMenuItemProps> = ({ to, className = false, children }) => {
  const { pathname } = useLocation()

  const classNames = classnames([NMenuItemClassName, className])

  return (
    <Container data-testid="MenuItem" className={classNames} to={to} selected={to === pathname}>
      {children}
    </Container>
  )
}
