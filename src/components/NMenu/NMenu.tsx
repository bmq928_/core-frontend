import { FC } from 'react'
import styled from 'styled-components'
import { classnames } from '../../common/classnames'
import { color } from '../GlobalStyle'
import { NMenuItem, NMenuItemClassName, NMenuItemProps } from './NMenuItem'

export type NMenuType = 'primary' | 'default'

const Container = styled.div`
  width: 100%;
  overflow: auto;

  &.default {
    .${NMenuItemClassName} {
      color: ${color('Neutral900')};
      background-color: ${color('white')};
      :hover {
        background-color: ${color('Neutral200')};
      }
      &.active {
        background-color: ${color('Primary900')};
      }
    }
  }

  &.primary {
    .${NMenuItemClassName} {
      color: ${color('Neutral700')};
      background-color: ${color('transparent')};
      :hover {
        background-color: ${color('Neutral200')};
      }
      &.active {
        background-color: ${color('Neutral200')};
      }
    }
  }
`

export type NMenuProps = {
  className?: string
  type?: NMenuType
}

export const NMenu: FC<NMenuProps> & {
  Item: FC<NMenuItemProps>
} = ({ type = 'default', className = false, children }) => {
  const classNames = classnames([type, className])
  return <Container className={classNames}>{children}</Container>
}

NMenu.Item = NMenuItem
