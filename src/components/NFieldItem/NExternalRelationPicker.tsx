import React, { FC, useState } from 'react'
import { Controller, UseFormReturn } from 'react-hook-form'
import { useQuery } from 'react-query'
import { useSearchText } from '../../hooks/useSearchText'
import { APIServices } from '../../services/api'
import { DataQueryKey } from '../../services/api/endpoints/data'
import { DataCollection, DataResponse, FieldSchema } from '../../services/api/models'
import { SelectOption } from '../NSelect/model'
import { NSingleSelect } from '../NSelect/NSingleSelect'
import { NToast } from '../NToast'

type NExternalRelationPickerProps = {
  field: FieldSchema
  contents: Record<string, any>
  formMethods: UseFormReturn
  error?: string
}

type ReturnDataType = {
  dataOptions: SelectOption[]
  dataByGuid: Record<string, DataResponse>
  rawData: DataCollection
}

export const NExternalRelationPicker: FC<NExternalRelationPickerProps> = ({ field, contents, formMethods, error }) => {
  const { control } = formMethods
  const { displayName, name, value, isRequired } = field

  const [{ searchText, searchValue }, onChangeSearch] = useSearchText()

  // get content list
  const { data, isLoading: isLoadingData } = useQuery(
    [
      DataQueryKey.getObjectData,
      {
        name: value as string,
        limit: 5,
        offset: 0,
        searchText,
      },
    ],
    APIServices.Data.getObjectData,
    {
      enabled: !!value,
      select: data => {
        return data.data.data.reduce(
          (prev, cur) => {
            return {
              //@ts-ignore
              dataOptions: [...prev.dataOptions, { value: cur.externalId, label: cur['name'] || cur.externalId }],
              //@ts-ignore
              dataByGuid: { ...prev.dataByGuid, [cur.externalId]: cur },
              rawData: prev.rawData,
            }
          },
          {
            dataOptions: [],
            dataByGuid: {},
            rawData: data.data,
          } as ReturnDataType,
        )
      },
      onError: error => {
        NToast.error({ title: 'Get object data error!', subtitle: error.response?.data.message })
      },
    },
  )

  const [selectedOption, setSelectedOptions] = useState<SelectOption | undefined>(
    contents['$metadata'] && contents['$metadata'].relations && contents['$metadata'].relations[name]
      ? {
          label: contents[name],
          value: contents['$metadata'].relations[name],
        }
      : undefined,
  )

  const relations = contents['$metadata'] && (contents['$metadata'].relations as Record<string, string>)
  const defaultValue = relations && relations[name]

  return (
    <Controller
      control={control}
      name={name}
      rules={{ required: { value: isRequired, message: 'Required' } }}
      defaultValue={defaultValue}
      render={({ field }) => {
        const options = data?.dataOptions || []
        let extOptions = options
        if (selectedOption) {
          const findSelected = options.find(op => op.value === selectedOption.value)
          extOptions = !findSelected ? [selectedOption, ...options] : options
        }
        return (
          <NSingleSelect
            fullWidth
            isSearchable
            searchValue={searchValue || ''}
            onSearchValueChange={onChangeSearch}
            isLoading={isLoadingData}
            options={extOptions}
            value={field.value || ''}
            required={isRequired}
            onValueChange={newVal => {
              newVal &&
                setSelectedOptions({
                  //@ts-ignore
                  label: data?.dataByGuid[newVal].name,
                  value: newVal,
                })
              field.onChange(newVal)
            }}
            label={displayName}
            error={error}
          />
        )
      }}
    />
  )
}
