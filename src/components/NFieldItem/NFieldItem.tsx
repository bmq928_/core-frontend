import React, { FC } from 'react'
import { Controller, useFormContext } from 'react-hook-form'
import { NavLink } from 'react-router-dom'
import styled from 'styled-components'
import { useValidateString } from '../../hooks/useValidateString'
import { useSession } from '../../redux/slices/sessionSlice'
import { DataResponseMetadata, FieldSchema } from '../../services/api/models'
import { getDateFromIsoString, getStringFromDate, getValueFromDate } from '../../utils/utils'
import { NDatePicker } from '../DateTime/NDatePicker'
import { NDateTimePicker } from '../DateTime/NDateTimePicker'
import { NTimePicker } from '../DateTime/NTimePicker'
import { color, spacing } from '../GlobalStyle'
import { NCheckbox } from '../NCheckbox/NCheckbox'
import { CurrencyData } from '../NCurrency/CurrencyData'
import { NCurrencyInput } from '../NCurrency/NCurrencyInput'
import { NHyperlinkOrText } from '../NHyperlinkOrText'
import { SelectOption } from '../NSelect/model'
import { NMultiSelect } from '../NSelect/NMultiSelect'
import { NSingleSelect } from '../NSelect/NSingleSelect'
import { NSpinner } from '../NSpinner/NSpinner'
import { NTextArea } from '../NTextArea/NTextArea'
import { NTextInput } from '../NTextInput/NTextInput'
import { NTypography, typography } from '../NTypography'
import { NExternalRelationPicker } from './NExternalRelationPicker'
import { NRelationPicker } from './NRelationPicker'

export enum FieldItemMode {
  View = 'view',
  Form = 'form',
}

const ViewContainer = styled.div`
  display: flex;
  flex-direction: column;
`
const LabelText = styled.div`
  ${typography('label')};
  margin-bottom: ${spacing('xs')};
`
const ValueText = styled.div`
  display: flex;
  overflow: auto;

  ${typography('small-ui-text')};
  padding-bottom: ${spacing('sm')};

  a {
    border-bottom: 1px solid ${color('Neutral400')};
  }
`

const TagContainer = styled.div`
  background: ${color('Neutral200')};
  border-radius: 3px;
  display: inline-flex;
  align-items: center;
  min-height: 32px;
  max-width: 100%;

  margin-right: ${spacing('xxs')};
  padding-left: ${spacing('sm')};
  padding-right: ${spacing('sm')};

  color: ${color('Neutral700')};
  ${typography('button')}
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`

const ViewItem: FC<{ title: string; value: string }> = ({ title, value }) => {
  return (
    <ViewContainer>
      <LabelText>{title}</LabelText>
      <ValueText>
        <NHyperlinkOrText>{value}</NHyperlinkOrText>
      </ValueText>
    </ViewContainer>
  )
}

const TemporaryText: FC<{ field: FieldSchema }> = ({ field }) => {
  return (
    <NTypography.Overline>
      Component in progress for field: {field.displayName} / type: {field.typeName}
    </NTypography.Overline>
  )
}

type NFieldItemProps = {
  mode?: FieldItemMode
  field: FieldSchema
  // view
  contents: Record<string, any>
  //
  displayOptions?: {
    dateFormat?: string
    timeFormat?: string
  }
  disableValidation?: boolean
}

const defaultDisplayOptions = {
  dateFormat: 'dd/MM/yyyy',
  timeFormat: 'HH:mm:ss',
}

export const NFieldItem: FC<NFieldItemProps> = ({
  mode = FieldItemMode.View,
  field,
  contents,
  displayOptions,
  disableValidation,
}) => {
  const formMethods = useFormContext<Record<string, any>>()
  const { defaultCurrencyCode } = useSession()

  const { typeName, subType, defaultValue, name, displayName, isRequired: fieldIsRequired } = field
  const { dateFormat, timeFormat } = { ...defaultDisplayOptions, ...displayOptions }

  const isRequired = disableValidation ? false : fieldIsRequired
  const { validateFunction } = useValidateString(isRequired)

  ////////////
  // form mode
  ////////////
  if (formMethods && mode === FieldItemMode.Form) {
    const { control, register } = formMethods
    const errorMsg = formMethods.formState.errors[name]?.message

    if (typeName === 'text') {
      return (
        <NTextInput
          required={isRequired}
          {...register(name, {
            validate: validateFunction,
          })}
          label={displayName}
          defaultValue={contents[name] || defaultValue}
          error={errorMsg}
        />
      )
    }

    if (typeName === 'numeric') {
      return (
        <NTextInput
          required={isRequired}
          {...register(name, {
            validate: validateFunction,
          })}
          type="number"
          name={name}
          label={displayName}
          defaultValue={contents[name] || defaultValue}
          error={errorMsg}
        />
      )
    }

    if (typeName === 'currency') {
      if (defaultCurrencyCode) {
        return (
          <Controller
            name={name}
            control={control}
            rules={{ required: { value: isRequired, message: 'Required' } }}
            defaultValue={contents[name] || defaultValue || ''}
            render={({ field: { value, onChange } }) => {
              return (
                <NCurrencyInput
                  currencyCode={defaultCurrencyCode}
                  required={isRequired}
                  label={displayName}
                  value={value}
                  onChange={e => {
                    onChange(e.target.value)
                  }}
                  error={errorMsg}
                />
              )
            }}
          />
        )
      }
      return <NSpinner size={20} strokeWidth={2} />
    }

    if (typeName === 'boolean') {
      const booleanDefaultValue =
        (contents[name] !== undefined
          ? contents[name]
          : typeof defaultValue === 'string'
          ? JSON.parse(defaultValue)
          : defaultValue) || false
      return (
        <Controller
          control={control}
          name={name}
          defaultValue={booleanDefaultValue}
          render={({ field: { value, onChange } }) => {
            const actualValue = typeof value === 'string' ? JSON.parse(value) : value
            return (
              <NCheckbox
                checked={actualValue}
                value={actualValue}
                onChange={event => {
                  onChange(event.target.checked)
                }}
                title={displayName}
              />
            )
          }}
        />
      )
    }

    if (typeName === 'dateTime') {
      return (
        <Controller
          control={control}
          name={name}
          rules={{ required: { value: isRequired, message: 'Required' } }}
          defaultValue={contents[name] || defaultValue}
          render={({ field: { value: val, onChange } }) => {
            // translate ios string to date
            let valueDate = val ? getDateFromIsoString(val, subType) : undefined

            // translate date to ios string
            const handleOnChange = (newDate: Date) => {
              onChange(getValueFromDate(newDate, subType))
            }

            switch (subType) {
              case 'date':
                return (
                  <NDatePicker
                    required={isRequired}
                    label={displayName}
                    value={valueDate}
                    onChangeValue={handleOnChange}
                    error={errorMsg}
                  />
                )
              case 'time':
                return (
                  <NTimePicker
                    required={isRequired}
                    label={displayName}
                    value={valueDate}
                    onChangeValue={handleOnChange}
                    error={errorMsg}
                  />
                )
              default:
                return (
                  <NDateTimePicker
                    required={isRequired}
                    label={displayName}
                    value={valueDate}
                    onValueChange={handleOnChange}
                    error={errorMsg}
                  />
                )
            }
          }}
        />
      )
    }

    if (typeName === 'pickList') {
      const options = (Array.isArray(field.value) ? field.value : []).map(op => ({
        label: op,
        value: op,
      }))
      if (subType === 'single') {
        return (
          <Controller
            control={control}
            name={name}
            rules={{ required: { value: isRequired, message: 'Required' } }}
            defaultValue={contents[name] || defaultValue}
            render={({ field: { value: val, onChange } }) => {
              return (
                <NSingleSelect
                  fullWidth
                  required={isRequired}
                  options={options as SelectOption[]}
                  value={val}
                  onValueChange={newValue => {
                    onChange(newValue)
                  }}
                  label={displayName}
                  error={errorMsg}
                />
              )
            }}
          />
        )
      } else {
        const multiDefaultValue: string[] = contents[name] || JSON.parse(defaultValue || '[]')
        return (
          <Controller
            control={control}
            name={name}
            rules={{
              required: { value: isRequired, message: 'Required' },
              validate: data => {
                const checkData = data || data.length === 0
                return checkData ? 'Required' : true
              },
            }}
            defaultValue={multiDefaultValue}
            render={({ field: { value: val, onChange } }) => {
              const convert: string[] = val || []
              return (
                <NMultiSelect
                  fullWidth
                  required={isRequired}
                  options={options as SelectOption[]}
                  values={convert}
                  onValuesChange={newValue => {
                    onChange(newValue)
                  }}
                  label={displayName}
                  error={errorMsg}
                />
              )
            }}
          />
        )
      }
    }

    if (typeName === 'relation') {
      return <NRelationPicker field={field} contents={contents} formMethods={formMethods} error={errorMsg} />
    }

    if (typeName === 'externalRelation') {
      return <NExternalRelationPicker field={field} contents={contents} formMethods={formMethods} error={errorMsg} />
    }

    if (typeName === 'generated') {
      return <ViewItem title={displayName} value={'The value of this field will be auto-generated'} />
    }

    if (typeName === 'json') {
      return (
        <NTextArea
          required={isRequired}
          {...register(name, {
            validate: validateFunction,
          })}
          label={displayName}
          defaultValue={contents[name] || defaultValue}
          error={errorMsg}
        />
      )
    }
  }
  ////////////
  // view mode
  ////////////
  const value = contents[name]
  if (typeName === 'text' || typeName === 'numeric' || typeName === 'generated') {
    return <ViewItem title={displayName} value={value} />
  }

  if (typeName === 'dateTime') {
    const date = value ? getDateFromIsoString(value, subType) : undefined
    let dateStr = date ? getStringFromDate({ date, type: subType, dateFormat, timeFormat }) : ''

    return <ViewItem title={displayName} value={dateStr} />
  }
  if (typeName === 'currency') {
    return (
      <ViewContainer>
        <LabelText>{displayName}</LabelText>
        <ValueText>
          {contents['currencyCode'] && (
            <CurrencyData
              dataCurrencyCode={contents['currencyCode']}
              value={value}
              convertedValue={contents['$metadata']?.currency_values?.[field.name]}
            />
          )}
        </ValueText>
      </ViewContainer>
    )
  }

  if (typeName === 'boolean') {
    return <NCheckbox title={displayName} checked={!!value} onChange={() => {}} />
  }

  if (typeName === 'pickList') {
    if (subType === 'multi') {
      const array: string[] = Array.isArray(value) ? value : []
      return (
        <ViewContainer>
          <LabelText>{displayName}</LabelText>
          <ValueText>
            {array.map(v => (
              <TagContainer key={`Field-item-multi-${name}-${v}`}>{v}</TagContainer>
            ))}
          </ValueText>
        </ViewContainer>
      )
    }

    return <ViewItem title={displayName} value={value} />
  }

  if (typeName === 'relation') {
    const metadata = contents['$metadata'] as DataResponseMetadata
    const relation = (metadata ? metadata.relations : {}) as Record<string, string>
    const link = `/${field.value}/${relation[name]}`

    return (
      <ViewContainer>
        <LabelText>{displayName}</LabelText>
        {value ? (
          <ValueText>
            <NavLink to={link}>{value}</NavLink>
          </ValueText>
        ) : (
          <ValueText>{value}</ValueText>
        )}
      </ViewContainer>
    )
  }

  if (typeName === 'externalRelation' || typeName === 'indirectRelation') {
    const link = `/${field.value}/${value}`

    return (
      <ViewContainer>
        <LabelText>{displayName}</LabelText>
        {value ? (
          <ValueText>
            <NavLink to={link}>{value}</NavLink>
          </ValueText>
        ) : (
          <ValueText>{value}</ValueText>
        )}
      </ViewContainer>
    )
  }

  if (typeName === 'json') {
    return (
      <ViewContainer>
        <LabelText>{displayName}</LabelText>
        <ValueText>{JSON.stringify(value)}</ValueText>
      </ViewContainer>
    )
  }

  return <TemporaryText field={field} />
}
