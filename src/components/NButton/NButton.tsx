import { forwardRef } from 'react'
import styled from 'styled-components'
import { classnames } from '../../common/classnames'
import { color, spacing } from '../GlobalStyle'
import { NDivider } from '../NDivider'
import { NSpinner } from '../NSpinner/NSpinner'
import { typography } from '../NTypography'

export type ButtonType = 'primary' | 'default' | 'ghost' | 'outline' | 'outline-2' | 'link' | 'outline-3'
export type ButtonSize = 'small' | 'default'
export type HTMLType = 'button' | 'submit' | 'reset'

const BaseButton = styled.button<{ size?: ButtonSize; block?: boolean }>`
  padding-left: ${spacing('xl')};
  padding-right: ${spacing('xl')};
  height: ${props => (props.size === 'small' ? '32px' : '40px')};
  transition: all 0.2s ease-in-out;
  width: ${props => props.block && '100%'};
  &.has-icon-only {
    padding-left: 0;
    padding-right: 0;
    width: ${props => (props.size === 'small' ? '32px' : '40px')};
  }
  ${typography('button')};
  &:focus {
    outline: none;
  }
  &:disabled {
    opacity: 0.5;
    cursor: not-allowed;
  }
  display: inline-flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  border: 0;
  border-radius: 3px;
  //
  &.primary {
    background: ${color('Primary700')};
    color: ${color('white')};
    &:hover:enabled {
      background: ${color('Primary900')};
    }
    &:active {
      background: ${color('Primary700')};
      box-shadow: 0 0 0 4px ${color('Primary200')};
    }
  }
  //
  &.default {
    background: ${color('Neutral200')};
    color: ${color('Neutral700')};
    &:hover:enabled {
      background: ${color('Neutral300')};
    }
    &:active {
      box-shadow: 0 0 0 4px ${color('Neutral100')};
      background: ${color('Neutral200')};
    }
  }
  //
  &.ghost {
    color: ${color('Neutral700')};
    background: transparent;
    &:hover:enabled {
      background: ${color('Neutral300')};
    }
    &:active {
      background: transparent;
      box-shadow: 0 0 0 4px ${color('Neutral300')};
    }
  }
  //
  &.outline {
    border: 1px solid ${color('Primary700')};
    color: ${color('Neutral700')};
    background: transparent;
    &:hover:enabled {
      background: ${color('Primary100')};
    }
    &:active {
      background: transparent;
      color: ${color('Neutral700')};
      box-shadow: 0 0 0 4px ${color('Primary200')};
    }
  }
  //
  &.outline-2 {
    border: 1px solid ${color('Primary700')};
    color: ${color('Primary700')};
    background: transparent;
    &:hover:enabled {
      color: ${color('Primary700')};
      background: ${color('Primary100')};
    }
    &:active {
      background: transparent;
      color: ${color('Primary300')};
      box-shadow: 0 0 0 4px ${color('Primary200')};
    }
  }
  &.link {
    background: ${color('transparent')};
    padding: 0;
    color: ${color('Primary700')};
    &:hover {
      text-decoration: underline;
    }
  }
  &.outline-3 {
    border: 1px solid ${color('Neutral700')};
    color: ${color('Neutral700')};
    background: transparent;
    &:hover:enabled {
      color: ${color('Neutral700')};
      background: ${color('Neutral200')};
    }
    &:active {
      background: transparent;
      color: ${color('Neutral300')};
      box-shadow: 0 0 0 4px ${color('Neutral300')};
    }
  }
`

export type NButtonProps = {
  type?: ButtonType
  size?: ButtonSize
  loading?: boolean
  disabled?: boolean
  onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void
  icon?: React.ReactNode
  block?: boolean
  htmlType?: HTMLType
  className?: string
} & React.HTMLAttributes<HTMLButtonElement>

export const NButton = forwardRef<HTMLButtonElement | null, NButtonProps>(
  (
    {
      type = 'default',
      size = 'default',
      onClick,
      loading = false,
      disabled = false,
      icon,
      block = false,
      htmlType = 'button',
      className,
      children,
      ...rest
    }: NButtonProps,
    ref,
  ) => {
    const handleClick = (e: React.MouseEvent<HTMLButtonElement>) => {
      if (loading || disabled) {
        return
      }
      if (onClick) {
        onClick(e)
      }
    }

    const spinnerSize = size === 'small' ? 8 : 16

    const cls = classnames([className || '', type, !!icon && !children && 'has-icon-only'])

    // use #ffffff because white cannot be used in getAlpha
    return (
      <BaseButton
        data-testid="NButton"
        ref={ref}
        {...rest}
        className={cls}
        disabled={disabled}
        size={size}
        type={htmlType}
        onClick={handleClick}
        block={block}>
        {loading && <NSpinner size={spinnerSize} strokeWidth={2} color={type === 'primary' ? '#ffffff' : undefined} />}
        {!loading && (
          <>
            {icon && (
              <>
                {icon}
                {!!children && <NDivider vertical size="xs" />}
              </>
            )}
            {children}
          </>
        )}
      </BaseButton>
    )
  },
)
