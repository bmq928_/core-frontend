import '@testing-library/jest-dom'
import React from 'react'
import { cleanup, fireEvent, render } from '../../utils/test-utils'
import { NButton } from './NButton'

describe('NButton', () => {
  afterEach(cleanup)
  test('icon should be rendered', () => {
    const Icon = () => <div data-testid="Icon" />
    const { getByTestId } = render(
      <NButton type="primary" icon={<Icon />}>
        Button with icon
      </NButton>,
    )

    expect(getByTestId('Icon')).toBeDefined()
  })
  test('loading button should not be disabled', () => {
    const { getByTestId } = render(
      <NButton type="primary" loading={true}>
        Test Button
      </NButton>,
    )
    expect(getByTestId('NButton')).toBeEnabled()
    expect(getByTestId('NSpinner')).toBeInTheDocument()
  })
  test('button should not fire onClick while loading', () => {
    const onClick = jest.fn()
    const { getByTestId } = render(
      <NButton loading={true} onClick={onClick}>
        Test Button
      </NButton>,
    )

    fireEvent.click(getByTestId('NButton'))
    expect(onClick).not.toHaveBeenCalled()
  })

  test('button should have divider between icon', () => {
    const { getByTestId } = render(<NButton icon={<div />}>Text here</NButton>)
    expect(getByTestId('NDivider')).toBeInTheDocument()
  })
})
