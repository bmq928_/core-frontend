import React, { FC } from 'react'
import { useDrop } from 'react-dnd'
import styled, { css } from 'styled-components'
import { spacing } from '../GlobalStyle'
import { DndOptionType, DraggableItemProps, OptionsType } from './NDraggableOption'

type WrapperSizeProps = {
  length: number
  height?: number
  noPadding?: boolean
}

const AvailableOptionsWrapper = styled.div<WrapperSizeProps>`
  display: grid;

  ${props => {
    const { length, height = 48 } = props
    return css`
      grid-template-rows: repeat(${length}, ${spacing(height)});
    `
  }}
`

const SelectedOptionsWrapper = styled.div<WrapperSizeProps>`
  display: grid;
  flex: 2;

  ${props => {
    const { length, height = 48 } = props
    return css`
      grid-template-rows: repeat(${length}, calc(${spacing(height)}));
    `
  }}
  ${props => {
    const { noPadding } = props
    if (!noPadding) {
      return css`
        grid-row-gap: ${spacing('md')};
        padding: calc(${spacing('xl')} - ${spacing('md')} / 2) ${spacing('xl')};
      `
    }
  }}
`

type Props = {
  onDrop: (option: DndOptionType) => void
} & WrapperSizeProps

export const AvailableOptionsContainer: FC<Props> = ({ onDrop, ...wrapperSizeProps }) => {
  const [, drop] = useDrop({
    accept: OptionsType.Selected,
    drop: (item: DraggableItemProps) => {
      const { index: _i, ...option } = item
      if (typeof option !== 'object') {
        return
      }
      onDrop(option)
    },
  })

  return <AvailableOptionsWrapper ref={drop} {...wrapperSizeProps} />
}

export const SelectedOptionsContainer: FC<Props> = ({ onDrop, noPadding, ...wrapperSizeProps }) => {
  const [, drop] = useDrop({
    accept: OptionsType.Available,
    options: {
      dropEffect: 'add',
    },
    drop: (item: DraggableItemProps, monitor) => {
      const didDrop = monitor.didDrop()
      if (didDrop) {
        return
      }
      const { index: _i, ...option } = item
      onDrop(option)
    },
  })
  return <SelectedOptionsWrapper noPadding={noPadding} ref={drop} {...wrapperSizeProps} />
}
