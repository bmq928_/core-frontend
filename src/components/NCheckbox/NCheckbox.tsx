import React, { forwardRef, InputHTMLAttributes, useEffect } from 'react'
import styled from 'styled-components'
import { useCombinedRefs } from '../../hooks/useCombinedRefs'
import { color, spacing } from '../GlobalStyle'
import { Icons } from '../Icons'
import { NTypography } from '../NTypography'

const Container = styled('label').attrs((props: { disabled?: boolean }) => {
  if (props.disabled) {
    return {
      style: {
        cursor: 'not-allowed',
      },
    }
  }
})<{ disabled?: boolean }>`
  display: flex;
  flex-direction: row;
  cursor: pointer;
`

const CheckboxSquare = styled.div<{ disabled?: boolean }>`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  width: ${spacing('md')};
  height: ${spacing('md')};
  border-radius: 4px;

  border-style: solid;
  border-width: 1px;
  border-color: ${color('Neutral200')};

  color: ${color('white')};
  text-align: center;
  letter-spacing: 0px;

  transition: background-color, border-color 0.25s;
  background-color: ${color('white')};
`

const CheckboxInput = styled.input`
  pointer-events: none;
  opacity: 0;
  width: 0;
  height: 0;
  color: ${color('white')};

  :checked ~ ${CheckboxSquare} {
    background-color: ${color('Primary700')};
    border-color: ${color('transparent')};
  }

  :indeterminate ~ ${CheckboxSquare} {
    background-color: ${color('Primary700')};
    border-color: ${color('transparent')};
  }

  :disabled ~ ${CheckboxSquare}, :disabled:indeterminate ~ ${CheckboxSquare} {
    background-color: ${color('Neutral200')};
    border-color: ${color('transparent')};
  }
`

const TextContainer = styled('div')`
  margin-left: ${spacing('md')};
`

let checkboxId = 1

export type NCheckboxProps = {
  // data
  title?: string
  subtitle?: string

  indeterminate?: boolean
} & InputHTMLAttributes<HTMLInputElement>

export const NCheckbox = forwardRef<HTMLInputElement | null, NCheckboxProps>(
  ({ id, className, title, subtitle, indeterminate, disabled, checked, ...inputProps }: NCheckboxProps, ref) => {
    const combinedRef = useCombinedRefs([ref])
    const { current: inputId } = React.useRef(id || `ncheckbox-${++checkboxId}`)

    useEffect(() => {
      if (combinedRef.current) {
        if (!checked) {
          combinedRef.current.indeterminate = !!indeterminate
          return
        }
        combinedRef.current.indeterminate = false
      }
    }, [indeterminate, checked])

    return (
      <Container data-testid="Container" className={className} disabled={disabled} htmlFor={inputId}>
        <CheckboxInput
          ref={combinedRef}
          type="checkbox"
          {...inputProps}
          disabled={disabled}
          checked={checked}
          id={inputId}
          aria-hidden={true}
        />
        <CheckboxSquare data-testid="CheckboxSquare" disabled={disabled}>
          {!checked && indeterminate && '-'}
          {checked && <Icons.Check />}
        </CheckboxSquare>
        {(title || subtitle) && (
          <TextContainer>
            {title && <NTypography.Headline className={disabled ? 'disabled' : ''}>{title}</NTypography.Headline>}
            {subtitle && <NTypography.Caption className={disabled ? 'disabled' : ''}>{subtitle}</NTypography.Caption>}
          </TextContainer>
        )}
      </Container>
    )
  },
)
