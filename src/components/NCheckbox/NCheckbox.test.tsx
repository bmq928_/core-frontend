import '@testing-library/jest-dom'
import { cleanup, render } from '@testing-library/react'
import { ThemeProvider } from 'styled-components'
import { theme } from '../GlobalStyle'
import { NCheckbox } from './NCheckbox'

describe('NCheckbox', () => {
  afterEach(cleanup)

  test('disabled NCheckbox should be disabled', () => {
    const { getByTestId } = render(
      <ThemeProvider theme={theme}>
        <NCheckbox disabled />
      </ThemeProvider>,
    )
    getByTestId('Container').click()
    expect(getByTestId('CheckboxSquare').getElementsByTagName('svg').length === 0)
  })

  test('if set checked value, NCheckbox checked status should be that value', () => {
    const { getByTestId } = render(
      <ThemeProvider theme={theme}>
        <NCheckbox checked onChange={() => {}} />
      </ThemeProvider>,
    )
    const CheckboxSquare = getByTestId('CheckboxSquare')
    expect(CheckboxSquare.getElementsByTagName('svg').length === 1)
    const checkbox = getByTestId('CheckboxSquare')
    checkbox.click()
    expect(CheckboxSquare.getElementsByTagName('svg').length === 1)
  })

  test('if set indeterminate value, NCheckbox status should be indeterminate', () => {
    const { getByTestId } = render(
      <ThemeProvider theme={theme}>
        <NCheckbox indeterminate />
      </ThemeProvider>,
    )
    const CheckboxSquare = getByTestId('CheckboxSquare')
    expect(CheckboxSquare.nodeValue === '-')
    const checkbox = getByTestId('CheckboxSquare')
    checkbox.click()
    expect(CheckboxSquare.nodeValue === '-')
  })

  test('if set indeterminate and checked value, indeterminate should has higher priority', () => {
    const { getByTestId } = render(
      <ThemeProvider theme={theme}>
        <NCheckbox checked indeterminate onChange={() => {}} />
      </ThemeProvider>,
    )
    const CheckboxSquare = getByTestId('CheckboxSquare')
    expect(CheckboxSquare.nodeValue === '-')
    expect(CheckboxSquare.getElementsByTagName('svg').length === 1)
  })
})
