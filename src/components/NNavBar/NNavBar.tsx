import { FC, ReactNode } from 'react'
import styled from 'styled-components'
import { spacing } from '../GlobalStyle'
import { NSideBar } from '../NLayout'
import { NNavBarItem, NNavBarItemProps } from './NNavBarItem'

const NavBody = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
`

const SideBar = styled(NSideBar)`
  padding-top: ${spacing('sm')};
  padding-bottom: ${spacing(10)};
`

const LogoContainer = styled.div`
  width: 32px;
  height: 32px;
  margin-bottom: ${spacing(18)};
  align-items: center;
  justify-content: center;
  display: flex;
`

export type NNavBarProps = {
  top?: ReactNode
  bottom?: ReactNode
  logo?: ReactNode
  className?: string
}

export const NNavBar: FC<NNavBarProps> & {
  Item: FC<NNavBarItemProps>
} = ({ children, bottom, logo, top, className }) => {
  return (
    <SideBar className={className} mode="light" collapsed>
      {logo && <LogoContainer>{logo}</LogoContainer>}
      {top}
      <NavBody>{children}</NavBody>
      {bottom}
    </SideBar>
  )
}

NNavBar.Item = NNavBarItem
