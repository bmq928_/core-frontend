import { FC, ReactNode } from 'react'
import { matchPath, NavLink, NavLinkProps, useLocation } from 'react-router-dom'
import styled from 'styled-components'
import { color, spacing } from '../GlobalStyle'
import { elevation } from '../NElevation'

const Tooltip = styled.div`
  display: none;
  position: absolute;
  z-index: 100;
  left: 50px;

  padding: ${spacing('xs')};
  border-radius: 4px;
  background-color: ${color('white')};
  ${elevation('Elevation200')}
`

const NavBarItem = styled(NavLink)`
  position: relative;
  text-decoration: none;
  background-color: transparent;
  color: ${color('Neutral900')};
  &:hover {
    background-color: ${color('Neutral200')};
    ${Tooltip} {
      display: initial;
    }
  }
  &.active {
    background-color: ${color('Primary200')};
    text-decoration: none;
    color: ${color('Primary900')};
  }
  height: 40px;
  width: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  transition: background-color 0.2s ease-in-out, color 0.2s ease-in-out;
  border-radius: 6px;
`

export type NNavBarItemProps = {
  icon?: ReactNode
  activeIcon?: ReactNode
  to: string
  className?: string
} & NavLinkProps

export const NNavBarItem: FC<NNavBarItemProps> = ({
  children,
  to,
  activeIcon,
  icon,
  className,
  title,
  ...restProps
}) => {
  const location = useLocation()
  const isActive = !!matchPath(location.pathname, { path: to })

  return (
    <NavBarItem className={className} {...restProps} to={to}>
      {activeIcon && icon && (isActive ? activeIcon : icon)}
      {children}
      {title && <Tooltip>{title}</Tooltip>}
    </NavBarItem>
  )
}
