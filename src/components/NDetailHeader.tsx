import * as React from 'react'
import styled from 'styled-components'
import { color, spacing } from './GlobalStyle'
import { Breadcrumb, NBreadcrumbs } from './NBreadcrumbs/NBreadcrumbs'
import { NDivider } from './NDivider'

const Wrapper = styled('div')`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: ${spacing('md')};
  height: 40px;
`

const ActionsWrapper = styled('div')`
  display: flex;
  align-items: center;
  p {
    color: ${color('Neutral400')};
  }
`

type HeaderProps = {
  children?: React.ReactNode
  label?: string
  breadcrumbs: Breadcrumb[]
}

export function NDetailHeader({ breadcrumbs, label, children }: HeaderProps) {
  return (
    <Wrapper>
      <NBreadcrumbs breadcrumbs={breadcrumbs} />
      <ActionsWrapper>
        {label && (
          <React.Fragment>
            <p>{label}</p>
            <NDivider vertical size="xl" />
          </React.Fragment>
        )}
        {children}
      </ActionsWrapper>
    </Wrapper>
  )
}
