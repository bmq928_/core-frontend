import '@testing-library/jest-dom'
import { cleanup, render, screen } from '../../utils/test-utils'
import { NModal } from './NModal'

describe('NModal', () => {
  afterEach(cleanup)
  test('modal should be rendered', () => {
    const { getByTestId } = render(
      <NModal visible={true}>
        <NModal.Header>Test Modal</NModal.Header>
      </NModal>,
    )
    expect(getByTestId('NModal')).toBeInTheDocument()
  })
  test('modal should not be rendered', () => {
    render(
      <NModal visible={false}>
        <NModal.Header>Test Modal</NModal.Header>
      </NModal>,
    )
    expect(screen.queryAllByTestId('NModal')).toHaveLength(0)
  })
  test('modal should render body and title', () => {
    render(
      <NModal visible={true}>
        <NModal.Header>Title</NModal.Header>
        <NModal.Body>Body</NModal.Body>
      </NModal>,
    )
    expect(screen.getByText('Title')).toBeInTheDocument()
    expect(screen.getByText('Body')).toBeInTheDocument()
  })
})
