import { FC } from 'react'
import styled from 'styled-components'
import { spacing } from '../GlobalStyle'
import { NModal } from './NModal'

export type NConfirmModalProps = {
  visible: boolean
  setVisible: (visible: boolean) => void
  title?: string
  onCancel: () => void
  onConfirm: () => void
}

const StyledBody = styled(NModal.Body)`
  padding: ${spacing('xl')};
`

export const NConfirmModal: FC<NConfirmModalProps> = ({
  children,
  visible,
  setVisible,
  title,
  onCancel,
  onConfirm,
}) => {
  return (
    <NModal visible={visible} top="middle" setVisible={setVisible}>
      <NModal.Header>{title || 'Confirmation'}</NModal.Header>
      <StyledBody>{children}</StyledBody>
      <NModal.Footer onCancel={onCancel} onFinish={onConfirm} finishText="Confirm" />
    </NModal>
  )
}
