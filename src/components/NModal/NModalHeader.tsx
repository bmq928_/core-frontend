import { FC } from 'react'
import styled from 'styled-components'
import { color, spacing } from '../GlobalStyle'
import { GlobalIcons } from '../Icons'
import { typography } from '../NTypography'

const Header = styled.div`
  display: flex;
  height: 48px;
  align-items: center;
  border-top-left-radius: 4px;
  border-top-right-radius: 4px;
  box-sizing: border-box;
  border-bottom: 1px solid ${color('Neutral200')};
  background-color: ${color('white')};
`
const HeaderContainer = styled.div`
  padding-left: ${spacing('xl')};
  padding-right: ${spacing('xl')};
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex: 1;
`

const TitleContainer = styled.div`
  display: flex;
  align-items: center;
`

const Title = styled.p`
  ${typography('h500')}
`
const Back = styled.div`
  cursor: pointer;
  border-right: 1px solid ${color('Neutral200')};
  height: 100%;
  width: 48px;
  box-sizing: border-box;
  padding: auto;
  display: flex;
  justify-content: center;
  align-items: center;
`

const Close = styled.div`
  cursor: pointer;
`

export type NModalHeaderProps = {
  title?: string
  onClose?: () => void
  onBack?: () => void
}

export const NModalHeader: FC<NModalHeaderProps> = ({ title, children, onClose, onBack }) => {
  return (
    <Header>
      {onBack && (
        <Back onClick={onBack}>
          <GlobalIcons.LeftLarge />
        </Back>
      )}
      <HeaderContainer>
        <TitleContainer>
          <Title>{title}</Title>
          {children}
        </TitleContainer>
        {onClose && (
          <Close
            onClick={() => {
              onClose()
            }}>
            <GlobalIcons.Close />
          </Close>
        )}
      </HeaderContainer>
    </Header>
  )
}

NModalHeader.displayName = 'NModal.Header'
