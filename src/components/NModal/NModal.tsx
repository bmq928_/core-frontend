import * as React from 'react'
import styled, { css } from 'styled-components'
import { rootPortal } from '../../App'
import { color } from '../GlobalStyle'
import { elevation } from '../NElevation'
import { Portal } from '../Portal'
import { NModalFooter, NModalFooterProps } from './NModalFooter'
import { NModalHeader, NModalHeaderProps } from './NModalHeader'

type ModalSize = 'default' | 'large' | 'x-large'
type TopPosition = 'top' | 'middle'

const Overlay = styled.div`
  height: 100vh;
  width: 100vw;
  position: fixed;
  top: 0px;
  background: ${color('Neutral900')};
  opacity: 0.2;
  z-index: 999;
  transition: all 0.3s ease-in-out;
`

const Modal = styled.div<{ size: ModalSize; top: TopPosition; zIndex?: number }>`
  position: fixed;
  left: 50%;
  background: ${color('white')};
  ${elevation('Elevation200')};
  border-radius: 4px;
  z-index: ${props => (props.zIndex ? props.zIndex : 999)};
  transition: all 0.3s ease-in-out;
  width: ${props => {
    switch (props.size) {
      case 'x-large':
        return '1200px'
      case 'large':
        return '900px'
      default:
        return '600px'
    }
  }};
  ${props => {
    if (props.top === 'top') {
      return css`
        top: 104px;
        transform: translateX(-50%);
      `
    }
    if (props.top === 'middle') {
      return css`
        top: 50%;
        transform: translateX(-50%) translateY(-50%);
      `
    }
  }}
`
export const NModalBody = styled.div``

export type NModalProps = {
  className?: string
  visible: boolean
  setVisible?: (visible: boolean) => void
  closeOnOverlayClick?: boolean
  size?: ModalSize
  top?: 'top' | 'middle'
  zIndex?: number
  root?: React.RefObject<HTMLDivElement | null>
}

export const NModal: React.FC<NModalProps> & {
  Header: React.FC<NModalHeaderProps>
  Footer: React.FC<NModalFooterProps>
  Body: React.FC
} = ({
  className,
  children,
  visible,
  zIndex,
  setVisible,
  closeOnOverlayClick = true,
  size = 'default',
  top = 'top',
  root,
}) => {
  React.useEffect(() => {
    if (visible) {
      document.body.style.overflow = 'hidden'
    } else {
      document.body.style.overflow = ''
    }
    return () => {
      document.body.style.overflow = ''
    }
  }, [visible])
  return (
    <Portal mountNode={root || rootPortal}>
      {visible && (
        <>
          <Overlay
            data-testid="NModalOverlay"
            onClick={e => {
              e.stopPropagation()
              closeOnOverlayClick && setVisible && setVisible(false)
            }}
          />
          <Modal className={className} data-testid="NModal" size={size} top={top} zIndex={zIndex}>
            {children}
          </Modal>
        </>
      )}
    </Portal>
  )
}

NModal.Header = NModalHeader
NModal.Footer = NModalFooter
NModal.Body = NModalBody

NModalBody.displayName = 'NModal.Body'
