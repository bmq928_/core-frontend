import { FC } from 'react'
import styled from 'styled-components'
import { color, spacing } from '../GlobalStyle'
import { NButton } from '../NButton/NButton'

const Footer = styled.div`
  background: ${color('Neutral200')};
  height: 48px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-left: ${spacing('xl')};
  padding-right: ${spacing('xl')};
  border-bottom-left-radius: 4px;
  border-bottom-right-radius: 4px;

  button {
    min-width: 88px;
  }
`

export type NModalFooterProps = {
  isLoading?: boolean
  onFinish?: () => void
  onCancel?: () => void
  cancelText?: string
  finishText?: string
}

export const NModalFooter: FC<NModalFooterProps> = ({
  onCancel,
  onFinish,
  isLoading,
  cancelText,
  finishText,
  children,
}) => {
  return (
    <Footer>
      {children || (
        <>
          <NButton
            size="small"
            type="ghost"
            onClick={() => {
              onCancel && onCancel()
            }}>
            {cancelText || 'Cancel'}
          </NButton>
          <NButton
            size="small"
            type="primary"
            loading={isLoading}
            onClick={() => {
              onFinish && onFinish()
            }}>
            {finishText || 'Finish'}
          </NButton>
        </>
      )}
    </Footer>
  )
}

NModalFooter.displayName = 'NModal.Footer'
