import styled, { css } from 'styled-components'
import { color, fontSize } from './GlobalStyle'

export const RequiredIndicator = styled.span`
  color: ${color('Red700')};
`

export function typography(
  type: // heading
  | 'h900'
    | 'h800'
    | 'h600'
    | 'h500'
    | 'h400'
    | 'h300'
    | 'h200'
    // subtitle
    | 's800'
    | 's600'
    // ui
    | 'overline'
    | 'small-overline'
    | 'small-bold-text'
    | 'x-small-bold-text'
    | 'ui-text'
    | 'small-ui-text'
    | 'x-small-ui-text'
    // other
    | 'label'
    | 'caption'
    | 'button',
) {
  switch (type) {
    // Heading
    case 'h900':
      return css`
        font-size: ${fontSize('xxl')};
        font-weight: bold;
      `
    case 'h800':
      return css`
        font-size: ${fontSize('xl')};
        font-weight: bold;
      `
    case 'h600':
      return css`
        font-size: ${fontSize('lg')};
        font-weight: bold;
      `
    case 'h500':
      return css`
        font-size: ${fontSize('md')};
        font-weight: bold;
      `
    case 'h400':
      return css`
        font-size: ${fontSize('sm')};
        font-weight: medium;
      `
    case 'h300':
      return css`
        font-size: ${fontSize('xs')};
        font-weight: bold;
      `
    case 'h200':
      return css`
        font-size: calc(${fontSize('xs')} - 1px);
        font-weight: bold;
        color: ${color('Neutral900')};
      `
    // Subtitle
    case 's800':
      return css`
        font-size: ${fontSize('lg')};
        font-weight: medium;
      `
    case 's600':
      return css`
        font-size: ${fontSize('md')};
        font-weight: medium;
      `
    // UI
    case 'overline':
      return css`
        font-family: 'IBM Plex Sans', sans-serif;
        font-weight: 500;
        font-size: ${fontSize('xs')};
        line-height: ${fontSize('md')};
        letter-spacing: 0.15em;
        text-transform: uppercase;
        font-weight: 500;
      `
    case 'small-overline':
      return css`
        font-family: 'IBM Flex Sans', sans-serif;
        font-size: ${fontSize('xxs')};
        line-height: ${fontSize('sm')};
        font-weight: 500;
        letter-spacing: 0.17em;
        text-transform: uppercase;
      `
    //
    case 'small-bold-text':
      return css`
        font-size: ${fontSize('sm')};
        font-style: medium;
      `
    case 'x-small-bold-text':
      return css`
        font-size: ${fontSize('xs')};
        font-style: medium;
      `
    //
    /* Paragraph / Body 1 */
    case 'ui-text':
      return css`
        font-size: ${fontSize('md')};
        line-height: ${fontSize('xl')};
      `
    /* Paragraph / Body 2 */
    case 'small-ui-text':
      return css`
        font-size: ${fontSize('sm')};
        line-height: calc(${fontSize('xl')} - 3px);
      `
    /* Paragraph / Body 3 */
    case 'x-small-ui-text':
      return css`
        font-size: ${fontSize('xs')};
        line-height: ${fontSize('lg')};
      `
    // other
    case 'label':
      return css`
        font-weight: bold;
        font-size: ${fontSize('xs')};
      `
    case 'button':
      return css`
        font-size: ${fontSize('sm')};
      `
    /* Paragraph / Body Cpation */
    case 'caption':
      return css`
        font-size: ${fontSize('xs')};
      `
  }
}

// Convert to component if needed
const Overline = styled('div')`
  ${typography('overline')}
`

const InputLabel = styled('label')`
  display: flex;
  ${typography('label')};
`
const InputCaption = styled('p')`
  color: ${color('Neutral400')};
  ${typography('caption')};
`

const Headline = styled.p`
  ${typography('small-bold-text')};
  color: ${color('Neutral900')};

  &.disabled {
    color: ${color('Neutral400')};
  }

  transition: color 0.25s;
`

const Caption = styled.p`
  ${typography('x-small-ui-text')};

  color: ${color('Neutral400')};
  &.disabled {
    color: ${color('Neutral300')};
  }

  transition: color 0.25s;
`

const SmallText = styled('p')`
  ${typography('x-small-ui-text')}
`

const H200 = styled('div')`
  ${typography('h200')}
`

export const NTypography = {
  H200,
  Overline,
  InputLabel,
  InputCaption,
  Headline,
  Caption,
  SmallText,
}
