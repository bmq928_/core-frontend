import * as React from 'react'
import { useCombinedRefs } from '../hooks/useCombinedRefs'
import { ElementRect, useMeasure } from '../hooks/useMeasure'
import { Portal } from './Portal'

export type NPopoverPlacement =
  | 'top'
  | 'topLeft'
  | 'topRight'
  | 'right'
  | 'rightTop'
  | 'rightBottom'
  | 'bottom'
  | 'bottomLeft'
  | 'bottomRight'
  | 'left'
  | 'leftTop'
  | 'leftBottom'

/*
 * Function calculate position based on placement
 * Only auto calculate position if placement === undefined
 * Return 1 position if placement existed.
 * Even content size bigger than viewport
 */
function getStyles(
  targetRect: ElementRect,
  contentRect: ElementRect,
  options: { offsetX?: number; offsetY?: number; placement?: NPopoverPlacement },
): { top: number; left: number } {
  const { offsetX = 0, offsetY = 0, placement } = options
  // if don't have placement, return auto calculate position
  if (!placement) {
    return getStylesDefault(targetRect, contentRect, offsetX, offsetY)
  }
  // calculate position for vertical placements
  if (placement.includes('top') || placement.includes('bottom')) {
    const isTop = placement.includes('top')
    const isLeft = placement.includes('Left')
    const isRight = placement.includes('Right')
    const top = isTop ? targetRect.top - contentRect.height + offsetY : targetRect.bottom + offsetY

    if (isLeft) {
      return {
        top,
        left: targetRect.left + offsetX,
      }
    }

    if (isRight) {
      return {
        top,
        left: targetRect.right - contentRect.width + offsetX,
      }
    }

    return {
      top,
      left: targetRect.left - (contentRect.width - targetRect.width) / 2 + offsetX,
    }
  }
  // calculate position for horizontal placement
  const isLeft = placement.includes('left')
  const isTop = placement.includes('Top')
  const isBottom = placement.includes('Bottom')
  const left = isLeft ? targetRect.left - contentRect.width : targetRect.right

  if (isTop) {
    return {
      left,
      top: targetRect.top + offsetY,
    }
  }

  if (isBottom) {
    return {
      left,
      top: targetRect.bottom - contentRect.height + offsetY,
    }
  }

  return {
    top: targetRect.top - (contentRect.height - targetRect.height) / 2 + offsetY,
    left,
  }
}

/*
 * Auto calculate position.
 * Perfer topLeft/bottomLeft/topRight/bottomRight
 */
function getStylesDefault(targetRect: ElementRect, contentRect: ElementRect, offsetX: number, offsetY: number) {
  const collisions = {
    top: targetRect.top - contentRect.height < 0,
    right: window.innerWidth < targetRect.left + contentRect.width - offsetX,
    bottom: window.innerHeight < targetRect.bottom + contentRect.height - offsetY,
    left: targetRect.left + targetRect.width - contentRect.width < 0,
  }

  const directionRight = collisions.right && !collisions.left
  // const directionLeft = collisions.left && !collisions.right
  const directionUp = collisions.bottom && !collisions.top
  // const directionDown = collisions.top && !collisions.bottom
  const top = directionUp
    ? targetRect.top - contentRect.height + window.scrollY - offsetY
    : targetRect.bottom + window.scrollY + offsetY

  const left = directionRight
    ? targetRect.left + targetRect.width - contentRect.width + window.scrollX - offsetX
    : targetRect.left + window.scrollX + offsetX
  return { top, left }
}

export type NPopoverProps = {
  children?: React.ReactNode
  targetRef: React.RefObject<HTMLDivElement>
  placement?: NPopoverPlacement
  offsetX?: number
  offsetY?: number
  fullWidth?: boolean
}

export const NPopover = React.forwardRef<HTMLDivElement, NPopoverProps>(
  ({ targetRef, children, offsetX, offsetY, placement, fullWidth = false }, forwardRef) => {
    const contentRef = React.useRef<HTMLDivElement>(null)
    const ref = useCombinedRefs([forwardRef, contentRef])
    const targetRect = useMeasure(targetRef)
    const contentRect = useMeasure(contentRef)
    const { top, left } = getStyles(targetRect, contentRect, { offsetX, offsetY, placement })

    const minWidth = fullWidth ? targetRect.width : 'unset'
    if (React.isValidElement(children)) {
      return (
        <Portal>
          {React.cloneElement(children, {
            ...children.props,
            ref,
            style: { ...children.props.style, position: 'absolute', top, left, minWidth },
          })}
        </Portal>
      )
    }
    return (
      <Portal>
        <div ref={ref} style={{ position: 'absolute', top, left, minWidth }}>
          {children}
        </div>
      </Portal>
    )
  },
)
