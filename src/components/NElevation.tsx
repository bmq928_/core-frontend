import { css } from 'styled-components'

export function elevation(
  type: 'Container100' | 'Elevation100' | 'Elevation200' | 'Elevation300' | 'Elevation400' | 'Elevation500',
) {
  switch (type) {
    case 'Container100':
      return css`
        box-shadow: 0px 1px 2px rgba(13, 24, 38, 0.24), 0px 8px 18px rgba(13, 24, 38, 0.05);
      `
    case 'Elevation100':
      return css`
        box-shadow: 0px 1px 1px rgba(9, 30, 66, 0.25), 0px 0px 1px rgba(9, 30, 66, 0.31);
      `
    case 'Elevation200':
      return css`
        box-shadow: 0px 3px 5px rgba(9, 30, 66, 0.2), 0px 0px 1px rgba(9, 30, 66, 0.31);
      `
    case 'Elevation300':
      return css`
        box-shadow: 0px 8px 12px rgba(9, 30, 66, 0.15), 0px 0px 1px rgba(9, 30, 66, 0.31);
      `
    case 'Elevation400':
      return css`
        box-shadow: 0px 10px 18px rgba(9, 30, 66, 0.15), 0px 0px 1px rgba(9, 30, 66, 0.31);
      `
    case 'Elevation500':
      return css`
        box-shadow: 0px 18px 28px rgba(9, 30, 66, 0.15), 0px 0px 1px rgba(9, 30, 66, 0.31);
      `
    default:
      return null
  }
}
