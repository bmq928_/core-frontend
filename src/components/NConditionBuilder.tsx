import * as React from 'react'
import { Controller, useFieldArray, useFormContext } from 'react-hook-form'
import styled from 'styled-components'
import { FieldSchema } from '../services/api/models'
import { color, spacing } from './GlobalStyle'
import { GlobalIcons } from './Icons'
import { NButton } from './NButton/NButton'
import { NDivider } from './NDivider'
import { elevation } from './NElevation'
import { SelectOption } from './NSelect/model'
import { NSingleSelect } from './NSelect/NSingleSelect'
import { NTextInput } from './NTextInput/NTextInput'

const ButtonWrapper = styled('div')`
  display: inline-flex;
  margin: ${spacing('sm')} 0 0 0;
  position: relative;
  transform: translateX(${spacing('sm')});
  &:before {
    content: '';
    position: absolute;
    background: ${color('Primary200')};
    width: 1px;
    height: calc(${spacing('sm')} - 4px);
    left: 50%;
    transform: translateX(-50%);
  }
  &:before {
    bottom: calc(100% + 2px);
  }
  &.or-condition {
    margin-left: ${spacing('xl')};
  }
`

const Label = styled('div')`
  background: ${color('Primary700')};
  color: ${color('white')};
  border-radius: 4px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 56px;
  height: 24px;
  letter-spacing: 0.15em;
  text-transform: uppercase;
  margin: ${spacing('sm')} 0;
  position: relative;
  &:after,
  &:before {
    content: '';
    position: absolute;
    background: ${color('Primary200')};
    width: 1px;
    height: calc(${spacing('sm')} - 4px);
    left: 50%;
    transform: translateX(-50%);
  }
  &:after {
    top: calc(100% + 2px);
  }
  &:before {
    bottom: calc(100% + 2px);
  }
  &.or-condition {
    margin-left: ${spacing('xl')};
  }
`

const AndWrapper = styled('div')`
  ${elevation('Container100')}
  border-radius: 4px;
  padding: ${spacing('xl')};
`

const AndInputsWrapper = styled('div')`
  display: flex;
  align-items: center;

  & > :nth-child(5) {
    flex: 1;
  }
  & > :last-child {
    cursor: pointer;
  }
`

type ConditionProps = {
  operators: Record<string, SelectOption[]>
  isOperatorsLoading: boolean
  fieldNames: SelectOption[]
  fields: Record<string, FieldSchema>
}

type FormProps = {
  [key: string]: {
    rules: {
      fieldName: string
      operator: string
      value: string
    }[]
  }[]
}

function AndConditions({
  fields,
  name,
  operators,
  fieldNames,
  onRemoveLastItem,
  isOperatorsLoading,
}: ConditionProps & { name: `${string}.${number}.rules`; onRemoveLastItem(): void }) {
  const { control, watch, setValue } = useFormContext<FormProps>()
  const { fields: andConditions, append, remove } = useFieldArray({ control, name })
  return (
    <AndWrapper>
      {andConditions.map((field, fieldIndex) => {
        const fieldName = watch(`${name}.${fieldIndex}`).fieldName
        const options = (fieldName && operators[fields[fieldName].typeName]) || []
        return (
          <React.Fragment key={field.id}>
            <AndInputsWrapper>
              <Controller
                name={`${name}.${fieldIndex}.fieldName`}
                defaultValue={field.fieldName || ''}
                control={control}
                render={({ field: { value, onChange, onBlur }, fieldState }) => (
                  <NSingleSelect
                    options={fieldNames}
                    placeholder="Select field"
                    value={value}
                    onValueChange={v => {
                      onChange(v)
                      setValue(`${name}.${fieldIndex}.operator`, '')
                    }}
                    onBlur={onBlur}
                    error={fieldState.error?.message}
                    required
                  />
                )}
                rules={{
                  required: {
                    value: true,
                    message: 'Required!',
                  },
                }}
              />
              <NDivider vertical size="md" />
              <Controller
                name={`${name}.${fieldIndex}.operator`}
                defaultValue={field.operator || ''}
                control={control}
                render={({ field: { value, onChange, onBlur }, fieldState }) => (
                  <NSingleSelect
                    isLoading={isOperatorsLoading}
                    disabled={!fieldName}
                    options={options}
                    placeholder="Select operator"
                    value={value}
                    onValueChange={onChange}
                    onBlur={onBlur}
                    required={Boolean(fieldName)}
                    error={fieldState.error?.message}
                  />
                )}
                rules={{
                  required: {
                    value: Boolean(fieldName),
                    message: 'Required!',
                  },
                }}
              />
              <NDivider vertical size="md" />
              <Controller
                name={`${name}.${fieldIndex}.value`}
                defaultValue={field.value || ''}
                control={control}
                render={({ field: { value, onChange, onBlur }, fieldState }) => (
                  <NTextInput
                    disabled={!fieldName}
                    placeholder="Enter value"
                    value={value}
                    onChange={e => onChange(e.target.value)}
                    onBlur={onBlur}
                    required={Boolean(fieldName)}
                    error={fieldState.error?.message}
                  />
                )}
                rules={{
                  required: {
                    value: Boolean(fieldName),
                    message: 'Required!',
                  },
                }}
              />
              <NDivider vertical size="md" />
              <GlobalIcons.Trash
                onClick={() => {
                  if (andConditions.length === 1) {
                    onRemoveLastItem()
                    return
                  }
                  remove(fieldIndex)
                }}
              />
            </AndInputsWrapper>
            {fieldIndex + 1 < andConditions.length && <Label>And</Label>}
          </React.Fragment>
        )
      })}
      <ButtonWrapper>
        <NButton
          size="small"
          type="primary"
          icon={<GlobalIcons.Add />}
          onClick={() => {
            append({ fieldName: '', operator: '', value: '' })
          }}
        />
      </ButtonWrapper>
    </AndWrapper>
  )
}

export function NConditionBuilder({
  fields,
  name,
  operators,
  fieldNames,
  isOperatorsLoading,
}: ConditionProps & { name: string }) {
  const { control } = useFormContext<FormProps>()
  const { fields: orConditions, append, remove } = useFieldArray({ control, name })
  return (
    <React.Fragment>
      {orConditions.map((field, fieldIndex) => (
        <React.Fragment key={field.id}>
          <AndConditions
            isOperatorsLoading={isOperatorsLoading}
            operators={operators}
            fieldNames={fieldNames}
            name={`${name}.${fieldIndex}.rules`}
            onRemoveLastItem={() => remove(fieldIndex)}
            fields={fields}
          />
          {fieldIndex + 1 < orConditions.length && <Label className="or-condition">Or</Label>}
        </React.Fragment>
      ))}
      <ButtonWrapper className="or-condition">
        <NButton
          size="small"
          type="primary"
          icon={<GlobalIcons.Add />}
          onClick={() => {
            append([{ rules: [{ fieldName: '', operator: '', value: '' }] }])
          }}
        />
      </ButtonWrapper>
    </React.Fragment>
  )
}

export type FilterItem = { fieldName: string; operator: string; value: string }
export type Filters = { rules: FilterItem[] }

export function transformToFormValue(filters: FilterItem[][]) {
  return filters.map(rules => ({ rules }))
}

export function transformToPropsValue(filters: Filters[]) {
  return filters.map(({ rules }) => rules)
}
