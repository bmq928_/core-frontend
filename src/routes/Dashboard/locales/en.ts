export default {
  objectListing: {
    header: 'Object Listing',
    createObject: 'Create Object',
    objectNumbers: '{{numbers}} Objects',
    newObject: 'New Object',
    editObject: 'Edit Object',

    createObjectModal: {
      nameField: 'Name',
      nameFieldCaption: 'Name is used to generate the API routes and databases tables/collections',
      displayNameField: 'Display name',
      displayNameCaption: 'The display name of the object',
      descriptionField: 'Description',
      descriptionCaption: 'The description of the object',
    },
  },
  logout: 'Logout',
  profile: 'Profile',
}
