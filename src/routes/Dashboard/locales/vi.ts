export default {
  objectListing: {
    header: 'vi: Object Listing',
    createObject: 'vi: Create Object',
    objectNumbers: 'vi: {{numbers}} Objects',
    newObject: 'vi: New Object',
    editObject: 'vi: Edit Object',

    createObjectModal: {
      nameField: 'vi: Name',
      nameFieldCaption: 'vi: Name is used to generate the API routes and databases tables/collections',
      displayNameField: 'vi: Display name',
      displayNameCaption: 'vi: The display name of the object',
      descriptionField: 'vi: Description',
      descriptionCaption: 'vi: The description of the object',
    },
  },
  logout: 'vi:Logout',
  profile: 'vi:Profile',
}
