import * as React from 'react'
import { useMutation } from 'react-query'
import { useDispatch } from 'react-redux'
import { Redirect, Route, Switch, useHistory } from 'react-router-dom'
import styled from 'styled-components'
import { DASHBOARD_ROUTE } from '../../common/constants'
import { AppHeader } from '../../components/AppHeader/AppHeader'
import { spacing } from '../../components/GlobalStyle'
import { SidebarIcons } from '../../components/Icons'
import { NLayout } from '../../components/NLayout'
import { NNavBar } from '../../components/NNavBar/NNavBar'
import i18n from '../../i18n/i18n'
import { sessionActions, useSession } from '../../redux/slices/sessionSlice'
import { APIServices } from '../../services/api'
import en from './locales/en'
import vi from './locales/vi'
import { AppRoutes } from './routes/App/AppRoutes'
import { FlowListing } from './routes/Flow/FlowListing'
import { ExternalObjectsDetailRoutes } from './routes/Integrations/ExternalObjects/ExternalObjectsDetailRoutes'
import { IntegrationsRoutes } from './routes/Integrations/IntegrationsRoutes'
import { LayoutListing } from './routes/Layout/LayoutListings'
import { ObjectDetail } from './routes/Object/ObjectDetail'
import { ObjectListing } from './routes/Object/ObjectListing'
import { Organization } from './routes/Organization/Organization'
import { People } from './routes/People/People'

i18n.addResourceBundle('en', 'translation', { dashboard: en })
i18n.addResourceBundle('vi', 'translation', { dashboard: vi })

type DashboardProps = {
  children?: React.ReactNode
}

const Wrapper = styled.div`
  display: flex;
  flex: 1;
`

const NavSideBar = styled(NNavBar)`
  border-right: 0;
  width: 56px;
`

const NavItem = styled(NNavBar.Item)`
  margin-bottom: ${spacing('xs')};
`

export function Dashboard({}: DashboardProps) {
  const dispatch = useDispatch()
  const history = useHistory()
  const session = useSession()

  const { mutate: updateSession } = useMutation(APIServices.Session.updateSession, {
    onSuccess(d) {
      dispatch(sessionActions.setSession(d.data))
      history.push('/')
    },
  })

  const onSelectApp = (appId: string) => {
    if (session.guid) {
      updateSession({
        last_used_app: appId,
      })
    }
  }

  return (
    <NLayout>
      <AppHeader title="Core setup" onSelectApp={onSelectApp} />
      <Wrapper>
        <NavSideBar>
          <NavItem
            title="Applications"
            to={`${DASHBOARD_ROUTE}/app`}
            icon={<SidebarIcons.AppOutlined />}
            activeIcon={<SidebarIcons.AppFilled />}
          />
          <NavItem
            title="Objects"
            to={`${DASHBOARD_ROUTE}/object`}
            icon={<SidebarIcons.ObjectOutlined />}
            activeIcon={<SidebarIcons.ObjectFilled />}
          />
          <NavItem
            title="Layouts"
            to={`${DASHBOARD_ROUTE}/layout`}
            icon={<SidebarIcons.LayoutOutlined />}
            activeIcon={<SidebarIcons.LayoutFilled />}
          />
          <NavItem
            title="People"
            to={`${DASHBOARD_ROUTE}/people`}
            icon={<SidebarIcons.PeopleOutlined />}
            activeIcon={<SidebarIcons.PeopleFilled />}
          />
          <NavItem
            title="Integrations"
            to={`${DASHBOARD_ROUTE}/integrations`}
            icon={<SidebarIcons.IntegrationOutlined />}
            activeIcon={<SidebarIcons.IntegrationFilled />}
          />
          <NavItem
            title="Flows"
            to={`${DASHBOARD_ROUTE}/flow`}
            icon={<SidebarIcons.AutomationOutlined />}
            activeIcon={<SidebarIcons.AutomationFilled />}
          />
          <NavItem
            title="Organization"
            to={`${DASHBOARD_ROUTE}/organization`}
            icon={<SidebarIcons.OrganizationOutlined />}
            activeIcon={<SidebarIcons.OrganizationFilled />}
          />
        </NavSideBar>
        <Switch>
          <Route path={`${DASHBOARD_ROUTE}/app`}>
            <AppRoutes />
          </Route>
          <Route path={`${DASHBOARD_ROUTE}/object/:objName`}>
            <ObjectDetail />
          </Route>
          <Route path={`${DASHBOARD_ROUTE}/object`} exact>
            <ObjectListing />
          </Route>
          <Route path={`${DASHBOARD_ROUTE}/layout`}>
            <LayoutListing />
          </Route>
          <Route path={`${DASHBOARD_ROUTE}/people`}>
            <People />
          </Route>
          <Route path={`${DASHBOARD_ROUTE}/integrations/external-objects/:objName`}>
            <ExternalObjectsDetailRoutes />
          </Route>
          <Route path={`${DASHBOARD_ROUTE}/integrations`}>
            <IntegrationsRoutes />
          </Route>
          <Route path={`${DASHBOARD_ROUTE}/flow`}>
            <FlowListing />
          </Route>
          <Route path={`${DASHBOARD_ROUTE}/organization`}>
            <Organization />
          </Route>
          <Route path={`${DASHBOARD_ROUTE}`}>
            <Redirect to={`${DASHBOARD_ROUTE}/app`} />
          </Route>
        </Switch>
      </Wrapper>
    </NLayout>
  )
}
