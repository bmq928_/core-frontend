import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { ProfileDetails } from './ProfileDetails'
import { ProfileListing } from './ProfileListing'

type ProfilesProps = {}

export function Profiles({}: ProfilesProps) {
  return (
    <NContainer>
      <Switch>
        <Route path={`${DASHBOARD_ROUTE}/people/profiles/:id`}>
          <ProfileDetails />
        </Route>
        <Route path={`${DASHBOARD_ROUTE}/people/profiles`} exact>
          <ProfileListing />
        </Route>
      </Switch>
    </NContainer>
  )
}
