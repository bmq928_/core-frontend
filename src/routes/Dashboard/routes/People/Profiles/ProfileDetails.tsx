import * as React from 'react'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { useParams } from 'react-router-dom'
import { Row } from 'react-table'
import styled from 'styled-components'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { color } from '../../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../../components/Icons'
import { NButton } from '../../../../../components/NButton/NButton'
import { NCheckbox } from '../../../../../components/NCheckbox/NCheckbox'
import { NDetailHeader } from '../../../../../components/NDetailHeader'
import { NDivider } from '../../../../../components/NDivider'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { NTable } from '../../../../../components/NTable/NTable'
import { NTextInput } from '../../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../../components/NToast'
import { typography } from '../../../../../components/NTypography'
import { APIServices } from '../../../../../services/api'
import { ProfilesQueryKey } from '../../../../../services/api/endpoints/profiles'

const HeaderLabel = styled.div`
  ${typography('overline')};
  color: ${color('Neutral400')};
  display: flex;
  align-items: center;
  cursor: pointer;
`

const Label = styled('label')`
  ${typography('small-bold-text')};
  color: ${color('Neutral900')};
  display: flex;
  align-items: center;
  cursor: pointer;
`

const SaveBtn = styled(NButton)`
  min-width: 80px;
`

type TableDatum = {
  name: string
  displayName: string
  read: boolean
  create: boolean
  delete: boolean
  update: boolean
}

type ProfileDetailsProps = {
  children?: React.ReactNode
}

export function ProfileDetails({}: ProfileDetailsProps) {
  const { id } = useParams<{ id: string }>()
  const [searchTerm, setSearchTerm] = React.useState('')
  const [headerData, setHeaderData] = React.useState({
    read: false,
    create: false,
    update: false,
    delete: false,
  })
  const [tableData, setTableData] = React.useState<TableDatum[]>([])
  const queryClient = useQueryClient()

  const { isFetching: isLoadingProfilesObjects, data: profileObjectsData } = useQuery(
    [ProfilesQueryKey.getProfileObjects, { id }],
    APIServices.Profiles.getProfileObjects,
  )

  const { data: profileData } = useQuery([ProfilesQueryKey.getProfile, { id }], APIServices.Profiles.getProfile)

  React.useEffect(() => {
    // useEffect of onSuccess becuz onSuccess won't be called when get data from cache
    if (profileObjectsData?.data) {
      const tmpTableData = []
      for (const objName in profileObjectsData?.data) {
        tmpTableData.push({
          name: objName,
          displayName: objName,
          ...profileObjectsData?.data[objName],
        })
      }
      setTableData(tmpTableData)
    }
  }, [profileObjectsData])

  const { mutate: updateProfileObjects, isLoading: isUpdateProfilesObjects } = useMutation(
    APIServices.Profiles.updateProfileObjects,
    {
      onSuccess() {
        queryClient.invalidateQueries([ProfilesQueryKey.getProfileObjects, { id }])
        NToast.success({ title: 'Update succesful' })
      },
      onError: error => {
        NToast.error({ title: 'Update unsuccessful', subtitle: error.response?.data.message })
      },
    },
  )

  const handleCheckboxChange = (row: Row<TableDatum>, key: string) => (e: React.ChangeEvent<HTMLInputElement>) => {
    setTableData(prev => {
      return [...prev.slice(0, row.index), { ...row.original, [key]: e.target.checked }, ...prev.slice(row.index + 1)]
    })
  }

  const handleCheckboxChangeAll = (key: string) => (e: React.ChangeEvent<HTMLInputElement>) => {
    setHeaderData(prev => ({ ...prev, [key]: e.target.checked }))
    setTableData(prev => prev.map(i => ({ ...i, [key]: e.target.checked })))
  }

  const count = tableData.reduce(
    (a, i) => {
      return {
        read: i.read ? a.read + 1 : a.read,
        create: i.create ? a.create + 1 : a.create,
        delete: i.delete ? a.delete + 1 : a.delete,
        update: i.update ? a.update + 1 : a.update,
      }
    },
    { read: 0, create: 0, delete: 0, update: 0 },
  )

  const data = React.useMemo(() => {
    if (searchTerm) {
      return tableData.filter(i => i.name.includes(searchTerm.toLowerCase()))
    }
    return tableData
  }, [searchTerm, tableData])

  const submit = () => {
    updateProfileObjects({
      id,
      items: tableData.map(item => {
        return {
          objName: item.name,
          access: {
            read: item.read,
            create: item.create,
            update: item.update,
            delete: item.delete,
          },
        }
      }),
    })
  }

  return (
    <NContainer>
      <NDetailHeader
        breadcrumbs={[
          { path: DASHBOARD_ROUTE, label: 'Dashboard' },
          { path: `${DASHBOARD_ROUTE}/people`, label: 'People' },
          { path: `${DASHBOARD_ROUTE}/people/profiles`, label: 'Profiles' },
          { label: profileData?.data.name || '' },
        ]}>
        <SaveBtn loading={isUpdateProfilesObjects} onClick={submit} type="primary">
          Save
        </SaveBtn>
      </NDetailHeader>
      <NContainer.Title>{profileData?.data.name}</NContainer.Title>
      <NContainer.Content>
        <NTextInput
          className="input"
          left={<GlobalIcons.Search />}
          placeholder="Search"
          value={searchTerm}
          onChange={e => setSearchTerm(e.target.value)}
        />
        <NDivider size="xl" />
        <NTable
          isLoading={isLoadingProfilesObjects}
          data={data}
          columns={[
            { Header: 'Permission', accessor: 'displayName' },
            {
              Header: () => {
                return (
                  <HeaderLabel>
                    <NCheckbox
                      checked={headerData.read}
                      indeterminate={count.read > 0 && count.read < tableData.length}
                      onChange={handleCheckboxChangeAll('read')}
                    />
                    <NDivider vertical size="md" />
                    Read
                  </HeaderLabel>
                )
              },
              accessor: 'read',
              Cell: ({ value, row }: { value: boolean; row: Row<TableDatum> }) => {
                return (
                  <Label>
                    <NCheckbox checked={value} onChange={handleCheckboxChange(row, 'read')} />
                    <NDivider vertical size="md" />
                    Read
                  </Label>
                )
              },
            },
            {
              Header: () => {
                return (
                  <HeaderLabel>
                    <NCheckbox
                      checked={headerData.create}
                      indeterminate={count.create > 0 && count.create < tableData.length}
                      onChange={handleCheckboxChangeAll('create')}
                    />
                    <NDivider vertical size="md" />
                    Create
                  </HeaderLabel>
                )
              },
              accessor: 'create',
              Cell: ({ value, row }: { value: boolean; row: Row<TableDatum> }) => {
                return (
                  <Label>
                    <NCheckbox checked={value} onChange={handleCheckboxChange(row, 'create')} />
                    <NDivider vertical size="md" />
                    Create
                  </Label>
                )
              },
            },
            {
              Header: () => {
                return (
                  <HeaderLabel>
                    <NCheckbox
                      checked={headerData.update}
                      indeterminate={count.update > 0 && count.update < tableData.length}
                      onChange={handleCheckboxChangeAll('update')}
                    />
                    <NDivider vertical size="md" />
                    Update
                  </HeaderLabel>
                )
              },
              accessor: 'update',
              Cell: ({ value, row }: { value: boolean; row: Row<TableDatum> }) => {
                return (
                  <Label>
                    <NCheckbox checked={value} onChange={handleCheckboxChange(row, 'update')} />
                    <NDivider vertical size="md" />
                    Update
                  </Label>
                )
              },
            },
            {
              Header: () => {
                return (
                  <HeaderLabel>
                    <NCheckbox
                      checked={headerData.delete}
                      indeterminate={count.delete > 0 && count.delete < tableData.length}
                      onChange={handleCheckboxChangeAll('delete')}
                    />
                    <NDivider vertical size="md" />
                    Delete
                  </HeaderLabel>
                )
              },
              accessor: 'delete',
              Cell: ({ value, row }: { value: boolean; row: Row<TableDatum> }) => {
                return (
                  <Label>
                    <NCheckbox checked={value} onChange={handleCheckboxChange(row, 'delete')} />
                    <NDivider vertical size="md" />
                    Delete
                  </Label>
                )
              },
            },
          ]}
        />
        <NDivider size="md" />
      </NContainer.Content>
    </NContainer>
  )
}
