import { FC, useEffect, useMemo, useState } from 'react'
import { useQuery } from 'react-query'
import { useHistory } from 'react-router-dom'
import { classnames } from '../../../../../common/classnames'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { NButton } from '../../../../../components/NButton/NButton'
import { NRow } from '../../../../../components/NGrid/NGrid'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { NPagination } from '../../../../../components/NPagination'
import { NTable, NTableColumnType } from '../../../../../components/NTable/NTable'
import { NTableCollapsedCellClassName } from '../../../../../components/NTable/NTableStyledContainer'
import { NTableToolbar } from '../../../../../components/NTable/NTableToolbar'
import { useTableConfigs } from '../../../../../hooks/useTableConfigs'
import { APIServices } from '../../../../../services/api'
import { ProfilesQueryKey } from '../../../../../services/api/endpoints/profiles'
import { ProfileResponse } from '../../../../../services/api/models'
import { getTableData } from '../../../../../utils/table-utils'
import { ProfileFormModal } from './ProfileFormModal'
import { ProfileTableAction } from './ProfileTableAction'

const PAGE_SIZE = 10

export const ProfileListing: FC = () => {
  const [
    { sortBy, sortOrder, searchText, searchValue, page: currentPage },
    { onChangeSort, onChangePage, onChangeSearch },
  ] = useTableConfigs()
  const [selectedRows, setSelectedRow] = useState<ProfileResponse[]>([])
  const history = useHistory()
  const [showProfileModal, setShowProfileModal] = useState(false)
  const [selectedProfile, setSelectedProfile] = useState<ProfileResponse>()

  // remove data from modal
  useEffect(() => {
    if (!showProfileModal) {
      setSelectedProfile(undefined)
    }
  }, [showProfileModal])

  const columns: NTableColumnType<ProfileResponse> = useMemo(
    () => [
      { Header: 'Name', accessor: 'name', defaultCanSort: true },
      {
        Header: 'Created At',
        accessor: 'createdAt',
        Cell: ({ value }) => {
          if (!value) {
            return null
          }
          const date = new Date(value)
          return date.toLocaleString('vi')
        },
        defaultCanSort: true,
      },
      {
        Header: 'Last modified',
        accessor: 'updatedAt',
        Cell: ({ value }) => {
          if (!value) {
            return null
          }
          const date = new Date(value)
          return date.toLocaleString('vi')
        },
        defaultCanSort: true,
      },
      {
        accessor: 'id',
        Cell: ({ value, row }) => {
          return (
            <ProfileTableAction
              profileId={value}
              onEdit={() => {
                setSelectedProfile(row.original)
                setShowProfileModal(true)
              }}
            />
          )
        },
        id: classnames([NTableCollapsedCellClassName]),
      },
    ],
    [],
  )

  const { data: profilesData, isFetching: isLoadingProfiles } = useQuery(
    [
      ProfilesQueryKey.getProfiles,
      {
        searchText,
        limit: PAGE_SIZE,
        offset: (currentPage - 1) * PAGE_SIZE,
        sortBy,
        sortOrder,
      },
    ],
    APIServices.Profiles.getProfiles,
  )

  const rowOnSelect = (selected: ProfileResponse[]) => {
    setSelectedRow(selected)
  }

  const { pageData, pageInfo, totalPage } = useMemo(() => {
    return getTableData(profilesData)
  }, [profilesData])

  return (
    <NContainer>
      <NRow justify="space-between">
        <NContainer.Title>Profiles</NContainer.Title>
        <NButton onClick={() => setShowProfileModal(true)} type="primary">
          New Profile
        </NButton>
      </NRow>
      <NContainer.Description>{pageInfo.total} profiles</NContainer.Description>
      <NTableToolbar
        searchConfig={{
          value: searchValue,
          onChange: e => {
            onChangeSearch(e.target.value)
          },
        }}
        selectedConfig={{
          selected: selectedRows,
          onDeSelectAll: () => {},
          onDelete: selected => {
            console.log(`Delete ${selected.length} rows`)
          },
        }}
      />
      <NTable
        columns={columns}
        data={pageData}
        isLoading={isLoadingProfiles}
        rowSelectionConfig={{ onChange: rowOnSelect }}
        pageSize={PAGE_SIZE}
        pagination={<NPagination total={totalPage} current={currentPage} onChangePage={onChangePage} />}
        onClickRow={data => {
          history.push(`${DASHBOARD_ROUTE}/people/profiles/${data.id}`)
        }}
        defaultSortBy={sortBy}
        defaultSortOrder={sortOrder}
        onChangeSort={onChangeSort}
      />
      {showProfileModal && (
        <ProfileFormModal visible={showProfileModal} setVisible={setShowProfileModal} profileData={selectedProfile} />
      )}
    </NContainer>
  )
}
