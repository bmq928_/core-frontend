import { FC } from 'react'
import { useForm } from 'react-hook-form'
import { useMutation, useQueryClient } from 'react-query'
import styled from 'styled-components'
import { spacing } from '../../../../../components/GlobalStyle'
import { NModal } from '../../../../../components/NModal/NModal'
import { NTextInput } from '../../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../../components/NToast'
import { useValidateString } from '../../../../../hooks/useValidateString'
import { APIServices } from '../../../../../services/api'
import { ProfilesQueryKey } from '../../../../../services/api/endpoints/profiles'
import { CreateProfileDto, ProfileResponse } from '../../../../../services/api/models'

const ProfileForm = styled.form`
  padding: ${spacing('lg')} ${spacing('xl')};
`

type Props = {
  visible: boolean
  setVisible: (visible: boolean) => void
  profileData?: ProfileResponse
}

export const ProfileFormModal: FC<Props> = ({ visible, setVisible, profileData }) => {
  const { handleSubmit, register, formState } = useForm()
  const { validateFunction } = useValidateString()

  const queryClient = useQueryClient()

  const { mutate: createProfile, isLoading: isCreating } = useMutation(APIServices.Profiles.createProfile, {
    onSuccess() {
      queryClient.invalidateQueries([ProfilesQueryKey.getProfiles])
      setVisible(false)
    },
    onError: error => {
      NToast.error({ title: 'Create unsuccessful', subtitle: error.response?.data.message })
    },
  })

  const { mutate: updateProfile, isLoading: isUpdating } = useMutation(APIServices.Profiles.updateProfile, {
    onSuccess() {
      queryClient.invalidateQueries([ProfilesQueryKey.getProfiles])
      setVisible(false)
    },
    onError: error => {
      NToast.error({ title: 'Update unsuccessful', subtitle: error.response?.data.message })
    },
  })

  const onSubmit = (data: CreateProfileDto) => {
    profileData ? updateProfile({ id: profileData.id, ...data }) : createProfile(data)
  }

  return (
    <NModal visible={visible} setVisible={setVisible}>
      <NModal.Header title={`${profileData ? 'Edit' : 'Create'} Profile`} onClose={() => setVisible(false)} />
      <NModal.Body>
        <ProfileForm>
          <NTextInput
            required
            defaultValue={profileData?.name || ''}
            {...register('name', {
              validate: validateFunction,
            })}
            error={formState.errors.name?.message}
            label="Name"
            placeholder="Enter profile's name"
          />
        </ProfileForm>
      </NModal.Body>
      <NModal.Footer
        isLoading={isCreating || isUpdating}
        onCancel={() => setVisible(false)}
        onFinish={handleSubmit(onSubmit)}
      />
    </NModal>
  )
}
