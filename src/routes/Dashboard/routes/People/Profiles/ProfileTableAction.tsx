import { FC, useState } from 'react'
import { useMutation, useQueryClient } from 'react-query'
import { NTableActions } from '../../../../../components/NTable/NTableActions'
import { NToast } from '../../../../../components/NToast'
import { APIServices } from '../../../../../services/api'
import { ProfilesQueryKey } from '../../../../../services/api/endpoints/profiles'

type Props = {
  profileId: string
  onEdit: () => void
}

export const ProfileTableAction: FC<Props> = ({ profileId, onEdit }) => {
  const [showDropList, setShowDropList] = useState(false)
  const queryClient = useQueryClient()

  const { mutate: deleteProfile, isLoading: isDeleting } = useMutation(APIServices.Profiles.deleteProfile, {
    onSuccess() {
      queryClient.invalidateQueries([ProfilesQueryKey.getProfiles])
      setShowDropList(false)
    },
    onError: error => {
      NToast.error({ title: 'Delete unsuccessful', subtitle: error.response?.data.message })
    },
  })

  return (
    <NTableActions
      options={[
        {
          title: 'Edit',
          onClick: () => {
            onEdit()
            setShowDropList(false)
          },
        },
        {
          title: 'Delete',
          onClick: () => deleteProfile({ id: profileId }),
          isLoading: isDeleting,
        },
      ]}
      showDropList={showDropList}
      setShowDropList={setShowDropList}
    />
  )
}
