import { useMemo, useState } from 'react'
import { useQuery } from 'react-query'
import { useHistory } from 'react-router-dom'
import { classnames } from '../../../../../common/classnames'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { GlobalIcons } from '../../../../../components/Icons'
import { NButton } from '../../../../../components/NButton/NButton'
import { NRow } from '../../../../../components/NGrid/NGrid'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { NPagination } from '../../../../../components/NPagination'
import { NTable, NTableColumnType } from '../../../../../components/NTable/NTable'
import { NTableCollapsedCellClassName } from '../../../../../components/NTable/NTableStyledContainer'
import { NTableToolbar } from '../../../../../components/NTable/NTableToolbar'
import { useTableConfigs } from '../../../../../hooks/useTableConfigs'
import { APIServices } from '../../../../../services/api'
import { UsersQueryKey } from '../../../../../services/api/endpoints/users'
import { UserResponse } from '../../../../../services/api/models'
import { getTableData } from '../../../../../utils/table-utils'
import { InviteUserModal } from './InviteUserModal'
import { UserTableActions } from './UserTableActions'

type UsersProps = {}

const PAGE_SIZE = 10

const COLUMNS: NTableColumnType<UserResponse> = [
  { Header: 'Name', accessor: 'name', defaultCanSort: true },
  { Header: 'Profile id', accessor: 'profileId', defaultCanSort: true },
  { Header: 'SSO id', accessor: 'ssoId', defaultCanSort: true },
  {
    Header: 'Active',
    accessor: 'isActive',
    defaultCanSort: true,
    Cell: ({ value }) => {
      return value ? <GlobalIcons.Check /> : null
    },
  },
  {
    Header: 'Created At',
    accessor: 'createdAt',
    Cell: ({ value }) => {
      if (!value) {
        return null
      }
      const date = new Date(value)
      return date.toLocaleString('vi')
    },
    defaultCanSort: true,
  },
  {
    Header: 'Last modified',
    accessor: 'updatedAt',
    Cell: ({ value }) => {
      if (!value) {
        return null
      }
      const date = new Date(value)
      return date.toLocaleString('vi')
    },
    defaultCanSort: true,
  },
  {
    accessor: 'id',
    Cell: ({ value, row }) => {
      return <UserTableActions value={value} data={row.original} />
    },
    id: classnames([NTableCollapsedCellClassName]),
  },
]

export function Users({}: UsersProps) {
  const [
    { sortBy, sortOrder, searchText, searchValue, page: currentPage },
    { onChangeSort, onChangePage, onChangeSearch },
  ] = useTableConfigs()
  const [selectedRows, setSelectedRow] = useState<UserResponse[]>([])
  const history = useHistory()
  const [showInviteModal, setShowInviteModal] = useState(false)

  const { data: usersData, isLoading: isLoadingUsers } = useQuery(
    [
      UsersQueryKey.getUsers,
      {
        searchText: searchText,
        limit: PAGE_SIZE,
        offset: (currentPage - 1) * PAGE_SIZE,
        sortBy,
        sortOrder,
      },
    ],
    APIServices.Users.getUsers,
  )

  const rowOnSelect = (selected: UserResponse[]) => {
    setSelectedRow(selected)
  }

  const { pageData, pageInfo, totalPage } = useMemo(() => {
    return getTableData(usersData)
  }, [currentPage, usersData])

  return (
    <NContainer>
      <NRow justify="space-between">
        <NContainer.Title>Users</NContainer.Title>
        <NButton onClick={() => setShowInviteModal(true)} type="primary">
          Invite User
        </NButton>
      </NRow>
      <NContainer.Description>{pageInfo.total} users</NContainer.Description>
      <NTableToolbar
        searchConfig={{
          value: searchValue,
          onChange: e => {
            onChangeSearch(e.target.value)
          },
        }}
        selectedConfig={{
          selected: selectedRows,
          onDeSelectAll: () => {},
          onDelete: selected => {
            console.log(`Delete ${selected.length} rows`)
          },
        }}
      />
      <NTable
        columns={COLUMNS}
        data={pageData}
        isLoading={isLoadingUsers}
        rowSelectionConfig={{ onChange: rowOnSelect }}
        pageSize={PAGE_SIZE}
        pagination={<NPagination total={totalPage} current={currentPage} onChangePage={onChangePage} />}
        onClickRow={data => {
          history.push(`${DASHBOARD_ROUTE}/people/users/${data.id}`)
        }}
        defaultSortBy={sortBy}
        defaultSortOrder={sortOrder}
        onChangeSort={onChangeSort}
      />
      {showInviteModal && <InviteUserModal visible={showInviteModal} setVisible={setShowInviteModal} />}
    </NContainer>
  )
}
