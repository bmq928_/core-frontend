import { FC } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import styled from 'styled-components'
import { spacing } from '../../../../../components/GlobalStyle'
import { NDivider } from '../../../../../components/NDivider'
import { NModal } from '../../../../../components/NModal/NModal'
import { NSingleSelect } from '../../../../../components/NSelect/NSingleSelect'
import { NTextInput } from '../../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../../components/NToast'
import { useSearchText } from '../../../../../hooks/useSearchText'
import { useValidateString } from '../../../../../hooks/useValidateString'
import { APIServices } from '../../../../../services/api'
import { ProfilesQueryKey } from '../../../../../services/api/endpoints/profiles'
import { UsersQueryKey } from '../../../../../services/api/endpoints/users'
import { InviteUserDto } from '../../../../../services/api/models'

const InviteForm = styled.form`
  padding: ${spacing('lg')} ${spacing('xl')};
`

type Props = {
  visible: boolean
  setVisible: (visible: boolean) => void
}

export const InviteUserModal: FC<Props> = ({ visible, setVisible }) => {
  const { handleSubmit, register, formState, control } = useForm()
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText()
  const { validateFunction } = useValidateString()

  const queryClient = useQueryClient()

  const { mutate: inviteUser, isLoading } = useMutation(APIServices.Users.inviteUser, {
    onSuccess: async () => {
      await queryClient.invalidateQueries([UsersQueryKey.getUsers])
      NToast.success({ title: 'User invited' })
      setVisible(false)
    },
    onError: err => {
      NToast.error({
        title: `Failed to invite`,
        subtitle: err.response?.data.message,
      })
    },
  })

  const { data: profilesData, isFetching: isLoadingProfiles } = useQuery(
    [
      ProfilesQueryKey.getProfiles,
      {
        searchText: searchText,
      },
    ],
    APIServices.Profiles.getProfiles,
  )

  const onSubmit = (data: InviteUserDto) => {
    inviteUser(data)
  }

  return (
    <NModal visible={visible} setVisible={setVisible}>
      <NModal.Header title="Invite User" onClose={() => setVisible(false)} />
      <NModal.Body>
        <InviteForm>
          <NTextInput
            required
            defaultValue=""
            {...register('name', {
              validate: validateFunction,
            })}
            error={formState.errors.name?.message}
            label="Username"
            placeholder="Enter username"
          />
          <NDivider size="sm" />
          <Controller
            defaultValue=""
            control={control}
            name="profileId"
            render={({ field }) => (
              <NSingleSelect
                required
                isLoading={isLoadingProfiles}
                fullWidth
                isSearchable
                searchValue={searchValue}
                onSearchValueChange={handleSearchChange}
                options={profilesData?.data.data.map(profile => ({ label: profile.name, value: profile.id })) || []}
                placeholder="Search for profile"
                label="Profile"
                value={field.value}
                onValueChange={value => {
                  field.onChange(value)
                }}
              />
            )}
          />
        </InviteForm>
      </NModal.Body>
      <NModal.Footer isLoading={isLoading} onCancel={() => setVisible(false)} onFinish={handleSubmit(onSubmit)} />
    </NModal>
  )
}
