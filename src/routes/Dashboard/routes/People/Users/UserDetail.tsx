import { formatDistanceToNow } from 'date-fns'
import React, { FC } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { useParams } from 'react-router-dom'
import styled from 'styled-components'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { spacing } from '../../../../../components/GlobalStyle'
import { NButton } from '../../../../../components/NButton/NButton'
import { NDetailHeader } from '../../../../../components/NDetailHeader'
import { NDivider } from '../../../../../components/NDivider'
import { NColumn, NRow } from '../../../../../components/NGrid/NGrid'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { NSingleSelect } from '../../../../../components/NSelect/NSingleSelect'
import { NTextInput } from '../../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../../components/NToast'
import { SettingBoard } from '../../../../../components/SettingBoard'
import { useSearchText } from '../../../../../hooks/useSearchText'
import { useValidateString } from '../../../../../hooks/useValidateString'
import { APIServices } from '../../../../../services/api'
import { ProfilesQueryKey } from '../../../../../services/api/endpoints/profiles'
import { UsersQueryKey } from '../../../../../services/api/endpoints/users'
import { UpdateUserDto } from '../../../../../services/api/models'

const DetailForm = styled.form`
  padding: ${spacing('xl')};
`

const SaveBtn = styled(NButton)`
  width: 80px;
`

export const UserDetail: FC = () => {
  const { userId } = useParams<{ userId: string }>()
  const { register, handleSubmit, formState, control } = useForm()

  const queryClient = useQueryClient()
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText()
  const { validateFunction } = useValidateString()

  const { data: user } = useQuery(
    [
      UsersQueryKey.getUser,
      {
        userId,
      },
    ],
    APIServices.Users.getUser,
    {
      onError: err => {
        NToast.error({
          title: `Failed to get user`,
          subtitle: err.response?.data.message,
        })
      },
    },
  )

  const { mutate: updateUser, isLoading: isUpdating } = useMutation(APIServices.Users.updateUser, {
    onSuccess: async () => {
      await queryClient.invalidateQueries([UsersQueryKey.getUsers])
      await queryClient.invalidateQueries([UsersQueryKey.getUser, { userId }])
    },
    onError: err => {
      NToast.error({
        title: `Failed to update user`,
        subtitle: err.response?.data.message,
      })
    },
  })

  const { data: profilesData, isFetching: isLoadingProfiles } = useQuery(
    [
      ProfilesQueryKey.getProfiles,
      {
        searchText: searchText,
      },
    ],
    APIServices.Profiles.getProfiles,
  )

  const onSubmit = (data: UpdateUserDto) => {
    updateUser({
      userId: userId,
      ...data,
    })
  }

  return (
    <NContainer>
      <NDetailHeader
        breadcrumbs={[
          { path: DASHBOARD_ROUTE, label: 'Dashboard' },
          { path: `${DASHBOARD_ROUTE}/people`, label: 'People' },
          { path: `${DASHBOARD_ROUTE}/people/users`, label: 'Users' },
          { label: user?.data.name || '' },
        ]}
        label={
          user?.data.updatedAt &&
          `Last updated ${formatDistanceToNow(new Date(user?.data.updatedAt), { addSuffix: true })}`
        }>
        <SaveBtn loading={isUpdating} type="outline-2" onClick={handleSubmit(onSubmit)}>
          Save
        </SaveBtn>
      </NDetailHeader>
      <SettingBoard title="User Detail">
        {user?.data && (
          <DetailForm>
            <NTextInput
              required
              defaultValue={user?.data.name || ''}
              {...register('name', {
                validate: validateFunction,
              })}
              label="Name"
              placeholder="Enter name"
              error={formState.errors.name?.message}
            />
            <NDivider size="xl" />
            <NRow>
              <NColumn flex={1}>
                {user?.data.profileId && (
                  <Controller
                    defaultValue={user?.data.profileId}
                    control={control}
                    name="profileId"
                    render={({ field }) => (
                      <NSingleSelect
                        fullWidth
                        isSearchable
                        required
                        searchValue={searchValue}
                        onSearchValueChange={handleSearchChange}
                        options={
                          profilesData?.data.data.map(profile => ({
                            label: profile.name,
                            value: profile.id,
                          })) || []
                        }
                        isLoading={isLoadingProfiles}
                        placeholder="Search for profile"
                        label="Profile"
                        value={field.value}
                        onValueChange={value => {
                          field.onChange(value)
                        }}
                      />
                    )}
                  />
                )}
              </NColumn>
              <NDivider vertical size="xl" />
              <NColumn flex={1}>
                <NTextInput defaultValue={user?.data.ssoId || ''} label="SSO Id" disabled />
              </NColumn>
            </NRow>
          </DetailForm>
        )}
      </SettingBoard>
    </NContainer>
  )
}
