import React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import { DASHBOARD_ROUTE } from '../../../../common/constants'
import { NLayout, NSideBar } from '../../../../components/NLayout'
import { NMenu } from '../../../../components/NMenu/NMenu'
import { Profiles } from './Profiles/Profiles'
import { UserDetail } from './Users/UserDetail'
import { Users } from './Users/Users'

type PeopleProps = {}

export function People({}: PeopleProps) {
  return (
    <NLayout isRow>
      <NSideBar title="People">
        <NMenu type="primary">
          <NMenu.Item to={`${DASHBOARD_ROUTE}/people/users`}>Users</NMenu.Item>
          <NMenu.Item to={`${DASHBOARD_ROUTE}/people/profiles`}>Profiles</NMenu.Item>
        </NMenu>
      </NSideBar>
      <Switch>
        <Route path={`${DASHBOARD_ROUTE}/people/profiles`} component={Profiles} />
        <Route path={`${DASHBOARD_ROUTE}/people/users/:userId`} component={UserDetail} />
        <Route path={`${DASHBOARD_ROUTE}/people/users`} component={Users} />
        <Redirect to={`${DASHBOARD_ROUTE}/people/users`} />
      </Switch>
    </NLayout>
  )
}
