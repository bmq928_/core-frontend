import React, { FC, useState } from 'react'
import { useQuery } from 'react-query'
import { useHistory } from 'react-router-dom'
import { classnames } from '../../../../../common/classnames'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { GlobalIcons } from '../../../../../components/Icons'
import { NDetailHeader } from '../../../../../components/NDetailHeader'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { NPagination } from '../../../../../components/NPagination'
import { NTable, NTableColumnType } from '../../../../../components/NTable/NTable'
import { NTableCollapsedCellClassName } from '../../../../../components/NTable/NTableStyledContainer'
import { NTableToolbar } from '../../../../../components/NTable/NTableToolbar'
import { useTableConfigs } from '../../../../../hooks/useTableConfigs'
import { APIServices } from '../../../../../services/api'
import { CurrenciesQueryKey } from '../../../../../services/api/endpoints/currencies'
import { OrganizationCurrencyResponse } from '../../../../../services/api/models'
import { getTableData } from '../../../../../utils/table-utils'
import { CurrencyTableActions } from '../components/CurrencyTableActions'

const PAGE_SIZE = 10

const COLUMNS: NTableColumnType<OrganizationCurrencyResponse> = [
  { Header: 'Currency', accessor: 'displayName', defaultCanSort: true },
  { Header: 'Code', accessor: 'currencyCode', defaultCanSort: true },
  {
    Header: 'Active',
    accessor: 'isActive',
    defaultCanSort: true,
    Cell: ({ value }) => {
      return value ? <GlobalIcons.Check /> : null
    },
  },
  {
    Header: 'Rate',
    accessor: 'rate',
    defaultCanSort: true,
    Cell: ({ value }) => {
      return value || 'Not set'
    },
  },
  {
    Header: 'Created at',
    accessor: 'createdAt',
    Cell: ({ value }) => {
      if (!value) {
        return null
      }
      const date = new Date(value)
      return date.toLocaleString('vi')
    },
    defaultCanSort: true,
  },
  {
    Header: 'Last modified',
    accessor: 'updatedAt',
    Cell: ({ value }) => {
      if (!value) {
        return null
      }
      const date = new Date(value)
      return date.toLocaleString('vi')
    },
    defaultCanSort: true,
  },
  {
    accessor: 'currencyCode',
    Cell: ({ value, row }) => {
      return row.original.isActive ? <CurrencyTableActions currencyCode={value} /> : null
    },
    id: classnames([NTableCollapsedCellClassName]),
  },
]

export const OrgCurrency: FC = () => {
  const history = useHistory()

  const [selectedRows, setSelectedRow] = useState<OrganizationCurrencyResponse[]>([])
  const [
    { sortBy, sortOrder, searchText, searchValue, page: currentPage },
    { onChangeSort, onChangePage, onChangeSearch },
  ] = useTableConfigs()

  const { data: currenciesData, isLoading: isLoadingCurrencies } = useQuery(
    [
      CurrenciesQueryKey.getCurrenciesList,
      { limit: PAGE_SIZE, offset: (currentPage - 1) * PAGE_SIZE, searchText, sortBy, sortOrder },
    ],
    APIServices.Currencies.getCurrenciesList,
    { keepPreviousData: true },
  )

  const { pageData, totalPage } = React.useMemo(() => {
    return getTableData(currenciesData)
  }, [currenciesData])

  return (
    <NContainer>
      <NDetailHeader
        breadcrumbs={[{ label: 'Organization', path: `${DASHBOARD_ROUTE}/organization` }, { label: 'Currency' }]}
      />
      <NContainer.Title>Currency</NContainer.Title>
      <NContainer.Description>
        Use this page to define all the currencies used by your organization. Corporate Currency should be set to the
        currency in which your corporate headquarters reports revenue. If you designate a different currency as
        corporate, all conversion rates will be modified to reflect the change.
      </NContainer.Description>
      <NTableToolbar
        searchConfig={{
          value: searchValue,
          onChange: e => {
            onChangeSearch(e.target.value)
          },
        }}
        selectedConfig={{
          selected: selectedRows,
          onDeSelectAll: () => {},
          onDelete: selected => {
            console.log(`Delete ${selected.length} rows`)
          },
        }}
      />
      <NTable
        isLoading={isLoadingCurrencies}
        columns={COLUMNS}
        data={pageData}
        rowSelectionConfig={{ onChange: selected => setSelectedRow(selected) }}
        defaultSortBy={sortBy}
        defaultSortOrder={sortOrder}
        onChangeSort={onChangeSort}
        pageSize={PAGE_SIZE}
        pagination={<NPagination total={totalPage} current={currentPage} onChangePage={onChangePage} />}
        onClickRow={data => history.push(`${DASHBOARD_ROUTE}/organization/currency/${data.currencyCode}`)}
      />
    </NContainer>
  )
}
