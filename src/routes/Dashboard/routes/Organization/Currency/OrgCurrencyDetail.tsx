import React, { FC, useState } from 'react'
import { useQuery } from 'react-query'
import { useParams } from 'react-router-dom'
import styled, { useTheme } from 'styled-components'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { color, fontSize, spacing } from '../../../../../components/GlobalStyle'
import { NButton } from '../../../../../components/NButton/NButton'
import { NDetailHeader } from '../../../../../components/NDetailHeader'
import { NDivider } from '../../../../../components/NDivider'
import { NColumn, NRow } from '../../../../../components/NGrid/NGrid'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { NSpinner } from '../../../../../components/NSpinner/NSpinner'
import { typography } from '../../../../../components/NTypography'
import { SettingBoard } from '../../../../../components/SettingBoard'
import { APIServices } from '../../../../../services/api'
import { CurrenciesQueryKey } from '../../../../../services/api/endpoints/currencies'
import { OrgCurrencyRateHistory } from '../components/OrgCurrencyRateHistory'
import { UpdateCurrencyModal } from '../components/UpdateCurrencyModal'
import { UpdateCurrencyRateModal } from '../components/UpdateCurrencyRateModal'

const BoardWrapper = styled.div`
  display: grid;
  padding: ${spacing('xl')} ${spacing('xs')};
  grid-template-columns: repeat(2, 1fr);
  grid-column-gap: ${spacing('xl')};
`

const FieldWrapper = styled.div`
  padding: ${spacing(14)} ${spacing('md')};
`

const FieldLabel = styled.p`
  color: ${color('Neutral700')};
  font-size: ${fontSize('xs')};
  line-height: ${fontSize('xs')};
  font-weight: bold;
`

const FieldValue = styled.p`
  ${typography('small-bold-text')}
  color: ${color('Neutral400')};
`

const LoadingWrapper = styled.div`
  padding: ${spacing('xl')};
  display: flex;
  justify-content: center;
`

type FieldItemProps = {
  label: string
  value?: string | number
}

const FieldItem: FC<FieldItemProps> = ({ label, value }) => {
  return (
    <FieldWrapper>
      <FieldLabel>{label}</FieldLabel>
      <NDivider size="xs" />
      <FieldValue>{value}</FieldValue>
    </FieldWrapper>
  )
}

export const OrgCurrencyDetail: FC = () => {
  const theme = useTheme()
  const { currencyCode } = useParams<{ currencyCode: string }>()
  const [showUpdateModal, setShowUpdateModal] = useState(false)
  const [showUpdateRateModal, setShowUpdateRateModal] = useState(false)

  const { data: currency, isLoading: isLoadingCurrency } = useQuery(
    [CurrenciesQueryKey.getCurrency, { currencyCode }],
    APIServices.Currencies.getCurrency,
  )

  return (
    <NContainer>
      <NDetailHeader
        breadcrumbs={[
          { label: 'Organization', path: `${DASHBOARD_ROUTE}/organization` },
          { label: 'Currency', path: `${DASHBOARD_ROUTE}/organization/currency` },
          { label: currency?.data.currencyCode || '' },
        ]}
      />
      <NContainer.Title>{currency?.data.name}</NContainer.Title>
      <NRow>
        <NColumn flex={4} style={{ padding: '1px' }}>
          <SettingBoard
            title={currency?.data.displayName}
            action={
              <NButton size="small" onClick={() => setShowUpdateModal(true)}>
                Edit
              </NButton>
            }>
            {isLoadingCurrency ? (
              <LoadingWrapper>
                <NSpinner size={22} strokeWidth={2} color={color('Primary700')({ theme })} />
              </LoadingWrapper>
            ) : (
              <BoardWrapper>
                <FieldItem label="Name" value={currency?.data.name} />
                <FieldItem label="Display name" value={currency?.data.displayName} />
                <FieldItem label="Code" value={currency?.data.currencyCode} />
                <FieldItem label="Status" value={currency?.data.isActive ? 'Active' : 'Inactive'} />
                <FieldItem label="Created" value={currency?.data.createdAt} />
                <FieldItem label="Modified" value={currency?.data.updatedAt} />
              </BoardWrapper>
            )}
          </SettingBoard>
          <SettingBoard
            title="Rate"
            action={
              <NButton size="small" onClick={() => setShowUpdateRateModal(true)}>
                Change
              </NButton>
            }>
            <BoardWrapper>
              <FieldItem label="Rate" value={currency?.data.rate || 'Not set'} />
              <FieldItem label="Modified" value={currency?.data.updatedAt} />
            </BoardWrapper>
          </SettingBoard>
          {showUpdateModal && currency?.data && (
            <UpdateCurrencyModal visible={showUpdateModal} setVisible={setShowUpdateModal} currency={currency?.data} />
          )}
          {showUpdateRateModal && currency?.data && (
            <UpdateCurrencyRateModal
              visible={showUpdateRateModal}
              setVisible={setShowUpdateRateModal}
              currency={currency?.data}
            />
          )}
        </NColumn>
        <NColumn flex={1}>
          <OrgCurrencyRateHistory currencyCode={currencyCode} />
        </NColumn>
      </NRow>
    </NContainer>
  )
}
