import { formatDistanceToNow } from 'date-fns'
import React, { FC, useMemo } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import styled, { useTheme } from 'styled-components'
import { color, spacing } from '../../../../components/GlobalStyle'
import { NButton } from '../../../../components/NButton/NButton'
import { NDetailHeader } from '../../../../components/NDetailHeader'
import { NDivider } from '../../../../components/NDivider'
import { NColumn, NRow } from '../../../../components/NGrid/NGrid'
import { NContainer } from '../../../../components/NLayout/NContainer'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { NSpinner } from '../../../../components/NSpinner/NSpinner'
import { NTextArea } from '../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../components/NToast'
import { SettingBoard } from '../../../../components/SettingBoard'
import { useSearchText } from '../../../../hooks/useSearchText'
import { useValidateString } from '../../../../hooks/useValidateString'
import { APIServices } from '../../../../services/api'
import { OrganizationQueryKey } from '../../../../services/api/endpoints/organization'
import { UpdateOrgDto } from '../../../../services/api/models'
import { emailRegex } from '../../../../utils/regex'

const SaveBtn = styled(NButton)`
  width: 80px;
`

const Section = styled.div`
  padding: ${spacing('xl')};
`

const DescTextArea = styled(NTextArea)`
  width: 100%;
`

const LoadingContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: ${spacing('xl')};
`

export const OrgDetail: FC = () => {
  const { t } = useTranslation()
  const theme = useTheme()
  const queryClient = useQueryClient()
  const { validateFunction } = useValidateString()

  const { data: organization, isLoading: isLoadingOrg } = useQuery(
    OrganizationQueryKey.getOrganization,
    APIServices.Organization.getOrganization,
  )

  const { data: locales, isLoading: isLoadingLocales } = useQuery(
    OrganizationQueryKey.getLocales,
    APIServices.Organization.getLocales,
    {
      select: locales => {
        const keys = Object.keys(locales.data)
        return keys.map(key => {
          return {
            value: key,
            label: locales.data[key],
          }
        })
      },
    },
  )

  const { mutate: updateOrg, isLoading: isUpdatingOrg } = useMutation(APIServices.Organization.updateOrganization, {
    onSuccess: () => {
      queryClient.invalidateQueries(OrganizationQueryKey.getOrganization)
    },
    onError: error => {
      NToast.error({ title: 'Update organization unsuccessful', subtitle: error.response?.data.message })
    },
  })

  const { handleSubmit, register, formState, control } = useForm()

  const onSubmit = (formData: UpdateOrgDto) => {
    updateOrg(formData)
  }

  //local filter for 2 different dropdown

  const [
    { searchValue: searchLocaleCurrencyValue, searchText: searchLocaleCurrencyText },
    handleSearchCurrencyLocale,
  ] = useSearchText('')
  const [{ searchValue: searchLocaleValue, searchText: searchLocaleText }, handleSearchLocale] = useSearchText('')

  const filteredDefaultLocales = useMemo(() => {
    if (locales) {
      return searchLocaleText
        ? locales.filter(
            locale =>
              (locale.label as string).toLocaleLowerCase().includes(searchLocaleText.toLocaleLowerCase()) ||
              locale.value.toLocaleLowerCase().includes(searchLocaleText.toLocaleLowerCase()),
          )
        : locales
    }
    return []
  }, [searchLocaleText, locales])

  const filteredCurrencyLocales = useMemo(() => {
    if (locales) {
      return searchLocaleCurrencyText
        ? locales.filter(
            locale =>
              (locale.label as string).toLocaleLowerCase().includes(searchLocaleCurrencyText.toLocaleLowerCase()) ||
              locale.value.toLocaleLowerCase().includes(searchLocaleCurrencyText.toLocaleLowerCase()),
          )
        : locales
    }
    return []
  }, [searchLocaleCurrencyText, locales])

  return (
    <NContainer>
      <NDetailHeader
        breadcrumbs={[{ label: 'Organization' }]}
        label={`Last updated ${
          organization?.data.updatedAt &&
          formatDistanceToNow(new Date(organization?.data.updatedAt), { addSuffix: true })
        }`}>
        <SaveBtn type="primary" onClick={handleSubmit(onSubmit)} loading={isUpdatingOrg}>
          Save
        </SaveBtn>
      </NDetailHeader>
      <NContainer.Title>General</NContainer.Title>
      <SettingBoard title="Organization">
        {isLoadingOrg ? (
          <LoadingContainer>
            <NSpinner size={24} strokeWidth={3} color={color('Primary700')({ theme })} />
          </LoadingContainer>
        ) : (
          <form>
            <Section>
              <NRow>
                <NColumn flex={1}>
                  <NTextInput
                    defaultValue={organization?.data.name}
                    required
                    label="Organization name"
                    placeholder="Enter organization name"
                    error={formState.errors.name?.message}
                    {...register('name', {
                      validate: validateFunction,
                    })}
                  />
                </NColumn>
                <NDivider vertical size="xl" />
                <NColumn flex={1}>
                  <NTextInput
                    defaultValue={organization?.data.contactPerson || ''}
                    label="Contact person"
                    placeholder="Enter contact person name"
                    {...register('contactPerson')}
                  />
                </NColumn>
              </NRow>
              <NDivider size="xl" />
              <NRow>
                <NColumn flex={1}>
                  <NTextInput
                    defaultValue={organization?.data.email || ''}
                    label="Email"
                    placeholder="Enter email"
                    error={formState.errors.email?.message}
                    {...register('email', {
                      pattern: {
                        value: emailRegex,
                        message: t('common.error.invalidEmail'),
                      },
                    })}
                  />
                </NColumn>
                <NDivider vertical size="xl" />
                <NColumn flex={1}>
                  <NTextInput
                    defaultValue={organization?.data.phoneNumber || ''}
                    label="Phone number"
                    placeholder="Enter phone number"
                    {...register('phoneNumber')}
                  />
                </NColumn>
              </NRow>
              <NDivider size="xl" />
              <NRow>
                <NColumn flex={1}>
                  <NTextInput
                    defaultValue={organization?.data.url || ''}
                    label="URL"
                    placeholder="Enter organization url"
                    {...register('url')}
                  />
                </NColumn>
                <NDivider vertical size="xl" />
                <NColumn flex={1}>
                  <NTextInput
                    disabled={true}
                    label="Default currency"
                    value={organization?.data.defaultCurrency.name}
                  />
                </NColumn>
              </NRow>
              <NDivider size="xl" />
              <NRow>
                <NColumn flex={1}>
                  <Controller
                    defaultValue={organization?.data.defaultLocale || ''}
                    control={control}
                    name="defaultLocale"
                    render={({ field }) => (
                      <NSingleSelect
                        fullWidth
                        isLoading={isLoadingLocales}
                        value={field.value}
                        onValueChange={field.onChange}
                        label="Default locale"
                        options={filteredDefaultLocales}
                        placeholder="Select default locale"
                        isSearchable
                        onSearchValueChange={handleSearchLocale}
                        searchValue={searchLocaleValue}
                      />
                    )}
                  />
                </NColumn>
                <NDivider vertical size="xl" />
                <NColumn flex={1}>
                  <Controller
                    defaultValue={organization?.data.currencyLocale || ''}
                    control={control}
                    name="currencyLocale"
                    render={({ field }) => (
                      <NSingleSelect
                        isSearchable
                        onSearchValueChange={handleSearchCurrencyLocale}
                        searchValue={searchLocaleCurrencyValue}
                        fullWidth
                        isLoading={isLoadingLocales}
                        value={field.value}
                        onValueChange={field.onChange}
                        label="Currency locale"
                        options={filteredCurrencyLocales}
                        placeholder="Select currency locale"
                      />
                    )}
                  />
                </NColumn>
              </NRow>
              <NDivider size="xl" />
              <DescTextArea
                defaultValue={organization?.data.description || ''}
                label="Description"
                rows={4}
                placeholder="Enter description"
                {...register('description')}
              />
            </Section>
          </form>
        )}
      </SettingBoard>
    </NContainer>
  )
}
