import { FC } from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import { DASHBOARD_ROUTE } from '../../../../common/constants'
import { NLayout, NSideBar } from '../../../../components/NLayout'
import { NMenu } from '../../../../components/NMenu/NMenu'
import { OrgCurrency } from './Currency/OrgCurrency'
import { OrgCurrencyDetail } from './Currency/OrgCurrencyDetail'
import { OrgDetail } from './OrgDetail'
import { PickListDetail } from './PickLists/PickListDetail'
import { PickLists } from './PickLists/PickLists'

export const Organization: FC = () => {
  return (
    <NLayout isRow>
      <NSideBar title="Organization">
        <NMenu type="primary">
          <NMenu.Item to={`${DASHBOARD_ROUTE}/organization/general`}>General</NMenu.Item>
          <NMenu.Item to={`${DASHBOARD_ROUTE}/organization/currency`}>Currency</NMenu.Item>
          <NMenu.Item to={`${DASHBOARD_ROUTE}/organization/pick-lists`}>Pick lists</NMenu.Item>
          {/* <NMenu.Item to={`${DASHBOARD_ROUTE}/organization/system`}>System</NMenu.Item> */}
        </NMenu>
      </NSideBar>
      <Switch>
        <Route path={`${DASHBOARD_ROUTE}/organization/general`}>
          <OrgDetail />
        </Route>
        <Route path={`${DASHBOARD_ROUTE}/organization/currency/:currencyCode`}>
          <OrgCurrencyDetail />
        </Route>
        <Route path={`${DASHBOARD_ROUTE}/organization/currency`}>
          <OrgCurrency />
        </Route>
        <Route path={`${DASHBOARD_ROUTE}/organization/pick-lists/:pickListName`}>
          <PickListDetail />
        </Route>
        <Route path={`${DASHBOARD_ROUTE}/organization/pick-lists`}>
          <PickLists />
        </Route>
        <Redirect to={`${DASHBOARD_ROUTE}/organization/general`} />
      </Switch>
    </NLayout>
  )
}
