import React, { FC, useState } from 'react'
import { useQuery } from 'react-query'
import { useHistory } from 'react-router-dom'
import { classnames } from '../../../../../common/classnames'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { GlobalIcons } from '../../../../../components/Icons'
import { NButton } from '../../../../../components/NButton/NButton'
import { NDetailHeader } from '../../../../../components/NDetailHeader'
import { NRow } from '../../../../../components/NGrid/NGrid'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { NPagination } from '../../../../../components/NPagination'
import { NTable, NTableColumnType } from '../../../../../components/NTable/NTable'
import { NTableCollapsedCellClassName } from '../../../../../components/NTable/NTableStyledContainer'
import { NTableToolbar } from '../../../../../components/NTable/NTableToolbar'
import { useTableConfigs } from '../../../../../hooks/useTableConfigs'
import { APIServices } from '../../../../../services/api'
import { PickListsQueryKey } from '../../../../../services/api/endpoints/pick-lists'
import { PickListResponse } from '../../../../../services/api/models'
import { getTableData } from '../../../../../utils/table-utils'
import { PickListModal } from './components/PickListModal'
import { PickListTableActions } from './components/PickListTableActions'

const PAGE_SIZE = 10

const COLUMNS: NTableColumnType<PickListResponse> = [
  { Header: 'Display name', accessor: 'displayName', defaultCanSort: true },
  { Header: 'Name', accessor: 'name', defaultCanSort: true },
  { Header: 'Description', accessor: 'description', defaultCanSort: true },
  {
    Header: 'Active',
    accessor: 'isActive',
    defaultCanSort: true,
    Cell: ({ value }) => {
      return value ? <GlobalIcons.Check /> : null
    },
  },
  {
    Header: 'Is system',
    accessor: 'isSystemDefault',
    defaultCanSort: true,
    Cell: ({ value }) => {
      return value ? <GlobalIcons.Check /> : null
    },
  },
  {
    Header: 'Created at',
    accessor: 'createdAt',
    Cell: ({ value }) => {
      if (!value) {
        return null
      }
      const date = new Date(value)
      return date.toLocaleString('vi')
    },
    defaultCanSort: true,
  },
  {
    Header: 'Last modified',
    accessor: 'updatedAt',
    Cell: ({ value }) => {
      if (!value) {
        return null
      }
      const date = new Date(value)
      return date.toLocaleString('vi')
    },
    defaultCanSort: true,
  },
  {
    accessor: 'id',
    Cell: ({ row }) => {
      return <PickListTableActions pickListName={row.original.name} data={row.original} />
    },
    id: classnames([NTableCollapsedCellClassName]),
  },
]

export const PickLists: FC = () => {
  const history = useHistory()
  const [selectedRows, setSelectedRow] = useState<PickListResponse[]>([])
  const [
    { sortBy, sortOrder, searchText, searchValue, page: currentPage },
    { onChangeSort, onChangePage, onChangeSearch },
  ] = useTableConfigs()
  const [showCreate, setShowCreate] = useState(false)

  const { data: pickLists, isLoading } = useQuery(
    [
      PickListsQueryKey.getAllPickLists,
      { limit: PAGE_SIZE, offset: (currentPage - 1) * PAGE_SIZE, searchText, sortBy, sortOrder },
    ],
    APIServices.PickLists.getAllPickLists,
    { keepPreviousData: true },
  )

  const { pageData, totalPage } = React.useMemo(() => {
    return getTableData(pickLists)
  }, [pickLists])

  return (
    <NContainer>
      {showCreate && <PickListModal visible={showCreate} setVisible={setShowCreate} />}
      <NDetailHeader
        breadcrumbs={[{ label: 'Organization', path: `${DASHBOARD_ROUTE}/organization` }, { label: 'Pick lists' }]}
      />
      <NRow justify="space-between">
        <NContainer.Title>Pick lists</NContainer.Title>
        <NButton type="primary" onClick={() => setShowCreate(true)}>
          Create Pick list
        </NButton>
      </NRow>
      <NTableToolbar
        searchConfig={{
          value: searchValue,
          onChange: e => {
            onChangeSearch(e.target.value)
          },
        }}
        selectedConfig={{
          selected: selectedRows,
          onDeSelectAll: () => {},
          onDelete: selected => {
            console.log(`Delete ${selected.length} rows`)
          },
        }}
      />
      <NTable
        isLoading={isLoading}
        columns={COLUMNS}
        data={pageData}
        rowSelectionConfig={{ onChange: selected => setSelectedRow(selected) }}
        defaultSortBy={sortBy}
        defaultSortOrder={sortOrder}
        onChangeSort={onChangeSort}
        pageSize={PAGE_SIZE}
        pagination={<NPagination total={totalPage} current={currentPage} onChangePage={onChangePage} />}
        onClickRow={data => history.push(`${DASHBOARD_ROUTE}/organization/pick-lists/${data.name}`)}
      />
    </NContainer>
  )
}
