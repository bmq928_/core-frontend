import { FC, useState } from 'react'
import { useMutation, useQueryClient } from 'react-query'
import { NTableActions } from '../../../../../../components/NTable/NTableActions'
import { NToast } from '../../../../../../components/NToast'
import { APIServices } from '../../../../../../services/api'
import { PickListsQueryKey } from '../../../../../../services/api/endpoints/pick-lists'
import { PickListResponse } from '../../../../../../services/api/models'

type Props = {
  pickListName: string
  data: PickListResponse
}

export const PickListTableActions: FC<Props> = ({ pickListName, data }) => {
  const [showDropList, setShowDropList] = useState(false)
  const queryClient = useQueryClient()

  const { mutate: activatePickList, isLoading: isActivating } = useMutation(APIServices.PickLists.activatePickList, {
    onSuccess: async () => {
      await queryClient.invalidateQueries([PickListsQueryKey.getAllPickLists])
      await queryClient.invalidateQueries([PickListsQueryKey.getPickList, { name: pickListName }])
      setShowDropList(false)
    },
    onError: err => {
      NToast.error({
        title: `Failed to activate pick list`,
        subtitle: err.response?.data.message,
      })
    },
  })

  const { mutate: deactivatePickList, isLoading: isDeactivating } = useMutation(
    APIServices.PickLists.deactivatePickList,
    {
      onSuccess: async () => {
        await queryClient.invalidateQueries([PickListsQueryKey.getAllPickLists])
        await queryClient.invalidateQueries([PickListsQueryKey.getPickList, { name: pickListName }])
        setShowDropList(false)
      },
      onError: err => {
        NToast.error({
          title: `Failed to deactivate pick list`,
          subtitle: err.response?.data.message,
        })
      },
    },
  )

  return (
    <NTableActions
      options={[
        {
          title: data.isActive ? 'Deactivate' : 'Activate',
          isLoading: isActivating || isDeactivating,
          onClick: () =>
            data.isActive ? deactivatePickList({ name: pickListName }) : activatePickList({ name: pickListName }),
        },
      ]}
      showDropList={showDropList}
      setShowDropList={setShowDropList}
    />
  )
}
