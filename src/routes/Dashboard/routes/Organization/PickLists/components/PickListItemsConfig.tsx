import { FC } from 'react'
import { Controller, useFieldArray, useForm } from 'react-hook-form'
import { useMutation, useQueryClient } from 'react-query'
import styled, { useTheme } from 'styled-components'
import { color, spacing } from '../../../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../../../components/Icons'
import { NButton } from '../../../../../../components/NButton/NButton'
import { NDivider } from '../../../../../../components/NDivider'
import { NColumn, NRow } from '../../../../../../components/NGrid/NGrid'
import { NSpinner } from '../../../../../../components/NSpinner/NSpinner'
import { NTextInput } from '../../../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../../../components/NToast'
import { SettingBoard } from '../../../../../../components/SettingBoard'
import { APIServices } from '../../../../../../services/api'
import { PickListsQueryKey } from '../../../../../../services/api/endpoints/pick-lists'
import { MutatePickListItemDto, PickListItemResponse, PickListResponse } from '../../../../../../services/api/models'

const BoardWrapper = styled.div`
  padding: ${spacing('xl')} ${spacing('xl')};
  position: relative;
`

const StyledRow = styled(NRow)`
  margin-bottom: ${spacing('md')};
`

const LoadingMask = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  opacity: 0.8;
  background-color: ${color('white')};
  top: 0;
  left: 0;
  display: flex;
  justify-content: center;
  align-items: center;
`

type UpdatePickListItemsForm = {
  items: PickListItemResponse[]
}

type Props = {
  data: PickListResponse
}

export const PickListItemsConfig: FC<Props> = ({ data }) => {
  const { control, handleSubmit, reset } = useForm<UpdatePickListItemsForm>({
    defaultValues: {
      items: data.items,
    },
  })
  const { fields, append, remove } = useFieldArray({ control, name: 'items' })
  const queryClient = useQueryClient()
  const theme = useTheme()

  const { mutate: updatePickList, isLoading: isUpdating } = useMutation(APIServices.PickLists.updatePickList, {
    onSuccess: res => {
      reset({ items: res.data.items })
      NToast.success({
        title: 'Pick list items updated',
      })
      queryClient.invalidateQueries(PickListsQueryKey.getAllPickLists)
      queryClient.invalidateQueries([PickListsQueryKey.getPickList, { name: data.name }])
    },
    onError: error => {
      NToast.error({
        title: 'Update pick list unsuccessful',
        subtitle: error.response?.data.message,
      })
    },
  })

  const onSubmit = (formData: UpdatePickListItemsForm) => {
    const itemsData = transformPayload(formData.items, data.items)
    itemsData.length > 0 && updatePickList({ name: data.name, items: itemsData })
  }

  return (
    <SettingBoard
      title="Pick list items"
      action={
        <NButton disabled={isUpdating} size="small" onClick={handleSubmit(onSubmit)}>
          Save
        </NButton>
      }>
      <BoardWrapper>
        {isUpdating && (
          <LoadingMask>
            <NSpinner size={30} strokeWidth={2} color={color('Primary900')({ theme })} />
          </LoadingMask>
        )}
        {fields.map((field, fieldIndex) => (
          <StyledRow key={field.id} align="flex-end">
            <NColumn>
              <Controller
                name={`items.${fieldIndex}.value`}
                control={control}
                render={({ field, fieldState }) => (
                  <NTextInput
                    value={field.value || ''}
                    onChange={e => field.onChange(e.target.value)}
                    label={`Value ${fieldIndex}`}
                    required
                    error={fieldState.error?.message}
                  />
                )}
                rules={{ required: { value: true, message: 'Required!' } }}
              />
            </NColumn>
            <NDivider vertical size="xs" />
            <NButton
              type="ghost"
              icon={<GlobalIcons.Trash />}
              onClick={() => {
                remove(fieldIndex)
              }}
            />
          </StyledRow>
        ))}
        <NButton icon={<GlobalIcons.Add />} type="primary" onClick={() => append({ value: '' })} />
      </BoardWrapper>
    </SettingBoard>
  )
}

const transformPayload = (newItems: PickListItemResponse[], originalItems: PickListItemResponse[]) => {
  // create hash object
  const { original, deleted } = originalItems.reduce(
    (a, item) => {
      a.original[item.id] = item.value
      a.deleted[item.id] = { action: 'DELETE', id: item.id }
      return a
    },
    { original: {} as Record<string, string>, deleted: {} as Record<string, any> },
  )

  return [
    ...newItems.reduce((a, item) => {
      if (!item.id) {
        a.push({ action: 'CREATE', value: item.value })
        return a
      }
      if (original[item.id] && original[item.id] !== item.value) {
        a.push({ action: 'UPDATE', value: item.value, id: item.id })
      }
      // delete existed items in new arrays from deleted hash
      delete deleted[item.id]
      return a
    }, [] as MutatePickListItemDto[]),
    // construct deleted array from hash deleted
    ...Object.values(deleted),
  ]
}
