import { FC } from 'react'
import { useForm } from 'react-hook-form'
import { useMutation, useQueryClient } from 'react-query'
import styled from 'styled-components'
import { spacing } from '../../../../../../components/GlobalStyle'
import { NDivider } from '../../../../../../components/NDivider'
import { NColumn, NRow } from '../../../../../../components/NGrid/NGrid'
import { NModal } from '../../../../../../components/NModal/NModal'
import { NTextArea } from '../../../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../../../components/NToast'
import { useValidateString } from '../../../../../../hooks/useValidateString'
import { APIServices } from '../../../../../../services/api'
import { PickListsQueryKey } from '../../../../../../services/api/endpoints/pick-lists'
import { CreatePickListDto, PickListResponse } from '../../../../../../services/api/models'
import { fieldNameRegex } from '../../../../../../utils/regex'
import { getNameFromDisplayName } from '../../../../../../utils/utils'

const DetailForm = styled.form`
  padding: ${spacing('lg')} ${spacing('xl')};
`

type Props = {
  visible: boolean
  setVisible: (visible: boolean) => void
  data?: PickListResponse
}

export const PickListModal: FC<Props> = ({ visible, setVisible, data }) => {
  const { validateFunction } = useValidateString()
  const { register, setValue, formState, handleSubmit, getValues } = useForm<CreatePickListDto>()
  const queryClient = useQueryClient()

  const { mutate: createPickList, isLoading: isCreating } = useMutation(APIServices.PickLists.createPickList, {
    onSuccess: () => {
      queryClient.invalidateQueries(PickListsQueryKey.getAllPickLists)
      setVisible(false)
    },
    onError: error => {
      NToast.error({
        title: 'Create pick list unsuccessful',
        subtitle: error.response?.data.message,
      })
    },
  })

  const { mutate: updatePickList, isLoading: isUpdating } = useMutation(APIServices.PickLists.updatePickList, {
    onSuccess: () => {
      queryClient.invalidateQueries(PickListsQueryKey.getAllPickLists)
      queryClient.invalidateQueries([PickListsQueryKey.getPickList, { name: data?.name }])
      setVisible(false)
    },
    onError: error => {
      NToast.error({
        title: 'Update pick list unsuccessful',
        subtitle: error.response?.data.message,
      })
    },
  })

  const submitForm = (formData: CreatePickListDto) => {
    data ? updatePickList({ ...formData, name: data.name }) : createPickList(formData)
  }

  return (
    <NModal size="large" visible={visible} setVisible={setVisible}>
      <NModal.Header onClose={() => setVisible(false)} title={data ? 'Update pick list' : 'Create pick list'} />
      <NModal.Body>
        <DetailForm>
          <NRow>
            <NColumn flex={1}>
              <NTextInput
                required
                defaultValue={data?.displayName || ''}
                {...register('displayName', {
                  validate: validateFunction,
                })}
                error={formState.errors.displayName?.message}
                placeholder="Enter display name"
                label="Display name"
                onBlur={e => {
                  if (!data) {
                    const formatName = getNameFromDisplayName(e.target.value)
                    !getValues('name') && setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
                  }
                }}
              />
            </NColumn>
            <NDivider vertical size="xl" />
            <NColumn flex={1}>
              <NTextInput
                disabled={!!data}
                defaultValue={data?.name || ''}
                {...register('name', {
                  validate: !data ? validateFunction : undefined,
                  pattern: {
                    value: fieldNameRegex,
                    message: 'Invalid pattern',
                  },
                })}
                required
                error={formState.errors.name?.message}
                placeholder="Name"
                label="API name"
              />
            </NColumn>
          </NRow>
          <NDivider size="md" />
          <NRow>
            <NColumn flex={1}>
              <NTextArea
                defaultValue={data?.description || ''}
                {...register('description')}
                placeholder="Pick list description"
                label="Description"
                rows={4}
              />
            </NColumn>
          </NRow>
        </DetailForm>
      </NModal.Body>
      <NModal.Footer
        isLoading={isCreating || isUpdating}
        onFinish={handleSubmit(submitForm)}
        onCancel={() => setVisible(false)}
      />
    </NModal>
  )
}
