import { FC, useState } from 'react'
import { useMutation, useQueryClient } from 'react-query'
import { NTableActions } from '../../../../../components/NTable/NTableActions'
import { NToast } from '../../../../../components/NToast'
import { APIServices } from '../../../../../services/api'
import { OrganizationQueryKey } from '../../../../../services/api/endpoints/organization'

type CurrencyTableActionsProps = {
  currencyCode: string
}

export const CurrencyTableActions: FC<CurrencyTableActionsProps> = ({ currencyCode }) => {
  const [showDropList, setShowDropList] = useState(false)
  const queryClient = useQueryClient()

  const { mutate: setDefaultCurrency, isLoading: isSettingDefault } = useMutation(
    APIServices.Organization.setDefaultCurrency,
    {
      onSuccess: data => {
        queryClient.invalidateQueries(OrganizationQueryKey.getOrganization)
        setShowDropList(false)
        NToast.success({
          title: 'Set default currency successful',
          subtitle: `Default currency is updated to ${data.data.defaultCurrency.name}`,
        })
      },
      onError: error => {
        NToast.error({ title: 'Set default currency unsuccessful', subtitle: error.response?.data.message })
      },
    },
  )

  return (
    <NTableActions
      options={[
        {
          title: 'Set as default',
          isLoading: isSettingDefault,
          onClick: () => setDefaultCurrency({ defaultCurrencyCode: currencyCode }),
        },
      ]}
      showDropList={showDropList}
      setShowDropList={setShowDropList}
    />
  )
}
