import { FC } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useMutation, useQueryClient } from 'react-query'
import styled from 'styled-components'
import { spacing } from '../../../../../components/GlobalStyle'
import { NDivider } from '../../../../../components/NDivider'
import { NModal } from '../../../../../components/NModal/NModal'
import { NSwitch } from '../../../../../components/NSwitch/NSwitch'
import { NTextInput } from '../../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../../components/NToast'
import { useValidateString } from '../../../../../hooks/useValidateString'
import { APIServices } from '../../../../../services/api'
import { CurrenciesQueryKey } from '../../../../../services/api/endpoints/currencies'
import { OrganizationCurrencyResponse, UpdateCurrencyDto } from '../../../../../services/api/models'

const FormWrapper = styled.form`
  padding: ${spacing('lg')} ${spacing('xl')};
`

type Props = {
  visible: boolean
  setVisible: (visible: boolean) => void
  currency: OrganizationCurrencyResponse
}

export const UpdateCurrencyModal: FC<Props> = ({ visible, setVisible, currency }) => {
  const queryClient = useQueryClient()
  const { validateFunction } = useValidateString()

  const { control, register, handleSubmit, formState } = useForm({
    defaultValues: {
      displayName: currency.displayName,
      isActive: currency.isActive || false,
    },
  })

  const { mutate: updateCurrency, isLoading } = useMutation(APIServices.Currencies.updateCurrency, {
    onSuccess: () => {
      queryClient.invalidateQueries([CurrenciesQueryKey.getCurrenciesList])
      queryClient.invalidateQueries([CurrenciesQueryKey.getCurrency, { currencyCode: currency.currencyCode }])
      setVisible(false)
    },
    onError: error => {
      NToast.error({ title: 'Update Currency', subtitle: error.response?.data.message })
    },
  })

  const onSubmit = (data: UpdateCurrencyDto) => {
    updateCurrency({ currencyCode: currency.currencyCode, ...data })
  }

  return (
    <NModal visible={visible} setVisible={setVisible}>
      <NModal.Header title={`Update ${currency.name}`} onClose={() => setVisible(false)} />
      <NModal.Body>
        <FormWrapper>
          <NTextInput
            required
            {...register('displayName', { validate: validateFunction })}
            label="Display name"
            placeholder="Enter display name"
            error={formState.errors.displayName?.message}
          />
          <NDivider size="xl" />
          <Controller
            control={control}
            name="isActive"
            render={({ field }) => <NSwitch title="Is active" checked={field.value} onChange={field.onChange} />}
          />
        </FormWrapper>
      </NModal.Body>
      <NModal.Footer onCancel={() => setVisible(false)} onFinish={handleSubmit(onSubmit)} isLoading={isLoading} />
    </NModal>
  )
}
