import { FC } from 'react'
import { useForm } from 'react-hook-form'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import styled, { useTheme } from 'styled-components'
import { color, spacing } from '../../../../../components/GlobalStyle'
import { NDivider } from '../../../../../components/NDivider'
import { NColumn, NRow } from '../../../../../components/NGrid/NGrid'
import { NModal } from '../../../../../components/NModal/NModal'
import { NSpinner } from '../../../../../components/NSpinner/NSpinner'
import { NTextInput } from '../../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../../components/NToast'
import { useValidateString } from '../../../../../hooks/useValidateString'
import { APIServices } from '../../../../../services/api'
import { CurrenciesQueryKey } from '../../../../../services/api/endpoints/currencies'
import { OrganizationQueryKey } from '../../../../../services/api/endpoints/organization'
import { OrganizationCurrencyResponse, UpdateCurrencyRateDto } from '../../../../../services/api/models'

const FormWrapper = styled.form`
  padding: ${spacing('lg')} ${spacing('xl')};
`

const LoadingWrapper = styled.div`
  padding: ${spacing('xl')};
  display: flex;
  justify-content: center;
`
const RowContentWrapper = styled.div`
  border: 1px solid ${color('Neutral200')};
  padding: ${spacing('xs')};
  min-height: 48px;
  display: flex;
  align-items: center;
  flex: 1;
`

type Props = {
  visible: boolean
  setVisible: (visible: boolean) => void
  currency: OrganizationCurrencyResponse
}

export const UpdateCurrencyRateModal: FC<Props> = ({ visible, setVisible, currency }) => {
  const theme = useTheme()
  const { register, formState, handleSubmit } = useForm()
  const queryClient = useQueryClient()
  const { validateFunction } = useValidateString()

  const { data: orgnaization, isLoading: isLoadingOrg } = useQuery(
    OrganizationQueryKey.getOrganization,
    APIServices.Organization.getOrganization,
  )

  const { mutate: updateCurrencyRate, isLoading: isUpdating } = useMutation(APIServices.Currencies.updateCurrencyRate, {
    onSuccess: () => {
      queryClient.invalidateQueries([CurrenciesQueryKey.getCurrenciesList])
      queryClient.invalidateQueries([CurrenciesQueryKey.getCurrency, { currencyCode: currency.currencyCode }])
      queryClient.invalidateQueries([CurrenciesQueryKey.getCurrencyRates, { currencyCode: currency.currencyCode }])
      setVisible(false)
    },
    onError: error => {
      NToast.error({ title: 'Update Currency', subtitle: error.response?.data.message })
    },
  })

  const onSubmit = (data: UpdateCurrencyRateDto) => {
    updateCurrencyRate({
      currencyCode: currency.currencyCode,
      ...data,
    })
  }

  return (
    <NModal visible={visible} setVisible={setVisible}>
      <NModal.Header title={`Update rate for ${currency.displayName}`} onClose={() => setVisible(false)} />
      <NModal.Body>
        {isLoadingOrg ? (
          <LoadingWrapper>
            <NSpinner size={22} strokeWidth={2} color={color('Primary700')({ theme })} />
          </LoadingWrapper>
        ) : (
          <FormWrapper>
            <NRow>
              <RowContentWrapper>
                <NColumn>{orgnaization?.data.defaultCurrency.name}</NColumn>
                <NDivider lineSize={1} vertical lineColor={color('Neutral200')({ theme })} />
                <NColumn>
                  <NTextInput
                    required
                    defaultValue={currency.rate}
                    type="number"
                    {...register('rate', {
                      validate: validateFunction,
                    })}
                    error={formState.errors.rate?.message}
                    placeholder="Enter conversion rate"
                    style={{ textAlign: 'right' }}
                  />
                </NColumn>
              </RowContentWrapper>
            </NRow>
          </FormWrapper>
        )}
      </NModal.Body>
      <NModal.Footer isLoading={isUpdating} onCancel={() => setVisible(false)} onFinish={handleSubmit(onSubmit)} />
    </NModal>
  )
}
