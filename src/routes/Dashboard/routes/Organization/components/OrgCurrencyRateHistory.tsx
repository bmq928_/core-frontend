import React, { FC } from 'react'
import { useQuery } from 'react-query'
import styled from 'styled-components'
import { color, fontSize, spacing } from '../../../../../components/GlobalStyle'
import { NSpinner } from '../../../../../components/NSpinner/NSpinner'
import { typography } from '../../../../../components/NTypography'
import { APIServices } from '../../../../../services/api'
import { CurrenciesQueryKey } from '../../../../../services/api/endpoints/currencies'

const Wrapper = styled.div`
  margin-left: ${spacing('xxl')};
  display: flex;
  flex-direction: column;
  overflow: visible;
`
////////////////////////////////
const Dot = styled.div`
  width: 4px;
  height: 4px;
  border-radius: 2px;
  border: 1px solid ${color('Neutral300')};
  background-color: ${color('white')};
`

const Line = styled.div`
  position: absolute;
  bottom: 8px;

  width: 1px;
  height: calc(36px + ${spacing('xxl')});
  background-color: ${color('Neutral300')};
`

const DotLineWrapper = styled.div`
  display: flex;
  position: relative;
  overflow: visible;
  justify-content: center;
  margin-right: ${spacing('xl')};
`

const RateItemWrapper = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  height: 48px;
  margin-top: ${spacing('xxl')};

  &:first-child {
    margin-top: 0;

    ${Dot} {
      border: 1px solid ${color('Primary700')};
      background-color: ${color('Primary700')};
    }

    ${Line} {
      display: none;
    }
  }
`

const TextBox = styled.div`
  display: flex;
  flex-direction: column;
`

const DataText = styled.div`
  ${typography('small-overline')};
  color: ${color('Neutral400')};
  text-overflow: ellipsis;
`

const RateText = styled.div`
  ${typography('small-bold-text')};
  text-overflow: ellipsis;
`

const ChangerNameText = styled.div`
  ${typography('x-small-ui-text')};
  color: ${color('Neutral400')};
  line-height: ${fontSize('md')};
  text-overflow: ellipsis;
`
////////////////////////////////

type Props = {
  currencyCode: string
}

export const OrgCurrencyRateHistory: FC<Props> = ({ currencyCode }) => {
  const { data, isFetching } = useQuery(
    [CurrenciesQueryKey.getCurrencyRates, { currencyCode }],
    APIServices.Currencies.getCurrencyRates,
  )

  if (isFetching) {
    return (
      <Wrapper>
        <NSpinner size={20} strokeWidth={2} />
      </Wrapper>
    )
  }

  const rateArray = data?.data.data || []
  return (
    <Wrapper>
      {rateArray.map((rate, index) => {
        return (
          <RateItemWrapper key={`${rate.fromCurrencyCode}-${rate.toCurrencyCode}-${index}`}>
            <DotLineWrapper>
              <Dot />
              <Line />
            </DotLineWrapper>
            <TextBox>
              <DataText>{rate.dateFrom}</DataText>
              <RateText>{rate.rate}</RateText>
              <ChangerNameText>
                {rate.fromCurrencyCode} - {rate.toCurrencyCode}
              </ChangerNameText>
            </TextBox>
          </RateItemWrapper>
        )
      })}
    </Wrapper>
  )
}
