import * as React from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useQuery } from 'react-query'
import styled from 'styled-components'
import { spacing } from '../../../../components/GlobalStyle'
import { NDivider } from '../../../../components/NDivider'
import { NColumn, NRow } from '../../../../components/NGrid/NGrid'
import { NModal, NModalProps } from '../../../../components/NModal/NModal'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { NTextArea } from '../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { useSearchText } from '../../../../hooks/useSearchText'
import { useValidateString } from '../../../../hooks/useValidateString'
import { APIServices } from '../../../../services/api'
import { BuilderQueryKey } from '../../../../services/api/endpoints/builder'
import { CreateLayoutDto } from '../../../../services/api/models'

const FormWrapper = styled('div')`
  padding: ${spacing('xl')};
`

const LAYOUT_TYPE = [
  { value: 'app-page', label: 'App Page' },
  { value: 'record-page', label: 'Record Page' },
]

const PAGE_SIZE = 20

type CreateLayoutModalProps = {
  onSubmit?(data: CreateLayoutDto): void
} & NModalProps

export function CreateLayoutModal({ visible, setVisible, onSubmit }: CreateLayoutModalProps) {
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText('')
  const { validateFunction } = useValidateString()

  const { data, isFetching } = useQuery(
    [BuilderQueryKey.getAllObjects, { limit: PAGE_SIZE, searchText }],
    APIServices.Builder.getAllObjects,
    { keepPreviousData: true },
  )

  const { register, handleSubmit, formState, control, watch, getValues } = useForm<CreateLayoutDto>()
  const handleClose = () => {
    if (setVisible) {
      setVisible(false)
    }
  }
  const handleFinish = handleSubmit(data => {
    handleClose()
    if (onSubmit) {
      onSubmit(data)
    }
  })

  const layoutType = watch('type')
  return (
    <NModal visible={visible} setVisible={setVisible}>
      <NModal.Header onClose={handleClose} title="New Layout" />
      <NModal.Body>
        <FormWrapper>
          <NRow>
            <NColumn flex={1}>
              <NTextInput
                required
                {...register('name', {
                  validate: validateFunction,
                })}
                error={formState.errors.name?.message}
                label="Label"
                caption="Layout Name"
              />
            </NColumn>
            <NDivider vertical size="xl" />
            <NColumn flex={1}>
              <Controller
                control={control}
                name="type"
                defaultValue={LAYOUT_TYPE[0].value as 'app-page'}
                render={({ field: { value, onChange } }) => {
                  return (
                    <NSingleSelect
                      label="Layout Type"
                      options={LAYOUT_TYPE}
                      value={value}
                      onValueChange={newValue => {
                        onChange(newValue)
                      }}
                    />
                  )
                }}
              />
            </NColumn>
          </NRow>
          {layoutType === 'record-page' && (
            <>
              <NDivider size="xl" />
              <Controller
                control={control}
                name="objectName"
                defaultValue=""
                render={({ field: { value, onChange }, fieldState }) => (
                  <NSingleSelect
                    label="Object"
                    options={(data?.data.data || []).map(option => {
                      const typeText = option.isExternal ? ' (external)' : ' (internal)'
                      return { value: option.name, label: option.displayName + typeText }
                    })}
                    value={value}
                    onValueChange={newValue => {
                      onChange(newValue)
                    }}
                    fullWidth
                    isSearchable
                    searchValue={searchValue}
                    onSearchValueChange={handleSearchChange}
                    isLoading={isFetching}
                    error={fieldState.error?.message}
                  />
                )}
                rules={{
                  validate: (value?: string) => {
                    return (getValues().type === 'record-page' && Boolean(value)) || 'Required!'
                  },
                }}
              />
            </>
          )}
          <NDivider size="xl" />
          <NTextArea name="description" label="Description" />
        </FormWrapper>
      </NModal.Body>
      <NModal.Footer onCancel={handleClose} onFinish={handleFinish} />
    </NModal>
  )
}
