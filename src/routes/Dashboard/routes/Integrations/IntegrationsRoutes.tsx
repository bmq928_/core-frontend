import React, { FC } from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import { DASHBOARD_ROUTE } from '../../../../common/constants'
import { NLayout, NSideBar } from '../../../../components/NLayout'
import { NMenu } from '../../../../components/NMenu/NMenu'
import { ActionMetadataForm } from './ActionMetadata/ActionMetadataForm'
import { ActionMetadataListing } from './ActionMetadata/ActionMetadataListing'
import { CredentialDetail } from './Credentials/CredentialDetail'
import { CredentialListing } from './Credentials/CredentialListing'
import { ExternalObjectsListing } from './ExternalObjects/ExternalObjectsListing'
import { MediaDetail } from './Media/MediaDetail'
import { MediaListing } from './Media/MediaListing'

export const IntegrationsRoutes: FC = () => {
  return (
    <NLayout isRow>
      <NSideBar title="Integration">
        <NMenu type="primary">
          <NMenu.Item to={`${DASHBOARD_ROUTE}/integrations/credentials`}>Credentials</NMenu.Item>
          <NMenu.Item to={`${DASHBOARD_ROUTE}/integrations/action-metadata`}>Action metadata</NMenu.Item>
          <NMenu.Item to={`${DASHBOARD_ROUTE}/integrations/external-objects`}>External Object</NMenu.Item>
          <NMenu.Item to={`${DASHBOARD_ROUTE}/integrations/media`}>Media</NMenu.Item>
        </NMenu>
      </NSideBar>
      <Switch>
        <Route path={`${DASHBOARD_ROUTE}/integrations/credentials/:credentialName`}>
          <CredentialDetail />
        </Route>
        <Route path={`${DASHBOARD_ROUTE}/integrations/credentials`}>
          <CredentialListing />
        </Route>
        <Route
          path={[
            `${DASHBOARD_ROUTE}/integrations/action-metadata/create`,
            `${DASHBOARD_ROUTE}/integrations/action-metadata/:actionMetadataName`,
          ]}>
          <ActionMetadataForm />
        </Route>
        <Route path={`${DASHBOARD_ROUTE}/integrations/action-metadata`}>
          <ActionMetadataListing />
        </Route>
        <Route path={`${DASHBOARD_ROUTE}/integrations/external-objects`}>
          <ExternalObjectsListing />
        </Route>
        <Route path={`${DASHBOARD_ROUTE}/integrations/media/:mediaId`}>
          <MediaDetail />
        </Route>
        <Route path={`${DASHBOARD_ROUTE}/integrations/media`}>
          <MediaListing />
        </Route>
        <Redirect to={`${DASHBOARD_ROUTE}/integrations/credentials`} />
      </Switch>
    </NLayout>
  )
}
