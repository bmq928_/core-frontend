import React, { FC } from 'react'
import { useQuery } from 'react-query'
import { Redirect, Route, Switch, useHistory, useParams, useRouteMatch } from 'react-router-dom'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { NLayout, NSideBar } from '../../../../../components/NLayout'
import { NMenu } from '../../../../../components/NMenu/NMenu'
import { APIServices } from '../../../../../services/api'
import { IntegrationsQueryKeys } from '../../../../../services/api/endpoints/integrations'
import { SidebarHeader } from '../../../components/SidebarHeader'
import { ExtObjFields } from './ExObjFields'
import { ExtObjDataSources } from './ExtObjDataSources'
import { ExtObjRules } from './ExtObjRules'

export const ExternalObjectsDetailRoutes: FC = () => {
  const { url, path } = useRouteMatch()
  const { objName } = useParams<{ objName: string }>()
  const history = useHistory()

  const { data: objectData } = useQuery(
    [IntegrationsQueryKeys.getExtObject, { objName }],
    APIServices.Integrations.getExtObject,
  )

  return (
    <NLayout isRow>
      <NSideBar>
        <SidebarHeader
          onBack={() => history.push(`${DASHBOARD_ROUTE}/integrations`)}
          backTitle="Back to Integration"
          title={objectData?.data.displayName || objName}
        />
        <NMenu type="primary">
          <NMenu.Item to={`${url}/fields`}>Fields</NMenu.Item>
          {/* <NMenu.Item to={`${url}/rules`}>Validation Rules</NMenu.Item> */}
          <NMenu.Item to={`${url}/data-sources`}>Data sources</NMenu.Item>
        </NMenu>
      </NSideBar>
      <Switch>
        <Route path={`${path}/fields`}>
          <ExtObjFields />
        </Route>
        <Route path={`${path}/rules`}>
          <ExtObjRules />
        </Route>
        <Route path={`${path}/data-sources`}>
          <ExtObjDataSources />
        </Route>
        <Redirect to={`${url}/fields`} />
      </Switch>
    </NLayout>
  )
}
