import { formatDistanceToNow } from 'date-fns'
import { omit } from 'lodash'
import React, { FC, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { useParams } from 'react-router-dom'
import styled from 'styled-components'
import { classnames } from '../../../../../common/classnames'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { spacing } from '../../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../../components/Icons'
import { NButton } from '../../../../../components/NButton/NButton'
import { NDetailHeader } from '../../../../../components/NDetailHeader'
import { NDivider } from '../../../../../components/NDivider'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { NModal } from '../../../../../components/NModal/NModal'
import { NTable, NTableColumnType } from '../../../../../components/NTable/NTable'
import { NTableCollapsedCellClassName } from '../../../../../components/NTable/NTableStyledContainer'
import { NToast } from '../../../../../components/NToast'
import { APIServices } from '../../../../../services/api'
import { BuilderQueryKey } from '../../../../../services/api/endpoints/builder'
import { IntegrationsQueryKeys } from '../../../../../services/api/endpoints/integrations'
import { ObjectRecordPagesQueryKey } from '../../../../../services/api/endpoints/objRecordPages'
import {
  DataTypeResponse,
  ExtDataTypeResponse,
  ExtFieldResponse,
  FieldResponse,
  LayoutComponent,
} from '../../../../../services/api/models'
import { PostAction } from '../../../../../services/api/request'
import { transformStringToOptions } from '../../../../../utils/utils'
import { DataTypeForm } from '../../../components/DatatypeForm/DataTypeForm'
import { DatatypeSelectModal } from '../../../components/DatatypeSelectModal'
import { FieldDecoration } from '../../../components/FieldDecoration'
import { UpdateLayoutComponentFieldModal } from '../../../components/UpdateLayoutComponentFieldModal'
import { IFieldForm } from '../../Object/models'
import { ExtFieldTableActions } from './components/ExtFieldTableActions'
import { ExtObjectFormModal } from './ExtObjectFormModal'

const ModalWrapper = styled.div<{ visible?: boolean }>`
  display: ${props => (!!props.visible ? 'initial' : 'none')};
`

const StyledRow = styled.div`
  display: flex;
  align-items: center;

  div {
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
  }
`

const COLUMNS: NTableColumnType<ExtFieldResponse> = [
  {
    Header: 'Display name',
    accessor: 'displayName',
    Cell: ({ value, row }) => {
      return (
        <StyledRow>
          <FieldDecoration type={row.original.dataType.name} />
          <NDivider vertical size="md" />
          <div>{value}</div>
        </StyledRow>
      )
    },
  },
  { Header: 'API name', accessor: 'name' },
  {
    Header: 'Type',
    accessor: 'dataType',
    Cell: ({ value, row }) => {
      const subType = row.original.attributes.subType ? `(${row.original.attributes.subType})` : ''
      return ` ${value.displayName} ${subType}`
    },
  },
  {
    Header: 'Is system',
    accessor: 'isSystemDefault',
    Cell: ({ value }) => {
      return value ? <GlobalIcons.Check /> : null
    },
  },
  {
    Header: 'Is required',
    accessor: 'isRequired',
    Cell: ({ value }) => {
      return value ? <GlobalIcons.Check /> : null
    },
  },
  {
    Header: 'Last modified',
    accessor: 'updatedAt',
    Cell: ({ value }) => {
      if (!value) {
        return null
      }
      const date = new Date(value)
      return date.toLocaleString('vi')
    },
  },
  {
    accessor: 'name',
    Cell: ({ value, row }) => {
      return row.original.isSystemDefault ? <></> : <ExtFieldTableActions fieldName={value} />
    },
    id: classnames([NTableCollapsedCellClassName]),
  },
]

const FieldDetailBody = styled.div`
  padding: ${spacing('xl')};
`

export const ExtObjFields: FC = () => {
  const { objName } = useParams<{ objName: string }>()

  const [showCreateObject, setShowCreateObject] = useState(false)

  const [showDataTypes, setShowDataTypes] = useState(false)
  const [showFieldDetail, setShowFieldDetail] = useState(false)
  const [showUpdateLayouts, setShowUpdateLayouts] = useState(false)

  const [selectedDataType, setSelectedDataType] = useState<DataTypeResponse | ExtDataTypeResponse>()
  const [selectedField, setSelectedField] = useState<ExtFieldResponse>()

  // form
  const methods = useForm<IFieldForm>({ mode: 'onBlur' })
  const watchName = methods.watch('name')
  const watchIsRequired = methods.watch('isRequired') as boolean
  //
  const queryClient = useQueryClient()

  const { mutate: postExtObjField, isLoading: isPostingField } = useMutation(APIServices.Integrations.postExtObjField)

  const onSubmit = (data: IFieldForm, updateLayouts: LayoutComponent[]) => {
    const submitData = {
      name: selectedField ? undefined : data.name,
      displayName: data.displayName || '',
      typeName: selectedField ? undefined : selectedDataType?.name,
      isRequired: data.isRequired,
      isExternalId: data.isExternalId,
      attributes: {
        ...data.attributes,
      },
      value: selectedDataType?.name === 'pickList' ? transformStringToOptions(data.value) : data.value,
    }

    postExtObjField(
      {
        body: {
          data: selectedField ? omit(submitData, ['name', 'typeName']) : submitData,
          action: selectedField ? PostAction.Update : PostAction.Create,
          name: selectedField && selectedField.name,
          updateLayouts,
        },
        objName: objName,
      },
      {
        onSuccess: async () => {
          await queryClient.invalidateQueries(IntegrationsQueryKeys.getExtObjFieldList)
          await queryClient.invalidateQueries(ObjectRecordPagesQueryKey.getObjectRecordPages)
          updateLayouts.map(async layout => {
            await queryClient.invalidateQueries([BuilderQueryKey.getLayoutScript, { id: layout.layoutId }])
            return
          })

          handleUpdateLayoutsClose()
        },
        onError: error => {
          NToast.error({
            title: `${selectedField ? 'Update' : 'Create'} field unsuccessful`,
            subtitle: error.response?.data.message,
          })
        },
      },
    )
  }
  const { data: objectData } = useQuery(
    [IntegrationsQueryKeys.getExtObject, { objName }],
    APIServices.Integrations.getExtObject,
  )

  const { data: fieldsData, isLoading: isLoadingFields } = useQuery(
    [IntegrationsQueryKeys.getExtObjFieldList, { objName }],
    APIServices.Integrations.getExtObjFieldList,
  )

  // select data type functions
  const onNewDataTypeSelect = (type: DataTypeResponse | ExtDataTypeResponse) => {
    setShowDataTypes(false)
    setShowFieldDetail(true)
    setSelectedDataType(type)
  }

  // field detail functions
  const onFieldCellSelect = (field: ExtFieldResponse) => {
    setSelectedField(field)
    setSelectedDataType(field.dataType)
    setShowFieldDetail(true)
  }

  const handleFieldDetailClose = () => {
    setShowFieldDetail(false)
    setShowUpdateLayouts(false)
    setSelectedDataType(undefined)
    setSelectedField(undefined)
  }

  const onFieldDetailBack = () => {
    setShowDataTypes(true)
    handleFieldDetailClose()
  }

  const onSubmitFieldDetail = async () => {
    const checking = await methods.trigger()

    if (checking) {
      setShowUpdateLayouts(true)
    }
  }

  // update layouts functions
  const onUpdateLayoutsBack = () => {
    setShowUpdateLayouts(false)
  }

  const handleSubmitUpdateLayouts = (newUpdateLayouts: LayoutComponent[]) => {
    methods.handleSubmit(data => {
      onSubmit(data, newUpdateLayouts)
    })()
  }

  const handleUpdateLayoutsClose = () => {
    setShowFieldDetail(false)
    setShowUpdateLayouts(false)
    setSelectedDataType(undefined)
    setSelectedField(undefined)
  }
  //
  return (
    <>
      <NContainer>
        <NDetailHeader
          label={
            objectData?.data.updatedAt &&
            `Last updated ${formatDistanceToNow(new Date(objectData?.data.updatedAt), { addSuffix: true })}`
          }
          breadcrumbs={[
            { path: DASHBOARD_ROUTE, label: 'Dashboard' },
            {
              path: `${DASHBOARD_ROUTE}/integrations/external-objects`,
              label: 'External Objects',
            },
            {
              label: objName,
            },
          ]}>
          {!objectData?.data.isSystemDefault && (
            <NButton icon={<GlobalIcons.Edit />} onClick={() => setShowCreateObject(true)}>
              Edit
            </NButton>
          )}
          <NDivider size="sm" vertical />
          <NButton type="primary" onClick={() => setShowDataTypes(true)}>
            Add Field
          </NButton>
        </NDetailHeader>
        <NContainer.Title>{objectData?.data.displayName}</NContainer.Title>
        {objectData?.data.description && (
          <NContainer.Description>{objectData?.data.description}</NContainer.Description>
        )}
        <NContainer.Content>
          <NTable
            isLoading={isLoadingFields}
            columns={COLUMNS}
            data={fieldsData?.data || []}
            onClickRow={data => {
              !data.isSystemDefault && onFieldCellSelect(data)
            }}
            pageSize={(fieldsData?.data || []).length}
          />
          <NDivider size="md" />
        </NContainer.Content>
        <NDivider size="xxl" />
      </NContainer>
      {/* Object field forms */}
      <DatatypeSelectModal
        visible={showDataTypes}
        setVisible={setShowDataTypes}
        onDatatypeSelect={onNewDataTypeSelect}
      />

      <NModal setVisible={setShowFieldDetail} size="large" visible={showFieldDetail}>
        <ModalWrapper visible={!showUpdateLayouts}>
          <NModal.Header
            title={selectedField ? `Edit ${selectedField?.displayName}` : `New ${selectedDataType?.displayName} field`}
            onClose={handleFieldDetailClose}
            onBack={selectedField ? undefined : onFieldDetailBack}
          />
          <NModal.Body>
            <FieldDetailBody>
              {selectedDataType && (
                <DataTypeForm
                  object={objectData?.data}
                  fieldData={selectedField as FieldResponse & ExtFieldResponse}
                  formMethods={methods}
                  dataType={selectedDataType}
                  isExternal={true}
                />
              )}
            </FieldDetailBody>
          </NModal.Body>
          <NModal.Footer finishText="Next" onCancel={handleFieldDetailClose} onFinish={onSubmitFieldDetail} />
        </ModalWrapper>
        {/* update layout */}
        <UpdateLayoutComponentFieldModal
          visible={showUpdateLayouts}
          setVisible={newVisible => {
            if (!newVisible) {
              handleFieldDetailClose()
            }
          }}
          onBack={onUpdateLayoutsBack}
          objName={objName}
          fieldName={selectedField?.name || watchName}
          isRequired={watchIsRequired}
          isSubmitting={isPostingField}
          onSubmit={handleSubmitUpdateLayouts}
        />
      </NModal>
      {/* Object form */}
      {showCreateObject && objectData?.data && (
        <ExtObjectFormModal visible={showCreateObject} setVisible={setShowCreateObject} objectData={objectData.data} />
      )}
    </>
  )
}
