import { FC, useState } from 'react'
import { useMutation, useQueryClient } from 'react-query'
import { useParams } from 'react-router-dom'
import { NTableActions } from '../../../../../../components/NTable/NTableActions'
import { NToast } from '../../../../../../components/NToast'
import { APIServices } from '../../../../../../services/api'
import { IntegrationsQueryKeys } from '../../../../../../services/api/endpoints/integrations'
import { PostAction } from '../../../../../../services/api/request'

type Props = {
  fieldName: string
}

export const ExtFieldTableActions: FC<Props> = ({ fieldName }) => {
  const [showDropList, setShowDropList] = useState(false)
  const queryClient = useQueryClient()
  const { objName } = useParams<{ objName: string }>()

  const { mutate: postExtObjField, isLoading: isPostingField } = useMutation(APIServices.Integrations.postExtObjField)

  const onDeleteField = () => {
    postExtObjField(
      {
        body: {
          action: PostAction.Delete,
          name: fieldName,
        },
        objName,
      },
      {
        onSuccess: async () => {
          setShowDropList(false)
          await queryClient.invalidateQueries(IntegrationsQueryKeys.getExtObjFieldList)
        },
        onError: error => {
          NToast.error({
            title: 'Delete unsuccessful',
            subtitle: error.response?.data.message,
          })
        },
      },
    )
  }

  return (
    <NTableActions
      showDropList={showDropList}
      setShowDropList={setShowDropList}
      options={[{ title: 'Delete', isLoading: isPostingField, onClick: onDeleteField }]}
    />
  )
}
