import { omit } from 'lodash'
import { FC, useMemo, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useMutation } from 'react-query'
import { useParams } from 'react-router-dom'
import styled from 'styled-components'
import { spacing } from '../../../../../../components/GlobalStyle'
import { NButton } from '../../../../../../components/NButton/NButton'
import { NCheckbox } from '../../../../../../components/NCheckbox/NCheckbox'
import { NDivider } from '../../../../../../components/NDivider'
import { NRow } from '../../../../../../components/NGrid/NGrid'
import { NModal } from '../../../../../../components/NModal/NModal'
import { NTextInput } from '../../../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../../../components/NToast'
import { typography } from '../../../../../../components/NTypography'
import { SettingBoard } from '../../../../../../components/SettingBoard'
import { APIServices } from '../../../../../../services/api'
import { EndpointAction } from '../../../../../../services/api/endpoints/integrations'
import { ExtDataSourceInputMap, ExtObjectDataSourceResponse } from '../../../../../../services/api/models'

const ContentWrapper = styled.div`
  padding: ${spacing('md')};
  overflow-x: auto;
`
const Label = styled.p`
  ${typography('small-bold-text')}
`
const Wrapper = styled.div`
  max-height: 60vh;
  overflow: auto;
`

const FullWidthInput = styled(NTextInput)`
  width: 100%;
`

const StyledRow = styled(NRow)`
  margin-bottom: ${spacing('xs')};
`

type Props = {
  visible: boolean
  setVisible: (visible: boolean) => void
  objectName?: string
  actionName: EndpointAction
  datasourceAction: ExtObjectDataSourceResponse
}

export const TestEndpointModal: FC<Props> = ({ visible, setVisible, objectName, actionName, datasourceAction }) => {
  const { register, reset, handleSubmit } = useForm({ shouldUnregister: false })
  const { objName } = useParams<{ objName: string }>()
  const [excludedFields, setExcludedFields] = useState<string[]>([])

  const { mutate: testSource, isLoading, data: testResult } = useMutation(APIServices.Integrations.testSource, {
    onError: error => {
      NToast.error({ title: 'Fail to test', subtitle: error.response?.data.message })
    },
  })

  const actionInputs: string[] = useMemo(() => {
    return datasourceAction.inputMap ? Object.keys(datasourceAction.inputMap) : []
  }, [datasourceAction.inputMap])

  const onSubmit = (data: any) => {
    const inputMap: Record<string, ExtDataSourceInputMap> = {}
    data.path &&
      Object.keys(data.path).forEach(input => {
        inputMap[`path.${input}`] = data.path[input]
      })
    data.header &&
      Object.keys(data.header).forEach(input => {
        inputMap[`header.${input}`] = data.header[input]
      })
    data.query &&
      Object.keys(data.query).forEach(input => {
        inputMap[`query.${input}`] = data.query[input]
      })
    inputMap[`body`] = data.body
    testSource({ data: omit(inputMap, excludedFields), objName, action: actionName })
  }

  return (
    <NModal size="large" visible={visible} setVisible={setVisible}>
      <NModal.Header
        title={`${objectName} (${actionName})`}
        onClose={() => {
          setVisible(false)
          reset()
        }}
      />
      <NModal.Body>
        <Wrapper>
          <ContentWrapper>
            <SettingBoard
              action={
                <NButton size="small" type="primary" onClick={handleSubmit(onSubmit)} loading={isLoading}>
                  Submit
                </NButton>
              }
              title="Request">
              <ContentWrapper>
                {actionInputs.map(input => {
                  // NOTE: Excluded fields => reverse checked
                  const isChecked = !excludedFields.some(field => field === input)
                  return (
                    <StyledRow align="center">
                      <NCheckbox
                        checked={isChecked}
                        value={input}
                        onChange={e => {
                          if (!isChecked) {
                            setExcludedFields(prev => prev.filter(field => field !== e.target.value))
                          } else {
                            setExcludedFields(prev => [...prev, e.target.value])
                          }
                        }}
                      />
                      <NDivider vertical size="md" />
                      <FullWidthInput key={`${input}`} {...register(input)} label={`${input}`} />
                    </StyledRow>
                  )
                })}
              </ContentWrapper>
            </SettingBoard>
            {testResult?.data && (
              <SettingBoard title="Response">
                <ContentWrapper>
                  {testResult?.data.url && (
                    <>
                      <Label>URL: {testResult?.data.url}</Label>
                      <NDivider size="md" />
                    </>
                  )}

                  {testResult?.data.status && (
                    <>
                      <Label>Status: {testResult?.data.status} </Label>
                      <NDivider size="md" />
                    </>
                  )}
                  {testResult?.data.headers && (
                    <SettingBoard title="Header">
                      <ContentWrapper>{JSON.stringify(testResult?.data.headers)}</ContentWrapper>
                    </SettingBoard>
                  )}
                  <SettingBoard title={testResult?.data.data ? 'Data' : 'Error'}>
                    <ContentWrapper>
                      {JSON.stringify(testResult?.data.data)}
                      {testResult?.data.error?.toString()}
                    </ContentWrapper>
                  </SettingBoard>
                </ContentWrapper>
              </SettingBoard>
            )}
          </ContentWrapper>
        </Wrapper>
      </NModal.Body>
    </NModal>
  )
}
