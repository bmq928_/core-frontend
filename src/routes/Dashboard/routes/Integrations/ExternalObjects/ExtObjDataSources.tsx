import { isEmpty } from 'lodash'
import React, { FC, useEffect, useMemo, useState } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { useParams } from 'react-router-dom'
import styled, { useTheme } from 'styled-components'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { color, spacing } from '../../../../../components/GlobalStyle'
import { NButton } from '../../../../../components/NButton/NButton'
import { NDetailHeader } from '../../../../../components/NDetailHeader'
import { NDivider } from '../../../../../components/NDivider'
import { NColumn, NRow } from '../../../../../components/NGrid/NGrid'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { NSingleSelect } from '../../../../../components/NSelect/NSingleSelect'
import { NSpinner } from '../../../../../components/NSpinner/NSpinner'
import { NTextInput } from '../../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../../components/NToast'
import { SettingBoard } from '../../../../../components/SettingBoard'
import { useSearchText } from '../../../../../hooks/useSearchText'
import { APIServices } from '../../../../../services/api'
import { EndpointAction, IntegrationsQueryKeys } from '../../../../../services/api/endpoints/integrations'
import { ExtDataSourceInputMap } from '../../../../../services/api/models'
import { ConfigTabs } from '../components/ConfigTabs'
import { InputMappingFormItem } from './components/InputMappingFormItem'
import { TestEndpointModal } from './components/TestEndpointModal'

const ENDPOINT_TABS: { label: string; value: EndpointAction }[] = [
  {
    label: 'GET LIST',
    value: 'list',
  },
  {
    label: 'GET ITEM',
    value: 'item',
  },
  {
    label: 'CREATE ITEM',
    value: 'create',
  },
  {
    label: 'UPDATE ITEM',
    value: 'update',
  },
  {
    label: 'DELETE ITEM',
    value: 'delete',
  },
]

const SaveBtn = styled(NButton)`
  width: 80px;
`

const Section = styled.div`
  padding: ${spacing('xl')};
`
const LoadingView = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`

const InputMapWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-column-gap: ${spacing('xl')};
  row-gap: ${spacing('xl')};
`

export const ExtObjDataSources: FC = () => {
  const { objName } = useParams<{ objName: string }>()
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText()
  const { register, control, reset, handleSubmit } = useForm()
  const [selectedTab, setSelectedTab] = useState<EndpointAction>('list')
  const theme = useTheme()
  const [selectedAMName, setSelectedAMName] = useState<string>()
  const queryClient = useQueryClient()
  const [selectedRequestTab, setSelectedRequestTab] = useState<string>()
  const [showTestModal, setShowTestModal] = useState(false)
  const [deprecatedInputs, setDeprecatedInputs] = useState<Record<string, any>>()

  const { data: extObjData } = useQuery(
    [IntegrationsQueryKeys.getExtObject, { objName }],
    APIServices.Integrations.getExtObject,
  )

  const { data: actionMetadaOptions, isFetching: isLoadingMetadataList } = useQuery(
    [IntegrationsQueryKeys.getAllActionMetadata, { searchText }],
    APIServices.Integrations.getAllActionMetadata,
    {
      select: data => {
        return data.data.data.map(am => {
          return {
            value: am.name,
            label: am.displayName,
          }
        })
      },
    },
  )

  const { data: selectedActionMetadata, isFetching: isLoadingSelectedAM } = useQuery(
    [IntegrationsQueryKeys.getSingleActionMetadata, { name: selectedAMName! }],
    APIServices.Integrations.getSingleActionMetadata,
    {
      enabled: !!selectedAMName,
    },
  )

  const { data: dataSourceAction, isFetching: isLoadingDataSource } = useQuery(
    [IntegrationsQueryKeys.getDataSourceAction, { objName, action: selectedTab }],
    APIServices.Integrations.getDataSourceAction,
  )

  const { mutate: updateDataSourceAction, isLoading: isUpdating } = useMutation(
    APIServices.Integrations.updateDataSourceAction,
    {
      onSuccess: async () => {
        await queryClient.invalidateQueries([
          IntegrationsQueryKeys.getDataSourceAction,
          { objName, action: selectedTab },
        ])
      },
      onError: error => {
        NToast.error({ title: 'Update data source', subtitle: error.response?.data.message })
      },
    },
  )

  useEffect(() => {
    if (dataSourceAction?.data) {
      // set form Value
      dataSourceAction?.data.actionMetadata && setSelectedAMName(dataSourceAction.data.actionMetadata.name)
      const { inputMap } = dataSourceAction?.data
      const formData: Record<string, any> = {
        actionMetadataName: dataSourceAction.data.actionMetadata?.name,
        responseMap: dataSourceAction.data.responseMap,
        statusCode: dataSourceAction.data.statusCode,
      }
      Object.keys(inputMap).forEach(key => {
        if (key === 'body') {
          let bodyStringValue
          try {
            bodyStringValue = JSON.stringify(inputMap[key].value)
          } catch {
            bodyStringValue = inputMap[key]
          }
          formData['body'] = { type: inputMap[key].type, value: bodyStringValue }
          return
        }
        const splits = key.split('.')
        formData[splits[0]] = {
          ...formData[splits[0]],
          [splits[1]]: inputMap[key],
        }
      })
      reset(formData)
    }
  }, [dataSourceAction?.data, reset])

  const actionInputs: Record<string, any> | undefined = useMemo(() => {
    // check metadata to render inputs
    if (selectedActionMetadata?.data) {
      const { inputs } = selectedActionMetadata.data
      const inputQueryArray = inputs?.query ? Object.keys(inputs?.query) : []
      const inputHeaderArray = inputs?.header ? Object.keys(inputs?.header) : []
      return {
        path: inputs.path || [],
        query: inputQueryArray,
        header: inputHeaderArray,
      }
    }
    return undefined
  }, [selectedActionMetadata?.data])

  useEffect(() => {
    // effect to check deleted config from metadata but still exist in datasource
    const inputs: Record<string, any> = {
      path: [],
      query: [],
      header: [],
      body: [],
    }
    if (
      dataSourceAction?.data &&
      selectedActionMetadata?.data &&
      dataSourceAction?.data.actionMetadata?.name === selectedActionMetadata.data.name &&
      actionInputs
    ) {
      const { inputMap } = dataSourceAction?.data
      Object.keys(inputMap).forEach(key => {
        if (key === 'body') {
          !selectedActionMetadata.data.inputs.body && inputs.body.push(key)
        } else {
          const splits = key.split('.')
          if (!actionInputs[splits[0]].some((input: string) => input === splits[1])) {
            inputs[splits[0]].push(splits[1])
          }
        }
      })
      setDeprecatedInputs(inputs)
    }
  }, [dataSourceAction?.data, selectedActionMetadata?.data, actionInputs])

  const requestTabs = useMemo(() => {
    const tabList: { label: string; value: string }[] = []
    if (actionInputs) {
      Object.keys(actionInputs).forEach(key => {
        //@ts-ignore
        if (actionInputs[key].length > 0 || (deprecatedInputs && deprecatedInputs[key].length > 0)) {
          tabList.push({
            label: key,
            value: key,
          })
        }
      })
    }
    if (selectedActionMetadata?.data.inputs.body || (deprecatedInputs && deprecatedInputs.body.length > 0)) {
      tabList.push({
        label: 'body',
        value: 'body',
      })
    }
    if (tabList.length > 0) {
      setSelectedRequestTab(tabList[0].value)
    }
    return tabList
  }, [actionInputs, selectedActionMetadata?.data.inputs.body, deprecatedInputs])

  const onTabChange = (tab: EndpointAction) => {
    if (tab === selectedTab) {
      return
    }
    reset()
    setSelectedAMName(undefined)
    setSelectedTab(tab)
  }

  const onSubmit = (data: any) => {
    const inputMap: Record<string, ExtDataSourceInputMap> = {}
    data.path &&
      Object.keys(data.path).forEach(input => {
        inputMap[`path.${input}`] = data.path[input]
      })
    data.header &&
      Object.keys(data.header).forEach(input => {
        inputMap[`header.${input}`] = data.header[input]
      })
    data.query &&
      Object.keys(data.query).forEach(input => {
        inputMap[`query.${input}`] = data.query[input]
      })
    inputMap[`body`] = data.body
    updateDataSourceAction({
      objName,
      action: selectedTab,
      body: {
        inputMap,
        responseMap: data.responseMap,
        statusCode: data.statusCode,
        actionMetadataName: data.actionMetadataName,
      },
    })
  }

  const onRemoveDeprecated = (name: string) => {
    if (!deprecatedInputs) {
      return
    }
    if (name === 'body') {
      setDeprecatedInputs(prev => ({ ...prev, body: [] }))
    } else {
      const splits = name.split('.')
      //@ts-ignore
      const newArr = [...deprecatedInputs[splits[0]]].filter(input => input !== splits[1])
      setDeprecatedInputs(prev => ({ ...prev, [splits[0]]: newArr }))
    }
  }

  return (
    <NContainer>
      <NDetailHeader
        breadcrumbs={[
          { label: 'Intergrations', path: `${DASHBOARD_ROUTE}/integrations` },
          { label: 'External Objects', path: `${DASHBOARD_ROUTE}/integrations/external-objects` },
          { label: extObjData?.data.displayName || '' },
        ]}></NDetailHeader>
      <NContainer.Title>{extObjData?.data.displayName || objName}</NContainer.Title>
      <form>
        <SettingBoard
          title="End point"
          headerComponent={
            <ConfigTabs<EndpointAction>
              tabOptions={ENDPOINT_TABS}
              selectedTab={selectedTab}
              onSelectTab={onTabChange}
            />
          }
          action={
            <NRow>
              <NButton
                disabled={
                  !dataSourceAction ||
                  (dataSourceAction?.data &&
                    isEmpty(dataSourceAction.data.inputMap) &&
                    isEmpty(dataSourceAction.data.responseMap))
                }
                size="small"
                onClick={() => setShowTestModal(true)}>
                Test
              </NButton>
              <NDivider vertical size="xs" />
              <SaveBtn loading={isUpdating} onClick={handleSubmit(onSubmit)} type="outline" size="small">
                Save
              </SaveBtn>
            </NRow>
          }>
          <Section>
            {isLoadingDataSource ? (
              <LoadingView>
                <NSpinner size={30} strokeWidth={2} color={color('Primary900')({ theme })} />
              </LoadingView>
            ) : (
              <>
                <NRow>
                  <NColumn>
                    <Controller
                      defaultValue={selectedAMName || ''}
                      name="actionMetadataName"
                      control={control}
                      render={({ field }) => {
                        // check if selected action metadata is in current option or not
                        const existedOption =
                          dataSourceAction?.data.actionMetadata &&
                          actionMetadaOptions &&
                          actionMetadaOptions.find(am => am.value === dataSourceAction?.data.actionMetadata.name)
                        // if not add it in the options array
                        const options =
                          !existedOption && dataSourceAction?.data.actionMetadata && actionMetadaOptions
                            ? [
                                ...actionMetadaOptions,
                                {
                                  label: dataSourceAction.data.actionMetadata.displayName,
                                  value: dataSourceAction.data.actionMetadata.name,
                                },
                              ]
                            : actionMetadaOptions

                        return (
                          <NSingleSelect
                            options={options || []}
                            isSearchable
                            fullWidth
                            label="Action metadata"
                            placeholder="Select action metadata"
                            isLoading={isLoadingMetadataList}
                            searchValue={searchValue}
                            onSearchValueChange={handleSearchChange}
                            value={field.value}
                            onValueChange={value => {
                              field.onChange(value)
                              setSelectedAMName(value)
                              value !== selectedActionMetadata?.data.name && reset({ actionMetadataName: value })
                            }}
                          />
                        )
                      }}
                    />
                  </NColumn>
                  <NDivider vertical size="xl" />
                  <NColumn>
                    <NTextInput
                      type="number"
                      defaultValue={dataSourceAction?.data.statusCode || ''}
                      label="Status code"
                      placeholder="Status code"
                      {...register('statusCode')}
                    />
                  </NColumn>
                </NRow>
              </>
            )}
          </Section>
        </SettingBoard>
        {isLoadingSelectedAM ? (
          <LoadingView>
            <NSpinner size={30} strokeWidth={2} color={color('Primary900')({ theme })} />
          </LoadingView>
        ) : (
          <>
            {selectedActionMetadata?.data && dataSourceAction?.data && (
              <>
                <SettingBoard
                  title="Request"
                  headerComponent={
                    <ConfigTabs
                      tabOptions={requestTabs}
                      selectedTab={selectedRequestTab}
                      onSelectTab={setSelectedRequestTab}
                    />
                  }>
                  <Section>
                    {requestTabs.map(requestType => {
                      if (requestType.value === 'body') {
                        let bodyDefault: any = ''
                        if (selectedActionMetadata.data.inputs.body?.default) {
                          try {
                            bodyDefault = JSON.stringify(selectedActionMetadata.data.inputs.body?.default)
                          } catch {
                            bodyDefault = selectedActionMetadata.data.inputs.body?.default
                          }
                        }

                        return (
                          <InputMappingFormItem
                            key={requestType.value}
                            name={`${requestType.value}`}
                            control={control}
                            input={`${requestType.value}`}
                            requestVars={dataSourceAction.data.availableRequestVars}
                            visible={requestType.value === selectedRequestTab}
                            defaultValue={bodyDefault}
                            isDeprecated={deprecatedInputs && deprecatedInputs.body.length > 0}
                            onRemove={onRemoveDeprecated}
                          />
                        )
                      }
                      return (
                        <div key={requestType.value}>
                          {actionInputs?.[requestType.value].map((input: string) => (
                            <InputMappingFormItem
                              key={`${requestType.value}.${input}`}
                              name={`${requestType.value}.${input}`}
                              control={control}
                              input={input}
                              requestVars={dataSourceAction.data.availableRequestVars}
                              visible={requestType.value === selectedRequestTab}
                              //@ts-ignore
                              defaultValue={selectedActionMetadata.data.inputs[requestType.value]?.[input]?.default}
                            />
                          ))}
                          {deprecatedInputs?.[requestType.value].map((input: string) => (
                            <InputMappingFormItem
                              key={`${requestType.value}.${input}`}
                              name={`${requestType.value}.${input}`}
                              control={control}
                              input={input}
                              requestVars={dataSourceAction.data.availableRequestVars}
                              visible={requestType.value === selectedRequestTab}
                              isDeprecated={true}
                              onRemove={onRemoveDeprecated}
                            />
                          ))}
                        </div>
                      )
                    })}
                  </Section>
                </SettingBoard>
                <SettingBoard title="Response">
                  <Section>
                    <InputMapWrapper>
                      {dataSourceAction.data.availableResponseVars.map(response => (
                        <NTextInput
                          //@ts-ignore
                          defaultValue={dataSourceAction.data.responseMap[response] || ''}
                          key={response}
                          label={response}
                          {...register(`responseMap.${response}`)}
                        />
                      ))}
                    </InputMapWrapper>
                  </Section>
                </SettingBoard>
              </>
            )}
          </>
        )}
        <NDivider size="xl" />
      </form>
      {showTestModal && dataSourceAction?.data && (
        <TestEndpointModal
          datasourceAction={dataSourceAction.data}
          objectName={extObjData?.data.displayName}
          actionName={selectedTab}
          visible={showTestModal}
          setVisible={setShowTestModal}
        />
      )}
    </NContainer>
  )
}
