import * as React from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { useHistory, useParams } from 'react-router-dom'
import styled from 'styled-components'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { color, spacing } from '../../../../../components/GlobalStyle'
import { NBreadcrumbs } from '../../../../../components/NBreadcrumbs/NBreadcrumbs'
import { NButton } from '../../../../../components/NButton/NButton'
import { NDivider } from '../../../../../components/NDivider'
import { elevation } from '../../../../../components/NElevation'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { SelectOption } from '../../../../../components/NSelect/model'
import { NSpinner } from '../../../../../components/NSpinner/NSpinner'
import { NToast } from '../../../../../components/NToast'
import { typography } from '../../../../../components/NTypography'
import { APIServices } from '../../../../../services/api'
import { IntegrationsQueryKeys } from '../../../../../services/api/endpoints/integrations'
import {
  ActionMetadataCreateDto,
  ActionMetadataResponse,
  InputMapDto,
  RequestInputType,
} from '../../../../../services/api/models'
import { GeneralForm } from './components/GeneralForm'
import { InputForm } from './components/InputForm'
import { OutputForm } from './components/OutputForm'
import { ActionMetadataFieldValues } from './constants'

const LoadingContainer = styled(NContainer)`
  justify-content: center;
  align-items: center;
`

const HeaderWrapper = styled('div')`
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const HeaderTitle = styled('div')`
  font-size: 24px;
  font-weight: bold;
  margin-top: ${spacing('sm')};
`
const CreateNewTitle = styled('div')`
  color: ${color('Primary700')};
  ${typography('button')}
`

const ButtonWrapper = styled('div')`
  display: flex;
`

const FormWrapper = styled('div')`
  border-radius: 4px;
  background: ${color('white')};
  margin-top: ${spacing('xl')};
  display: flex;
  flex-direction: column;
  ${elevation('Elevation100')}
  & > :first-child {
    border-top-left-radius: 4px;
    border-top-right-radius: 4px;
  }
  & > :last-child {
    border-bottom-left-radius: 4px;
    border-bottom-right-radius: 4px;
  }
`

type ActionMetadataFormProps = {}

export const ActionMetadataForm = React.memo(function Node({}: ActionMetadataFormProps) {
  const { actionMetadataName } = useParams<{ actionMetadataName?: string }>()
  const history = useHistory()
  const queryClient = useQueryClient()

  const formMethods = useForm<ActionMetadataFieldValues>({
    shouldUnregister: false,
  })

  // get default value
  const { data, isLoading: isLoadingData } = useQuery(
    [IntegrationsQueryKeys.getSingleActionMetadata, { name: actionMetadataName! }],
    APIServices.Integrations.getSingleActionMetadata,
    {
      enabled: !!actionMetadataName,
      onError: error => {
        NToast.error({ title: 'Get action metadata errsor!', subtitle: error.response?.data.message })
      },
    },
  )

  React.useEffect(() => {
    if (data?.data) {
      formMethods.reset(transformResponseToValues(data.data))
    }
  }, [data])

  // get credentials
  const { data: credentialOptions, isLoading: isLoadingCredentials } = useQuery(
    [IntegrationsQueryKeys.getCredentials, { searchText: '' }],
    APIServices.Integrations.getCredentials,
    {
      select: data => {
        let options: SelectOption[] = []
        for (let credential of data.data.data) {
          options.push({ value: credential.name, label: credential.displayName })
        }
        return options
      },
      onError: error => {
        NToast.error({ title: 'Get credentials error!', subtitle: error.response?.data.message })
      },
    },
  )

  // create action metadata
  const { mutate: createActionMetadata, isLoading: isLoadingCreate } = useMutation(
    APIServices.Integrations.createActionMetadata,
    {
      onSuccess: () => {
        queryClient.invalidateQueries(IntegrationsQueryKeys.getAllActionMetadata)
        history.goBack()
      },
      onError: error => {
        NToast.error({ title: 'Create unsuccessful', subtitle: error.response?.data.message })
      },
    },
  )

  // edit action metadata
  const { mutate: updateActionMetadata, isLoading: isLoadingUpdate } = useMutation(
    APIServices.Integrations.updateActionMetadata,
    {
      onSuccess: () => {
        queryClient.invalidateQueries(IntegrationsQueryKeys.getAllActionMetadata)
        queryClient.invalidateQueries([IntegrationsQueryKeys.getSingleActionMetadata, { name: actionMetadataName! }])
        history.goBack()
      },
      onError: error => {
        NToast.error({ title: 'Edit unsuccessful', subtitle: error.response?.data.message })
      },
    },
  )

  const isLoading = isLoadingData || isLoadingCredentials
  const isMutating = isLoadingUpdate || isLoadingCreate
  const isCreateAction = !actionMetadataName
  const { isDirty } = formMethods.formState

  if (isLoading) {
    return (
      <LoadingContainer>
        <NSpinner size={40} strokeWidth={2} />
      </LoadingContainer>
    )
  }

  const handleSubmit = formMethods.handleSubmit((formData: ActionMetadataFieldValues) => {
    if (isCreateAction) {
      createActionMetadata(transformValuesToParams(formData))
      return
    }
    updateActionMetadata(transformValuesToParams(formData))
  })

  return (
    <NContainer>
      <HeaderWrapper>
        {isCreateAction && (
          <div>
            <CreateNewTitle>Create new</CreateNewTitle>
            <HeaderTitle>Action Metadata</HeaderTitle>
          </div>
        )}
        {!isCreateAction && (
          <div>
            <NBreadcrumbs
              breadcrumbs={[
                { path: DASHBOARD_ROUTE, label: 'Dashboard' },
                {
                  path: `${DASHBOARD_ROUTE}/integrations/action-metadata`,
                  label: 'Action Metadata',
                },
                { label: isLoadingData ? 'Loading ...' : data?.data.name || 'Unnamed' },
              ]}
            />
            <HeaderTitle>{data?.data.name} Details</HeaderTitle>
          </div>
        )}
        <ButtonWrapper>
          <NButton
            disabled={isLoading || isMutating}
            onClick={() => {
              if (!isCreateAction && isDirty && data) {
                formMethods.reset(transformResponseToValues(data.data))
                return
              }
              history.goBack()
            }}>
            {!isCreateAction && isDirty ? 'Reset' : 'Cancel'}
          </NButton>
          <NDivider vertical size="xl" />
          <NButton type="outline-2" loading={isMutating} disabled={isLoading} onClick={handleSubmit}>
            {isCreateAction ? 'Submit' : 'Save'}
          </NButton>
        </ButtonWrapper>
      </HeaderWrapper>
      <FormWrapper>
        <FormProvider {...formMethods}>
          <GeneralForm isCreateAction={isCreateAction} credentialOptions={credentialOptions} />
          <InputForm />
          <OutputForm />
        </FormProvider>
      </FormWrapper>
      <NDivider size="xxl" />
    </NContainer>
  )
})

ActionMetadataForm.displayName = 'ActionMetadataForm'

function transformValuesToParams({
  displayName,
  name,
  description,
  credentialName,
  path,
  method,
  outputs: outputValues,
  inputs: inputValues,
}: ActionMetadataFieldValues): ActionMetadataCreateDto {
  let params: ActionMetadataCreateDto = {
    displayName,
    name,
    description,
    credentialName,
    path,
    method,
  }

  let outputs: ActionMetadataCreateDto['outputs'] = {}
  for (let { name, ...output } of outputValues) {
    outputs[name] = output
  }

  const isRequiredBody = Boolean(inputValues.body.isRequired)
  let inputs: Partial<InputMapDto> = {
    body: { ...inputValues.body, isRequired: isRequiredBody },
  }

  let headerInput: InputMapDto['header'] = {}
  for (let { name, ...header } of inputValues?.header || []) {
    headerInput[name] = header
  }

  let queryInput: InputMapDto['query'] = {}
  for (let { name, ...query } of inputValues?.query || []) {
    queryInput[name] = query
  }

  inputs['header'] = headerInput
  inputs['query'] = queryInput

  params['inputs'] = inputs as InputMapDto
  params['outputs'] = outputs

  return params
}

function transformResponseToValues({
  name,
  displayName,
  description,
  credential,
  path,
  method,
  outputs: outputResponse,
  inputs: inputResponse,
}: ActionMetadataResponse): ActionMetadataFieldValues {
  let formData: ActionMetadataFieldValues = {
    name,
    displayName,
    description,
    credentialName: credential.name,
    path,
    method,
    inputs: {
      body: (inputResponse.body || {}) as RequestInputType,
      header: Object.entries(inputResponse.header).map(([name, value]) => ({ name, ...value })),
      query: Object.entries(inputResponse.query).map(([name, value]) => ({ name, ...value })),
    },
    outputs: Object.entries(outputResponse).map(([name, value]) => ({ name, ...value })),
  }
  return formData
}
