import React, { useMemo, useState } from 'react'
import { useQuery } from 'react-query'
import { NavLink, useHistory } from 'react-router-dom'
import { classnames } from '../../../../../common/classnames'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { NButton } from '../../../../../components/NButton/NButton'
import { NRow } from '../../../../../components/NGrid/NGrid'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { NPagination } from '../../../../../components/NPagination'
import { NTable, NTableColumnType } from '../../../../../components/NTable/NTable'
import { NTableCollapsedCellClassName } from '../../../../../components/NTable/NTableStyledContainer'
import { NTableToolbar } from '../../../../../components/NTable/NTableToolbar'
import { useTableConfigs } from '../../../../../hooks/useTableConfigs'
import { APIServices } from '../../../../../services/api'
import { IntegrationsQueryKeys } from '../../../../../services/api/endpoints/integrations'
import { ActionMetadataResponse } from '../../../../../services/api/models'
import { getTableData } from '../../../../../utils/table-utils'
import { ActionMetadataTableAction } from './ActionMetadataTableAction'

const PAGE_SIZE = 10

type ObjectProps = {
  children?: React.ReactNode
}

const COLUMNS: NTableColumnType<ActionMetadataResponse> = [
  {
    Header: 'Name',
    accessor: 'name',
    defaultCanSort: true,
  },
  {
    Header: 'Description',
    accessor: 'description',
  },
  {
    Header: 'Method',
    accessor: 'method',
    defaultCanSort: true,
  },
  {
    Header: 'Created at',
    accessor: 'createdAt',
    Cell: ({ value }) => {
      if (!value) {
        return null
      }
      const date = new Date(value)
      return date.toLocaleString('vi')
    },
    defaultCanSort: true,
  },
  {
    Header: 'Last modified',
    accessor: 'updatedAt',
    Cell: ({ value }) => {
      if (!value) {
        return null
      }
      const date = new Date(value)
      return date.toLocaleString('vi')
    },
    defaultCanSort: true,
  },

  {
    accessor: 'name',
    Cell: ({ row }) => {
      return <ActionMetadataTableAction data={row.original} />
    },
    id: classnames([NTableCollapsedCellClassName]),
  },
]

export function ActionMetadataListing({}: ObjectProps) {
  const [
    { sortBy, sortOrder, searchText, searchValue, page: currentPage },
    { onChangeSort, onChangePage, onChangeSearch },
  ] = useTableConfigs()
  const history = useHistory()

  const [selectedRows, setSelectedRow] = useState<ActionMetadataResponse[]>([])
  const { data, isFetching } = useQuery(
    [
      IntegrationsQueryKeys.getAllActionMetadata,
      { limit: PAGE_SIZE, offset: (currentPage - 1) * PAGE_SIZE, searchText, sortBy, sortOrder },
    ],
    APIServices.Integrations.getAllActionMetadata,
    { keepPreviousData: true },
  )

  const { pageData, pageInfo, totalPage } = useMemo(() => {
    return getTableData(data)
  }, [data])

  // test on select
  const rowOnSelect = (selected: ActionMetadataResponse[]) => {
    setSelectedRow(selected)
  }

  return (
    <NContainer>
      <NRow justify="space-between">
        <NContainer.Title>Action Metadata Listing</NContainer.Title>
        <NavLink to={`${DASHBOARD_ROUTE}/integrations/action-metadata/create`}>
          <NButton type="primary">Create Action Metadata</NButton>
        </NavLink>
      </NRow>
      <NContainer.Description>{pageInfo.total} action metadata</NContainer.Description>
      <NContainer.Content>
        <NTableToolbar
          searchConfig={{
            value: searchValue,
            onChange: e => {
              onChangeSearch(e.target.value)
            },
          }}
          selectedConfig={{
            selected: selectedRows,
            onDeSelectAll: () => {},
            onDelete: selected => {
              console.log(`Delete ${selected.length} rows`)
            },
          }}
        />
        <NTable
          isLoading={isFetching}
          columns={COLUMNS}
          data={pageData}
          rowSelectionConfig={{
            onChange: rowOnSelect,
          }}
          defaultSortBy={sortBy}
          defaultSortOrder={sortOrder}
          onChangeSort={onChangeSort}
          pageSize={PAGE_SIZE}
          pagination={<NPagination total={totalPage} current={currentPage} onChangePage={onChangePage} />}
          onClickRow={value => {
            history.push(`${DASHBOARD_ROUTE}/integrations/action-metadata/${value.name}`)
          }}
        />
      </NContainer.Content>
    </NContainer>
  )
}
