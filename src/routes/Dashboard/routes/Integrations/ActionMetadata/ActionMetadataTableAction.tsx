import React, { FC, useState } from 'react'
import { useMutation, useQueryClient } from 'react-query'
import { NTableActions } from '../../../../../components/NTable/NTableActions'
import { APIServices } from '../../../../../services/api'
import { IntegrationsQueryKeys } from '../../../../../services/api/endpoints/integrations'
import { ActionMetadataResponse } from '../../../../../services/api/models'

type ActionMetadataTableActionProps = {
  data: ActionMetadataResponse
}
export const ActionMetadataTableAction: FC<ActionMetadataTableActionProps> = ({ data }) => {
  const [showDropList, setShowDropList] = useState(false)
  // post object mutation
  const [isLoading, setIsLoading] = useState(false)
  const queryClient = useQueryClient()

  const { mutate: deleteActionMetadata } = useMutation(APIServices.Integrations.deleteActionMetadata, {
    onSuccess: () => {
      queryClient.invalidateQueries(IntegrationsQueryKeys.getAllActionMetadata)
    },
  })

  const handleDelete = () => {
    setIsLoading(true)
    deleteActionMetadata({ name: data.name })
  }
  return (
    <NTableActions
      showDropList={showDropList}
      setShowDropList={setShowDropList}
      options={[{ title: 'Delete', isLoading, onClick: handleDelete }]}
    />
  )
}
