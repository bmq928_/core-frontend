import { HeaderInputType, RequestInputType, ResponseOutputType } from '../../../../../services/api/models'

export const RULES = { required: { value: true, message: 'Required!' } }
export const DATA_TYPE_OPTIONS = [
  { label: 'String', value: 'string' },
  { label: 'Number', value: 'number' },
  { label: 'Object', value: 'object' },
  { label: 'Array', value: 'array' },
  { label: 'Boolean', value: 'boolean' },
]

export type DefaultType = string | number | object | boolean | (string | number | object | boolean)[]
type ResponseOutput = { name: string } & ResponseOutputType
type HeaderInput = { name: string } & HeaderInputType
type RequestInput = { name: string } & RequestInputType

export type InputMapFieldValues = {
  header: Array<HeaderInput>
  query: Array<RequestInput>
  body: RequestInputType
}

export type ActionMetadataFieldValues = {
  name: string
  displayName: string
  description?: string
  credentialName: string
  path: string
  method: 'post' | 'get' | 'put' | 'delete'
  inputs: InputMapFieldValues
  outputs: Array<ResponseOutput>
}
