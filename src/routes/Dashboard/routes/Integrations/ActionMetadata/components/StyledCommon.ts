import styled from 'styled-components'
import { spacing } from '../../../../../../components/GlobalStyle'
import { typography } from '../../../../../../components/NTypography'

export const AccordionContent = styled('div')`
  padding: ${spacing('xl')};
`

export const AccordionTitle = styled('div')`
  ${typography('h500')}
`

export const InputItemWrapper = styled('div')`
  border-radius: 6px;
  border: 1px solid #eee;
  padding: ${spacing('xl')};
  &:not(:last-child) {
    margin-bottom: ${spacing('xl')};
  }
`
