import * as React from 'react'
import { Controller, useFormContext } from 'react-hook-form'
import { NAccordion } from '../../../../../../components/NAccordion/NAccordion'
import { NDivider } from '../../../../../../components/NDivider'
import { NColumn, NRow } from '../../../../../../components/NGrid/NGrid'
import { SelectOption } from '../../../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../../../components/NSelect/NSingleSelect'
import { NTextArea } from '../../../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../../../components/NTextInput/NTextInput'
import { getNameFromDisplayName } from '../../../../../../utils/utils'
import { ActionMetadataFieldValues, RULES } from '../constants'
import { AccordionContent, AccordionTitle } from './StyledCommon'

const METHOD_OPTIONS: SelectOption[] = [
  { label: 'Post', value: 'post' },
  { label: 'Get', value: 'get' },
  { label: 'Put', value: 'put' },
  { label: 'Delete', value: 'delete' },
]

type GeneralFormProps = {
  isCreateAction: boolean
  credentialOptions?: SelectOption[]
}

export const GeneralForm = React.memo(function Node({ isCreateAction, credentialOptions = [] }: GeneralFormProps) {
  const { control, getValues, setValue } = useFormContext<ActionMetadataFieldValues>()

  return (
    <NAccordion title={<AccordionTitle>General</AccordionTitle>}>
      <AccordionContent>
        <NRow>
          <NColumn>
            <Controller
              control={control}
              name="displayName"
              render={({ field, fieldState }) => (
                <NTextInput
                  required
                  error={fieldState.error?.message}
                  label="Display name"
                  value={field.value}
                  onChange={e => field.onChange(e.target.value)}
                  onBlur={e => {
                    if (isCreateAction && !getValues('name')) {
                      setValue('name', getNameFromDisplayName(e.target.value), {
                        shouldDirty: true,
                        shouldValidate: true,
                      })
                    }
                  }}
                />
              )}
              rules={RULES}
            />
          </NColumn>
          <NDivider vertical size="xl" />
          <NColumn>
            <Controller
              control={control}
              name="name"
              render={({ field, fieldState }) => (
                <NTextInput
                  required
                  error={fieldState.error?.message}
                  label="Name"
                  value={field.value}
                  onChange={e => field.onChange(e.target.value)}
                  disabled={!isCreateAction}
                />
              )}
              rules={RULES}
            />
          </NColumn>
        </NRow>
        <NDivider size="xl" />
        <Controller
          control={control}
          name="description"
          render={({ field }) => (
            <NTextArea label="Description" value={field.value} onChange={e => field.onChange(e.target.value)} />
          )}
        />
        <NDivider size="xl" />
        <NRow>
          <NColumn>
            <Controller
              name="credentialName"
              rules={RULES}
              control={control}
              render={({ field, fieldState }) => {
                return (
                  <NSingleSelect
                    required
                    fullWidth
                    label="Pick credential"
                    options={credentialOptions}
                    value={field.value}
                    error={fieldState.error?.message}
                    onValueChange={field.onChange}
                  />
                )
              }}
            />
          </NColumn>
          <NDivider vertical size="xl" />
          <NColumn>
            <Controller
              name="method"
              control={control}
              render={({ field, fieldState }) => {
                return (
                  <NSingleSelect
                    fullWidth
                    required
                    label="Method"
                    options={METHOD_OPTIONS}
                    value={field.value}
                    error={fieldState.error?.message}
                    onValueChange={field.onChange}
                  />
                )
              }}
            />
          </NColumn>
        </NRow>
        <NDivider size="xl" />
        <Controller
          control={control}
          name="path"
          render={({ field, fieldState }) => (
            <NTextInput
              required
              error={fieldState.error?.message}
              label="Url Path"
              value={field.value}
              onChange={e => field.onChange(e.target.value)}
              onBlur={field.onBlur}
            />
          )}
          rules={RULES}
        />
      </AccordionContent>
    </NAccordion>
  )
})

GeneralForm.displayName = 'GeneralForm'
