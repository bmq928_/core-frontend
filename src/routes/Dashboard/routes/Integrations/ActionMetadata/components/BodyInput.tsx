import * as React from 'react'
import { Controller, useFormContext } from 'react-hook-form'
import { NCheckbox } from '../../../../../../components/NCheckbox/NCheckbox'
import { NDivider } from '../../../../../../components/NDivider'
import { NSingleSelect } from '../../../../../../components/NSelect/NSingleSelect'
import { NTextInput } from '../../../../../../components/NTextInput/NTextInput'
import { ActionMetadataFieldValues, DATA_TYPE_OPTIONS, DefaultType, RULES } from '../constants'

type BodyInputProps = {
  children?: React.ReactNode
}

export const BodyInput = React.memo(function Node({}: BodyInputProps) {
  const { control, watch, setValue } = useFormContext<ActionMetadataFieldValues>()
  const name = 'inputs.body'
  const field = watch(name)
  const { type } = field || {}
  return (
    <div>
      <Controller
        name={`${name}.type`}
        control={control}
        render={({ field: { value, onChange } }) => (
          <NSingleSelect
            fullWidth
            label="Type"
            options={DATA_TYPE_OPTIONS}
            value={value || ''}
            onValueChange={newValue => {
              onChange(newValue)
              if (newValue) {
                const defaultValue: Record<string, DefaultType> = {
                  checkbox: false,
                  array: [],
                  object: {},
                }
                setValue(`${name}.default`, defaultValue[newValue])
              }
            }}
          />
        )}
        rules={RULES}
      />
      {type && type !== 'object' && type !== 'array' && (
        <React.Fragment>
          <NDivider size="xl" />
          <Controller
            name={`${name}.default`}
            control={control}
            render={({ field: { value, onChange } }) => {
              if (type === 'number') {
                return (
                  <NTextInput
                    label="Default Value"
                    type="number"
                    value={(value as number) || ''}
                    onChange={e => {
                      if (e.target.value) {
                        onChange(+e.target.value)
                        return
                      }
                      onChange('')
                    }}
                    onBlur={e => {
                      if (!e.target.value) {
                        onChange(0)
                      }
                    }}
                  />
                )
              }
              if (type === 'boolean') {
                return (
                  <NCheckbox
                    title="Default Value"
                    checked={Boolean(value)}
                    onChange={e => onChange(e.target.checked)}
                  />
                )
              }
              return (
                <NTextInput
                  label="Default Value"
                  value={(value as string) || ''}
                  onChange={e => onChange(e.target.value)}
                />
              )
            }}
          />
          <NDivider size="xl" />

          <Controller
            control={control}
            name={`${name}.isRequired`}
            render={({ field: { value, onChange } }) => (
              <NCheckbox title="Required" checked={Boolean(value)} onChange={e => onChange(e.target.checked)} />
            )}
          />
        </React.Fragment>
      )}
    </div>
  )
})

BodyInput.displayName = 'BodyInput'
