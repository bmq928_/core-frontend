import * as React from 'react'
import { Controller, useFieldArray, useFormContext } from 'react-hook-form'
import { GlobalIcons } from '../../../../../../components/Icons'
import { NButton } from '../../../../../../components/NButton/NButton'
import { NCheckbox } from '../../../../../../components/NCheckbox/NCheckbox'
import { NDivider } from '../../../../../../components/NDivider'
import { NColumn, NRow } from '../../../../../../components/NGrid/NGrid'
import { NSingleSelect } from '../../../../../../components/NSelect/NSingleSelect'
import { NTextArea } from '../../../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../../../components/NTextInput/NTextInput'
import { ActionMetadataFieldValues, DATA_TYPE_OPTIONS, DefaultType, RULES } from '../constants'
import { InputItemWrapper } from './StyledCommon'

type QueryInputProps = {}

export const QueryInput = React.memo(function Node({}: QueryInputProps) {
  const { control, watch, setValue } = useFormContext<ActionMetadataFieldValues>()
  const name = 'inputs.query'
  const { fields, remove, append } = useFieldArray({ control, name })
  return (
    <React.Fragment>
      {fields.map((field, fieldIndex) => {
        const type = watch(`${name}.${fieldIndex}`)
        return (
          <InputItemWrapper key={field.id}>
            <NRow align="flex-end">
              <NColumn>
                <Controller
                  name={`${name}.${fieldIndex}.name`}
                  control={control}
                  render={({ field: { value, onChange }, fieldState }) => (
                    <NTextInput
                      value={value}
                      onChange={e => onChange(e.target.value)}
                      label="Name"
                      required
                      error={fieldState.error?.message}
                    />
                  )}
                  rules={RULES}
                />
              </NColumn>
              <NDivider size="xl" vertical />
              <NColumn>
                <Controller
                  name={`${name}.${fieldIndex}.type`}
                  control={control}
                  render={({ field: { value, onChange } }) => (
                    <NSingleSelect
                      fullWidth
                      label="Type"
                      options={DATA_TYPE_OPTIONS}
                      value={value || ''}
                      onValueChange={newValue => {
                        onChange(newValue)
                        if (newValue) {
                          const defaultValue: Record<string, DefaultType> = {
                            checkbox: false,
                            array: [],
                            object: {},
                          }
                          setValue(`${name}.${fieldIndex}.default`, defaultValue[newValue])
                        }
                      }}
                    />
                  )}
                  rules={RULES}
                />
              </NColumn>
              <NDivider vertical size="xl" />
              <div className="delete-btn">
                <NButton
                  className="delete-btn"
                  type="ghost"
                  icon={<GlobalIcons.Trash />}
                  onClick={() => {
                    remove(fieldIndex)
                  }}
                />
              </div>
            </NRow>
            <NDivider size="xl" />
            <Controller
              control={control}
              name={`${name}.${fieldIndex}.description`}
              render={({ field: { value, onChange } }) => (
                <NTextArea label="Description" value={value || ''} onChange={e => onChange(e.target.value)} />
              )}
            />
            <NDivider size="xl" />
            <Controller
              control={control}
              name={`${name}.${fieldIndex}.isRequired`}
              render={({ field: { value, onChange } }) => (
                <NCheckbox title="Required" checked={Boolean(value)} onChange={e => onChange(e.target.checked)} />
              )}
            />
            {type !== 'object' && type !== 'array' && (
              <React.Fragment>
                <NDivider size="xl" />
                <Controller
                  name={`${name}.${fieldIndex}.default`}
                  control={control}
                  render={({ field: { value, onChange } }) => {
                    if (type === 'number') {
                      return (
                        <NTextInput
                          label="Default Value"
                          type="number"
                          value={(value as number) || ''}
                          onChange={e => {
                            if (e.target.value) {
                              onChange(+e.target.value)
                              return
                            }
                            onChange('')
                          }}
                          onBlur={e => {
                            if (!e.target.value) {
                              onChange(0)
                            }
                          }}
                        />
                      )
                    }
                    if (type === 'boolean') {
                      return (
                        <NCheckbox
                          title="Default Value"
                          checked={Boolean(value)}
                          onChange={e => onChange(e.target.checked)}
                        />
                      )
                    }
                    return (
                      <NTextInput
                        label="Default Value"
                        value={(value as string) || ''}
                        onChange={e => onChange(e.target.value)}
                      />
                    )
                  }}
                />
              </React.Fragment>
            )}
          </InputItemWrapper>
        )
      })}
      <NButton type="primary" size="small" onClick={() => append({ type: 'string', isRequired: false })}>
        <GlobalIcons.Plus />
      </NButton>
    </React.Fragment>
  )
})

QueryInput.displayName = 'QueryInput'
