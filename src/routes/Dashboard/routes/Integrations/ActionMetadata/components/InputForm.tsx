import * as React from 'react'
import styled from 'styled-components'
import { color } from '../../../../../../components/GlobalStyle'
import { NAccordion } from '../../../../../../components/NAccordion/NAccordion'
import { NDivider } from '../../../../../../components/NDivider'
import { typography } from '../../../../../../components/NTypography'
import { BodyInput } from './BodyInput'
import { HeaderInput } from './HeaderInput'
import { QueryInput } from './QueryInput'
import { AccordionContent, AccordionTitle } from './StyledCommon'

const TitleWrapper = styled('div')`
  display: flex;
  align-items: center;
`

const GroupButton = styled('div')`
  display: flex;
  padding: 2px;
  height: 24px;
  align-items: center;
  border-radius: 6px;
  background-color: ${color('Neutral200')};
  &:not(:last-child) {
    margin-right: 2px;
  }
`

const Button = styled('div')`
  border-radius: 4px;
  cursor: pointer;
  padding: 3px 8px;
  &.active {
    background: ${color('white')};
  }
  ${typography('small-overline')}
`

type InputFormProps = {}

export const InputForm = React.memo(function Node({}: InputFormProps) {
  const [inputType, setInputType] = React.useState<'query' | 'body' | 'header'>('query')
  return (
    <NAccordion
      title={
        <TitleWrapper>
          <AccordionTitle>Input</AccordionTitle>
          <NDivider vertical size="xl" />
          <GroupButton
            onClick={e => {
              e.stopPropagation()
            }}>
            <Button className={inputType === 'query' ? 'active' : undefined} onClick={() => setInputType('query')}>
              Query
            </Button>
            <Button className={inputType === 'body' ? 'active' : undefined} onClick={() => setInputType('body')}>
              Body
            </Button>
            <Button className={inputType === 'header' ? 'active' : undefined} onClick={() => setInputType('header')}>
              Header
            </Button>
          </GroupButton>
        </TitleWrapper>
      }>
      <AccordionContent>
        {inputType === 'query' && <QueryInput />}
        {inputType === 'body' && <BodyInput />}
        {inputType === 'header' && <HeaderInput />}
      </AccordionContent>
    </NAccordion>
  )
})

InputForm.displayName = 'InputForm'
