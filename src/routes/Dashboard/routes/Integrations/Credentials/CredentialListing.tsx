import { FC, useMemo, useState } from 'react'
import { useQuery } from 'react-query'
import { useHistory } from 'react-router-dom'
import { classnames } from '../../../../../common/classnames'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { NButton } from '../../../../../components/NButton/NButton'
import { NRow } from '../../../../../components/NGrid/NGrid'
import { NHyperlinkOrText } from '../../../../../components/NHyperlinkOrText'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { NPagination } from '../../../../../components/NPagination'
import { NTable, NTableColumnType } from '../../../../../components/NTable/NTable'
import { NTableCollapsedCellClassName } from '../../../../../components/NTable/NTableStyledContainer'
import { NTableToolbar } from '../../../../../components/NTable/NTableToolbar'
import { useTableConfigs } from '../../../../../hooks/useTableConfigs'
import { APIServices } from '../../../../../services/api'
import { IntegrationsQueryKeys } from '../../../../../services/api/endpoints/integrations'
import { CredentialResponse } from '../../../../../services/api/models'
import { getTableData } from '../../../../../utils/table-utils'
import { CreateCredentialModal } from './components/CreateCredentialModal'
import { CredentialTableAction } from './components/CredentialTableAction'

const PAGE_SIZE = 10

const COLUMNS: NTableColumnType<CredentialResponse> = [
  { Header: 'Display name', accessor: 'displayName', defaultCanSort: true },
  {
    Header: 'Name',
    accessor: 'name',
    defaultCanSort: true,
  },
  {
    Header: 'Description',
    accessor: 'description',
  },
  {
    Header: 'Url',
    accessor: 'url',
    Cell: ({ value }) => {
      return <NHyperlinkOrText>{value}</NHyperlinkOrText>
    },
    defaultCanSort: true,
  },
  {
    Header: 'Auth Protocol',
    accessor: 'authProtocol',
    defaultCanSort: true,
  },
  {
    Header: 'Identity Type',
    accessor: 'identityType',
    defaultCanSort: true,
  },
  {
    Header: 'Created at',
    accessor: 'createdAt',
    Cell: ({ value }) => {
      if (!value) {
        return null
      }
      const date = new Date(value)
      return date.toLocaleString('vi')
    },
    defaultCanSort: true,
  },
  {
    Header: 'Last modified',
    accessor: 'updatedAt',
    Cell: ({ value }) => {
      if (!value) {
        return null
      }
      const date = new Date(value)
      return date.toLocaleString('vi')
    },
    defaultCanSort: true,
  },
  {
    accessor: 'name',
    Cell: ({ value }) => {
      return <CredentialTableAction value={value} />
    },
    id: classnames([NTableCollapsedCellClassName]),
  },
]

export const CredentialListing: FC = () => {
  const [
    { sortBy, sortOrder, searchText, searchValue, page: currentPage },
    { onChangeSort, onChangePage, onChangeSearch },
  ] = useTableConfigs()
  const [selectedRows, setSelectedRow] = useState<CredentialResponse[]>([])
  const history = useHistory()
  const [showCreateCredential, setShowCreateCredential] = useState(false)

  const { data: credentialsData, isLoading: isLoadingCredentials } = useQuery(
    [
      IntegrationsQueryKeys.getCredentials,
      {
        searchText: searchText,
        limit: PAGE_SIZE,
        offset: (currentPage - 1) * PAGE_SIZE,
        sortBy,
        sortOrder,
      },
    ],
    APIServices.Integrations.getCredentials,
  )

  const rowOnSelect = (selected: CredentialResponse[]) => {
    setSelectedRow(selected)
  }

  const { pageData, totalPage, pageInfo } = useMemo(() => {
    return getTableData(credentialsData)
  }, [credentialsData])

  return (
    <NContainer>
      <NRow justify="space-between">
        <NContainer.Title>Credential Listing</NContainer.Title>
        <NButton type="primary" onClick={() => setShowCreateCredential(true)}>
          Create Credential
        </NButton>
      </NRow>
      <NContainer.Description>{pageInfo.total} credentials</NContainer.Description>
      <NTableToolbar
        searchConfig={{
          value: searchValue,
          onChange: e => {
            onChangeSearch(e.target.value)
          },
        }}
        selectedConfig={{
          selected: selectedRows,
          onDeSelectAll: () => {},
          onDelete: selected => {
            console.log(`Delete ${selected.length} rows`)
          },
        }}
      />
      <NTable
        isLoading={isLoadingCredentials}
        columns={COLUMNS}
        data={pageData}
        rowSelectionConfig={{ onChange: rowOnSelect }}
        pageSize={PAGE_SIZE}
        pagination={<NPagination total={totalPage} current={currentPage} onChangePage={onChangePage} />}
        onClickRow={data => {
          history.push(`${DASHBOARD_ROUTE}/integrations/credentials/${data.name}`)
        }}
        defaultSortBy={sortBy}
        defaultSortOrder={sortOrder}
        onChangeSort={onChangeSort}
      />
      {showCreateCredential && (
        <CreateCredentialModal visible={showCreateCredential} setVisible={setShowCreateCredential} />
      )}
    </NContainer>
  )
}
