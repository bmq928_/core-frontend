import React, { FC } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useMutation, useQueryClient } from 'react-query'
import styled from 'styled-components'
import { spacing } from '../../../../../../components/GlobalStyle'
import { NDivider } from '../../../../../../components/NDivider'
import { NColumn, NRow } from '../../../../../../components/NGrid/NGrid'
import { NModal } from '../../../../../../components/NModal/NModal'
import { NSingleSelect } from '../../../../../../components/NSelect/NSingleSelect'
import { NTextInput } from '../../../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../../../components/NToast'
import { useValidateString } from '../../../../../../hooks/useValidateString'
import { APIServices } from '../../../../../../services/api'
import { IntegrationsQueryKeys } from '../../../../../../services/api/endpoints/integrations'
import { CredentialCreateDto } from '../../../../../../services/api/models'
import { fieldNameRegex } from '../../../../../../utils/regex'
import { getNameFromDisplayName } from '../../../../../../utils/utils'
import { AUTH_PROTOCOLS, IDENTITY_TYPES } from '../../model'

const CredentialForm = styled.form`
  padding: ${spacing('lg')} ${spacing('xl')};
`

type Props = {
  visible: boolean
  setVisible: (visible: boolean) => void
}

export const CreateCredentialModal: FC<Props> = ({ visible, setVisible }) => {
  const { register, setValue, control, handleSubmit, formState, getValues, watch } = useForm<CredentialCreateDto>({
    defaultValues: {
      name: '',
      displayName: '',
      url: '',
      identityType: IDENTITY_TYPES[0],
    },
  })
  const queryClient = useQueryClient()
  const { validateFunction } = useValidateString()
  const watchIdentityType = watch('identityType')
  const watchAuthProtocol = watch('authProtocol')

  const { mutate: createCredential, isLoading: isCreating } = useMutation(APIServices.Integrations.createCredential, {
    onSuccess: async () => {
      await queryClient.invalidateQueries([IntegrationsQueryKeys.getCredentials])
      setVisible(false)
    },
    onError: error => {
      NToast.error({ title: 'Create credential', subtitle: error.response?.data.message })
    },
  })

  const onSubmit = (data: CredentialCreateDto) => {
    createCredential(data)
  }

  return (
    <NModal size="large" visible={visible} setVisible={setVisible}>
      <NModal.Header title="Credential" onClose={() => setVisible(false)} />
      <NModal.Body>
        <CredentialForm>
          <NRow>
            <NColumn>
              <NTextInput
                required
                {...register('displayName', {
                  validate: validateFunction,
                })}
                label="Display name"
                error={formState.errors?.displayName?.message}
                onBlur={e => {
                  const formatName = getNameFromDisplayName(e.target.value)
                  !getValues('name') && setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
                }}
              />
            </NColumn>
            <NDivider vertical size="xl" />
            <NColumn>
              <NTextInput
                required
                label="API name"
                error={formState.errors?.name?.message}
                {...register('name', {
                  validate: validateFunction,
                  pattern: {
                    value: fieldNameRegex,
                    message: 'Invalid pattern',
                  },
                })}
                caption="The API name is used to generate the API routes and databases tables/collections"
              />
            </NColumn>
          </NRow>
          <NDivider size="xl" />
          <NRow>
            <NColumn>
              <NTextInput
                required
                {...register('url', {
                  validate: validateFunction,
                })}
                label="URL"
                error={formState.errors?.url?.message}
              />
            </NColumn>
          </NRow>
          <NDivider size="xl" />
          <NRow>
            <NColumn>
              <Controller
                control={control}
                name="identityType"
                render={({ field }) => (
                  <NSingleSelect
                    fullWidth
                    options={IDENTITY_TYPES.map(type => ({ value: type }))}
                    label="Identity type"
                    value={field.value}
                    onValueChange={value => {
                      if (value === 'anon') {
                        setValue('credential', undefined)
                        setValue('authProtocol', undefined)
                      }
                      field.onChange(value)
                    }}
                  />
                )}
              />
            </NColumn>
            <NDivider vertical size="xl" />
            <NColumn>
              {watchIdentityType !== 'anon' && (
                <Controller
                  control={control}
                  name="authProtocol"
                  render={({ field }) => (
                    <NSingleSelect
                      fullWidth
                      options={AUTH_PROTOCOLS.map(item => ({ value: item as string }))}
                      label="Auth protocol"
                      value={field.value}
                      onValueChange={field.onChange}
                    />
                  )}
                />
              )}
            </NColumn>
          </NRow>
          {watchIdentityType !== 'anon' && watchAuthProtocol === 'password' && (
            <>
              <NDivider size="xl" />
              <NRow>
                <NColumn>
                  <NTextInput
                    required
                    {...register('credential.username', {
                      validate: validateFunction,
                    })}
                    label="Username"
                    error={formState.errors.credential?.username?.message}
                  />
                </NColumn>
                <NDivider vertical size="xl" />
                <NColumn>
                  <NTextInput label="Password" type="password" {...register('credential.password')} />
                </NColumn>
              </NRow>
            </>
          )}
        </CredentialForm>
      </NModal.Body>
      <NModal.Footer isLoading={isCreating} onFinish={handleSubmit(onSubmit)} onCancel={() => setVisible(false)} />
    </NModal>
  )
}
