import { FC, useState } from 'react'
import { useMutation, useQueryClient } from 'react-query'
import { NTableActions } from '../../../../../../components/NTable/NTableActions'
import { NToast } from '../../../../../../components/NToast'
import { APIServices } from '../../../../../../services/api'
import { IntegrationsQueryKeys } from '../../../../../../services/api/endpoints/integrations'

type Props = {
  value: string
}

export const CredentialTableAction: FC<Props> = ({ value }) => {
  const [showDropList, setShowDropList] = useState(false)
  const queryClient = useQueryClient()
  const { mutate: deleteCredential, isLoading: isDeleting } = useMutation(APIServices.Integrations.deleteCredential, {
    onSuccess: async () => {
      await queryClient.invalidateQueries([IntegrationsQueryKeys.getCredentials])
    },
    onError: error => {
      NToast.error({ title: 'Create credential', subtitle: error.response?.data.message })
    },
  })

  const handleDelete = () => {
    deleteCredential({ name: value })
    setShowDropList(false)
  }

  return (
    <NTableActions
      options={[{ title: 'Delete', onClick: handleDelete, isLoading: isDeleting }]}
      showDropList={showDropList}
      setShowDropList={setShowDropList}
    />
  )
}
