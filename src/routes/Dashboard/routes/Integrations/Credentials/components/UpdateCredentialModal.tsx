import { FC } from 'react'
import { useForm } from 'react-hook-form'
import { useMutation, useQueryClient } from 'react-query'
import { useParams } from 'react-router-dom'
import styled from 'styled-components'
import { spacing } from '../../../../../../components/GlobalStyle'
import { NDivider } from '../../../../../../components/NDivider'
import { NColumn, NRow } from '../../../../../../components/NGrid/NGrid'
import { NModal } from '../../../../../../components/NModal/NModal'
import { NTextInput } from '../../../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../../../components/NToast'
import { useValidateString } from '../../../../../../hooks/useValidateString'
import { APIServices } from '../../../../../../services/api'
import { IntegrationsQueryKeys } from '../../../../../../services/api/endpoints/integrations'
import { CredentialResponse, CredentialUpdateDto } from '../../../../../../services/api/models'

const CredentialForm = styled.form`
  padding: ${spacing('lg')} ${spacing('xl')};
`

type Props = {
  visible: boolean
  setVisible: (visible: boolean) => void
  credential?: CredentialResponse
}

export const UpdateCredentialModal: FC<Props> = ({ visible, setVisible, credential }) => {
  const { register, handleSubmit, formState } = useForm({
    defaultValues: {
      displayName: credential?.displayName || '',
      url: credential?.url || '',
    },
  })
  const { credentialName } = useParams<{ credentialName: string }>()
  const queryClient = useQueryClient()
  const { validateFunction } = useValidateString()

  const { mutate: updateCredential, isLoading: isUpdating } = useMutation(APIServices.Integrations.updateCredential, {
    onSuccess: async () => {
      await queryClient.invalidateQueries([IntegrationsQueryKeys.getCredentials])
      await queryClient.invalidateQueries([IntegrationsQueryKeys.getCredential, { name: credentialName }])
      setVisible(false)
    },
    onError: error => {
      NToast.error({ title: 'Update credential', subtitle: error.response?.data.message })
    },
  })
  const onSubmit = (data: CredentialUpdateDto) => {
    updateCredential({
      name: credentialName,
      ...data,
    })
  }

  return (
    <NModal size="large" visible={visible} setVisible={setVisible}>
      <NModal.Header title="Change authentication" onClose={() => setVisible(false)} />
      <NModal.Body>
        <CredentialForm>
          <NRow>
            <NColumn>
              <NTextInput
                required
                {...register('displayName', {
                  validate: validateFunction,
                })}
                label="Display name"
                error={formState.errors?.displayName?.message}
              />
            </NColumn>
            <NDivider vertical size="xl" />
            <NColumn>
              <NTextInput
                value={credential?.name}
                label="API name"
                name="name"
                disabled
                caption="The API name is used to generate the API routes and databases tables/collections"
              />
            </NColumn>
          </NRow>
          <NDivider size="xl" />
          <NRow>
            <NColumn>
              <NTextInput
                required
                {...register('url', {
                  validate: validateFunction,
                })}
                label="URL"
                error={formState.errors?.url?.message}
              />
            </NColumn>
          </NRow>
        </CredentialForm>
      </NModal.Body>
      <NModal.Footer isLoading={isUpdating} onFinish={handleSubmit(onSubmit)} onCancel={() => setVisible(false)} />
    </NModal>
  )
}
