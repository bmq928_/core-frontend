import { FC } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useMutation, useQueryClient } from 'react-query'
import { useParams } from 'react-router-dom'
import styled from 'styled-components'
import { spacing } from '../../../../../../components/GlobalStyle'
import { NDivider } from '../../../../../../components/NDivider'
import { NColumn, NRow } from '../../../../../../components/NGrid/NGrid'
import { NModal } from '../../../../../../components/NModal/NModal'
import { NSingleSelect } from '../../../../../../components/NSelect/NSingleSelect'
import { NTextInput } from '../../../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../../../components/NToast'
import { useValidateString } from '../../../../../../hooks/useValidateString'
import { APIServices } from '../../../../../../services/api'
import { IntegrationsQueryKeys } from '../../../../../../services/api/endpoints/integrations'
import { CredentialUpdateDto } from '../../../../../../services/api/models'
import { AUTH_PROTOCOLS, IDENTITY_TYPES } from '../../model'

const CredentialForm = styled.form`
  padding: ${spacing('lg')} ${spacing('xl')};
`

type Props = {
  visible: boolean
  setVisible: (visible: boolean) => void
}

export const ChangeAuthenModal: FC<Props> = ({ visible, setVisible }) => {
  const { register, control, handleSubmit, formState, setValue, watch } = useForm<CredentialUpdateDto>({
    defaultValues: {
      identityType: IDENTITY_TYPES[0],
    },
  })
  const queryClient = useQueryClient()
  const { credentialName } = useParams<{ credentialName: string }>()
  const { validateFunction } = useValidateString()
  const watchIdentityType = watch('identityType')
  const watchAuthProtocol = watch('authProtocol')

  const { mutate: updateCredential, isLoading: isUpdating } = useMutation(APIServices.Integrations.updateCredential, {
    onSuccess: async () => {
      await queryClient.invalidateQueries([IntegrationsQueryKeys.getCredentials])
      await queryClient.invalidateQueries([IntegrationsQueryKeys.getCredential, { name: credentialName }])
      setVisible(false)
    },
    onError: error => {
      NToast.error({ title: 'Update credential', subtitle: error.response?.data.message })
    },
  })
  const onSubmit = (data: CredentialUpdateDto) => {
    updateCredential({
      name: credentialName,
      ...data,
    })
  }

  return (
    <NModal size="large" visible={visible} setVisible={setVisible}>
      <NModal.Header title="Change authentication" onClose={() => setVisible(false)} />
      <NModal.Body>
        <CredentialForm>
          <NRow>
            <NColumn>
              <Controller
                control={control}
                name="identityType"
                render={({ field }) => (
                  <NSingleSelect
                    fullWidth
                    options={IDENTITY_TYPES.map(type => ({ value: type }))}
                    label="Identity type"
                    value={field.value}
                    onValueChange={value => {
                      if (value === 'anon') {
                        setValue('credential', undefined)
                        setValue('authProtocol', undefined)
                      }
                      field.onChange(value)
                    }}
                  />
                )}
              />
            </NColumn>
            <NDivider vertical size="xl" />
            <NColumn>
              {watchIdentityType !== 'anon' && (
                <Controller
                  control={control}
                  name="authProtocol"
                  render={({ field }) => (
                    <NSingleSelect
                      fullWidth
                      options={AUTH_PROTOCOLS.map(item => ({ value: item as string }))}
                      label="Auth protocol"
                      value={field.value}
                      onValueChange={field.onChange}
                    />
                  )}
                />
              )}
            </NColumn>
          </NRow>
          {watchIdentityType !== 'anon' && watchAuthProtocol === 'password' && (
            <>
              <NDivider size="xl" />
              <NRow>
                <NColumn>
                  <NTextInput
                    required
                    {...register('credential.username', {
                      validate: validateFunction,
                    })}
                    label="Username"
                    error={formState.errors.credential?.username?.message}
                  />
                </NColumn>
                <NDivider vertical size="xl" />
                <NColumn>
                  <NTextInput label="Password" type="password" {...register('credential.password')} />
                </NColumn>
              </NRow>
            </>
          )}
        </CredentialForm>
      </NModal.Body>
      <NModal.Footer isLoading={isUpdating} onFinish={handleSubmit(onSubmit)} onCancel={() => setVisible(false)} />
    </NModal>
  )
}
