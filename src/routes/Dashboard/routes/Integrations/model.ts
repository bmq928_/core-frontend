import { SelectOption } from '../../../../components/NSelect/model'
import {
  CredentialCreateDto,
  HeaderInputType,
  RequestInputType,
  ResponseOutputType,
} from '../../../../services/api/models'
export const AUTH_PROTOCOLS: CredentialCreateDto['authProtocol'][] = ['password', 'oauth', 'jwt']
export const IDENTITY_TYPES: CredentialCreateDto['identityType'][] = ['anon', 'orgWide', 'perUser']
export const DATA_TYPE_OPTIONS: SelectOption[] = [
  { label: 'String', value: 'string' },
  { label: 'Number', value: 'number' },
  { label: 'Object', value: 'object' },
  { label: 'Array', value: 'array' },
  { label: 'Boolean', value: 'boolean' },
]
export const REQUEST_INPUT_TYPE_OPTIONS: SelectOption[] = [
  { label: 'Value', value: 'value' },
  { label: 'Variable', value: 'variable' },
  { label: 'Template', value: 'template' },
]

export type ActionMetaDataFormType = {
  name: string
  displayName: string
  description?: string
  credentialName: string
  path: string
  method: 'post' | 'get' | 'put' | 'delete'
  inputs: {
    header: (HeaderInputType & { name: string })[]
    query: (RequestInputType & { name: string })[]
    body?: RequestInputType
  }
  outputs: ({ httpCode: string } & ResponseOutputType)[]
}
