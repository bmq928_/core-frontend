import React, { FC } from 'react'
import { useForm } from 'react-hook-form'
import { useMutation, useQueryClient } from 'react-query'
import styled from 'styled-components'
import { spacing } from '../../../../../components/GlobalStyle'
import { NDivider } from '../../../../../components/NDivider'
import { NColumn, NRow } from '../../../../../components/NGrid/NGrid'
import { NModal } from '../../../../../components/NModal/NModal'
import { NTextArea } from '../../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../../components/NToast'
import { useValidateString } from '../../../../../hooks/useValidateString'
import { APIServices } from '../../../../../services/api'
import { MediaQueryKeys } from '../../../../../services/api/endpoints/media'
import { MediaResponse, UpdateMediaDto } from '../../../../../services/api/models'

const FormWrapper = styled.form`
  padding: ${spacing('lg')} ${spacing('xl')};
`

type Props = {
  visible: boolean
  setVisible: (visible: boolean) => void
  mediaData: MediaResponse
}

export const MediaEditModal: FC<Props> = ({ visible, setVisible, mediaData }) => {
  const { handleSubmit, register, formState } = useForm()
  const queryClient = useQueryClient()
  const { validateFunction } = useValidateString()

  const { mutate: updateMedia, isLoading } = useMutation(APIServices.Media.updateMedia, {
    onSuccess: () => {
      queryClient.invalidateQueries(MediaQueryKeys.getAllMedia)
      queryClient.invalidateQueries(MediaQueryKeys.getMedia)
      setVisible(false)
    },
    onError: error => {
      NToast.error({
        title: 'Update media unsuccessful',
        subtitle: error.response?.data.message,
      })
    },
  })

  const onSubmit = (data: UpdateMediaDto) => {
    updateMedia({ id: mediaData.id, ...data })
  }

  return (
    <NModal visible={visible} setVisible={setVisible}>
      <NModal.Header title="Update media" onClose={() => setVisible(false)} />
      <NModal.Body>
        <FormWrapper>
          <NRow>
            <NColumn flex={1}>
              <NTextInput
                {...register('displayName', {
                  validate: validateFunction,
                })}
                defaultValue={mediaData.displayName || ''}
                required
                error={formState.errors.displayName?.message}
                placeholder="Display name"
                label="Display name"
                caption="Your application display name"
              />
            </NColumn>
          </NRow>
          <NDivider size="md" />
          <NRow>
            <NColumn flex={1}>
              <NTextArea
                rows={4}
                defaultValue={mediaData.description || ''}
                {...register('description')}
                placeholder="Enter description"
                label="Description"
              />
            </NColumn>
          </NRow>
        </FormWrapper>
      </NModal.Body>
      <NModal.Footer isLoading={isLoading} onCancel={() => setVisible(false)} onFinish={handleSubmit(onSubmit)} />
    </NModal>
  )
}
