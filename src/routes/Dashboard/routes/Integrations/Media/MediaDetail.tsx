import { FC, useState } from 'react'
import { useQuery } from 'react-query'
import { useParams } from 'react-router-dom'
import styled from 'styled-components'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { color, fontSize, spacing } from '../../../../../components/GlobalStyle'
import { NButton } from '../../../../../components/NButton/NButton'
import { NDetailHeader } from '../../../../../components/NDetailHeader'
import { NDivider } from '../../../../../components/NDivider'
import { NHyperlinkOrText } from '../../../../../components/NHyperlinkOrText'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { NToast } from '../../../../../components/NToast'
import { typography } from '../../../../../components/NTypography'
import { SettingBoard } from '../../../../../components/SettingBoard'
import { APIServices } from '../../../../../services/api'
import { MediaQueryKeys } from '../../../../../services/api/endpoints/media'
import { MediaEditModal } from './MediaEditModal'

const FieldWrapper = styled.div`
  padding: ${spacing(14)} ${spacing('md')};
`

const FieldLabel = styled.p`
  color: ${color('Neutral700')};
  font-size: ${fontSize('xs')};
  line-height: ${fontSize('xs')};
  font-weight: bold;
`

const FieldValue = styled.p`
  ${typography('small-bold-text')}
  color: ${color('Neutral400')};
`

const BoardWrapper = styled.div`
  display: grid;
  padding: ${spacing('xl')} ${spacing('xs')};
  grid-template-columns: repeat(2, 1fr);
  grid-column-gap: ${spacing('xl')};
`

type FieldItemProps = {
  title: string
  value?: string | number
}
const FieldItem: FC<FieldItemProps> = ({ title, value }) => {
  return (
    <FieldWrapper>
      <FieldLabel>{title}</FieldLabel>
      <NDivider size="xs" />
      <FieldValue>
        <NHyperlinkOrText>{value}</NHyperlinkOrText>
      </FieldValue>
    </FieldWrapper>
  )
}

export const MediaDetail: FC = () => {
  const { mediaId } = useParams<{ mediaId: string }>()
  const { data: mediaData } = useQuery([MediaQueryKeys.getMedia, { id: mediaId }], APIServices.Media.getMedia, {
    onError: error => {
      NToast.error({ title: 'Fail to get media detail', subtitle: error.response?.data.message })
    },
  })
  const [showEdit, setShowEdit] = useState(false)

  return (
    <NContainer>
      {showEdit && mediaData?.data && (
        <MediaEditModal visible={showEdit} setVisible={setShowEdit} mediaData={mediaData?.data} />
      )}
      <NDetailHeader
        breadcrumbs={[
          { label: 'Intergrations', path: `${DASHBOARD_ROUTE}/integrations` },
          { label: 'Media', path: `${DASHBOARD_ROUTE}/integrations/media` },
          { label: mediaData?.data.displayName || mediaData?.data.name || '' },
        ]}
      />
      <NContainer.Title>{mediaData?.data.displayName || mediaData?.data.name}</NContainer.Title>
      {mediaData?.data.description && <NContainer.Description>{mediaData?.data.description}</NContainer.Description>}
      <SettingBoard
        title="Media detail"
        action={
          <NButton size="small" onClick={() => setShowEdit(true)} type="outline">
            Edit
          </NButton>
        }>
        <BoardWrapper>
          <FieldItem title="Name" value={mediaData?.data.name} />
          <FieldItem title="Display name" value={mediaData?.data.displayName} />
          <FieldItem title="Size" value={mediaData?.data.size} />
          <FieldItem title="MIME type" value={mediaData?.data.mimeType} />
          <FieldItem title="URL" value={mediaData?.data.url} />
          <FieldItem title="Reference key" value={mediaData?.data.referenceKey} />
          <FieldItem title="Created at" value={mediaData?.data.createdAt} />
          <FieldItem title="Updated at" value={mediaData?.data.updatedAt} />
          <FieldItem title="Created by" value={mediaData?.data.createdBy} />
        </BoardWrapper>
      </SettingBoard>
      <SettingBoard title="Action metadata">
        <BoardWrapper>
          <FieldItem title="Name" value={mediaData?.data.actionMetadata.name} />
          <FieldItem title="Display name" value={mediaData?.data.actionMetadata.displayName} />
        </BoardWrapper>
      </SettingBoard>
    </NContainer>
  )
}
