import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { DASHBOARD_ROUTE } from '../../../../common/constants'
import { AppEditRoutes } from './AppEditRoutes'
import { AppListing } from './AppListing'

type AppRoutesProps = {}

export function AppRoutes({}: AppRoutesProps) {
  return (
    <Switch>
      <Route path={`${DASHBOARD_ROUTE}/app/:id/edit`}>
        <AppEditRoutes />
      </Route>
      <Route path={`${DASHBOARD_ROUTE}/app/`} exact>
        <AppListing />
      </Route>
    </Switch>
  )
}
