import { FC } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { useParams } from 'react-router-dom'
import styled, { useTheme } from 'styled-components'
import { DASHBOARD_ROUTE } from '../../../../common/constants'
import { color, spacing } from '../../../../components/GlobalStyle'
import { NAvatar } from '../../../../components/NAvatar/NAvatar'
import { NBreadcrumbs } from '../../../../components/NBreadcrumbs/NBreadcrumbs'
import { NButton } from '../../../../components/NButton/NButton'
import { NDivider } from '../../../../components/NDivider'
import { NContainer } from '../../../../components/NLayout/NContainer'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { NTextArea } from '../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../components/NToast'
import { NTypography, typography } from '../../../../components/NTypography'
import { SettingBoard } from '../../../../components/SettingBoard'
import { APIServices } from '../../../../services/api'
import { BuilderQueryKey } from '../../../../services/api/endpoints/builder'
import { UpdateApplicationDto } from '../../../../services/api/models'
import { ObjectHeadersComponent } from '../Object/components/ObjectHeaderComponent'

const NameTextInput = styled(NTextInput)`
  width: 100%;
`

const Section = styled.div`
  padding: ${spacing('xl')};
`

const IconTitle = styled.p`
  ${typography('h500')}
`

const Label = styled(NTypography.InputLabel)`
  margin-bottom: ${spacing('xs')};
`

const ImageWrapper = styled.div`
  display: flex;
`

const ImageUploadCaption = styled.p`
  ${typography('caption')}
  color: ${color('Neutral400')};
`

const SaveBtn = styled(NButton)`
  width: 80px;
`

const DescTextArea = styled(NTextArea)`
  width: 100%;
`

export const AppDetail: FC = () => {
  const { t } = useTranslation()
  const { register, formState, handleSubmit } = useForm()
  const theme = useTheme()
  const { id } = useParams<{ id: string }>()
  const queryClient = useQueryClient()

  const { data: appData } = useQuery([BuilderQueryKey.getApp, { id }], APIServices.Builder.getApp)

  const { mutate: updateApplication, isLoading } = useMutation(APIServices.Builder.updateApp, {
    onSuccess: async () => {
      await queryClient.invalidateQueries([BuilderQueryKey.getApps])
      await queryClient.invalidateQueries([BuilderQueryKey.getApp, { id }])
    },
    onError: error => {
      NToast.error({ title: 'Update Application', subtitle: error.response?.data.message })
    },
  })

  const onSubmit = (data: UpdateApplicationDto) => {
    updateApplication({
      id,
      ...data,
    })
  }

  return (
    <>
      <ObjectHeadersComponent.HeaderContainer>
        <NBreadcrumbs
          breadcrumbs={[
            { path: DASHBOARD_ROUTE, label: 'Dashboard' },
            {
              path: `${DASHBOARD_ROUTE}/app`,
              label: 'Applications',
            },
            {
              label: appData?.data.name || '',
            },
          ]}
        />

        <ObjectHeadersComponent.ButtonContainer>
          Last updated <NDivider vertical size="xl" />
          <SaveBtn loading={isLoading} type="outline-2" onClick={handleSubmit(onSubmit)}>
            Save
          </SaveBtn>
        </ObjectHeadersComponent.ButtonContainer>
      </ObjectHeadersComponent.HeaderContainer>
      <NContainer.Title>{appData?.data.name || ''}</NContainer.Title>
      <SettingBoard title="App Detail">
        <form>
          <Section>
            <NameTextInput
              defaultValue={appData?.data.name || ''}
              placeholder="Display name"
              label="Display name"
              error={formState.errors.name?.message}
              {...register('name', {
                required: {
                  value: true,
                  message: t('common.error.required'),
                },
              })}
              required
            />
            <NDivider size="xl" />
            <DescTextArea
              defaultValue={appData?.data.description || ''}
              label="Description"
              rows={4}
              placeholder="Enter description"
              {...register('description')}
            />
          </Section>
          <NDivider lineColor={color('Neutral200')({ theme })} />
          <Section>
            {/* TODO: handle upload */}
            <IconTitle>Icon</IconTitle>
            <NDivider size="xl" />
            <Label>Image</Label>
            <NDivider size="xs" />
            <ImageWrapper>
              <NAvatar size={156} name={appData?.data.name || ''} variant="rounded" />
              <NDivider vertical size="xl" />
              <div>
                <NButton type="primary" size="small">
                  Upload
                </NButton>
                <NDivider size="xxs" />
                <ImageUploadCaption>
                  Upload 256 x 256 pixel PNG. This icon show up present for you app.
                </ImageUploadCaption>
                <NDivider size="xl" />
                <NSingleSelect label="Primary color" options={[{ value: '#666BB5' }]} placeholder="Select a color" />
              </div>
            </ImageWrapper>
          </Section>
        </form>
      </SettingBoard>
    </>
  )
}
