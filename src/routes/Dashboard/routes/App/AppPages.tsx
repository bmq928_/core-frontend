import { formatDistanceToNow } from 'date-fns'
import { FC, useCallback, useEffect, useState } from 'react'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { useHistory, useParams } from 'react-router-dom'
import styled, { useTheme } from 'styled-components'
import { DASHBOARD_ROUTE } from '../../../../common/constants'
import { color, spacing } from '../../../../components/GlobalStyle'
import { NBreadcrumbs } from '../../../../components/NBreadcrumbs/NBreadcrumbs'
import { NButton } from '../../../../components/NButton/NButton'
import { NDivider } from '../../../../components/NDivider'
import { NContainer } from '../../../../components/NLayout/NContainer'
import { NSortableList } from '../../../../components/NSortableList'
import { NToast } from '../../../../components/NToast'
import { typography } from '../../../../components/NTypography'
import { SettingBoard } from '../../../../components/SettingBoard'
import { APIServices } from '../../../../services/api'
import { AppQueryKey } from '../../../../services/api/endpoints/app'
import { BuilderQueryKey } from '../../../../services/api/endpoints/builder'
import { AppBuilderLayoutResponse } from '../../../../services/api/models'
import { ObjectHeadersComponent } from '../Object/components/ObjectHeaderComponent'
import { PageItem } from './components/PageItem'

const AppSectionHeader = styled.p`
  ${typography('overline')}
  padding: ${spacing('sm')} ${spacing('xl')};
  color: ${color('Neutral400')};
`

const SaveBtn = styled(NButton)`
  width: 80px;
`

export const AppPages: FC = () => {
  const [appPages, setAppPages] = useState<AppBuilderLayoutResponse[]>([])
  const theme = useTheme()
  const queryClient = useQueryClient()
  const { id } = useParams<{ id: string }>()
  const history = useHistory()

  const { data: appData } = useQuery([BuilderQueryKey.getApp, { id }], APIServices.Builder.getApp)

  const { data: pagesData } = useQuery([BuilderQueryKey.getAppPages, { id }], APIServices.Builder.getAppPages)

  const { mutate: updateAppPagesOrder, isLoading: isUpdating } = useMutation(APIServices.Builder.orderPages, {
    onSuccess: async () => {
      await queryClient.invalidateQueries([BuilderQueryKey.getApp, { id }])
      await queryClient.invalidateQueries([AppQueryKey.getAppPages, { id }])
      await queryClient.invalidateQueries([BuilderQueryKey.getAppPages, { id }])
    },
    onError: error => {
      NToast.error({ title: 'Failed to update pages order', subtitle: error.response?.data.message })
    },
  })

  // set initial set for app
  useEffect(() => {
    if (pagesData) {
      setAppPages(pagesData.data.app_page)
    }
  }, [pagesData])

  const movePage = useCallback(
    (dragIndex: number, hoverIndex: number) => {
      const dragPage = appPages[dragIndex]
      const newPages = [...appPages]
      newPages[dragIndex] = newPages[hoverIndex]
      newPages[hoverIndex] = dragPage
      setAppPages(newPages)
    },
    [appPages],
  )

  const onSave = () => {
    updateAppPagesOrder({ id, layoutIds: appPages.map(page => page.id) })
  }

  return (
    <>
      <ObjectHeadersComponent.HeaderContainer>
        <NBreadcrumbs
          breadcrumbs={[
            { path: DASHBOARD_ROUTE, label: 'Dashboard' },
            {
              path: `${DASHBOARD_ROUTE}/app`,
              label: 'Applications',
            },
            {
              label: appData?.data.name || '',
            },
          ]}
        />
        <ObjectHeadersComponent.ButtonContainer>
          {appData?.data.updatedAt &&
            `Last updated ${formatDistanceToNow(new Date(appData?.data.updatedAt), { addSuffix: true })}`}
          <NDivider vertical size="xl" />
          <NButton type="primary" onClick={() => history.push(`${DASHBOARD_ROUTE}/layout`)}>
            Create
          </NButton>
          <NDivider vertical size="xl" />
          <SaveBtn type="outline-2" loading={isUpdating} onClick={onSave}>
            Save
          </SaveBtn>
        </ObjectHeadersComponent.ButtonContainer>
      </ObjectHeadersComponent.HeaderContainer>
      <NContainer.Title>{appData?.data.name}</NContainer.Title>
      <NContainer.Content>
        <SettingBoard title="Pages">
          <AppSectionHeader>HOME PAGE</AppSectionHeader>
          <PageItem page={{ name: 'home', displayName: 'My home' }} canEdit={false} canDrag={false} />
          <NDivider lineColor={color('Neutral200')({ theme })} />
          <AppSectionHeader>APP PAGES</AppSectionHeader>
          <NSortableList onSort={movePage}>
            {appPages.map(app => (
              <PageItem page={{ name: app.id, displayName: app.name }} key={app.id} />
            ))}
          </NSortableList>
          <NDivider lineColor={color('Neutral200')({ theme })} />
          <AppSectionHeader>RECORD PAGES</AppSectionHeader>
          {pagesData?.data.record_page.map(record => (
            <PageItem page={{ name: record.id, displayName: record.name }} key={record.id} canDrag={false} />
          ))}
        </SettingBoard>
      </NContainer.Content>
    </>
  )
}
