import * as React from 'react'
import { useHistory } from 'react-router-dom'
import styled from 'styled-components'
import { color, spacing } from '../../../../../components/GlobalStyle'
import { Icons } from '../../../../../components/Icons'
import { NMenuPopover, NMenuPopoverItem } from '../../../../../components/NMenuPopover'

const Wrapper = styled('div').attrs((props: { canDrag?: boolean; theme: any }) => ({
  style: {
    cursor: props.canDrag ? 'move' : 'pointer',
    paddingLeft: props.canDrag ? spacing('xs')(props) : spacing('xl')(props),
  },
}))<{ canDrag?: boolean }>`
  padding: ${spacing('sm')} ${spacing('xl')};
  border: 1px solid transparent;
  display: flex;
  align-items: center;
  height: 44px;
  &.is-dragging {
    opacity: 0.2;
  }
`

const MoreBtn = styled(Icons.More)`
  cursor: pointer;
  &:hover {
    background: ${color('Neutral200')};
  }
  width: 20px;
  height: 20px;
`

type PageItemProps = {
  children?: React.ReactNode
  page: { displayName: string; name: string }
  canEdit?: boolean
  canDrag?: boolean
  className?: string
}

export const PageItem = React.forwardRef<HTMLDivElement, PageItemProps>(function PageItem(
  { page, canEdit = true, canDrag = true, className },
  ref,
) {
  const history = useHistory()
  return (
    <Wrapper ref={ref} className={className} canDrag={canDrag}>
      {canDrag && <Icons.Reorder />}
      <div style={{ flex: 1 }}>{page.displayName}</div>
      {canEdit && (
        <NMenuPopover trigger={<MoreBtn />} placement="bottomRight">
          <NMenuPopoverItem onClick={() => history.push(`/layout-builder/${page.name}`)}>Edit</NMenuPopoverItem>
        </NMenuPopover>
      )}
    </Wrapper>
  )
})
