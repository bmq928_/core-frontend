import { FC } from 'react'
import { Redirect, Route, Switch, useParams } from 'react-router-dom'
import { DASHBOARD_ROUTE } from '../../../../common/constants'
import { NLayout, NSideBar } from '../../../../components/NLayout'
import { NContainer } from '../../../../components/NLayout/NContainer'
import { NMenu } from '../../../../components/NMenu/NMenu'
import { AppDetail } from './AppDetail'
import { AppPages } from './AppPages'
import { UserProfile } from './UserProfile'

export const AppEditRoutes: FC = () => {
  const { id } = useParams<{ id: string }>()

  return (
    <NLayout isRow>
      <NSideBar title="Application">
        <NMenu type="primary">
          <NMenu.Item to={`${DASHBOARD_ROUTE}/app/${id}/edit/details`}>Details</NMenu.Item>
          <NMenu.Item to={`${DASHBOARD_ROUTE}/app/${id}/edit/profiles`}>User Profile</NMenu.Item>
          <NMenu.Item to={`${DASHBOARD_ROUTE}/app/${id}/edit/pages`}>Pages</NMenu.Item>
        </NMenu>
      </NSideBar>
      <NContainer>
        <Switch>
          <Route path={`${DASHBOARD_ROUTE}/app/:id/edit/profiles`}>
            <UserProfile />
          </Route>
          <Route path={`${DASHBOARD_ROUTE}/app/:id/edit/details`}>
            <AppDetail />
          </Route>
          <Route path={`${DASHBOARD_ROUTE}/app/:id/edit/pages`}>
            <AppPages />
          </Route>
          <Redirect to={`${DASHBOARD_ROUTE}/app/:id/edit/details`} />
        </Switch>
      </NContainer>
    </NLayout>
  )
}
