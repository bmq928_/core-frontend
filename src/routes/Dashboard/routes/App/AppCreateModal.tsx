import React, { FC } from 'react'
import { useForm } from 'react-hook-form'
import { useMutation, useQueryClient } from 'react-query'
import { useHistory } from 'react-router-dom'
import styled from 'styled-components'
import { DASHBOARD_ROUTE } from '../../../../common/constants'
import { spacing } from '../../../../components/GlobalStyle'
import { NDivider } from '../../../../components/NDivider'
import { NColumn, NRow } from '../../../../components/NGrid/NGrid'
import { NModal } from '../../../../components/NModal/NModal'
import { NTextArea } from '../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../components/NToast'
import { useValidateString } from '../../../../hooks/useValidateString'
import { APIServices } from '../../../../services/api'
import { BuilderQueryKey } from '../../../../services/api/endpoints/builder'
import { CreateApplicationDto } from '../../../../services/api/models'

const AppForm = styled.form`
  padding: ${spacing('lg')} ${spacing('xl')};
`

type AppCreateModalProps = {
  visible: boolean
  setVisible: (visible: boolean) => void
}

export const AppCreateModal: FC<AppCreateModalProps> = ({ visible, setVisible }) => {
  const history = useHistory()
  const { handleSubmit, register, formState } = useForm()
  const queryClient = useQueryClient()
  const { validateFunction } = useValidateString()

  const { mutate: createApplication, isLoading } = useMutation(APIServices.Builder.createApp, {
    onSuccess: async result => {
      await queryClient.invalidateQueries(BuilderQueryKey.getApps)
      history.push(`${DASHBOARD_ROUTE}/app/${result.data.id}/edit`)
    },
    onError: error => {
      NToast.error({
        title: 'Create App unsuccessful',
        subtitle: error.response?.data.message,
      })
    },
  })

  const onSubmit = (data: CreateApplicationDto) => {
    createApplication(data)
  }

  return (
    <NModal size="large" visible={visible} setVisible={setVisible}>
      <NModal.Header title="Create application" onClose={() => setVisible(false)} />
      <NModal.Body>
        <AppForm>
          <NRow>
            <NColumn flex={1}>
              <NTextInput
                {...register('name', {
                  validate: validateFunction,
                })}
                required
                error={formState.errors.name?.message}
                placeholder="Display name"
                label="Display name"
                caption="Your application display name"
              />
            </NColumn>
          </NRow>
          <NDivider size="md" />
          <NRow>
            <NColumn flex={1}>
              <NTextArea rows={4} {...register('description')} placeholder="Enter description" label="Description" />
            </NColumn>
          </NRow>
        </AppForm>
      </NModal.Body>
      <NModal.Footer isLoading={isLoading} onCancel={() => setVisible(false)} onFinish={handleSubmit(onSubmit)} />
    </NModal>
  )
}
