import { FC, useMemo, useState } from 'react'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { useHistory } from 'react-router'
import { classnames } from '../../../../common/classnames'
import { GlobalIcons } from '../../../../components/Icons'
import { NButton } from '../../../../components/NButton/NButton'
import { NRow } from '../../../../components/NGrid/NGrid'
import { NContainer } from '../../../../components/NLayout/NContainer'
import { NPagination } from '../../../../components/NPagination'
import { NTable, NTableColumnType } from '../../../../components/NTable/NTable'
import { NTableCollapsedCellClassName } from '../../../../components/NTable/NTableStyledContainer'
import { NTableToolbar } from '../../../../components/NTable/NTableToolbar'
import { getRequestError, NToast } from '../../../../components/NToast'
import { useTableConfigs } from '../../../../hooks/useTableConfigs'
import { APIServices } from '../../../../services/api'
import { FlowResponse, FlowsQueryKey, FlowType } from '../../../../services/api/endpoints/flows'
import { getTableData } from '../../../../utils/table-utils'
import { FlowDetailForm, FlowDetailModal } from './components/FlowDetailModal'
import { FlowTableActions } from './components/FlowTableActions'
import { FlowTypesModal } from './components/FlowTypesModal'

const PAGE_SIZE = 10

export const FlowListing: FC = () => {
  const [
    { sortBy, sortOrder, searchText, searchValue, page: currentPage },
    { onChangeSort, onChangePage, onChangeSearch },
  ] = useTableConfigs()
  const [selectedRows, setSelectedRows] = useState<FlowResponse[]>([])
  const [showFlowTypesModal, setShowFlowTypesModal] = useState(false)
  const [selectedFlowType, setSelectedFlowType] = useState<FlowType>('screen')
  const [showFlowDetailModal, setShowFlowDetailModal] = useState(false)
  const queryClient = useQueryClient()
  const [editingFlow, setEditingFlow] = useState<FlowResponse | undefined>()
  const history = useHistory()

  const { data: flowData, isFetching } = useQuery(
    [
      FlowsQueryKey.getFlowsList,
      { limit: PAGE_SIZE, offset: (currentPage - 1) * PAGE_SIZE, searchText, sortBy, sortOrder },
    ],
    APIServices.Flows.getFlowsList,
    {
      keepPreviousData: true,
      onError: error => {
        NToast.error(getRequestError('Get Flow', error))
      },
    },
  )

  const { mutate: createFlow, isLoading: isCreating } = useMutation(APIServices.Flows.createFlow, {
    onSuccess: async data => {
      await queryClient.invalidateQueries([
        FlowsQueryKey.getFlowsList,
        { limit: PAGE_SIZE, offset: (currentPage - 1) * PAGE_SIZE },
      ])
      setShowFlowDetailModal(false)
      if (data) {
        history.push(`/flow-builder/${data?.data.name}`)
      }
    },
    onError: error => {
      NToast.error({ title: 'Create unsuccesful', subtitle: error.response?.data.message })
    },
  })

  const { mutate: updateFlow, isLoading: isUpdating } = useMutation(APIServices.Flows.updateFlow, {
    onSuccess: async () => {
      await queryClient.invalidateQueries([
        FlowsQueryKey.getFlowsList,
        { limit: PAGE_SIZE, offset: (currentPage - 1) * PAGE_SIZE },
      ])
      setEditingFlow(undefined)
      setShowFlowDetailModal(false)
    },
    onError: error => {
      NToast.error({ title: 'Update unsuccesful', subtitle: error.response?.data.message })
    },
  })

  const { pageData, pageInfo, totalPage } = useMemo(() => {
    return getTableData(flowData)
  }, [flowData])

  const rowOnSelect = (selected: FlowResponse[]) => {
    setSelectedRows(selected)
  }

  const onSubmitForm = (data: FlowDetailForm) => {
    if (editingFlow) {
      updateFlow({
        flowName: editingFlow.name,
        data: {
          display_name: data.display_name,
          description: data.description,
        },
      })
    } else {
      createFlow({ ...data, flow_type: selectedFlowType })
    }
  }

  const columns = useMemo<NTableColumnType<FlowResponse>>(
    () => [
      {
        Header: 'Label',
        accessor: 'display_name',
        defaultCanSort: true,
      },
      { Header: 'Description', accessor: 'description' },
      { Header: 'Type', accessor: 'flow_type' },
      {
        Header: 'Active',
        accessor: 'is_active',
        defaultCanSort: true,
        Cell: ({ value }) => {
          return value ? <GlobalIcons.Check /> : null
        },
      },
      {
        Header: 'Created at',
        accessor: 'createdAt',
        Cell: ({ value }) => {
          if (!value) {
            return null
          }
          const date = new Date(value)
          return date.toLocaleString('vi')
        },
        defaultCanSort: true,
      },
      {
        Header: 'Last modified',
        accessor: 'updatedAt',
        Cell: ({ value }) => {
          if (!value) {
            return null
          }
          const date = new Date(value)
          return date.toLocaleString('vi')
        },
        defaultCanSort: true,
      },
      {
        accessor: 'name',
        Cell: ({ value, row }) => {
          return (
            <FlowTableActions
              onEdit={() => {
                setShowFlowDetailModal(true)
                setEditingFlow(row.original)
              }}
              value={value}
              data={row.original}
            />
          )
        },
        id: classnames([NTableCollapsedCellClassName]),
      },
    ],
    [],
  )

  return (
    <NContainer>
      <FlowTypesModal
        selectedType={selectedFlowType}
        setSelectedType={setSelectedFlowType}
        visible={showFlowTypesModal}
        setVisible={setShowFlowTypesModal}
        onFinish={() => {
          setShowFlowTypesModal(false)
          setShowFlowDetailModal(true)
        }}
      />
      <FlowDetailModal
        data={editingFlow}
        visible={showFlowDetailModal}
        setVisible={setShowFlowDetailModal}
        onBack={() => {
          setShowFlowDetailModal(false)
          setShowFlowTypesModal(true)
        }}
        onSubmit={onSubmitForm}
        isLoading={isCreating || isUpdating}
      />
      <NRow justify="space-between">
        <NContainer.Title>Flow Listing</NContainer.Title>
        <NButton
          type="primary"
          onClick={() => {
            setShowFlowTypesModal(true)
            setEditingFlow(undefined)
          }}>
          Create Flow
        </NButton>
      </NRow>
      <NContainer.Description>{pageInfo.total} Flows</NContainer.Description>
      <NTableToolbar
        searchConfig={{
          value: searchValue,
          onChange: e => {
            onChangeSearch(e.target.value)
          },
        }}
        selectedConfig={{
          selected: selectedRows,
          onDeSelectAll: () => {},
          onDelete: selected => {
            console.log(`Delete ${selected.length} rows`)
          },
        }}
      />
      <NTable
        rowSelectionConfig={{ onChange: rowOnSelect }}
        columns={columns}
        isLoading={isFetching}
        data={pageData}
        defaultSortBy={sortBy}
        defaultSortOrder={sortOrder}
        onChangeSort={onChangeSort}
        pageSize={PAGE_SIZE}
        pagination={<NPagination total={totalPage} current={currentPage} onChangePage={onChangePage} />}
        onClickRow={data => {
          history.push(`/flow-builder/${data.name}`)
        }}
      />
    </NContainer>
  )
}
