import { FC, useState } from 'react'
import { NTableActions } from '../../../../../components/NTable/NTableActions'
import { FlowResponse } from '../../../../../services/api/endpoints/flows'

type FlowTableActionsProps = {
  value: string
  data: FlowResponse
  onEdit: () => void
}

export const FlowTableActions: FC<FlowTableActionsProps> = ({ onEdit }) => {
  const [showDropList, setShowDropList] = useState(false)

  const handleEdit = () => {
    onEdit()
    setShowDropList(false)
  }

  return (
    <NTableActions
      showDropList={showDropList}
      setShowDropList={setShowDropList}
      options={[{ title: 'Edit', onClick: handleEdit }]}
    />
  )
}
