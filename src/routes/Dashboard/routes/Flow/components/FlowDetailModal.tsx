import { FC } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'
import { spacing } from '../../../../../components/GlobalStyle'
import { NDivider } from '../../../../../components/NDivider'
import { NColumn, NRow } from '../../../../../components/NGrid/NGrid'
import { NModal } from '../../../../../components/NModal/NModal'
import { NTextArea } from '../../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../../components/NTextInput/NTextInput'
import { useValidateString } from '../../../../../hooks/useValidateString'
import { FlowResponse } from '../../../../../services/api/endpoints/flows'
import { FlowCreateDto } from '../../../../../services/api/models'
import { fieldNameRegex } from '../../../../../utils/regex'
import { getNameFromDisplayName } from '../../../../../utils/utils'

export type FlowDetailForm = FlowCreateDto

const DetailForm = styled.form`
  padding: ${spacing('lg')} ${spacing('xl')};
`

type FlowDetailModalProps = {
  visible: boolean
  setVisible: (visible: boolean) => void
  onSubmit: (data: FlowDetailForm) => void
  data?: FlowResponse
  onBack: () => void
  isLoading: boolean
}

export const FlowDetailModal: FC<FlowDetailModalProps> = ({
  visible,
  setVisible,
  data,
  onBack,
  onSubmit,
  isLoading,
}) => {
  const { register, setValue, formState, handleSubmit, getValues } = useForm()
  const { t } = useTranslation()
  const { validateFunction } = useValidateString()

  const submitForm = (data: FlowDetailForm) => {
    onSubmit(data)
  }

  return (
    <NModal size="large" visible={visible} setVisible={setVisible}>
      <NModal.Header
        onBack={data ? undefined : onBack}
        onClose={() => setVisible(false)}
        title={data ? 'Edit flow' : 'Create new flow'}
      />
      <NModal.Body>
        <DetailForm>
          <NRow>
            <NColumn flex={1}>
              <NTextInput
                required
                defaultValue={data?.display_name || ''}
                {...register('display_name', {
                  validate: validateFunction,
                })}
                name="display_name"
                error={formState.errors.display_name?.message}
                placeholder={t('dashboard.objectListing.createObjectModal.displayNameField')}
                label="Display name"
                onBlur={e => {
                  if (!!!data) {
                    const formatName = getNameFromDisplayName(e.target.value)
                    !getValues('name') && setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
                  }
                }}
              />
            </NColumn>
            <NDivider vertical size="xl" />
            <NColumn flex={1}>
              <NTextInput
                defaultValue={data?.name || ''}
                {...register('name', {
                  validate: !data ? validateFunction : undefined,
                  pattern: {
                    value: fieldNameRegex,
                    message: 'Invalid pattern',
                  },
                })}
                required
                disabled={!!data}
                error={formState.errors.name?.message}
                placeholder={t('dashboard.objectListing.createObjectModal.nameField')}
                label="Name"
              />
            </NColumn>
          </NRow>
          <NDivider size="md" />
          <NRow>
            <NColumn flex={1}>
              <NTextArea
                defaultValue={data?.description || ''}
                {...register('description')}
                placeholder="Flow description"
                label="Description"
              />
            </NColumn>
          </NRow>
        </DetailForm>
      </NModal.Body>
      <NModal.Footer isLoading={isLoading} onFinish={handleSubmit(submitForm)} onCancel={() => setVisible(false)} />
    </NModal>
  )
}
