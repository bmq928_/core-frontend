import { FC } from 'react'
import styled from 'styled-components'
import { color, spacing } from '../../../../../components/GlobalStyle'
import { NDivider } from '../../../../../components/NDivider'
import { NModal } from '../../../../../components/NModal/NModal'
import { typography } from '../../../../../components/NTypography'
import { FlowType } from '../../../../../services/api/endpoints/flows'

const FLOW_TYPES = [
  {
    type: 'screen' as FlowType,
    name: 'Screen Flow',
    description:
      'is triggered by user action like Setup New User Flow or Add External Credit Card Flow. Such flows require user input in between the steps. This is the only type of Flow with direct user interaction.',
  },
  {
    type: 'data' as FlowType,
    name: 'Data Manipulation Flow ',
    description: 'could be triggered by any CRUD operations.',
  },
  {
    type: 'action' as FlowType,
    name: 'Action Flow',
    description:
      'is triggered by the system whenever some event happens. These could be events from other Flows, like CRUD actions (created User record, removed Account etc.) or general maintenance procedures.',
  },
  {
    type: 'time-based' as FlowType,
    name: 'Time Based Flow',
    description: 'is, obviously, triggered at a certain time.',
  },
]

const Wrapper = styled.div`
  padding: ${spacing('xl')};
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  column-gap: ${spacing('xl')};
  row-gap: ${spacing('lg')};
`

const FlowTypeWrapper = styled.div`
  display: flex;
  padding: ${spacing('lg')};
  border: 1px solid ${color('Neutral200')};
  box-sizing: border-box;
  border-radius: 6px;
  cursor: pointer;
  &.selected {
    border: 1px solid ${color('Primary700')};
  }
  transition: border 0.2s ease-in-out;
`

const Decorator = styled.div`
  width: 40px;
  height: 40px;
  border-radius: 4px;
  background: ${color('Primary700')};
`

const TextWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
`

const TypeName = styled.p`
  ${typography('button')}
`

const TypeDescription = styled.p`
  ${typography('x-small-ui-text')}
  color: ${color('Neutral400')};
`

type FlowTypesModalProps = {
  visible: boolean
  setVisible: (visible: boolean) => void
  selectedType?: string
  setSelectedType: (type: FlowType) => void
  onFinish: () => void
}

export const FlowTypesModal: FC<FlowTypesModalProps> = ({
  visible,
  setVisible,
  selectedType,
  setSelectedType,
  onFinish,
}) => {
  return (
    <NModal size="large" visible={visible} setVisible={setVisible}>
      <NModal.Header onClose={() => setVisible(false)} title="Select Flow Types"></NModal.Header>
      <NModal.Body>
        <Wrapper>
          {FLOW_TYPES.map(flow => (
            <FlowTypeWrapper
              onClick={() => setSelectedType(flow.type)}
              className={flow.type === selectedType ? 'selected' : ''}
              key={flow.type}>
              <Decorator />
              <NDivider vertical size="lg" />
              <TextWrapper>
                <TypeName>{flow.name}</TypeName>
                <NDivider size="xs" />
                <TypeDescription>{flow.description}</TypeDescription>
              </TextWrapper>
            </FlowTypeWrapper>
          ))}
        </Wrapper>
      </NModal.Body>
      <NModal.Footer onFinish={onFinish} onCancel={() => setVisible(false)} />
    </NModal>
  )
}
