import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { useQuery } from 'react-query'
import { useHistory } from 'react-router-dom'
import { classnames } from '../../../../common/classnames'
import { DASHBOARD_ROUTE } from '../../../../common/constants'
import { GlobalIcons } from '../../../../components/Icons'
import { NButton } from '../../../../components/NButton/NButton'
import { NRow } from '../../../../components/NGrid/NGrid'
import { NContainer } from '../../../../components/NLayout/NContainer'
import { NPagination } from '../../../../components/NPagination'
import { NTable, NTableColumnType } from '../../../../components/NTable/NTable'
import { NTableCollapsedCellClassName } from '../../../../components/NTable/NTableStyledContainer'
import { NTableToolbar } from '../../../../components/NTable/NTableToolbar'
import { useTableConfigs } from '../../../../hooks/useTableConfigs'
import { APIServices } from '../../../../services/api'
import { MetadataQueryKey } from '../../../../services/api/endpoints/metadata'
import { ObjectResponse } from '../../../../services/api/models'
import { getTableData } from '../../../../utils/table-utils'
import { CreateObjectModal } from './components/CreateObjectModal'
import { ObjectTableActions } from './components/ObjectTableActions'

const PAGE_SIZE = 10

type ObjectProps = {
  children?: React.ReactNode
}

export function ObjectListing({}: ObjectProps) {
  const [
    { sortBy, sortOrder, searchText, searchValue, page: currentPage },
    { onChangeSort, onChangePage, onChangeSearch },
  ] = useTableConfigs()
  const history = useHistory()
  const [showCreateObject, setShowCreateObject] = useState(false)
  const [editObjectData, setEditObjectData] = useState<ObjectResponse>()

  // remove edit object data when close create modal
  useEffect(() => {
    if (!showCreateObject) {
      setEditObjectData(undefined)
    }
  }, [showCreateObject])

  // handle edit
  const handleEditObject = useCallback((data: ObjectResponse) => {
    setEditObjectData(data)
    setShowCreateObject(true)
  }, [])

  const columns = React.useMemo<NTableColumnType<ObjectResponse>>(
    () => [
      {
        Header: 'Display Name',
        accessor: 'displayName',
        defaultCanSort: true,
      },
      {
        Header: 'Name',
        accessor: 'name',
        defaultCanSort: true,
      },
      {
        Header: 'Description',
        accessor: 'description',
      },
      {
        Header: 'Record name',
        accessor: 'recordName',
        Cell: ({ value }) => {
          return value.label || null
        },
      },
      {
        Header: 'Created at',
        accessor: 'createdAt',
        Cell: ({ value }) => {
          if (!value) {
            return null
          }
          const date = new Date(value)
          return date.toLocaleString('vi')
        },
        defaultCanSort: true,
      },
      {
        Header: 'Last modified',
        accessor: 'updatedAt',
        Cell: ({ value }) => {
          if (!value) {
            return null
          }
          const date = new Date(value)
          return date.toLocaleString('vi')
        },
        defaultCanSort: true,
      },
      {
        Header: 'Is System',
        accessor: 'isSystemDefault',
        defaultCanSort: true,
        Cell: ({ value }) => {
          return value ? <GlobalIcons.Check /> : null
        },
      },
      {
        accessor: 'name',
        Cell: ({ value, row }) => {
          if (row.original.isSystemDefault) {
            return <></>
          }
          return <ObjectTableActions value={value} data={row.original} onEditClick={handleEditObject} />
        },
        id: classnames([NTableCollapsedCellClassName]),
      },
    ],
    [],
  )

  const [selectedRows, setSelectedRow] = useState<ObjectResponse[]>([])
  // get MO pagination + sort
  const { data, isFetching } = useQuery(
    [
      MetadataQueryKey.getObjects,
      { limit: PAGE_SIZE, offset: (currentPage - 1) * PAGE_SIZE, searchText, sortBy, sortOrder },
    ],
    APIServices.Metadata.getObjects,
    { keepPreviousData: true },
  )
  const { pageData, pageInfo, totalPage } = useMemo(() => {
    return getTableData(data)
  }, [data])

  // test on select
  const rowOnSelect = (selected: ObjectResponse[]) => {
    setSelectedRow(selected)
  }

  return (
    <>
      <NContainer>
        <NRow justify="space-between">
          <NContainer.Title>Object Listing</NContainer.Title>
          <NButton type="primary" onClick={() => setShowCreateObject(true)}>
            Create Object
          </NButton>
        </NRow>
        <NContainer.Description>{pageInfo.total} Objects</NContainer.Description>
        <NContainer.Content>
          <NTableToolbar
            searchConfig={{
              value: searchValue,
              onChange: e => {
                onChangeSearch(e.target.value)
              },
            }}
            selectedConfig={{
              selected: selectedRows,
              onDeSelectAll: () => {},
              onDelete: selected => {
                console.log(`Delete ${selected.length} rows`)
              },
            }}
          />
          <NTable
            isLoading={isFetching}
            columns={columns}
            data={pageData}
            rowSelectionConfig={{ onChange: rowOnSelect }}
            defaultSortBy={sortBy}
            defaultSortOrder={sortOrder}
            onChangeSort={onChangeSort}
            pageSize={PAGE_SIZE}
            pagination={<NPagination total={totalPage} current={currentPage} onChangePage={onChangePage} />}
            onClickRow={data => {
              history.push(`${DASHBOARD_ROUTE}/object/${data.name}/fields`)
            }}
          />
        </NContainer.Content>
      </NContainer>

      {showCreateObject && (
        <CreateObjectModal visible={showCreateObject} setVisible={setShowCreateObject} objectData={editObjectData} />
      )}
    </>
  )
}
