import { FC } from 'react'
import { useQuery } from 'react-query'
import { Redirect, Route, Switch, useHistory, useParams } from 'react-router-dom'
import { DASHBOARD_ROUTE } from '../../../../common/constants'
import { NLayout, NSideBar } from '../../../../components/NLayout'
import { NMenu } from '../../../../components/NMenu/NMenu'
import { APIServices } from '../../../../services/api'
import { MetadataQueryKey } from '../../../../services/api/endpoints/metadata'
import { SidebarHeader } from '../../components/SidebarHeader'
import { ObjectFields } from './ObjectFields'
import { ObjectRuleDetail } from './ObjectRuleDetail'
import { ObjectRules } from './ObjectRules'

export const ObjectDetail: FC = () => {
  const { objName } = useParams<{ objName: string }>()
  const history = useHistory()

  const { data: objectData } = useQuery([MetadataQueryKey.getObject, { name: objName }], APIServices.Metadata.getObject)

  return (
    <NLayout isRow>
      <NSideBar>
        <SidebarHeader
          onBack={() => history.push(`${DASHBOARD_ROUTE}/object`)}
          backTitle="Back to Object Listing"
          title={objectData?.data.displayName || objName}
        />
        <NMenu type="primary">
          <NMenu.Item to={`${DASHBOARD_ROUTE}/object/${objName}/fields`}>Fields</NMenu.Item>
          <NMenu.Item to={`${DASHBOARD_ROUTE}/object/${objName}/rules`}>Rules</NMenu.Item>
        </NMenu>
      </NSideBar>
      <Switch>
        <Route path={`${DASHBOARD_ROUTE}/object/:objName/fields`} component={ObjectFields} />
        <Route path={`${DASHBOARD_ROUTE}/object/:objName/rules`} component={ObjectRules} exact />
        <Route path={`${DASHBOARD_ROUTE}/object/:objName/rules/:ruleName`} component={ObjectRuleDetail} />
        <Redirect to={`${DASHBOARD_ROUTE}/object/${objName}/fields`} />
      </Switch>
    </NLayout>
  )
}
