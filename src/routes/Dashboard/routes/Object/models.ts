import { AndLogicRule, FieldAttributesDto, ValidationRuleResponse } from '../../../../services/api/models'

export interface IFieldForm {
  name: string
  displayName: string
  defaultValue: string
  isRequired: boolean
  isExternalId: boolean
  value: string
  attributes: FieldAttributesDto
}

export type RuleState = {
  isEditing?: boolean
  ruleData?: Partial<ValidationRuleResponse>
}

export type RuleScript = { script: AndLogicRule[]; errorMessage?: string }
