import { FC, useEffect, useMemo, useState } from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { useHistory, useLocation, useParams } from 'react-router-dom'
import styled, { useTheme } from 'styled-components'
import { DASHBOARD_ROUTE } from '../../../../common/constants'
import { color, spacing } from '../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../components/Icons'
import { NBreadcrumbs } from '../../../../components/NBreadcrumbs/NBreadcrumbs'
import { NButton } from '../../../../components/NButton/NButton'
import { Filters, NConditionBuilder } from '../../../../components/NConditionBuilder'
import { NDivider } from '../../../../components/NDivider'
import { NContainer } from '../../../../components/NLayout/NContainer'
import { SelectOption } from '../../../../components/NSelect/model'
import { NTextArea } from '../../../../components/NTextArea/NTextArea'
import { NToast } from '../../../../components/NToast'
import { typography } from '../../../../components/NTypography'
import { APIServices } from '../../../../services/api'
import { BuilderQueryKey } from '../../../../services/api/endpoints/builder'
import { MetadataQueryKey } from '../../../../services/api/endpoints/metadata'
import { FieldSchema } from '../../../../services/api/models'
import { PostAction } from '../../../../services/api/request'
import { CreateRuleModal, CreateRuleParams } from './components/CreateRuleModal'
import { ObjectHeadersComponent } from './components/ObjectHeaderComponent'
import { RuleScript, RuleState } from './models'

const UpdatedAt = styled.div`
  ${typography('h400')}
  color: ${color('Neutral400')};
  font-weight: normal;
  margin-right: ${spacing('md')};
`

const TitleHeader = styled.div`
  display: flex;
`

const EditWrapper = styled.div`
  width: 40px;
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
`

export const ObjectRuleDetail: FC = () => {
  const { objName, ruleName } = useParams<{ objName: string; ruleName: string }>()
  const location = useLocation<RuleState>()
  const history = useHistory()
  const { t } = useTranslation()
  const queryClient = useQueryClient()
  const [showEditModal, setShowEditModal] = useState(false)
  const [editModalFormValues, setEditModalFormValues] = useState<CreateRuleParams | undefined>()
  const theme = useTheme()

  const formMethods = useForm({
    defaultValues: {
      script: [{ rules: [{ fieldName: '', value: '' }] }] as Filters[],
      errorMessage: '',
    },
  })

  const { data: ruleDataApi } = useQuery(
    [MetadataQueryKey.getObjectRule, { objName, ruleName }],
    APIServices.Metadata.getObjectRule,
    {
      enabled: location.state.isEditing,
      onSuccess(response) {
        formMethods.setValue('script', response.data.script as Filters[])
        formMethods.setValue('errorMessage', response.data.errorMessage)
      },
    },
  )

  const { data: fieldsData } = useQuery(
    [BuilderQueryKey.getObjectSchema, { objName }],
    APIServices.Builder.getObjectSchema,
    {
      select(response) {
        let fieldsByName: Record<string, FieldSchema> = {}
        let fieldNameOptions: SelectOption[] = []

        for (let field of response.data.fields) {
          fieldsByName[field.name] = field
          fieldNameOptions.push({ label: field.displayName, value: field.name })
        }
        return {
          fields: response.data.fields,
          fieldsByName,
          fieldNameOptions,
        }
      },
    },
  )

  const { mutate: postObjectRule, isLoading: isPostingRule } = useMutation(APIServices.Metadata.postObjectRule)

  useEffect(() => {
    // update errorMsg
    if (ruleDataApi?.data && ruleDataApi?.data.errorMessage) {
      formMethods.setValue('errorMessage', ruleDataApi?.data.errorMessage)
    }
  }, [ruleDataApi?.data])

  const ruleData = useMemo(() => {
    // data will come from 2 sources: newly create one (not yet submit) from route or existing one when hitting api
    const currentRuleData = location.state.isEditing ? ruleDataApi?.data : location.state.ruleData
    if (editModalFormValues) {
      if (location.state.isEditing) {
        // if editing won't override name
        return {
          ...currentRuleData,
          displayName: editModalFormValues.displayName,
          description: editModalFormValues.description,
        }
      }
      return { ...currentRuleData, ...editModalFormValues }
    }
    return currentRuleData
  }, [ruleDataApi?.data, location.state.ruleData, location.state.isEditing, editModalFormValues])

  const onSave = (data: RuleScript) => {
    postObjectRule(
      {
        data: {
          displayName: ruleData?.displayName || '',
          name: !location.state.isEditing ? ruleData?.name : undefined,
          description: ruleData?.description,
          script: data.script,
          errorMessage: data.errorMessage || '',
        },
        action: location.state.isEditing ? PostAction.Update : PostAction.Create,
        objName,
        ruleName: location.state.isEditing ? ruleData?.name : undefined,
      },
      {
        onSuccess: () => {
          queryClient.invalidateQueries(MetadataQueryKey.getObjectRules)
          queryClient.invalidateQueries(MetadataQueryKey.getObjectRule)
          if (!location.state.isEditing) {
            // goback when create new to prevent multiple submit from same form
            history.goBack()
          }
        },
        onError: error => {
          NToast.error({
            title: 'Save unsuccessful',
            subtitle: error.response?.data.message,
          })
        },
      },
    )
  }

  const onSubmitModal = (data: CreateRuleParams) => {
    setEditModalFormValues(data)
    setShowEditModal(false)
  }

  return (
    <NContainer>
      <ObjectHeadersComponent.HeaderContainer>
        <NBreadcrumbs
          breadcrumbs={[
            { path: DASHBOARD_ROUTE, label: 'Dashboard' },
            {
              path: `${DASHBOARD_ROUTE}/object`,
              label: 'Object',
            },
            {
              path: `${DASHBOARD_ROUTE}/object/${objName}/rules`,
              label: objName,
            },
            {
              label: ruleData?.name || ruleName,
            },
          ]}
        />

        <ObjectHeadersComponent.ButtonContainer>
          {ruleData?.updatedAt && <UpdatedAt>Last updated {ruleDataApi?.data.updatedAt}</UpdatedAt>}
          <NButton type="primary" onClick={formMethods.handleSubmit(onSave)} loading={isPostingRule}>
            Save
          </NButton>
        </ObjectHeadersComponent.ButtonContainer>
      </ObjectHeadersComponent.HeaderContainer>
      <NContainer.Title>
        <TitleHeader>
          {ruleData?.displayName}
          <EditWrapper onClick={() => setShowEditModal(true)}>
            <GlobalIcons.Edit />
          </EditWrapper>
        </TitleHeader>
      </NContainer.Title>
      <NContainer.Description>{ruleData?.description}</NContainer.Description>
      <NContainer.Content>
        <FormProvider {...formMethods}>
          <form>
            <NConditionBuilder
              fields={fieldsData?.fieldsByName || {}}
              name="script"
              fieldNames={fieldsData?.fieldNameOptions || []}
              operators={{}}
              isOperatorsLoading={false}
            />
            <NDivider lineColor={color('Neutral200')({ theme })} />
            <NDivider size="xl" />
            <NTextArea
              defaultValue={ruleData?.errorMessage ? ruleData?.errorMessage : ''}
              placeholder="Enter error message"
              error={formMethods.formState.errors.errorMessage?.message}
              {...formMethods.register('errorMessage', {
                required: {
                  value: true,
                  message: t('common.error.required'),
                },
              })}
              required
            />
          </form>
        </FormProvider>
      </NContainer.Content>
      <CreateRuleModal
        isEditing={location.state.isEditing}
        visible={showEditModal}
        setVisible={setShowEditModal}
        onFormSubmit={onSubmitModal}
        title="Edit rule"
        defaultValue={{
          name: ruleData?.name || '',
          displayName: ruleData?.displayName || '',
          description: ruleData?.description,
        }}
      />
    </NContainer>
  )
}
