import { FC } from 'react'
import styled from 'styled-components'
import { color, spacing } from '../../../../../components/GlobalStyle'

const Wrapper = styled.div`
  display: inline-flex;
  flex-direction: column;
  align-items: center;
`

const LinkLine = styled.div`
  margin-top: ${spacing(2)};
  margin-bottom: ${spacing(2)};
  width: 1px;
  height: 12px;
  background: ${color('Neutral200')};
`
const Logic = styled.div`
  background: ${color('Primary700')};
  color: ${color('white')};
  border-radius: 4px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 56px;
  height: 24px;
  font-family: 'IBM Plex Sans';
  letter-spacing: 0.15em;
  text-transform: uppercase;
`

type Props = {
  className?: string
}

export const LogicOperator: FC<Props> = ({ children, className }) => {
  return (
    <Wrapper className={className}>
      <LinkLine />
      <Logic>{children}</Logic>
      <LinkLine />
    </Wrapper>
  )
}
