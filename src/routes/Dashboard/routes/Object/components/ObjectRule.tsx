import { FC } from 'react'
import { useMutation, useQueryClient } from 'react-query'
import { useParams } from 'react-router'
import styled from 'styled-components'
import { color, spacing } from '../../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../../components/Icons'
import { NColumn, NRow } from '../../../../../components/NGrid/NGrid'
import { NSpinner } from '../../../../../components/NSpinner/NSpinner'
import { NToast } from '../../../../../components/NToast'
import { APIServices } from '../../../../../services/api'
import { MetadataQueryKey } from '../../../../../services/api/endpoints/metadata'
import { ValidationRuleResponse } from '../../../../../services/api/models'
import { PostAction } from '../../../../../services/api/request'

const RuleCell = styled.div`
  height: 48px;
  padding-left: ${spacing('xl')};
  padding-right: ${spacing('xl')};
  &:hover {
    background: ${color('Neutral200')};
  }
  cursor: pointer;
`
const RuleRow = styled(NRow)`
  align-items: center;
  height: 100%;
`

type ObjectRuleProps = {
  rule: ValidationRuleResponse
  onClick?: () => void
  className?: string
}

export const ObjectRule: FC<ObjectRuleProps> = ({ rule, onClick, className }) => {
  const queryClient = useQueryClient()
  const { objName } = useParams<{ objName: string }>()

  const { mutate: postObjectRule, isLoading: isPostingRule } = useMutation(APIServices.Metadata.postObjectRule)

  const onDeleteRule = () => {
    postObjectRule(
      {
        action: PostAction.Delete,
        objName,
        ruleName: rule.name,
      },
      {
        onSuccess: async () => {
          await queryClient.invalidateQueries(MetadataQueryKey.getObjectRules)
        },
        onError: error => {
          NToast.error({
            title: 'Delete unsuccessful',
            subtitle: error.response?.data.message,
          })
        },
      },
    )
  }
  return (
    <RuleCell className={className} onClick={onClick}>
      <RuleRow>
        <NColumn flex={1}>{rule.displayName}</NColumn>
        <NColumn flex={1}>{rule.name}</NColumn>
        {isPostingRule ? (
          <NSpinner size={16} strokeWidth={2} />
        ) : (
          <GlobalIcons.Trash
            onClick={e => {
              e.stopPropagation()
              onDeleteRule()
            }}
          />
        )}
      </RuleRow>
    </RuleCell>
  )
}
