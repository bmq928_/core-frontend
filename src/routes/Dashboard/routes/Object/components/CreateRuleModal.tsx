import { Dispatch, FC, SetStateAction } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'
import { spacing } from '../../../../../components/GlobalStyle'
import { NDivider } from '../../../../../components/NDivider'
import { NColumn, NRow } from '../../../../../components/NGrid/NGrid'
import { NModal } from '../../../../../components/NModal/NModal'
import { NTextInput } from '../../../../../components/NTextInput/NTextInput'
import { fieldNameRegex } from '../../../../../utils/regex'
import { getNameFromDisplayName } from '../../../../../utils/utils'

export type CreateRuleParams = {
  name: string
  displayName: string
  description?: string
}

type CreateRuleModalProps = {
  defaultValue?: CreateRuleParams
  visible: boolean
  setVisible: Dispatch<SetStateAction<boolean>>
  title?: string
  onFormSubmit: (data: CreateRuleParams) => void
  isLoading?: boolean
  isEditing?: boolean
}

const CreateForm = styled.form`
  padding: ${spacing('lg')} ${spacing('xl')};
`

const StyledTextInput = styled(NTextInput)`
  width: 100%;
`

export const CreateRuleModal: FC<CreateRuleModalProps> = ({
  defaultValue,
  visible,
  setVisible,
  onFormSubmit,
  title,
  isLoading,
  isEditing,
}) => {
  const { handleSubmit, register, setValue, formState, getValues } = useForm()
  const { t } = useTranslation()

  const onSubmit = (data: CreateRuleParams) => {
    onFormSubmit(data)
  }

  return (
    <NModal visible={visible} setVisible={setVisible} size="large">
      <NModal.Header onClose={() => setVisible(false)} title={title} />
      <NModal.Body>
        <CreateForm>
          <NRow>
            <NColumn flex={1}>
              <StyledTextInput
                defaultValue={defaultValue?.displayName}
                {...register('displayName')}
                error={formState.errors.displayName?.message}
                placeholder={t('dashboard.objectListing.createObjectModal.displayNameField')}
                label={t('dashboard.objectListing.createObjectModal.displayNameField')}
                caption={t('dashboard.objectListing.createObjectModal.displayNameCaption')}
                onBlur={e => {
                  if (!isEditing) {
                    const formatName = getNameFromDisplayName(e.target.value)
                    !getValues('name') && setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
                  }
                }}
              />
            </NColumn>
            <NDivider vertical size="xl" />
            <NColumn flex={1}>
              <StyledTextInput
                defaultValue={defaultValue?.name}
                {...register('name', {
                  required: {
                    value: true,
                    message: t('common.error.required'),
                  },
                  pattern: {
                    value: fieldNameRegex,
                    message: 'Invalid pattern',
                  },
                })}
                required
                disabled={isEditing}
                error={formState.errors.name?.message}
                placeholder={t('dashboard.objectListing.createObjectModal.nameField')}
                label={t('dashboard.objectListing.createObjectModal.nameField')}
                caption={t('dashboard.objectListing.createObjectModal.nameFieldCaption')}
              />
            </NColumn>
          </NRow>
          <NDivider size="md" />
          <NRow>
            <NColumn flex={1}>
              <StyledTextInput
                defaultValue={defaultValue?.description}
                {...register('description')}
                placeholder={t('dashboard.objectListing.createObjectModal.descriptionField')}
                label={t('dashboard.objectListing.createObjectModal.descriptionField')}
                caption={t('dashboard.objectListing.createObjectModal.descriptionCaption')}
              />
            </NColumn>
          </NRow>
        </CreateForm>
      </NModal.Body>
      <NModal.Footer isLoading={isLoading} onCancel={() => setVisible(false)} onFinish={handleSubmit(onSubmit)} />
    </NModal>
  )
}
