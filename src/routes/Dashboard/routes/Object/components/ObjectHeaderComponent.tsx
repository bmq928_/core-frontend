import styled from 'styled-components'
import { color, spacing } from '../../../../../components/GlobalStyle'
import { NRow } from '../../../../../components/NGrid/NGrid'
import { typography } from '../../../../../components/NTypography'

const ButtonContainer = styled(NRow)`
  ${typography('h400')}
  align-items: center;
  text-transform: none;
  font-weight: normal;
  color: ${color('Neutral400')};
  button {
    min-width: 80px;
  }
`

const HeaderContainer = styled(NRow)`
  justify-content: space-between;
  align-items: center;
  margin-bottom: ${spacing('md')};
  height: 40px;
`

export const ObjectHeadersComponent = {
  ButtonContainer,
  HeaderContainer,
}
