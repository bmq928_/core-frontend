import { Dispatch, FC, SetStateAction } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useMutation, useQueryClient } from 'react-query'
import styled from 'styled-components'
import { spacing } from '../../../../../components/GlobalStyle'
import { NModal } from '../../../../../components/NModal/NModal'
import { NToast } from '../../../../../components/NToast'
import { APIServices } from '../../../../../services/api'
import { MetadataQueryKey } from '../../../../../services/api/endpoints/metadata'
import { ObjectResponse } from '../../../../../services/api/models'
import { PostAction } from '../../../../../services/api/request'
import { ObjectForm, ObjectFormType } from '../../../components/ObjectForm'

type CreateObjectModalProps = {
  objectData?: ObjectResponse
  visible: boolean
  setVisible: Dispatch<SetStateAction<boolean>>
}

const FormWrapper = styled.form`
  padding: ${spacing('lg')} ${spacing('xl')};
`

export const CreateObjectModal: FC<CreateObjectModalProps> = ({ objectData, visible, setVisible }) => {
  // post object mutation
  const queryClient = useQueryClient()

  const { mutate: postObjectApi, isLoading } = useMutation(APIServices.Metadata.postObject, {
    onSuccess: async () => {
      await queryClient.invalidateQueries(MetadataQueryKey.getObjects)
      await queryClient.invalidateQueries([MetadataQueryKey.getObject, { name: objectData?.name }])
      setVisible(false)
    },
    onError: error => {
      NToast.error({
        title: `${objectData ? 'Update' : 'Create'} internal object unsuccessful`,
        subtitle: error.response?.data.message,
      })
    },
  })

  const formMethods = useForm({
    defaultValues: {
      name: objectData?.name || '',
      recordName: objectData?.recordName.label || '',
      description: objectData?.description || '',
      displayName: objectData?.displayName || '',
    },
  })

  const { t } = useTranslation()

  const onSubmit = (data: ObjectFormType) => {
    postObjectApi({
      data: {
        displayName: data.displayName,
        recordName: {
          label: data.recordName,
          type: 'text',
        },
        name: objectData ? undefined : data.name,
        description: data.description,
      },
      action: objectData ? PostAction.Update : PostAction.Create,
      name: objectData?.name,
    })
  }

  return (
    <NModal visible={visible} setVisible={setVisible} size="large">
      <NModal.Header
        onClose={() => setVisible(false)}
        title={objectData ? t('dashboard.objectListing.editObject') : t('dashboard.objectListing.newObject')}
      />
      <NModal.Body>
        <FormWrapper>
          <ObjectForm objectData={objectData} formMethods={formMethods} />
        </FormWrapper>
      </NModal.Body>
      <NModal.Footer
        isLoading={isLoading}
        onCancel={() => setVisible(false)}
        onFinish={formMethods.handleSubmit(onSubmit)}
      />
    </NModal>
  )
}
