import { FC, useState } from 'react'
import { useMutation, useQueryClient } from 'react-query'
import { useParams } from 'react-router-dom'
import { NTableActions } from '../../../../../components/NTable/NTableActions'
import { NToast } from '../../../../../components/NToast'
import { APIServices } from '../../../../../services/api'
import { MetadataQueryKey } from '../../../../../services/api/endpoints/metadata'
import { PostAction } from '../../../../../services/api/request'

type Props = {
  fieldName: string
}

export const FieldTableActions: FC<Props> = ({ fieldName }) => {
  const [showDropList, setShowDropList] = useState(false)
  const queryClient = useQueryClient()
  const { objName } = useParams<{ objName: string }>()

  const { mutate: postObjectField, isLoading: isPostingField } = useMutation(APIServices.Metadata.postObjectField)

  const onDeleteField = () => {
    postObjectField(
      {
        body: {
          action: PostAction.Delete,
          name: fieldName,
        },
        objName,
      },
      {
        onSuccess: async () => {
          await queryClient.invalidateQueries(MetadataQueryKey.getObjectFields)
        },
        onError: error => {
          NToast.error({
            title: 'Delete unsuccessful',
            subtitle: error.response?.data.message,
          })
        },
      },
    )
  }

  return (
    <NTableActions
      showDropList={showDropList}
      setShowDropList={setShowDropList}
      options={[{ title: 'Delete', isLoading: isPostingField, onClick: onDeleteField }]}
    />
  )
}
