import React, { FC, useState } from 'react'
import { useQuery } from 'react-query'
import { useHistory, useLocation, useParams } from 'react-router-dom'
import styled from 'styled-components'
import { DASHBOARD_ROUTE } from '../../../../common/constants'
import { color, fontSize } from '../../../../components/GlobalStyle'
import { NBreadcrumbs } from '../../../../components/NBreadcrumbs/NBreadcrumbs'
import { NContainer } from '../../../../components/NLayout/NContainer'
import { SettingBoard } from '../../../../components/SettingBoard'
import { APIServices } from '../../../../services/api'
import { MetadataQueryKey } from '../../../../services/api/endpoints/metadata'
import { AddCell } from '../../components/AddCell'
import { CreateRuleModal, CreateRuleParams } from './components/CreateRuleModal'
import { ObjectHeadersComponent } from './components/ObjectHeaderComponent'
import { ObjectRule } from './components/ObjectRule'

const UpdatedText = styled.p`
  color: ${color('Neutral400')};
  font-size: ${fontSize('sm')};
  font-weight: 400;
`

export const ObjectRules: FC = () => {
  const { objName } = useParams<{ objName: string }>()
  const history = useHistory()
  const location = useLocation()
  const [showCreate, setShowCreate] = useState(false)

  const { data: rulesData } = useQuery(
    [MetadataQueryKey.getObjectRules, { name: objName }],
    APIServices.Metadata.getObjectRules,
  )

  const { data: objectData } = useQuery([MetadataQueryKey.getObject, { name: objName }], APIServices.Metadata.getObject)

  const onSubmit = (data: CreateRuleParams) => {
    history.push({
      pathname: `${location.pathname}/${data.name}`,
      state: {
        isEditing: false,
        ruleData: data,
      },
    })
  }

  return (
    <NContainer>
      <ObjectHeadersComponent.HeaderContainer>
        <NBreadcrumbs
          breadcrumbs={[
            { path: DASHBOARD_ROUTE, label: 'Dashboard' },
            {
              path: `${DASHBOARD_ROUTE}/object`,
              label: 'Object',
            },
            {
              label: objName,
            },
          ]}
        />
        {objectData?.data.updatedAt && <UpdatedText>Last updated {objectData?.data.updatedAt}</UpdatedText>}
      </ObjectHeadersComponent.HeaderContainer>
      <NContainer.Title>Rules</NContainer.Title>
      <NContainer.Content>
        <SettingBoard title={`${rulesData?.data.length} rules`}>
          {rulesData?.data.map(rule => (
            <ObjectRule
              key={rule.name}
              rule={rule}
              onClick={() =>
                history.push({ pathname: `${location.pathname}/${rule.name}`, state: { isEditing: true } })
              }
            />
          ))}
          <AddCell title="Add another rule to this object" onClick={() => setShowCreate(true)} />
        </SettingBoard>
      </NContainer.Content>
      <CreateRuleModal
        isEditing={false}
        visible={showCreate}
        setVisible={setShowCreate}
        onFormSubmit={onSubmit}
        title="New rule"
      />
    </NContainer>
  )
}
