import { FC } from 'react'
import { useQuery } from 'react-query'
import { useParams } from 'react-router-dom'
import styled, { useTheme } from 'styled-components'
import { color, spacing } from '../../../components/GlobalStyle'
import { NModal } from '../../../components/NModal/NModal'
import { NSpinner } from '../../../components/NSpinner/NSpinner'
import { APIServices } from '../../../services/api'
import { BuilderQueryKey } from '../../../services/api/endpoints/builder'
import { DataTypeResponse, ExtDataTypeResponse } from '../../../services/api/models'
import { DataTypeComponent } from './DataTypeComponent'

const DataTypeList = styled.div`
  display: grid;
  padding: ${spacing('xl')};
  grid-template-columns: repeat(2, 1fr);
  grid-column-gap: ${spacing('xl')};
`

const LoadingContainer = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
  padding: ${spacing('xl')};
`

type Props = {
  visible: boolean
  setVisible: (visible: boolean) => void
  onDatatypeSelect: (type: DataTypeResponse | ExtDataTypeResponse) => void
}

export const DatatypeSelectModal: FC<Props> = ({ visible, setVisible, onDatatypeSelect }) => {
  const theme = useTheme()
  const { objName } = useParams<{ objName: string }>()

  const { data: dataTypes, isLoading: dataTypesLoading } = useQuery(
    [BuilderQueryKey.getObjectDataTypes, { objName }],
    APIServices.Builder.getObjectDataTypes,
  )

  return (
    <NModal setVisible={setVisible} size="large" visible={visible}>
      <NModal.Header title="Select Datatype" onClose={() => setVisible(false)}></NModal.Header>
      <NModal.Body>
        {dataTypesLoading ? (
          <LoadingContainer>
            <NSpinner size={30} strokeWidth={3} color={color('Primary700')({ theme })} />
          </LoadingContainer>
        ) : (
          <DataTypeList>
            {(dataTypes?.data || []).map(type => (
              <DataTypeComponent key={type.name} type={type} onClick={() => onDatatypeSelect(type)} />
            ))}
          </DataTypeList>
        )}
      </NModal.Body>
    </NModal>
  )
}
