import { FC, Fragment } from 'react'
import { Controller } from 'react-hook-form'
import styled from 'styled-components'
import { classnames } from '../../../../common/classnames'
import { color, spacing } from '../../../../components/GlobalStyle'
import { NDivider } from '../../../../components/NDivider'
import { NColumn, NRow } from '../../../../components/NGrid/NGrid'
import { NRadio } from '../../../../components/NRadio/NRadio'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { DataTypeFormProps } from './DataTypeForm'
import { NameInputRow } from './NameInputRow'

const RadioWithBorder = styled(NRadio)`
  padding: ${spacing('md')};
  border: 1px solid ${color('Neutral200')};
  &.active {
    border: 1px solid ${color('Primary700')};
  }
  border-radius: 3px;
  height: 82px;
  width: 100%;
`
type TextTypeRadioGroupProps = {
  value: string
  onValueChange?: (type: string) => void
  subTypes: string[]
}

const tempDesc: {
  [key: string]: string
} = {
  short: 'Best for title, name, links (URL). It also enables exact search on the field',
  long: 'Best for descriptions, biography. Exact search is disabled.',
}

const TextTypeRadioGroup: FC<TextTypeRadioGroupProps> = ({ value, onValueChange, subTypes }) => {
  return (
    <NRow>
      {subTypes.map((sub, index) => (
        <Fragment key={sub}>
          <NColumn flex={1}>
            <RadioWithBorder
              className={classnames([value === sub && 'active'])}
              checked={value === sub}
              title={sub}
              value={sub}
              subtitle={tempDesc[sub]}
              onChange={() => {
                onValueChange && onValueChange(sub)
              }}
            />
          </NColumn>
          {index < subTypes.length - 1 && <NDivider vertical size="xl" />}
        </Fragment>
      ))}
    </NRow>
  )
}

export const TextForm: FC<DataTypeFormProps> = ({ formMethods, dataType, fieldData, isExternal }) => {
  const { control, setValue } = formMethods

  return (
    <>
      <NameInputRow formMethods={formMethods} dataType={dataType} fieldData={fieldData} />
      <NDivider size="xl" />
      <NTextInput
        defaultValue={fieldData?.attributes.defaultValue}
        label="Default value"
        {...formMethods.register('defaultValue')}
        disabled={isExternal}
      />
      <NDivider size="xl" />
      <Controller
        defaultValue={fieldData?.attributes.subType || dataType.attributes.subTypes[0]}
        control={control}
        name="attributes.subType"
        render={({ field: { value, onChange } }) => (
          <TextTypeRadioGroup
            subTypes={dataType.attributes.subTypes}
            value={value || ''}
            onValueChange={value => {
              if (value === 'long' && !isExternal) {
                setValue('attributes.sensitivity', 'none')
              }
              onChange(value)
            }}
          />
        )}
      />
    </>
  )
}
