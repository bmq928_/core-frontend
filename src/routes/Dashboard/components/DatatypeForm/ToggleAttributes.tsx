import { FC } from 'react'
import { Controller, UseFormReturn } from 'react-hook-form'
import styled from 'styled-components'
import { spacing } from '../../../../components/GlobalStyle'
import { NRow } from '../../../../components/NGrid/NGrid'
import { NSwitch } from '../../../../components/NSwitch/NSwitch'
import { ExtFieldResponse, FieldAttributeMeta, FieldResponse } from '../../../../services/api/models'
import { IFieldForm } from '../../routes/Object/models'

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  column-gap: ${spacing('xl')};
  row-gap: ${spacing('lg')};
  width: 100%;
`

type Props = {
  attributes: Record<string, FieldAttributeMeta>
  formMethods: UseFormReturn<IFieldForm>
  fieldData?: FieldResponse & ExtFieldResponse
}

export const ToggleAttributes: FC<Props> = ({ attributes, formMethods, fieldData }) => {
  return (
    <NRow>
      <Wrapper>
        {Object.keys(attributes).map(attrKey => (
          <Controller
            key={attrKey}
            //@ts-ignore
            defaultValue={fieldData?.[attrKey] || fieldData?.attributes?.[attrKey] || false}
            control={formMethods.control}
            // becuz isRequired and isExternalId is outside of attributes
            name={
              (attrKey === 'isRequired' || attrKey === 'isExternalId'
                ? attrKey
                : `attributes.${attrKey}`) as keyof IFieldForm
            }
            render={({ field }) => (
              <NSwitch
                title={attributes[attrKey].displayName}
                checked={Boolean(field.value)}
                onChange={value => {
                  // if externalId is true => set isUnique true
                  if (value && attrKey === 'isExternalId') {
                    formMethods.setValue('attributes.isUnique', true)
                  }
                  field.onChange(value)
                }}
              />
            )}
          />
        ))}
      </Wrapper>
    </NRow>
  )
}
