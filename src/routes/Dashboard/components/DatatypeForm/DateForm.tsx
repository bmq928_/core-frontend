import { FC } from 'react'
import { Controller } from 'react-hook-form'
import { NDatePicker } from '../../../../components/DateTime/NDatePicker'
import { NDateTimePicker } from '../../../../components/DateTime/NDateTimePicker'
import { NTimePicker } from '../../../../components/DateTime/NTimePicker'
import { NDivider } from '../../../../components/NDivider'
import { NColumn, NRow } from '../../../../components/NGrid/NGrid'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { getDateFromIsoString } from '../../../../utils/utils'
import { DataTypeFormProps } from './DataTypeForm'
import { NameInputRow } from './NameInputRow'

export const DateForm: FC<DataTypeFormProps> = ({ formMethods, dataType, fieldData, isExternal }) => {
  const { control, watch, setValue } = formMethods
  const dateType = watch('attributes.subType', fieldData?.attributes.subType || dataType.attributes.subTypes[0])

  return (
    <>
      <NameInputRow formMethods={formMethods} dataType={dataType} fieldData={fieldData} />
      <NDivider size="xl" />
      <NRow>
        <NColumn flex={1}>
          <Controller
            control={control}
            name="attributes.subType"
            defaultValue={fieldData?.attributes.subType || dataType.attributes.subTypes[0]}
            render={({ field }) => {
              return (
                <NSingleSelect
                  fullWidth
                  value={field.value}
                  onValueChange={value => {
                    setValue('defaultValue', '')
                    field.onChange(value)
                  }}
                  label="Type"
                  options={dataType.attributes.subTypes.map(type => {
                    return {
                      value: type,
                    }
                  })}
                />
              )
            }}
          />
        </NColumn>
        <NDivider vertical size="xl" />
        <NColumn flex={1}>
          <Controller
            defaultValue={fieldData?.attributes.defaultValue}
            control={control}
            name="defaultValue"
            render={({ field }) => {
              // translate ios string to date
              let curValDate = field.value ? getDateFromIsoString(field.value, dateType) : undefined

              // translate date to ios string
              const handleOnChange = (newDate: Date) => {
                field.onChange(newDate.toISOString())
              }

              switch (dateType) {
                case 'date':
                  return (
                    <NDatePicker
                      disabled={isExternal}
                      label="Default value"
                      value={curValDate}
                      onChangeValue={handleOnChange}
                    />
                  )
                case 'time':
                  return (
                    <NTimePicker
                      disabled={isExternal}
                      label="Default value"
                      value={curValDate}
                      onChangeValue={handleOnChange}
                    />
                  )
                default:
                  return (
                    <NDateTimePicker
                      disabled={isExternal}
                      label="Default value"
                      value={curValDate}
                      onValueChange={handleOnChange}
                    />
                  )
              }
            }}
          />
        </NColumn>
      </NRow>
    </>
  )
}
