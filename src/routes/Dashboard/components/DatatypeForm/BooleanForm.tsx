import React, { FC } from 'react'
import { Controller } from 'react-hook-form'
import styled from 'styled-components'
import { classnames } from '../../../../common/classnames'
import { color, spacing } from '../../../../components/GlobalStyle'
import { NDivider } from '../../../../components/NDivider'
import { NRow } from '../../../../components/NGrid/NGrid'
import { NTypography } from '../../../../components/NTypography'
import { DataTypeFormProps } from './DataTypeForm'
import { NameInputRow } from './NameInputRow'

const DefaultValue = styled.div`
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
  &.active {
    background: ${color('Primary700')};
    color: ${color('white')};
    border-radius: 3px;
  }

  transform: all 0.3s ease-in-out;
  width: 62px;
  height: 40px;
`

const DefaultValContainer = styled.div`
  display: flex;
  border: 1px solid ${color('Neutral200')};
  box-sizing: border-box;
  border-radius: 3px;

  ${DefaultValue} {
    border-right: 1px solid ${color('Neutral200')};
  }
  ${DefaultValue}:last-child {
    border-right: 0px;
  }
`
const DefaultLabel = styled(NTypography.InputLabel)`
  margin-bottom: ${spacing('xs')};
`

const defaultValues: { label: string; value: string | null }[] = [
  {
    label: 'True',
    value: 'true',
  },
  {
    label: 'False',
    value: 'false',
  },
  {
    label: 'Null',
    value: null,
  },
]

type DefaultValueSelectProps = {
  value?: string | null
  onValueChange: (val: string | null) => void
}

const DefaultValueSelect: FC<DefaultValueSelectProps> = ({ value, onValueChange }) => {
  return (
    <div>
      <DefaultLabel>Default Value</DefaultLabel>
      <DefaultValContainer>
        {defaultValues.map(val => (
          <DefaultValue
            className={classnames([value === val.value && 'active'])}
            key={val.label}
            onClick={() => onValueChange(val.value)}>
            {val.label}
          </DefaultValue>
        ))}
      </DefaultValContainer>
    </div>
  )
}

export const BooleanForm: FC<DataTypeFormProps> = ({ formMethods, fieldData, dataType, isExternal }) => {
  const { control } = formMethods

  return (
    <>
      <NameInputRow formMethods={formMethods} dataType={dataType} fieldData={fieldData} />
      {!isExternal && (
        <>
          <NDivider size="xl" />
          <NRow>
            <Controller
              defaultValue={fieldData?.attributes?.defaultValue ? fieldData.attributes.defaultValue : ''}
              control={control}
              name="defaultValue"
              render={({ field }) => <DefaultValueSelect value={field.value} onValueChange={field.onChange} />}
            />
          </NRow>
        </>
      )}
    </>
  )
}
