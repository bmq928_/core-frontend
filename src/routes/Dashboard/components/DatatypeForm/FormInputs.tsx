import { FC } from 'react'
import { BooleanForm } from './BooleanForm'
import { CurrencyForm } from './CurrencyForm'
import { DataTypeFormProps } from './DataTypeForm'
import { DateForm } from './DateForm'
import { ExternalRelationForm } from './ExternalRelationForm'
import { GeneratedForm } from './GeneratedForm'
import { IndirectRelationForm } from './IndirectRelationForm'
import { NumberForm } from './NumberForm'
import { RelationshipForm } from './RelationshipForm'
import { SelectForm } from './SelectForm'
import { TextForm } from './TextForm'

export const FormInputs: FC<DataTypeFormProps> = props => {
  switch (props.dataType?.name) {
    case 'text':
      return <TextForm {...props} />
    case 'boolean':
      return <BooleanForm {...props} />
    case 'numeric':
      return <NumberForm {...props} />
    case 'pickList':
      return <SelectForm {...props} />
    case 'dateTime':
      return <DateForm {...props} />
    case 'relation':
      return <RelationshipForm {...props} />
    case 'generated':
      return <GeneratedForm {...props} />
    case 'externalRelation':
      return <ExternalRelationForm {...props} />
    case 'currency':
      return <CurrencyForm {...props} />
    case 'indirectRelation':
      return <IndirectRelationForm {...props} />
    default:
      return null
  }
}
