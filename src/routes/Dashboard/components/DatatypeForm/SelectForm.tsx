import { FC, useMemo } from 'react'
import { Controller } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'
import { NDivider } from '../../../../components/NDivider'
import { NColumn, NRow } from '../../../../components/NGrid/NGrid'
import { SelectOption } from '../../../../components/NSelect/model'
import { NMultiSelect } from '../../../../components/NSelect/NMultiSelect'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { NTextArea } from '../../../../components/NTextArea/NTextArea'
import { useSearchText } from '../../../../hooks/useSearchText'
import { transformOptionsToString, transformStringToOptions } from '../../../../utils/utils'
import { DataTypeFormProps } from './DataTypeForm'
import { NameInputRow } from './NameInputRow'

const ValuesArea = styled(NTextArea)`
  width: 100%;
`

export const SelectForm: FC<DataTypeFormProps> = ({ formMethods, dataType, fieldData }) => {
  const { t } = useTranslation()
  const { register, formState, control, watch, setValue } = formMethods

  const watchAttributes = watch('attributes') || fieldData?.attributes

  const [{ searchText, searchValue }, handleChangeOptions] = useSearchText(transformOptionsToString(fieldData?.value))

  const options = useMemo<SelectOption[]>(() => {
    const stringOptions: string[] = JSON.parse(transformStringToOptions(searchText) || '[]') || []
    return stringOptions.map(strVal => {
      return {
        label: strVal,
        value: strVal,
      }
    })
  }, [searchText])

  return (
    <>
      <NameInputRow formMethods={formMethods} dataType={dataType} fieldData={fieldData} />
      <NDivider size="xl" />
      <NRow>
        <NColumn flex={1}>
          <NRow>
            <Controller
              control={control}
              name="attributes.subType"
              defaultValue={fieldData?.attributes.subType || dataType.attributes.subTypes[0]}
              render={({ field }) => (
                <NSingleSelect
                  fullWidth
                  value={field.value}
                  onValueChange={newType => {
                    field.onChange(newType)
                    setValue('defaultValue', '')
                  }}
                  label="Type"
                  options={dataType.attributes.subTypes.map(type => {
                    return {
                      value: type,
                    }
                  })}
                />
              )}
            />
          </NRow>
        </NColumn>
        <NDivider vertical size="xl" />
        <NColumn flex={1}>
          <ValuesArea
            error={formState.errors.value?.message}
            label="Values (one line per value)"
            rows={5}
            placeholder="Ex: Morning"
            {...register('value', {
              required: {
                value: true,
                message: t('common.error.required'),
              },
            })}
            required
            value={searchValue}
            onChange={e => {
              handleChangeOptions(e.target.value)
            }}
          />
        </NColumn>
      </NRow>
      <NDivider size="xl" />
      <Controller
        control={control}
        name="defaultValue"
        defaultValue={fieldData?.attributes.defaultValue}
        render={({ field: { value, onChange } }) => {
          if (watchAttributes?.subType === 'multi') {
            const mulValues: string[] = JSON.parse(value || '[]')
            return (
              <NMultiSelect
                label="Default value"
                placeholder="Multiple pick"
                fullWidth
                values={mulValues}
                options={options}
                onValuesChange={values => {
                  onChange(JSON.stringify(values))
                }}
              />
            )
          }
          return (
            <NSingleSelect
              label="Default value"
              placeholder="Single pick"
              fullWidth
              value={value}
              options={options}
              onValueChange={onChange}
            />
          )
        }}
      />
    </>
  )
}
