import React, { FC } from 'react'
import { Controller } from 'react-hook-form'
import { NDivider } from '../../../../components/NDivider'
import { NColumn, NRow } from '../../../../components/NGrid/NGrid'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { DataTypeFormProps } from './DataTypeForm'
import { NameInputRow } from './NameInputRow'

export const NumberForm: FC<DataTypeFormProps> = ({ formMethods, dataType, fieldData, isExternal }) => {
  const { control } = formMethods
  return (
    <>
      <NameInputRow formMethods={formMethods} dataType={dataType} fieldData={fieldData} />
      <NDivider size="xl" />
      <NRow>
        <NColumn>
          <NTextInput
            defaultValue={fieldData?.attributes.defaultValue}
            type="number"
            label="Default value"
            {...formMethods.register('defaultValue')}
            disabled={isExternal}
          />
        </NColumn>
        <NDivider vertical size="xl" />
        <NColumn>
          <Controller
            defaultValue={fieldData?.attributes.subType || dataType.attributes.subTypes[0]}
            control={control}
            name="attributes.subType"
            render={({ field }) => (
              <NSingleSelect
                fullWidth
                value={field.value}
                onValueChange={field.onChange}
                label="Number Format"
                options={dataType.attributes.subTypes.map(type => {
                  return {
                    value: type,
                  }
                })}
              />
            )}
          />
        </NColumn>
      </NRow>
    </>
  )
}
