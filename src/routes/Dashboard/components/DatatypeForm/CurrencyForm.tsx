import { FC } from 'react'
import { NDivider } from '../../../../components/NDivider'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { DataTypeFormProps } from './DataTypeForm'
import { NameInputRow } from './NameInputRow'

export const CurrencyForm: FC<DataTypeFormProps> = ({ formMethods, dataType, fieldData, isExternal }) => {
  return (
    <>
      <NameInputRow formMethods={formMethods} dataType={dataType} fieldData={fieldData} />
      <NDivider size="xl" />
      <NTextInput
        defaultValue={fieldData?.attributes.defaultValue}
        label="Default value"
        {...formMethods.register('defaultValue', {
          min: {
            value: 0,
            message: 'Default value cannot be negative',
          },
        })}
        disabled={isExternal}
        error={formMethods.formState.errors.defaultValue?.message}
      />
    </>
  )
}
