import { FC } from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'
import { NDivider } from '../../../../components/NDivider'
import { NColumn, NRow } from '../../../../components/NGrid/NGrid'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { fieldNameRegex } from '../../../../utils/regex'
import { getNameFromDisplayName } from '../../../../utils/utils'
import { DataTypeFormProps } from './DataTypeForm'

const NameTextInput = styled(NTextInput)`
  width: 100%;
`

export const NameInputRow: FC<DataTypeFormProps> = ({ fieldData, formMethods }) => {
  const { register, formState, setValue, getValues } = formMethods
  const { t } = useTranslation()

  return (
    <NRow>
      <NColumn flex={1}>
        <NameTextInput
          defaultValue={fieldData?.displayName || ''}
          placeholder="Display name"
          label="Display name"
          caption="Display name"
          error={formState.errors.displayName?.message}
          required
          {...register('displayName', {
            required: {
              value: true,
              message: t('common.error.required'),
            },
          })}
          onBlur={e => {
            if (!!!fieldData) {
              const formatName = getNameFromDisplayName(e.target.value)
              !getValues('name') && setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
            }
          }}
        />
      </NColumn>
      <NDivider vertical size="xl" />
      <NColumn flex={1}>
        <NameTextInput
          placeholder="Name"
          label="Name"
          caption="No space and capitalize letters are allowed for name of tribute"
          error={formState.errors.name?.message}
          required
          {...register('name', {
            required: {
              value: true,
              message: t('common.error.required'),
            },
            pattern: {
              value: fieldNameRegex,
              message: 'Invalid pattern',
            },
          })}
          disabled={!!fieldData}
          defaultValue={fieldData?.name ? fieldData?.name : ''}
        />
      </NColumn>
    </NRow>
  )
}
