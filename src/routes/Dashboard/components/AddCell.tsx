import { FC } from 'react'
import styled from 'styled-components'
import { color, spacing } from '../../../components/GlobalStyle'
import { GlobalIcons } from '../../../components/Icons'

const Cell = styled.div`
  display: flex;
  align-items: center;
  padding-left: ${spacing('xl')};
  height: 48px;
  color: ${color('Primary700')};
  &:hover {
    background: ${color('Primary200')};
  }
  cursor: pointer;
`

const AddIcon = styled.div`
  background: ${color('Primary400')};
  width: 48px;
  height: 24px;
  border-radius: 3px;
  margin-right: ${spacing('md')};
  justify-content: center;
  align-items: center;
  display: flex;
  color: ${color('white')};
  svg {
    height: 11px;
    width: 11px;
  }
`

type AddCellProps = {
  title?: string
  onClick?: () => void
}

export const AddCell: FC<AddCellProps> = ({ title, onClick }) => {
  return (
    <Cell onClick={onClick}>
      <AddIcon>
        <GlobalIcons.Add />
      </AddIcon>
      {title}
    </Cell>
  )
}
