import { FC } from 'react'
import { UseFormReturn } from 'react-hook-form'
import { NDivider } from '../../../components/NDivider'
import { NColumn, NRow } from '../../../components/NGrid/NGrid'
import { NTextArea } from '../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../components/NTextInput/NTextInput'
import { useValidateString } from '../../../hooks/useValidateString'
import { ObjectResponse } from '../../../services/api/models'
import { fieldNameRegex } from '../../../utils/regex'
import { getNameFromDisplayName } from '../../../utils/utils'

export type ObjectFormType = {
  name: string
  displayName: string
  recordName: string
  description: string
}

type Props = {
  objectData?: ObjectResponse
  formMethods: UseFormReturn<ObjectFormType>
}

export const ObjectForm: FC<Props> = ({ objectData, formMethods }) => {
  const { register, setValue, formState, getValues } = formMethods
  const { validateFunction } = useValidateString()

  return (
    <form>
      <NRow>
        <NColumn>
          <NTextInput
            {...register('displayName', {
              validate: validateFunction,
            })}
            label="Display name"
            caption="The display name of the object"
            placeholder="Enter display name"
            error={formState.errors.displayName?.message}
            required
            onBlur={e => {
              if (!!!objectData) {
                const formatName = getNameFromDisplayName(e.target.value)
                !getValues('name') && setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
                !getValues('recordName') &&
                  setValue('recordName', e.target.value, { shouldDirty: true, shouldValidate: true })
              }
            }}
          />
        </NColumn>
        <NDivider vertical size="xl" />
        <NColumn>
          <NTextInput
            {...register('name', {
              validate: !objectData ? validateFunction : undefined,
              pattern: {
                value: fieldNameRegex,
                message: 'Invalid pattern',
              },
            })}
            error={formState.errors.name?.message}
            name="name"
            label="API name"
            required
            caption="Name is used to generate the API routes and databases tables/collections"
            placeholder="Enter API name"
            disabled={!!objectData}
          />
        </NColumn>
      </NRow>
      <NDivider size="md" />
      <NRow>
        <NColumn>
          <NTextInput
            {...register('recordName', {
              validate: validateFunction,
            })}
            required
            error={formState.errors.recordName?.message}
            name="recordName"
            label="Record name"
            caption="Label of the record name"
            placeholder="Enter record name"
          />
        </NColumn>
        <NDivider vertical size="xl" />
        <NColumn>
          <NTextArea
            {...register('description')}
            label="Description"
            caption="The description of the object"
            placeholder="Enter description"
            rows={3}
          />
        </NColumn>
      </NRow>
    </form>
  )
}
