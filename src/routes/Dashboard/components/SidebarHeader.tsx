import { FC } from 'react'
import styled, { useTheme } from 'styled-components'
import { color, spacing } from '../../../components/GlobalStyle'
import { NDivider } from '../../../components/NDivider'
import { typography } from '../../../components/NTypography'
import { ReactComponent as BackArrow } from './icons/BackArrow.svg'

const NavWrapper = styled.div`
  padding-left: ${spacing('md')};
  padding-right: ${spacing('md')};
`

const BackWrapper = styled.div`
  display: flex;
  align-items: center;
  color: ${color('Neutral500')};
  cursor: pointer;
`
const NavText = styled.p`
  ${typography('x-small-ui-text')}
`
const Title = styled.p`
  ${typography('h500')};
  margin-bottom: ${spacing('md')};
  text-transform: capitalize;
`

type Props = {
  onBack: () => void
  backTitle?: string
  title?: string
}

export const SidebarHeader: FC<Props> = ({ onBack, backTitle, title }) => {
  const theme = useTheme()

  return (
    <>
      <NavWrapper>
        <BackWrapper onClick={onBack}>
          <BackArrow />
          <NDivider vertical size="xs" />
          <NavText>{backTitle}</NavText>
        </BackWrapper>
        <NDivider size="lg" />
        <Title>{title}</Title>
      </NavWrapper>
      <NDivider lineSize={1} lineColor={color('Neutral200')({ theme })} size="md" />
    </>
  )
}
