import axios from 'axios'
import * as React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { Auth } from './Auth/Auth'
import { PrivateRoutes } from './PrivateRoutes'

type MainProps = {
  children?: React.ReactNode
}

export function Main({}: MainProps) {
  React.useEffect(() => {
    // check server
    axios
      .get(`${process.env.REACT_APP_API_HOST || ''}/v1/_ping`)
      .then(async res => ({
        status: res.status,
        data: await res.data,
      }))
      .then(data => console.info('CHECK_SERVER', data))
  }, [])

  return (
    <BrowserRouter>
      <Switch>
        <Route path="/auth">
          <Auth />
        </Route>
        <Route path="/">
          <PrivateRoutes />
        </Route>
      </Switch>
    </BrowserRouter>
  )
}
