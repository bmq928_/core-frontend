import * as React from 'react'
import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'
import { useDispatch } from 'react-redux'
import { Redirect, Route, Switch, useHistory, useLocation } from 'react-router-dom'
import styled, { useTheme } from 'styled-components'
import { DASHBOARD_ROUTE } from '../common/constants'
import { color } from '../components/GlobalStyle'
import { NSpinner } from '../components/NSpinner/NSpinner'
import { authActions, useAuth } from '../redux/slices/authSlice'
import { sessionActions, useSession } from '../redux/slices/sessionSlice'
import { AppViewer } from './AppViewer/AppViewer'
import { Dashboard } from './Dashboard/Dashboard'
import { FlowBuilder } from './FlowBuilder/FlowBuilder'
import { LayoutBuilderProvider } from './LayoutBuilder/contexts/LayoutBuilderContext'
import { LayoutBuilderDataProvider } from './LayoutBuilder/contexts/LayoutBuilderDataContext'
import { LayoutBuilder } from './LayoutBuilder/LayoutBuilder'
import { NotFound } from './NotFound/NotFound'

const Overlay = styled('div')`
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background: ${color('white')};
  display: flex;
  justify-content: center;
  align-items: center;
`

type PrivateRoutesProps = {
  children?: React.ReactNode
}

export function PrivateRoutes({}: PrivateRoutesProps) {
  const { pathname, state } = useLocation<{ redirect: string }>()
  const { accessToken, canRedirectToLastUrl } = useAuth()
  const dispatch = useDispatch()
  const { status, message, lastUrl } = useSession()
  const history = useHistory()

  const theme = useTheme()

  React.useEffect(() => {
    if (accessToken) {
      dispatch(sessionActions.getSession())
    }
  }, [accessToken, dispatch])

  if (!accessToken) {
    return <Redirect to={{ pathname: '/auth', state: { redirect: pathname } }} />
  }

  const isLoading = status === 'loading'
  const isError = status === 'error'

  if (isLoading) {
    return (
      <Overlay>
        <NSpinner size={30} strokeWidth={2} color={color('Primary900')({ theme })} />
      </Overlay>
    )
  }

  if (isError) {
    return (
      <Overlay>
        <div>Something went wrong!</div>
        <div>{message}</div>
      </Overlay>
    )
  }

  const hasRedirect = state?.redirect !== undefined
  const redirectToReference = hasRedirect && state!.redirect !== '/'

  if (redirectToReference) {
    return <Redirect to={state!.redirect} />
  }

  if (canRedirectToLastUrl && pathname === '/' && !!lastUrl) {
    console.log('redirect', lastUrl)
    dispatch(authActions.stopRedirectToLastUrl())
    history.push(lastUrl)
  }

  return (
    <DndProvider backend={HTML5Backend}>
      <Switch>
        <Route path={`${DASHBOARD_ROUTE}`}>
          <Dashboard />
        </Route>
        <Route path={['/layout-builder/new', '/layout-builder/:layoutId']}>
          <LayoutBuilderProvider>
            <LayoutBuilderDataProvider>
              <LayoutBuilder />
            </LayoutBuilderDataProvider>
          </LayoutBuilderProvider>
        </Route>
        <Route path="/flow-builder/:flowName">
          <FlowBuilder />
        </Route>
        <Route path={['/:objectName/create', '/:objectName/:id', '/:layoutId', '/']}>
          <AppViewer />
        </Route>
        <Route>
          <NotFound />
        </Route>
      </Switch>
    </DndProvider>
  )
}
