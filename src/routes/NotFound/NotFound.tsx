import * as React from 'react'
import { useHistory } from 'react-router-dom'
import styled from 'styled-components'
import { NButton } from '../../components/NButton/NButton'

const Wrapper = styled('div')`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  min-height: 100vh;
`

type NotFoundProps = {
  children?: React.ReactNode
}

export function NotFound({}: NotFoundProps) {
  const { push } = useHistory()
  return (
    <Wrapper>
      <h1>404: The page not found!</h1>
      <div>
        <NButton type="link" onClick={() => push('/')}>
          Go to Home
        </NButton>
      </div>
    </Wrapper>
  )
}
