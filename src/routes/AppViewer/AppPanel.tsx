import React, { FC, useEffect, useMemo, useState } from 'react'
import { useMutation, useQuery } from 'react-query'
import { useDispatch } from 'react-redux'
import { NavLink, Route, Switch, useHistory, useLocation, useParams } from 'react-router-dom'
import styled from 'styled-components'
import { color, spacing } from '../../components/GlobalStyle'
import { NButton } from '../../components/NButton/NButton'
import { NDivider } from '../../components/NDivider'
import { NToast } from '../../components/NToast'
import { NTypography } from '../../components/NTypography'
import { sessionActions, useSession } from '../../redux/slices/sessionSlice'
import { APIServices } from '../../services/api'
import { AppQueryKey } from '../../services/api/endpoints/app'
import { DataQueryKey } from '../../services/api/endpoints/data'
import { AppPanelTabBar, TabData } from './components/AppPanelTabBar'
import { LoadingView } from './components/LoadingView'
import { ActionDataProvider } from './contexts/ActionDataContext'
import { RecordPage, RecordPageType } from './Layout/RecordPage'
import { RecordPageMutation } from './Layout/RecordPageMutation'
import { TabPage } from './Layout/TabPage'

const Container = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  flex: 1;
  position: relative;
  overflow: auto;
  background: ${color('Neutral100')};
`

const TabBarPad = styled.div`
  display: flex;
  padding-top: 40px;
`

const EmptyTabBar = styled.div`
  height: 40px;
  position: fixed;
  top: 56px;
`

const LayoutContainer = styled.div`
  display: flex;
  flex-direction: column;
  padding: ${spacing('md')};
`

type AppPanelProps = {
  appId: string
}

export const AppPanel: FC<AppPanelProps> = ({ appId }) => {
  const { layoutId, objectName, id } = useParams<{ objectName?: string; layoutId?: string; id?: string }>()
  const { pathname } = useLocation()

  const history = useHistory()
  const session = useSession()
  const dispatch = useDispatch()

  // get app pages (tabs on tabbar)
  const { data: appPage, isLoading } = useQuery([AppQueryKey.getAppPages, { id: appId }], APIServices.Apps.getAppPages)

  const { mutate: updateSession } = useMutation(APIServices.Session.updateSession, {
    onSuccess(d) {
      dispatch(sessionActions.setSession(d.data))
    },
  })

  useEffect(() => {
    if (session.guid) {
      updateSession({
        last_used_app: appId,
        last_url: pathname,
      })
    }
  }, [session.guid, appId, pathname, updateSession])

  const [selectedTab, setSelectedTab] = useState<string | undefined>(layoutId)

  // get object data
  const { data: contents, isFetching: isLoadingContents } = useQuery(
    [DataQueryKey.getSingleObjectData, { name: objectName!, guid: id! }],
    APIServices.Data.getSingleObjectData,
    {
      enabled: !!objectName && !!id,
      // TODO: Handle error on UI
      onError: error => {
        NToast.error({ title: 'Get contents error', subtitle: error.response?.data.message })
      },
      select(res) {
        return res.data
      },
    },
  )

  // update when appId, objectName change
  const tabs = useMemo(() => {
    if (!appPage) {
      return []
    }

    const result: TabData[] = appPage.data.map(appTab => ({
      value: appTab.id,
      displayName: appTab.name,
    }))

    // if object name add temporary tab and move to it
    if (objectName) {
      //@ts-ignore
      const tabName: string | undefined = contents?.name
      result.push({
        displayName: tabName || (!id ? `Create ${objectName}` : objectName),
        value: 'Not found',
        isLoading: isLoadingContents,
      })
      setSelectedTab('Not found')
      return result
    }

    // if url has layout
    if (layoutId) {
      const foundTab = result.find(tab => tab.value === layoutId)
      if (!foundTab) {
        if (result.length && result[0].value) {
          const newTab = result[0].value
          setSelectedTab(newTab)
          history.push(`/${newTab}`)
        } else {
          history.push('/')
        }
      } else {
        setSelectedTab(foundTab.value)
      }

      return result
    }

    // unrecognized url
    if (result.length) {
      const newTab = result[0].value
      setSelectedTab(newTab)
      history.push(`/${newTab}`)
    } else {
      history.push('/')
    }

    return result
  }, [appPage, layoutId, objectName, history, contents, isLoadingContents, id])

  const onTabChange = (value: string) => {
    if (value === 'Not found') {
      return
    }
    history.push(`/${value}`)
    setSelectedTab(value)
  }

  return (
    <Container>
      {!!tabs.length ? <AppPanelTabBar tabs={tabs} value={selectedTab} onTabChange={onTabChange} /> : <EmptyTabBar />}
      <TabBarPad />
      <ActionDataProvider
        id={id}
        postDelete={() => {
          history.goBack()
        }}>
        <LayoutContainer>
          {layoutId && <TabPage layoutId={layoutId} />}
          {objectName && (
            <Switch>
              <Route path={['/:objectName/create', '/:objectName/:id/edit']}>
                <RecordPageMutation />
              </Route>
              <Route path="/:objectName/:id">
                <RecordPage objectName={objectName} mode={RecordPageType.Detail} />
              </Route>
            </Switch>
          )}
        </LayoutContainer>
      </ActionDataProvider>
      {!layoutId && !objectName && <EmptyTab isLoading={isLoading} />}
    </Container>
  )
}

const EmptyContainer = styled.div`
  display: flex;
  flex: 1;
  background: ${color('Neutral200')};
`
const EmptyWrapper = styled.div`
  padding: ${spacing('md')} ${spacing('lg')};
  margin: ${spacing('md')} ${spacing('lg')};
  background: ${color('white')};
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`
const EmptyTab: FC<{ isLoading?: boolean }> = ({ isLoading }) => {
  if (isLoading) {
    return <LoadingView />
  }
  return (
    <EmptyContainer>
      <EmptyWrapper>
        <NTypography.Overline>No tab</NTypography.Overline>
        <NDivider size="md" />
        <NTypography.Headline>Your app is empty. Go to editor to setup your app.</NTypography.Headline>
        <NDivider size="md" />
        <NavLink to="/setup/app">
          <NButton type="primary">Switch To Editor</NButton>
        </NavLink>
      </EmptyWrapper>
    </EmptyContainer>
  )
}
