import React, { createContext, FC } from 'react'
import { useQuery } from 'react-query'
import { useParams } from 'react-router'
import { NToast } from '../../../components/NToast'
import { SessionState, useSession } from '../../../redux/slices/sessionSlice'
import { APIServices } from '../../../services/api'
import { DataQueryKey } from '../../../services/api/endpoints/data'
import { DataResponse } from '../../../services/api/models'
import { Element } from '../../LayoutBuilder/utils/models'

export type InputMap = {
  curUser?: SessionState['user']
  curRecord?: DataResponse
  now: string
}

export type LayoutViewerContext = {
  elements: Record<string, Element>
  isLoadingContents?: boolean
  layoutType: string
  layoutId: string
  contents?: Record<string, any> & DataResponse
  //
  objectName?: string
  inputMap: InputMap
}

export const LayoutViewerContext = createContext<LayoutViewerContext | string>(
  'useLayoutViewer should be used inside LayoutViewerProvider',
)

export type LayoutViewerContextProps = {
  elements: Record<string, Element>
  layoutType: string
  //
  objectName?: string
  layoutId: string
}

export const LayoutViewerProvider: FC<LayoutViewerContextProps> = ({
  layoutId,
  elements,
  layoutType,
  objectName,
  children,
}) => {
  const { id: guid } = useParams<{ id?: string }>()
  const session = useSession()

  // get default value
  const { data: contents, isFetching: isLoadingContents } = useQuery(
    [DataQueryKey.getSingleObjectData, { name: objectName!, guid: guid! }],
    APIServices.Data.getSingleObjectData,
    {
      enabled: !!objectName && !!guid,
      // TODO: Handle error on UI
      onError: error => {
        NToast.error({ title: 'Get contents error', subtitle: error.response?.data.message })
      },
      select(res) {
        return res.data
      },
    },
  )

  const inputMap = React.useMemo(
    () => ({
      curUser: session.user,
      curRecord: contents,
      now: new Date().toISOString(),
    }),
    [contents, session],
  )

  const value = {
    layoutId,
    elements,
    layoutType,
    isLoadingContents,
    contents,
    //
    objectName,
    inputMap,
  }
  return <LayoutViewerContext.Provider value={value}>{children}</LayoutViewerContext.Provider>
}

export const useLayoutViewer = () => {
  const context = React.useContext(LayoutViewerContext)
  if (typeof context === 'string') {
    throw new Error(context)
  }
  return context
}
