import { get } from 'lodash'
import { FilterItem } from '../../services/api/models'
import { InputMap } from './contexts/LayoutViewerContext'

export function resolveFilterVariable(filters: FilterItem[][] = [], inputMap: InputMap) {
  return filters.map(filter =>
    filter.map(f => {
      const value = f.value as any
      const isVariable = value && value.includes('$')
      return {
        ...f,
        value: isVariable ? get(inputMap, value.replace('$', ''), null) : value,
      }
    }),
  )
}

export function resolveButtonVariable(inputs: Record<string, string> = {}, inputMap: InputMap) {
  let result = {} as Record<string, any>
  for (let [key, path] of Object.entries(inputs)) {
    result[key] = path
    if (typeof path === 'string' && path.includes('$')) {
      result[key] = get(inputMap, path.replace('$', ''), null)
    }
  }
  return result
}

const VARIABLE_REGEX = new RegExp(/\{\$(.*?)\}/, 'gm')

export function resolveTextVariable(raw: string = '', inputMap: InputMap) {
  if (!raw) {
    return ''
  }
  const groups = raw.matchAll(VARIABLE_REGEX)
  let result = raw
  for (let group of groups) {
    result = result.replace(group[0], get(inputMap, group[1], group[0]))
  }
  return result
}

const CTX_EXPIRE_TIME = 1 * 60 * 1000

const CTX_SESSION_ID = 'LAST_USED_CTXID'

type SavedCtx = {
  actionId: string
  ctxId: string
  expiredAt: number
}

export function saveCtxId(actionId: string, ctxId: string, expiredIn: number = CTX_EXPIRE_TIME) {
  sessionStorage.setItem(
    CTX_SESSION_ID,
    JSON.stringify({ actionId, ctxId, expiredAt: new Date().getTime() + expiredIn }),
  )
}
export function getCtxId(actionId: string, initialValue: string = '') {
  try {
    const ctx = sessionStorage.getItem(CTX_SESSION_ID)
    if (ctx) {
      const ctxObj = JSON.parse(ctx) as SavedCtx
      if (actionId !== ctxObj.actionId) {
        return initialValue
      }
      if (new Date().getTime() < ctxObj.expiredAt) {
        return ctxObj.ctxId
      }
    }
    return initialValue
  } catch (e) {
    return initialValue
  }
}
export function removeCtxId() {
  sessionStorage.removeItem(CTX_SESSION_ID)
}
