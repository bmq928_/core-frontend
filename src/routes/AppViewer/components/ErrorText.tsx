import styled from 'styled-components'
import { color } from '../../../components/GlobalStyle'
import { NTypography } from '../../../components/NTypography'

export const ErrorText = styled(NTypography.Caption)`
  color: ${color('Red700')};
`
