import styled from 'styled-components'
import { color, spacing } from '../../../components/GlobalStyle'
import { NTypography } from '../../../components/NTypography'

export const ErrorContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  justify-content: center;
  align-items: center;
`
export const ErrorTitle = styled(NTypography.Headline)`
  color: ${color('Red700')};
  margin-bottom: ${spacing('md')};
`
export const ErrorSubtitle = styled(NTypography.Caption)``
