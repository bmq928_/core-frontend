import React from 'react'
import styled, { useTheme } from 'styled-components'
import { color } from '../../../components/GlobalStyle'
import { NSpinner } from '../../../components/NSpinner/NSpinner'

const LoadingContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  justify-content: center;
  align-items: center;
`
export function LoadingView() {
  const theme = useTheme()

  return (
    <LoadingContainer>
      <NSpinner size={30} strokeWidth={2} color={color('Primary900')({ theme })} />
    </LoadingContainer>
  )
}
