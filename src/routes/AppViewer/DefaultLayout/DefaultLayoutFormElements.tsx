import { nanoid } from 'nanoid'
import { ROOT_ELEMENT } from '../../LayoutBuilder/utils/builder'
import { Element, ElementType } from '../../LayoutBuilder/utils/models'

// Record Detail
const generateRecordDetail = (fieldNames: string[]) => {
  const result: Element = {
    id: nanoid(),
    label: 'Record Detail',
    type: ElementType.Field,
    component: 'RecordDetail',
    props: {
      isPrimary: true,
      numberOfColumns: 2,
      fieldNames,
      actions: ['editable', 'submit'],
    },
    children: [],
  }
  return result
}

export const GenerateInputElements = {
  generateRecordDetail,
}

//
export const DefaultLayoutFormElements: Record<string, Element> = {
  [ROOT_ELEMENT]: {
    id: ROOT_ELEMENT,
    label: 'Body',
    type: ElementType.Layout,
    component: '',
    props: {},
    children: [],
  },
}
