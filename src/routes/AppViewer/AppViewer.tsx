import React, { FC, useEffect, useState } from 'react'
import { useQuery } from 'react-query'
import { useDispatch } from 'react-redux'
import { NavLink, useHistory } from 'react-router-dom'
import styled from 'styled-components'
import { DASHBOARD_ROUTE } from '../../common/constants'
import { AppHeader } from '../../components/AppHeader/AppHeader'
import { color } from '../../components/GlobalStyle'
import { NAvatar } from '../../components/NAvatar/NAvatar'
import { NButton } from '../../components/NButton/NButton'
import { NDivider } from '../../components/NDivider'
import { NTypography, typography } from '../../components/NTypography'
import { sessionActions, useSession } from '../../redux/slices/sessionSlice'
import { APIServices } from '../../services/api'
import { AppQueryKey } from '../../services/api/endpoints/app'
import { AppPanel } from './AppPanel'
import { LoadingView } from './components/LoadingView'

const Wrapper = styled('div')`
  display: flex;
  flex-direction: column;
  height: 100vh;
`

const AppNameWrapper = styled.div`
  display: flex;
  align-items: center;
`

const AppName = styled('div')`
  ${typography('h500')}
  color:  ${color('Neutral700')};
`

type AppViewerProps = {
  children?: React.ReactNode
}

export function AppViewer({}: AppViewerProps) {
  const dispatch = useDispatch()
  const history = useHistory()
  const { lastUsedApp } = useSession()
  const [selectedApp, setSelectedApp] = useState<string | undefined>(lastUsedApp)

  const { data: appsList } = useQuery([AppQueryKey.getApps, { limit: 5 }], APIServices.Apps.getApps)

  const { data: appData, isLoading: isLoadingApp } = useQuery(
    [AppQueryKey.getApp, { id: selectedApp }],
    APIServices.Apps.getApp,
    {
      enabled: !!selectedApp,
    },
  )

  useEffect(() => {
    //handle when there isn't any last used app
    if (lastUsedApp === undefined && appsList?.data && appsList.data.data.length > 0) {
      setSelectedApp(appsList.data.data[0].id)
    }
  }, [lastUsedApp, appsList])

  useEffect(() => {
    return () => {
      dispatch(sessionActions.getSession())
    }
  }, [dispatch])

  const onSelectApp = (appId?: string) => {
    setSelectedApp(appId)
    history.push('/')
  }

  return (
    <Wrapper>
      <AppHeader
        selectedApp={appData?.data}
        left={
          <AppNameWrapper>
            <NDivider vertical size="xs" />
            {appData?.data.name && <NAvatar size={24} variant="circle" name={appData?.data.name} />}
            <NDivider vertical size="xs" />
            <AppName>{appData?.data.name}</AppName>
          </AppNameWrapper>
        }
        onSelectApp={onSelectApp}
        right={
          <NButton type="outline" size="small" onClick={() => history.push(`${DASHBOARD_ROUTE}/app`)}>
            Go to Setup
          </NButton>
        }
      />
      {!selectedApp && <EmptyApp isLoading={isLoadingApp} />}
      {selectedApp && <AppPanel appId={selectedApp} />}
    </Wrapper>
  )
}

const EmptyContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  justify-content: center;
  align-items: center;
`
const EmptyApp: FC<{ isLoading?: boolean }> = ({ isLoading }) => {
  if (isLoading) {
    return <LoadingView />
  }
  return (
    <EmptyContainer>
      <NTypography.Headline>No application</NTypography.Headline>
      <NDivider size="md" />
      <NavLink to="/app">
        <NButton type="primary">Go back</NButton>
      </NavLink>
    </EmptyContainer>
  )
}
