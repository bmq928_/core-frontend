import React, { FC, useEffect, useMemo, useRef, useState } from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import styled, { css, useTheme } from 'styled-components'
import { color, spacing } from '../../../../components/GlobalStyle'
import { NButton } from '../../../../components/NButton/NButton'
import { NDivider } from '../../../../components/NDivider'
import { NRow } from '../../../../components/NGrid/NGrid'
import { NModal } from '../../../../components/NModal/NModal'
import { NSpinner } from '../../../../components/NSpinner/NSpinner'
import { NToast } from '../../../../components/NToast'
import { NTypography, typography } from '../../../../components/NTypography'
import { APIServices } from '../../../../services/api'
import { FlowsQueryKey, getScreenFlowSSEUrl } from '../../../../services/api/endpoints/flows'
import { ScreenElement } from '../../../FlowBuilder/FlowBuilderItem/ScreenBuilder/utils/models'
import { ROOT_ELEMENT } from '../../../LayoutBuilder/utils/builder'
import { ZoneProps } from '../../../LayoutBuilder/utils/models'
import { FlowViewerProvider } from '../../contexts/FlowViewerContext'
import { useLayoutViewer } from '../../contexts/LayoutViewerContext'
import { ComponentContainerClassName } from '../../model'
import { ScreenFlowLayoutRoot } from './ScreenFlowLayoutRoot'

const Wrapper = styled.div<{ isLoading?: boolean }>`
  display: flex;
  flex-direction: column;
  background-color: ${color('white')};
  border-radius: 4px;

  ${props => {
    const { isLoading } = props
    if (isLoading) {
      return css`
        height: 20vh;
        justify-content: center;
        align-items: center;
      `
    }
  }}
`

const Header = styled.div`
  padding: 0 ${spacing('md')};
  display: flex;
  height: 48px;
  align-items: center;
  border-top-left-radius: 4px;
  border-top-right-radius: 4px;
  box-sizing: border-box;
  background-color: ${color('white')};
`
const Title = styled.p`
  ${typography('h500')};
`

export const Flow: FC<ZoneProps> = ({ elementId }) => {
  const theme = useTheme()
  const queryClient = useQueryClient()
  const { elements } = useLayoutViewer()
  const eventSourceRef = useRef<EventSource>()
  const [isEventSourceError, setIsEventSourceError] = useState(false)
  const formMethods = useForm()
  const [ctxId, setCtxId] = useState<string | undefined>(undefined)
  const [openSSE, setOpenSSE] = useState(false)
  const [isFlowEnd, setIsFlowEnd] = useState(false)

  const element = useMemo(() => {
    return elements[elementId]
  }, [elements, elementId])

  // mutation to start screen flow
  const { mutate: startFlow, isLoading: isStartingScreenFlow } = useMutation(APIServices.Flows.startScreenFlow, {
    onSuccess: data => {
      const { ctxId } = data.data
      setCtxId(ctxId)
      // after started fetch screen
      refetchSreenLayout()
    },
    onError: error => {
      NToast.error({
        title: 'Start flow unsuccessful',
        subtitle: error.response?.data.message,
      })
    },
  })

  // get current screen flow
  const { data: screenLayout, isFetching: isLoadingScreenLayout, refetch: refetchSreenLayout } = useQuery(
    [FlowsQueryKey.getScreenFlowLayout, { ctxId: ctxId! }],
    APIServices.Flows.getScreenFlowLayout,
    {
      enabled: false, // for manual control
      onSuccess: () => {
        //close SSE when finished getting a new screen
        if (openSSE) {
          eventSourceRef.current?.close()
          eventSourceRef.current = undefined
          setOpenSSE(false)
        }
      },
      onError: error => {
        // status code that indicates flow finished
        if (error.response?.data.statusCode === 410) {
          setIsFlowEnd(true)
        } else {
          NToast.error({
            title: 'Get screen flow unsuccessful',
            subtitle: error.response?.data.message,
          })
        }
      },
    },
  )

  // submit current flow screen data
  const { mutate: submitFormData, isLoading: isSubmitting } = useMutation(APIServices.Flows.submitScreenFlowData, {
    onSuccess: () => {
      // use remove query to prevent refetch, only rely on SSE to reload
      queryClient.removeQueries([FlowsQueryKey.getScreenFlowLayout, { ctxId }])
      // open SSE when finish submitting
      setOpenSSE(true)
    },
    onError: error => {
      NToast.error({
        title: 'Submit unsuccessful',
        subtitle: error.response?.data.message,
      })
    },
  })

  //
  useEffect(() => {
    if (ctxId && element.props.flowId) {
      return
    }
    setIsFlowEnd(false)
    startFlow({ flowId: element.props.flowId })
  }, [ctxId, element])

  useEffect(() => {
    // init event source when ctxId change
    if (ctxId && openSSE) {
      eventSourceRef.current = new EventSource(getScreenFlowSSEUrl(ctxId))
      eventSourceRef.current.onmessage = ev => {
        if (ev.data === 'RELOAD') {
          refetchSreenLayout()
        }
      }
      eventSourceRef.current.onerror = () => {
        setIsEventSourceError(true)
      }
      return () => {
        eventSourceRef.current?.close()
        eventSourceRef.current = undefined
      }
    }
  }, [openSSE, ctxId, refetchSreenLayout])

  const handleSubmit = () => {
    formMethods.handleSubmit(
      data => {
        ctxId && submitFormData({ ctxId, body: data })
        console.log('Form data', data)
      },
      () => {
        NToast.error({ title: 'Submit form unsuccesful' })
      },
    )()
  }

  const handleRetryFlow = () => {
    setIsEventSourceError(false)
    setIsFlowEnd(false)
    startFlow({ flowId: element.props.flowId })
  }

  // handle flow end
  if (isFlowEnd) {
    return (
      <Wrapper className={ComponentContainerClassName} isLoading>
        <NTypography.Headline>Flow finished.</NTypography.Headline>
        <NDivider size="sm" />
        <NButton onClick={handleRetryFlow} type="primary">
          Restart flow
        </NButton>
      </Wrapper>
    )
  }

  // when error display retry button. TODO: check retry 3 times or after a period of time
  if (isEventSourceError) {
    return (
      <Wrapper className={ComponentContainerClassName} isLoading>
        <NTypography.Headline>Something went wrong.</NTypography.Headline>
        <NDivider size="sm" />
        <NButton onClick={handleRetryFlow} type="primary">
          Retry flow
        </NButton>
      </Wrapper>
    )
  }

  // connecting or reconnecting to SSE server
  if (eventSourceRef.current?.readyState === EventSource.CONNECTING) {
    return (
      <Wrapper className={ComponentContainerClassName} isLoading>
        <NSpinner size={24} strokeWidth={4} color={color('Primary700')({ theme })} />
        <NDivider size="sm" />
        <NTypography.Headline>Processing...</NTypography.Headline>
      </Wrapper>
    )
  }

  if (isStartingScreenFlow || isLoadingScreenLayout || isSubmitting || !screenLayout || !screenLayout.data) {
    return (
      <Wrapper className={ComponentContainerClassName} isLoading>
        <NSpinner size={24} strokeWidth={4} color={color('Primary700')({ theme })} />
      </Wrapper>
    )
  }

  // if (!screenLayout || screenLayout.data === null) {
  //   return (
  //     <Wrapper className={ComponentContainerClassName} isLoading>
  //       <NTypography.Headline>Missing screen layout</NTypography.Headline>
  //     </Wrapper>
  //   )
  // }

  const script = screenLayout.data.script as Record<string, ScreenElement>
  const content = screenLayout.data.content ? (screenLayout.data.content as Record<string, any>) : undefined
  const { displayName } = screenLayout.data

  const rootElement = script[ROOT_ELEMENT]

  return (
    <Wrapper className={ComponentContainerClassName}>
      {rootElement.props.showHeader && (
        <>
          <Header>
            <Title>{displayName || '{REQUIRED DISPLAY NAME}'}</Title>
          </Header>
          <NDivider size={1} lineSize={1} lineColor={color('Neutral200')({ theme })} />
        </>
      )}

      {!isEventSourceError && !isLoadingScreenLayout && screenLayout && screenLayout.data && (
        <FlowViewerProvider elements={script} content={content}>
          <FormProvider {...formMethods}>
            <ScreenFlowLayoutRoot />
          </FormProvider>
        </FlowViewerProvider>
      )}

      {rootElement.props.showFooter && (
        <NModal.Footer>
          <div />
          <NRow>
            <NButton size="small" type="primary" onClick={handleSubmit}>
              Finish
            </NButton>
          </NRow>
        </NModal.Footer>
      )}
    </Wrapper>
  )
}
