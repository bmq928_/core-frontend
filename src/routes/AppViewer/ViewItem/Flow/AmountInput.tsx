import { nanoid } from 'nanoid'
import numberInUSWords from 'num-words'
import * as React from 'react'
import { Controller, useFormContext } from 'react-hook-form'
import styled from 'styled-components'
import { color } from '../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../components/Icons'
import { NButton } from '../../../../components/NButton/NButton'
import { NCurrencyInput } from '../../../../components/NCurrencyInput'
import { NDivider } from '../../../../components/NDivider'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { integerRegex } from '../../../../utils/regex'
import { numberFormat } from '../../../../utils/utils'
import { useFlowViewer } from '../../contexts/FlowViewerContext'
import { RendererPropsType } from './model'
import { NOTES, Notes, NOTES_LABEL, NOTES_VALUE, numberInVNWords, suggestBill1Amount } from './utils'

const inText = {
  USD: numberInUSWords,
  VND: numberInVNWords,
}

const RowWrapper = styled('div')`
  overflow: auto;
`

const AmountRowWrapper = styled('div')`
  display: flex;
  & > :first-child {
    flex: 2;
  }
`

const Row = styled('div')`
  display: flex;
  align-items: flex-end;
  & > :not(:last-child) {
    margin-right: 8px;
  }
  & > :nth-child(2) {
    flex: 1;
    width: 100%;
  }
  & > :last-child {
    padding: 0 8px;
  }
  &:not(:last-child) {
    margin-bottom: 8px;
  }
`

const Error = styled('div')`
  color: ${color('danger')};
`

function getTotal(fields: Array<{ id: string; note?: Notes; value: string }>, notesValue: Record<string, number>) {
  return fields.reduce((sum, i) => {
    const v = Number(i.value)
    if (!v || !i.note) {
      return sum
    }
    return sum + notesValue[i.note] * v
  }, 0)
}

export const AmountInput = React.memo(function Node({ elementId, actualName, label, defaultValue }: RendererPropsType) {
  const { context, elements } = useFlowViewer()
  const element = elements[elementId]

  const isWithdrawal = element.props.isWithdrawal as boolean
  const isRequired = element.props.isRequired as boolean
  const currencyCode = element.props.isUSD ? 'USD' : 'VND'

  const { setValue, watch, setError, clearErrors, formState, control, register } = useFormContext()

  const [fields, setFields] = React.useState<Array<{ id: string; note?: Notes; value: string }>>([
    { id: nanoid(), note: undefined, value: '0' },
  ])

  const amount = watch('amount')

  const { balance, ...notesCount } = React.useMemo(() => {
    const result = {
      balance: Number(context?.getSpace?.balances) || 0,
    } as Record<'balance' | Notes, number>
    for (let note of NOTES) {
      const count = Number((context?.getTill || {})[note]) || 0
      if (!Number.isNaN(count) && count > 0) {
        result[note] = count
      }
    }
    return result
  }, [context])

  // Init notes
  React.useEffect(() => {
    for (let note of Object.keys(notesCount)) {
      setValue(`updated${note}`, notesCount[note as Notes])
    }
  }, [notesCount, setValue])

  const total = React.useMemo(() => getTotal(fields, NOTES_VALUE[currencyCode]), [fields, currencyCode])

  React.useEffect(() => {
    if (Number(amount) !== total && Number(amount) > 0) {
      setError('money', { message: 'Invalid count!' })
      return
    }
    clearErrors('money')
  }, [amount, clearErrors, setError, total])

  // Update balance
  React.useEffect(() => {
    if (isWithdrawal) {
      setValue(`updatedBalance`, balance - total)
    } else {
      setValue(`updatedBalance`, balance + total)
    }
  }, [balance, isWithdrawal, setValue, total])

  // Update notes
  React.useEffect(() => {
    for (let field of fields) {
      const note = field.note as Notes
      const value = Number(field.value)
      if (!field.value || Number.isNaN(value) || !field.note) {
        return
      }
      setValue(note, field.value)
      if (isWithdrawal) {
        setValue(`updated${note}`, notesCount[note] - value)
      } else {
        setValue(`updated${note}`, notesCount[note] + value)
      }
    }
  }, [fields, isWithdrawal, notesCount, setValue])

  const handleSuggestBill = () => {
    try {
      const suggestedBill = suggestBill1Amount(Number(amount), currencyCode, notesCount)
      let suggestedFields = [] as { id: string; note: Notes; value: string }[]
      for (let [note, value] of Object.entries(suggestedBill)) {
        if (value > 0) {
          setValue(note, value)
          suggestedFields.push({ id: nanoid(), note: note as Notes, value: `${value}` })
        }
      }
      setFields(suggestedFields)
    } catch (e) {}
  }

  return (
    <React.Fragment>
      <AmountRowWrapper>
        <Controller
          control={control}
          name={actualName}
          defaultValue={defaultValue || ''}
          rules={{
            required:
              (isRequired && {
                value: true,
                message: 'Required!',
              }) ||
              undefined,
            max:
              (isWithdrawal && {
                value: balance,
                message: 'Not enough money',
              }) ||
              undefined,
            pattern: {
              value: integerRegex,
              message: 'Invalid value',
            },
          }}
          render={({ field, fieldState }) => (
            <NCurrencyInput
              label={label}
              value={field.value}
              min={0}
              required={isRequired}
              formatter={value => `${numberFormat((value || '').toString())}`}
              onChange={v => field.onChange(v)}
              onBlur={() => {
                field.onBlur()
                if (!fieldState.error && isWithdrawal) {
                  handleSuggestBill()
                  return
                }
                if (fieldState.error && isWithdrawal) {
                  setFields([{ id: nanoid(), note: undefined, value: '' }])
                }
              }}
              error={fieldState.error?.message}
            />
          )}
        />
        <NDivider vertical />

        <div style={{ flex: 1, display: 'flex', flexDirection: 'column' }}>
          <div className="t">Currency</div>
          <div className="v" style={{ display: 'flex', flex: 1, alignItems: 'center' }}>
            {currencyCode}
          </div>
        </div>
      </AmountRowWrapper>
      <RowWrapper>
        {fields.map((field, fieldIndex) => {
          return (
            <Row key={field.id}>
              <NSingleSelect
                label={fieldIndex === 0 ? 'Money' : undefined}
                options={Object.keys(notesCount).map(i => ({
                  value: i,
                  label: NOTES_LABEL[currencyCode][i as Notes],
                }))}
                value={field.note || ''}
                onValueChange={newNote => {
                  setFields(prev =>
                    prev.map(p => {
                      if (p.id === field.id) {
                        return {
                          ...p,
                          note: newNote as Notes,
                          value: '0',
                        }
                      }
                      return p
                    }),
                  )
                }}
              />
              <NTextInput
                label={fieldIndex === 0 ? 'Note' : undefined}
                disabled={!field.note}
                value={field.value}
                name={field.note || 'shouldnotusethiskey'}
                ref={(field.note && register(field.note, { valueAsNumber: true }).ref) || undefined}
                onChange={e => {
                  setFields(prev =>
                    prev.map(p => {
                      if (p.id === field.id) {
                        return {
                          ...p,
                          value: e.target.value,
                        }
                      }
                      return p
                    }),
                  )
                }}
              />
              {fields.length > 1 && (
                <NButton
                  htmlType="button"
                  type="ghost"
                  onClick={() => {
                    setFields(prev => prev.filter(i => i.id !== field.id))
                  }}>
                  <GlobalIcons.Trash />
                </NButton>
              )}
            </Row>
          )
        })}
        <NButton
          type="primary"
          size="small"
          onClick={() => setFields(prev => [...prev, { id: nanoid(), note: undefined, value: '' }])}>
          +
        </NButton>
      </RowWrapper>
      {formState.errors['money'] && <Error>{formState.errors['money']?.message}</Error>}
      <div style={{ display: 'flex' }}>
        <div style={{ flex: 1 }}>
          <div className="t">Total</div>
          <div className="v">
            {numberFormat(total)} {currencyCode}
          </div>
        </div>
        <div style={{ flex: 2 }}>
          <div className="t">In Text</div>
          <div className="v">
            {Number.isNaN(Number(amount)) ? '' : inText[currencyCode](Number(amount))}{' '}
            {(currencyCode === 'USD' && `dollar${(Number(amount) > 1 && 's') || ''}`) || 'Dong'}
          </div>
        </div>
      </div>
    </React.Fragment>
  )
})

AmountInput.displayName = 'AmountInput'
