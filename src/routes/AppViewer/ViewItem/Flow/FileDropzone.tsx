import React, { FC, useState } from 'react'
import { Controller, useFormContext } from 'react-hook-form'
import { useMutation } from 'react-query'
import styled from 'styled-components'
import { NFileDropzone } from '../../../../components/NFileDropzone/NFileDropZone'
import { NToast } from '../../../../components/NToast'
import { APIServices } from '../../../../services/api'
import { MediaResponse } from '../../../../services/api/models'
import { useFlowViewer } from '../../contexts/FlowViewerContext'
import { RendererPropsType } from './model'

const Wrapper = styled.div`
  display: inline-flex;
  flex-direction: column;
  width: 100%;
`

export const FileDropzone: FC<RendererPropsType> = ({ elementId, actualName, label }) => {
  const { elements } = useFlowViewer()
  const formMethods = useFormContext()
  const { control } = formMethods

  const element = elements[elementId]
  const { isRequired, actionMetadata } = element.props

  const [file, setFile] = useState<MediaResponse>()

  const { mutate: uploadFile, isLoading } = useMutation(APIServices.UploadFile.uploadFile, {
    onError: error => {
      NToast.error({ title: 'Upload file error', subtitle: error.response?.data.message })
    },
  })

  return (
    <Wrapper>
      <Controller
        control={control}
        name={actualName}
        rules={{ required: { value: !!isRequired, message: 'Required' } }}
        render={({ field: { onChange }, fieldState }) => {
          return (
            <NFileDropzone
              label={label}
              error={fieldState.error?.message}
              file={file}
              isUploading={isLoading}
              onDropAccepted={(newFiles: File[]) => {
                const formData = new FormData()
                formData.append('file', newFiles[0])
                uploadFile(
                  {
                    name: actionMetadata,
                    formData,
                  },
                  {
                    onSuccess: data => {
                      onChange(data.data.id)
                      setFile(data.data)
                    },
                  },
                )
              }}
              onRemove={() => {
                setFile(undefined)
              }}
            />
          )
        }}
      />
    </Wrapper>
  )
}
