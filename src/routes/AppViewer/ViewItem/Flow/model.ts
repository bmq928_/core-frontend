import { get } from 'lodash'
import { SelectOption } from '../../../../components/NSelect/model'
import { ZoneProps } from '../../../LayoutBuilder/utils/models'

export type RendererPropsType = ZoneProps & {
  label?: string
  defaultValue?: any
  transformOptions?: SelectOption[]
  actualName: string
}

export const getInputFieldContent = (parentName?: string, index?: number, content?: Record<string, any>) => {
  if (!content || !parentName || !index) {
    return {} as Record<string, any>
  }

  const parentArray = get(content, parentName)

  if (!Array.isArray(parentArray)) {
    return {} as Record<string, any>
  }

  return get(content, parentName)[index] as Record<string, any>
}
