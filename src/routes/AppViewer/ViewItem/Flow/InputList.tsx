import React, { FC, useEffect } from 'react'
import { useFieldArray, useFormContext } from 'react-hook-form'
import styled from 'styled-components'
import { spacing } from '../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../components/Icons'
import { NButton } from '../../../../components/NButton/NButton'
import { NDivider } from '../../../../components/NDivider'
import { NRow } from '../../../../components/NGrid/NGrid'
import { SelectOption } from '../../../../components/NSelect/model'
import { NTypography } from '../../../../components/NTypography'
import { ScreenElementComponent } from '../../../FlowBuilder/FlowBuilderItem/ScreenBuilder/utils/models'
import { renderVariableValue } from '../../../FlowBuilder/utils/functions'
import { useFlowViewer } from '../../contexts/FlowViewerContext'
import { ViewUnderConstruction } from '../../contexts/useRenderView'
import { getScreenViewItemFromElement, RendererType } from '../ViewItems'
import { getInputFieldContent, RendererPropsType } from './model'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`

const AddBtn = styled(NButton)`
  width: 56px;
  height: 24px;
`
const InputRowContainer = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  min-height: 40px;
  & > *:not(:last-child) {
    margin-right: ${spacing('md')};
  }
  margin-bottom: ${spacing('sm')};
`
const FieldContainer = styled.div<{ flexSize?: number }>`
  flex: ${props => (props.flexSize ? props.flexSize : 1)};
`

const TrashBtn = styled(NButton)`
  width: 40px;
  height: 40px;
`

export const InputList: FC<RendererPropsType> = ({ elementId, actualName, label, defaultValue }) => {
  const { elements } = useFlowViewer()
  const formMethods = useFormContext()

  const element = elements[elementId]

  useEffect(() => {
    formMethods.setValue(actualName, defaultValue)
  }, [defaultValue])

  const { fields, append, remove } = useFieldArray({
    name: actualName,
    control: formMethods.control,
  })

  const handleAddRow = () => {
    append({})
  }

  const handleRemoveRow = (rowIndex: number) => {
    remove(rowIndex)
  }

  return (
    <Wrapper>
      <NTypography.InputLabel>{label}</NTypography.InputLabel>
      <NDivider size="sm" />
      <NRow>
        {element.children.map(childId => {
          const element = elements[childId]
          return (
            <FieldContainer key={`InputList-label-${childId}`}>
              <NTypography.InputLabel>{element.label}</NTypography.InputLabel>
            </FieldContainer>
          )
        })}
      </NRow>
      <NDivider size="sm" />
      {fields.map((field, rowIndex) => {
        return (
          <InputRow
            key={field.id}
            elementId={elementId}
            index={rowIndex}
            disableRemoveBtn={fields.length <= 1}
            onRemove={handleRemoveRow}
          />
        )
      })}
      <AddBtn type="primary" icon={<GlobalIcons.Plus />} onClick={handleAddRow} />
    </Wrapper>
  )
}

type InputRowProps = {
  elementId: string
  index: number
  disableRemoveBtn: boolean
  onRemove: (index: number) => void
}
const InputRow: FC<InputRowProps> = ({ elementId, index, disableRemoveBtn, onRemove }) => {
  const { elements, content } = useFlowViewer()
  const parentElement = elements[elementId]
  const parentName = parentElement.name

  return (
    <InputRowContainer>
      {parentElement.children &&
        parentElement.children.map((id, idx) => {
          const { component, props, name } = elements[id]
          const { label, options, defaultValue } = props

          const actualLabel =
            component === ScreenElementComponent.Input && props.inputType === 'boolean' ? label : undefined
          const actualName = `${parentName}[${index}].${name}`

          const Component: RendererType | undefined = getScreenViewItemFromElement(elements[id])
          if (!Component) {
            return <ViewUnderConstruction key={id} />
          }

          const rowContent = getInputFieldContent(parentName, idx, content)
          switch (component) {
            case ScreenElementComponent.Input:
              const renderDefaultValue = renderVariableValue(defaultValue, rowContent)
              return (
                <FieldContainer key={`ListInputField_${id}_${index}`}>
                  <Component
                    elementId={id}
                    parentId={elementId}
                    index={idx}
                    label={actualLabel}
                    actualName={actualName}
                    defaultValue={renderDefaultValue}
                  />
                </FieldContainer>
              )
            case ScreenElementComponent.Selection:
              const transformOptions: SelectOption[] = (options || []).map(op => {
                const label = renderVariableValue(op, rowContent)
                const value = (content && content[op.value]) || op.value
                return { label, value }
              })
              return (
                <FieldContainer key={`ListInputField_${id}_${index}`}>
                  <Component
                    elementId={id}
                    parentId={elementId}
                    index={idx}
                    label={actualLabel}
                    actualName={actualName}
                    transformOptions={transformOptions}
                  />
                </FieldContainer>
              )
            default:
              return (
                <FieldContainer key={`ListInputField_${id}_${index}`}>
                  <Component key={id} elementId={id} parentId={elementId} index={idx} actualName={actualName} />
                </FieldContainer>
              )
          }
        })}
      <TrashBtn
        type="ghost"
        icon={<GlobalIcons.Trash />}
        disabled={disableRemoveBtn}
        onClick={() => {
          onRemove(index)
        }}
      />
    </InputRowContainer>
  )
}
