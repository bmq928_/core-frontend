export const NOTES = ['b500', 'b200', 'b100', 'b50', 'b20', 'b10', 'b5'] as const
export type Notes = typeof NOTES[number]

export const NOTES_VALUE: Record<string, Record<Notes, number>> = {
  VND: {
    b500: 500000,
    b200: 200000,
    b100: 100000,
    b50: 50000,
    b20: 20000,
    b10: 10000,
    b5: 5000,
  },
  USD: {
    b500: 100,
    b200: 50,
    b100: 20,
    b50: 10,
    b20: 5,
    b10: 2,
    b5: 1,
  },
}

export const NOTES_LABEL: Record<string, Record<Notes, string>> = {
  VND: {
    b500: '500.000',
    b200: '200.000',
    b100: '100.000',
    b50: '50.000',
    b20: '20.000',
    b10: '10.000',
    b5: '5.000',
  },
  USD: {
    b500: '$100',
    b200: '$50',
    b100: '$20',
    b50: '$10',
    b20: '$5',
    b10: '$2',
    b5: '$1',
  },
}

function getCountedNote(amount: number, notesCount: Record<Notes, number>, notesValue: Record<Notes, number>) {
  const notes = Object.keys(notesCount) as Notes[]
  const totalNotesValue = notes.reduce((a, i) => a + notesValue[i] * notesCount[i], 0)
  if (totalNotesValue < amount) {
    throw Error('getCountedNote: Not Enough Money')
  }
  let restAmount = amount
  let result = {} as Record<Notes, number>
  for (let note of notes) {
    const noteCount = Math.min(notesCount[note], Math.floor(restAmount / notesValue[note]))
    restAmount = restAmount - noteCount * notesValue[note]
    result[note] = noteCount
  }
  if (restAmount > 0) {
    throw Error('getCountedNote: Not Enough Count')
  }
  return result
}

export function suggestBill1Amount(amount: number, currencyCode: string = 'VND', notesCount: Record<Notes, number>) {
  const notes = Object.keys(notesCount) as Notes[]
  const availableNotes = notes.filter(note => notesCount[note] > 0)

  if (availableNotes.length < 1) {
    throw Error('Not enough money')
  }

  let limits = {} as Record<Notes, number>
  let halfLimits = {} as Record<Notes, number>
  for (let [index, note] of availableNotes.entries()) {
    limits[note] = notesCount[note]
    halfLimits[note] = index === 0 || index === 1 ? Math.floor(notesCount[note] / 2) : notesCount[note]
  }

  const notesValue = NOTES_VALUE[currencyCode]

  try {
    return getCountedNote(amount, halfLimits, notesValue)
  } catch (e) {
    return getCountedNote(amount, limits, notesValue)
  }
}

export function numberInVNWords(raw: string | number = '') {
  const postfix = ['Ty', 'Trieu', 'Nghin', 'Dong']
  const numbers = ['Khong', 'Mot', 'Hai', 'Ba', 'Bon', 'Nam', 'Sau', 'Bay', 'Tam', 'Chin']
  const padded = raw.toString().padStart(12, '0')
  const groupped = padded.match(/.{1,3}/g)
  function getBlock(input: string) {
    const value = Number(input)
    if (Number.isNaN(value) || value < 1) {
      return
    }
    const hundress = Math.floor(value / 100)
    const tens = Math.floor((value % 100) / 10)
    const ones = value % 10
    let result = []
    if (hundress > 0) {
      result.push(`${numbers[hundress]} tram`)
    }
    if (tens > 1) {
      result.push(`${numbers[tens]} muoi`)
    }
    if (hundress > 0 && tens === 1) {
      result.push(`Muoi ${numbers[tens].toLocaleLowerCase()}`)
    }
    if (hundress < 1 && tens === 1) {
      result.push(`Muoi`)
    }
    if (ones > 0) {
      result.push(`${numbers[ones]}`)
    }
    return result.join(' ')
  }
  if (groupped) {
    return groupped?.reduce((a, i, idx) => {
      const inText = getBlock(i)
      if (inText) {
        return `${a} ${inText} ${postfix[idx]}`
      }
      return a
    }, '')
  }
  return raw
}
