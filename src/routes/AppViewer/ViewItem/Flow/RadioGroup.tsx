import React, { FC } from 'react'
import { Controller, useFormContext } from 'react-hook-form'
import styled from 'styled-components'
import { color, spacing } from '../../../../components/GlobalStyle'
import { NRow } from '../../../../components/NGrid/NGrid'
import { NRadio } from '../../../../components/NRadio/NRadio'
import { SelectOption } from '../../../../components/NSelect/model'
import { NTypography, RequiredIndicator } from '../../../../components/NTypography'
import { useFlowViewer } from '../../contexts/FlowViewerContext'
import { RendererPropsType } from './model'

const RadioContainer = styled.div`
  display: flex;
  flex-direction: column;

  .radio {
    margin-top: ${spacing('md')};
  }
`

const ErrorMsg = styled(NTypography.InputCaption)`
  color: ${color('Red700')};
`

export const RadioGroup: FC<RendererPropsType> = ({ elementId, actualName, label, transformOptions }) => {
  const { elements } = useFlowViewer()
  const formMethods = useFormContext()

  const element = elements[elementId]
  const { label: _l, defaultValue: _, defaultIndexes, options: _o, isRequired } = element.props

  const defaultValue =
    transformOptions && defaultIndexes && defaultIndexes[0] ? transformOptions[defaultIndexes[0]].value : undefined

  return (
    <Controller
      control={formMethods.control}
      name={actualName}
      defaultValue={defaultValue}
      rules={{ required: { value: !!isRequired, message: 'Required' } }}
      render={({ field: { value, onChange }, fieldState }) => {
        return (
          <RadioContainer>
            <NRow justify="space-between">
              <NTypography.InputLabel>
                {isRequired && <RequiredIndicator>*</RequiredIndicator>}
                {label}
              </NTypography.InputLabel>
            </NRow>
            {fieldState.error?.message && <ErrorMsg>{fieldState.error?.message}</ErrorMsg>}
            {(transformOptions || []).map((op: SelectOption) => {
              return (
                <NRadio
                  className="radio"
                  key={`${elementId}_radio_group_${op.value}`}
                  title={op.value}
                  name={actualName}
                  value={op.value}
                  checked={value === op.value}
                  onChange={onChange}
                />
              )
            })}
          </RadioContainer>
        )
      }}
    />
  )
}
