import React, { FC } from 'react'
import { Controller, useFormContext } from 'react-hook-form'
import { NCheckbox } from '../../../../components/NCheckbox/NCheckbox'
import { InputProps } from '../../../../services/api/models'
import { useFlowViewer } from '../../contexts/FlowViewerContext'
import { RendererPropsType } from './model'

export const Checkbox: FC<RendererPropsType> = ({ elementId, actualName, label, defaultValue }) => {
  const { elements } = useFlowViewer()
  const formMethods = useFormContext()

  const element = elements[elementId]
  const { label: _l, defaultValue: _dV, isRequired: _, ...restProps } = element.props as InputProps

  return (
    <Controller
      control={formMethods.control}
      name={actualName}
      defaultValue={typeof defaultValue === 'boolean' ? defaultValue : false}
      render={({ field: { value, onChange } }) => {
        return (
          <NCheckbox
            checked={value}
            value={value}
            onChange={event => {
              onChange(event.target.checked)
            }}
            title={label}
            {...restProps}
          />
        )
      }}
    />
  )
}
