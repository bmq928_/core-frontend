import { get } from 'lodash'
import { MarkdownViewer } from '../../../../components/MarkdownViewer'
import { numberFormat } from '../../../../utils/utils'
import { ZoneProps } from '../../../LayoutBuilder/utils/models'
import { useFlowViewer } from '../../contexts/FlowViewerContext'

const renderStringWithVariable = (rawString = '', content?: any): string => {
  const regex = new RegExp('{.*?}', 'gm')
  const allVars = Array.from(rawString.matchAll(regex), ([v]) => v)
  if (!content || Object.keys(content).length <= 0) {
    return rawString
  }
  return allVars.reduce((a, i) => {
    const key = i.replace('{', '').replace('}', '')
    if (!key) {
      return a
    }
    const value = get(content, key, '') as string
    return a.replace(i, numberFormat(value).toString())
  }, rawString)
}

export function DisplayText({ elementId }: ZoneProps) {
  const { elements, content, context } = useFlowViewer()
  const element = elements[elementId]

  return <MarkdownViewer>{renderStringWithVariable(element.props.content?.value, context || content)}</MarkdownViewer>
}
