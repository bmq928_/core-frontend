import React, { FC } from 'react'
import { useFormContext } from 'react-hook-form'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { useValidateString } from '../../../../hooks/useValidateString'
import { InputProps } from '../../../../services/api/models'
import { integerRegex } from '../../../../utils/regex'
import { useFlowViewer } from '../../contexts/FlowViewerContext'
import { RendererPropsType } from './model'

const resolveHtmlType = (inputType: InputProps['inputType'] | undefined) => {
  switch (inputType) {
    case 'string':
      return 'text'
    case 'integer':
    case 'float':
      return 'number'
  }
}

export const Input: FC<RendererPropsType> = ({ elementId, label, defaultValue, actualName }) => {
  const { elements } = useFlowViewer()

  const formMethods = useFormContext()

  const element = elements[elementId]
  const { defaultValue: _dV, label: _l, inputType: type, isRequired, isHidden, ...restProps } = element.props

  const { validateFunction } = useValidateString(isRequired)

  if (isHidden) {
    return null
  }

  return (
    <NTextInput
      {...formMethods?.register(actualName, {
        validate: validateFunction,
        pattern:
          type === 'integer'
            ? {
                value: integerRegex,
                message: 'Invalid integer',
              }
            : undefined,
      })}
      error={formMethods.formState.errors[actualName]?.message}
      required={isRequired}
      defaultValue={defaultValue}
      type={resolveHtmlType(type)}
      label={label}
      {...restProps}
    />
  )
}
