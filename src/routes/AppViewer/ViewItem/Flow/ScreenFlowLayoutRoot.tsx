import React, { FC } from 'react'
import styled from 'styled-components'
import { spacing } from '../../../../components/GlobalStyle'
import { SelectOption } from '../../../../components/NSelect/model'
import { ROOT_ELEMENT } from '../../../FlowBuilder/FlowBuilderItem/ScreenBuilder/utils/builder'
import { ScreenElementComponent } from '../../../FlowBuilder/FlowBuilderItem/ScreenBuilder/utils/models'
import { renderVariableValue } from '../../../FlowBuilder/utils/functions'
import { useFlowViewer } from '../../contexts/FlowViewerContext'
import { ViewUnderConstruction } from '../../contexts/useRenderView'
import { getScreenViewItemFromElement, RendererType } from '../ViewItems'

const ScreenBody = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  padding: ${spacing('md')};

  & > div {
    margin-top: ${spacing('xs')};
    margin-bottom: ${spacing('xs')};
  }
`
export const ScreenFlowLayoutRoot: FC = () => {
  const { elements, content } = useFlowViewer()
  const element = elements[ROOT_ELEMENT]

  return (
    <ScreenBody>
      {element.children &&
        element.children.map((id, idx) => {
          const { component, props, name } = elements[id]
          const { label, options, defaultValue } = props

          const Component: RendererType | undefined = getScreenViewItemFromElement(elements[id])

          if (!Component) {
            return <ViewUnderConstruction key={id} />
          }

          switch (component) {
            case ScreenElementComponent.Input:
              const renderDefaultValue = renderVariableValue(defaultValue, content)
              return (
                <Component
                  key={id}
                  elementId={id}
                  parentId={ROOT_ELEMENT}
                  index={idx}
                  label={label}
                  actualName={name || id}
                  defaultValue={renderDefaultValue}
                />
              )
            case ScreenElementComponent.Selection:
              const transformOptions: SelectOption[] = (options || []).map(op => {
                const label = renderVariableValue(op, content)
                const value = (content && content[op.value]) || op.value
                return { label, value }
              })
              return (
                <Component
                  key={id}
                  elementId={id}
                  parentId={ROOT_ELEMENT}
                  index={idx}
                  label={label}
                  actualName={name || id}
                  transformOptions={transformOptions}
                />
              )
            default:
              return (
                <Component
                  key={id}
                  elementId={id}
                  parentId={ROOT_ELEMENT}
                  index={idx}
                  actualName={name || id}
                  label={label}
                />
              )
          }
        })}
    </ScreenBody>
  )
}
