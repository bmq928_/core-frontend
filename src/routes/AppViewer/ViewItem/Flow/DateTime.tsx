import React, { FC } from 'react'
import { Controller, useFormContext } from 'react-hook-form'
import { NDatePicker } from '../../../../components/DateTime/NDatePicker'
import { NDateTimePicker } from '../../../../components/DateTime/NDateTimePicker'
import { NTimePicker } from '../../../../components/DateTime/NTimePicker'
import { getDateFromIsoString, getValueFromDate } from '../../../../utils/utils'
import { useFlowViewer } from '../../contexts/FlowViewerContext'
import { RendererPropsType } from './model'

export const DateTime: FC<RendererPropsType> = ({ elementId, actualName, label, defaultValue }) => {
  const { elements } = useFlowViewer()
  const formMethods = useFormContext()

  const element = elements[elementId]
  const { defaultValue: _dV, label: _l, inputType: type, isRequired, ...restProps } = element.props

  return (
    <Controller
      control={formMethods.control}
      name={actualName}
      defaultValue={defaultValue}
      rules={{ required: { value: !!isRequired, message: 'Required' } }}
      render={({ field: { value: val, onChange }, fieldState }) => {
        // translate ios string to date
        let valueDate = undefined

        try {
          valueDate = getDateFromIsoString(val, type)
        } catch (_err) {}

        // translate date to ios string
        const handleOnChange = (newDate: Date) => {
          onChange(getValueFromDate(newDate, type))
        }

        if (type === 'date') {
          return (
            <NDatePicker
              required={isRequired}
              value={valueDate}
              error={fieldState.error?.message}
              onChangeValue={handleOnChange}
              label={label}
              {...restProps}
            />
          )
        }

        if (type === 'time') {
          return (
            <NTimePicker
              required={isRequired}
              value={valueDate}
              error={fieldState.error?.message}
              onChangeValue={handleOnChange}
              label={label}
              {...restProps}
            />
          )
        }

        return (
          <NDateTimePicker
            required={isRequired}
            value={valueDate}
            error={fieldState.error?.message}
            onValueChange={handleOnChange}
            label={label}
            {...restProps}
          />
        )
      }}
    />
  )
}
