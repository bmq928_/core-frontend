import React, { FC } from 'react'
import { Controller, useFormContext } from 'react-hook-form'
import { NMultiSelect } from '../../../../components/NSelect/NMultiSelect'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { useFlowViewer } from '../../contexts/FlowViewerContext'
import { RendererPropsType } from './model'

export const PickList: FC<RendererPropsType> = ({ elementId, actualName, label, transformOptions }) => {
  const { elements } = useFlowViewer()
  const formMethods = useFormContext()

  const element = elements[elementId]
  const {
    defaultValue: _dV,
    options: _o,
    label: _l,
    selectionType: type,
    isRequired,
    defaultIndexes,
    ...restProps
  } = element.props

  // TODO: update default index when server fix swagger
  let defaultValues: string[] | undefined =
    transformOptions && defaultIndexes
      ? defaultIndexes.map(defIndex => {
          return transformOptions[defIndex].value
        })
      : undefined

  return (
    <Controller
      control={formMethods.control}
      name={actualName}
      defaultValue={defaultValues}
      rules={{
        validate: value => {
          return type === 'multi' ? (value.length > 0 ? true : 'Required') : true
        },
        required: { value: !!isRequired, message: 'Required' },
      }}
      render={({ field: { value, onChange }, fieldState }) => {
        if (type === 'multi') {
          return (
            <NMultiSelect
              fullWidth
              values={value || []}
              options={transformOptions || []}
              error={fieldState.error?.message}
              required={isRequired}
              onValuesChange={newVals => {
                onChange(newVals)
              }}
              label={label}
              {...restProps}
            />
          )
        }
        return (
          <NSingleSelect
            fullWidth
            value={value && value[0]}
            error={fieldState.error?.message}
            options={transformOptions || []}
            required={isRequired}
            onValueChange={newVal => {
              onChange([newVal])
            }}
            label={label}
            {...restProps}
          />
        )
      }}
    />
  )
}
