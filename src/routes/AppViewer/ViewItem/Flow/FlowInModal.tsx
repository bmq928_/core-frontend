import * as React from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { useMutation, useQuery } from 'react-query'
import styled from 'styled-components'
import { NButton } from '../../../../components/NButton/NButton'
import { NDivider } from '../../../../components/NDivider'
import { NRow } from '../../../../components/NGrid/NGrid'
import { NModal, NModalProps } from '../../../../components/NModal/NModal'
import { NPerfectScrollbar } from '../../../../components/NPerfectScrollbar'
import { NSpinner } from '../../../../components/NSpinner/NSpinner'
import { NToast } from '../../../../components/NToast'
import { NTypography } from '../../../../components/NTypography'
import { APIServices } from '../../../../services/api'
import { FlowsQueryKey, getScreenFlowSSEUrl } from '../../../../services/api/endpoints/flows'
import { CtxFlowResponse } from '../../../../services/api/models'
import { ScreenElement } from '../../../FlowBuilder/FlowBuilderItem/ScreenBuilder/utils/models'
import { FlowViewerProvider } from '../../contexts/FlowViewerContext'
import { getCtxId, removeCtxId, saveCtxId } from '../../utils'
import { ScreenFlowLayoutRoot } from './ScreenFlowLayoutRoot'

const Wrapper = styled('div')`
  height: 20vh;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`

const HackedForDemo = styled('div')`
  .g {
    display: grid;
    grid-template-rows: minmax(0, 1fr);
    grid-gap: 16px;
  }
  .h {
    font-size: 14px;
    text-transform: uppercase;
    color: #9fa9b3;
  }
  .g-3,
  .notes-row {
    grid-template-columns: repeat(3, minmax(0, 1fr));
  }
  .br {
    padding-bottom: 8px;
    margin-bottom: 8px;
    border-bottom: 1px solid #eceef0;
  }
  .g-2 {
    grid-template-columns: repeat(2, minmax(0, 1fr));
  }
  .t {
    font-weight: bold;
    font-size: 12px;
    color: #525c66;
    margin-bottom: 8px;
  }
  .v {
    font-size: 14px;
    color: #525c66;
    font-weight: thin;
  }
`

type FlowInModalProps = {
  children?: React.ReactNode
  layoutId: string
  componentId: string
  inputMap?: Record<string, string>
  actionId: string
} & NModalProps

export const FlowInModal = React.memo(function Node({
  layoutId,
  componentId,
  visible,
  setVisible,
  inputMap,
  actionId,
}: FlowInModalProps) {
  const eventSourceRef = React.useRef<EventSource>()
  const [openSSE, setOpenSSE] = React.useState(false)
  const [ctxId, setCtxId] = React.useState(getCtxId(actionId))
  const [isError, setIsError] = React.useState(false)
  const formMethods = useForm({ shouldUnregister: false, reValidateMode: 'onChange', mode: 'onChange' })

  const handleCloseModal = () => {
    if (setVisible) {
      setVisible(false)
    }
    eventSourceRef.current?.close()
    eventSourceRef.current = undefined
  }

  const { mutate: getTriggers, isLoading: isGettingCtxId } = useMutation(APIServices.Layout.triggers, {
    onSuccess(res) {
      const data = res.data as CtxFlowResponse
      if (!data.ctxId) {
        NToast.error({
          title: 'Invalid component',
          subtitle: 'Something went wrong!',
        })
        handleCloseModal()
        return
      }
      saveCtxId(actionId, data.ctxId)
      setCtxId(data.ctxId)
      refetchSreenLayout()
    },
    onError(error) {
      NToast.error({
        title: 'Get Context Id unsuccessful',
        subtitle: error.response?.data.message || error.message,
      })
      handleCloseModal()
    },
  })

  React.useEffect(() => {
    if (visible && layoutId && componentId) {
      if (!ctxId) {
        getTriggers({
          layoutId,
          componentId,
          ...inputMap,
        })
      }
    }
  }, [componentId, ctxId, getTriggers, inputMap, layoutId, visible])

  // get current screen flow
  const { data: screenLayout, isLoading: isLoadingScreenLayout, refetch: refetchSreenLayout } = useQuery(
    [FlowsQueryKey.getScreenFlowLayout, { ctxId: ctxId! }],
    APIServices.Flows.getScreenFlowLayout,
    {
      cacheTime: 0, // disable cache
      retry: false,
      enabled: false, // for manual control
      keepPreviousData: false,
      onSuccess: res => {
        // (No data || no script) === Always need to oepn SSE
        if (!res.data || !res.data.script) {
          setOpenSSE(true)
          return
        }
        //close SSE when got layout script
        if (res.data.script) {
          eventSourceRef.current?.close()
          eventSourceRef.current = undefined
          setOpenSSE(false)
          formMethods.clearErrors()
        }
      },
      onError: error => {
        // status code that indicates flow finished
        if (error.response?.data.statusCode === 410) {
          removeCtxId()
          if (visible) {
            NToast.success({
              title: 'Screen flow finished!',
            })
            handleCloseModal()
          }
        } else {
          if (visible) {
            NToast.error({
              title: 'Get screen flow unsuccessful',
              subtitle: error.response?.data.message,
            })
          }
        }
      },
    },
  )

  // submit current flow screen data
  const { mutate: submitFormData, isLoading: isSubmitting } = useMutation(APIServices.Flows.submitScreenFlowData, {
    onSuccess: () => {
      // open SSE when finish submitting
      setOpenSSE(true)
    },
    onError: error => {
      NToast.error({
        title: 'Submit unsuccessful',
        subtitle: error.response?.data.message,
      })
    },
  })

  React.useEffect(() => {
    if (ctxId && !openSSE) {
      refetchSreenLayout()
    }
  }, [])

  React.useEffect(() => {
    // init event source when ctxId change
    if (ctxId && openSSE) {
      console.log('open sse with ctxId', ctxId)
      eventSourceRef.current = new EventSource(getScreenFlowSSEUrl(ctxId))
      eventSourceRef.current.onmessage = ev => {
        if (ev.data === 'RELOAD') {
          console.log('reload')
          refetchSreenLayout()
        }
      }
      eventSourceRef.current.onerror = () => {
        setIsError(true)
      }
      return () => {
        console.log('close sse')
        eventSourceRef.current?.close()
        eventSourceRef.current = undefined
      }
    }
  }, [openSSE, ctxId, refetchSreenLayout])

  const handleRetry = () => {
    setIsError(false)
    refetchSreenLayout()
  }

  const isSSEConnecting = eventSourceRef.current?.readyState === EventSource.CONNECTING
  const isLoading =
    isLoadingScreenLayout || isGettingCtxId || !screenLayout || !screenLayout.data.script || isSSEConnecting

  const script = screenLayout?.data.script as Record<string, ScreenElement>
  const content = screenLayout?.data.content as Record<string, any>
  const context = screenLayout?.data.ctx as Record<string, any>
  const { displayName } = screenLayout?.data || {}
  // const root = script?.[ROOT_ELEMENT]

  return (
    <NModal visible={visible} setVisible={setVisible} closeOnOverlayClick={false} size="large">
      <NModal.Header title={isLoading ? 'Processing ...' : displayName} onClose={handleCloseModal} />
      <NModal.Body>
        {isLoading && !isError && (
          <Wrapper>
            <NSpinner size={24} strokeWidth={4} />
            <NDivider size="sm" />
            <NTypography.Headline>Loading ...</NTypography.Headline>
          </Wrapper>
        )}
        {isError && (
          <Wrapper>
            <NTypography.Headline>Something went wrong!</NTypography.Headline>
            <NDivider size="sm" />
            <NButton onClick={handleRetry} type="primary">
              Retry flow
            </NButton>
          </Wrapper>
        )}
        {!isLoading && !isError && screenLayout && (
          <NPerfectScrollbar style={{ maxHeight: '70vh' }}>
            <HackedForDemo>
              <FlowViewerProvider elements={script} content={content} context={context}>
                <FormProvider {...formMethods}>
                  <ScreenFlowLayoutRoot />
                </FormProvider>
              </FlowViewerProvider>
            </HackedForDemo>
          </NPerfectScrollbar>
        )}
      </NModal.Body>
      <NModal.Footer>
        <NButton onClick={handleCloseModal} type="default" size="small">
          Cancel
        </NButton>
        <NRow>
          {/* {root?.props?.showPrintBtn && (
            <NButton type="outline" size="small">
              Print
            </NButton>
          )} */}
          <NDivider size="md" vertical />
          <NButton
            type="primary"
            size="small"
            onClick={formMethods.handleSubmit(body => {
              console.log({ body })
              if (ctxId) {
                submitFormData({ ctxId, body })
              }
            })}
            disabled={isLoading || isError || !formMethods.formState.isValid}
            loading={isSubmitting}>
            Next
          </NButton>
        </NRow>
      </NModal.Footer>
    </NModal>
  )
})
