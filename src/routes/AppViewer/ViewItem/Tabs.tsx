import React, { FC } from 'react'
import styled from 'styled-components'
import { classnames } from '../../../common/classnames'
import { color, spacing } from '../../../components/GlobalStyle'
import { NTabs } from '../../../components/NTabs/NTabs'
import { ZoneProps } from '../../LayoutBuilder/utils/models'
import { useLayoutViewer } from '../contexts/LayoutViewerContext'
import { useRenderView } from '../contexts/useRenderView'
import { ComponentContainerClassName } from '../model'
import { resolveTextVariable } from '../utils'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  border-radius: 4px;
  overflow: hidden;

  &.default .tab-list:first-of-type {
    margin: 0 ${spacing('md')};
  }
`

export const Tabs: FC<ZoneProps> = ({ elementId }) => {
  const { elements, inputMap } = useLayoutViewer()
  const [active, setActive] = React.useState(0)

  const element = elements[elementId]
  const { type, showUnderlineBg } = element.props
  const elementChildren = element.children
  const tabId = elementChildren[active]

  return (
    <Wrapper className={classnames([ComponentContainerClassName, type || 'default'])}>
      <NTabs active={active} onChangeTab={setActive} type={type} showUnderlineBg={showUnderlineBg}>
        {elementChildren.map((id, idx) => {
          const tab = elements[id]
          return (
            <NTabs.Tab title={resolveTextVariable(tab.props.title, inputMap) || `Tab ${idx + 1}`} key={`tab-${idx}`}>
              <Tab elementId={tabId} />
            </NTabs.Tab>
          )
        })}
      </NTabs>
    </Wrapper>
  )
}

const TabWrapper = styled.div`
  display: flex;
  flex-direction: column;
  background-color: ${color('white')};

  & > :not(component-container):not(:last-child) {
    padding-bottom: 0px;
  }
`

const EmptyTab = styled.div`
  display: flex;
  padding: ${spacing('md')};
`

type TabProps = {
  elementId: string
}
const Tab: FC<TabProps> = ({ elementId }) => {
  const children = useRenderView(elementId)
  return <TabWrapper>{children.length ? children : <EmptyTab />}</TabWrapper>
}
