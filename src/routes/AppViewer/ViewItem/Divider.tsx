import React, { FC } from 'react'
import { useTheme } from 'styled-components'
import { color } from '../../../components/GlobalStyle'
import { NDivider } from '../../../components/NDivider'
import { ZoneProps } from '../../LayoutBuilder/utils/models'
import { useLayoutViewer } from '../contexts/LayoutViewerContext'

export const Divider: FC<ZoneProps> = ({ elementId }) => {
  const theme = useTheme()
  const { elements } = useLayoutViewer()
  const element = elements[elementId]
  return <NDivider lineColor={color('Neutral900')({ theme })} {...element.props} />
}
