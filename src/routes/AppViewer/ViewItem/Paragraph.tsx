import React, { FC } from 'react'
import styled from 'styled-components'
import { spacing } from '../../../components/GlobalStyle'
import { ZoneProps } from '../../LayoutBuilder/utils/models'
import { useLayoutViewer } from '../contexts/LayoutViewerContext'
import { resolveTextVariable } from '../utils'

const Text = styled.div`
  padding: ${spacing('md')};
`

export const Paragraph: FC<ZoneProps> = ({ elementId }) => {
  const { elements, inputMap } = useLayoutViewer()
  const element = elements[elementId]

  return <Text>{resolveTextVariable(element.props.children, inputMap)}</Text>
}
