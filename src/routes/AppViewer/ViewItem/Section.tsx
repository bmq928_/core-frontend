import React, { FC } from 'react'
import styled, { css } from 'styled-components'
import { color } from '../../../components/GlobalStyle'
import { NColumn, NRow } from '../../../components/NGrid/NGrid'
import { ZoneProps } from '../../LayoutBuilder/utils/models'
import { useLayoutViewer } from '../contexts/LayoutViewerContext'
import { useRenderView } from '../contexts/useRenderView'
import { ComponentContainerClassName } from '../model'

const StyledRow = styled(NRow)<{ showBackground?: boolean }>`
  ${props => {
    if (props.showBackground) {
      return css`
        background-color: ${color('white')};
        border-radius: 4px;
      `
    }
  }}
`

const SingleColumnWrapper = styled.div<{ showBackground?: boolean }>`
  display: flex;
  flex-direction: column;
  ${props => {
    if (props.showBackground) {
      return css`
        background-color: ${color('white')};
        border-radius: 4px;
      `
    }
  }}
`

export const Section: FC<ZoneProps> = ({ elementId }) => {
  const { elements } = useLayoutViewer()
  const children = useRenderView(elementId)
  const element = elements[elementId]
  const { columns, showBackground } = element.props as { columns: number[]; showBackground?: boolean }

  if (columns.length > 1) {
    return (
      <StyledRow className={ComponentContainerClassName} gutter={16} align="stretch" showBackground={showBackground}>
        {columns.map((flex, index) => {
          return (
            <NColumn key={`section_${elementId}_${index}`} flex={flex}>
              {children && children[index]}
            </NColumn>
          )
        })}
      </StyledRow>
    )
  }

  return (
    <SingleColumnWrapper className={ComponentContainerClassName} showBackground={showBackground}>
      {children}
    </SingleColumnWrapper>
  )
}
