import React from 'react'
import { useMutation, useQueryClient } from 'react-query'
import { useHistory } from 'react-router-dom'
import { NButton } from '../../../components/NButton/NButton'
import { NToast } from '../../../components/NToast'
import { APIServices } from '../../../services/api'
import { DataQueryKey } from '../../../services/api/endpoints/data'
import { ZoneProps } from '../../LayoutBuilder/utils/models'
import { useActionData } from '../contexts/ActionDataContext'
import { useLayoutViewer } from '../contexts/LayoutViewerContext'
import { removeCtxId, resolveButtonVariable, resolveTextVariable } from '../utils'
import { FlowInModal } from './Flow/FlowInModal'

export const Button = React.memo(function Node({ elementId, parentId }: ZoneProps) {
  const { elements, layoutId, inputMap, isLoadingContents } = useLayoutViewer()
  const queryClient = useQueryClient()

  const element = elements[elementId]
  const parent = elements[parentId]

  const history = useHistory()
  const params = useActionData()

  const [showFlow, setShowFlow] = React.useState(false)

  const actionId = `button_${element.props.actionId}`

  const isFlow = element.props.flowId

  const { mutate: deleteObjectData } = useMutation(APIServices.Data.deleteObjectData, {
    onError: error => {
      NToast.error({ title: 'Delete unsuccessful', subtitle: error.response?.data.message })
    },
  })

  const resolvedInputMap = React.useMemo(() => {
    return resolveButtonVariable(element.props.inputMap, inputMap)
  }, [inputMap])

  React.useEffect(() => {
    return () => {
      removeCtxId()
    }
  }, [actionId])

  const handleActionPress = (e: any) => {
    e.stopPropagation()
    if (isFlow) {
      setShowFlow(true)
      return
    }
    if (parent?.component === 'RelatedList') {
      if (element.props.action === 'new') {
        history.push(
          `/${element.props.objName}/create?layoutId=${layoutId}&componentId=${elementId}&relatedId=${params.id}&relatedField=${parent.props.relationField}`,
        )
        return
      }
    }
    if (element.props.type === 'object') {
      if (element.props.action === 'edit') {
        history.push(`/${element.props.objName}/${params.id}/edit?layoutId=${layoutId}&componentId=${elementId}`)
        return
      }
      if (element.props.action === 'new') {
        const urlSearchParams = new URLSearchParams(resolvedInputMap).toString()
        history.push(
          `/${element.props.objName}/create?layoutId=${layoutId}&componentId=${elementId}&${urlSearchParams}`,
        )
        return
      }
      if (element.props.action === 'delete' && params.id) {
        deleteObjectData(
          { name: element.props.objName, guid: params.id },
          {
            onSettled: () => {
              params.postDelete && params.postDelete()
            },
            onSuccess: () => {
              queryClient.invalidateQueries([DataQueryKey.getObjectData])
            },
            onError: error => {
              NToast.error({
                title: 'Delete unsuccessful',
                subtitle: error.response?.data.message,
              })
            },
          },
        )
        return
      }
    }
    NToast.error({
      title: 'Invalid Component',
      subtitle: `Action: ${element.props.title} is not supported!`,
    })
  }

  return (
    <React.Fragment>
      <NButton
        type={element.props.buttonType || 'default'}
        size={element.props.buttonSize || 'default'}
        onClick={handleActionPress}
        loading={isLoadingContents}>
        {resolveTextVariable(element.props.title, inputMap) || 'Untitled'}
      </NButton>
      {isFlow && showFlow && (
        <FlowInModal
          visible={showFlow}
          setVisible={setShowFlow}
          layoutId={layoutId}
          componentId={elementId}
          inputMap={resolvedInputMap}
          actionId={actionId}
        />
      )}
    </React.Fragment>
  )
})
