import * as React from 'react'
import styled from 'styled-components'
import { spacing } from '../../../components/GlobalStyle'
import { ZoneProps } from '../../LayoutBuilder/utils/models'
import { useLayoutViewer } from '../contexts/LayoutViewerContext'
import { resolveTextVariable } from '../utils'

const H = styled('h1')`
  padding: ${spacing('md')};
`

export const Heading: React.FC<ZoneProps> = ({ elementId }) => {
  const { elements, inputMap } = useLayoutViewer()
  const element = elements[elementId]
  return <H as={element.props.level}>{resolveTextVariable(element.props.children, inputMap)}</H>
}
