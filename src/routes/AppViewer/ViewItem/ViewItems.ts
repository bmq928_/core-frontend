import { ScreenElement } from '../../FlowBuilder/FlowBuilderItem/ScreenBuilder/utils/models'
import { ZoneProps } from '../../LayoutBuilder/utils/models'
import { Attachments } from './Attachments'
import { Button } from './Button'
import { Header } from './demo/Header'
import { Divider } from './Divider'
import { AmountInput } from './Flow/AmountInput'
import { Checkbox } from './Flow/Checkbox'
import { DateTime } from './Flow/DateTime'
import { DisplayText } from './Flow/DisplayText'
import { FileDropzone } from './Flow/FileDropzone'
import { Flow } from './Flow/Flow'
import { Input } from './Flow/Input'
import { InputList } from './Flow/InputList'
import { RendererPropsType } from './Flow/model'
import { PickList } from './Flow/PickList'
import { RadioGroup } from './Flow/RadioGroup'
import { Withdrawal } from './Flow/Withdrawal'
import { Heading } from './Heading'
import { ListView } from './Listview/ListView'
import { RelatedList } from './Listview/RelatedList'
import { Paragraph } from './Paragraph'
import { RecordDetail } from './RecordDetail/RecordDetail'
import { Section } from './Section'
import { Tabs } from './Tabs'

export const VIEW_ITEMS = {
  Section,
  Tabs,
  RecordDetail,
  Heading,
  Paragraph,
  Divider,
  Button,
  ObjectList: ListView,
  Flow,
  RelatedList,
  DemoHeader: Header,
  Attachments,
} as Record<string, (props: ZoneProps) => JSX.Element>

export type RendererType = (props: RendererPropsType) => JSX.Element

export const SCREEN_VIEW_ITEMS_INPUTS = {
  string: Input,
  float: Input,
  integer: Input,
  date: DateTime,
  time: DateTime,
  'date-time': DateTime,
  boolean: Checkbox,
} as Record<string, RendererType>
export const SCREEN_VIEW_ITEMS_SELECTIONS = {
  PickList,
  Radio: RadioGroup,
} as Record<string, RendererType>
export const SCREEN_VIEW_ITEMS = {
  Root: Flow,
  InputList,
  FileDropzone,
  DisplayText,
  Withdrawal,
  AmountInput,
} as Record<string, RendererType>

export const getScreenViewItemFromElement = (element: ScreenElement) => {
  const { props, component } = element
  if (component === 'AmountInput') {
    return SCREEN_VIEW_ITEMS.AmountInput
  }
  if (props.inputType) {
    return SCREEN_VIEW_ITEMS_INPUTS[props.inputType]
  }
  if (props.selectionType) {
    return SCREEN_VIEW_ITEMS_SELECTIONS[props.isRadio ? 'Radio' : 'PickList']
  }
  return SCREEN_VIEW_ITEMS[component]
}
