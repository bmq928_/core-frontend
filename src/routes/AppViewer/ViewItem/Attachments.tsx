import React, { FC } from 'react'
import styled from 'styled-components'
import { color, spacing } from '../../../components/GlobalStyle'
import { NFile } from '../../../components/NFileDropzone/NFile'
import { NSpinner } from '../../../components/NSpinner/NSpinner'
import { ZoneProps } from '../../LayoutBuilder/utils/models'
import { useLayoutViewer } from '../contexts/LayoutViewerContext'

const LoadingWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex: 1;
`

const AttachmentsWrapper = styled.div<{ hasAttachments: boolean }>`
  display: ${props => (props.hasAttachments ? 'flex' : 'none')};
  flex-wrap: wrap;
  width: 100%;
  padding: ${spacing('md')};
  background-color: ${color('white')};
  border-radius: 4px;
`

export const Attachments: FC<ZoneProps> = () => {
  const { contents, isLoadingContents } = useLayoutViewer()

  const mediaKeys = !!contents?.$metadata?.media ? Object.keys(contents.$metadata.media) : []
  const hasAttachments = mediaKeys.length > 0

  return (
    <AttachmentsWrapper hasAttachments={isLoadingContents || hasAttachments}>
      {(() => {
        if (isLoadingContents) {
          return (
            <LoadingWrapper>
              <NSpinner size={40} />
            </LoadingWrapper>
          )
        }
        // check media
        if (mediaKeys.length === 0) {
          return <></>
        }
        // display media
        return mediaKeys.map(key => {
          if (!contents?.$metadata?.media) {
            return <></>
          }
          const media = contents.$metadata.media[key]
          return <NFile key={`media_${media.id}`} name={key} media={media} review />
        })
      })()}
    </AttachmentsWrapper>
  )
}
