import React, { FC, useEffect, useMemo } from 'react'
import { useQuery } from 'react-query'
import { NavLink, useHistory } from 'react-router-dom'
import { Column } from 'react-table'
import styled from 'styled-components'
import { color, spacing } from '../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../components/Icons'
import { CurrencyData } from '../../../../components/NCurrency/CurrencyData'
import { NDivider } from '../../../../components/NDivider'
import { NRow } from '../../../../components/NGrid/NGrid'
import { NHyperlinkOrText } from '../../../../components/NHyperlinkOrText'
import { NContainer } from '../../../../components/NLayout/NContainer'
import { NPagination } from '../../../../components/NPagination'
import { NSpinner } from '../../../../components/NSpinner/NSpinner'
import { NTable, NTableColumnType } from '../../../../components/NTable/NTable'
import { NTableCollapsedCellClassName, NTableTagItem } from '../../../../components/NTable/NTableStyledContainer'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../components/NToast'
import { typography } from '../../../../components/NTypography'
import { useLocalTableConfigs } from '../../../../hooks/useLocalTableConfigs'
import { useSearchText } from '../../../../hooks/useSearchText'
import { APIServices } from '../../../../services/api'
import { DataQueryKey } from '../../../../services/api/endpoints/data'
import { FieldSchema, FilterItem } from '../../../../services/api/models'
import { getTableData } from '../../../../utils/table-utils'
import { capitalize, getDateFromIsoString, getStringFromDate } from '../../../../utils/utils'
import { ZoneProps } from '../../../LayoutBuilder/utils/models'
import { useLayoutViewer } from '../../contexts/LayoutViewerContext'
import { useRenderView } from '../../contexts/useRenderView'
import { ComponentContainerClassName } from '../../model'
import { resolveFilterVariable, resolveTextVariable } from '../../utils'
import { ListViewTableActions } from './ListViewTableActions'

const HeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: ${spacing('md')};
`
const Title = styled.div`
  ${typography('h500')};
`
const Description = styled.div`
  ${typography('ui-text')};
  color: ${color('Neutral500')};
  margin-bottom: ${spacing('md')};
`

const ErrorText = styled.p`
  color: ${color('Red700')};
  ${typography('overline')}
`

const ActionWrapper = styled.div`
  display: flex;
  & > button {
    margin-left: ${spacing('xs')};
    margin-right: ${spacing('xs')};
  }
  & > button:first-of-type {
    margin-left: 0;
  }
  & > button:last-of-type {
    margin-right: 0;
  }
`

export const ListViewWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: ${spacing('md')};
  background-color: ${color('white')};
  border-radius: 4px;
`

type ElementProps = {
  objName?: string
  limit?: number
  fields: string[]
  filters?: FilterItem[][]
  rowActions?: string[]
}

export const ListView: FC<ZoneProps> = ({ elementId }) => {
  const { elements, inputMap } = useLayoutViewer()
  const history = useHistory()

  const element = elements[elementId]
  const actions = useRenderView(elementId)

  const { objName, limit, fields, filters, rowActions } = (element?.props || {
    objName: undefined,
    limit: 10,
  }) as ElementProps

  const isFiltered = !!(filters && filters.length > 0)

  const [{ searchText, searchValue }, onChangeSearch] = useSearchText()
  const [{ sort, currentPage }, { onChangeSort, onChangePage }] = useLocalTableConfigs()

  const isSeachable = useMemo(() => {
    if (!element || !element.schema) {
      return false
    }

    const foundSearchable = element.schema.fields.find(f => f.isSearchable)
    return !!foundSearchable
  }, [element])

  const mapFilter = resolveFilterVariable(filters || [], inputMap)
  // get content list
  const { data: filteredData, isLoading: isLoadingFiltered } = useQuery(
    [
      DataQueryKey.searchObjectData,
      {
        name: objName!,
        limit,
        offset: limit ? (currentPage - 1) * limit : 0,
        searchText,
        filters: mapFilter,
        sortBy: sort?.sortBy || element.props.sortBy,
        sortOrder: sort?.sortOrder || (element.props.sortBy ? element.props.sortOrder : undefined),
      },
    ],
    APIServices.Data.searchObjectData,
    {
      onError: error => {
        NToast.error({ title: 'Get object data error!', subtitle: error.response?.data.message })
      },
      enabled: Boolean(isFiltered && objName),
    },
  )

  const { data, isLoading: isLoadingData } = useQuery(
    [
      DataQueryKey.getObjectData,
      {
        name: objName!,
        limit,
        offset: limit ? (currentPage - 1) * limit : 0,
        searchText,
        sortBy: sort?.sortBy || element.props.sortBy,
        sortOrder: sort?.sortOrder || (element.props.sortBy ? element.props.sortOrder : undefined),
      },
    ],
    APIServices.Data.getObjectData,
    {
      onError: error => {
        NToast.error({ title: 'Get object data error!', subtitle: error.response?.data.message })
      },
      enabled: Boolean(!isFiltered && objName),
    },
  )

  useEffect(() => {
    onChangePage(1)
  }, [searchText])

  // this is neccessary because of react table :(
  const { pageData, pageInfo, totalPage } = useMemo(() => {
    return getTableData(isFiltered ? filteredData : data)
  }, [data, filteredData, isFiltered])

  const columns = React.useMemo(() => {
    if (!element.schema) {
      return []
    }

    const fieldsByName = element.schema.fields.reduce(
      (a, i) => ({ ...a, [i.name]: i }),
      {} as Record<string, FieldSchema>,
    )

    return getTableColumns(elementId, fields, fieldsByName, rowActions)
  }, [elementId, element.schema, fields, rowActions]) as NTableColumnType<any>

  if (!element) {
    return <ErrorText>Cannot find element</ErrorText>
  }

  if (!element.schema) {
    return <ErrorText>Script missing fields schema</ErrorText>
  }

  const titleText = resolveTextVariable(element.props.title, inputMap) || element.schema?.displayName
  const descriptionText =
    resolveTextVariable(element.props.description, inputMap) || `${pageInfo.total} ${element.schema?.displayName}`
  return (
    <ListViewWrapper className={ComponentContainerClassName}>
      <HeaderContainer>
        <Title>
          {titleText || (
            <NRow align="center">
              Loading
              <NDivider vertical size="md" />
              <NSpinner size={20} strokeWidth={4} />
            </NRow>
          )}
        </Title>
        <ActionWrapper>{actions}</ActionWrapper>
      </HeaderContainer>
      <Description>{descriptionText}</Description>
      <NContainer.Content>
        {isSeachable && (
          <NTextInput
            left={<GlobalIcons.Search />}
            placeholder="Search"
            value={searchValue || ''}
            onChange={e => {
              onChangeSearch(e.target.value)
            }}
          />
        )}
        <NDivider size="md" />

        <NTable
          isLoading={isLoadingData || isLoadingFiltered}
          columns={columns as any}
          data={pageData}
          defaultSortBy={sort?.sortBy}
          defaultSortOrder={sort?.sortOrder}
          onChangeSort={onChangeSort}
          pageSize={limit}
          pagination={<NPagination total={totalPage} current={currentPage} onChangePage={onChangePage} />}
          onClickRow={data => {
            const id = data.guid || data.externalId
            if (!id) {
              NToast.error({ title: "Missing 'id' in the row data" })
              return
            }
            history.push(`/${objName}/${id}`)
          }}
        />
      </NContainer.Content>
    </ListViewWrapper>
  )
}

const StyledNavLink = styled(NavLink)`
  text-decoration: underline;
  border-radius: 2px;
  padding: ${spacing('xxs')};
  margin: -${spacing('xxs')};
  color: ${color('Neutral700')};
  &:hover {
    background-color: ${color('Neutral300')};
  }
`

const dateFormat = 'dd/MM/yyyy'
const timeFormat = 'HH:mm:ss'

export function getTableColumns(
  elementId: string,
  fields: string[],
  fieldsByName: Record<string, FieldSchema>,
  rowActions?: string[],
): Column<any>[] {
  const tableActions: Column<any> = {
    accessor: 'objName',
    Cell: ({ row }) => {
      return <ListViewTableActions parentId={elementId} data={row.original} rowActions={rowActions || []} />
    },
    id: NTableCollapsedCellClassName,
  }

  const columns = fields.map<Column<any>>(field => {
    const fieldData = fieldsByName[field]
    if (!fieldData) {
      return {
        Header: capitalize(field),
        Cell: () => `{Missing field}`,
      }
    }
    const { displayName: Header, isSortable: defaultCanSort, typeName, subType } = fieldData
    const accessor = field
    if (typeName === 'boolean') {
      return {
        Header,
        accessor,
        defaultCanSort,
        Cell: ({ value }) => (value ? <GlobalIcons.Check /> : null),
      }
    }
    if (typeName === 'dateTime') {
      return {
        Header,
        accessor,
        defaultCanSort,
        Cell: ({ value }) => {
          if (!value) {
            return null
          }
          const date = getDateFromIsoString(value, subType)
          return getStringFromDate({ date, type: subType, dateFormat, timeFormat })
        },
      }
    }
    if (subType === 'pickList' || subType === 'multi') {
      return {
        Header,
        accessor,
        defaultCanSort,
        Cell: ({ value, row }) => {
          return (
            <NTableTagItem.Container>
              {value.map((val: string, index: number) => {
                return <NTableTagItem.Tag key={`${field}_${row.original.guid}_${index}`}>{val}</NTableTagItem.Tag>
              })}
              <NTableTagItem.Blur />
            </NTableTagItem.Container>
          )
        },
      }
    }
    if (typeName === 'relation') {
      return {
        Header,
        accessor,
        defaultCanSort,
        Cell: ({ value, row }) => {
          const relation = row.original['$metadata']?.relations
          const link = `/${fieldData.value}/${relation[accessor]}`
          return (
            <StyledNavLink to={link} onClick={e => e.stopPropagation()}>
              {value}
            </StyledNavLink>
          )
        },
      }
    }
    if (typeName === 'currency') {
      return {
        Header,
        accessor,
        defaultCanSort,
        Cell: ({ value, row }) => {
          return (
            <CurrencyData
              dataCurrencyCode={row.original.currencyCode}
              value={value}
              convertedValue={row.original['$metadata']?.currency_values?.[fieldData.name]}
            />
          )
        },
      }
    }
    if (typeName === 'externalRelation' || typeName === 'indirectRelation') {
      return {
        Header,
        accessor,
        defaultCanSort,
        Cell: ({ value }) => {
          const link = `/${fieldData.value}/${value}`
          return (
            <StyledNavLink to={link} onClick={e => e.stopPropagation()}>
              {value}
            </StyledNavLink>
          )
        },
      }
    }
    return {
      Header,
      accessor,
      defaultCanSort,
      Cell: ({ value }) => {
        if (typeof value === 'object') {
          return 'N/A'
        }
        return <NHyperlinkOrText>{value ?? null}</NHyperlinkOrText>
      },
    }
  })

  return (rowActions || []).length ? columns.concat(tableActions) : columns
}
