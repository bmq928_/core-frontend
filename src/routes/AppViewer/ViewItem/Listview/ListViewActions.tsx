import React, { FC } from 'react'
import { NavLink } from 'react-router-dom'
import styled from 'styled-components'
import { spacing } from '../../../../components/GlobalStyle'
import { NButton } from '../../../../components/NButton/NButton'
import { NRow } from '../../../../components/NGrid/NGrid'

const StyledRow = styled(NRow)`
  align-items: center;

  button:not(:last-child) {
    margin-right: ${spacing('xs')};
  }
`
type Props = {
  actions: string[]
  objectName?: string
}

export const ListViewActions: FC<Props> = ({ actions, objectName }) => {
  return (
    <StyledRow>
      {actions.map((act, index) => {
        if (act === 'new' && objectName) {
          return (
            <NavLink key={`${act}_${index}`} to={`/${objectName}/create`}>
              <NButton type="primary" size="small">
                New
              </NButton>
            </NavLink>
          )
        }

        return (
          <NButton key={`${act}_${index}`} type="default" size="small">
            {act}
          </NButton>
        )
      })}
    </StyledRow>
  )
}
