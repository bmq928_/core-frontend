import React, { FC, useMemo, useRef, useState } from 'react'
import styled from 'styled-components'
import { color } from '../../../../components/GlobalStyle'
import { Icons } from '../../../../components/Icons'
import { NButton } from '../../../../components/NButton/NButton'
import { elevation } from '../../../../components/NElevation'
import { Portal } from '../../../../components/Portal'
import { useClickOutside } from '../../../../hooks/useClickOutside'
import { DataResponse } from '../../../../services/api/models'
import { ActionDataProvider } from '../../contexts/ActionDataContext'
import { Button } from '../Button'

const DropListContainer = styled.div`
  position: absolute;
  display: flex;
  flex-direction: column;
  width: 100px;
  z-index: 10000;
  background: ${color('white')};
  border-radius: 4px;
  ${elevation('Elevation200')}
`

type ListViewTableActionsProps = {
  parentId: string
  data: DataResponse
  rowActions: string[]
  mountNode?: React.RefObject<HTMLDivElement>
}
export const ListViewTableActions: FC<ListViewTableActionsProps> = ({ parentId, data, rowActions, mountNode }) => {
  const [showDropList, setShowDropList] = useState(false)
  const buttonRef = useRef<HTMLButtonElement>(null)
  const dropdownRef = useRef(null)

  useClickOutside(() => {
    setShowDropList(false)
  }, dropdownRef)

  const { top, left } = useMemo(() => {
    if (showDropList) {
      const containerSizes = buttonRef.current?.getBoundingClientRect()
      return {
        top: (containerSizes?.top || 0) + (containerSizes?.height || 0) + 4,
        left: containerSizes?.left ? containerSizes.left - 88 : 0,
      }
    }
    return { top: 0, left: 0 }
  }, [showDropList])

  const postDelete = () => {
    setShowDropList(false)
  }

  return (
    <>
      <NButton
        ref={buttonRef}
        type="ghost"
        icon={<Icons.More />}
        style={{ width: 24, height: 24, padding: 0 }}
        onClick={e => {
          e.stopPropagation()
          setShowDropList(true)
        }}
      />
      <Portal mountNode={mountNode}>
        {showDropList && (
          <ActionDataProvider id={data.guid} postDelete={postDelete}>
            <DropListContainer ref={dropdownRef} style={{ top, left }}>
              {rowActions.map((actionId, idx) => {
                return <Button key={actionId} elementId={actionId} parentId={parentId} index={idx} />
              })}
            </DropListContainer>
          </ActionDataProvider>
        )}
      </Portal>
    </>
  )
}
