import React, { useEffect } from 'react'
import { useQuery } from 'react-query'
import { useHistory, useParams } from 'react-router-dom'
import styled from 'styled-components'
import { color, spacing } from '../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../components/Icons'
import { NDivider } from '../../../../components/NDivider'
import { NRow } from '../../../../components/NGrid/NGrid'
import { NContainer } from '../../../../components/NLayout/NContainer'
import { NPagination } from '../../../../components/NPagination'
import { NSpinner } from '../../../../components/NSpinner/NSpinner'
import { NTable, NTableColumnType } from '../../../../components/NTable/NTable'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../components/NToast'
import { typography } from '../../../../components/NTypography'
import { useLocalTableConfigs } from '../../../../hooks/useLocalTableConfigs'
import { useSearchText } from '../../../../hooks/useSearchText'
import { APIServices } from '../../../../services/api'
import { BuilderQueryKey } from '../../../../services/api/endpoints/builder'
import { DataQueryKey } from '../../../../services/api/endpoints/data'
import { FieldSchema, FilterItem } from '../../../../services/api/models'
import { getTableData } from '../../../../utils/table-utils'
import { ZoneProps } from '../../../LayoutBuilder/utils/models'
import { useLayoutViewer } from '../../contexts/LayoutViewerContext'
import { useRenderView } from '../../contexts/useRenderView'
import { ComponentContainerClassName } from '../../model'
import { resolveFilterVariable, resolveTextVariable } from '../../utils'
import { getTableColumns, ListViewWrapper } from './ListView'

const HeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: ${spacing('md')};
`
const Title = styled.div`
  ${typography('h500')};
`
const Description = styled.div`
  ${typography('ui-text')};
  color: ${color('Neutral500')};
  margin-bottom: ${spacing('md')};
`

type Params = {
  objectName?: string
  id?: string
}

type ElementProps = {
  relationField: string
  objName: string
  fields: string[]
  title: string
  description: string
  limit: number
  filters?: FilterItem[][]
  rowActions?: string[]
}

export function RelatedList({ elementId }: ZoneProps) {
  const { elements, inputMap } = useLayoutViewer()
  const { objectName, id } = useParams<Params>()
  const history = useHistory()

  const element = elements[elementId]

  const actions = useRenderView(elementId)

  const {
    title,
    description,
    relationField,
    objName,
    limit,
    fields,
    filters,
    rowActions,
  } = element.props as ElementProps

  const isFiltered = filters && filters.length > 0

  const [{ searchText, searchValue }, onChangeSearch] = useSearchText()
  const [{ sort, currentPage }, { onChangeSort, onChangePage }] = useLocalTableConfigs()

  // get Object Data
  const { data: objectData, isLoading: isObjectDataLoading } = useQuery(
    [BuilderQueryKey.getObjectSchema, { objName: objName! }],
    APIServices.Builder.getObjectSchema,
    {
      enabled: !!objectName,
      onError: error => {
        NToast.error({ title: `Get Object ${objectName} unsuccessful`, subtitle: `${error.response?.data.message}` })
        console.error('Related List', error)
      },
    },
  )

  // get Related Data
  const { data, isLoading } = useQuery(
    [
      DataQueryKey.getObjectRelatedData,
      {
        name: objectName!,
        guid: id!,
        relation: relationField,
        limit,
        searchText,
        offset: limit ? (currentPage - 1) * limit : 0,
        sortBy: sort?.sortBy || element.props.sortBy,
        sortOrder: sort?.sortOrder || (element.props.sortBy ? element.props.sortOrder : undefined),
      },
    ],
    APIServices.Data.getObjectRelatedData,
    {
      enabled: Boolean(objectName && id && element.props.relationField && !isFiltered),
    },
  )

  const mapFilter = resolveFilterVariable(filters || [], inputMap)
  const { data: filteredData, isLoading: isLoadingFiltered } = useQuery(
    [
      DataQueryKey.searchObjectRelatedData,
      {
        name: objectName!,
        guid: id!,
        relation: relationField,
        limit,
        searchText,
        offset: limit ? (currentPage - 1) * limit : 0,
        filters: mapFilter,
        sortBy: sort?.sortBy || element.props.sortBy,
        sortOrder: sort?.sortOrder || (element.props.sortBy ? element.props.sortOrder : undefined),
      },
    ],
    APIServices.Data.searchObjectRelatedData,
    {
      enabled: Boolean(objectName && id && element.props.relationField && isFiltered),
    },
  )
  const isSearchable = (element && element.schema?.fields.some(f => f.isSortable)) || false

  useEffect(() => {
    onChangePage(1)
  }, [searchText])

  const columns = React.useMemo(() => {
    if (!element.schema) {
      return []
    }

    const fieldsByName = element.schema.fields.reduce(
      (a, i) => ({ ...a, [i.name]: i }),
      {} as Record<string, FieldSchema>,
    )

    return getTableColumns(elementId, fields, fieldsByName, rowActions)
  }, [elementId, element.schema, fields, rowActions]) as NTableColumnType<any>

  const { pageData, totalPage } = React.useMemo(() => {
    return getTableData(isFiltered ? filteredData : data)
  }, [data, filteredData, isFiltered])

  return (
    <ListViewWrapper className={ComponentContainerClassName}>
      <HeaderContainer>
        <Title>
          {isObjectDataLoading && (
            <NRow align="center">
              Loading
              <NDivider vertical size="md" />
              <NSpinner size={20} strokeWidth={4} />
            </NRow>
          )}
          {!isObjectDataLoading && (resolveTextVariable(title, inputMap) || objectData?.data.displayName)}
        </Title>
        <NRow>{actions}</NRow>
      </HeaderContainer>
      <Description>
        {!isObjectDataLoading && !isLoadingFiltered && resolveTextVariable(description, inputMap)}
      </Description>

      <NContainer.Content>
        {isSearchable && (
          <NTextInput
            left={<GlobalIcons.Search />}
            placeholder="Search"
            value={searchValue}
            onChange={e => onChangeSearch(e.target.value)}
          />
        )}
        <NDivider size="md" />
        <NTable
          isLoading={isLoading || isLoadingFiltered}
          columns={columns}
          data={pageData}
          defaultSortBy={sort?.sortBy}
          defaultSortOrder={sort?.sortOrder}
          onChangeSort={onChangeSort}
          pageSize={limit}
          pagination={<NPagination total={totalPage} current={currentPage} onChangePage={onChangePage} />}
          onClickRow={data => {
            const id = data.guid || data.externalId
            if (!id) {
              NToast.error({ title: "Missing 'id' in the row data" })
              return
            }
            history.push(`/${objName}/${id}`)
          }}
        />
      </NContainer.Content>
    </ListViewWrapper>
  )
}
