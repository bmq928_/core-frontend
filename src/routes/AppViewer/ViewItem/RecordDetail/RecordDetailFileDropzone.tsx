import React, { FC, useMemo } from 'react'
import styled from 'styled-components'
import { spacing } from '../../../../components/GlobalStyle'
import { NFile } from '../../../../components/NFileDropzone/NFile'
import { NFileType } from '../../../../components/NFileDropzone/NFileType'
import { useLayoutViewer } from '../../contexts/LayoutViewerContext'

const FileDropzoneGrid = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: ${spacing('sm')};
  min-height: 200px;
`

type RecordDetailFileDropzoneProps = {
  elementId: string
}
type MediaType = Record<string, NFileType | undefined>
export const RecordDetailFileDropzone: FC<RecordDetailFileDropzoneProps> = ({ elementId }) => {
  const { elements, contents, isLoadingContents } = useLayoutViewer()

  const media = useMemo<MediaType>(() => {
    const element = elements[elementId]
    const fileNames: string[] = element.props.fileNames
    const result = fileNames.reduce((prev, cur) => {
      return { ...prev, [cur]: undefined } as MediaType
    }, {} as MediaType)
    // transform content
    const contentMedia: MediaType = contents?.$metadata?.media || {}
    const contentKeys = Object.keys(contentMedia)
    const transformMedia = contentKeys.reduce((prev, cur) => {
      const curMedia = contentMedia[cur]
      if (!curMedia) {
        return prev
      }
      return {
        ...prev,
        [curMedia.tagName || curMedia.id]: curMedia,
      }
    }, {} as MediaType)

    return {
      ...result,
      ...transformMedia,
    }
  }, [contents, elements, elementId])

  if (isLoadingContents) {
    return <></>
  }

  const mediaKeys = Object.keys(media)
  return (
    <FileDropzoneGrid>
      {mediaKeys.map((key: string) => {
        const mediaContent = media[key]
        const keyName = `media_${key}`

        return <NFile key={keyName} name={key} media={mediaContent} />
      })}
    </FileDropzoneGrid>
  )
}
