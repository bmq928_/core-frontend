import React, { FC, useMemo } from 'react'
import { Controller, useFormContext } from 'react-hook-form'
import { useMutation } from 'react-query'
import styled from 'styled-components'
import { spacing } from '../../../../components/GlobalStyle'
import { NFileDropzone } from '../../../../components/NFileDropzone/NFileDropZone'
import { NFileType } from '../../../../components/NFileDropzone/NFileType'
import { NToast } from '../../../../components/NToast'
import { APIServices } from '../../../../services/api'
import { useLayoutViewer } from '../../contexts/LayoutViewerContext'

const FileDropzoneGrid = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: ${spacing('sm')};
  min-height: 200px;
`

type RecordDetailFileDropzoneFormProps = {
  elementId: string
}
type MediaType = Record<string, NFileType | undefined>
export const RecordDetailFileDropzoneForm: FC<RecordDetailFileDropzoneFormProps> = ({ elementId }) => {
  const formMethods = useFormContext()
  const { elements, contents, isLoadingContents } = useLayoutViewer()
  const formName = '$metadata.media'
  // const theme = useTheme()
  const defaultValue = useMemo<MediaType>(() => {
    const element = elements[elementId]
    const fileNames: string[] = element.props.fileNames
    const result = fileNames.reduce((prev, cur) => {
      return { ...prev, [cur]: undefined } as MediaType
    }, {} as MediaType)
    // transform content
    const contentMedia: MediaType = contents?.$metadata?.media || {}
    const contentKeys = Object.keys(contentMedia)
    const transformMedia = contentKeys.reduce((prev, cur) => {
      const curMedia = contentMedia[cur]
      if (!curMedia) {
        return prev
      }
      return {
        ...prev,
        [curMedia.tagName || curMedia.id]: curMedia,
      }
    }, {} as MediaType)

    return {
      ...result,
      ...transformMedia,
    }
  }, [contents, elements, elementId])

  if (isLoadingContents) {
    return <></>
  }

  const { actionMetadata } = elements[elementId].props

  return (
    <Controller
      name={formName}
      control={formMethods.control}
      defaultValue={defaultValue}
      render={({ field: { value, onChange } }) => {
        const mediaKeys = Object.keys(value || {})

        return (
          <FileDropzoneGrid>
            {mediaKeys.map((key: string) => {
              const mediaContent = value[key]
              const keyName = `media_${key}`

              return (
                <FileDropzone
                  key={keyName}
                  name={key}
                  actionMetadata={actionMetadata}
                  file={mediaContent}
                  onChange={newMedia => {
                    const newVal: MediaType = { ...value }
                    newVal[key] = newMedia
                    onChange(newVal)
                  }}
                  onRemove={() => {
                    const newVal: MediaType = { ...value }
                    newVal[key] = undefined
                    onChange(newVal)
                  }}
                />
              )
            })}
          </FileDropzoneGrid>
        )
      }}
    />
  )
}

type FileDropzoneProps = {
  name: string
  actionMetadata: string
  file?: NFileType
  onChange: (newMedia: NFileType) => void
  onRemove: () => void
}
const FileDropzone: FC<FileDropzoneProps> = ({ name, actionMetadata, file, onChange, onRemove }) => {
  const { mutate: uploadFile, isLoading } = useMutation(APIServices.UploadFile.uploadFile, {
    onError: error => {
      NToast.error({ title: 'Upload file error', subtitle: error.response?.data.message })
    },
  })

  return (
    <NFileDropzone
      label={name}
      file={file}
      isUploading={isLoading}
      onDropAccepted={newFiles => {
        const formData = new FormData()
        formData.append('file', newFiles[0])
        uploadFile(
          {
            name: actionMetadata,
            formData,
          },
          {
            onSuccess: data => {
              onChange(data.data)
            },
          },
        )
      }}
      onRemove={onRemove}
    />
  )
}
