import React, { FC } from 'react'
import { FieldItemMode, NFieldItem } from '../../../../components/NFieldItem/NFieldItem'
import { FieldSchema } from '../../../../services/api/models'
import { ErrorText } from '../../components/ErrorText'

type FieldItemProps = {
  fieldName: string
  fields: FieldSchema[]
  mode: FieldItemMode
  contents: Record<string, any>
}

export const FieldItem: FC<FieldItemProps> = ({ fieldName, fields, mode, contents }) => {
  // props
  const field = fields.find(f => f.name === fieldName)

  if (!field) {
    return <ErrorText>Field {fieldName} doesn't exist</ErrorText>
  }
  return <NFieldItem mode={mode} field={field} contents={contents} />
}
