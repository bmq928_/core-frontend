import React, { FC, useMemo } from 'react'
import { useHistory } from 'react-router-dom'
import styled, { useTheme } from 'styled-components'
import { color, spacing } from '../../../../components/GlobalStyle'
import { Icons } from '../../../../components/Icons'
import { NButton } from '../../../../components/NButton/NButton'
import { NDivider } from '../../../../components/NDivider'
import { FieldItemMode } from '../../../../components/NFieldItem/NFieldItem'
import { NRow } from '../../../../components/NGrid/NGrid'
import { NSpinner } from '../../../../components/NSpinner/NSpinner'
import { typography } from '../../../../components/NTypography'
import { capitalize } from '../../../../utils/utils'
import { ZoneProps } from '../../../LayoutBuilder/utils/models'
import { useLayoutViewer } from '../../contexts/LayoutViewerContext'
import { useRenderView } from '../../contexts/useRenderView'
import { ComponentContainerClassName, LayoutType } from '../../model'
import { resolveTextVariable } from '../../utils'
import { FieldItem } from './FieldItem'
import { RecordDetailFileDropzone } from './RecordDetailFileDropzone'
import { RecordDetailFileDropzoneForm } from './RecordDetailFileDropzoneForm'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  padding: ${spacing('md')};
  background-color: ${color('white')};
  border-radius: 4px;
`

const GoBackBtn = styled(NButton)`
  padding-right: ${spacing('md')};
  padding-left: ${spacing('md')};
`

const Title = styled.div`
  margin-bottom: 0px;
  ${typography('h500')};
`

const Description = styled.div`
  margin-top: ${spacing('md')};
  color: ${color('Neutral500')};
  ${typography('ui-text')};
`

const LoadingContainer = styled.div`
  display: flex;
  justify-content: center;
`

const GridWrapper = styled.div<{ numOfCols?: number }>`
  display: grid;
  grid-template-columns: repeat(${props => props?.numOfCols || 1}, minmax(0, 1fr));
  grid-column-gap: ${spacing('xxl')};
  grid-row-gap: ${spacing('xl')};
`

const ActionsWrapper = styled('div')`
  display: flex;
  & > *:not(:last-child) {
    margin-right: ${spacing('xs')};
  }
`

export const RecordDetail: FC<ZoneProps> = ({ elementId }) => {
  const { elements, contents, layoutType, isLoadingContents, objectName, inputMap } = useLayoutViewer()
  const actions = useRenderView(elementId)
  const history = useHistory()
  const theme = useTheme()

  const element = useMemo(() => {
    return elements[elementId]
  }, [elements, elementId])

  const { numOfCols, fields, title, description } = element.props as {
    isPrimary: boolean
    numOfCols: number
    fields: string[]
    actions: string[]
    title?: string
    description?: string
  }

  const isFormLayout = layoutType === LayoutType.Create || layoutType === LayoutType.Edit

  const titleByType = ({
    [LayoutType.Create]: `Create new ${capitalize(objectName)}`,
    [LayoutType.Edit]: `Edit ${capitalize(objectName)}`,
  } as Record<string, string>)[layoutType]

  return (
    <Wrapper className={ComponentContainerClassName}>
      <NRow justify="space-between" align="center">
        <NRow align="center">
          {!element.props.hideBackBtn && (
            <GoBackBtn
              type="ghost"
              size="small"
              onClick={() => {
                history.goBack()
              }}>
              <Icons.Left />
            </GoBackBtn>
          )}
          <Title>{titleByType || resolveTextVariable(title, inputMap)}</Title>
        </NRow>
        <ActionsWrapper>{!isFormLayout && actions}</ActionsWrapper>
      </NRow>
      {!isFormLayout && description && <Description>{resolveTextVariable(description, inputMap)}</Description>}
      <NDivider size="xxl" lineSize={1} lineColor={color('Neutral200')({ theme })} />
      <NDivider size="sm" />
      {isLoadingContents && (
        <LoadingContainer>
          <NSpinner size={40} strokeWidth={2} />
        </LoadingContainer>
      )}
      {!isLoadingContents && (
        <GridWrapper numOfCols={numOfCols}>
          {fields.map((fieldName: string) => {
            return (
              <FieldItem
                key={`record_detail_${elementId}_field_${fieldName}`}
                fieldName={fieldName}
                fields={element?.schema?.fields || []}
                mode={isFormLayout ? FieldItemMode.Form : FieldItemMode.View}
                contents={contents || {}}
              />
            )
          })}
        </GridWrapper>
      )}
      {!!element.props.showFileDropzones && (
        <>
          <NDivider size="md" />
          {isFormLayout ? (
            <RecordDetailFileDropzoneForm elementId={elementId} />
          ) : (
            <RecordDetailFileDropzone elementId={elementId} />
          )}
        </>
      )}
    </Wrapper>
  )
}
