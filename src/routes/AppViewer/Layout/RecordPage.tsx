import React, { FC } from 'react'
import { useQuery } from 'react-query'
import { useParams } from 'react-router-dom'
import { NSpinner } from '../../../components/NSpinner/NSpinner'
import { APIServices } from '../../../services/api'
import { LayoutQueryKeys } from '../../../services/api/endpoints/layout'
import { Element } from '../../LayoutBuilder/utils/models'
import { ErrorContainer, ErrorSubtitle, ErrorTitle } from '../components/ErrorComponents'
import { LayoutViewerProvider } from '../contexts/LayoutViewerContext'
import { LayoutType } from '../model'
import { LayoutViewerRoot } from './LayoutViewerRoot'

export enum RecordPageType {
  Detail = 'RECORD_DETAIL_PAGE',
  Form = 'RECORD_FORM_PAGE',
}

type RecordPageProps = {
  objectName: string
  defaultValue?: object
  mode: RecordPageType
}

export const RecordPage: FC<RecordPageProps> = ({ objectName, mode }) => {
  const { id } = useParams<{ id: string | undefined }>()

  // const queryFunction =
  //   mode === RecordPageType.Detail ? APIServices.Layout.getObjectLayout : APIServices.Layout.getObjectMutationLayout
  // const queryKey =
  //   mode === RecordPageType.Detail ? LayoutQueryKeys.getObjectLayout : LayoutQueryKeys.getObjectMutationLayout
  // get template
  const { data: templateData, isFetching: isLoadingTemplate, error: getLayoutError } = useQuery(
    [LayoutQueryKeys.getObjectLayout, { objName: objectName }],
    APIServices.Layout.getObjectLayout,
  )

  if (getLayoutError && getLayoutError.response) {
    return (
      <ErrorContainer>
        <ErrorTitle>Error get form layout</ErrorTitle>
        <ErrorSubtitle>Code {getLayoutError.response.data.statusCode}</ErrorSubtitle>
        <ErrorSubtitle>{getLayoutError.response.data.message}</ErrorSubtitle>
      </ErrorContainer>
    )
  }

  if (isLoadingTemplate) {
    return (
      <ErrorContainer>
        <NSpinner size={16} strokeWidth={2} />
      </ErrorContainer>
    )
  }

  if (!templateData) {
    return (
      <ErrorContainer>
        <ErrorTitle>Error get form layout</ErrorTitle>
        <ErrorSubtitle>Missing layout script</ErrorSubtitle>
      </ErrorContainer>
    )
  }

  const layoutType = mode === RecordPageType.Detail ? LayoutType.Detail : id ? LayoutType.Edit : LayoutType.Create
  return (
    <LayoutViewerProvider
      elements={templateData.data.script as Record<string, Element>}
      layoutId={templateData.data.id}
      layoutType={layoutType}
      objectName={objectName}>
      <LayoutViewerRoot />
    </LayoutViewerProvider>
  )
}
