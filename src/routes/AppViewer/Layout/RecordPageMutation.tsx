import * as React from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { useMutation, useQueryClient } from 'react-query'
import { useHistory, useLocation, useParams } from 'react-router-dom'
import styled from 'styled-components'
import { NButton } from '../../../components/NButton/NButton'
import { NFileType } from '../../../components/NFileDropzone/NFileType'
import { NSpinner } from '../../../components/NSpinner/NSpinner'
import { NToast } from '../../../components/NToast'
import { APIServices } from '../../../services/api'
import { DataQueryKey } from '../../../services/api/endpoints/data'
import { ObjectLayoutResponse } from '../../../services/api/models'
import { Element } from '../../LayoutBuilder/utils/models'
import { ErrorContainer, ErrorSubtitle, ErrorTitle } from '../components/ErrorComponents'
import { LayoutViewerProvider } from '../contexts/LayoutViewerContext'
import { LayoutType } from '../model'
import { LayoutViewerRoot } from './LayoutViewerRoot'

const ActionsWrapper = styled('div')`
  display: flex;
  justify-content: flex-end;
  margin-top: 8px;
  background: white;
  padding: 8px;
  border-radius: 4px;
  & > :not(:last-child) {
    margin-right: 8px;
  }
`

type RecordPageMutationProps = {
  children?: React.ReactNode
}

export function RecordPageMutation({}: RecordPageMutationProps) {
  const params = useParams<{ id?: string; objectName: string }>()
  const location = useLocation()
  const history = useHistory()
  const urlSearch = new URLSearchParams(location.search)

  // required fields
  const layoutId = urlSearch.get('layoutId')
  const componentId = urlSearch.get('componentId')

  // related list fields
  const relatedId = urlSearch.get('relatedId')
  const relatedField = urlSearch.get('relatedField')

  const guid = params.id
  const objectName = params.objectName

  // Calculate default relation values if exist
  const defaultValues = React.useMemo(() => {
    if (relatedField && relatedId) {
      const paths = relatedField.split('.')
      return {
        // Assumed relatedField follow format `objectName.relatedFieldName`
        [paths[paths.length - 1]]: relatedId,
      }
    }
    const result = {} as Record<string, any>
    urlSearch.forEach((value, key) => {
      // skip required fields and related fields
      if (key === 'layoutId' || key === 'componentId' || key === 'relatedId' || key === 'relatedField') {
        return
      }
      result[key] = value
    })

    return result
  }, [relatedField, relatedId])

  const formMethods = useForm({ defaultValues })

  const queryClient = useQueryClient()

  const { mutate: getLayoutScript, data, isLoading, error } = useMutation(APIServices.Layout.triggers)

  const { mutate: createObject, isLoading: isCreateLoading } = useMutation(APIServices.Data.createObjectData, {
    onSuccess(data) {
      NToast.success({
        title: 'Created successful',
      })
      formMethods.reset()
      queryClient.invalidateQueries([DataQueryKey.getObjectData])
      queryClient.invalidateQueries([DataQueryKey.searchObjectData])
      history.replace(`/${data.data.objName}/${data.data.guid}`)
    },
    onError(e) {
      NToast.error({
        title: `Create unsuccessful`,
        subtitle: e.response?.data.message,
      })
    },
  })
  const { mutate: updateObject, isLoading: isUpdateLoading } = useMutation(APIServices.Data.updateObjectData, {
    onSuccess() {
      NToast.success({
        title: 'Updated successful',
      })
      queryClient.invalidateQueries([DataQueryKey.getSingleObjectData, { name: objectName, guid: guid }])
      history.replace(`/${objectName}/${guid}`)
    },
    onError(e) {
      NToast.error({
        title: `Update unsuccessful`,
        subtitle: e.response?.data.message,
      })
    },
  })

  React.useEffect(() => {
    if (layoutId && componentId) {
      if (guid) {
        getLayoutScript({
          layoutId,
          componentId,
          guid,
        })
        return
      }
      getLayoutScript({
        layoutId,
        componentId,
      })
    }
  }, [layoutId, componentId, getLayoutScript, guid])

  if (!layoutId || !componentId) {
    return (
      <ErrorContainer>
        <ErrorTitle>Error get form layout</ErrorTitle>
        <ErrorSubtitle>Missing required field!</ErrorSubtitle>
      </ErrorContainer>
    )
  }

  if (isLoading) {
    return (
      <ErrorContainer>
        <NSpinner size={16} strokeWidth={2} />
      </ErrorContainer>
    )
  }

  if (error && error.response) {
    return (
      <ErrorContainer>
        <ErrorTitle>Error get form layout</ErrorTitle>
        <ErrorSubtitle>Code {error.response.data.statusCode}</ErrorSubtitle>
        <ErrorSubtitle>{error.response.data.message}</ErrorSubtitle>
      </ErrorContainer>
    )
  }

  if (!data?.data) {
    return (
      <ErrorContainer>
        <ErrorTitle>Error get form layout</ErrorTitle>
        <ErrorSubtitle>Missing layout script</ErrorSubtitle>
      </ErrorContainer>
    )
  }

  const layoutType = guid ? LayoutType.Edit : LayoutType.Create
  const elements = (data.data as ObjectLayoutResponse).script as Record<string, Element>

  const formSubmit = (body: any) => {
    // metadata media map
    const media: Record<string, NFileType | undefined> = body?.$metadata?.media || {}
    const mediaKeys = Object.keys(media)
    const transformMedia = mediaKeys.reduce((prev, cur) => {
      const mediaFile = media[cur]
      if (!mediaFile) {
        return prev
      }
      return { ...prev, [mediaFile.id]: cur }
    }, {} as Record<string, string>)

    const transformBody = {
      ...body,
      $metadata: {
        ...body.$metadata,
        media: transformMedia,
      },
    }

    if (guid) {
      updateObject({
        name: objectName,
        guid,
        body: transformBody,
      })
      return
    }
    createObject({
      name: objectName,
      body: transformBody,
    })
  }

  return (
    <FormProvider {...formMethods}>
      <form onSubmit={formMethods.handleSubmit(formSubmit)}>
        <LayoutViewerProvider elements={elements} layoutId={layoutId} layoutType={layoutType} objectName={objectName}>
          <LayoutViewerRoot />
          <ActionsWrapper>
            <NButton
              onClick={() => {
                if (history.length > 1) {
                  history.goBack()
                  return
                }
                if (guid) {
                  history.push(`/${objectName}/${guid}`)
                  return
                }
                history.push(`/${objectName}`)
              }}>
              Cancel
            </NButton>
            <NButton htmlType="submit" type="primary" loading={isCreateLoading || isUpdateLoading}>
              {!guid ? 'Create new Record' : 'Update record'}
            </NButton>
          </ActionsWrapper>
        </LayoutViewerProvider>
      </form>
    </FormProvider>
  )
}
