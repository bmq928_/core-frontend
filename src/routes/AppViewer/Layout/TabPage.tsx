import React, { FC } from 'react'
import { useQuery } from 'react-query'
import { APIServices } from '../../../services/api'
import { AppQueryKey } from '../../../services/api/endpoints/app'
import { Element } from '../../LayoutBuilder/utils/models'
import { ErrorContainer, ErrorSubtitle, ErrorTitle } from '../components/ErrorComponents'
import { LoadingView } from '../components/LoadingView'
import { LayoutViewerProvider } from '../contexts/LayoutViewerContext'
import { LayoutType } from '../model'
import { LayoutViewerRoot } from './LayoutViewerRoot'

type TabPageProps = {
  layoutId: string
}

export const TabPage: FC<TabPageProps> = ({ layoutId }) => {
  // get tab page script
  const { data, isFetching } = useQuery([AppQueryKey.getAppScript, { layoutId }], APIServices.Apps.getAppScript)

  if (isFetching) {
    return <LoadingView />
  }

  if (!data || !data.data) {
    return (
      <ErrorContainer>
        <ErrorTitle>Get layout error!</ErrorTitle>
        <ErrorSubtitle>No script</ErrorSubtitle>
      </ErrorContainer>
    )
  }

  return (
    <LayoutViewerProvider
      elements={data.data as Record<string, Element>}
      layoutType={LayoutType.Tab}
      layoutId={layoutId}>
      <LayoutViewerRoot />
    </LayoutViewerProvider>
  )
}
