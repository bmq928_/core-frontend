import React from 'react'
import styled from 'styled-components'
import { spacing } from '../../../components/GlobalStyle'
import { ROOT_ELEMENT } from '../../LayoutBuilder/utils/builder'
import { useRenderView } from '../contexts/useRenderView'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;

  &.loading {
    flex: 1;
    justify-content: center;
    align-items: center;
  }

  & > .component-container:not(:last-child) {
    margin-bottom: ${spacing('md')};
  }

  .temp_input {
    margin-bottom: ${spacing('md')};
  }
`

type LayoutViewerRootProps = {}

export function LayoutViewerRoot({}: LayoutViewerRootProps) {
  const children = useRenderView(ROOT_ELEMENT)

  return <Wrapper>{children}</Wrapper>
}
