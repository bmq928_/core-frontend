import styled from 'styled-components'
import { color, spacing, ThemeColorType } from '../../../components/GlobalStyle'
import { typography } from '../../../components/NTypography'

export const FlowIconContainer = styled.div<{ color?: ThemeColorType }>`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 48px;
  height: 48px;
  border-radius: 8px;
  background-color: ${props => color(props.color || 'Neutral400')};
`

export const VarOptionContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding-right: ${spacing('sm')};
  padding-left: ${spacing('sm')};
  height: 100%;
`

export const Header = styled.p`
  color: ${color('Neutral400')};
  ${typography('overline')}
  text-transform: uppercase;
`
