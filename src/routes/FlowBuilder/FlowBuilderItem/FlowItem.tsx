import { FC } from 'react'
import { FlowType } from '../../../services/api/endpoints/flows'
import { FlowNodeModalProps, NodeType, WidgetItemType } from '../utils/types'
import { ActionBuilder, actionWidget } from './ActionBuilder/ActionBuilder'
import { AssignmentModal, assignmentWidget } from './FlowItemAssignment/AssignmentModal'
import { DataCreateModal, dataCreateWidget } from './FlowItemData/DataCreateItem'
import { DataDeleteModal, dataDeleteWidget } from './FlowItemData/DataDeleteItem'
import { DataGetModal, dataGetWidget } from './FlowItemData/DataGetItem'
import { DataUpdateModal, dataUpdateWidget } from './FlowItemData/DataUpdateItem'
import { DecisionFlowWidget, DecisionModal } from './FlowItemDecision/DecisionModal'
import { LoopBuilder, loopBuilderWidget } from './LoopBuilder/LoopBuilder'
import { ScreenBuilder, screenBuilderWidget } from './ScreenBuilder/ScreenBuilder'
import { SubFlowBuilderModal, subflowWidget } from './SubFlow/SubFlowBuilderModal'

export const leftSideWidgets: Record<string, WidgetItemType[]> = {
  Interaction: [screenBuilderWidget, actionWidget, subflowWidget],
  Logic: [assignmentWidget, DecisionFlowWidget, loopBuilderWidget],
  Data: [dataCreateWidget, dataUpdateWidget, dataGetWidget, dataDeleteWidget],
}

export type GroupName = 'Interaction' | 'Logic' | 'Data'
export function getWidgetByGroups(type?: FlowType): Record<GroupName, WidgetItemType[]> {
  if (type === 'screen') {
    return {
      Interaction: [screenBuilderWidget, actionWidget, subflowWidget],
      Logic: [assignmentWidget, DecisionFlowWidget, loopBuilderWidget],
      Data: [dataCreateWidget, dataUpdateWidget, dataGetWidget, dataDeleteWidget],
    }
  }
  return {
    Interaction: [actionWidget, subflowWidget],
    Logic: [assignmentWidget, DecisionFlowWidget, loopBuilderWidget],
    Data: [dataCreateWidget, dataUpdateWidget, dataGetWidget, dataDeleteWidget],
  }
}

export const WIDGET_FLOW_ITEMS: Record<NodeType, WidgetItemType> = {
  [NodeType.Screen]: screenBuilderWidget,
  [NodeType.Action]: actionWidget,
  [NodeType.Subflow]: subflowWidget,

  [NodeType.Loop]: loopBuilderWidget,
  [NodeType.Decision]: DecisionFlowWidget,
  [NodeType.Assignment]: assignmentWidget,

  [NodeType.DataCreate]: dataCreateWidget,
  [NodeType.DataUpdate]: dataUpdateWidget,
  [NodeType.DataGet]: dataGetWidget,
  [NodeType.DataDelete]: dataDeleteWidget,
}

export const FLOW_ITEM_BUILDER: Record<NodeType, FC<FlowNodeModalProps>> = {
  [NodeType.Screen]: ScreenBuilder,
  [NodeType.Action]: ActionBuilder,
  [NodeType.Subflow]: SubFlowBuilderModal,

  [NodeType.Loop]: LoopBuilder,
  [NodeType.Decision]: DecisionModal,
  [NodeType.Assignment]: AssignmentModal,

  [NodeType.DataCreate]: DataCreateModal,
  [NodeType.DataUpdate]: DataUpdateModal,
  [NodeType.DataGet]: DataGetModal,
  [NodeType.DataDelete]: DataDeleteModal,
}
