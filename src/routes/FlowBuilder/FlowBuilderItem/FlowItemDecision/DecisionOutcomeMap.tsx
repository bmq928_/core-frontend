import { nanoid } from 'nanoid'
import React, { FC } from 'react'
import { useFieldArray, UseFormReturn } from 'react-hook-form'
import styled, { useTheme } from 'styled-components'
import { classnames } from '../../../../common/classnames'
import { color, spacing } from '../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../components/Icons'
import { NButton } from '../../../../components/NButton/NButton'
import { NDivider } from '../../../../components/NDivider'
import { NRow } from '../../../../components/NGrid/NGrid'
import { NSortableList } from '../../../../components/NSortableList'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { typography } from '../../../../components/NTypography'
import { DecisionConditionType, DecisionOutcomeData } from '../../../../services/api/models'
import { DecisionFormType } from '../../models'
import { DecisionOutcomeItem } from './DecisionOutcomeItem'

/////////////////////
const OutcomesWrapper = styled('div')`
  display: flex;
`

const ListOutcome = styled('div')`
  width: 200px;
`

const SortableOutcome = styled(NSortableList)`
  max-height: 230px;
  overflow: auto;
`

const OutcomeItem = styled('div')`
  display: flex;
  align-items: center;
  min-height: 40px;
  line-height: 16px;
  padding: ${spacing('sm')} ${spacing('xl')};
  font-size: 14px;
  color: ${color('Neutral700')};
  cursor: pointer;
  margin-bottom: 1px;
  &:hover,
  &.is-selected {
    background: ${color('Primary100')};
  }
  &.is-dragging {
    opacity: 0.6;
  }
`

const ErrorText = styled.span`
  color: ${color('Red700')};
`

const Title = styled(OutcomeItem)`
  ${typography('overline')};
  color: ${color('Neutral400')};
  text-transform: uppercase;
  cursor: unset;
  &:hover {
    background: transparent;
  }
`

const TitleDetail = styled.p`
  ${typography('overline')};
  color: ${color('Neutral400')};
  text-transform: uppercase;
`

const AddOutcomeItem = styled(OutcomeItem)`
  color: ${color('Primary700')};
  &:hover {
    background: transparent;
  }
`

const OutcomeDetails = styled('div')`
  flex: 1;
`
const OutcomeDetailsContent = styled('div')`
  padding: ${spacing('xl')};
`

const OutcomeWrapper = styled.div`
  &.hidden {
    display: none;
    visibility: hidden;
  }
`

/////////////////////
const DEFAULT_OUTCOME_ID = 'DEFAULT_OUTCOME_ID'

const DEFAULT_OUTCOME: PartialDecisionOutcomeData = {
  name: '',
  displayName: '',
  conditionLogic: 'and',
  conditions: [],
}

/////////////////////
export type PartialDecisionOutcomeData = Omit<DecisionOutcomeData, 'conditions'> & {
  conditions: Partial<DecisionConditionType>[]
}

type Props = {
  formMethods: UseFormReturn<DecisionFormType>
}

export const DecisionOutcomeMap: FC<Props> = ({ formMethods }) => {
  const theme = useTheme()
  const { register, formState, watch, control } = formMethods

  const { fields: outcomes, remove: removeOutcome, append: addOutcome, swap } = useFieldArray({
    control,
    name: 'outcomes',
  })

  const [selectedOutcome, setSelectedOutcome] = React.useState<string>(outcomes[0]?.id || DEFAULT_OUTCOME_ID)

  const handleAddOutcome = () => {
    const outcomeId = nanoid()
    //ignore here to override id by hook-form -> can focus when created new
    //@ts-ignore
    addOutcome({ ...DEFAULT_OUTCOME, id: outcomeId })
    setSelectedOutcome(outcomeId)
  }

  const handleSort = (fromIndex: number, toIndex: number) => {
    swap(fromIndex, toIndex)
  }

  const isDefaultItem = selectedOutcome === DEFAULT_OUTCOME_ID

  return (
    <OutcomesWrapper>
      <ListOutcome>
        <Title>Outcomes</Title>
        <SortableOutcome onSort={handleSort}>
          {outcomes.map((outcome, index) => {
            return (
              <OutcomeItem
                key={outcome.id}
                className={classnames([outcome.id === selectedOutcome && 'is-selected'])}
                onClick={() => setSelectedOutcome(outcome.id)}>
                {watch(`outcomes.${index}.displayName`) || 'New Outcome'}
                {formState.errors.outcomes?.[index] && <ErrorText>!</ErrorText>}
              </OutcomeItem>
            )
          })}
        </SortableOutcome>
        <NDivider size={1} lineSize={1} lineColor={color('Neutral200')({ theme })} />
        <OutcomeItem
          className={classnames([DEFAULT_OUTCOME_ID === selectedOutcome && 'is-selected'])}
          onClick={() => setSelectedOutcome(DEFAULT_OUTCOME_ID)}>
          {watch('default_outcome_label')}
        </OutcomeItem>
        <AddOutcomeItem onClick={handleAddOutcome}>
          <GlobalIcons.Plus />
          Add new item
        </AddOutcomeItem>
      </ListOutcome>
      <NDivider vertical size={1} lineSize={1} lineColor={color('Neutral200')({ theme })} style={{ height: 'unset' }} />
      <OutcomeDetails>
        <OutcomeDetailsContent>
          {!isDefaultItem &&
            outcomes.map((outcome, index) => (
              <OutcomeWrapper key={outcome.id} className={classnames([outcome.id !== selectedOutcome && 'hidden'])}>
                <NRow justify="space-between" align="center">
                  <TitleDetail>Outcome Details</TitleDetail>
                  <NButton size="small" onClick={() => removeOutcome(index)}>
                    Delete
                  </NButton>
                </NRow>
                <NDivider size="md" />
                <DecisionOutcomeItem outcomeIndex={index} formMethods={formMethods} />
              </OutcomeWrapper>
            ))}
          {isDefaultItem && (
            <NTextInput
              label="Default Outcome Label"
              {...register('default_outcome_label', {
                required: {
                  value: true,
                  message: 'Required',
                },
              })}
              error={formState.errors.default_outcome_label?.message}
            />
          )}
        </OutcomeDetailsContent>
      </OutcomeDetails>
    </OutcomesWrapper>
  )
}
