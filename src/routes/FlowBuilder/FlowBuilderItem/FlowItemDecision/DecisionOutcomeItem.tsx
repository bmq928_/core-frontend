import React, { FC } from 'react'
import { Controller, useFieldArray, UseFormReturn } from 'react-hook-form'
import { useQuery } from 'react-query'
import styled from 'styled-components'
import { color, spacing } from '../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../components/Icons'
import { NButton } from '../../../../components/NButton/NButton'
import { NDivider } from '../../../../components/NDivider'
import { NColumn, NRow } from '../../../../components/NGrid/NGrid'
import { SelectOption } from '../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { APIServices } from '../../../../services/api'
import { FlowsQueryKey } from '../../../../services/api/endpoints/flows'
import { fieldNameRegex } from '../../../../utils/regex'
import { getNameFromDisplayName } from '../../../../utils/utils'
import { FlowVariablePicker } from '../../FlowBuilderVariable/FlowVariablePicker'
import { DecisionFormType } from '../../models'

const CONDITION_LOGICS = [
  { label: 'All conditions are met (AND)', value: 'and' },
  { label: 'Any conditions is met (OR)', value: 'or' },
  { label: 'Custom condition', value: 'custom' },
]

const ConditionWrapper = styled('div')`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  & > *:first-child {
    width: 100%;
  }

  .picker-row {
    flex: 1;
  }
  .field-picker {
    flex: 2;
  }
  .sign-picker {
    flex: 2;
  }
  .value-picker {
    flex: 3;
  }
`

const ConditionLogic = styled('div')<{ isLast?: boolean }>`
  border-radius: 3px;
  background: ${color('Primary700')};
  color: ${color('white')};
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 ${spacing('sm')};
  margin: 14px 0;
  margin-bottom: ${props => (props.isLast ? '0px' : '14px')};
  min-width: 85px;
  height: 24px;
  position: relative;
  text-transform: uppercase;
  font-weight: bold;
  &:before,
  &:after {
    position: absolute;
    content: '';
    width: 1px;
    height: 10px;
    background: ${color('Neutral200')};
    left: 50%;
  }
  &:before {
    bottom: calc(100% + 2px);
  }
  &:after {
    display: ${props => (props.isLast ? 'none' : 'block')};
    top: calc(100% + 2px);
  }
`

type Props = {
  outcomeIndex: number
  formMethods: UseFormReturn<DecisionFormType>
}

export const DecisionOutcomeItem: FC<Props> = ({ outcomeIndex, formMethods }) => {
  const { register, formState, control, getValues, setValue, watch, unregister } = formMethods
  const { fields: conditions, append: addCondition, remove: removeCondition } = useFieldArray({
    control,
    name: `outcomes.${outcomeIndex}.conditions`,
  })

  const watchConditionType = watch(`outcomes.${outcomeIndex}.conditionLogic`)

  const { data: conditionOperators = [], isLoading: isLoadingConditionOperators } = useQuery(
    [FlowsQueryKey.getConditionOperators],
    APIServices.Flows.getConditionOperators,
    {
      select: data => {
        return data.data.map(op => {
          return {
            value: op,
            label: op,
          } as SelectOption
        })
      },
    },
  )

  return (
    <React.Fragment>
      <NRow>
        <NColumn flex={1}>
          <NTextInput
            required
            label="Display Name"
            {...register(`outcomes.${outcomeIndex}.displayName`, {
              required: {
                value: true,
                message: 'Required',
              },
            })}
            error={formState.errors.outcomes?.[outcomeIndex]?.displayName?.message}
            onBlur={e => {
              if (!getValues(`outcomes.${outcomeIndex}.name`)) {
                setValue(`outcomes.${outcomeIndex}.name`, getNameFromDisplayName(e.target.value))
              }
            }}
          />
        </NColumn>
        <NDivider vertical size="xl" />
        <NColumn flex={1}>
          <NTextInput
            required
            label="Outcome Name"
            error={formState.errors.outcomes?.[outcomeIndex]?.name?.message}
            {...register(`outcomes.${outcomeIndex}.name`, {
              required: {
                value: true,
                message: 'Required',
              },
              pattern: {
                value: fieldNameRegex,
                message: 'Invalid pattern',
              },
            })}
          />
        </NColumn>
      </NRow>
      <NDivider size="xl" />
      <Controller
        control={control}
        name={`outcomes.${outcomeIndex}.conditionLogic`}
        render={({ field: { value, onChange } }) => (
          <NSingleSelect
            label="Condition Requirements to Execute Outcome"
            options={CONDITION_LOGICS}
            value={value}
            onValueChange={value => {
              if (value !== 'custom') {
                setValue(`outcomes.${outcomeIndex}.customConditionLogic`, undefined)

                unregister(`outcomes.${outcomeIndex}.customConditionLogic`)
              }
              onChange(value)
            }}
          />
        )}
      />

      {watchConditionType === 'custom' && (
        <React.Fragment>
          <NDivider size="sm" />
          <NTextInput
            {...register(`outcomes.${outcomeIndex}.customConditionLogic`)}
            label="Custom Condition"
            caption="Use AND, OR and parentheses to customize the logic. For example, (1 AND 2 AND 3) OR 4"
          />
        </React.Fragment>
      )}
      <NDivider size="xl" />
      {conditions.map((condition, conditionIndex, arr) => {
        const watchConditionField = watch(`outcomes.${outcomeIndex}.conditions.${conditionIndex}.field`)
        return (
          <ConditionWrapper key={conditionIndex}>
            <NRow align="center">
              <NRow className="picker-row">
                <Controller
                  name={`outcomes.${outcomeIndex}.conditions.${conditionIndex}.field`}
                  rules={{
                    required: {
                      value: true,
                      message: 'Required',
                    },
                  }}
                  defaultValue={condition.field}
                  control={control}
                  render={({ field: { value, onChange }, fieldState }) => {
                    return (
                      <FlowVariablePicker
                        className="field-picker"
                        label="Select a variable"
                        placeholder="Select a variable"
                        noEnterValue
                        error={fieldState.error?.message}
                        selectedVariable={value}
                        onVariableSelected={onChange}
                      />
                    )
                  }}
                />

                <NDivider vertical size="xl" />
                <Controller
                  name={`outcomes.${outcomeIndex}.conditions.${conditionIndex}.sign`}
                  rules={{
                    required: {
                      value: true,
                      message: 'Required',
                    },
                  }}
                  defaultValue={condition.sign}
                  control={control}
                  render={({ field: { value, onChange }, fieldState }) => {
                    return (
                      <NSingleSelect
                        disabled={!watchConditionField}
                        containerClassName="sign-picker"
                        isLoading={isLoadingConditionOperators}
                        options={conditionOperators}
                        error={fieldState.error?.message}
                        value={value}
                        onValueChange={onChange}
                      />
                    )
                  }}
                />

                <NDivider vertical size="xl" />

                <Controller
                  name={`outcomes.${outcomeIndex}.conditions.${conditionIndex}.value`}
                  control={control}
                  defaultValue={condition.value}
                  render={({ field: { value, onChange } }) => {
                    return (
                      <FlowVariablePicker
                        disabled={!watchConditionField}
                        className="value-picker"
                        selectedVariable={value}
                        onVariableSelected={onChange}
                      />
                    )
                  }}
                />
              </NRow>
              <NDivider vertical size="sm" />
              <NButton type="ghost" icon={<GlobalIcons.Trash />} onClick={() => removeCondition(conditionIndex)} />
            </NRow>
            {conditionIndex + 1 < arr.length && (
              <ConditionLogic>{watch(`outcomes.${outcomeIndex}.conditionLogic`)}</ConditionLogic>
            )}
          </ConditionWrapper>
        )
      })}
      <ConditionLogic
        onClick={() => addCondition({ conditionNumber: conditions.length + 1 })}
        isLast
        style={{ width: 85, cursor: 'pointer' }}>
        <GlobalIcons.Plus />
      </ConditionLogic>
    </React.Fragment>
  )
}
