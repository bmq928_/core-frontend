import { omit } from 'lodash'
import * as React from 'react'
import { useForm } from 'react-hook-form'
import styled, { useTheme } from 'styled-components'
import { color, spacing } from '../../../../components/GlobalStyle'
import { NDivider } from '../../../../components/NDivider'
import { NModal } from '../../../../components/NModal/NModal'
import { NPerfectScrollbar } from '../../../../components/NPerfectScrollbar'
import { NTextArea } from '../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { fieldNameRegex } from '../../../../utils/regex'
import { getNameFromDisplayName } from '../../../../utils/utils'
import { FlowIcons } from '../../components/Icons'
import { DecisionFormType } from '../../models'
import { FlowIconContainerClassName } from '../../utils/constants'
import { FlowNodeModalProps, NodeType, WidgetItemType } from '../../utils/types'
import { FlowIconContainer } from '../CommonStyledComponents'
import { DecisionOutcomeMap, PartialDecisionOutcomeData } from './DecisionOutcomeMap'

const ModalBody = styled(NModal.Body)`
  max-height: calc(100vh - 300px);
  position: relative;
`

const MetadataWrapper = styled('div')`
  padding: ${spacing('xl')};
  display: grid;
  grid-template: repeat(2, auto) / repeat(2, 1fr);
  grid-gap: ${spacing('xl')};
  & > *:last-child {
    grid-column: span 2;
  }
`

export function DecisionModal({ isSubmitting, nodeModal, onCancel, onSubmit }: FlowNodeModalProps) {
  const theme = useTheme()

  const formMethods = useForm<DecisionFormType>({
    defaultValues: {
      name: nodeModal.data?.name || '',
      displayName: nodeModal.data?.displayName || '',
      description: nodeModal.data?.description || '',
      default_outcome_label: nodeModal.data?.default_outcome_label || 'Default Outcome',
      outcomes: nodeModal.data?.outcomes ? Object.values(nodeModal.data?.outcomes) : [],
    },
    shouldUnregister: false,
  })

  const { register, getValues, setValue, formState, handleSubmit } = formMethods

  const submitForm = (data: DecisionFormType) => {
    const { orderData, outcomesData } = data.outcomes.reduce(
      (a, item) => {
        a.orderData.push(item.name)
        a.outcomesData[item.name] = omit(item, 'id') //remove id from fieldArray
        return a
      },
      {
        orderData: [] as string[],
        outcomesData: {} as Record<string, PartialDecisionOutcomeData>,
      },
    )
    onSubmit({ ...data, outcomes: outcomesData, order: orderData })
  }

  return (
    <React.Fragment>
      <NModal.Header onClose={onCancel} title={`${nodeModal.nodeId ? 'Edit' : 'New'} Decision`} />
      <NPerfectScrollbar>
        <ModalBody>
          <MetadataWrapper>
            <NTextInput
              required
              placeholder="Display Name"
              label="Display Name"
              {...register('displayName', {
                required: {
                  value: true,
                  message: 'Required',
                },
              })}
              error={formState.errors.displayName?.message}
              onBlur={e => {
                if (!!!nodeModal.nodeId) {
                  const formatName = getNameFromDisplayName(e.target.value)
                  !getValues('name') && setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
                }
              }}
            />
            <NTextInput
              required
              disabled={!!nodeModal.nodeId}
              placeholder="API Name"
              label="API Name"
              error={formState.errors.name?.message}
              {...register('name', {
                required: {
                  value: true,
                  message: 'Required',
                },
                pattern: {
                  value: fieldNameRegex,
                  message: 'Invalid pattern',
                },
              })}
            />
            <NTextArea rows={4} label="Description" {...register('description')} />
          </MetadataWrapper>
          <NDivider size={1} lineSize={1} lineColor={color('Neutral200')({ theme })} />
          <DecisionOutcomeMap formMethods={formMethods} />
        </ModalBody>
      </NPerfectScrollbar>

      <NModal.Footer isLoading={isSubmitting} onCancel={onCancel} onFinish={handleSubmit(submitForm)} />
    </React.Fragment>
  )
}

export const DecisionFlowWidget: WidgetItemType<DecisionFormType> = {
  leftIcon: <FlowIcons.Decision />,
  flowIcon: (
    <FlowIconContainer className={FlowIconContainerClassName} color="Secondary900">
      <FlowIcons.Decision />
    </FlowIconContainer>
  ),
  label: 'Decision',
  type: NodeType.Decision,
  data: {
    name: '',
    displayName: '',
    description: '',
    outcomes: [],
    default_outcome_label: '',
  },
}
