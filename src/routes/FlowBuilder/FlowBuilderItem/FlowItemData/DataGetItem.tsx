import React, { FC } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useQuery } from 'react-query'
import styled, { useTheme } from 'styled-components'
import { color, spacing } from '../../../../components/GlobalStyle'
import { NDivider } from '../../../../components/NDivider'
import { NColumn, NRow } from '../../../../components/NGrid/NGrid'
import { NModal } from '../../../../components/NModal/NModal'
import { NRadio } from '../../../../components/NRadio/NRadio'
import { SelectOption } from '../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { NTextArea } from '../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { RequiredIndicator, typography } from '../../../../components/NTypography'
import { useSearchText } from '../../../../hooks/useSearchText'
import { useValidateString } from '../../../../hooks/useValidateString'
import { APIServices } from '../../../../services/api'
import { BuilderQueryKey } from '../../../../services/api/endpoints/builder'
import { FieldSchema } from '../../../../services/api/models'
import { fieldNameRegex } from '../../../../utils/regex'
import { getNameFromDisplayName } from '../../../../utils/utils'
import { FlowIcons } from '../../components/Icons'
import { DataGetFormType } from '../../models'
import { FlowIconContainerClassName } from '../../utils/constants'
import { FlowNodeModalProps, NodeType, WidgetItemType } from '../../utils/types'
import { FlowIconContainer } from '../CommonStyledComponents'
import { Header, ScrollableWrapper, Section } from './components/CommonComponents'
import { FilterRecordRules } from './components/FilterRecordRules'

const SubHeader = styled.p`
  ${typography('small-bold-text')}
  color: ${color('Neutral400')};
  margin-bottom: ${spacing('md')};
`
const ErrorText = styled.p`
  color: ${color('Red700')};
  margin-top: ${spacing('xs')};
`

type ObjectSchemaType = { fieldOptions: SelectOption[]; fieldsByName: Record<string, FieldSchema> }

export const DataGetModal: FC<FlowNodeModalProps> = ({ nodeModal, isSubmitting, onSubmit, onCancel }) => {
  const formMethods = useForm<DataGetFormType>({
    defaultValues: nodeModal.data || {
      condition: 'and',
      config: [{ fieldName: '', value: '' }],
    },
  })
  const { handleSubmit, formState, setValue, register, control, watch, getValues } = formMethods

  const theme = useTheme()
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText('')

  const watchObjectName = watch('object_name')
  const { validateFunction } = useValidateString()

  // get all objects
  const { data: objectOptions, isFetching: isLoadingObjects } = useQuery(
    [BuilderQueryKey.getAllObjects, { searchText }],
    APIServices.Builder.getAllObjects,
    {
      keepPreviousData: true,
      select: data => {
        return data.data.data.map(option => {
          const typeName = option.isExternal ? ' (external)' : ' (internal)'
          return {
            value: option.name,
            label: option.displayName + typeName,
          }
        }) as SelectOption[]
      },
    },
  )

  // get object fields schema
  const {
    data: { fieldOptions } = { fieldOptions: [], fieldsByName: {} } as ObjectSchemaType,
    isLoading: isLoadingFields,
  } = useQuery([BuilderQueryKey.getObjectSchema, { objName: watchObjectName }], APIServices.Builder.getObjectSchema, {
    enabled: !!watchObjectName,
    select: res => {
      const { fieldOptions, fieldsByName } = res.data.fields.reduce(
        (allFields, field) => {
          return {
            fieldOptions: [...allFields.fieldOptions, { value: field.name, label: field.displayName }],
            fieldsByName: { ...allFields.fieldsByName, [field.name]: field },
          }
        },
        { fieldOptions: [], fieldsByName: {} } as {
          fieldOptions: SelectOption[]
          fieldsByName: Record<string, FieldSchema>
        },
      )
      return { fieldOptions, fieldsByName, actions: [] }
    },
  })

  const submitForm = (data: any) => {
    onSubmit(data)
  }

  return (
    <>
      <NModal.Header title={`${!!nodeModal.nodeId ? 'Edit' : 'New'} get record`} onClose={onCancel} />
      <NModal.Body>
        <ScrollableWrapper>
          <Section>
            <NRow>
              <NColumn flex={1}>
                <NTextInput
                  {...register('displayName', {
                    validate: validateFunction,
                  })}
                  required
                  error={formState.errors.displayName?.message}
                  placeholder="Display name"
                  label="Display Name"
                  onBlur={e => {
                    if (!!nodeModal.nodeId) {
                      return
                    }
                    const formatName = getNameFromDisplayName(e.target.value)
                    !getValues('name') && setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
                  }}
                />
              </NColumn>
              <NDivider vertical size="xl" />
              <NColumn flex={1}>
                <NTextInput
                  disabled={!!nodeModal.nodeId}
                  {...register('name', {
                    validate: validateFunction,
                    pattern: {
                      value: fieldNameRegex,
                      message: 'Invalid pattern',
                    },
                  })}
                  required
                  error={formState.errors.name?.message}
                  placeholder="API name"
                  label="API Name"
                />
              </NColumn>
            </NRow>
            <NDivider size="xl" />
            <NRow>
              <NColumn flex={1}>
                <NTextArea rows={3} {...register('description')} label="Description" />
              </NColumn>
            </NRow>
          </Section>
          <NDivider lineColor={color('Neutral200')({ theme })} />
          <Section>
            <Header>
              <RequiredIndicator>*</RequiredIndicator>Get record from this Object
            </Header>
            <NRow>
              <NColumn flex={0.5}>
                <Controller
                  name="object_name"
                  rules={{
                    required: {
                      value: true,
                      message: 'Required',
                    },
                  }}
                  control={control}
                  render={({ field, fieldState }) => (
                    <NSingleSelect
                      required
                      options={objectOptions || []}
                      searchValue={searchValue}
                      onSearchValueChange={handleSearchChange}
                      isLoading={isLoadingObjects}
                      isSearchable
                      fullWidth
                      placeholder="Select object"
                      error={fieldState.error?.message}
                      value={field.value}
                      onValueChange={value => {
                        setValue('config', [{ fieldName: '', value: '', sign: '' }])
                        field.onChange(value)
                      }}
                    />
                  )}
                />
              </NColumn>
            </NRow>
          </Section>
          {watchObjectName && (
            <>
              <NDivider lineColor={color('Neutral200')({ theme })} />
              <FilterRecordRules formMethods={formMethods} fieldOptions={fieldOptions} required />
              <NDivider lineColor={color('Neutral200')({ theme })} />
              <Section>
                <Header>Sort record</Header>
                <NRow>
                  <Controller
                    name="sortOrder"
                    control={control}
                    render={({ field, fieldState }) => (
                      <NSingleSelect
                        options={[{ value: 'DESC' }, { value: 'ASC' }]}
                        placeholder="Select order"
                        value={field.value}
                        onValueChange={field.onChange}
                        label="Sort Order"
                        error={fieldState.error?.message}
                      />
                    )}
                    rules={{ required: { value: true, message: 'Required' } }}
                  />
                  <NDivider vertical size="sm" />
                  <Controller
                    name="sortBy"
                    control={control}
                    render={({ field, fieldState }) => (
                      <NSingleSelect
                        isLoading={isLoadingFields}
                        options={fieldOptions}
                        placeholder="Select field"
                        value={field.value}
                        onValueChange={value => {
                          field.onChange(value)
                        }}
                        label="Sort By"
                        error={fieldState.error?.message}
                      />
                    )}
                    rules={{ required: { value: true, message: 'Required' } }}
                  />
                </NRow>
                <NDivider size="sm" />
                <SubHeader>How Many Records to Store</SubHeader>
                <Controller
                  control={control}
                  name="getOptions"
                  render={({ field, fieldState }) => {
                    return (
                      <>
                        <NRadio
                          name="getOptions"
                          value="one"
                          checked={field.value === 'one'}
                          onChange={e => field.onChange(e.target.value)}
                          title="Only the first record"
                        />
                        <NDivider size="sm" />
                        <NRadio
                          name="getOptions"
                          value="all"
                          onChange={e => field.onChange(e.target.value)}
                          checked={field.value === 'all'}
                          title="All records"
                        />
                        {fieldState.error?.message && <ErrorText>{fieldState.error.message}</ErrorText>}
                      </>
                    )
                  }}
                  rules={{ required: { value: true, message: 'Required' } }}
                />
              </Section>
            </>
          )}
        </ScrollableWrapper>
      </NModal.Body>
      <NModal.Footer isLoading={isSubmitting} onCancel={onCancel} onFinish={handleSubmit(submitForm)} />
    </>
  )
}

export const dataGetWidget: WidgetItemType = {
  leftIcon: <FlowIcons.DataGet />,
  flowIcon: (
    <FlowIconContainer className={FlowIconContainerClassName} color="Tertiary900">
      <FlowIcons.DataGet />
    </FlowIconContainer>
  ),
  label: 'Get Record',
  type: NodeType.DataGet,
  data: {
    name: '',
    displayName: '',
    condition: 'and',
    config: [{ fieldName: '', value: '' }],
    object_name: '',
    sortOrder: 'DESC',
  },
}
