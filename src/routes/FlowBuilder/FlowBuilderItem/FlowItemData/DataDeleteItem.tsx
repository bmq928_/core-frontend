import React, { FC } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useQuery } from 'react-query'
import { useTheme } from 'styled-components'
import { color } from '../../../../components/GlobalStyle'
import { NDivider } from '../../../../components/NDivider'
import { NColumn, NRow } from '../../../../components/NGrid/NGrid'
import { NModal } from '../../../../components/NModal/NModal'
import { SelectOption } from '../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { NTextArea } from '../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { RequiredIndicator } from '../../../../components/NTypography'
import { useSearchText } from '../../../../hooks/useSearchText'
import { useValidateString } from '../../../../hooks/useValidateString'
import { APIServices } from '../../../../services/api'
import { BuilderQueryKey } from '../../../../services/api/endpoints/builder'
import { fieldNameRegex } from '../../../../utils/regex'
import { getNameFromDisplayName } from '../../../../utils/utils'
import { FlowIcons } from '../../components/Icons'
import { FlowVariablePicker } from '../../FlowBuilderVariable/FlowVariablePicker'
import { FlowIconContainerClassName } from '../../utils/constants'
import { FlowNodeModalProps, NodeType, WidgetItemType } from '../../utils/types'
import { FlowIconContainer } from '../CommonStyledComponents'
import { Header, Section } from './components/CommonComponents'

export const DataDeleteModal: FC<FlowNodeModalProps> = ({ nodeModal, isSubmitting, onCancel, onSubmit }) => {
  const { handleSubmit, formState, setValue, register, control, getValues } = useForm({
    defaultValues: nodeModal.data || {
      name: '',
      displayName: '',
      object_name: '',
      guid: '',
    },
  })

  const theme = useTheme()
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText('')
  const { validateFunction } = useValidateString()

  // get all objects
  const { data: objectOptions, isFetching: isLoadingObjects } = useQuery(
    [BuilderQueryKey.getAllObjects, { searchText }],
    APIServices.Builder.getAllObjects,
    {
      keepPreviousData: true,
      select: data => {
        return data.data.data.map(option => {
          const typeName = option.isExternal ? ' (external)' : ' (internal)'
          return {
            value: option.name,
            label: option.displayName + typeName,
          }
        }) as SelectOption[]
      },
    },
  )

  const submitForm = (data: any) => {
    onSubmit(data)
  }

  return (
    <>
      <NModal.Header title={`${!!nodeModal.nodeId ? 'Edit' : 'New'} delete record`} onClose={onCancel} />
      <NModal.Body>
        <Section>
          <NRow>
            <NColumn flex={1}>
              <NTextInput
                {...register('displayName', {
                  validate: validateFunction,
                })}
                required
                error={formState.errors.displayName?.message}
                placeholder="Display name"
                label="Display Name"
                onBlur={e => {
                  if (!!nodeModal.nodeId) {
                    return
                  }
                  const formatName = getNameFromDisplayName(e.target.value)
                  !getValues('name') && setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
                }}
              />
            </NColumn>
            <NDivider vertical size="xl" />
            <NColumn flex={1}>
              <NTextInput
                disabled={!!nodeModal.nodeId}
                {...register('name', {
                  validate: validateFunction,
                  pattern: {
                    value: fieldNameRegex,
                    message: 'Invalid pattern',
                  },
                })}
                required
                error={formState.errors.name?.message}
                name="name"
                placeholder="API name"
                label="API Name"
              />
            </NColumn>
          </NRow>
          <NDivider size="xl" />
          <NRow>
            <NColumn flex={1}>
              <NTextArea rows={3} {...register('description')} label="Description" />
            </NColumn>
          </NRow>
        </Section>
        <NDivider lineColor={color('Neutral200')({ theme })} />
        <Section>
          <Header>
            <RequiredIndicator>*</RequiredIndicator>Delete a Record from this Object
          </Header>
          <NRow>
            <NColumn flex={1}>
              <Controller
                name="object_name"
                rules={{
                  required: {
                    value: true,
                    message: 'Required',
                  },
                }}
                control={control}
                render={({ field, fieldState }) => (
                  <NSingleSelect
                    required
                    options={objectOptions || []}
                    searchValue={searchValue}
                    onSearchValueChange={handleSearchChange}
                    isLoading={isLoadingObjects}
                    isSearchable
                    fullWidth
                    placeholder="Select object"
                    error={fieldState.error?.message}
                    value={field.value}
                    onValueChange={field.onChange}
                  />
                )}
              />
            </NColumn>
            <NDivider vertical size="xl" />
            <NColumn flex={1}>
              <Controller
                name="guid"
                rules={{
                  required: {
                    value: true,
                    message: 'Required',
                  },
                }}
                control={control}
                render={({ field, fieldState }) => (
                  <FlowVariablePicker
                    label="Guid"
                    className="Picker"
                    placeholder="Pick guid"
                    error={fieldState.error?.message}
                    selectedVariable={field.value}
                    onVariableSelected={field.onChange}
                  />
                )}
              />
            </NColumn>
          </NRow>
        </Section>
      </NModal.Body>
      <NModal.Footer isLoading={isSubmitting} onCancel={onCancel} onFinish={handleSubmit(submitForm)} />
    </>
  )
}

export const dataDeleteWidget: WidgetItemType = {
  leftIcon: <FlowIcons.DataDelete />,
  flowIcon: (
    <FlowIconContainer className={FlowIconContainerClassName} color="Tertiary900">
      <FlowIcons.DataDelete />
    </FlowIconContainer>
  ),
  label: 'Delete Record',
  type: NodeType.DataDelete,
  data: {
    name: '',
    displayName: '',
    object_name: '',
    guid: '',
  },
}
