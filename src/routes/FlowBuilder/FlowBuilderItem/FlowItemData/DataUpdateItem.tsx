import React, { FC } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useQuery } from 'react-query'
import { useTheme } from 'styled-components'
import { color } from '../../../../components/GlobalStyle'
import { NDivider } from '../../../../components/NDivider'
import { NColumn, NRow } from '../../../../components/NGrid/NGrid'
import { NModal } from '../../../../components/NModal/NModal'
import { SelectOption } from '../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { NTextArea } from '../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { RequiredIndicator } from '../../../../components/NTypography'
import { useSearchText } from '../../../../hooks/useSearchText'
import { useValidateString } from '../../../../hooks/useValidateString'
import { APIServices } from '../../../../services/api'
import { BuilderQueryKey } from '../../../../services/api/endpoints/builder'
import { fieldNameRegex } from '../../../../utils/regex'
import { getNameFromDisplayName } from '../../../../utils/utils'
import { FlowIcons } from '../../components/Icons'
import { FlowVariablePicker } from '../../FlowBuilderVariable/FlowVariablePicker'
import { FlowIconContainerClassName } from '../../utils/constants'
import { FlowNodeModalProps, NodeType, WidgetItemType } from '../../utils/types'
import { FlowIconContainer } from '../CommonStyledComponents'
import { AssignValueForm, FormType } from './components/AssignValueForm'
import { Header, ScrollableWrapper, Section } from './components/CommonComponents'

export const DataUpdateModal: FC<FlowNodeModalProps> = ({ nodeModal, isSubmitting, onCancel, onSubmit }) => {
  const formMethods = useForm<FormType>({
    defaultValues: (nodeModal.data as FormType) || {
      config: [{ fieldName: '', value: '' }],
    },
  })
  const { register, formState, setValue, control, handleSubmit, watch, getValues } = formMethods

  const theme = useTheme()
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText('')
  const watchObjectName = watch('object_name')
  const { validateFunction } = useValidateString()

  // get all objects
  const { data: objectOptions, isFetching: isLoadingObjects } = useQuery(
    [BuilderQueryKey.getAllObjects, { searchText }],
    APIServices.Builder.getAllObjects,
    {
      keepPreviousData: true,
      select: data => {
        return data.data.data.map(option => {
          const typeName = option.isExternal ? ' (external)' : ' (internal)'
          return {
            value: option.name,
            label: option.displayName + typeName,
          }
        }) as SelectOption[]
      },
    },
  )

  const submitForm = (data: any) => {
    onSubmit(data)
  }

  return (
    <>
      <NModal.Header title={`${!!nodeModal.nodeId ? 'Edit' : 'New'} update record`} onClose={onCancel} />
      <NModal.Body>
        <ScrollableWrapper>
          <Section>
            <NRow>
              <NColumn flex={1}>
                <NTextInput
                  {...register('displayName', {
                    validate: validateFunction,
                  })}
                  required
                  error={formState.errors.displayName?.message || formState.errors.displayName?.type}
                  placeholder="Display name"
                  label="Display Name"
                  onBlur={e => {
                    if (!!nodeModal.nodeId) {
                      return
                    }
                    const formatName = getNameFromDisplayName(e.target.value)
                    !getValues('name') && setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
                  }}
                />
              </NColumn>
              <NDivider vertical size="xl" />
              <NColumn flex={1}>
                <NTextInput
                  disabled={!!nodeModal.nodeId}
                  {...register('name', {
                    validate: validateFunction,
                    pattern: {
                      value: fieldNameRegex,
                      message: 'Invalid pattern',
                    },
                  })}
                  required
                  error={formState.errors.name?.message}
                  placeholder="API name"
                  label="API Name"
                />
              </NColumn>
            </NRow>
            <NDivider size="xl" />
            <NRow>
              <NColumn flex={1}>
                <NTextArea rows={3} {...register('description')} name="description" label="Description" />
              </NColumn>
            </NRow>
          </Section>
          <NDivider lineColor={color('Neutral200')({ theme })} />
          <Section>
            <Header>
              <RequiredIndicator>*</RequiredIndicator>Update a Record from this Object
            </Header>
            <NRow>
              <NColumn flex={1}>
                <Controller
                  name="object_name"
                  rules={{
                    required: {
                      value: true,
                      message: 'Required',
                    },
                  }}
                  control={control}
                  render={({ field, fieldState }) => {
                    return (
                      <NSingleSelect
                        required
                        options={objectOptions || []}
                        searchValue={searchValue}
                        onSearchValueChange={handleSearchChange}
                        isLoading={isLoadingObjects}
                        isSearchable
                        fullWidth
                        placeholder="Select object"
                        error={fieldState.error?.message}
                        value={field.value}
                        onValueChange={value => {
                          setValue('config', [{ fieldName: '', value: { type: 'value', value: '' } }])
                          field.onChange(value)
                        }}
                      />
                    )
                  }}
                />
              </NColumn>
              <NDivider vertical size="xl" />
              <NColumn flex={1}>
                <Controller
                  name="guid"
                  rules={{
                    required: {
                      value: true,
                      message: 'Required',
                    },
                  }}
                  control={control}
                  render={({ field, fieldState }) => {
                    return (
                      <FlowVariablePicker
                        label="Guid"
                        className="Picker"
                        placeholder="Pick guid"
                        error={fieldState.error?.message}
                        selectedVariable={field.value}
                        onVariableSelected={varOption => {
                          field.onChange(varOption)
                        }}
                      />
                    )
                  }}
                />
              </NColumn>
            </NRow>
          </Section>
          {watchObjectName && (
            <Section>
              <Header>Set Field Values for this Object</Header>
              <AssignValueForm formMethods={formMethods} objectName={watchObjectName} />
            </Section>
          )}
        </ScrollableWrapper>
      </NModal.Body>
      <NModal.Footer isLoading={isSubmitting} onCancel={onCancel} onFinish={handleSubmit(submitForm)} />
    </>
  )
}

export const dataUpdateWidget: WidgetItemType = {
  leftIcon: <FlowIcons.DataUpdate />,
  flowIcon: (
    <FlowIconContainer className={FlowIconContainerClassName} color="Tertiary900">
      <FlowIcons.DataUpdate />
    </FlowIconContainer>
  ),
  label: 'Update Record',
  type: NodeType.DataUpdate,
  data: {
    name: '',
    displayName: '',
    object_name: '',
    guid: '',
    config: [{ fieldName: '', value: '' }],
  },
}
