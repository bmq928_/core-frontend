import React, { FC } from 'react'
import { Controller, useFieldArray, UseFormReturn } from 'react-hook-form'
import { useQuery } from 'react-query'
import styled, { useTheme } from 'styled-components'
import { color, spacing } from '../../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../../components/Icons'
import { NButton } from '../../../../../components/NButton/NButton'
import { NRow } from '../../../../../components/NGrid/NGrid'
import { SelectOption } from '../../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../../components/NSelect/NSingleSelect'
import { NSpinner } from '../../../../../components/NSpinner/NSpinner'
import { APIServices } from '../../../../../services/api'
import { BuilderQueryKey } from '../../../../../services/api/endpoints/builder'
import { FieldSchema } from '../../../../../services/api/models'
import { FlowVariablePicker } from '../../../FlowBuilderVariable/FlowVariablePicker'
import { CommonWidgetDataType, DataFlowItemConfigType } from '../../../utils/types'
import { AddBtn } from './AddBtn'
import { AssignElement } from './CommonComponents'

const LoadingContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 44px;
  justify-content: center;
  align-items: center;
`

const ErrorContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 44px;
`

const ArrowWrapper = styled.div`
  margin-left: ${spacing('xl')};
  margin-right: ${spacing('xl')};
  align-self: center;
`

export type FormType = {
  config: DataFlowItemConfigType[]
} & CommonWidgetDataType

type Props = {
  objectName: string
  formMethods: UseFormReturn<FormType>
}

type ObjectSchemaType = {
  fieldOptions: SelectOption[]
  fieldsByName: Record<string, FieldSchema>
}

export const AssignValueForm: FC<Props> = ({ formMethods, objectName }) => {
  const theme = useTheme()
  const { control, watch } = formMethods

  // get object fields schema
  const {
    data: { fieldOptions, fieldsByName } = { fieldOptions: [], fieldsByName: {} } as ObjectSchemaType,
    isLoading,
  } = useQuery([BuilderQueryKey.getObjectSchema, { objName: objectName }], APIServices.Builder.getObjectSchema, {
    select: data => {
      const { fieldOptions, fieldsByName } = data.data.fields.reduce(
        (allFields, field) => {
          let newFieldOptions = field.isReadOnly
            ? allFields.fieldOptions
            : [...allFields.fieldOptions, { value: field.name, label: field.displayName }]
          return {
            fieldOptions: newFieldOptions,
            fieldsByName: { ...allFields.fieldsByName, [field.name]: field },
          }
        },
        { fieldOptions: [], fieldsByName: {} } as {
          fieldOptions: SelectOption[]
          fieldsByName: Record<string, FieldSchema>
        },
      )
      return { fieldOptions, fieldsByName, actions: [] }
    },
  })

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'config',
  })

  const watchConfig = watch('config') as DataFlowItemConfigType[]

  if (isLoading) {
    return (
      <LoadingContainer>
        <NSpinner size={22} strokeWidth={2} color={color('Primary700')({ theme })} />
      </LoadingContainer>
    )
  }

  if (!fieldOptions || !fieldsByName || fieldOptions.length === 0) {
    return <ErrorContainer>Object '{objectName}' doesn't have any fields</ErrorContainer>
  }

  return (
    <>
      {fields.map((field, index) => {
        return (
          <AssignElement key={field.id}>
            <NRow className="picker-container">
              <Controller
                name={`config.${index}.fieldName`}
                defaultValue={field.fieldName || ''}
                rules={{
                  required: {
                    value: true,
                    message: 'Required',
                  },
                }}
                control={control}
                render={({ field: { value, onChange }, fieldState }) => {
                  let options: SelectOption[] = []
                  if (fieldOptions) {
                    options = fieldOptions.filter(option => {
                      return option.value === value || !watchConfig.some(f => f.fieldName === option.value)
                    })
                  }

                  return (
                    <NSingleSelect
                      required
                      containerClassName="picker"
                      options={options}
                      placeholder="Select field"
                      error={fieldState.error?.message}
                      value={value}
                      onValueChange={onChange}
                    />
                  )
                }}
              />
              <ArrowWrapper>
                <GlobalIcons.LeftArrow />
              </ArrowWrapper>
              <Controller
                name={`config.${index}.value`}
                defaultValue={field.value}
                control={control}
                rules={{
                  required: {
                    value: true,
                    message: 'Required',
                  },
                }}
                render={({ field: { value, onChange }, fieldState }) => {
                  return (
                    <FlowVariablePicker
                      label="Field value"
                      className="picker"
                      error={fieldState.error?.message}
                      selectedVariable={value}
                      onVariableSelected={onChange}
                    />
                  )
                }}
              />
            </NRow>
            <NButton
              type="ghost"
              icon={<GlobalIcons.Trash />}
              disabled={fields.length < 1}
              onClick={() => remove(index)}
            />
          </AssignElement>
        )
      })}
      {fields.length < fieldOptions.length && (
        <AddBtn
          onClick={() =>
            append({
              fieldName: undefined,
              value: undefined,
            })
          }
        />
      )}
    </>
  )
}
