import React, { FC } from 'react'
import { Controller, useFieldArray, UseFormReturn } from 'react-hook-form'
import { GlobalIcons } from '../../../../../components/Icons'
import { NButton } from '../../../../../components/NButton/NButton'
import { NDivider } from '../../../../../components/NDivider'
import { NRow } from '../../../../../components/NGrid/NGrid'
import { SelectOption } from '../../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../../components/NSelect/NSingleSelect'
import { FlowVariablePicker } from '../../../FlowBuilderVariable/FlowVariablePicker'
import { DataGetFormType } from '../../../models'
import { AssignElement, Header, Section } from '../components/CommonComponents'
import { AddBtn } from './AddBtn'

const CONDITION_OPTIONS: SelectOption[] = [
  { value: 'and', label: 'All Conditions Are Met (AND)' },
  // { value: '|', label: 'Any Conditions Is Met (OR)' },
  // { value: 'custom', label: 'Custom Condition Logic Is Met' },
]
// sign: "equals" | "notEquals" | "isNull" | "isNotNull";
const OPERATOR_OPTIONS: SelectOption[] = [
  { value: 'equals', label: 'Equal' },
  { value: 'notEquals', label: 'Not Equal' },
  { value: 'isNull', label: 'Is Null' },
  { value: 'isNotNull', label: 'Is Not Null' },
]

// const VALUE_TYPES = [
//   { value: 'variables', label: 'Variable' },
//   { value: 'value', label: 'Custom' },
// ]

type Props = {
  formMethods: UseFormReturn<DataGetFormType>
  fieldOptions: SelectOption[]
  required?: boolean
}

export const FilterRecordRules: FC<Props> = ({ formMethods, fieldOptions, required }) => {
  const { control } = formMethods

  const { fields, append, remove } = useFieldArray({
    control,
    name: `config`,
  })

  return (
    <Section>
      <Header>Filter record</Header>
      <Controller
        control={control}
        name="condition"
        render={({ field, fieldState }) => (
          <NSingleSelect
            options={CONDITION_OPTIONS}
            placeholder="Select Condition"
            value={field.value}
            label="Condition Requirements to Execute Outcome"
            onValueChange={field.onChange}
            error={fieldState.error?.message}
          />
        )}
        rules={{ required: required && { value: required, message: 'Required' } }}
      />
      <NDivider size="lg" />
      {fields.map((field, index) => {
        return (
          <AssignElement key={`rule-${index}`}>
            <NRow className="picker-container">
              <Controller
                name={`config[${index}].fieldName`}
                defaultValue={field.fieldName || ''}
                control={control}
                render={({ field: { value, onChange }, fieldState }) => (
                  <NSingleSelect
                    fullWidth
                    options={fieldOptions}
                    placeholder="Select field"
                    value={value}
                    onValueChange={onChange}
                    error={fieldState.error?.message}
                  />
                )}
                rules={{ required: required && { value: required, message: 'Required' } }}
              />
              <NDivider vertical size="sm" />
              <Controller
                name={`config[${index}].sign`}
                defaultValue={field.sign || ''}
                control={control}
                render={({ field: { value, onChange }, fieldState }) => (
                  <NSingleSelect
                    options={OPERATOR_OPTIONS}
                    placeholder="Select operator"
                    value={value}
                    onValueChange={onChange}
                    error={fieldState.error?.message}
                  />
                )}
                rules={{ required: required && { value: required, message: 'Required' } }}
              />
              <NDivider vertical size="sm" />
              <Controller
                name={`config[${index}].value`}
                defaultValue={field.value || ''}
                control={control}
                render={({ field: { value, onChange }, fieldState }) => (
                  <FlowVariablePicker
                    label="Select value"
                    selectedVariable={value}
                    onVariableSelected={onChange}
                    error={fieldState.error?.message}
                    style={{ width: '100%' }}
                  />
                )}
                rules={{ required: required && { value: required, message: 'Required' } }}
              />
            </NRow>
            <NButton
              type="ghost"
              icon={<GlobalIcons.Trash />}
              disabled={fields.length < 1}
              onClick={() => remove(index)}
            />
          </AssignElement>
        )
      })}
      <AddBtn
        onClick={() =>
          append({
            fieldName: '',
            value: '',
          })
        }
      />
    </Section>
  )
}
