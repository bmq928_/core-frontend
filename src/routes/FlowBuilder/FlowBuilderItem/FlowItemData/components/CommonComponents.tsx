import styled from 'styled-components'
import { color, spacing } from '../../../../../components/GlobalStyle'
import { NRow } from '../../../../../components/NGrid/NGrid'
import { typography } from '../../../../../components/NTypography'

export const ScrollableWrapper = styled.div`
  max-height: 70vh;
  overflow-y: auto;
`
export const Section = styled.div`
  padding: ${spacing('sm')} ${spacing('xl')};
`
export const Header = styled.p`
  color: ${color('Neutral400')};
  ${typography('overline')}
  text-transform: uppercase;
  margin-bottom: ${spacing('lg')};
`
export const DeleteBtn = styled.div`
  margin-left: ${spacing('xl')};
  min-width: 24px;
  cursor: pointer;
  height: 40px;
  display: flex;
  align-items: center;
  margin-top: ${spacing('md')};
  &:first-child {
    margin-top: 0;
  }
  .Picker {
    flex: 1;
  }
`
export const AssignElement = styled(NRow)`
  align-items: center;
  margin-top: ${spacing('md')};
  &:first-child {
    margin-top: 0;
  }
  .picker-container {
    flex: 1;
    margin-right: ${spacing('xs')};
  }
  .picker {
    flex: 1;
  }
`
