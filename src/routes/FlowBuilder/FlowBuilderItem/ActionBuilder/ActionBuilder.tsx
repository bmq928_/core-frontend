import React from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useQuery } from 'react-query'
import styled, { useTheme } from 'styled-components'
import { color, spacing } from '../../../../components/GlobalStyle'
import { NDivider } from '../../../../components/NDivider'
import { NColumn, NRow } from '../../../../components/NGrid/NGrid'
import { NModal } from '../../../../components/NModal/NModal'
import { SelectOption } from '../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { NTextArea } from '../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { typography } from '../../../../components/NTypography'
import { useSearchText } from '../../../../hooks/useSearchText'
import { useValidateString } from '../../../../hooks/useValidateString'
import { APIServices } from '../../../../services/api'
import { IntegrationsQueryKeys } from '../../../../services/api/endpoints/integrations'
import { ActionMetadataResponse } from '../../../../services/api/models'
import { getNameFromDisplayName } from '../../../../utils/utils'
import { FlowIcons } from '../../components/Icons'
import { ActionFormType } from '../../models'
import { FlowIconContainerClassName } from '../../utils/constants'
import { FlowNodeModalProps, NodeType, WidgetItemType } from '../../utils/types'
import { FlowIconContainer } from '../CommonStyledComponents'
import { ActionInputMap } from './ActionInputMap'

const Wrapper = styled.div`
  padding: ${spacing('lg')} ${spacing('xl')};
`
const SubFlowForm = styled.div`
  display: flex;
  flex-direction: column;
  max-height: 70vh;
  overflow-y: auto;
`
const TextHeader = styled.div`
  color: ${color('Neutral500')};
  margin: ${spacing('xl')} 0;
  ${typography('small-bold-text')}
`
const ContentText = styled.div`
  color: ${color('Neutral500')};
`

const Title = styled.p`
  ${typography('h500')}
  color:${color('Neutral700')};
  margin-bottom: ${spacing('xs')};
`
const LargeWrapper = styled.div`
  padding: ${spacing(40)} ${spacing(48)};
  display: flex;
  flex-direction: column;
  align-items: center;
`

type CustomActionMetadataResponse = {
  actions: SelectOption[]
  actionByGuid: Record<string, ActionMetadataResponse>
}

export const ActionBuilder: React.FC<FlowNodeModalProps> = ({ nodeModal, isSubmitting, onCancel, onSubmit }) => {
  const formMethods = useForm<ActionFormType>({
    defaultValues: nodeModal.data || {
      name: '',
      displayName: '',
      input_map: {},
    },
  })
  const { handleSubmit, control, register, formState, setValue, watch, getValues } = formMethods
  const theme = useTheme()
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText()
  const { validateFunction } = useValidateString()

  const {
    data: { actions, actionByGuid } = { actions: [], actionByGuid: {} } as CustomActionMetadataResponse,
    isLoading,
  } = useQuery(
    [IntegrationsQueryKeys.getAllActionMetadata, { searchText }],
    APIServices.Integrations.getAllActionMetadata,
    {
      select: data => {
        return data.data.data.reduce(
          (allActions, action) => {
            return {
              actions: [...allActions.actions, { value: action.name, label: action.displayName }],
              actionByGuid: { ...allActions.actionByGuid, [action.name]: action },
            }
          },
          { actions: [], actionByGuid: {} } as CustomActionMetadataResponse,
        )
      },
    },
  )

  const handleActionSubmit = (data: ActionFormType) => {
    onSubmit(data)
  }

  const actionValue = watch('action_metadata_name')

  return (
    <>
      <NModal.Header onClose={onCancel} title={`${nodeModal.nodeId ? 'Edit' : 'New'} Action`} />
      <NModal.Body>
        <SubFlowForm>
          <Wrapper>
            <NRow>
              <NColumn flex={1}>
                <Controller
                  control={control}
                  rules={{
                    required: {
                      value: true,
                      message: 'Required',
                    },
                  }}
                  name="action_metadata_name"
                  render={({ field, fieldState }) => (
                    <NSingleSelect
                      required
                      isSearchable
                      disabled={!!nodeModal.nodeId}
                      isLoading={isLoading}
                      searchValue={searchValue}
                      onSearchValueChange={handleSearchChange}
                      options={actions}
                      placeholder="Search all actions"
                      label="Action"
                      error={fieldState.error?.message}
                      value={field.value}
                      onValueChange={value => {
                        field.onChange(value)
                      }}
                    />
                  )}
                />
              </NColumn>
            </NRow>
            <NRow>
              <TextHeader>
                Use values from earlier in the flow to set the inputs for the "Create New Task" core action. To use its
                outputs later in the flow, store them in variables.
              </TextHeader>
            </NRow>
            {actionValue && (
              <NRow>
                <NColumn flex={1}>
                  <NTextInput
                    {...register('displayName', {
                      validate: validateFunction,
                    })}
                    required
                    name="displayName"
                    error={formState.errors.displayName?.message}
                    placeholder="Enter display name"
                    label="Display name"
                    onBlur={e => {
                      if (!!nodeModal.nodeId) {
                        return
                      }
                      const formatName = getNameFromDisplayName(e.target.value)
                      !getValues('name') && setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
                    }}
                  />
                </NColumn>
                <NDivider vertical size="xl" />
                <NColumn flex={1}>
                  <NTextInput
                    {...register('name', {
                      validate: validateFunction,
                    })}
                    required
                    disabled={!!nodeModal.nodeId}
                    error={formState.errors.name?.message}
                    name="name"
                    placeholder="API name"
                    label="API Name"
                  />
                </NColumn>
              </NRow>
            )}
            {actionValue && (
              <NRow>
                <NColumn flex={1}>
                  <NDivider size="xl" />
                  <NTextArea rows={4} {...register('description')} placeholder="Text Field" label="Description" />
                </NColumn>
              </NRow>
            )}
          </Wrapper>
          <NDivider size={1} lineSize={1} lineColor={color('Neutral300')({ theme })} />
          {actionValue ? (
            <>
              {actionByGuid && actionByGuid[actionValue]?.inputs && (
                <ActionInputMap actionMetadata={actionByGuid[actionValue]} formMethods={formMethods} />
              )}
            </>
          ) : (
            <LargeWrapper>
              <Title>Select Action</Title>
              <ContentText>Choose the flow to launch when this Subflow element is executed.</ContentText>
            </LargeWrapper>
          )}
        </SubFlowForm>
      </NModal.Body>
      <NDivider size={1} lineSize={1} lineColor={color('Neutral300')({ theme })} />
      <NModal.Footer isLoading={isSubmitting} onCancel={onCancel} onFinish={handleSubmit(handleActionSubmit)} />
    </>
  )
}

export const actionWidget: WidgetItemType = {
  leftIcon: <FlowIcons.Action />,
  flowIcon: (
    <FlowIconContainer className={FlowIconContainerClassName}>
      <FlowIcons.Action />
    </FlowIconContainer>
  ),
  label: 'Action',
  type: NodeType.Action,
  data: {
    name: '',
    displayName: '',
    input_map: {},
  },
}
