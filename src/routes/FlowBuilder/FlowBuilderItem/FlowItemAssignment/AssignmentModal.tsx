import React from 'react'
import { useForm } from 'react-hook-form'
import styled, { useTheme } from 'styled-components'
import { color, spacing } from '../../../../components/GlobalStyle'
import { NAccordion } from '../../../../components/NAccordion/NAccordion'
import { NDivider } from '../../../../components/NDivider'
import { NColumn, NRow } from '../../../../components/NGrid/NGrid'
import { NModal } from '../../../../components/NModal/NModal'
import { NTextArea } from '../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { useValidateString } from '../../../../hooks/useValidateString'
import { getNameFromDisplayName } from '../../../../utils/utils'
import { FlowIcons } from '../../components/Icons'
import { AssignmentFormType } from '../../models'
import { FlowIconContainerClassName } from '../../utils/constants'
import { FlowNodeModalProps, NodeType, WidgetItemType } from '../../utils/types'
import { FlowIconContainer } from '../CommonStyledComponents'
import { AssignmentRules } from './AssignmentRules'

const DecisionForm = styled.div`
  display: flex;
  flex-direction: column;
  overflow-y: scroll;
  height: 70vh;
`

const StyledColumn = styled(NColumn)`
  flex: 1;
`

const AccordionWrapper = styled.div`
  padding: ${spacing('lg')} ${spacing('xl')};
`

export const AssignmentModal: React.FC<FlowNodeModalProps> = ({ nodeModal, isSubmitting, onCancel, onSubmit }) => {
  //const { t } = useTranslation()
  const theme = useTheme()
  const { validateFunction } = useValidateString()

  const formMethods = useForm<AssignmentFormType>({
    defaultValues: nodeModal.data || {
      assignment_variables: [{}],
      displayName: '',
      name: '',
      description: '',
    },
  })

  const handleSubmit = (data: AssignmentFormType) => {
    onSubmit({
      name: data.name,
      displayName: data.displayName,
      description: data.description,
      assignment_variables: data.assignment_variables,
    })
  }

  return (
    <>
      <NModal.Header onClose={onCancel} title="New Assignment" />
      <NModal.Body>
        <DecisionForm>
          <AccordionWrapper>
            <NRow>
              <NColumn flex={1}>
                <NTextInput
                  {...formMethods.register('displayName', { validate: validateFunction })}
                  required
                  error={
                    formMethods.formState.errors.displayName?.message || formMethods.formState.errors.displayName?.type
                  }
                  placeholder="Display name"
                  label="Display Name"
                  onBlur={e => {
                    const formatName = getNameFromDisplayName(e.target.value)
                    !formMethods.getValues('name') &&
                      formMethods.setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
                  }}
                />
              </NColumn>

              <NDivider vertical size="xl" />
              <NColumn flex={1}>
                <NTextInput
                  disabled={!!nodeModal.nodeId}
                  {...formMethods.register('name', { validate: validateFunction })}
                  required
                  error={formMethods.formState.errors.name?.message || formMethods.formState.errors.name?.type}
                  placeholder="API Name"
                  label="API Name"
                />
              </NColumn>
            </NRow>
            <NDivider size="xl" />
            <NRow>
              <NColumn flex={1}>
                <NTextArea rows={4} {...formMethods.register('description')} label="Description" />
              </NColumn>
            </NRow>
          </AccordionWrapper>
          <NDivider size={1} lineSize={1} lineColor={color('Neutral300')({ theme })} />
          <NRow>
            <StyledColumn>
              <NAccordion title="Set variable values">
                <AccordionWrapper>
                  <AssignmentRules formMethods={formMethods} />
                </AccordionWrapper>
              </NAccordion>
            </StyledColumn>
          </NRow>
        </DecisionForm>
      </NModal.Body>
      <NDivider size={1} lineSize={1} lineColor={color('Neutral300')({ theme })} />
      <NModal.Footer isLoading={isSubmitting} onCancel={onCancel} onFinish={formMethods.handleSubmit(handleSubmit)} />
    </>
  )
}

export const assignmentWidget: WidgetItemType = {
  leftIcon: <FlowIcons.Assignment />,
  flowIcon: (
    <FlowIconContainer className={FlowIconContainerClassName} color="Secondary900">
      <FlowIcons.Assignment />
    </FlowIconContainer>
  ),
  label: 'Assignment',
  type: NodeType.Assignment,
  data: {
    name: '',
    displayName: '',
    description: '',
    assignment_variables: [{}],
  },
}
