import React, { FC, useMemo } from 'react'
import { Controller, FieldArrayWithId, useFieldArray, UseFormReturn } from 'react-hook-form'
import { useQuery } from 'react-query'
import { useParams } from 'react-router'
import styled from 'styled-components'
import { color, spacing } from '../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../components/Icons'
import { NButton } from '../../../../components/NButton/NButton'
import { NDivider } from '../../../../components/NDivider'
import { NColumn, NRow } from '../../../../components/NGrid/NGrid'
import { SelectOption } from '../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { NToast } from '../../../../components/NToast'
import { APIServices } from '../../../../services/api'
import { FlowsQueryKey } from '../../../../services/api/endpoints/flows'
import { FlowVariableResponse } from '../../../../services/api/models'
import { FlowVariablePicker } from '../../FlowBuilderVariable/FlowVariablePicker'
import { AssignmentFormType } from '../../models'

///////////////////////////////
const RuleWrapper = styled.div`
  width: 100%;
  background-color: ${color('white')};
`
const InnerRuleWrapper = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: ${spacing('xl')};
  .operator-select {
    width: 200px;
  }
  .picker {
    flex: 1;
  }
`
const FullRow = styled.div`
  display: flex;
  flex: 1;
`

const StyledButton = styled(NButton)`
  flex: 1;
  padding: ${spacing('sm')} ${spacing('sm')};
  background: ${color('white')} !important;
`
const StyledColumn = styled(NColumn)`
  border: 2px solid ${color('Primary700')};
  border-radius: 6px;
  background: ${color('white')};
`

type FlowVarByNameType = Record<string, FlowVariableResponse>

type Props = {
  formMethods: UseFormReturn<AssignmentFormType>
}

export const AssignmentRules: FC<Props> = ({ formMethods }) => {
  const { flowName } = useParams<{ flowName: string }>()

  const { control } = formMethods
  const { fields, append, remove } = useFieldArray({
    control,
    name: `assignment_variables`,
  })

  const { data: operationData, isLoading: isLoadingOperator } = useQuery(
    [FlowsQueryKey.getAssignmentOperations],
    APIServices.Flows.getAssignmentOperations,
    {
      select: data => {
        const types = Object.keys(data.data)
        return types.reduce((prev, cur) => {
          return {
            ...prev,
            [cur]: data.data[cur].map(op => {
              return {
                value: op.value,
                label: op.displayName,
              }
            }),
          }
        }, {} as Record<string, SelectOption[]>)
      },
    },
  )

  // get flow item variables
  const { data: flowVarByName = {} } = useQuery(
    [FlowsQueryKey.getFlowVariables, { flowName }],
    APIServices.Flows.getFlowVariables,
    {
      enabled: !!flowName,
      onError: error => {
        NToast.error({ title: 'Get flow variable error', subtitle: error.response?.data.message })
      },
      select: data => {
        return data.data.reduce((prev, cur) => {
          return {
            ...prev,
            [cur.name]: cur,
          }
        }, {} as FlowVarByNameType)
      },
    },
  )

  return (
    <RuleWrapper>
      {fields.map((clause, innerIndex) => (
        <RuleRow
          key={clause.id}
          clause={clause}
          innerIndex={innerIndex}
          operationData={operationData || {}}
          isLoadingOperator={isLoadingOperator}
          flowVarByName={flowVarByName}
          formMethods={formMethods}
          onRemove={remove}
        />
      ))}
      <NRow>
        <StyledColumn flex={1}>
          <StyledButton
            onClick={() =>
              append({
                variable: '',
                value: undefined,
              })
            }>
            <GlobalIcons.Plus /> Add Assignment
          </StyledButton>
        </StyledColumn>
      </NRow>
    </RuleWrapper>
  )
}

////////////////////////////////////////////////////////
type RuleRowProps = {
  clause: FieldArrayWithId<AssignmentFormType, 'assignment_variables', 'id'>
  innerIndex: number
  isLoadingOperator?: boolean
  operationData: Record<string, SelectOption[]>
  flowVarByName: FlowVarByNameType
  formMethods: UseFormReturn<AssignmentFormType>
  onRemove: (index?: number) => void
}
const RuleRow: FC<RuleRowProps> = ({
  clause,
  innerIndex,
  isLoadingOperator,
  operationData,
  flowVarByName,
  formMethods,
  onRemove,
}) => {
  const { control, watch, setValue } = formMethods
  const watchAssignmentVars = watch('assignment_variables')

  const variableType = useMemo(() => {
    if (!watchAssignmentVars[innerIndex]) {
      return
    }
    const { variable } = watchAssignmentVars[innerIndex]

    if (!variable) {
      return
    }

    // if variable is a field
    const slicedVar = variable.split('.')
    if (slicedVar.length > 1) {
      const [flowVarName, fieldName] = slicedVar
      const flowVar = flowVarByName[flowVarName]
      const field = (flowVar?.schema || []).find(f => {
        return f.name === fieldName
      })
      return field?.type
    }

    if (flowVarByName && variable) {
      return flowVarByName[variable]?.type
    }

    return
  }, [flowVarByName, watchAssignmentVars, innerIndex])

  const signOptions = useMemo<SelectOption[]>(() => {
    let result: SelectOption[] = []
    if (operationData && variableType && operationData[variableType]) {
      result = operationData[variableType]
    }
    return result
  }, [operationData, variableType])

  return (
    <InnerRuleWrapper key={clause.id}>
      <FullRow>
        <Controller
          defaultValue={clause.variable || ''}
          control={control}
          rules={{
            required: {
              value: true,
              message: 'Required',
            },
          }}
          name={`assignment_variables[${innerIndex}].variable`}
          render={({ field: { value, onChange }, fieldState }) => (
            <FlowVariablePicker
              className="picker"
              label="Pick assigned variable"
              placeholder="Select variable"
              noEnterValue
              error={fieldState.error?.message}
              selectedVariable={value ? { value, type: 'variable' } : undefined}
              onVariableSelected={varOption => {
                onChange(varOption?.value)
                setValue(`assignment_variables.${innerIndex}.sign`, 'add')
                setValue(`assignment_variables.${innerIndex}.value`, undefined)
              }}
            />
          )}
        />
        <NDivider vertical size="md" />
        <Controller
          defaultValue={clause.sign || ''}
          control={control}
          rules={{
            required: {
              value: true,
              message: 'Required',
            },
          }}
          name={`assignment_variables.${innerIndex}.sign`}
          render={({ field: { value, onChange }, fieldState }) => {
            return (
              <NSingleSelect
                disabled={!watchAssignmentVars[innerIndex]?.variable}
                isLoading={isLoadingOperator}
                options={signOptions}
                placeholder="Operator"
                error={fieldState.error?.message}
                value={value}
                onValueChange={onChange}
                className="operator-select"
              />
            )
          }}
        />
        <NDivider vertical size="md" />
        <Controller
          control={control}
          rules={{
            required: {
              value: true,
              message: 'Required',
            },
          }}
          name={`assignment_variables.${innerIndex}.value`}
          render={({ field: { value, onChange }, fieldState }) => {
            return (
              <FlowVariablePicker
                disabled={!watchAssignmentVars[innerIndex]?.variable}
                className="picker"
                label="Pick value"
                error={fieldState.error?.message}
                selectedVariable={value}
                onVariableSelected={onChange}
              />
            )
          }}
        />
      </FullRow>
      <NDivider vertical size="md" />
      <NButton
        type="ghost"
        icon={<GlobalIcons.Trash />}
        disabled={watchAssignmentVars.length <= 1}
        onClick={() => {
          onRemove(innerIndex)
        }}
      />
    </InnerRuleWrapper>
  )
}
