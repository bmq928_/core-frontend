import React from 'react'
import { Controller, UseFormReturn } from 'react-hook-form'
import styled from 'styled-components'
import { color, spacing } from '../../../../components/GlobalStyle'
import { NDivider } from '../../../../components/NDivider'
import { NSpinner } from '../../../../components/NSpinner/NSpinner'
import { NSwitch } from '../../../../components/NSwitch/NSwitch'
import { NToast } from '../../../../components/NToast'
import { NTypography } from '../../../../components/NTypography'
import { ContentValue, FlowInputVariableListResponse } from '../../../../services/api/models'
import { FlowVariablePicker } from '../../FlowBuilderVariable/FlowVariablePicker'
import { SubFlowFormType } from '../../models'
import { Header } from '../CommonStyledComponents'

const LoadingContainer = styled.div`
  display: flex;
  justify-content: center;
  padding: ${spacing('lg')} ${spacing('xl')};
`

const Wrapper = styled.div`
  padding: ${spacing('lg')} ${spacing('xl')};
`

const SetValueContainer = styled.div`
  display: flex;
  padding: ${spacing('md')} 0;
  justify-content: space-between;

  .variable_switch {
    height: fit-content;
  }

  &:not(:last-child) {
    border-bottom: 1px solid ${color('Neutral200')};
  }
`

const SetValueContainerLeft = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  width: 40%;
`

const VariableNameText = styled(NTypography.Headline)``

type SubFlowInputMapProps = {
  isLoading?: boolean
  flowInput: FlowInputVariableListResponse[]
  formMethods: UseFormReturn<SubFlowFormType>
}

export const SubFlowInputMap: React.FC<SubFlowInputMapProps> = ({ isLoading, flowInput, formMethods }) => {
  const { control } = formMethods

  if (isLoading) {
    return (
      <LoadingContainer>
        <NSpinner size={20} strokeWidth={2} />
      </LoadingContainer>
    )
  }

  return (
    <Wrapper>
      <Header>Set input values</Header>
      <Controller
        control={control}
        name="input_map"
        render={({ field }) => {
          return (
            <>
              {flowInput.map(input => {
                const inputName = input.name
                const isChecked = inputName in field.value && field.value[inputName] !== inputName

                const toggleVariable = (newVal: boolean) => {
                  const newInputMap = { ...field.value }
                  if (newVal) {
                    newInputMap[inputName] = undefined
                  } else {
                    delete newInputMap[inputName]
                  }
                  field.onChange(newInputMap)
                }

                const setVariableToInput = (selectedVariable?: ContentValue) => {
                  const newInputMap = { ...field.value }
                  newInputMap[inputName] = selectedVariable
                  field.onChange(newInputMap)
                }

                return (
                  <SetValueContainer key={`input_map_${inputName}`}>
                    <SetValueContainerLeft>
                      <VariableNameText>{inputName}</VariableNameText>
                      {isChecked && (
                        <>
                          <NDivider size="sm" />
                          <FlowVariablePicker
                            label="Input value"
                            selectedVariable={field.value[inputName]}
                            onVariableSelected={(varOption, flowVar) => {
                              if (flowVar ? input.type !== flowVar.type : input.type !== 'string') {
                                NToast.error({ title: `Variable type should be: ${input.type}` })
                                return
                              }
                              setVariableToInput(varOption)
                            }}
                          />
                        </>
                      )}
                    </SetValueContainerLeft>
                    <NSwitch className="variable_switch" checked={isChecked} onChange={toggleVariable} />
                  </SetValueContainer>
                )
              })}
            </>
          )
        }}
      />
    </Wrapper>
  )
}
