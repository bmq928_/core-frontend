import React from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useQuery } from 'react-query'
import { useParams } from 'react-router-dom'
import styled, { useTheme } from 'styled-components'
import { color, spacing } from '../../../../components/GlobalStyle'
import { NDivider } from '../../../../components/NDivider'
import { NColumn, NRow } from '../../../../components/NGrid/NGrid'
import { NModal } from '../../../../components/NModal/NModal'
import { SelectOption } from '../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { NTextArea } from '../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { typography } from '../../../../components/NTypography'
import { useSearchText } from '../../../../hooks/useSearchText'
import { useValidateString } from '../../../../hooks/useValidateString'
import { APIServices } from '../../../../services/api'
import { FlowResponse, FlowsQueryKey } from '../../../../services/api/endpoints/flows'
import { getNameFromDisplayName } from '../../../../utils/utils'
import { FlowIcons } from '../../components/Icons'
import { SubFlowFormType } from '../../models'
import { FlowIconContainerClassName } from '../../utils/constants'
import { FlowNodeModalProps, NodeType, WidgetItemType } from '../../utils/types'
import { FlowIconContainer } from '../CommonStyledComponents'
import { SubFlowInputMap } from './SubFlowInputMap'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: ${spacing('lg')} ${spacing('xl')};
`
const SubFlowForm = styled.form`
  display: flex;
  flex-direction: column;
  max-height: 60vh;
  overflow: auto;
`
const TextHeader = styled.div`
  color: ${color('Neutral500')};
  margin: ${spacing('xl')} 0;
  ${typography('small-bold-text')}
`
const ContentText = styled.div`
  color: ${color('Neutral500')};
`

const Title = styled.p`
  ${typography('h500')}
  color:${color('Neutral700')};
  margin-bottom: ${spacing('xs')};
`
const LargeWrapper = styled.div`
  padding: ${spacing(40)} ${spacing(48)};
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const SubFlowBuilderModal: React.FC<FlowNodeModalProps> = ({ nodeModal, isSubmitting, onSubmit, onCancel }) => {
  const formMethods = useForm<SubFlowFormType>({
    defaultValues: nodeModal.data || {
      displayName: '',
      name: '',
      input_map: {},
    },
  })

  const { handleSubmit, control, register, formState, setValue, watch, getValues } = formMethods

  const theme = useTheme()

  const referencedValue = watch('referencedFlow')
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText()

  const { flowName } = useParams<{ flowName: string }>()
  const { validateFunction } = useValidateString()

  const {
    data: { flowOptions, flowByName } = { flowOptions: [], flowByName: {} },
    isLoading: isLoadingSubflows,
  } = useQuery([FlowsQueryKey.getSubFlows, { searchText, flowName }], APIServices.Flows.getSubFlows, {
    select: data => {
      return data.data.data.reduce(
        (prev, cur) => {
          return {
            flowOptions: [
              ...prev.flowOptions,
              {
                label: `(${cur.name}) - ${cur.display_name}`,
                value: cur.name,
              },
            ],
            flowByName: { ...prev.flowByName, [cur.name]: cur },
          }
        },
        {
          flowOptions: [] as SelectOption[],
          flowByName: {} as Record<string, FlowResponse>,
        },
      )
    },
  })

  const { data: flowInputs, isLoading: isLoadingFlowInputs } = useQuery(
    [FlowsQueryKey.getFlowInputVariables, { flowName: referencedValue! }],
    APIServices.Flows.getFlowInputVariables,
    {
      enabled: !!referencedValue,
    },
  )

  const handleSubmitFunction = (data: SubFlowFormType) => {
    onSubmit(data)
  }

  const selectedFlow = referencedValue && flowByName ? flowByName[referencedValue] : undefined

  return (
    <>
      <NModal.Header onClose={onCancel} title={`${!!nodeModal.nodeId ? 'Edit' : 'New'} Subflow`} />
      <NModal.Body>
        <SubFlowForm>
          <Wrapper>
            <NRow>
              <NColumn flex={1}>
                <Controller
                  control={control}
                  name="referencedFlow"
                  rules={{
                    required: {
                      value: true,
                      message: 'Required',
                    },
                  }}
                  render={({ field, fieldState }) => (
                    <NSingleSelect
                      required
                      disabled={!!nodeModal.nodeId}
                      isLoading={isLoadingSubflows}
                      isSearchable
                      searchValue={searchValue}
                      onSearchValueChange={handleSearchChange}
                      options={flowOptions}
                      placeholder="Search Flow"
                      label="Referenced Flow"
                      error={fieldState.error?.message}
                      value={field.value}
                      onValueChange={value => {
                        field.onChange(value)
                      }}
                    />
                  )}
                />
              </NColumn>
            </NRow>
            <NRow>
              <TextHeader>
                Use values from the master flow to set the inputs for the "Verify Identity" flow. By default, the master
                flow stores all outputs. You can either reference outputs via the API name of the Subflow element or
                manually assign variables in the master flow to store individual outputs from the "Verify Identity"
                flow.
              </TextHeader>
            </NRow>
            {referencedValue && (
              <NRow>
                <NColumn flex={1}>
                  <NTextInput
                    {...register('displayName', {
                      validate: validateFunction,
                    })}
                    required
                    name="displayName"
                    error={formState.errors.displayName?.message}
                    placeholder="Display Name"
                    label="Display Name"
                    onBlur={e => {
                      if (!!nodeModal.nodeId) {
                        return
                      }
                      const formatName = getNameFromDisplayName(e.target.value)
                      !getValues('name') && setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
                    }}
                  />
                </NColumn>
                <NDivider vertical size="xl" />
                <NColumn flex={1}>
                  <NTextInput
                    {...register('name', {
                      validate: validateFunction,
                    })}
                    required
                    disabled={!!nodeModal.nodeId}
                    error={formState.errors.name?.message}
                    name="name"
                    placeholder="API Name"
                    label="API Name"
                  />
                </NColumn>
              </NRow>
            )}
            {referencedValue && (
              <NRow>
                <NColumn flex={1}>
                  <NDivider size="xl" />
                  <NTextArea rows={4} {...register('description')} placeholder="Description" label="Description" />
                </NColumn>
              </NRow>
            )}
          </Wrapper>
          <NDivider size={1} lineSize={1} lineColor={color('Neutral300')({ theme })} />
          {referencedValue &&
            selectedFlow &&
            (flowInputs && flowInputs.data.length > 0 ? (
              <SubFlowInputMap isLoading={isLoadingFlowInputs} flowInput={flowInputs.data} formMethods={formMethods} />
            ) : (
              <LargeWrapper>
                <Title>Nothing to set or store here</Title>
                <ContentText>This flow has no inputs or outputs</ContentText>
              </LargeWrapper>
            ))}

          {!referencedValue && (
            <LargeWrapper>
              <Title>Pick any flow</Title>
              <ContentText>Choose the flow to launch when this Subflow element is executed.</ContentText>
            </LargeWrapper>
          )}
        </SubFlowForm>
      </NModal.Body>
      <NDivider size={1} lineSize={1} lineColor={color('Neutral300')({ theme })} />
      <NModal.Footer isLoading={isSubmitting} onCancel={onCancel} onFinish={handleSubmit(handleSubmitFunction)} />
    </>
  )
}

export const subflowWidget: WidgetItemType<SubFlowFormType> = {
  leftIcon: <FlowIcons.Subflow />,
  flowIcon: (
    <FlowIconContainer className={FlowIconContainerClassName}>
      <FlowIcons.Subflow />
    </FlowIconContainer>
  ),
  label: 'Sub-flow',
  type: NodeType.Subflow,
  data: {
    displayName: '',
    name: '',
    description: '',
    input_map: {},
  },
}
