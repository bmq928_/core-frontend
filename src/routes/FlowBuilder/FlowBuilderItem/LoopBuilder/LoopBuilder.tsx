import React from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import styled, { useTheme } from 'styled-components'
import { color, spacing } from '../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../components/Icons'
import { NDivider } from '../../../../components/NDivider'
import { NColumn, NRow } from '../../../../components/NGrid/NGrid'
import { NModal } from '../../../../components/NModal/NModal'
import { NRadio } from '../../../../components/NRadio/NRadio'
import { NTextArea } from '../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { RequiredIndicator } from '../../../../components/NTypography'
import { useValidateString } from '../../../../hooks/useValidateString'
import { getNameFromDisplayName } from '../../../../utils/utils'
import { FlowIcons } from '../../components/Icons'
import { FlowVariablePicker } from '../../FlowBuilderVariable/FlowVariablePicker'
import { LoopFormType } from '../../models'
import { FlowIconContainerClassName } from '../../utils/constants'
import { FlowNodeModalProps, NodeType, WidgetItemType } from '../../utils/types'
import { FlowIconContainer, Header } from '../CommonStyledComponents'

const LoopWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: ${spacing('lg')} ${spacing('xl')};
`

const LoopForm = styled.div`
  display: flex;
  flex-direction: column;
  overflow-y: scroll;
  height: 70vh;
`

const TextHeader = styled.div`
  color: ${color('Neutral500')};
  margin-bottom: ${spacing('xs')};
`

const WrapperRadio = styled.div`
  padding: ${spacing(4)} 0;
`

const NoteWrapper = styled.div`
  margin: 0 ${spacing('lg')} ${spacing('lg')} ${spacing('lg')};
  background: ${color('Neutral200')};
`

const NoteContent = styled.div`
  display: flex;
  justify-content: space-between;
  padding: ${spacing('lg')} 0;
  align-items: center;
`

const NoteIcon = styled.div`
  padding: 0 ${spacing('lg')};
`

const NoteText = styled.div`
  flex: 1;
  line-height: ${spacing('xl')};
`

export const LoopBuilder: React.FC<FlowNodeModalProps> = ({ nodeModal, isSubmitting, onSubmit, onCancel }) => {
  const theme = useTheme()
  const { t } = useTranslation()
  const { validateFunction } = useValidateString()

  const { register, formState, handleSubmit, control, setValue, getValues } = useForm({
    defaultValues: nodeModal.data || {
      name: '',
      displayName: '',
      description: '',
      collection_variable_name: '',
      loop_order: 'ASC',
    },
  })

  return (
    <>
      <NModal.Header onClose={onCancel} title={nodeModal.nodeId ? 'Edit Loop' : 'New Loop'} />
      <NModal.Body>
        <LoopForm>
          <LoopWrapper>
            <TextHeader>
              Start a loop path for iterating over items in a collection variable. For each iteration, the flow
              temporarily stores the item in the loop variable
            </TextHeader>
            <NRow>
              <NColumn flex={1}>
                <NTextInput
                  {...register('displayName', {
                    validate: validateFunction,
                  })}
                  required
                  error={formState.errors.displayName?.message}
                  placeholder="Display name"
                  label="Display name"
                  onBlur={e => {
                    if (!!nodeModal.nodeId) {
                      return
                    }
                    const formatName = getNameFromDisplayName(e.target.value)
                    !getValues('name') && setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
                  }}
                />
              </NColumn>

              <NDivider vertical size="xl" />
              <NColumn flex={1}>
                <NTextInput
                  disabled={!!nodeModal.nodeId}
                  {...register('name', {
                    validate: validateFunction,
                  })}
                  required
                  error={formState.errors.name?.message}
                  placeholder="API name"
                  label="API Name"
                />
              </NColumn>
            </NRow>
            <NDivider size="xl" />
            <NTextArea rows={4} {...register('description')} label="Description" />
          </LoopWrapper>
          <NDivider size={1} lineSize={1} lineColor={color('Neutral300')({ theme })} />
          <LoopWrapper>
            <Header>
              <RequiredIndicator>*</RequiredIndicator>Select Collection Variable
            </Header>
            <NDivider size="xs" />
            <Controller
              control={control}
              name="collection_variable_name"
              rules={{
                required: {
                  value: true,
                  message: t('common.error.required'),
                },
              }}
              render={({ field }) => {
                return (
                  <FlowVariablePicker
                    label="Select a collection variable"
                    placeholder="Select a collection variable"
                    noEnterValue
                    error={formState.errors.collection_variable_name?.message}
                    selectedVariable={field.value ? { value: field.value, type: 'variable' } : undefined}
                    onVariableSelected={varOption => {
                      field.onChange(varOption?.value)
                    }}
                  />
                )
              }}
            />
          </LoopWrapper>
          <NDivider size={1} lineSize={1} lineColor={color('Neutral300')({ theme })} />
          <LoopWrapper>
            <Header>Specify Direction for Iterating Over Collection</Header>
            <WrapperRadio>Direction:</WrapperRadio>
            <Controller
              control={control}
              name="loop_order"
              render={({ field }) => (
                <>
                  <WrapperRadio>
                    <NRadio
                      name="loop_order"
                      value="ASC"
                      checked={field.value === 'ASC'}
                      title="First item to last item"
                      onChange={e => {
                        field.onChange(e.target.value)
                      }}
                    />
                  </WrapperRadio>
                  <WrapperRadio>
                    <NRadio
                      name="loop_order"
                      checked={field.value === 'DESC'}
                      value="DESC"
                      title="Last item to first item"
                      onChange={e => {
                        field.onChange(e.target.value)
                      }}
                    />
                  </WrapperRadio>
                </>
              )}
            />
          </LoopWrapper>

          <NoteWrapper>
            <NoteContent>
              <NoteIcon>
                <GlobalIcons.Info />
              </NoteIcon>
              <NoteText>
                To use the current item in other elements in the loop, use the API name of the Loop element. Example: if
                your flow iterates over accounts with a Loop elements named "My_Account_Loop" you can reference the
                current item from that loop elements. Just start typing "My_Account_Loop" and select "Current Item from
                Loop My_Account_Loop".
              </NoteText>
            </NoteContent>
          </NoteWrapper>
        </LoopForm>
      </NModal.Body>
      <NDivider size={1} lineSize={1} lineColor={color('Neutral300')({ theme })} />
      <NModal.Footer isLoading={isSubmitting} onCancel={onCancel} onFinish={handleSubmit(onSubmit)} />
    </>
  )
}

export const loopBuilderWidget: WidgetItemType<LoopFormType> = {
  leftIcon: <FlowIcons.Loop />,
  flowIcon: (
    <FlowIconContainer className={FlowIconContainerClassName} color="Secondary900">
      <FlowIcons.Loop />
    </FlowIconContainer>
  ),
  label: 'Loop',
  type: NodeType.Loop,
  data: {
    name: '',
    displayName: '',
    description: '',
    collection_variable_name: '',
    loop_order: 'ASC',
  },
}
