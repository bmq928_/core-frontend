import {
  BaseScreenFIComponent,
  InputProps,
  RootProps,
  SelectionProps,
  TextProps,
} from '../../../../../services/api/models'

export enum ScreenElementType {
  Root = 'ELEMENT_ROOT',
  Input = 'ELEMENT_INPUT',
  Display = 'ELEMENT_DISPLAY',
  Component = 'ELEMENT_COMPONENT',
}

export enum ScreenElementComponent {
  Root = 'Root',
  Input = 'Input',
  Selection = 'Selection',
  //
  DisplayText = 'DisplayText',
  //
  InputList = 'InputList',
  FileDropzone = 'FileDropzone',
  Withdrawal = 'Withdrawal',

  AmountInput = 'AmountInput',
}

export type ExtendInputProps = InputProps & Record<string, any>
export type ExtendSelectionProps = SelectionProps & { defaultSelectValue?: string[] } & Record<string, any>

export type ScreenElement = Partial<Omit<BaseScreenFIComponent, 'props'>> & {
  component: ScreenElementComponent
  label: string
  props: Partial<RootProps & ExtendInputProps & ExtendSelectionProps & TextProps>
  children: string[]
}

export type ScreenWidgetItemType = {
  id?: string
  icon: JSX.Element
  label: string
  type: ScreenElementType
  component: ScreenElementComponent
  props: RootProps | ExtendInputProps | ExtendSelectionProps | TextProps | Record<string, any>
  children: string[]
}

export type DraggableElement = {
  elementId?: string
  item?: Omit<ScreenElement, 'id'> & { id?: string }
  parentId: string
  index: number
}
