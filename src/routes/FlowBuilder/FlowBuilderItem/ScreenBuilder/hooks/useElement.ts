import throttle from 'lodash/throttle'
import { nanoid } from 'nanoid'
import * as React from 'react'
import { useDrag, useDrop } from 'react-dnd'
import { hasValue } from '../../../../../utils/utils'
import { Direction, MousePosition, ZoneProps } from '../../../../LayoutBuilder/utils/models'
import { ZONE_ITEMS, ZONE_ITEMS_INPUTS, ZONE_ITEMS_SELECTIONS } from '../components/ZoneItems/zoneItems'
import { useScreenAction, useScreenBuilder } from '../contexts/ScreenBuilderContext'
import { LEFT_SIDEBAR } from '../utils/builder'
import {
  DraggableElement,
  ScreenElement,
  ScreenElementComponent,
  ScreenElementType,
  ScreenWidgetItemType,
} from '../utils/models'

type Rect = { top: number; left: number; right: number; bottom: number }
type MouseOffset = { x: number; y: number }

const ACTION_VERTICAL_SIZE = 20
const ACTION_HORIZONTAL_SIZE = 20

function calculateMousePosition(elementRect: Rect, mouseOffset: MouseOffset, direction: Direction) {
  const actionsRect = {
    top: elementRect.top + ACTION_VERTICAL_SIZE,
    right: elementRect.right - ACTION_HORIZONTAL_SIZE,
    bottom: elementRect.bottom - ACTION_VERTICAL_SIZE,
    left: elementRect.left + ACTION_HORIZONTAL_SIZE,
  }

  // last mouseOffsetX <= elementRect.right - 1
  // last mouseOffsetY <= elementRect.bottom - 1
  const { x: mouseOffsetX, y: mouseOffsetY } = mouseOffset

  // move-top || move-bottom
  if (direction === Direction.Vertical) {
    const isInZone = mouseOffsetX > elementRect.left && mouseOffsetX <= elementRect.right - 1
    // move-top
    if (mouseOffsetY > elementRect.top && mouseOffsetY < actionsRect.top && isInZone) {
      return MousePosition.Top
    }
    // move-bottom
    if (mouseOffsetY <= elementRect.bottom - 1 && mouseOffsetY > actionsRect.bottom && isInZone) {
      return MousePosition.Bottom
    }
  }

  if (direction === Direction.Horizontal) {
    const isInZone = mouseOffsetY > elementRect.top && mouseOffsetY <= elementRect.bottom - 1
    // move-left
    if (mouseOffsetX > elementRect.left && mouseOffsetX < actionsRect.left && isInZone) {
      return MousePosition.Left
    }
    // move-right
    if (mouseOffsetX <= elementRect.right - 1 && mouseOffsetX > actionsRect.right && isInZone) {
      return MousePosition.Right
    }
  }

  return null
}

type ReturnUseElement = [
  { isOver: boolean; isDragging: boolean; isMouseOver: boolean; mousePosition: MousePosition | null },
  React.RefObject<HTMLDivElement>,
]

export function useElement({
  elementId,
  parentId,
  index,
  canDrag = true,
  canDrop = true,
}: ZoneProps): ReturnUseElement {
  const elementRef = React.useRef<HTMLDivElement>(null)

  const [mousePosition, setMousePosition] = React.useState<MousePosition | null>(null)
  const [mouseOver, setMouseOver] = React.useState(false)

  React.useEffect(() => {
    if (elementRef.current) {
      const el = elementRef.current
      const handleMouseOver = (e: MouseEvent) => {
        e.stopPropagation()
        setMouseOver(true)
      }
      const handleMouseOut = (e: MouseEvent) => {
        e.stopPropagation()
        setMouseOver(false)
      }
      el.addEventListener('mouseover', handleMouseOver)
      el.addEventListener('mouseout', handleMouseOut)
      return () => {
        el.removeEventListener('mouseover', handleMouseOver)
        el.removeEventListener('mouseout', handleMouseOut)
      }
    }
  }, [])

  const { elements } = useScreenBuilder()
  const { addElement, moveElement } = useScreenAction()

  const element = elements[elementId]
  const parent = elements[parentId]

  const [{ isDragging }, drag] = useDrag({
    type: element.type || '',
    item: { parentId, elementId, index },
    canDrag() {
      return Boolean(canDrag)
    },
    collect(monitor) {
      return {
        isDragging: monitor.isDragging(),
      }
    },
  })

  const [{ isOver }, drop] = useDrop({
    accept: [ScreenElementType.Display, ScreenElementType.Component, ScreenElementType.Input, ScreenElementType.Root],
    canDrop() {
      const droppableComponent =
        element.type === ScreenElementType.Root || element.component === ScreenElementComponent.InputList
      return (canDrop && droppableComponent) || hasValue(mousePosition)
    },
    drop(dragItem: DraggableElement, monitor) {
      //
      if (monitor.didDrop() || dragItem.elementId === elementId) {
        return
      }
      // List Input cannot have List Input item
      if (
        element.component === ScreenElementComponent.InputList &&
        dragItem.item?.component === ScreenElementComponent.InputList
      ) {
        return
      }
      const targetIndex =
        mousePosition === MousePosition.Right || mousePosition === MousePosition.Bottom ? index + 1 : index
      // add new element to screen builder root
      if (dragItem.parentId === LEFT_SIDEBAR) {
        const newElementId = nanoid()
        const component = dragItem.item!.component
        const props = dragItem.item!.props
        let Component: ScreenWidgetItemType | undefined

        if (props.inputType) {
          Component = ZONE_ITEMS_INPUTS[props.inputType]
        } else if (props.selectionType) {
          Component = ZONE_ITEMS_SELECTIONS[props.isRadio ? 'Radio' : 'PickList']
        } else {
          Component = ZONE_ITEMS[component]
        }
        //
        const { icon: _icon, ...item } = Component
        const newElement = {
          id: newElementId,
          ...item,
        } as ScreenElement

        if (hasValue(mousePosition)) {
          addElement({
            element: newElement,
            source: { id: dragItem.parentId },
            target: { id: parentId, index: targetIndex },
          })
          setMousePosition(null)
          return
        }
        addElement({ element: newElement, source: { id: dragItem.parentId }, target: { id: elementId } })
        return
      }
      // move element around root
      if (hasValue(mousePosition)) {
        moveElement({
          elementId: dragItem.elementId!,
          source: { id: dragItem.parentId },
          target: { id: parentId, index: targetIndex },
        })
        setMousePosition(null)
        return
      }
      moveElement({
        elementId: dragItem.elementId!,
        source: { id: dragItem.parentId },
        target: { id: elementId },
      })
    },
    hover: throttle(function (dragItem: DraggableElement, monitor: any) {
      // hover over itself
      if (!elementRef.current || dragItem.elementId === elementId) {
        setMousePosition(null)
        return
      }

      const elementRect = elementRef.current.getBoundingClientRect()

      const mouseOffset = monitor.getClientOffset() || { x: 0, y: 0 }
      const direction =
        parent.component === ScreenElementComponent.InputList ? Direction.Horizontal : Direction.Vertical
      const position = calculateMousePosition(elementRect, mouseOffset, direction)
      setMousePosition(position)
    }, 100),
    collect(monitor) {
      return {
        isOver: canDrop && monitor.isOver({ shallow: true }),
      }
    },
  })

  drag(drop(elementRef))

  return [{ isDragging, isOver, isMouseOver: mouseOver, mousePosition: isOver ? mousePosition : null }, elementRef]
}
