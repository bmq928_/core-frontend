import { ZoneProps } from '../../../../../LayoutBuilder/utils/models'
import { ScreenWidgetItemType } from '../../utils/models'
import { FileDropzone, widgetFileDropzone } from './Components/FileDropzone'
import { InputList, widgetInputList } from './Components/InputList'
import { AmountInput, widgetAmountInput } from './Fields/AmountInput'
import { Checkbox, widgetCheckbox } from './Fields/Checkbox'
import { DateTime, widgetDateTime } from './Fields/DateTime'
import { Input, widgetInput } from './Fields/Input'
import { Paragraph, widgetParagraph } from './Fields/Paragraph'
import { PickList, widgetPickList } from './Fields/PickList'
import { RadioGroup, widgetRadioGroup } from './Fields/RadioGroup'

export const WIDGET_ITEMS = [
  widgetInput,
  widgetPickList,
  widgetDateTime,
  widgetCheckbox,
  widgetRadioGroup,
  widgetInputList,
  widgetFileDropzone,
  //
  widgetParagraph,
  widgetAmountInput,
]

//
export const ZONE_ITEMS_INPUTS = {
  string: widgetInput,
  float: widgetInput,
  integer: widgetInput,
  date: widgetDateTime,
  time: widgetDateTime,
  'date-time': widgetDateTime,
  boolean: widgetCheckbox,
} as Record<string, ScreenWidgetItemType>

export const ZONE_ITEMS_SELECTIONS = {
  PickList: widgetPickList,
  Radio: widgetRadioGroup,
} as Record<string, ScreenWidgetItemType>

export const ZONE_ITEMS = {
  DisplayText: widgetParagraph,
  InputList: widgetInputList,
  FileDropzone: widgetFileDropzone,
  AmountInput: widgetAmountInput,
} as Record<string, ScreenWidgetItemType>

//
type RendererType = (props: ZoneProps) => JSX.Element
export const ZONE_ELEMENTS_INPUTS = {
  string: Input,
  float: Input,
  integer: Input,
  date: DateTime,
  time: DateTime,
  'date-time': DateTime,
  boolean: Checkbox,
} as Record<string, RendererType>
export const ZONE_ELEMENTS_SELECTIONS = {
  PickList,
  Radio: RadioGroup,
} as Record<string, RendererType>
export const ZONE_ELEMENTS = {
  // temporary
  Root: Input,
  DisplayText: Paragraph,
  InputList,
  FileDropzone,
  AmountInput,
} as Record<string, RendererType>
