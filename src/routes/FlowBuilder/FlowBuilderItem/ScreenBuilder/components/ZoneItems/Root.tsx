import React, { FC, useEffect } from 'react'
import styled from 'styled-components'
import { classnames } from '../../../../../../common/classnames'
import { color, spacing } from '../../../../../../components/GlobalStyle'
import { NButton } from '../../../../../../components/NButton/NButton'
import { NCheckbox } from '../../../../../../components/NCheckbox/NCheckbox'
import { NDivider } from '../../../../../../components/NDivider'
import { elevation } from '../../../../../../components/NElevation'
import { NRow } from '../../../../../../components/NGrid/NGrid'
import { NModal } from '../../../../../../components/NModal/NModal'
import { NTextArea } from '../../../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../../../components/NTextInput/NTextInput'
import { RootProps } from '../../../../../../services/api/models'
import { getNameFromDisplayName } from '../../../../../../utils/utils'
import { ROOT_ELEMENT } from '../../../../../LayoutBuilder/utils/builder'
import { useScreenAction, useScreenBuilder } from '../../contexts/ScreenBuilderContext'
import { useElement } from '../../hooks/useElement'
import { useElementRenderer } from '../../hooks/useElementRenderer'
import { ScreenElementComponent, ScreenElementType, ScreenWidgetItemType } from '../../utils/models'
import { AccordionWidget, Widget } from '../Sidebar'
import { Placeholder } from './StyledElements'
import { ZoneItemLayout } from './ZoneItemLayout'

const RootWrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  height: calc(100vh - 152px * 2);
  overflow: auto;
  align-items: stretch;
`

const Actions = styled('div')`
  position: absolute;
  top: 0;
  left: 0;
  background: ${color('Primary700')};
  z-index: 9999;
  display: none;
  padding: 4px 8px;
  color: ${color('white')};
`

const Wrapper = styled.div.attrs(props => {
  if (props.className?.includes('is-over')) {
    return {
      style: {
        background: color('Primary200')(props),
      },
    }
  }
})`
  display: flex;
  flex-direction: column;

  margin: ${spacing('sm')};
  position: relative;
  background: ${color('white')};
  border-radius: 4px;
  ${elevation('Elevation100')}

  &.is-selected,
  &.is-mouse-over {
    & > ${Actions} {
      display: block;
    }
  }
`

const PadCheckbox = styled(NCheckbox)`
  padding: ${spacing('sm')} 0;
`

type RootElementProps = {
  isEditing?: boolean
  flowName: string
  flowDisplayName: string
  flowDescription: string
  setFlowName: (name: string) => void
  setFlowDisplayName: (displayName: string) => void
  setFlowDescription: (description: string) => void
}

export const Root: FC<RootElementProps> = ({
  isEditing,
  flowName,
  flowDisplayName,
  flowDescription,
  setFlowName,
  setFlowDisplayName,
  setFlowDescription,
}) => {
  const elementId = ROOT_ELEMENT
  const parentId = ROOT_ELEMENT
  const index = 0
  const canDrop = true
  const canDrag = false

  const { elements, selectedElement } = useScreenBuilder()
  const element = elements[elementId]
  const [{ isOver, isMouseOver, mousePosition }, ref] = useElement({
    elementId,
    parentId,
    index,
    canDrop,
    canDrag,
  })
  const children = useElementRenderer(elementId)
  const { selectElement, configElement, setElementName } = useScreenAction()

  useEffect(() => {
    selectElement(elementId)
  }, [])

  const isSelected = selectedElement === elementId

  const { showHeader, showFooter } = element.props as RootProps

  return (
    <RootWrapper>
      <ZoneItemLayout
        shouldRenderSidebar={isSelected}
        sidebar={
          <React.Fragment>
            <AccordionWidget title="Screen properties" addPadContent>
              <Widget required title="Display Name">
                <NTextInput
                  required
                  value={flowDisplayName}
                  error={!flowDisplayName.length ? 'Required' : undefined}
                  onChange={e => {
                    setFlowDisplayName(e.target.value)
                    configElement(elementId, { label: e.target.value })
                  }}
                  onBlur={e => {
                    if (!flowName) {
                      const formatName = getNameFromDisplayName(e.target.value)
                      setFlowName(formatName)
                      setElementName(elementId, formatName)
                    }
                  }}
                />
              </Widget>

              <Widget required title="API Name">
                <NTextInput
                  required
                  disabled={isEditing}
                  value={flowName}
                  error={!flowName.length ? 'Required' : undefined}
                  onChange={e => {
                    setFlowName(e.target.value)
                    setElementName(elementId, e.target.value)
                  }}
                />
              </Widget>

              <Widget title="Description">
                <NTextArea
                  value={flowDescription}
                  onChange={e => {
                    setFlowDescription(e.target.value)
                  }}
                />
              </Widget>
            </AccordionWidget>

            <AccordionWidget title="Configure frame" addPadContent>
              <PadCheckbox
                title="Show Header"
                name="showHeader"
                checked={showHeader}
                onChange={e => {
                  configElement(elementId, { showHeader: e.target.checked })
                }}
              />
              <PadCheckbox
                title="Show Footer"
                name="showFooter"
                checked={showFooter}
                onChange={e => {
                  configElement(elementId, { showFooter: e.target.checked })
                }}
              />
              {/* <PadCheckbox
                title="Print Button"
                name="showPrintBtn"
                checked={showPrintBtn}
                onChange={e => {
                  configElement(elementId, { showPrintBtn: e.target.checked })
                }}
              /> */}
              <NDivider size="lg" />
            </AccordionWidget>

            {/* <AccordionWidget title="Control navigation" addPadContent>
            <PadCheckbox
              title="Next or Finish"
              name="nextBtn"
              checked={element.props.nextBtn}
              onChange={e => {
                configElement(elementId, { nextBtn: e.target.checked })
              }}
            />
            <PadCheckbox
              title="Previous"
              name="previousBtn"
              checked={element.props.previousBtn}
              onChange={e => {
                configElement(elementId, { previousBtn: e.target.checked })
              }}
            />
            <PadCheckbox
              title="Pause"
              name="pauseBtn"
              checked={element.props.pauseBtn}
              onChange={e => {
                configElement(elementId, { pauseBtn: e.target.checked })
              }}
            />
            <Widget title="Pause Confirmation Message">
              <NTextInput placeholder="Insert a resource..." left={<GlobalIcons.Search />} />
              <NDivider size="sm" />
              <NTextArea
                value={element.props.pauseMsg}
                onChange={e => {
                  configElement(elementId, { pauseMsg: e.target.value })
                }}
              />
            </Widget>
          </AccordionWidget>

          <AccordionWidget title="Provide Help" addPadContent>
            <Widget title="Help Text">
              <NTextInput placeholder="Insert a resource..." left={<GlobalIcons.Search />} />
              <NDivider size="sm" />
              <NTextArea
                value={element.props.helpText}
                onChange={e => {
                  configElement(elementId, { helpText: e.target.value })
                }}
              />
            </Widget>
          </AccordionWidget> */}
          </React.Fragment>
        }>
        {/* ////////////////////////// */}
        <Wrapper
          onClick={(e: any) => {
            e.stopPropagation()
            selectElement(elementId)
          }}
          className={classnames([
            isOver && 'is-over',
            isMouseOver && 'is-mouse-over',
            mousePosition && `is-sort ${mousePosition}`,
            isSelected && 'is-selected',
          ])}
          ref={ref}>
          {showHeader && <NModal.Header title={flowDisplayName || '{REQUIRED DISPLAY NAME}'} />}
          <NDivider size="xxl"></NDivider>
          {children}
          <NDivider size="xxl"></NDivider>
          <Placeholder mousePosition={mousePosition} />
          {showFooter && (
            <NModal.Footer>
              {/* <div>
                {showPrintBtn && (
                  <NButton size="small" type="outline" onClick={() => {}}>
                    Print
                  </NButton>
                )}
              </div> */}
              <NRow>
                <NButton size="small" onClick={() => {}}>
                  Previous
                </NButton>
                <NButton size="small" type="primary" onClick={() => {}}>
                  Finish
                </NButton>
              </NRow>
            </NModal.Footer>
          )}
          <Actions className="element-actions">{element.label}</Actions>
        </Wrapper>
      </ZoneItemLayout>
    </RootWrapper>
  )
}

export const InitRootElement: ScreenWidgetItemType = {
  id: ROOT_ELEMENT,
  icon: <></>,
  label: 'Root',
  type: ScreenElementType.Root,
  component: ScreenElementComponent.Root,
  props: {
    showHeader: true,
    showFooter: true,
  },
  children: [] as string[],
}
