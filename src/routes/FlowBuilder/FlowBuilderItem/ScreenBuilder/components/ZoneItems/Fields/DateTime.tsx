import * as React from 'react'
import { classnames } from '../../../../../../../common/classnames'
import { NDatePicker } from '../../../../../../../components/DateTime/NDatePicker'
import { NDateTimePicker } from '../../../../../../../components/DateTime/NDateTimePicker'
import { NTimePicker } from '../../../../../../../components/DateTime/NTimePicker'
import { SelectOption } from '../../../../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../../../../components/NSelect/NSingleSelect'
import { NTextInput } from '../../../../../../../components/NTextInput/NTextInput'
import { getDateFromIsoString } from '../../../../../../../utils/utils'
import { ZoneProps } from '../../../../../../LayoutBuilder/utils/models'
import { FlowVariablePicker } from '../../../../../FlowBuilderVariable/FlowVariablePicker'
import { useScreenAction, useScreenBuilder } from '../../../contexts/ScreenBuilderContext'
import { useElement } from '../../../hooks/useElement'
import { ROOT_ELEMENT } from '../../../utils/builder'
import { ScreenElementComponent, ScreenElementType, ScreenWidgetItemType } from '../../../utils/models'
import { BaseRightWidget } from '../../BaseRightWidget'
import { ScreenFlowItemIcons } from '../../ScreenFlowItemIcons'
import { AccordionWidget, Widget } from '../../Sidebar'
import { FieldWrapper as Wrapper, Placeholder } from '../StyledElements'
import { ZoneItemLayout } from '../ZoneItemLayout'

const DATETIME_TYPE_OPTIONS: SelectOption[] = [
  { label: 'Date & Time', value: 'date-time' },
  { label: 'Date', value: 'date' },
  { label: 'Time', value: 'time' },
]

export function DateTime({ elementId, parentId, index }: ZoneProps) {
  const { elements, selectedElement } = useScreenBuilder()
  const { selectElement, configElement, setElementName } = useScreenAction()
  const [{ isMouseOver, mousePosition }, ref] = useElement({ elementId, parentId, index })
  const element = elements[elementId]
  const isSelected = selectedElement === elementId

  const { defaultValue, inputType, isRequired, ...restProps } = element.props

  const defaultValueDate =
    defaultValue && defaultValue.type === 'value' ? getDateFromIsoString(defaultValue.value, inputType) : undefined

  return (
    <ZoneItemLayout
      shouldRenderSidebar={isSelected}
      sidebar={
        <React.Fragment>
          <AccordionWidget
            title="Date & Time"
            addPadContent
            onBack={() => {
              selectElement(ROOT_ELEMENT)
            }}>
            <BaseRightWidget
              elementId={elementId}
              element={element}
              configElement={configElement}
              setElementName={setElementName}
            />
            <Widget title="Type">
              <NSingleSelect
                options={DATETIME_TYPE_OPTIONS}
                value={inputType}
                onValueChange={newVal => {
                  configElement(elementId, { inputType: newVal, defaultValue: undefined })
                }}
              />
            </Widget>
            <Widget title="Caption">
              <NTextInput
                value={element.props.caption}
                onChange={e => {
                  configElement(elementId, { caption: e.target.value })
                }}
              />
            </Widget>
            <Widget title="Default Value">
              <FlowVariablePicker
                label="Default Value"
                inputType={inputType}
                selectedVariable={defaultValue}
                onVariableSelected={varOption => {
                  configElement(elementId, { defaultValue: varOption })
                }}
              />
            </Widget>
          </AccordionWidget>
        </React.Fragment>
      }>
      <Wrapper
        ref={ref}
        className={classnames([
          isMouseOver && 'is-mouse-over',
          mousePosition && `is-sort ${mousePosition}`,
          isSelected && 'is-selected',
        ])}
        onClick={e => {
          e.stopPropagation()
          selectElement(elementId)
        }}>
        {inputType === 'date' ? (
          <NDatePicker
            required={isRequired}
            disabled
            value={defaultValueDate}
            onChangeValue={() => {}}
            {...restProps}
          />
        ) : inputType === 'time' ? (
          <NTimePicker
            required={isRequired}
            disabled
            value={defaultValueDate}
            onChangeValue={() => {}}
            {...restProps}
          />
        ) : (
          <NDateTimePicker
            required={isRequired}
            disabled
            value={defaultValueDate}
            onValueChange={() => {}}
            {...restProps}
          />
        )}
        <Placeholder mousePosition={mousePosition} />
      </Wrapper>
    </ZoneItemLayout>
  )
}

export const widgetDateTime: ScreenWidgetItemType = {
  icon: <ScreenFlowItemIcons.DateTime />,
  label: 'Date & Time',
  type: ScreenElementType.Input,
  component: ScreenElementComponent.Input,
  props: {
    label: 'Date & Time',
    inputType: 'date-time',
  },
  children: [],
}
