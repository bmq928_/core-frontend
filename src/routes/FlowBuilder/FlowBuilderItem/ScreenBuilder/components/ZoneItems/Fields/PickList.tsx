import * as React from 'react'
import { classnames } from '../../../../../../../common/classnames'
import { SelectOption } from '../../../../../../../components/NSelect/model'
import { NMultiSelect } from '../../../../../../../components/NSelect/NMultiSelect'
import { NSingleSelect } from '../../../../../../../components/NSelect/NSingleSelect'
import { NTextInput } from '../../../../../../../components/NTextInput/NTextInput'
import { ContentValue } from '../../../../../../../services/api/models'
import { ZoneProps } from '../../../../../../LayoutBuilder/utils/models'
import { MultiFlowVariablesPicker } from '../../../../../FlowBuilderVariable/MultiFlowVariablesPicker'
import { useScreenAction, useScreenBuilder } from '../../../contexts/ScreenBuilderContext'
import { useElement } from '../../../hooks/useElement'
import { ROOT_ELEMENT } from '../../../utils/builder'
import { ScreenElementComponent, ScreenElementType, ScreenWidgetItemType } from '../../../utils/models'
import { BaseRightWidget } from '../../BaseRightWidget'
import { ScreenFlowItemIcons } from '../../ScreenFlowItemIcons'
import { AccordionWidget, Widget } from '../../Sidebar'
import { FieldWrapper as Wrapper, Placeholder } from '../StyledElements'
import { ZoneItemLayout } from '../ZoneItemLayout'

const PICKLIST_TYPE_OPTIONS: SelectOption[] = [
  { label: 'Single', value: 'single' },
  { label: 'Multiple', value: 'multi' },
]

export function PickList({ elementId, parentId, index }: ZoneProps) {
  const { elements, selectedElement } = useScreenBuilder()
  const { selectElement, configElement, setElementName } = useScreenAction()
  const [{ isMouseOver, mousePosition }, ref] = useElement({ elementId, parentId, index })

  const element = elements[elementId]
  const isSelected = selectedElement === elementId
  const { options, defaultSelectValue, defaultValue: _, selectionType, isRequired, ...restProps } = element.props

  const mapOptions: SelectOption[] = (options || []).map(op => ({ value: op.value, label: op.value }))
  const extOptions: SelectOption[] =
    selectionType === 'multi' ? mapOptions : [...mapOptions, { value: '', label: 'No default' }]

  return (
    <ZoneItemLayout
      shouldRenderSidebar={isSelected}
      sidebar={
        <React.Fragment>
          <AccordionWidget
            title="Pick list"
            addPadContent
            onBack={() => {
              selectElement(ROOT_ELEMENT)
            }}>
            <BaseRightWidget
              elementId={elementId}
              element={element}
              configElement={configElement}
              setElementName={setElementName}
            />
            <Widget title="Type">
              <NSingleSelect
                options={PICKLIST_TYPE_OPTIONS}
                value={element.props.selectionType}
                onValueChange={newVal => {
                  configElement(elementId, { selectionType: newVal, defaultIndex: undefined })
                }}
              />
            </Widget>
            <Widget title="Caption">
              <NTextInput
                value={element.props.caption}
                onChange={e => {
                  configElement(elementId, { caption: e.target.value })
                }}
              />
            </Widget>
            <Widget title="Default Value">
              {selectionType === 'multi' ? (
                <NMultiSelect
                  fullWidth
                  values={defaultSelectValue ? defaultSelectValue : []}
                  options={extOptions}
                  onValuesChange={newVal => {
                    configElement(elementId, { defaultSelectValue: newVal })
                  }}
                />
              ) : (
                <NSingleSelect
                  value={defaultSelectValue ? defaultSelectValue[0] : undefined}
                  options={extOptions}
                  onValueChange={newVal => {
                    configElement(elementId, { defaultSelectValue: [newVal] })
                  }}
                />
              )}
            </Widget>
            <Widget title="Options">
              <MultiFlowVariablesPicker
                flowVariables={options || []}
                setFlowVariables={(newOptions: ContentValue[]) => {
                  configElement(elementId, { options: newOptions })
                }}
              />
            </Widget>
          </AccordionWidget>
        </React.Fragment>
      }>
      <Wrapper
        ref={ref}
        className={classnames([
          isMouseOver && 'is-mouse-over',
          mousePosition && `is-sort ${mousePosition}`,
          isSelected && 'is-selected',
        ])}
        onClick={e => {
          e.stopPropagation()
          selectElement(elementId)
        }}>
        {selectionType === 'multi' ? (
          <NMultiSelect
            required={isRequired}
            disabled
            values={defaultSelectValue ? defaultSelectValue : []}
            options={extOptions}
            {...restProps}
          />
        ) : (
          <NSingleSelect
            required={isRequired}
            disabled
            value={defaultSelectValue ? defaultSelectValue[0] : undefined}
            options={extOptions}
            {...restProps}
          />
        )}
        <Placeholder mousePosition={mousePosition} />
      </Wrapper>
    </ZoneItemLayout>
  )
}

export const widgetPickList: ScreenWidgetItemType = {
  icon: <ScreenFlowItemIcons.PickList />,
  label: 'Pick list',
  type: ScreenElementType.Input,
  component: ScreenElementComponent.Selection,
  props: {
    label: 'Pick list',
    selectionType: 'single',
    options: [],
  },
  children: [],
}
