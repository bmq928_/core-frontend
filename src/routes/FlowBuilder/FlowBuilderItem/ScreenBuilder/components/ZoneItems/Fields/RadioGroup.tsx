import * as React from 'react'
import styled from 'styled-components'
import { classnames } from '../../../../../../../common/classnames'
import { spacing } from '../../../../../../../components/GlobalStyle'
import { NRadio } from '../../../../../../../components/NRadio/NRadio'
import { SelectOption } from '../../../../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../../../../components/NSelect/NSingleSelect'
import { NTypography, RequiredIndicator } from '../../../../../../../components/NTypography'
import { ContentValue } from '../../../../../../../services/api/models'
import { ZoneProps } from '../../../../../../LayoutBuilder/utils/models'
import { MultiFlowVariablesPicker } from '../../../../../FlowBuilderVariable/MultiFlowVariablesPicker'
import { useScreenAction, useScreenBuilder } from '../../../contexts/ScreenBuilderContext'
import { useElement } from '../../../hooks/useElement'
import { ROOT_ELEMENT } from '../../../utils/builder'
import { ScreenElementComponent, ScreenElementType, ScreenWidgetItemType } from '../../../utils/models'
import { BaseRightWidget } from '../../BaseRightWidget'
import { ScreenFlowItemIcons } from '../../ScreenFlowItemIcons'
import { AccordionWidget, Widget } from '../../Sidebar'
import { FieldWrapper as Wrapper, Placeholder } from '../StyledElements'
import { ZoneItemLayout } from '../ZoneItemLayout'

const RadioContainer = styled.div`
  display: flex;
  flex-direction: column;

  .radio {
    margin-top: ${spacing('md')};
  }
`

export function RadioGroup({ elementId, parentId, index }: ZoneProps) {
  const { elements, selectedElement } = useScreenBuilder()
  const { selectElement, configElement, setElementName } = useScreenAction()
  const [{ isMouseOver, mousePosition }, ref] = useElement({ elementId, parentId, index })

  const element = elements[elementId]
  const isSelected = selectedElement === elementId
  const { label, name, options, defaultValue: _, defaultSelectValue, isRequired } = element.props

  const mapOptions: SelectOption[] = (options || []).map(op => ({ value: op.value, label: op.value }))
  const extOptions: SelectOption[] = [...mapOptions, { value: '', label: 'No default' }]

  return (
    <ZoneItemLayout
      shouldRenderSidebar={isSelected}
      sidebar={
        <React.Fragment>
          <AccordionWidget
            title="Pick list"
            addPadContent
            onBack={() => {
              selectElement(ROOT_ELEMENT)
            }}>
            <BaseRightWidget
              elementId={elementId}
              element={element}
              configElement={configElement}
              setElementName={setElementName}
            />
            <Widget title="Default Value">
              <NSingleSelect
                value={defaultSelectValue ? defaultSelectValue[0] : undefined}
                options={extOptions}
                onValueChange={newVal => {
                  configElement(elementId, { defaultSelectValue: [newVal] })
                }}
              />
            </Widget>
            <Widget title="Options">
              <MultiFlowVariablesPicker
                flowVariables={options || []}
                setFlowVariables={(newOptions: ContentValue[]) => {
                  configElement(elementId, { options: newOptions })
                }}
              />
            </Widget>
          </AccordionWidget>
        </React.Fragment>
      }>
      <Wrapper
        ref={ref}
        className={classnames([
          isMouseOver && 'is-mouse-over',
          mousePosition && `is-sort ${mousePosition}`,
          isSelected && 'is-selected',
        ])}
        onClick={e => {
          e.stopPropagation()
          selectElement(elementId)
        }}>
        <RadioContainer>
          <NTypography.InputLabel>
            {isRequired && <RequiredIndicator>*</RequiredIndicator>}
            {label}
          </NTypography.InputLabel>
          {(options || []).map((op: ContentValue) => {
            return (
              <NRadio
                required={isRequired}
                className="radio"
                key={`${elementId}_radio_group_${op.value}`}
                title={op.value}
                name={name || elementId}
                value={op.value}
                checked={defaultSelectValue && defaultSelectValue[0] === op.value}
                onChange={() => {}}
              />
            )
          })}
        </RadioContainer>
        <Placeholder mousePosition={mousePosition} />
      </Wrapper>
    </ZoneItemLayout>
  )
}

export const widgetRadioGroup: ScreenWidgetItemType = {
  icon: <ScreenFlowItemIcons.Radio />,
  label: 'Radio group',
  type: ScreenElementType.Input,
  component: ScreenElementComponent.Selection,
  props: {
    label: 'Radio group',
    selectionType: 'single',
    options: [],
    isRadio: true,
  },
  children: [],
}
