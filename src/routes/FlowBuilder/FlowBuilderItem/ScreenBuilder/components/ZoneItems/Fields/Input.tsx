import * as React from 'react'
import { classnames } from '../../../../../../../common/classnames'
import { NCheckbox } from '../../../../../../../components/NCheckbox/NCheckbox'
import { NDivider } from '../../../../../../../components/NDivider'
import { SelectOption } from '../../../../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../../../../components/NSelect/NSingleSelect'
import { NTextInput } from '../../../../../../../components/NTextInput/NTextInput'
import { ZoneProps } from '../../../../../../LayoutBuilder/utils/models'
import { FlowVariablePicker } from '../../../../../FlowBuilderVariable/FlowVariablePicker'
import { renderVariableValue } from '../../../../../utils/functions'
import { useScreenAction, useScreenBuilder } from '../../../contexts/ScreenBuilderContext'
import { useElement } from '../../../hooks/useElement'
import { ROOT_ELEMENT } from '../../../utils/builder'
import { ScreenElementComponent, ScreenElementType, ScreenWidgetItemType } from '../../../utils/models'
import { BaseRightWidget } from '../../BaseRightWidget'
import { ScreenFlowItemIcons } from '../../ScreenFlowItemIcons'
import { AccordionWidget, Widget } from '../../Sidebar'
import { FieldWrapper as Wrapper, Placeholder } from '../StyledElements'
import { ZoneItemLayout } from '../ZoneItemLayout'

const INPUT_TYPE_OPTIONS: SelectOption[] = [
  { label: 'Text', value: 'string' },
  { label: 'Float', value: 'float' },
  { label: 'Integer', value: 'integer' },
]

export function Input({ elementId, parentId, index }: ZoneProps) {
  const { elements, selectedElement } = useScreenBuilder()
  const { selectElement, configElement, setElementName } = useScreenAction()
  const [{ isMouseOver, mousePosition }, ref] = useElement({ elementId, parentId, index })
  const element = elements[elementId]
  const isSelected = selectedElement === elementId

  const { defaultValue, inputType, isRequired, ...restProps } = element.props

  return (
    <ZoneItemLayout
      shouldRenderSidebar={isSelected}
      sidebar={
        <React.Fragment>
          <AccordionWidget
            title="Text Input"
            addPadContent
            onBack={() => {
              selectElement(ROOT_ELEMENT)
            }}>
            <BaseRightWidget
              elementId={elementId}
              element={element}
              configElement={configElement}
              setElementName={setElementName}
            />
            <Widget required title="Type">
              <NSingleSelect
                options={INPUT_TYPE_OPTIONS}
                value={inputType}
                onValueChange={newVal => {
                  configElement(elementId, { inputType: newVal })
                }}
              />
            </Widget>
            <Widget title="Placeholder">
              <NTextInput
                value={element.props.placeholder}
                onChange={e => {
                  configElement(elementId, { placeholder: e.target.value })
                }}
              />
            </Widget>
            <Widget title="Caption">
              <NTextInput
                value={element.props.caption}
                onChange={e => {
                  configElement(elementId, { caption: e.target.value })
                }}
              />
            </Widget>
            <Widget title="Default Value">
              <FlowVariablePicker
                label="Default Value"
                selectedVariable={defaultValue}
                onVariableSelected={varOption => {
                  configElement(elementId, { defaultValue: varOption })
                }}
              />
            </Widget>
            <NDivider />
            <NCheckbox
              title="Is Hidden?"
              checked={element.props.isHidden}
              onChange={e => {
                configElement(elementId, { isHidden: e.target.checked })
              }}
            />
          </AccordionWidget>
        </React.Fragment>
      }>
      <Wrapper
        ref={ref}
        className={classnames([
          isMouseOver && 'is-mouse-over',
          mousePosition && `is-sort ${mousePosition}`,
          isSelected && 'is-selected',
        ])}
        onClick={e => {
          e.stopPropagation()
          selectElement(elementId)
        }}>
        <NTextInput
          required={isRequired}
          disabled
          type={inputType}
          defaultValue={renderVariableValue(defaultValue)}
          {...restProps}
        />
        <Placeholder mousePosition={mousePosition} />
      </Wrapper>
    </ZoneItemLayout>
  )
}

export const widgetInput: ScreenWidgetItemType = {
  icon: <ScreenFlowItemIcons.TextInput />,
  label: 'Text Input',
  type: ScreenElementType.Input,
  component: ScreenElementComponent.Input,
  props: {
    label: 'Text Input',
    inputType: 'string',
    isHidden: false,
  },
  children: [],
}
