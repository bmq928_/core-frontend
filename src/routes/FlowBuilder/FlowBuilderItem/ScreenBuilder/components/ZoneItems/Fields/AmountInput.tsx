import * as React from 'react'
import { classnames } from '../../../../../../../common/classnames'
import { NCheckbox } from '../../../../../../../components/NCheckbox/NCheckbox'
import { NDivider } from '../../../../../../../components/NDivider'
import { NTextInput } from '../../../../../../../components/NTextInput/NTextInput'
import { ZoneProps } from '../../../../../../LayoutBuilder/utils/models'
import { useScreenAction, useScreenBuilder } from '../../../contexts/ScreenBuilderContext'
import { useElement } from '../../../hooks/useElement'
import { ROOT_ELEMENT } from '../../../utils/builder'
import { ScreenElementComponent, ScreenElementType, ScreenWidgetItemType } from '../../../utils/models'
import { ScreenFlowItemIcons } from '../../ScreenFlowItemIcons'
import { AccordionWidget, Widget } from '../../Sidebar'
import { FieldWrapper as Wrapper, Placeholder } from '../StyledElements'
import { ZoneItemLayout } from '../ZoneItemLayout'

export const AmountInput = React.memo(function Node({ elementId, parentId, index, canDrag, canDrop }: ZoneProps) {
  const { elements, selectedElement } = useScreenBuilder()
  const { selectElement, configElement } = useScreenAction()
  const element = elements[elementId]
  const isSelected = selectedElement === elementId
  const [{ isOver, isMouseOver, mousePosition }, ref] = useElement({ elementId, parentId, index, canDrop, canDrag })

  return (
    <ZoneItemLayout
      shouldRenderSidebar={isSelected}
      sidebar={
        <React.Fragment>
          <AccordionWidget
            title="Amount Input"
            addPadContent
            onBack={() => {
              selectElement(ROOT_ELEMENT)
            }}>
            <Widget required title="Label">
              <NTextInput
                required
                value={element.props.label}
                onChange={e => {
                  configElement(elementId, { label: e.target.value })
                }}
              />
            </Widget>
            <Widget required title="API Name">
              <NTextInput required disabled defaultValue={element.name} />
            </Widget>
            <NDivider />
            <NCheckbox
              title="Is required"
              name="isRequired"
              checked={!!element.props.isRequired}
              onChange={e => {
                configElement(elementId, { isRequired: e.target.checked })
              }}
            />
            <NDivider />
            <NCheckbox
              title="Is withdrawal?"
              checked={element.props.isWithdrawal}
              onChange={e => {
                configElement(elementId, { isWithdrawal: e.target.checked })
              }}
            />
            <NDivider />
            <NCheckbox
              title="Is USD?"
              checked={element.props.isUSD}
              onChange={e => {
                configElement(elementId, { isUSD: e.target.checked })
              }}
            />
          </AccordionWidget>
        </React.Fragment>
      }>
      <Wrapper
        ref={ref}
        className={classnames([
          isOver && 'is-over',
          isMouseOver && 'is-mouse-over',
          mousePosition && `is-sort ${mousePosition}`,
          isSelected && 'is-selected',
        ])}
        onClick={e => {
          e.stopPropagation()
          selectElement(elementId)
        }}>
        <NTextInput disabled label={element.props.label} required={element.props.isRequired} />
        <Placeholder mousePosition={mousePosition} />
      </Wrapper>
    </ZoneItemLayout>
  )
})

AmountInput.displayName = 'AmountInput'

export const widgetAmountInput: ScreenWidgetItemType & { name: string } = {
  icon: <ScreenFlowItemIcons.PickList />,
  label: 'Money Input',
  type: ScreenElementType.Input,
  component: ScreenElementComponent.AmountInput,
  name: 'amount',
  props: {
    label: 'Amount',
    isWithdrawal: true,
    isUSD: false,
  },
  children: [],
}
