import * as React from 'react'
import styled from 'styled-components'
import { NButton } from '../../../../../../components/NButton/NButton'
import { Portal } from '../../../../../../components/Portal'
import { useScreenAction, useScreenBuilder } from '../../contexts/ScreenBuilderContext'
import { ROOT_ELEMENT } from '../../utils/builder'
// import { ZoneElement } from '../DroppableZone'
import { sidebarPortalRef } from '../RightSidebar'

const ButtonWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 1rem;
`

type ZoneItemLayoutProps = {
  shouldRenderSidebar?: boolean
  sidebar?: React.ReactNode
  children?: React.ReactNode
}

export function ZoneItemLayout({ children, sidebar, shouldRenderSidebar = false }: ZoneItemLayoutProps) {
  const { removeElement } = useScreenAction()
  const { selectedElement } = useScreenBuilder()
  return (
    <React.Fragment>
      {children}
      <Portal mountNode={sidebarPortalRef}>
        {shouldRenderSidebar && (
          <div
            onClick={e => {
              e.stopPropagation()
            }}>
            {sidebar}
            {selectedElement !== ROOT_ELEMENT && (
              <ButtonWrapper>
                <NButton
                  size="small"
                  type="primary"
                  onClick={() => {
                    selectedElement && removeElement({ elementId: selectedElement })
                  }}>
                  Remove
                </NButton>
              </ButtonWrapper>
            )}
          </div>
        )}
      </Portal>
    </React.Fragment>
  )
}
