import React, { useState } from 'react'
import { useQuery } from 'react-query'
import styled from 'styled-components'
import { classnames } from '../../../../../../../common/classnames'
import { ActionMetadataMapInputModal } from '../../../../../../../components/Builder/ActionMetadataMapInputModal'
import { spacing } from '../../../../../../../components/GlobalStyle'
import { NButton } from '../../../../../../../components/NButton/NButton'
import { NDivider } from '../../../../../../../components/NDivider'
import { SelectOption } from '../../../../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../../../../components/NSelect/NSingleSelect'
import { NToast } from '../../../../../../../components/NToast'
import { typography } from '../../../../../../../components/NTypography'
import { useSearchText } from '../../../../../../../hooks/useSearchText'
import { APIServices } from '../../../../../../../services/api'
import { IntegrationsQueryKeys } from '../../../../../../../services/api/endpoints/integrations'
import { ActionMetadataResponse } from '../../../../../../../services/api/models'
import { ZoneProps } from '../../../../../../LayoutBuilder/utils/models'
import { FlowVariablePicker } from '../../../../../FlowBuilderVariable/FlowVariablePicker'
import { useScreenAction, useScreenBuilder } from '../../../contexts/ScreenBuilderContext'
import { useElement } from '../../../hooks/useElement'
import { ROOT_ELEMENT } from '../../../utils/builder'
import { ScreenElementComponent, ScreenElementType, ScreenWidgetItemType } from '../../../utils/models'
import { BaseRightWidget } from '../../BaseRightWidget'
import { ScreenFlowItemIcons } from '../../ScreenFlowItemIcons'
import { AccordionWidget, Widget } from '../../Sidebar'
import { LayoutWrapper } from '../StyledElements'
import { ZoneItemLayout } from '../ZoneItemLayout'

export const StyledWrapper = styled(LayoutWrapper)`
  flex-direction: column;
  padding: ${spacing('sm')};
`

export const Header = styled.div`
  display: flex;
  align-items: center;
`

export const HeaderTxt = styled.p`
  ${typography('s600')}
`

export const PlaceholderTxt = styled.p`
  ${typography('x-small-ui-text')}
`

export function FileDropzone({ elementId, parentId, index, canDrop, canDrag }: ZoneProps) {
  const [{ isOver, isMouseOver, mousePosition }, ref] = useElement({ elementId, parentId, index, canDrop, canDrag })

  const [{ searchText, searchValue }, handleSearchChange] = useSearchText()
  // get action metadata data
  const { data, isFetching: isLoadingActionMetadata } = useQuery(
    [IntegrationsQueryKeys.getAllActionMetadata, { searchText }],
    APIServices.Integrations.getAllActionMetadata,
    {
      keepPreviousData: true,
      select: data => {
        const { selectOptions, getDataById } = data.data.data.reduce(
          (prev, cur) => {
            return {
              selectOptions: [...prev.selectOptions, { value: cur.name, label: cur.displayName }],
              getDataById: { ...prev.getDataById, [cur.name]: cur },
            }
          },
          {
            selectOptions: [] as SelectOption[],
            getDataById: {} as Record<string, ActionMetadataResponse>,
          },
        )

        return {
          actionMetadataOptions: selectOptions,
          actionMetadataById: getDataById,
        }
      },
    },
  )

  //
  const [showInputMap, setShowInputMap] = useState(false)

  const { elements, selectedElement } = useScreenBuilder()
  const { selectElement, configElement, setElementName } = useScreenAction()
  const isSelected = selectedElement === elementId

  const element = elements[elementId]

  return (
    <ZoneItemLayout
      shouldRenderSidebar={isSelected}
      sidebar={
        <>
          <AccordionWidget
            title="File Dropzone"
            addPadContent
            onBack={() => {
              selectElement(ROOT_ELEMENT)
            }}>
            <BaseRightWidget
              elementId={elementId}
              element={element}
              configElement={configElement}
              setElementName={setElementName}
            />
            <Widget title="Action metadata">
              <NSingleSelect
                label="Select Action metadata"
                options={data?.actionMetadataOptions || []}
                value={element.props.actionMetadata}
                onValueChange={newValue => {
                  configElement(elementId, { actionMetadata: newValue, inputMap: {} })
                }}
                fullWidth
                isSearchable
                searchValue={searchValue}
                onSearchValueChange={handleSearchChange}
                isLoading={isLoadingActionMetadata}
              />
              <NDivider size="md" />
              <NButton
                disabled={!element.props.actionMetadata}
                size="small"
                type="outline-3"
                block
                onClick={() => setShowInputMap(true)}>
                Map input
              </NButton>
              <ActionMetadataMapInputModal
                visible={showInputMap}
                setVisible={setShowInputMap}
                actionMetadata={
                  element.props.actionMetadata ? data?.actionMetadataById[element.props.actionMetadata] : undefined
                }
                defaultInputMap={element.props.inputMap || {}}
                onSubmit={newInputMap => {
                  configElement(elementId, { inputMap: newInputMap })
                  setShowInputMap(false)
                }}
              />
            </Widget>
            <Widget title="Assign Record">
              <FlowVariablePicker
                label="Pick assigned record"
                placeholder="Select Record variable"
                noEnterValue
                selectedVariable={element.props.record}
                onVariableSelected={(varOption, flowVar) => {
                  if (!flowVar || flowVar.type !== 'record') {
                    NToast.error({ title: `Variable type should be record` })
                    return
                  }
                  configElement(elementId, { record: varOption })
                }}
              />
            </Widget>
          </AccordionWidget>
        </>
      }>
      <StyledWrapper
        ref={ref}
        className={classnames([
          isOver && 'is-over',
          isMouseOver && 'is-mouse-over',
          mousePosition && `is-sort ${mousePosition}`,
          isSelected && 'is-selected',
        ])}
        onClick={e => {
          e.stopPropagation()
          selectElement(elementId)
        }}>
        <Header>
          <ScreenFlowItemIcons.PickList />
          <NDivider vertical size="md" />
          <div>
            <HeaderTxt>File Dropzone component:</HeaderTxt>
          </div>
        </Header>
        <NDivider size="md" />
        <PlaceholderTxt>This is a placeholder. File Dropzone don't run in builder.</PlaceholderTxt>
      </StyledWrapper>
    </ZoneItemLayout>
  )
}

export const widgetFileDropzone: ScreenWidgetItemType = {
  icon: <ScreenFlowItemIcons.PickList />,
  label: 'File Dropzone',
  type: ScreenElementType.Component,
  component: ScreenElementComponent.FileDropzone,
  props: {
    label: 'File Dropzone',
    inputMap: {},
  },
  children: [],
}
