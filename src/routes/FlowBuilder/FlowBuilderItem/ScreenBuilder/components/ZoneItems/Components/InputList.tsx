import React from 'react'
import styled from 'styled-components'
import { classnames } from '../../../../../../../common/classnames'
import { spacing } from '../../../../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../../../../components/Icons'
import { NDivider } from '../../../../../../../components/NDivider'
import { NTypography } from '../../../../../../../components/NTypography'
import { EmptyMessage } from '../../../../../../LayoutBuilder/components/ZoneItems/StyledElements'
import { ROOT_ELEMENT } from '../../../../../../LayoutBuilder/utils/builder'
import { ZoneProps } from '../../../../../../LayoutBuilder/utils/models'
import { FlowVariablePicker } from '../../../../../FlowBuilderVariable/FlowVariablePicker'
import { useScreenAction, useScreenBuilder } from '../../../contexts/ScreenBuilderContext'
import { useElement } from '../../../hooks/useElement'
import { ScreenElementComponent, ScreenElementType, ScreenWidgetItemType } from '../../../utils/models'
import { BaseRightWidget } from '../../BaseRightWidget'
import { ScreenFlowItemIcons } from '../../ScreenFlowItemIcons'
import { AccordionWidget, Widget } from '../../Sidebar'
import { LayoutWrapper, Placeholder } from '../StyledElements'
import { ZoneItemLayout } from '../ZoneItemLayout'
import { ZONE_ELEMENTS, ZONE_ELEMENTS_INPUTS, ZONE_ELEMENTS_SELECTIONS } from '../zoneItems'

const StyledWrapper = styled(LayoutWrapper)`
  flex-direction: column;
  padding: ${spacing('sm')};
`

const InputRow = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  min-height: 86px;
  & > *:not(:last-child) {
    margin-right: ${spacing('md')};
  }
`

const TrashBtn = styled.div`
  cursor: pointer;
`

export function InputList({ elementId, parentId, index, canDrop, canDrag }: ZoneProps) {
  const { elements, selectedElement } = useScreenBuilder()
  const { selectElement, configElement, setElementName } = useScreenAction()
  const [{ isOver, isMouseOver, mousePosition }, ref] = useElement({ elementId, parentId, index, canDrop, canDrag })
  const element = elements[elementId]
  const isSelected = selectedElement === elementId

  const { defaultValue } = element.props
  const { children } = element

  return (
    <ZoneItemLayout
      shouldRenderSidebar={isSelected}
      sidebar={
        <>
          <AccordionWidget
            title="Text Input"
            addPadContent
            onBack={() => {
              selectElement(ROOT_ELEMENT)
            }}>
            <BaseRightWidget
              elementId={elementId}
              element={element}
              configElement={configElement}
              setElementName={setElementName}
            />
            <Widget title="Default Value">
              <FlowVariablePicker
                label="Default Value"
                noEnterValue
                selectedVariable={defaultValue}
                onVariableSelected={varOption => {
                  configElement(elementId, { defaultValue: varOption })
                }}
              />
            </Widget>
          </AccordionWidget>
        </>
      }>
      <StyledWrapper
        ref={ref}
        className={classnames([
          isOver && 'is-over',
          isMouseOver && 'is-mouse-over',
          mousePosition && `is-sort ${mousePosition}`,
          isSelected && 'is-selected',
        ])}
        onClick={e => {
          e.stopPropagation()
          selectElement(elementId)
        }}>
        <NTypography.InputLabel>{element.props.label || '{Label}'}</NTypography.InputLabel>
        <NDivider size="sm" />
        <InputRow>
          {children.map((id, idx) => {
            const e = elements[id]
            let Component: ((props: ZoneProps) => JSX.Element) | undefined
            if (e.props.inputType) {
              Component = ZONE_ELEMENTS_INPUTS[e.props.inputType]
            } else if (e.props.selectionType) {
              Component = ZONE_ELEMENTS_SELECTIONS[e.props.isRadio ? 'Radio' : 'PickList']
            } else {
              Component = ZONE_ELEMENTS[e.component]
            }

            return (
              <div key={id} style={{ flex: 2 }}>
                <Component elementId={id} parentId={elementId} index={idx} />
              </div>
            )
          })}
          <EmptyMessage style={{ flex: 1 }}>Drop an item here to start</EmptyMessage>
          <TrashBtn>
            <GlobalIcons.Trash />
          </TrashBtn>
        </InputRow>
        <Placeholder mousePosition={mousePosition} />
      </StyledWrapper>
    </ZoneItemLayout>
  )
}

export const widgetInputList: ScreenWidgetItemType = {
  icon: <ScreenFlowItemIcons.PickList />,
  label: 'Input List',
  type: ScreenElementType.Component,
  component: ScreenElementComponent.InputList,
  props: {
    label: 'Input List',
  },
  children: [],
}
