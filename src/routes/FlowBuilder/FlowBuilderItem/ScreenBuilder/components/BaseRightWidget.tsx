import React, { FC } from 'react'
import styled from 'styled-components'
import { spacing } from '../../../../../components/GlobalStyle'
import { NCheckbox } from '../../../../../components/NCheckbox/NCheckbox'
import { NTextInput } from '../../../../../components/NTextInput/NTextInput'
import { InputProps, SelectionProps } from '../../../../../services/api/models'
import { getNameFromDisplayName } from '../../../../../utils/utils'
import { ScreenElement } from '../utils/models'
import { Widget } from './Sidebar'

type Props = {
  elementId: string
  element: ScreenElement
  configElement: (elementId: string, props: Record<string, any>) => void
  setElementName: (elementId: string, name?: string) => void
  noRequired?: boolean
}

const PadCheckbox = styled(NCheckbox)`
  padding: ${spacing('sm')} 0;
`

export const BaseRightWidget: FC<Props> = ({ elementId, element, configElement, setElementName, noRequired }) => {
  const { label, isRequired } = element.props as InputProps | SelectionProps

  return (
    <>
      <Widget required title="Label">
        <NTextInput
          required
          value={label}
          onChange={e => {
            configElement(elementId, { label: e.target.value })
          }}
          onBlur={e => {
            const formatName = getNameFromDisplayName(e.target.value)
            setElementName(elementId, formatName)
          }}
        />
      </Widget>
      <Widget required title="API Name">
        <NTextInput
          required
          value={element.name}
          onChange={e => {
            setElementName(elementId, e.target.value)
          }}
        />
      </Widget>
      {!noRequired && (
        <PadCheckbox
          title="Is required"
          name="isRequired"
          checked={!!isRequired}
          onChange={e => {
            configElement(elementId, { isRequired: e.target.checked })
          }}
        />
      )}
    </>
  )
}
