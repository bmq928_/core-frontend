import React, { FC, useState } from 'react'
import styled, { useTheme } from 'styled-components'
import { color } from '../../../../components/GlobalStyle'
import { NDivider } from '../../../../components/NDivider'
import { NModal } from '../../../../components/NModal/NModal'
import { FlowIcons } from '../../components/Icons'
import { FlowIconContainerClassName } from '../../utils/constants'
import { FlowNodeModalProps, NodeType, WidgetItemType } from '../../utils/types'
import { FlowIconContainer } from '../CommonStyledComponents'
import { LeftSidebar } from './components/LeftSidebar'
import { RightSidebar } from './components/RightSidebar'
import { Root } from './components/ZoneItems/Root'
import { ScreenBuilderProvider, useScreenBuilder } from './contexts/ScreenBuilderContext'

const ScreenBuilderContainer = styled.div`
  display: flex;
  height: calc(100vh - 152px * 2);
  background-color: ${color('Neutral200')};
`

export const ScreenBuilder: FC<FlowNodeModalProps> = ({ nodeModal, isSubmitting, onCancel, onSubmit }) => {
  return (
    <ScreenBuilderProvider defaultValue={nodeModal.data}>
      <ScreenBuilderModal nodeModal={nodeModal} isSubmitting={isSubmitting} onCancel={onCancel} onSubmit={onSubmit} />
    </ScreenBuilderProvider>
  )
}

const ScreenBuilderModal: FC<FlowNodeModalProps> = ({ nodeModal, isSubmitting, onCancel, onSubmit }) => {
  const theme = useTheme()
  const { elements } = useScreenBuilder()

  const [flowName, setFlowName] = useState(nodeModal.data?.name || '')
  const [flowDisplayName, setFlowDisplayName] = useState(nodeModal.data?.displayName || '')
  const [flowDescription, setFlowDescription] = useState(nodeModal.data?.description || '')

  const handleSubmit = () => {
    // TODO: Handle error on UI
    if (!flowName.length || !flowDisplayName.length) {
      alert(`Missing params:\n${!flowName.length && `Name\n`}${!flowDisplayName.length && `Display name\n`}`)
      return
    }

    onSubmit({
      name: flowName,
      displayName: flowDisplayName,
      description: flowDescription,
      script: elements,
    })
  }

  return (
    <>
      <NModal.Header title={`${nodeModal.nodeId ? 'Edit' : 'New'} Screen`} onClose={onCancel} />
      <NModal.Body>
        <ScreenBuilderContainer>
          <LeftSidebar />
          <Root
            isEditing={!!nodeModal.nodeId}
            flowName={flowName}
            flowDisplayName={flowDisplayName}
            flowDescription={flowDescription}
            setFlowName={setFlowName}
            setFlowDisplayName={setFlowDisplayName}
            setFlowDescription={setFlowDescription}
          />
          <RightSidebar />
        </ScreenBuilderContainer>
      </NModal.Body>
      <NDivider size={1} lineSize={1} lineColor={color('Neutral300')({ theme })} />
      <NModal.Footer isLoading={isSubmitting} onCancel={onCancel} onFinish={handleSubmit} />
    </>
  )
}

export const screenBuilderWidget: WidgetItemType = {
  leftIcon: <FlowIcons.Screen />,
  flowIcon: (
    <FlowIconContainer className={FlowIconContainerClassName}>
      <FlowIcons.Screen />
    </FlowIconContainer>
  ),
  label: 'Screen',
  type: NodeType.Screen,
  data: {
    name: '',
    displayName: '',
    description: '',
  },
}
