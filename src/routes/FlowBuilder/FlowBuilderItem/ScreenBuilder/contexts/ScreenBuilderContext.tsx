import * as React from 'react'
import { CommonWidgetDataType } from '../../../utils/types'
import { InitRootElement } from '../components/ZoneItems/Root'
import { ROOT_ELEMENT } from '../utils/builder'
import { ScreenElement } from '../utils/models'

function appendElementAt<T = string>(element: T, target: T[], position: number): T[] {
  return [...target.slice(0, position), element, ...target.slice(position)]
}

type ScreenBuilderContextType = {
  elements: Record<string, ScreenElement>
  selectedElement?: string
}

const ScreenBuilderContext = React.createContext<ScreenBuilderContextType | string>(
  'useScreenBuilder should be used inside ScreenBuilderProvider',
)

const initialState = {
  elements: {
    [ROOT_ELEMENT]: InitRootElement,
  },
} as ScreenBuilderContextType

enum Actions {
  LoadScript,
  AddElement,
  MoveElement,
  SelectElement,
  SetElementName,
  ConfigElement,
  RemoveElement,
}

type Action =
  | {
      type: Actions.LoadScript
      payload: {
        script: Record<string, ScreenElement>
      }
    }
  | {
      type: Actions.AddElement
      payload: {
        element: ScreenElement
        source: { id: string }
        target: { id: string; index?: number }
        selectElementId?: string
      }
    }
  | {
      type: Actions.MoveElement
      payload: {
        elementId: string
        source: { id: string }
        target: { id: string; index?: number }
      }
    }
  | {
      type: Actions.SelectElement
      payload: string
    }
  | {
      type: Actions.SetElementName
      payload: { elementId: string; name?: string }
    }
  | {
      type: Actions.ConfigElement
      payload: { elementId: string; props: Record<string, any> }
    }
  | {
      type: Actions.RemoveElement
      payload: { elementId: string; selectElementId?: string }
    }

function reducer(store: ScreenBuilderContextType, action: Action) {
  switch (action.type) {
    //
    case Actions.LoadScript: {
      const { script } = action.payload
      // validate script
      if (!script[ROOT_ELEMENT]) {
        return store
      }

      return {
        ...store,
        elements: script,
      }
    }
    //
    case Actions.AddElement: {
      const {
        element,
        target: { id: targetId, index },
        selectElementId,
      } = action.payload

      if (!element.id) {
        throw new Error('Element missing id')
      }

      const target = store.elements[targetId]
      const children = target.children || []

      return {
        ...store,
        elements: {
          ...store.elements,
          [targetId]: {
            ...target,
            children: index === undefined ? children.concat(element.id) : appendElementAt(element.id, children, index),
          },
          [element.id]: element,
        },
        selectedElement: selectElementId || element.id,
      }
    }
    //
    case Actions.MoveElement: {
      const {
        elementId,
        source: { id: sourceId },
        target: { id: targetId, index },
      } = action.payload

      const source = store.elements[sourceId]
      const target = store.elements[targetId]

      // same parent
      if (sourceId === targetId) {
        const children = (target.children || []).filter(i => i !== elementId)
        const newElement = {
          ...store.elements,
          [targetId]: {
            ...target,
            children: index === undefined ? children.concat(elementId) : appendElementAt(elementId, children, index),
          },
        }
        return {
          ...store,
          elements: newElement,
        }
      }
      //
      const children = target.children || []

      const tempElement = {
        ...store.elements,
        [targetId]: {
          ...target,
          children: index === undefined ? children.concat(elementId) : appendElementAt(elementId, children, index),
        },
        [sourceId]: {
          ...source,
          children: (source.children || []).filter(i => i !== elementId),
        },
      }

      return {
        ...store,
        elements: tempElement,
      }
    }
    //
    case Actions.SelectElement: {
      if (action.payload === store.selectedElement) {
        return store
      }
      return {
        ...store,
        selectedElement: action.payload,
      }
    }
    //
    case Actions.SetElementName: {
      const { name, elementId } = action.payload
      const element = store.elements[elementId]
      return {
        ...store,
        elements: {
          ...store.elements,
          [elementId]: {
            ...element,
            name,
          },
        },
      }
    }
    //
    case Actions.ConfigElement: {
      const { props, elementId } = action.payload
      const element = store.elements[elementId]
      return {
        ...store,
        elements: {
          ...store.elements,
          [elementId]: {
            ...element,
            props: {
              ...element.props,
              ...props,
            },
          },
        },
      }
    }
    //
    case Actions.RemoveElement: {
      const { elementId, selectElementId } = action.payload
      const element = store.elements[elementId]
      let listDeleteElement: string[] = [elementId]
      if (element.children && element.children.length > 0) {
        listDeleteElement.forEach(value => {
          const eachElement = store.elements[value].children
          if (eachElement && eachElement.length > 0) {
            listDeleteElement.push(...eachElement)
          }
        })
      }

      const tempStore =
        store.elements &&
        Object.keys(store.elements)
          .filter(item => !listDeleteElement.includes(item))
          .reduce((acc, key) => {
            const item = store.elements[key]
            if (item.children) {
              const listChildren = item.children.filter(i => !listDeleteElement.includes(i))
              return {
                ...acc,
                [key]: { ...store.elements[key], children: listChildren.length > 0 ? listChildren : [] },
              }
            }
            return { ...acc, [key]: store.elements[key] }
          }, {})
      return { elements: tempStore, selectedElement: selectElementId || ROOT_ELEMENT }
    }

    default:
      return store
  }
}

const ScreenBuilderActionContext = React.createContext<React.Dispatch<Action> | string>(
  'useScreenAction should be used inside ScreenBuilderProvider',
)

type ScreenBuilderContextProps = {
  defaultValue?: CommonWidgetDataType & { script?: Record<string, ScreenElement> }
}

export const ScreenBuilderProvider: React.FC<ScreenBuilderContextProps> = ({ defaultValue, children }) => {
  const [value, dispatch] = React.useReducer(reducer, initialState)

  console.log(value)

  React.useEffect(() => {
    if (!defaultValue) {
      return
    }
    // update default script
    if (defaultValue.script) {
      dispatch({
        type: Actions.LoadScript,
        payload: {
          script: defaultValue.script,
        },
      })
    }
    // update root label
    dispatch({
      type: Actions.ConfigElement,
      payload: {
        elementId: ROOT_ELEMENT,
        props: {
          name: defaultValue.name,
          displayName: defaultValue.displayName,
          description: defaultValue.description,
        },
      },
    })
  }, [defaultValue])

  return (
    <ScreenBuilderContext.Provider value={value}>
      <ScreenBuilderActionContext.Provider value={dispatch}>{children}</ScreenBuilderActionContext.Provider>
    </ScreenBuilderContext.Provider>
  )
}

export const useScreenBuilder = () => {
  const context = React.useContext(ScreenBuilderContext)
  if (typeof context === 'string') {
    throw new Error(context)
  }
  return context
}

export const useScreenAction = () => {
  const dispatch = React.useContext(ScreenBuilderActionContext)
  if (typeof dispatch === 'string') {
    throw new Error(dispatch)
  }

  const addElement = React.useCallback(
    (payload: { element: ScreenElement; source: { id: string }; target: { id: string; index?: number } }) => {
      dispatch({ type: Actions.AddElement, payload })
    },
    [dispatch],
  )

  const moveElement = React.useCallback(
    (payload: { elementId: string; source: { id: string }; target: { id: string; index?: number } }) => {
      dispatch({ type: Actions.MoveElement, payload })
    },
    [dispatch],
  )

  const removeElement = React.useCallback(
    (payload: { elementId: string; selectElementId?: string }) => {
      dispatch({ type: Actions.RemoveElement, payload })
    },
    [dispatch],
  )

  const selectElement = React.useCallback(
    (payload: string) => {
      dispatch({ type: Actions.SelectElement, payload })
    },
    [dispatch],
  )

  const setElementName = React.useCallback(
    (elementId: string, name?: string) => {
      dispatch({ type: Actions.SetElementName, payload: { elementId, name } })
    },
    [dispatch],
  )

  const configElement = React.useCallback(
    (elementId: string, props: Record<string, any>) => {
      dispatch({ type: Actions.ConfigElement, payload: { elementId, props } })
    },
    [dispatch],
  )

  return {
    addElement,
    moveElement,
    selectElement,
    setElementName,
    configElement,
    removeElement,
    dispatch,
  }
}
