import React, { FC } from 'react'
import styled from 'styled-components'
import { color } from '../../../components/GlobalStyle'
import { FlowType } from '../../../services/api/endpoints/flows'
import { capitalize } from '../../../utils/utils'
import { TriggerIcons } from '../components/Icons'
import { Subtitle, Title, TitleWrapper } from '../components/Node'

const FlowIconContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 48px;
  height: 48px;
  border-radius: 50%;
  background-color: ${color('Primary700')};
`

type BasicFlowTriggerProps = {
  type: FlowType
}

export const BasicFlowTrigger: FC<BasicFlowTriggerProps> = ({ type }) => {
  return (
    <>
      <FlowIconContainer>
        <TriggerIcons.ScheduleTrigger />
      </FlowIconContainer>
      <TitleWrapper>
        <Title>Start</Title>
        <Subtitle>{capitalize(type)} Flow</Subtitle>
      </TitleWrapper>
    </>
  )
}
