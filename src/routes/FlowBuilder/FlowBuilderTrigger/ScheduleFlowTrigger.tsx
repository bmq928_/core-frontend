import { format, parse } from 'date-fns'
import React from 'react'
import { useQuery } from 'react-query'
import { useParams } from 'react-router-dom'
import styled from 'styled-components'
import { color, spacing } from '../../../components/GlobalStyle'
import { NButton } from '../../../components/NButton/NButton'
import { NRow } from '../../../components/NGrid/NGrid'
import { typography } from '../../../components/NTypography'
import { APIServices } from '../../../services/api'
import { FlowsQueryKey } from '../../../services/api/endpoints/flows'
import { ScheduleFlowResponse } from '../../../services/api/models'
import { TriggerIcons } from '../components/Icons'
import { FlowSetScheduleModal } from '../FlowConfigModal/FlowSetScheduleModal'

const Wrapper = styled.div`
  background: ${color('Neutral900')};
  width: 220px;
  padding: ${spacing('md')};
  display: flex;
  flex-direction: column;
  border-radius: ${spacing('xs')};
`
const IconWrapper = styled.div`
  width: 24px;
  height: 24px;
  border-radius: 24px;
  background: ${color('Primary700')};
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: ${spacing('xs')};
`
const TextHeader = styled.div`
  ${typography('x-small-ui-text')}
  color:${color('Neutral500')};
`

const TimeSection = styled.div`
  padding: ${spacing('xs')} 0;
  color: ${color('white')};
  display: flex;
  flex-direction: column;
`
const Element = styled.div`
  ${typography('x-small-ui-text')};
  display: flex;
  align-items: center;
  padding: ${spacing('xs')} 0;
`
const Icon = styled.div`
  margin-right: ${spacing('xs')};
  display: flex;
  align-items: center;
`

const HeaderWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`

export const ScheduleFlowTrigger: React.FC = () => {
  const [modalSchedule, setModalSchedule] = React.useState(false)
  const { flowName } = useParams<{ flowName: string }>()
  const { data: flowData } = useQuery([FlowsQueryKey.getFlow, { flowName }], APIServices.Flows.getFlow)
  return (
    <Wrapper>
      <HeaderWrapper>
        <NRow align="center">
          <IconWrapper>
            <TriggerIcons.ScheduleTrigger />
          </IconWrapper>
          <TextHeader>Schedule Trigger</TextHeader>
        </NRow>
        <NButton size="small" type="link" onClick={() => setModalSchedule(true)}>
          EDIT
        </NButton>
      </HeaderWrapper>
      <TimeSection>
        <Element>
          <Icon>
            <TriggerIcons.ScheduleTriggerDate />
          </Icon>
          {(flowData?.data as ScheduleFlowResponse)?.start_date &&
            format(
              parse((flowData?.data as ScheduleFlowResponse).start_date, 'yyyy-MM-dd', new Date()),
              'MMM dd, yyyy',
            )}
        </Element>
        <Element>
          <Icon>
            <TriggerIcons.ScheduleTriggerTime />
          </Icon>
          {(flowData?.data as ScheduleFlowResponse)?.start_time}
        </Element>
      </TimeSection>
      {modalSchedule && (
        <FlowSetScheduleModal
          flowData={flowData?.data as ScheduleFlowResponse}
          showModal={modalSchedule}
          setShowModal={setModalSchedule}
        />
      )}
    </Wrapper>
  )
}
