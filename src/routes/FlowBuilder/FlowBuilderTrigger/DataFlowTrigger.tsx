import React from 'react'
import { useQuery } from 'react-query'
import { useParams } from 'react-router-dom'
import styled, { useTheme } from 'styled-components'
import { color, spacing } from '../../../components/GlobalStyle'
import { NButton } from '../../../components/NButton/NButton'
import { NDivider } from '../../../components/NDivider'
import { NRow } from '../../../components/NGrid/NGrid'
import { NSpinner } from '../../../components/NSpinner/NSpinner'
import { typography } from '../../../components/NTypography'
import { APIServices } from '../../../services/api'
import { BuilderQueryKey } from '../../../services/api/endpoints/builder'
import { FlowsQueryKey } from '../../../services/api/endpoints/flows'
import { DataFlowResponse } from '../../../services/api/models'
import { ReactComponent as ConditonIcon } from '../assets/icons/condition.svg'
import { TriggerIcons } from '../components/Icons'
import { DataFlowTriggerModal } from '../FlowConfigModal/DataFlowTriggerModal'
import { FlowSelectObjectModal } from '../FlowConfigModal/FlowSelectObjectModal'

const Wrapper = styled.div`
  background: ${color('Neutral900')};
  width: 220px;
  min-height: 140px;
  padding: ${spacing('md')};
  display: flex;
  flex-direction: column;
  border-radius: ${spacing('xs')};
`
const IconWrapper = styled.div`
  width: 24px;
  height: 24px;
  border-radius: 24px;
  background: ${color('Primary700')};
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: ${spacing('xs')};
`
const TextHeader = styled.div`
  ${typography('x-small-ui-text')}
  color:${color('Neutral500')};
`

const SelectObjectBtn = styled(NButton)`
  justify-content: flex-start;
`
const DetailText = styled.p`
  color: ${color('white')};
  ${typography('button')}
  margin-left: ${spacing('xs')};
`

const LoadingWrapper = styled.div`
  padding: ${spacing('xl')};
  display: flex;
  justify-content: center;
`

export const DataFlowTrigger: React.FC = () => {
  const theme = useTheme()
  const [modalObject, setModalObject] = React.useState(false)
  const [modalTrigger, setModalTrigger] = React.useState(false)
  const { flowName } = useParams<{ flowName: string }>()

  const { data: flowData, isLoading: isLoadingFlow } = useQuery(
    [FlowsQueryKey.getFlow, { flowName }],
    APIServices.Flows.getFlow,
    {
      select: data => data.data as DataFlowResponse,
    },
  )

  const { data: objectData, isLoading: isLoadingObjectData } = useQuery(
    [BuilderQueryKey.getObjectSchema, { objName: flowData?.object_name! }],
    APIServices.Builder.getObjectSchema,
    {
      enabled: !!flowData?.object_name,
    },
  )

  return (
    <Wrapper>
      <NRow justify="space-between" align="center">
        <NRow align="center">
          <IconWrapper>
            <TriggerIcons.ScheduleTrigger />
          </IconWrapper>
          <TextHeader>Data Trigger</TextHeader>
        </NRow>
        <NButton size="small" type="link" onClick={() => setModalTrigger(true)}>
          EDIT
        </NButton>
      </NRow>
      <NDivider size="sm" lineSize={1} lineColor={color('Neutral700')({ theme })} />
      {isLoadingFlow ? (
        <LoadingWrapper>
          <NSpinner size={22} strokeWidth={2} color={color('Primary700')({ theme })} />
        </LoadingWrapper>
      ) : (
        <>
          {flowData && (
            <NRow align="center">
              <ConditonIcon />
              <DetailText>A record is {flowData.action}</DetailText>
            </NRow>
          )}
          <NDivider size="sm" />
          {/* <NRow align="center">
        <RunIcon />
        <DetailText>Before a record is saved</DetailText>
      </NRow> */}

          <SelectObjectBtn
            icon={<TriggerIcons.ScheduleTriggerObject />}
            size="small"
            type="link"
            loading={isLoadingObjectData}
            onClick={() => setModalObject(true)}>
            {objectData?.data.displayName || 'Select Object'}
          </SelectObjectBtn>
        </>
      )}

      {modalObject && (
        <FlowSelectObjectModal flowData={flowData} showModal={modalObject} setShowModal={setModalObject} />
      )}
      {modalTrigger && (
        <DataFlowTriggerModal flowData={flowData} showModal={modalTrigger} setShowModal={setModalTrigger} />
      )}
    </Wrapper>
  )
}
