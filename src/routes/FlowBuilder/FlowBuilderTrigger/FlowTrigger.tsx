import { DataFlowTrigger } from './DataFlowTrigger'
import { ScheduleFlowTrigger } from './ScheduleFlowTrigger'

export const FLOW_TRIGGER: Record<string, JSX.Element | undefined> = {
  data: <DataFlowTrigger />,
  'time-based': <ScheduleFlowTrigger />,
}
