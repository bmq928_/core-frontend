import * as React from 'react'
import { Provider } from 'react-redux'
import { FlowItem } from '../../../services/api/endpoints/flows'
import { FlowItemConnectionResponse } from '../../../services/api/models'
import { builderActions, NodeConnections } from '../redux/builderSlice'
import { store } from '../redux/store'
import { TRIGGER_CONNECTION, TRIGGER_NAME } from '../utils/constants'
import { fromFlowItemToFlowNode } from '../utils/functions'
import { FlowNode, LinkType } from '../utils/types'

type FlowBuilderProviderProps = {
  children: React.ReactNode
  nodes: FlowItem[]
  links: FlowItemConnectionResponse[]
}

export function FlowBuilderProvider({ children, nodes, links }: FlowBuilderProviderProps) {
  React.useEffect(() => {
    // transform FIs to Nodes
    const { transformNodes, nodesByName } = nodes.reduce<{
      transformNodes: FlowNode[]
      nodesByName: Record<string, FlowNode>
    }>(
      (prev, flowItem) => {
        const transformed = fromFlowItemToFlowNode(flowItem)
        if (!transformed) {
          console.error(`Flow item type: "${flowItem.type}" is not supported yet`)
          return prev
        }

        return {
          transformNodes: [...prev.transformNodes, transformed],
          nodesByName: {
            ...prev.nodesByName,
            [flowItem.name]: transformed,
          },
        }
      },
      { transformNodes: [], nodesByName: {} },
    )

    store.dispatch(builderActions.setNodes({ nodes: transformNodes, nodesByName }))

    // transform FI Connections to Links
    const transformedLinks = links.map(connect => ({
      id: connect.guid,
      target: connect.trg_item_name,
      source: connect.src_item_name,
      type: LinkType.Success,
      data: {},
      outcomeName: connect.src_outcome_name,
      isFault: connect.is_fault || false,
    }))

    store.dispatch(builderActions.setLinks({ links: transformedLinks }))

    // calculate remaining connections can drag
    if (nodes && links) {
      // extract max_connections and has_fault_connection out of the nodes
      const flowItems = nodes.reduce<NodeConnections>(
        (a, { max_connections, has_fault_connection, name }) => {
          return {
            ...a,
            [name]: {
              max_connections: has_fault_connection ? max_connections + 1 : max_connections,
              has_fault_connection,
            },
          }
        },
        {
          [TRIGGER_NAME]: TRIGGER_CONNECTION,
        },
      )

      // remaining connections
      const nodeConnections = links.reduce((a, { src_item_name }) => {
        const node = a[src_item_name]
        if (node) {
          return {
            ...a,
            [src_item_name]: {
              ...node,
              max_connections: node.max_connections - 1,
            },
          }
        }
        return a
      }, flowItems)

      store.dispatch(builderActions.setNodeConnections(nodeConnections))
    }
  }, [nodes, links])

  return <Provider store={store}>{children}</Provider>
}
