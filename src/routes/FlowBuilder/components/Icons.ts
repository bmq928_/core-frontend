import { ReactComponent as Action } from '../assets/icons/FlowIcons/action-flow-icon.svg'
import { ReactComponent as Assignment } from '../assets/icons/FlowIcons/assignment-flow-icon.svg'
import { ReactComponent as DataCreate } from '../assets/icons/FlowIcons/data-create-icon.svg'
import { ReactComponent as DataDelete } from '../assets/icons/FlowIcons/data-delete-icon.svg'
import { ReactComponent as DataGet } from '../assets/icons/FlowIcons/data-get-icon.svg'
import { ReactComponent as DataUpdate } from '../assets/icons/FlowIcons/data-update-icon.svg'
import { ReactComponent as Decision } from '../assets/icons/FlowIcons/decision-flow-icon.svg'
import { ReactComponent as Loop } from '../assets/icons/FlowIcons/loop-flow-icon.svg'
import { ReactComponent as Screen } from '../assets/icons/FlowIcons/screen-flow-icon.svg'
import { ReactComponent as Subflow } from '../assets/icons/FlowIcons/subflow-flow-icon.svg'
import { ReactComponent as ScheduleTriggerDate } from '../assets/icons/TriggerIcons/schedule-trigger-date.svg'
import { ReactComponent as ScheduleTrigger } from '../assets/icons/TriggerIcons/schedule-trigger-icon.svg'
import { ReactComponent as ScheduleTriggerObject } from '../assets/icons/TriggerIcons/schedule-trigger-object.svg'
import { ReactComponent as ScheduleTriggerTime } from '../assets/icons/TriggerIcons/schedule-trigger-time.svg'

export const FlowIcons = {
  Screen,
  Action,
  Subflow,
  Loop,
  Decision,
  Assignment,
  DataCreate,
  DataUpdate,
  DataGet,
  DataDelete,
}

export const TriggerIcons = {
  ScheduleTrigger,
  ScheduleTriggerDate,
  ScheduleTriggerTime,
  ScheduleTriggerObject,
}
