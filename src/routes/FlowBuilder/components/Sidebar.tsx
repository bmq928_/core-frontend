import React from 'react'
import { useDrag } from 'react-dnd'
import { Item, ItemLabel, Widget, Wrapper } from '../../../components/Builder/LeftSidebar'
import { FlowType } from '../../../services/api/endpoints/flows'
import { getWidgetByGroups, GroupName } from '../FlowBuilderItem/FlowItem'
import { DroppableType, WidgetItemType } from '../utils/types'

type SidebarProps = {
  flowType?: FlowType
}

export function Sidebar({ flowType }: SidebarProps) {
  const widgetsByGroup = getWidgetByGroups(flowType)
  const widgetGroups = Object.keys(widgetsByGroup) as GroupName[]
  return (
    <Wrapper>
      {widgetGroups.map(groupKey => {
        return (
          <Widget key={`flow_widget_${groupKey}`} title={groupKey}>
            {widgetsByGroup[groupKey].map(widget => {
              return (
                <DraggableItem key={`${groupKey}_${widget.label}`} widget={widget}>
                  {widget?.leftIcon || `Icon ${widget.label}`}
                </DraggableItem>
              )
            })}
          </Widget>
        )
      })}
    </Wrapper>
  )
}

type DraggableItemProps = {
  children: React.ReactNode
  widget: WidgetItemType
}

function DraggableItem({ children, widget }: DraggableItemProps) {
  const [, ref] = useDrag({
    type: DroppableType.AddNode,
    item: { widget },
  })
  return (
    <Item ref={ref}>
      {children}
      <ItemLabel>{widget.label}</ItemLabel>
    </Item>
  )
}
