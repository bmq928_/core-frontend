import * as React from 'react'
import { useTheme } from 'styled-components'
import { color } from '../../../components/GlobalStyle'
import { useBuilderState } from '../redux/hooks/useBuilderState'
import { CELL_HEIGHT, CELL_WIDTH } from '../utils/constants'

export function SvgDefs({ children }: { children?: React.ReactNode }) {
  const theme = useTheme()
  return (
    <defs>
      <marker id="dot" viewBox="0 0 10 10" refX="5" refY="5" markerWidth="5" markerHeight="5">
        <circle cx="5" cy="5" r="4" fill={color('Neutral300')({ theme })} />
      </marker>
      <marker id="dot-selected" viewBox="0 0 10 10" refX="5" refY="5" markerWidth="5" markerHeight="5">
        <circle cx="5" cy="5" r="4" fill={color('Primary700')({ theme })} />
      </marker>
      <marker
        id="arrow"
        viewBox="0 0 10 10"
        refX="5"
        refY="5"
        markerWidth="6"
        markerHeight="6"
        orient="auto-start-reverse">
        <path d="M 0 0 L 10 5 L 0 10 z" fill={color('Neutral300')({ theme })} />
      </marker>
      <marker
        id="arrow-selected"
        viewBox="0 0 10 10"
        refX="5"
        refY="5"
        markerWidth="6"
        markerHeight="6"
        orient="auto-start-reverse">
        <path d="M 0 0 L 10 5 L 0 10 z" fill={color('Primary700')({ theme })} />
      </marker>
      <DotPattern />
      {children}
    </defs>
  )
}

export function DotPattern() {
  const { configs } = useBuilderState()
  const [translateX, translateY] = configs.translate
  const theme = useTheme()
  return (
    <pattern
      id="background-pattern"
      x={translateX - 1}
      y={translateY - 1}
      width={CELL_WIDTH}
      height={CELL_HEIGHT}
      patternUnits="userSpaceOnUse">
      <circle cx="1" cy="1" r="1" fill={color('Neutral300')({ theme })} />
    </pattern>
  )
}
