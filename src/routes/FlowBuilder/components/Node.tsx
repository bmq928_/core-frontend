import * as React from 'react'
import { useDrag, useDrop } from 'react-dnd'
import { useMutation, useQueryClient } from 'react-query'
import { useDispatch } from 'react-redux'
import { useParams } from 'react-router-dom'
import styled from 'styled-components'
import { color, spacing } from '../../../components/GlobalStyle'
import { GlobalIcons } from '../../../components/Icons'
import { NSpinner } from '../../../components/NSpinner/NSpinner'
import { NToast } from '../../../components/NToast'
import { typography } from '../../../components/NTypography'
import { APIServices } from '../../../services/api'
import { FlowsQueryKey } from '../../../services/api/endpoints/flows'
import { WIDGET_FLOW_ITEMS } from '../FlowBuilderItem/FlowItem'
import { builderActions } from '../redux/builderSlice'
import { useBuilderState } from '../redux/hooks/useBuilderState'
import { FlowIconContainerClassName, NODE_HEIGHT, NODE_WIDTH } from '../utils/constants'
import { DroppableType, FlowNode, NodeType, NODE_TYPE_DATA } from '../utils/types'

type WrapperProps = { top: number; left: number; isDragging: boolean; isSelected: boolean }

const Wrapper = styled('div').attrs(({ top, left, isDragging }: WrapperProps) => ({
  style: {
    top,
    left,
    opacity: isDragging ? 0 : 1,
  },
}))<WrapperProps>`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  width: ${NODE_WIDTH}px;
  height: ${NODE_HEIGHT}px;
  margin-top: -${NODE_HEIGHT / 2}px;
  margin-left: -${NODE_WIDTH / 2}px;
  position: absolute;

  cursor: move;

  .${FlowIconContainerClassName} {
    color: ${color('white')};
    transform: ${props => (props.isSelected ? 'scale(1.1)' : 'scale(1)')};
    box-shadow: ${props =>
      props.isSelected ? '0px 4px 16px rgba(53, 147, 139, 0.8)' : '0px 4px 16px rgba(163, 171, 181, 0.5)'};
  }
`

export const Handle = styled('div')`
  position: absolute;
  left: 50%;
  bottom: 0;
  transform: translate(-50%, 50%);
  width: 12px;
  height: 12px;
  border-radius: 50%;
  border: 2px solid ${color('Neutral200')};
  cursor: grab;
  background: ${color('Neutral300')};
  margin-top: ${spacing('xxs')};
`

export const TitleWrapper = styled('div')`
  position: absolute;
  left: 50%;
  transform: translateX(-50%);
  top: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

const CloseBtn = styled.div`
  position: absolute;
  top: -24px;
  right: -24px;
  display: flex;
  justify-content: center;
  align-items: center;

  width: 24px;
  height: 24px;
  color: ${color('Neutral400')};

  cursor: pointer;
  &:hover {
    color: ${color('Primary400')};
  }
`

export const Title = styled('div')`
  ${typography('button')};
  margin-top: ${spacing('xs')};
  text-align: center;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
`

export const Subtitle = styled('div')`
  margin-top: 2px;
  ${typography('x-small-ui-text')};
  color: ${color('Neutral500')};
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
`

type NodeProps = {
  node: FlowNode
  selectedNodeId?: string
}

export const Node = React.memo(function Node({ node, selectedNodeId }: NodeProps) {
  const { flowName } = useParams<{ flowName: string }>()
  const { nodeConnections } = useBuilderState()
  const nodeConnection = nodeConnections[node.name]
  const canConnect = (nodeConnection && nodeConnection?.max_connections > 0) || false
  const queryClient = useQueryClient()

  const dispatch = useDispatch()

  const { mutate: createConnection } = useMutation(APIServices.Flows.createFlowConnection, {
    onSuccess(data) {
      dispatch(
        builderActions.connectNodes({
          sourceName: data.data.src_item_name,
          targetName: data.data.trg_item_name,
          sourceOutcomeName: data.data.src_outcome_name,
          id: data.data.guid,
          isFault: data.data.is_fault || false,
        }),
      )
      queryClient.invalidateQueries([FlowsQueryKey.getFlowItems, { flowName }])
      queryClient.invalidateQueries([FlowsQueryKey.getFlowConnections, { flowName }])
    },
    onError: error => {
      NToast.error({
        title: 'Create connection unsuccessful',
        subtitle: error.response?.data.message,
      })
    },
  })

  const [, dropRef] = useDrop({
    accept: [DroppableType.ConnectNodes],
    drop(item: { id: string; name: string; nodeType: NodeType }) {
      // open link modal to choose outcome
      if (item.nodeType === NodeType.Decision || item.nodeType === NodeType.Loop) {
        dispatch(
          builderActions.openLinkModal({
            srcNode: item,
            targetNode: node,
          }),
        )
        return
      }
      const itemConnection = nodeConnections[item.name]
      if (NODE_TYPE_DATA.includes(item.nodeType)) {
        const is_fault = itemConnection?.max_connections === 1 && itemConnection?.has_fault_connection
        createConnection({
          flowName,
          src_item_name: item.name,
          trg_item_name: node.name,
          is_fault,
        })
        return
      }
      createConnection({
        flowName,
        src_item_name: item.name,
        trg_item_name: node.name,
      })
    },
  })

  const [{ isDragging }, nodeRef, nodePreviewRef] = useDrag({
    type: DroppableType.UpdateNode,
    item: () => {
      dispatch(builderActions.setDrag(node.name))
      return {
        id: node.id,
        name: node.name,
      }
    },
    end() {
      dispatch(builderActions.setDrag())
    },
    collect(monitor) {
      return {
        isDragging: monitor.isDragging(),
      }
    },
  })

  const [, handleRef] = useDrag({
    type: DroppableType.ConnectNodes,
    item: {
      id: node.id,
      name: node.name,
      nodeType: node.type,
    },
  })

  // delete flow item mutation
  const { mutate: deleteFlowItem, isLoading: isDeleting } = useMutation(APIServices.Flows.deleteFlowItem, {
    onSuccess: () => {
      dispatch(builderActions.removeNode({ nodeId: node.id, nodeName: node.name }))
      queryClient.invalidateQueries([FlowsQueryKey.getFlowItems, { flowName }])
      queryClient.invalidateQueries([FlowsQueryKey.getFlowConnections, { flowName }])
    },
    onError: error => {
      NToast.error({
        title: 'Delete unsuccessful',
        subtitle: error.response?.data.message,
      })
    },
  })

  const widget = WIDGET_FLOW_ITEMS[node.type]

  const flowIcon =
    widget.flowIcon &&
    React.cloneElement<React.HTMLProps<HTMLDivElement>>(widget.flowIcon, {
      ref: nodePreviewRef,
    })

  return (
    <>
      <Wrapper
        ref={dropRef}
        top={node.position.y}
        left={node.position.x}
        isSelected={selectedNodeId === node.id}
        isDragging={isDragging}
        onClick={e => {
          e.stopPropagation()
          dispatch(builderActions.selectNode(node.id))
        }}
        onDoubleClick={e => {
          e.stopPropagation()
          dispatch(
            builderActions.openModal({
              position: node.position,
              nodeId: node.id,
              type: node.type,
              data: node.data,
            }),
          )
        }}>
        <div ref={nodeRef}>{flowIcon}</div>
        <TitleWrapper>
          <Title>{widget.label}</Title>
          <Subtitle>{node.data.displayName}</Subtitle>{' '}
        </TitleWrapper>
        {canConnect && <Handle ref={handleRef} />}
        {selectedNodeId === node.id && (
          <CloseBtn
            onClick={e => {
              e.stopPropagation()
              !isDeleting && deleteFlowItem({ flowName, flowItemName: node.name })
            }}>
            {isDeleting ? <NSpinner size={16} strokeWidth={2} /> : <GlobalIcons.Close />}
          </CloseBtn>
        )}
      </Wrapper>
    </>
  )
})
