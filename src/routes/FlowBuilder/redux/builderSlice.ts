import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { TRIGGER_CONNECTION, TRIGGER_NAME } from '../utils/constants'
import {
  CommonWidgetDataType,
  FlowLink,
  FlowNode,
  LinkModalType,
  LinkType,
  NodeModalType,
  NodeType,
  XY,
} from '../utils/types'

export const SLICE_NAME = 'builder'
type BuilderConfig = {
  scale: number
  translate: [number, number]
}

export type BuilderState = {
  configs: BuilderConfig
  nodes: FlowNode[]
  links: FlowLink[]
  nodesByName: Record<string, FlowNode>
  selectedNodeId?: string
  selectedLinkId?: string
  openedNodeModal?: NodeModalType
  openedLinkModal?: LinkModalType
  isDragging: boolean
  draggingNodeName?: string
  nodeConnections: NodeConnections
}

export type NodeConnections = Record<string, { max_connections: number; has_fault_connection: boolean } | undefined>

const initialState: BuilderState = {
  configs: {
    scale: 1,
    translate: [0, 0],
  },
  nodes: [],
  links: [],
  nodesByName: {},
  nodeConnections: {
    [TRIGGER_NAME]: TRIGGER_CONNECTION,
  },
  isDragging: false,
}

const builderSlice = createSlice({
  name: SLICE_NAME,
  initialState,
  reducers: {
    setNodes(state: BuilderState, action: PayloadAction<{ nodes: FlowNode[]; nodesByName: Record<string, FlowNode> }>) {
      state.nodes = action.payload.nodes
      state.nodesByName = action.payload.nodesByName
    },
    setLinks(state: BuilderState, action: PayloadAction<{ links: FlowLink[] }>) {
      state.links = action.payload.links
    },
    panAndZoom(state: BuilderState, action: PayloadAction<{ translate: [number, number]; scale: number }>) {
      state.configs.scale = action.payload.scale
      state.configs.translate[0] += action.payload.translate[0]
      state.configs.translate[1] += action.payload.translate[1]
    },
    setDrag(state: BuilderState, action: PayloadAction<string | undefined>) {
      if (action.payload) {
        state.isDragging = true
        state.draggingNodeName = action.payload
        return
      }
      state.isDragging = false
      state.draggingNodeName = undefined
    },
    setNodePosition(state: BuilderState, action: PayloadAction<{ nodeId: string; position: XY }>) {
      state.nodes = state.nodes.map(node => {
        if (node.id === action.payload.nodeId) {
          return {
            ...node,
            position: action.payload.position,
          }
        }
        return node
      })
    },
    openModal(
      state: BuilderState,
      action: PayloadAction<{ nodeId?: string; position: XY; type: NodeType; data?: CommonWidgetDataType }>,
    ) {
      const { nodeId, position, type, data } = action.payload
      state.openedNodeModal = {
        nodeId,
        position,
        type,
        data,
      }
      state.selectedLinkId = undefined
    },
    closeModal(state: BuilderState) {
      state.openedNodeModal = undefined
    },
    setNodeData(state: BuilderState, action: PayloadAction<{ nodeId: string; data: CommonWidgetDataType }>) {
      state.nodes = state.nodes.map(node => {
        if (node.id === action.payload.nodeId) {
          return {
            ...node,
            data: action.payload.data,
          }
        }
        return node
      })
    },
    addNode(state: BuilderState, action: PayloadAction<FlowNode>) {
      state.nodes.push(action.payload)
    },
    removeNode(state: BuilderState, action: PayloadAction<{ nodeName: string; nodeId: string }>) {
      const { nodeId, nodeName } = action.payload
      state.nodes = state.nodes.filter(node => node.id !== nodeId)
      state.links = state.links.filter(link => link.source !== nodeName && link.target !== nodeName)
      state.selectedNodeId = undefined
    },
    selectNode(state: BuilderState, action: PayloadAction<string>) {
      state.selectedNodeId = action.payload
    },
    deselectNode(state: BuilderState) {
      state.selectedNodeId = undefined
    },
    openLinkModal(state: BuilderState, action: PayloadAction<LinkModalType>) {
      state.openedLinkModal = action.payload
      state.selectedLinkId = undefined
    },
    closeLinkModal(state: BuilderState) {
      state.openedLinkModal = undefined
    },
    connectNodes(
      state: BuilderState,
      action: PayloadAction<{
        sourceName: string
        targetName: string
        id: string
        sourceOutcomeName?: string
        isFault: boolean
      }>,
    ) {
      state.links.push({
        id: action.payload.id,
        source: action.payload.sourceName,
        target: action.payload.targetName,
        data: {},
        isFault: action.payload.isFault,
        type: LinkType.Success,
        outcomeName: action.payload.sourceOutcomeName,
      })
    },
    selectLink(state: BuilderState, action: PayloadAction<string>) {
      state.selectedLinkId = action.payload
    },
    unselectLink(state: BuilderState) {
      state.selectedLinkId = undefined
    },
    removeLink(state: BuilderState, action: PayloadAction<string>) {
      const linkId = action.payload
      state.links = state.links.filter(link => link.id !== linkId)
      state.selectedLinkId = undefined
    },
    setNodeConnections(state: BuilderState, action: PayloadAction<NodeConnections>) {
      state.nodeConnections = action.payload
    },
  },
})

export const builderActions = builderSlice.actions
export const builderReducer = builderSlice.reducer
