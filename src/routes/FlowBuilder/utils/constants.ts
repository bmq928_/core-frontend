export const FLOW_BUILDER_NAME = 'flow-builder'
export const FLOW_LEFT_SIDEBAR = 'FLOW_LEFT_SIDEBAR'

export const FlowIconContainerClassName = 'flow-icon-container'

export const NODE_WIDTH = 48
export const NODE_HEIGHT = 48
export const CELL_WIDTH = 24
export const CELL_HEIGHT = 24
export const BORDER_RADIUS = 12

export const TRIGGER_NAME = '*'
export const TRIGGER_POSITION = 144

export const TRIGGER_CONNECTION = {
  max_connections: 1,
  has_fault_connection: false,
}

export const TRIGGER_WIDTH = NODE_WIDTH
export const TRIGGER_HEIGHT = NODE_HEIGHT

export const BIG_TRIGGER_WIDTH = 220
export const BIG_TRIGGER_HEIGHT = 140
