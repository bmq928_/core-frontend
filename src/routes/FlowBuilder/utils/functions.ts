import { get } from 'lodash'
import { NToast } from '../../../components/NToast'
import { BaseCreateFlowItemDto, BaseUpdateFlowItemDto, FlowItem } from '../../../services/api/endpoints/flows'
import {
  BaseScreenFIComponent,
  ContentValue,
  CreateRecordReadDto,
  Outcome,
  RecordReadConfig,
  ScreenFIComponentDto,
  ScreenFlowItemResponse,
} from '../../../services/api/models'
import { ROOT_ELEMENT } from '../FlowBuilderItem/ScreenBuilder/utils/builder'
import { ScreenElement, ScreenElementComponent } from '../FlowBuilderItem/ScreenBuilder/utils/models'
import { CommonWidgetDataType, DataFlowItemConfigType, FlowNode, NodeType, XY } from './types'

export function fromFlowItemToFlowNode(flowItem: FlowItem): FlowNode | undefined {
  const { position, ...restBody } = flowItem.node_attributes as Record<string, any>

  if (flowItem.type === NodeType.Decision) {
    return {
      id: flowItem.guid,
      name: flowItem.name,
      position: position || { x: 0, y: 0 },
      outcome: [],
      type: NodeType.Decision,
      data: {
        name: flowItem.name,
        displayName: flowItem.display_name,
        order: flowItem.order,
        outcomes: flowItem.outcomes,
        default_outcome_label: flowItem.default_outcome_label,
        ...restBody,
      },
    }
  }

  if (flowItem.type === NodeType.Screen) {
    return {
      id: flowItem.guid,
      name: flowItem.name,
      position: position || { x: 0, y: 0 },
      outcome: [],
      data: {
        name: flowItem.name,
        displayName: flowItem.display_name,
        script: addRootLabel(flowItem.layout_script, flowItem),
        ...restBody,
      },
      type: NodeType.Screen,
    }
  }

  if (flowItem.type === NodeType.Action) {
    return {
      id: flowItem.guid,
      name: flowItem.name,
      position: position || { x: 0, y: 0 },
      outcome: [],
      data: {
        name: flowItem.name,
        displayName: flowItem.display_name,
        action_metadata_name: flowItem.action_metadata_name,
        input_map: flowItem.input_map,
        ...restBody,
      },
      type: NodeType.Action,
    }
  }

  if (flowItem.type === NodeType.Subflow) {
    return {
      id: flowItem.guid,
      name: flowItem.name,
      position: position || { x: 0, y: 0 },
      outcome: [],
      data: {
        name: flowItem.name,
        displayName: flowItem.display_name,
        referencedFlow: flowItem.subflow_id,
        input_map: flowItem.input_map,
        ...restBody,
      },
      type: NodeType.Subflow,
    }
  }

  if (flowItem.type === NodeType.Assignment) {
    return {
      id: flowItem.guid,
      name: flowItem.name,
      position: position || { x: 0, y: 0 },
      outcome: [],
      data: {
        name: flowItem.name,
        displayName: flowItem.display_name,
        assignment_variables: flowItem.assignment_variables,
        ...restBody,
      },
      type: NodeType.Assignment,
    }
  }

  if (flowItem.type === NodeType.Loop) {
    return {
      id: flowItem.guid,
      name: flowItem.name,
      position: position || { x: 0, y: 0 },
      outcome: [],
      data: {
        name: flowItem.name,
        displayName: flowItem.display_name,
        collection_variable_name: flowItem.collection_variable_name,
        loop_order: flowItem.loop_order,
        ...restBody,
      },
      type: NodeType.Loop,
    }
  }

  if (flowItem.type === NodeType.DataCreate) {
    return {
      id: flowItem.guid,
      name: flowItem.name,
      position: position || { x: 0, y: 0 },
      outcome: [],
      data: {
        name: flowItem.name,
        displayName: flowItem.display_name,
        config: parseConfigObjectToConfigArray(flowItem.config.payload),
        object_name: flowItem.object_name,
        ...restBody,
      },
      type: NodeType.DataCreate,
    }
  }
  if (flowItem.type === NodeType.DataGet) {
    return {
      id: flowItem.guid,
      name: flowItem.name,
      position: position || { x: 0, y: 0 },
      type: NodeType.DataGet,
      outcome: [],
      data: {
        name: flowItem.name,
        displayName: flowItem.display_name,
        object_name: flowItem.object_name,
        sortBy: flowItem.config.sortBy,
        sortOrder: flowItem.config.sortOrder,
        getOptions: flowItem.config.getOptions,
        condition: flowItem.config.conditionLogic,
        config: flowItem.config.conditions,
        ...restBody,
      },
    }
  }

  if (flowItem.type === NodeType.DataUpdate) {
    return {
      id: flowItem.guid,
      name: flowItem.name,
      position: position || { x: 0, y: 0 },
      outcome: [],
      data: {
        name: flowItem.name,
        displayName: flowItem.display_name,
        // TODO: fix in NCL-1128
        //@ts-ignore
        config: parseConfigObjectToConfigArray(flowItem.config.payload),
        guid: flowItem.config.guid,
        object_name: flowItem.object_name,
        ...restBody,
      },
      type: NodeType.DataUpdate,
    }
  }

  if (flowItem.type === NodeType.DataDelete) {
    return {
      id: flowItem.guid,
      name: flowItem.name,
      position: position || { x: 0, y: 0 },
      outcome: [],
      data: {
        name: flowItem.name,
        displayName: flowItem.display_name,
        guid: flowItem.config.guid,
        object_name: flowItem.object_name,
        ...restBody,
      },
      type: NodeType.DataDelete,
    }
  }

  return
}

export function createFlowItemFromModalForm({
  flowName,
  position,
  type,
  data,
}: {
  flowName: string
  position: XY
  type: NodeType
  data: CommonWidgetDataType
}): { flowName: string; data: BaseCreateFlowItemDto } | undefined {
  if (type === NodeType.Decision) {
    return {
      flowName,
      data: {
        name: data.name as string,
        display_name: data.displayName as string,
        order: renameDecisionOrder(data.order, data.outcomes),
        outcomes: renameObjectKeyForDecisionOutcome(data.outcomes),
        default_outcome_label: data.default_outcome_label,
        type,
        node_attributes: {
          description: data.description,
          position,
        },
      },
    }
  }
  if (type === NodeType.Screen) {
    const script = transformScreenScript(data.script)

    if (!script) {
      return
    }

    return {
      flowName,
      data: {
        name: data.name as string,
        display_name: data.displayName as string,
        type,
        layout_script: script,
        node_attributes: {
          description: data.description,
          position: position,
        },
      },
    }
  }

  if (type === NodeType.Action) {
    return {
      flowName,
      data: {
        name: data.name as string,
        display_name: data.displayName as string,
        type,
        input_map: data.input_map,
        action_metadata_name: data.action_metadata_name,
        node_attributes: {
          description: data.description,
          position: position,
        },
      },
    }
  }

  if (type === NodeType.Subflow) {
    return {
      flowName,
      data: {
        name: data.name as string,
        display_name: data.displayName as string,
        type,
        input_map: data.input_map,
        subflow_id: data.referencedFlow,
        node_attributes: {
          description: data.description,
          position: position,
        },
      },
    }
  }

  if (type === NodeType.Assignment) {
    return {
      flowName,
      data: {
        name: data.name as string,
        display_name: data.displayName as string,
        type,
        assignment_variables: data.assignment_variables,
        node_attributes: {
          description: data.description,
          position: position,
        },
      },
    }
  }

  if (type === NodeType.Loop) {
    return {
      flowName,
      data: {
        name: data.name as string,
        display_name: data.displayName as string,
        type,
        collection_variable_name: data.collection_variable_name,
        loop_order: data.loop_order,
        node_attributes: {
          description: data.description,
          position: position,
        },
      },
    }
  }

  if (type === NodeType.DataCreate) {
    return {
      flowName,
      data: {
        name: data.name as string,
        display_name: data.displayName as string,
        type,
        object_name: data.object_name,
        config: {
          payload: parseConfigArrayToConfigObject(data.config),
        },
        node_attributes: {
          description: data.description,
          position: position,
        },
      },
    }
  }

  if (type === NodeType.DataGet) {
    return {
      flowName,
      data: {
        type,
        name: data.name,
        display_name: data.displayName,
        object_name: data.object_name,
        node_attributes: { description: data.description, position },
        config: {
          conditionLogic: data.condition as string,
          conditions: data.config,
          sortBy: data.sortBy as RecordReadConfig['sortBy'],
          sortOrder: data.sortOrder as RecordReadConfig['sortOrder'],
          getOptions: data.getOptions as RecordReadConfig['getOptions'],
        } as RecordReadConfig,
      } as CreateRecordReadDto,
    }
  }

  if (type === NodeType.DataUpdate) {
    return {
      flowName,
      data: {
        name: data.name as string,
        display_name: data.displayName as string,
        type,
        object_name: data.object_name,
        config: {
          payload: parseConfigArrayToConfigObject(data.config),
          guid: data.guid,
        },
        node_attributes: {
          description: data.description,
          position: position,
        },
      },
    }
  }

  if (type === NodeType.DataDelete) {
    return {
      flowName,
      data: {
        name: data.name as string,
        display_name: data.displayName as string,
        type,
        object_name: data.object_name,
        config: {
          guid: data.guid,
        },
        node_attributes: {
          description: data.description,
          position: position,
        },
      },
    }
  }

  return
}

export function updateFlowItemFromModalForm(params: {
  flowItemName: string
  flowName: string
  position: XY
  type: NodeType
  data: CommonWidgetDataType
}):
  | {
      flowName: string
      flowItemName: string
      data: BaseUpdateFlowItemDto
    }
  | undefined {
  const { flowItemName, flowName, position, type, data } = params
  if (type === NodeType.Decision) {
    return {
      flowItemName,
      flowName,
      data: {
        display_name: data.displayName as string,

        default_outcome_label: data.default_outcome_label,
        order: renameDecisionOrder(data.order, data.outcomes),
        outcomes: renameObjectKeyForDecisionOutcome(data.outcomes),
        node_attributes: {
          position,
          description: data.description,
        },
      },
    }
  }
  if (type === NodeType.Screen) {
    const script = transformScreenScript(data.script)
    if (!script) {
      return
    }
    return {
      flowItemName,
      flowName,
      data: {
        display_name: data.displayName as string,
        layout_script: script,
        node_attributes: {
          description: data.description,
          position: position,
        },
      },
    }
  }

  if (type === NodeType.Action) {
    return {
      flowItemName,
      flowName,
      data: {
        display_name: data.displayName as string,

        action_metadata_name: data.action_metadata_name,
        input_map: data.input_map,
        node_attributes: {
          description: data.description,
          position: position,
        },
      },
    }
  }

  if (type === NodeType.Subflow) {
    return {
      flowItemName,
      flowName,
      data: {
        display_name: data.displayName as string,

        subflow_id: data.referencedFlow,
        input_map: data.input_map,
        node_attributes: {
          description: data.description,
          position: position,
        },
      },
    }
  }

  if (type === NodeType.Assignment) {
    return {
      flowItemName,
      flowName,
      data: {
        display_name: data.displayName as string,

        assignment_variables: data.assignment_variables,
        node_attributes: {
          description: data.description,
          position: position,
        },
      },
    }
  }

  if (type === NodeType.Loop) {
    return {
      flowItemName,
      flowName,
      data: {
        display_name: data.displayName as string,

        collection_variable_name: data.collection_variable_name,
        loop_order: data.loop_order,
        node_attributes: {
          description: data.description,
          position: position,
        },
      },
    }
  }

  if (type === NodeType.DataCreate) {
    return {
      flowItemName,
      flowName,
      data: {
        display_name: data.displayName as string,

        object_name: data.object_name,
        config: {
          payload: parseConfigArrayToConfigObject(data.config),
        },
        node_attributes: {
          description: data.description,
          position: position,
        },
      },
    }
  }

  if (type === NodeType.DataGet) {
    return {
      flowItemName,
      flowName,
      data: {
        display_name: data.displayName,
        object_name: data.object_name,
        node_attributes: { description: data.description, position },
        config: {
          conditionLogic: data.condition as string,
          conditions: data.config,
          sortBy: data.sortBy as RecordReadConfig['sortBy'],
          sortOrder: data.sortOrder as RecordReadConfig['sortOrder'],
          getOptions: data.getOptions as RecordReadConfig['getOptions'],
        } as RecordReadConfig,
      } as CreateRecordReadDto,
    }
  }

  if (type === NodeType.DataUpdate) {
    return {
      flowItemName,
      flowName,
      data: {
        display_name: data.displayName as string,

        object_name: data.object_name,
        config: {
          payload: parseConfigArrayToConfigObject(data.config),
          guid: data.guid,
        },
        node_attributes: {
          description: data.description,
          position: position,
        },
      },
    }
  }

  if (type === NodeType.DataDelete) {
    return {
      flowItemName,
      flowName,
      data: {
        display_name: data.displayName as string,

        object_name: data.object_name,
        config: {
          guid: data.guid,
        },
        node_attributes: {
          description: data.description,
          position: position,
        },
      },
    }
  }

  return
}

//// SUPPORT FUNCTIONS
export function addRootLabel(script: Record<string, BaseScreenFIComponent>, flowItem: ScreenFlowItemResponse) {
  const { display_name } = flowItem
  const root = script[ROOT_ELEMENT]
  const editScript = {
    ...script,
    [ROOT_ELEMENT]: {
      ...root,
      props: {
        ...root.props,
        label: display_name,
      },
    },
  }
  return editScript
}

export function transformScreenScript(script: Record<string, ScreenElement>) {
  const elementIds = Object.keys(script)
  console.log('update', elementIds)

  return elementIds.reduce((prev, curId, _, array) => {
    const element = script[curId]
    const { id, name, type, component, props, children } = element

    if (!id || !name || !type || !component || !props || !children) {
      NToast.error({ title: 'Script is not in correct format' })
      array.slice(1)
      return undefined
    }

    let defaultIndexes: number[] | undefined = undefined
    if (element.component === ScreenElementComponent.Selection) {
      const { options, defaultSelectValue } = element.props

      if (defaultSelectValue) {
        defaultIndexes = defaultSelectValue.reduce((prev, defaultVal) => {
          const foundIndex = (options || []).findIndex(op => {
            return op.value === defaultVal
          })

          if (foundIndex < 0) {
            return prev
          }

          return [...prev, foundIndex]
        }, [] as number[])
      }
    }

    const transformed: ScreenFIComponentDto = {
      id,
      name,
      type,
      component,
      children,
      //@ts-ignore
      props: {
        ...props,
        defaultIndexes,
      },
    }

    return {
      ...prev,
      [id]: transformed,
    }
  }, {} as Record<string, ScreenFIComponentDto> | undefined)
}

export function renameDecisionOrder(order: string[], outcomes: Record<string, Outcome>) {
  return order.map(key => {
    return outcomes[key]?.name
  })
}

export function renameObjectKeyForDecisionOutcome(outcomes: Record<string, Outcome>) {
  const outcomeKeys = Object.keys(outcomes)
  const result = outcomeKeys.reduce((prev, curKey) => {
    const outcome = outcomes[curKey]
    return {
      ...prev,
      [outcome.name]: outcome,
    }
  }, {} as Record<string, Outcome>)
  return result
}

export function parseConfigArrayToConfigObject(array: DataFlowItemConfigType[]) {
  return array.reduce((object, curField) => {
    return {
      [curField.fieldName]: curField.value,
      ...object,
    }
  }, {} as Record<string, ContentValue>)
}

export function parseConfigObjectToConfigArray(object: Record<string, ContentValue>) {
  const keys = Object.keys(object)
  return keys.map(key => {
    return {
      fieldName: key,
      value: object[key],
    } as DataFlowItemConfigType
  })
}

export const renderVariableValue = (value?: ContentValue, content?: Record<string, any>) => {
  if (!value) {
    return ''
  }

  if (value.type === 'variable') {
    if (content) {
      return get(content, value.value)
    }
    return `{${value.value}}`
  }

  return value.value
}
