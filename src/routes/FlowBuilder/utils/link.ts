import { BORDER_RADIUS } from './constants'
import { XY } from './types'

const avg = (...args: number[]) => args.reduce((a, i) => a + i) / args.length

export const getCenterPoint = (source: XY, target: XY) => ({
  x: avg(source.x, target.x),
  y: avg(source.y, target.y),
})

export const getEdgeLine = (source: XY, target: XY, offset: XY, borderRadius = BORDER_RADIUS) => {
  const center = getCenterPoint(source, target)
  const dx = Math.abs(target.x - source.x)
  const dy = Math.abs(target.y - source.y)

  const size = Math.min(borderRadius, dx / 2, dy / 2)

  // corner 1 - 4 - 5 - 8
  if (dx < dy) {
    if (target.x > source.x) {
      if (target.y < source.y) {
        // corner 1
        return `
${topLeftCorner({ ...source, y: source.y - offset.y }, center, size)}
${bottomRightCorner(center, { ...target, y: target.y + offset.y }, size)}`
      }
      // corner 4
      return `
${bottomLeftCorner({ ...source, y: source.y + offset.y }, center, size)}
${topRightCorner(center, { ...target, y: target.y - offset.y }, size)}`
    }
    if (target.y > source.y) {
      // corner 5
      return `
${bottomRightCorner({ ...source, y: source.y + offset.y }, center, size)}
${topLeftCorner(center, { ...target, y: target.y - offset.y }, size)}`
    }
    // corner 8
    return `
${topRightCorner({ ...source, y: source.y - offset.y }, center, size)}
${bottomLeftCorner(center, { ...target, y: target.y + offset.y }, size)}`
  }
  // corner 2 - 3 - 6 - 7
  if (target.x > source.x) {
    if (target.y < source.y) {
      // corner 2
      return `
${bottomRightCorner({ ...source, x: source.x + offset.x }, center, size)}
${topLeftCorner(center, { ...target, x: target.x - offset.x }, size)}`
    }
    // corner 3
    return `
${topRightCorner({ ...source, x: source.x + offset.x }, center, size)}
${bottomLeftCorner(center, { ...target, x: target.x - offset.x }, size)}`
  }
  const sourceWithOffset = { ...source, x: source.x - offset.x }
  const targetWidthOffset = { ...target, x: target.x + offset.x }
  if (target.y > source.y) {
    // corner 6
    return `
${topLeftCorner(sourceWithOffset, center, size)}
${bottomRightCorner(center, targetWidthOffset, size)}`
  }
  // corner 7
  return `
${bottomLeftCorner(sourceWithOffset, center, size)}
${topRightCorner(center, targetWidthOffset, size)}`
}

const topLeftCorner = (source: XY, target: XY, size: number) => {
  // from top to bottom
  if (target.y > source.y) {
    return `
  M${source.x} ${source.y}
  L${target.x + size} ${source.y}
  Q${target.x} ${source.y} ${target.x} ${source.y + size}
  L${target.x} ${target.y}`
  }
  // from bottom to top
  return `
  M${source.x} ${source.y}
  L${source.x} ${target.y + size}
  Q${source.x} ${target.y} ${source.x + size} ${target.y}
  L${target.x} ${target.y}`
}

const bottomRightCorner = (source: XY, target: XY, size: number) => {
  // fromt left to right
  if (target.x > source.x) {
    return `
    M${source.x} ${source.y}
    L${target.x - size} ${source.y}
    Q${target.x} ${source.y} ${target.x} ${source.y - size}
    L${target.x} ${target.y}`
  }
  // from right to left
  return `
  M${source.x} ${source.y}
  L${source.x} ${target.y - size}
  Q${source.x} ${target.y} ${source.x - size} ${target.y}
  L${target.x} ${target.y}`
}

const topRightCorner = (source: XY, target: XY, size: number) => {
  // from top to bottom
  if (target.y > source.y) {
    return `
    M${source.x} ${source.y}
    L${target.x - size} ${source.y}
    Q${target.x} ${source.y} ${target.x} ${source.y + size}
    L${target.x} ${target.y}`
  }
  // from bottom to top
  return `
  M${source.x} ${source.y}
  L${source.x} ${target.y + size}
  Q${source.x} ${target.y} ${source.x - size} ${target.y}
  L${target.x} ${target.y}`
}
const bottomLeftCorner = (source: XY, target: XY, size: number) => {
  // from left to right
  if (target.x > source.x) {
    return `
    M${source.x} ${source.y}
    L${source.x} ${target.y - size}
    Q${source.x} ${target.y} ${source.x + size} ${target.y}
    L${target.x} ${target.y}`
  }
  // from right to left
  return `
  M${source.x} ${source.y}
  L${target.x + size} ${source.y}
  Q${target.x} ${source.y} ${target.x} ${source.y - size}
  L${target.x} ${target.y}`
}
