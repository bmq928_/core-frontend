import { ContentValue } from '../../../services/api/models'

export type CommonWidgetDataType = {
  name: string
  displayName: string
  description?: string
} & Record<string, any>

export type WidgetItemType<Data = CommonWidgetDataType> = {
  leftIcon?: JSX.Element
  flowIcon?: JSX.Element
  label: string
  type: NodeType
  data: Data
}

export type NodeModalType<Data = CommonWidgetDataType> = {
  nodeId?: string
  position: XY
  type: NodeType
  data?: Data
}

export type FlowNodeModalProps = {
  nodeModal: NodeModalType
  isSubmitting?: boolean
  onSubmit: (data: CommonWidgetDataType) => void
  onCancel: () => void
}

export type FlowNode<Data = CommonWidgetDataType> = {
  id: string
  position: { x: number; y: number }
  outcome: string[]
  data: Data
  type: NodeType
  name: string
}

export enum NodeType {
  Screen = 'screen',
  Action = 'action',
  Decision = 'decision',
  Loop = 'loop',
  Subflow = 'sub-flow',
  Assignment = 'assignment',
  DataCreate = 'create',
  DataUpdate = 'update',
  DataGet = 'read',
  DataDelete = 'delete',
}

export const NODE_TYPE_DATA = [NodeType.DataCreate, NodeType.DataUpdate, NodeType.DataGet, NodeType.DataDelete]

export type LinkModalType = {
  srcNode: { id: string; name: string; nodeType: NodeType }
  targetNode: FlowNode
}
export type FlowLink<Data = {}> = {
  id: string
  source: string
  target: string
  data: Data
  type: LinkType
  isFault: boolean
  outcomeName?: string
}

export enum LinkType {
  Failure = 'FAILURE_LINK',
  Success = 'SUCCESS_LINK',
}

export type XY = {
  x: number
  y: number
}

export type SourceXY = {
  sourceX: number
  sourceY: number
}

export type TargetXY = {
  targetX: number
  targetY: number
}

export enum DroppableType {
  AddNode = 'DROPPABLE_ADD_NODE',
  UpdateNode = 'DROPPABLE_UPDATE_NODE',
  ConnectNodes = 'DROPPABLE_CONNECT_NODES',
}

//
export type DataFlowItemConfigType = {
  fieldName: string
  value: ContentValue
}

//
export enum FlowVariablePickerType {
  Variable = 'variable',
  Constant = 'constant',
}

export type VariableOption = {
  value: string
  type: FlowVariablePickerType
}
