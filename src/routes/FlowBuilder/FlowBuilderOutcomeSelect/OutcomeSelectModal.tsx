import React, { FC, useMemo } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useQuery } from 'react-query'
import { useDispatch } from 'react-redux'
import { useParams } from 'react-router-dom'
import styled from 'styled-components'
import { spacing } from '../../../components/GlobalStyle'
import { NButton } from '../../../components/NButton/NButton'
import { NModal } from '../../../components/NModal/NModal'
import { SelectOption } from '../../../components/NSelect/model'
import { NSingleSelect } from '../../../components/NSelect/NSingleSelect'
import { APIServices } from '../../../services/api'
import { FlowsQueryKey } from '../../../services/api/endpoints/flows'
import { CreateFlowItemConnectionDto } from '../../../services/api/models'
import { builderActions } from '../redux/builderSlice'
import { useBuilderState } from '../redux/hooks/useBuilderState'
import { FlowNode } from '../utils/types'

const FormWrapper = styled.div`
  padding: ${spacing('lg')} ${spacing('xl')};
`
type outcomeFormType = {
  outcomeName: string
}

type Props = {
  srcNodeName: string
  targetNode: FlowNode
  createConnection(args: { flowName: string } & CreateFlowItemConnectionDto): void
  isCreating: boolean
}

export const OutcomeSelectModal: FC<Props> = ({ srcNodeName, targetNode, createConnection, isCreating }) => {
  const { flowName } = useParams<{ flowName: string }>()
  const { control, handleSubmit } = useForm()
  const dispatch = useDispatch()
  const { links } = useBuilderState()

  const {
    data: { srcFlowItem, outcomeOptions } = { srcFlowItem: undefined, outcomeOptions: [] },
    isLoading: isLoadingFlowItem,
  } = useQuery([FlowsQueryKey.getFlowItem, { flowItemName: srcNodeName, flowName }], APIServices.Flows.getFlowItem, {
    select: data => {
      if (data.data.type !== 'decision') {
        return {
          srcFlowItem: data.data,
          outcomeOptions: [],
        }
      }

      const { outcomes } = data.data

      const outcomeKeys = Object.keys(outcomes)
      const options = outcomeKeys.map(key => {
        return {
          label: outcomes[key].displayName,
          value: key,
        }
      })
      options.push({ label: data.data.default_outcome_label, value: '' })
      return {
        srcFlowItem: data.data,
        outcomeOptions: options,
      }
    },
  })

  const filteredOutcomeOptions: SelectOption[] = useMemo(() => {
    // filter all connected outcomes
    return outcomeOptions.filter(item => {
      return !links.some(link => {
        // filter out existing links. becuz default outcome doesn't have src_out_name so need to check source and outcome_name = null
        return (
          link.outcomeName === item.value ||
          (item.value === '' && link.source === srcFlowItem?.name && link.outcomeName === null)
        )
      })
    })
  }, [srcFlowItem, outcomeOptions, links])

  const onSubmit = (data: outcomeFormType) => {
    if (srcFlowItem) {
      createConnection({
        flowName,
        src_item_name: srcFlowItem.name,
        trg_item_name: targetNode.name,
        src_outcome_name: data.outcomeName || undefined,
      })
    }
  }

  return (
    <>
      <NModal.Header
        title="Select outcome"
        onClose={() => {
          dispatch(builderActions.closeLinkModal())
        }}
      />
      <NModal.Body>
        <FormWrapper>
          <Controller
            defaultValue={filteredOutcomeOptions[0]?.value || ''}
            control={control}
            name="outcomeName"
            render={({ field }) => (
              <NSingleSelect
                placeholder="Select outcome"
                fullWidth
                isLoading={isLoadingFlowItem}
                options={filteredOutcomeOptions}
                value={field.value}
                onValueChange={value => {
                  field.onChange(value)
                }}
              />
            )}
          />
        </FormWrapper>
      </NModal.Body>
      <NModal.Footer>
        <NButton size="small" type="ghost" onClick={() => {}}>
          Cancel
        </NButton>
        <NButton
          size="small"
          type="primary"
          loading={isCreating}
          onClick={handleSubmit(onSubmit)}
          disabled={filteredOutcomeOptions.length === 0}>
          Submit
        </NButton>
      </NModal.Footer>
    </>
  )
}
