import React, { FC, useMemo } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useDispatch } from 'react-redux'
import { useParams } from 'react-router'
import styled from 'styled-components'
import { spacing } from '../../../components/GlobalStyle'
import { NButton } from '../../../components/NButton/NButton'
import { NModal } from '../../../components/NModal/NModal'
import { SelectOption } from '../../../components/NSelect/model'
import { NSingleSelect } from '../../../components/NSelect/NSingleSelect'
import { CreateFlowItemConnectionDto } from '../../../services/api/models'
import { builderActions } from '../redux/builderSlice'
import { useBuilderState } from '../redux/hooks/useBuilderState'
import { LinkModalType } from '../utils/types'

type outcomeFormType = {
  outcomeName: string
}

const FormWrapper = styled.div`
  padding: ${spacing('lg')} ${spacing('xl')};
`

type Props = LinkModalType & {
  createConnection(args: { flowName: string } & CreateFlowItemConnectionDto): void
  isCreating: boolean
}

export const LoopOutcomeSelectModal: FC<Props> = ({ srcNode, targetNode, createConnection, isCreating }) => {
  const { flowName } = useParams<{ flowName: string }>()
  const { control, handleSubmit } = useForm()
  const dispatch = useDispatch()
  const { links } = useBuilderState()

  const filteredOutcomeOptions: SelectOption[] = useMemo(() => {
    const outcomeOptions: SelectOption[] = [
      { label: 'For each', value: 'forEach' },
      { label: 'After all', value: 'afterAll' },
    ]
    // filter all connected outcomes
    return outcomeOptions.filter(item => {
      return !links.some(link => {
        // filter out existing links. becuz default outcome doesn't have src_out_name so need to check source and outcome_name = null
        return (
          link.outcomeName === item.value ||
          (item.value === '' && link.source === srcNode.name && link.outcomeName === null)
        )
      })
    })
  }, [srcNode, links])

  const onSubmit = (data: outcomeFormType) => {
    createConnection({
      flowName,
      src_item_name: srcNode.name,
      trg_item_name: targetNode.name,
      src_outcome_name: data.outcomeName || undefined,
    })
  }

  return (
    <>
      <NModal.Header
        title="Select outcome"
        onClose={() => {
          dispatch(builderActions.closeLinkModal())
        }}
      />
      <NModal.Body>
        <FormWrapper>
          <Controller
            defaultValue={filteredOutcomeOptions[0]?.value || ''}
            control={control}
            name="outcomeName"
            render={({ field }) => (
              <NSingleSelect
                placeholder="Select outcome"
                fullWidth
                options={filteredOutcomeOptions}
                value={field.value}
                onValueChange={value => {
                  field.onChange(value)
                }}
              />
            )}
          />
        </FormWrapper>
      </NModal.Body>
      <NModal.Footer>
        <NButton size="small" type="ghost" onClick={() => {}}>
          Cancel
        </NButton>
        <NButton
          size="small"
          type="primary"
          loading={isCreating}
          onClick={handleSubmit(onSubmit)}
          disabled={filteredOutcomeOptions.length === 0}>
          Submit
        </NButton>
      </NModal.Footer>
    </>
  )
}
