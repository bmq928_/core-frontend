import React, { FC } from 'react'
import { useMutation, useQueryClient } from 'react-query'
import { useDispatch } from 'react-redux'
import { useParams } from 'react-router'
import { NToast } from '../../../components/NToast'
import { APIServices } from '../../../services/api'
import { FlowsQueryKey } from '../../../services/api/endpoints/flows'
import { CreateFlowItemConnectionDto } from '../../../services/api/models'
import { builderActions } from '../redux/builderSlice'
import { LinkModalType, NodeType } from '../utils/types'
import { LoopOutcomeSelectModal } from './LoopOutcomeSelectModal'
import { OutcomeSelectModal } from './OutcomeSelectModal'

type Props = {
  openedLinkModal: LinkModalType
}

export const LinkModal: FC<Props> = ({ openedLinkModal }) => {
  const { flowName } = useParams<{ flowName: string }>()
  const dispatch = useDispatch()

  const queryClient = useQueryClient()

  const { mutate: createConnection, isLoading: isCreating } = useMutation(APIServices.Flows.createFlowConnection, {
    onSuccess(data) {
      dispatch(
        builderActions.connectNodes({
          sourceName: data.data.src_item_name,
          targetName: data.data.trg_item_name,
          sourceOutcomeName: data.data.src_outcome_name,
          id: data.data.guid,
          isFault: data.data.is_fault || false,
        }),
      )
      queryClient.invalidateQueries([FlowsQueryKey.getFlowItems, { flowName }])
      queryClient.invalidateQueries([FlowsQueryKey.getFlowConnections, { flowName }])
      dispatch(builderActions.closeLinkModal())
    },
    onError: error => {
      NToast.error({
        title: 'Create connection unsuccessful',
        subtitle: error.response?.data.message,
      })
    },
  })

  const handleCreateConnection = (args: { flowName: string } & CreateFlowItemConnectionDto) => {
    createConnection(args)
  }

  const { srcNode, targetNode } = openedLinkModal

  if (srcNode.nodeType === NodeType.Decision) {
    return (
      <OutcomeSelectModal
        srcNodeName={srcNode.name}
        targetNode={targetNode}
        createConnection={handleCreateConnection}
        isCreating={isCreating}
      />
    )
  }

  if (srcNode.nodeType === NodeType.Loop) {
    return (
      <LoopOutcomeSelectModal
        srcNode={srcNode}
        targetNode={targetNode}
        createConnection={handleCreateConnection}
        isCreating={isCreating}
      />
    )
  }

  return <div>Not supported yet</div>
}
