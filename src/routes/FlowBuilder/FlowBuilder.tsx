import * as React from 'react'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { useHistory, useParams } from 'react-router-dom'
import styled from 'styled-components'
import { DASHBOARD_ROUTE } from '../../common/constants'
import { BuilderHeader } from '../../components/Builder/BuilderHeader'
import { color } from '../../components/GlobalStyle'
import { SidebarIcons } from '../../components/Icons'
import { NBreadcrumbs } from '../../components/NBreadcrumbs/NBreadcrumbs'
import { NButton } from '../../components/NButton/NButton'
import { NDivider } from '../../components/NDivider'
import { NToast } from '../../components/NToast'
import { APIServices } from '../../services/api'
import { FlowsQueryKey } from '../../services/api/endpoints/flows'
import { FlowBuilderProvider } from './components/FlowBuilderProvider'
import { FlowRenderer } from './components/FlowRenderer'
import { Sidebar } from './components/Sidebar'
import { FlowBuilderVariable } from './FlowBuilderVariable/FlowBuilderVariable'

const Wrapper = styled('div')`
  display: grid;
  grid-template-rows: auto 1fr;
  grid-template-columns: 251px 1fr;
  grid-template-areas:
    'header header'
    'sidebar droppable-zone';
  width: 100vw;
  height: 100vh;
  background: ${color('Neutral200')};
  overflow: hidden;
`

const HeaderBtn = styled(NButton)`
  min-width: 120px;
`

type Props = {}

export function FlowBuilder({}: Props) {
  const { flowName } = useParams<{ flowName: string }>()
  const queryClient = useQueryClient()
  const history = useHistory()

  const { data: flowData, isFetching: isLoadingFlowData } = useQuery(
    [FlowsQueryKey.getFlow, { flowName }],
    APIServices.Flows.getFlow,
    {
      onError: () => {
        history.push(`${DASHBOARD_ROUTE}/flow`)
      },
    },
  )

  const { mutate: activateFlow, isLoading: isActivating } = useMutation(APIServices.Flows.activateFlow, {
    onSuccess: () => {
      queryClient.invalidateQueries([FlowsQueryKey.getFlow, { flowName }])
    },
    onError: error => {
      NToast.error({
        title: 'Activate unsuccessful',
        subtitle: error.response?.data.message,
      })
    },
  })

  const { mutate: deactivateFlow, isLoading: isDeActivating } = useMutation(APIServices.Flows.deactivateFlow, {
    onSuccess: () => {
      queryClient.invalidateQueries([FlowsQueryKey.getFlow, { flowName }])
    },
    onError: error => {
      NToast.error({
        title: 'Deactivate unsuccessful',
        subtitle: error.response?.data.message,
      })
    },
  })

  // get flow items
  const { data: flowItems, isLoading: isLoadingFlowItems } = useQuery(
    [FlowsQueryKey.getFlowItems, { flowName }],
    APIServices.Flows.getFlowItems,
    { keepPreviousData: true, staleTime: 0 },
  )

  const { data: flowConnections, isLoading: isLoadingFlowConnections } = useQuery(
    [FlowsQueryKey.getFlowConnections, { flowName }],
    APIServices.Flows.getFlowConnections,
    {
      keepPreviousData: true,
      staleTime: 0,
    },
  )

  const [showVariableManager, setShowVariableManager] = React.useState(false)

  const handleShowVariableManager = () => {
    setShowVariableManager(true)
  }

  const updateFlowStatus = () => {
    if (flowData) {
      if (flowData.data.is_active) {
        deactivateFlow({ flowName })
        return
      }
      activateFlow({ flowName })
    }
  }

  return (
    <Wrapper>
      <BuilderHeader
        title="Flow Builder"
        icon={<SidebarIcons.AutomationOutlined />}
        style={{ gridArea: 'header' }}
        rightElement={
          <React.Fragment>
            <HeaderBtn size="small" type="outline" loading={isLoadingFlowData} onClick={handleShowVariableManager}>
              Variable Manager
            </HeaderBtn>
            <NDivider vertical size="xs" />
            <HeaderBtn
              size="small"
              type="outline"
              loading={isLoadingFlowData || isActivating || isDeActivating}
              onClick={updateFlowStatus}>
              {flowData?.data.is_active ? 'Deactivate' : 'Activate'}
            </HeaderBtn>
            <NDivider vertical size="xs" />
            <HeaderBtn disabled={isLoadingFlowData} size="small" type="primary">
              Save
            </HeaderBtn>
          </React.Fragment>
        }>
        <NBreadcrumbs
          breadcrumbs={[
            { label: 'Flow', path: `${DASHBOARD_ROUTE}/flow` },
            { label: flowData?.data.display_name || '' },
          ]}
        />
      </BuilderHeader>
      <FlowBuilderProvider nodes={flowItems?.data || []} links={flowConnections?.data || []}>
        <Sidebar flowType={flowData?.data.flow_type} />
        <FlowRenderer flowType={flowData?.data.flow_type} isLoading={isLoadingFlowItems || isLoadingFlowConnections} />
      </FlowBuilderProvider>
      {showVariableManager && <FlowBuilderVariable visible={showVariableManager} setVisible={setShowVariableManager} />}
    </Wrapper>
  )
}
