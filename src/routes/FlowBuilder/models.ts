import { AndLogicRule, AssignmentVariableDto } from '../../services/api/models'
import { PartialDecisionOutcomeData } from './FlowBuilderItem/FlowItemDecision/DecisionOutcomeMap'
import { CommonWidgetDataType } from './utils/types'

export type DecisionFormType = CommonWidgetDataType & {
  default_outcome_label: string
  outcomes: PartialDecisionOutcomeData[]
}

export type OutcomeDetail = {
  script: AndLogicRule[]
  displayName: string
  name: string
  condition: string
}

export type AssignmentFormType = CommonWidgetDataType & {
  assignment_variables: AssignmentVariableDto[]
}

export type LoopFormType = CommonWidgetDataType & {
  collection_variable_name: string
  loop_order: 'ASC' | 'DESC'
}

export type SubFlowFormType = CommonWidgetDataType & {
  referencedFlow?: string
  input_map: Record<string, any>
}

export type ActionFormType = CommonWidgetDataType & {
  action_metadata_id?: string
  input_map: Record<string, any>
}

export type DataCreateFormType = CommonWidgetDataType & {
  object_name: string
  config: { fieldName: string; value: string }[]
}

export type DataGetFormType = CommonWidgetDataType & {
  object_name: string
  config: { fieldName: string; value: string; sign: string }[]
  sortOrder: 'DESC' | 'ASC'
}

export type DataUpdateFormType = CommonWidgetDataType & {
  object_name: string
  guid: string
  config: { fieldName: string; value: string }[]
}

export type DataDeleteFormType = CommonWidgetDataType & {
  object_name: string
  guid: string
}
