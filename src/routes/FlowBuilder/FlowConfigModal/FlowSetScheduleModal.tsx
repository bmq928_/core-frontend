import { format, parse } from 'date-fns'
import React from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useMutation, useQueryClient } from 'react-query'
import { useParams } from 'react-router-dom'
import styled from 'styled-components'
import { NDatePicker } from '../../../components/DateTime/NDatePicker'
import { NTimePicker } from '../../../components/DateTime/NTimePicker'
import { spacing } from '../../../components/GlobalStyle'
import { NDivider } from '../../../components/NDivider'
import { NColumn, NRow } from '../../../components/NGrid/NGrid'
import { NModal } from '../../../components/NModal/NModal'
import { NPerfectScrollbar } from '../../../components/NPerfectScrollbar'
import { NSingleSelect } from '../../../components/NSelect/NSingleSelect'
import { NToast } from '../../../components/NToast'
import { APIServices } from '../../../services/api'
import { FlowsQueryKey } from '../../../services/api/endpoints/flows'
import { ScheduleFlowResponse, UpdateScheduleFlowDto } from '../../../services/api/models'

const FREQUENCY_OPTIONS = [
  { label: 'Once', value: 'once' },
  { label: 'Daily', value: 'daily' },
  { label: 'Weekly', value: 'weekly' },
]

const Wrapper = styled.div`
  padding: ${spacing('lg')} ${spacing('xl')};
`
type ScheduleFormType = {
  start_date: Date
  start_time: Date
  frequency: UpdateScheduleFlowDto['frequency']
}

type Props = {
  showModal: boolean
  setShowModal: (val: boolean) => void
  flowData?: ScheduleFlowResponse
}

export const FlowSetScheduleModal: React.FC<Props> = ({ showModal, setShowModal, flowData }) => {
  const { handleSubmit, control } = useForm()
  const queryClient = useQueryClient()
  const { flowName } = useParams<{ flowName: string }>()

  const { mutate: updateFlow, isLoading: isUpdating } = useMutation(APIServices.Flows.updateFlow, {
    onSuccess: async () => {
      await queryClient.invalidateQueries([FlowsQueryKey.getFlow, { flowName }])
      setShowModal(false)
    },
    onError: error => {
      NToast.error({ title: 'Update unsuccesful', subtitle: error.response?.data.message })
    },
  })

  const onSubmit = (data: ScheduleFormType) => {
    updateFlow({
      flowName,
      data: {
        frequency: data.frequency,
        start_date: format(data.start_date, 'yyyy-MM-dd'),
        start_time: format(data.start_time, 'HH:mm'),
      },
    })
  }

  return (
    <NModal size="large" setVisible={setShowModal} visible={showModal}>
      <NModal.Header onClose={() => setShowModal(false)} title="Set a schedule" />
      <NModal.Body>
        <NPerfectScrollbar style={{ maxHeight: '60vh' }}>
          <form>
            <Wrapper>
              <NRow>
                <NColumn flex={1}>
                  <Controller
                    defaultValue={
                      flowData?.start_date ? parse(flowData.start_date, 'yyyy-MM-dd', new Date()) : new Date()
                    }
                    control={control}
                    name="start_date"
                    render={({ field }) => (
                      <NDatePicker
                        placeholder="Set Date"
                        label="Date"
                        value={field.value}
                        onChangeValue={value => {
                          field.onChange(value)
                        }}
                      />
                    )}
                  />
                </NColumn>
                <NDivider vertical size="md" />
                <NColumn flex={1}>
                  <Controller
                    defaultValue={flowData?.start_time ? parse(flowData.start_time, 'HH:mm', new Date()) : new Date()}
                    control={control}
                    name="start_time"
                    render={({ field }) => (
                      <NTimePicker
                        secondPicker={false}
                        placeholder="Set Time"
                        label="Time"
                        value={field.value}
                        minuteInterval={15}
                        onChangeValue={value => {
                          field.onChange(value)
                        }}
                      />
                    )}
                  />
                </NColumn>
              </NRow>
              <NDivider size="md" />
              <NRow>
                <NColumn flex={1}>
                  <Controller
                    defaultValue={flowData?.frequency || FREQUENCY_OPTIONS[0].value}
                    control={control}
                    name="frequency"
                    render={({ field }) => (
                      <NSingleSelect
                        options={FREQUENCY_OPTIONS}
                        placeholder="Frequency"
                        label="Frequency"
                        value={field.value}
                        onValueChange={value => {
                          field.onChange(value)
                        }}
                      />
                    )}
                  />
                </NColumn>
              </NRow>
            </Wrapper>
          </form>
        </NPerfectScrollbar>
      </NModal.Body>
      <NModal.Footer isLoading={isUpdating} onCancel={() => setShowModal(false)} onFinish={handleSubmit(onSubmit)} />
    </NModal>
  )
}
