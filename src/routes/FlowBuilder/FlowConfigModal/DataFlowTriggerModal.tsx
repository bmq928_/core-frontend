import React from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useMutation, useQueryClient } from 'react-query'
import { useParams } from 'react-router-dom'
import styled, { css, useTheme } from 'styled-components'
import { color, spacing } from '../../../components/GlobalStyle'
import { GlobalIcons } from '../../../components/Icons'
import { NDivider } from '../../../components/NDivider'
import { NColumn, NRow } from '../../../components/NGrid/NGrid'
import { NModal } from '../../../components/NModal/NModal'
import { NPerfectScrollbar } from '../../../components/NPerfectScrollbar'
import { NRadio } from '../../../components/NRadio/NRadio'
import { NToast } from '../../../components/NToast'
import { typography } from '../../../components/NTypography'
import { APIServices } from '../../../services/api'
import { FlowsQueryKey } from '../../../services/api/endpoints/flows'
import { DataFlowResponse, UpdateDataFlowDto } from '../../../services/api/models'

const Wrapper = styled.div`
  padding: ${spacing('lg')} ${spacing('xl')};
  display: flex;
  flex-direction: column;
`
const TextHeader = styled.div`
  color: ${color('Neutral500')};
  margin-bottom: ${spacing('xs')};
`
const WrapperRadio = styled.div`
  padding: ${spacing(8)} 0;
  color: ${color('Neutral700')};
`
const TextDescription = styled.div`
  ${typography('small-bold-text')};
  color: ${color('Neutral500')};
  padding: ${spacing('md')} 0;
  line-height: 21px;
`

const DescriptionRadio = styled.div`
  color: ${color('Neutral400')};
  padding: ${spacing('sm')} ${spacing('xl')};
  line-height: 18px;
`
const NoteWrapper = styled.div`
  background: ${color('Neutral200')};
  margin-top: ${spacing('md')};
  border-radius: ${spacing(4)};
`

const NoteContent = styled.div`
  display: flex;
  justify-content: space-between;
  padding: ${spacing('lg')} 0;
  align-items: center;
`

const NoteIcon = styled.div`
  padding: 0 ${spacing('lg')};
`

const NoteText = styled.div`
  flex: 1;
  line-height: ${spacing(18)};
  color: ${color('Neutral400')};
`

const StyledColumn = styled(NColumn)<{ isBorder?: boolean }>`
  padding: ${spacing('xs')} !important;
  border-radius: ${spacing(4)};
  ${props => {
    const { isBorder } = props
    if (isBorder) {
      return css`
        border: 1px solid ${color('Primary700')};
      `
    }
    return css`
      border: 1px solid ${color('white')};
    `
  }};
`

const RECORD_ACTIONS = [
  {
    value: 'create',
    title: 'A record is created',
  },
  {
    value: 'update',
    title: 'A record is updated',
  },
  {
    value: 'delete',
    title: 'A record is deleted',
  },
]

type ActionFormType = {
  action: UpdateDataFlowDto['action']
}

type Props = {
  showModal: boolean
  setShowModal: (val: boolean) => void
  flowData?: DataFlowResponse
}
export const DataFlowTriggerModal: React.FC<Props> = ({ showModal, setShowModal, flowData }) => {
  const { handleSubmit, control, watch } = useForm()
  const theme = useTheme()
  const { flowName } = useParams<{ flowName: string }>()
  const queryClient = useQueryClient()
  const watchAction = watch('action', flowData?.action)

  const { mutate: updateFlow, isLoading: isUpdating } = useMutation(APIServices.Flows.updateFlow, {
    onSuccess: async () => {
      await queryClient.invalidateQueries([FlowsQueryKey.getFlow, { flowName }])
      setShowModal(false)
    },
    onError: error => {
      NToast.error({ title: 'Update unsuccesful', subtitle: error.response?.data.message })
    },
  })

  const onSubmit = (data: ActionFormType) => {
    updateFlow({
      flowName,
      data: { action: data.action },
    })
  }

  const [selectedValue, setSelectedValue] = React.useState<string | undefined>()

  return (
    <NModal size="large" setVisible={setShowModal} visible={showModal}>
      <NModal.Header onClose={() => setShowModal(false)} title="Configure Trigger" />
      <NModal.Body>
        <NPerfectScrollbar style={{ maxHeight: '70vh' }}>
          <form>
            <Wrapper>
              <TextHeader>Trigger the Flow When</TextHeader>
              <Controller
                defaultValue={flowData?.action || ''}
                control={control}
                name="action"
                render={({ field }) => (
                  <>
                    {RECORD_ACTIONS.map(radio => (
                      <WrapperRadio key={radio.value}>
                        <NRadio
                          name="config"
                          value={radio.value}
                          checked={field.value === radio.value}
                          onChange={e => {
                            setSelectedValue(undefined)
                            field.onChange(e.target.value)
                          }}
                          title={radio.title}
                        />
                      </WrapperRadio>
                    ))}
                  </>
                )}
              />
            </Wrapper>
            <NDivider size={1} lineSize={1} lineColor={color('Neutral300')({ theme })} />
            {watchAction === 'update' || watchAction === 'create' ? (
              <Wrapper>
                <div>Run the Flow</div>
                <TextDescription>
                  The flow can access any record and perform actions before the triggering record is deleted from the
                  database. To create, update, or delete other records, add Create Records, Update Records, and Delete
                  Records elements to your flow. The $Record global variable contains the CORE record that launches the
                  flow.
                </TextDescription>
                <NRow>
                  <Controller
                    name="save"
                    defaultValue={'after'}
                    control={control}
                    render={({ field }) => (
                      <>
                        <StyledColumn flex={1} isBorder={selectedValue === 'before'}>
                          <NRadio
                            name="save"
                            value="before"
                            checked={field.value === 'before'}
                            title="Before the record is saved"
                            onChange={e => {
                              setSelectedValue(e.target.value)
                              field.onChange(e.target.value)
                            }}
                            disabled
                          />

                          <DescriptionRadio>
                            If all you need is to update the record that launches the flow, select this option to
                            quickly build a high-performance flow. To update the record, simply use an Assignment
                            element to set fields on the $Record global variable. CORE handles the rest for you.
                          </DescriptionRadio>
                        </StyledColumn>
                        <NDivider vertical size="md" />
                        <StyledColumn flex={1} isBorder={selectedValue === 'after'}>
                          <NRadio
                            name="save"
                            value="after"
                            checked={field.value === 'after'}
                            title="After the record is saved"
                            onChange={e => {
                              setSelectedValue(e.target.value)
                              field.onChange(e.target.value)
                            }}
                          />
                          <DescriptionRadio>
                            To build a richer flow that can access any record and perform actions, select this option.
                            This flow can access most flow elements. The $Record global variable contains the CORE
                            record that launches the flow.
                          </DescriptionRadio>
                        </StyledColumn>
                      </>
                    )}
                  />
                </NRow>
              </Wrapper>
            ) : (
              <Wrapper>
                <div>Run the Flow</div>
                <NDivider size="md" />
                <NRow>
                  <StyledColumn flex={1} isBorder={selectedValue === 'after'}>
                    <Controller
                      defaultValue={'after'}
                      control={control}
                      name="save"
                      render={({ field }) => {
                        return (
                          <NRadio
                            name="save"
                            value={field.value}
                            checked={true}
                            title="After the record is saved"
                            onChange={e => {
                              setSelectedValue(e.target.value)
                              field.onChange(e.target.value)
                            }}
                          />
                        )
                      }}
                    />
                    <DescriptionRadio>
                      If all you need is to update the record that launches the flow, select this option to quickly
                      build a high-performance flow. To update the record, simply use an Assignment element to set
                      fields on the $Record global variable. CORE handles the rest for you.
                    </DescriptionRadio>
                  </StyledColumn>
                </NRow>
                <NoteWrapper>
                  <NoteContent>
                    <NoteIcon>
                      <GlobalIcons.Info />
                    </NoteIcon>
                    <NoteText>
                      If a deleted record is later recovered, the flow’s database changes and actions aren’t rolled
                      back. Also, record recovery can’t trigger a flow to run.
                    </NoteText>
                  </NoteContent>
                </NoteWrapper>
              </Wrapper>
            )}
          </form>
        </NPerfectScrollbar>
      </NModal.Body>
      <NModal.Footer isLoading={isUpdating} onCancel={() => setShowModal(false)} onFinish={handleSubmit(onSubmit)} />
    </NModal>
  )
}
