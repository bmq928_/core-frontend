import React, { Fragment } from 'react'
import { Controller, useFieldArray, useForm } from 'react-hook-form'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { useParams } from 'react-router-dom'
import styled, { useTheme } from 'styled-components'
import { color, spacing } from '../../../components/GlobalStyle'
import { GlobalIcons } from '../../../components/Icons'
import { NDivider } from '../../../components/NDivider'
import { NColumn, NRow } from '../../../components/NGrid/NGrid'
import { NModal } from '../../../components/NModal/NModal'
import { SelectOption } from '../../../components/NSelect/model'
import { NSingleSelect } from '../../../components/NSelect/NSingleSelect'
import { NTextInput } from '../../../components/NTextInput/NTextInput'
import { NToast } from '../../../components/NToast'
import { NTypography, typography } from '../../../components/NTypography'
import { useSearchText } from '../../../hooks/useSearchText'
import { APIServices } from '../../../services/api'
import { BuilderQueryKey } from '../../../services/api/endpoints/builder'
import { FlowsQueryKey } from '../../../services/api/endpoints/flows'
import {
  ConditionType,
  ContentValue,
  DataFlowCondition,
  DataFlowResponse,
  FlowVariableResponse,
} from '../../../services/api/models'
import { AddBtn } from '../../Dashboard/routes/Object/components/AddBtn'
import { LogicOperator } from '../../Dashboard/routes/Object/components/LogicOperator'
import { VarOptionContainer } from '../FlowBuilderItem/CommonStyledComponents'
import { DeleteBtn } from '../FlowBuilderItem/FlowItemData/components/CommonComponents'

const ScrollWrapper = styled.div`
  max-height: 70vh;
  overflow-y: auto;
`

const Wrapper = styled.div`
  padding: ${spacing('lg')} ${spacing('xl')};
`
const TextHeader = styled.div`
  color: ${color('Neutral500')};
`

const IndexTxt = styled.p`
  ${typography('small-bold-text')}
  min-width: 15px;
`

const AddBtnWrapper = styled(AddBtn)`
  margin-left: ${spacing('sm')};
`

const CONDITIONS = [
  { label: 'NONE', value: 'none' },
  { label: 'All conditions are met (AND)', value: 'and' },
  { label: 'Any condition is met (OR)', value: 'or' },
  { label: 'Custom condition logic is met', value: 'custom' },
]

const SIGNS = [
  '===',
  '!==',
  '<=',
  '>=',
  '<',
  ' >',
  'isNull',
  'isNotNull',
  'startsWith',
  'endsWith',
  'contains',
  'notContains',
  'wasSet',
]

type ObjectFormType = {
  object_name: string
  conditionLogic: DataFlowCondition['conditionLogic']
  customConditionLogic: string
  conditionClauses: (ConditionType & { value: ContentValue })[]
}

type Props = {
  showModal: boolean
  setShowModal: (val: boolean) => void
  flowData?: DataFlowResponse
}
export const FlowSelectObjectModal: React.FC<Props> = ({ showModal, setShowModal, flowData }) => {
  const { watch, control, handleSubmit, register } = useForm<ObjectFormType>({
    shouldUnregister: false,
    defaultValues: {
      object_name: undefined,
      conditionLogic: undefined,
      customConditionLogic: '',
      conditionClauses: flowData?.conditions?.conditionClauses || [
        {
          field: '',
          sign: '===',
          value: {
            type: 'variable',
            value: undefined,
          },
        },
      ],
    },
  })
  const { fields, append, remove } = useFieldArray({
    control,
    name: 'conditionClauses',
  })

  const { flowName } = useParams<{ flowName: string }>()
  const theme = useTheme()
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText()
  const queryClient = useQueryClient()
  const watchObjectName = watch('object_name', flowData?.object_name)
  const watchConditionLogic = watch('conditionLogic', flowData?.conditions?.conditionLogic)

  // get all objects
  const { data: allObjects, isLoading: isLoadingObjects } = useQuery(
    [BuilderQueryKey.getAllObjects, { searchText }],
    APIServices.Builder.getAllObjects,
    {
      keepPreviousData: true,
      select: data => {
        return data.data.data.map(option => {
          const typeName = option.isExternal ? ' (external)' : ' (internal)'
          return {
            value: option.name,
            label: option.displayName + typeName,
          }
        }) as SelectOption[]
      },
    },
  )
  //
  const { mutate: updateFlow, isLoading: isUpdating } = useMutation(APIServices.Flows.updateFlow, {
    onSuccess: async () => {
      await queryClient.invalidateQueries([FlowsQueryKey.getFlow, { flowName }])
      setShowModal(false)
    },
    onError: error => {
      NToast.error({ title: 'Update unsuccesful', subtitle: error.response?.data.message })
    },
  })

  const { data: fieldData, isLoading: isLoadingFields } = useQuery(
    [BuilderQueryKey.getObjectSchema, { objName: watchObjectName! }],

    APIServices.Builder.getObjectSchema,
    {
      enabled: !!watchObjectName,
    },
  )

  // get flow item variables
  const { data: { selectOptions } = { selectOptions: [], variables: {} } } = useQuery(
    [FlowsQueryKey.getFlowVariables, { flowName }],
    APIServices.Flows.getFlowVariables,
    {
      enabled: !!flowName,
      select: data => {
        let variables = {} as Record<string, FlowVariableResponse>
        let selectOptions = [] as SelectOption[]

        for (let flowVariable of data.data) {
          variables[flowVariable.guid] = flowVariable
          selectOptions.push({
            value: flowVariable.guid,
            label: (
              <VarOptionContainer>
                <NTypography.Headline>{flowVariable.name}</NTypography.Headline>
                <NTypography.InputCaption>{flowVariable.type}</NTypography.InputCaption>
              </VarOptionContainer>
            ),
          })
        }

        return {
          variables,
          selectOptions,
        }
      },
    },
  )

  const onSubmit = ({ object_name, conditionClauses, conditionLogic, customConditionLogic }: ObjectFormType) => {
    updateFlow({
      flowName,
      data: {
        object_name,
        conditions: {
          conditionLogic,
          conditionClauses: conditionLogic === 'none' ? [] : conditionClauses,
          customConditionLogic,
        },
      },
    })
  }

  return (
    <NModal size="large" setVisible={setShowModal} visible={showModal}>
      <NModal.Header onClose={() => setShowModal(false)} title="Choose Object" />
      <NModal.Body>
        <ScrollWrapper>
          <form>
            <Wrapper>
              <TextHeader>
                Choose the object whose records trigger the flow to run when they’re created or updated.
              </TextHeader>
              <NDivider size="md" />
              <NRow>
                <NColumn flex={1}>
                  <Controller
                    defaultValue={flowData?.object_name || ''}
                    control={control}
                    name="object_name"
                    render={({ field }) => (
                      <NSingleSelect
                        isLoading={isLoadingObjects}
                        isSearchable
                        searchValue={searchValue}
                        onSearchValueChange={handleSearchChange}
                        options={allObjects || []}
                        placeholder="Search all objects"
                        label="Objects"
                        value={field.value}
                        onValueChange={value => {
                          field.onChange(value)
                        }}
                      />
                    )}
                  />
                </NColumn>
              </NRow>
            </Wrapper>

            {watchObjectName && (
              <Wrapper>
                <NDivider size={1} lineSize={1} lineColor={color('Neutral300')({ theme })} />
                <NDivider size="md" />
                <NRow>
                  <NColumn flex={1}>
                    <Controller
                      name="conditionLogic"
                      control={control}
                      render={({ field }) => (
                        <NSingleSelect
                          options={CONDITIONS}
                          placeholder="Select Condition"
                          value={field.value}
                          label="Condition Requirements to Execute Outcome"
                          onValueChange={field.onChange}
                          className="conditionSelect"
                        />
                      )}
                    />
                  </NColumn>
                </NRow>
                {watchConditionLogic !== 'none' && (
                  <>
                    <NDivider size="md" />
                    {watchConditionLogic === 'custom' && (
                      <NTextInput
                        placeholder="Enter custom logic"
                        label="Condition logic"
                        {...register('customConditionLogic')}
                      />
                    )}
                    <NDivider size="md" />
                    {fields.map((field, fieldIndex) => (
                      <Fragment key={field.id}>
                        <NRow align="center">
                          {watchConditionLogic === 'custom' && (
                            <>
                              <IndexTxt>{fieldIndex}</IndexTxt>
                              <NDivider size="md" vertical />
                            </>
                          )}
                          <Controller
                            name={`conditionClauses.${fieldIndex}.field`}
                            defaultValue={field.field || ''}
                            control={control}
                            render={({ field }) => {
                              return (
                                <NSingleSelect
                                  fullWidth
                                  isLoading={isLoadingFields}
                                  options={
                                    fieldData?.data.fields.map(field => ({
                                      value: field.name,
                                      label: field.displayName,
                                    })) || []
                                  }
                                  placeholder="Select field"
                                  value={field.value}
                                  onValueChange={field.onChange}
                                />
                              )
                            }}
                          />
                          <NDivider size="xs" vertical />
                          <Controller
                            name={`conditionClauses.${fieldIndex}.sign`}
                            defaultValue={field.sign || '==='}
                            control={control}
                            render={({ field }) => {
                              return (
                                <NSingleSelect
                                  options={SIGNS.map(sign => ({ value: sign }))}
                                  placeholder="Select operator"
                                  value={field.value}
                                  onValueChange={field.onChange}
                                />
                              )
                            }}
                          />
                          <NDivider size="xs" vertical />
                          <Controller
                            name={`conditionClauses.${fieldIndex}.value.value`}
                            control={control}
                            render={({ field }) => (
                              <NSingleSelect
                                fullWidth
                                options={selectOptions}
                                placeholder="Select value"
                                value={field.value}
                                onValueChange={field.onChange}
                              />
                            )}
                          />
                          <DeleteBtn>
                            <GlobalIcons.Trash onClick={() => remove(fieldIndex)} />
                          </DeleteBtn>
                        </NRow>
                        {fieldIndex !== fields.length - 1 && (
                          <>
                            {watchConditionLogic === 'or' ? (
                              <LogicOperator>OR</LogicOperator>
                            ) : watchConditionLogic === 'and' ? (
                              <LogicOperator>AND</LogicOperator>
                            ) : null}
                          </>
                        )}
                      </Fragment>
                    ))}
                    <AddBtnWrapper
                      onClick={() =>
                        append({
                          field: '',
                          sign: '===',
                          value: {
                            type: 'variable',
                            value: '',
                          },
                        })
                      }
                    />
                  </>
                )}
              </Wrapper>
            )}
          </form>
        </ScrollWrapper>
      </NModal.Body>
      <NModal.Footer isLoading={isUpdating} onCancel={() => setShowModal(false)} onFinish={handleSubmit(onSubmit)} />
    </NModal>
  )
}
