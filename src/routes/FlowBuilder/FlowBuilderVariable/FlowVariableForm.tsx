import React, { FC } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useMutation, useQueryClient } from 'react-query'
import { useParams } from 'react-router'
import styled, { useTheme } from 'styled-components'
import { NDateTimePicker } from '../../../components/DateTime/NDateTimePicker'
import { color, spacing } from '../../../components/GlobalStyle'
import { NCheckbox } from '../../../components/NCheckbox/NCheckbox'
import { NDivider } from '../../../components/NDivider'
import { NColumn, NRow } from '../../../components/NGrid/NGrid'
import { NModal } from '../../../components/NModal/NModal'
import { SelectOption } from '../../../components/NSelect/model'
import { NSingleSelect } from '../../../components/NSelect/NSingleSelect'
import { NTextInput } from '../../../components/NTextInput/NTextInput'
import { NToast } from '../../../components/NToast'
import { useValidateString } from '../../../hooks/useValidateString'
import { APIServices } from '../../../services/api'
import { FlowsQueryKey } from '../../../services/api/endpoints/flows'
import { FlowVariableResponse } from '../../../services/api/models'
import { fieldNameRegex } from '../../../utils/regex'
import { getDateFromIsoString, getNameFromDisplayName } from '../../../utils/utils'
import { FlowVariableCollectionInput } from './FlowVariableExtraInput/FlowVariableCollectionInput'
import { FlowVariableRecordInput } from './FlowVariableExtraInput/FlowVariableRecordInput'

const TYPE_OPTIONS: SelectOption[] = [
  { value: 'string', label: 'String' },
  { value: 'boolean', label: 'Boolean' },
  { value: 'integer', label: 'Integer' },
  { value: 'float', label: 'Float' },
  { value: 'date', label: 'Date' },
  { value: 'record', label: 'Record' },
  { value: 'collection', label: 'Collection' },
]

const ObjectForm = styled.form`
  padding: ${spacing('xl')};
`

const ValueContainer = styled.div<{ isConstant?: boolean }>`
  display: flex;
  flex-direction: column;
  .constant {
    display: ${props => (props.isConstant ? 'flex' : 'none')};
  }
  .default {
    display: ${props => (props.isConstant ? 'none' : 'flex')};
  }
`

type Props = {
  defaultValue?: FlowVariableResponse
  onGoBack: () => void
}

export const FlowVariableForm: FC<Props> = ({ defaultValue, onGoBack }) => {
  const { flowName } = useParams<{ flowName: string }>()
  // post object mutation
  const theme = useTheme()
  const queryClient = useQueryClient()
  const { validateFunction } = useValidateString()

  // create mode
  const { mutate: createFlowVariables, isLoading: isCreating } = useMutation(APIServices.Flows.createFlowVariables, {
    onSuccess: () => {
      queryClient.invalidateQueries([FlowsQueryKey.getFlow, { flowName }])
      queryClient.invalidateQueries(FlowsQueryKey.getFlowVariables)

      onGoBack()
    },
    onError: err => {
      NToast.error({ title: 'Create variable error!', subtitle: `${err.response?.data.message}` })
    },
  })

  // update mode
  const { mutate: updateFlowVariables, isLoading: isUpdating } = useMutation(APIServices.Flows.updateFlowVariables, {
    onSuccess: () => {
      queryClient.invalidateQueries([FlowsQueryKey.getFlow, { flowName }])
      queryClient.invalidateQueries(FlowsQueryKey.getFlowVariables)

      onGoBack()
    },
    onError: err => {
      NToast.error({ title: 'Update variable error!', subtitle: `${err.response?.data.message}` })
    },
  })

  const formMethods = useForm<FlowVariableResponse>({
    defaultValues: defaultValue || {
      name: '',
      type: 'string',
      is_for_input: false,
      is_for_output: false,
    },
  })
  const { handleSubmit, register, control, formState, watch, setValue, getValues } = formMethods

  const watchType = watch('type')
  const watchIsConstant = watch('is_constant')

  const onSubmit = (data: FlowVariableResponse) => {
    if (defaultValue?.guid) {
      updateFlowVariables({
        flowName,
        varName: defaultValue.name,
        body: {
          is_for_input: data.is_for_input,
          is_for_output: data.is_for_output,
        },
      })
      return
    }

    // TODO: fix for different type TS type
    const { type, ...restData } = data
    if (type === 'object') {
      return
    }

    createFlowVariables({
      flowName,
      body: {
        type,
        ...restData,
      },
    })
  }

  const isEdit = !!defaultValue?.guid

  return (
    <>
      <ObjectForm>
        <NRow>
          <NColumn>
            <NTextInput
              {...register('display_name', {
                validate: validateFunction,
              })}
              label="Display name"
              error={formState.errors.display_name?.message}
              required
              onBlur={e => {
                if (!!!defaultValue) {
                  const formatName = getNameFromDisplayName(e.target.value)
                  !getValues('name') && setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
                }
              }}
            />
          </NColumn>
          <NDivider vertical size="md" />
          <NColumn>
            <NTextInput
              {...register('name', {
                validate: validateFunction,
                pattern: {
                  value: fieldNameRegex,
                  message: 'Invalid pattern',
                },
              })}
              disabled={isEdit}
              required
              label="Name"
              error={formState.errors.name?.message}
            />
          </NColumn>
        </NRow>
        <NDivider size="md" />
        <Controller
          name="is_constant"
          control={control}
          render={({ field }) => {
            return (
              <NCheckbox
                disabled={isEdit}
                title="Is constant"
                checked={field.value}
                onChange={e => {
                  field.onChange(e.target.checked)
                  setValue('const_value', undefined)
                  setValue('default_value', undefined)
                }}
              />
            )
          }}
        />
        <NDivider size="md" />
        <NRow>
          <NColumn>
            <Controller
              name="type"
              control={control}
              rules={{ required: { value: true, message: 'Required' } }}
              render={({ field, fieldState }) => {
                return (
                  <NSingleSelect
                    required
                    disabled={isEdit}
                    label="Type"
                    error={fieldState.error?.message}
                    options={TYPE_OPTIONS}
                    value={field.value}
                    onValueChange={newType => {
                      field.onChange(newType)
                      setValue('collection_data_type', undefined)
                      setValue('record_object_name', undefined)
                      if (newType === 'boolean') {
                        setValue('const_value', '')
                        setValue('default_value', '')
                        return
                      }
                      setValue('const_value', undefined)
                      setValue('default_value', undefined)
                    }}
                  />
                )
              }}
            />
          </NColumn>
          <NDivider size="md" vertical />
          <NColumn>
            {watchType === 'string' && (
              <>
                <ValueContainer isConstant={watchIsConstant}>
                  <NTextInput
                    required
                    {...register('const_value', {
                      validate: data => {
                        if (!watchIsConstant) {
                          return true
                        }
                        return validateFunction(data)
                      },
                    })}
                    className="constant"
                    disabled={isEdit}
                    error={formState.errors.const_value?.message}
                    label="Constant value"
                  />
                  <NTextInput
                    {...register('default_value')}
                    className="default"
                    disabled={isEdit}
                    label="Default value"
                  />
                </ValueContainer>
              </>
            )}
            {watchType === 'boolean' && (
              <>
                <NDivider size="md" />
                <ValueContainer isConstant={watchIsConstant}>
                  <Controller
                    name="const_value"
                    control={control}
                    render={({ field }) => {
                      return (
                        <NCheckbox
                          disabled={isEdit}
                          className="constant"
                          title="Constant value"
                          checked={Boolean(field.value)}
                          onChange={e => {
                            field.onChange(e.target.checked)
                          }}
                        />
                      )
                    }}
                  />
                  <Controller
                    name="default_value"
                    control={control}
                    render={({ field }) => {
                      return (
                        <NCheckbox
                          disabled={isEdit}
                          className="default"
                          title="Default value"
                          checked={Boolean(field.value)}
                          onChange={e => {
                            field.onChange(e.target.checked)
                          }}
                        />
                      )
                    }}
                  />
                </ValueContainer>
              </>
            )}
            {(watchType === 'integer' || watchType === 'float') && (
              <>
                <ValueContainer isConstant={watchIsConstant}>
                  <NTextInput
                    required
                    {...register('const_value', {
                      validate: data => {
                        if (!watchIsConstant) {
                          return true
                        }
                        return validateFunction(data)
                      },
                    })}
                    className="constant"
                    disabled={isEdit}
                    type="number"
                    label="Constant value"
                  />
                  <NTextInput
                    {...register('default_value')}
                    className="default"
                    disabled={isEdit}
                    type="number"
                    label="Default value"
                  />
                </ValueContainer>
              </>
            )}
            {watchType === 'date' && (
              <>
                <ValueContainer isConstant={watchIsConstant}>
                  <Controller
                    name="const_value"
                    control={control}
                    rules={{ required: { value: !!watchIsConstant, message: 'Required' } }}
                    render={({ field, fieldState }) => {
                      // translate ios string to date
                      let valueDate = field.value ? getDateFromIsoString(field.value, 'datetime') : undefined
                      // translate date to ios string
                      const handleOnChange = (newDate: Date) => {
                        const iosString = newDate.toISOString()

                        field.onChange(iosString)
                      }
                      return (
                        <NDateTimePicker
                          required
                          disabled={isEdit}
                          error={fieldState.error?.message}
                          className="constant"
                          label="Constant value"
                          value={valueDate}
                          onValueChange={handleOnChange}
                        />
                      )
                    }}
                  />
                  <Controller
                    name="default_value"
                    control={control}
                    render={({ field }) => {
                      // translate ios string to date
                      let valueDate = field.value ? getDateFromIsoString(field.value, 'datetime') : undefined
                      // translate date to ios string
                      const handleOnChange = (newDate: Date) => {
                        const iosString = newDate.toISOString()

                        field.onChange(iosString)
                      }
                      return (
                        <NDateTimePicker
                          disabled={isEdit}
                          className="default"
                          label="Default value"
                          value={valueDate}
                          onValueChange={handleOnChange}
                        />
                      )
                    }}
                  />
                </ValueContainer>
              </>
            )}
            {watchType === 'record' && <FlowVariableRecordInput formMethods={formMethods} />}
            {watchType === 'collection' && <FlowVariableCollectionInput formMethods={formMethods} />}
          </NColumn>
        </NRow>

        <NDivider size={32} lineSize={1} lineColor={color('Neutral200')({ theme })} />
        <Controller
          name="is_for_input"
          control={control}
          render={({ field }) => {
            return (
              <NCheckbox
                title="Available for input"
                checked={Boolean(field.value)}
                onChange={e => {
                  field.onChange(e.target.checked)
                }}
              />
            )
          }}
        />

        <NDivider size="md" />
        <Controller
          name="is_for_output"
          control={control}
          render={({ field }) => {
            return (
              <NCheckbox
                title="Available for output"
                checked={field.value}
                onChange={e => {
                  field.onChange(e.target.checked)
                }}
              />
            )
          }}
        />
      </ObjectForm>
      <NModal.Footer isLoading={isCreating || isUpdating} onCancel={onGoBack} onFinish={handleSubmit(onSubmit)} />
    </>
  )
}
