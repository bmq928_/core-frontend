import { format, isValid, parse } from 'date-fns'
import React, { FC, useState } from 'react'
import styled from 'styled-components'
import { color, spacing } from '../../../components/GlobalStyle'
import { NModal } from '../../../components/NModal/NModal'
import { NTypography } from '../../../components/NTypography'
import { ContentValue, FlowVariableResponse } from '../../../services/api/models'
import { getDateFromIsoString, getValueFromDate, noop } from '../../../utils/utils'
import { renderVariableValue } from '../utils/functions'
import { FlowVariableForm } from './FlowVariableForm'
import { FlowVariableListing } from './FlowVariableListing'
import { FlowVariableSchema } from './FlowVariableSchema'
import { FlowBuilderVariableState } from './types'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`

const InputWrapper = styled.div`
  display: flex;
  align-items: center;
  height: 40px;
  padding: 0 ${spacing('xs')};

  cursor: pointer;
  transition: border-color 0.3s ease-in-out;
  border-radius: 4px;
  border: 1px solid ${color('Neutral200')};
  &:hover {
    border-color: ${color('Neutral400')};
  }
  &:focus-within {
    border-color: ${color('Primary700')};
  }

  &.is-disabled {
    cursor: not-allowed;
    background: ${color('Neutral200')};
    &:hover {
      border-color: ${color('Neutral200')};
    }
  }
`

const Input = styled('input')`
  border: none;
  font-size: 14px;
  line-height: 18px;
  padding: 0px 8px;
  flex: 1;
  width: 100%;
  color: ${color('Neutral900')};

  &:focus {
    outline: 0;
  }

  &:disabled {
    cursor: not-allowed;
  }

  ::placeholder {
    font-size: inherit;
    color: ${color('Neutral500')};
  }
`

const Caption = styled(NTypography.InputCaption)<{ isError?: boolean }>`
  margin-top: ${spacing('xxs')};
  color: ${props => (props.isError ? color('Red700') : color('Neutral400'))};
`

type Props = {
  className?: string
  label?: string
  placeholder?: string
  caption?: string
  disabled?: boolean
  error?: string
  noEnterValue?: boolean
  inputType?: 'string' | 'boolean' | 'date-time' | 'date' | 'time' | string
  selectedVariable?: ContentValue
  onVariableSelected: (args?: ContentValue, flowVariable?: { name: string; type: string }) => void
} & React.HTMLAttributes<HTMLDivElement>

export const FlowVariablePicker: FC<Props> = ({
  label,
  placeholder,
  caption,
  disabled,
  inputType = 'string',
  selectedVariable,
  onVariableSelected,
  children,
  error,
  noEnterValue,
  ...restProps
}) => {
  const [showPicker, setShowPicker] = useState(false)
  const [modalState, setModalState] = useState(FlowBuilderVariableState.Listing)
  const [selectedVar, setSelectedVar] = useState<FlowVariableResponse | undefined>()

  const [searchError, setSearchError] = useState<string>()
  const [localError, setError] = useState<string>()

  const changeStateToListing = () => {
    setSelectedVar(undefined)
    setModalState(FlowBuilderVariableState.Listing)
  }
  const closePicker = () => {
    setModalState(FlowBuilderVariableState.Listing)
    setSearchError(undefined)
    setShowPicker(false)
  }

  const cannotEnterConstantValue = noEnterValue || inputType === 'boolean'
  const isDatetimePicker = inputType === 'date' || inputType === 'time' || inputType === 'date-time'

  const datetimeFormatPlaceholder =
    inputType === 'date' ? dateFormat : inputType === 'time' ? timeFormat : `${dateFormat} ${timeFormat}`

  const searchDefaultValue =
    isDatetimePicker && selectedVariable?.type === 'value' && selectedVariable?.value
      ? format(getDateFromIsoString(selectedVariable.value, inputType), datetimeFormatPlaceholder)
      : selectedVariable?.value

  const renderValue =
    isDatetimePicker && selectedVariable?.type === 'value' && selectedVariable?.value
      ? searchDefaultValue
      : renderVariableValue(selectedVariable)

  ////////////////////
  const onEnterKeyDown = (newValue: string) => {
    if (cannotEnterConstantValue) {
      return
    }
    if (newValue.trim() === '') {
      return
    }

    // date type value
    if (inputType === 'date') {
      const date = checkDateInputFormat(newValue)
      if (date) {
        const dateStr = getValueFromDate(date, inputType)
        onVariableSelected({ value: dateStr, type: 'value' })
        setSearchError(undefined)
        closePicker()
        return
      }
      setSearchError(`Date format should be ${dateFormat}`)
      return
    }

    // time type value
    if (inputType === 'time') {
      const time = checkTimeInputFormat(newValue)
      if (time) {
        const timeStr = getValueFromDate(time, inputType)
        onVariableSelected({ value: timeStr, type: 'value' })
        setSearchError(undefined)
        closePicker()
        return
      }
      setSearchError(`Time format should be ${timeFormat}`)
      return
    }

    // date-time type value
    if (inputType === 'date-time') {
      const datetime = checkDateTimeInputFormat(newValue)
      if (datetime) {
        const datetimeStr = getValueFromDate(datetime, inputType)
        onVariableSelected({ value: datetimeStr, type: 'value' })
        setSearchError(undefined)
        closePicker()
        return
      }
      setSearchError(`Date Time format should be ${dateFormat} ${timeFormat}`)
      return
    }

    // Script type value
    const matchScript = newValue.match(/\{\\\$([A-Z,a-z,0-9,\.,\$,_]+)\}/g)
    if (matchScript && matchScript.length > 0) {
      onVariableSelected({ value: newValue, type: 'script' })
      closePicker()
      return
    }

    // string type value
    onVariableSelected({ value: newValue, type: 'value' })
    closePicker()
  }

  const onRowSelect = (data: FlowVariableResponse) => {
    let errorMsg = undefined
    //
    const isVarWithSchema = data.type === 'record' || data.type === 'object'
    if (isVarWithSchema) {
      setModalState(FlowBuilderVariableState.Schema)
      setSelectedVar(data)
      return
    }
    //
    if (isDatetimePicker && data.type !== 'date') {
      errorMsg = 'variable type is not date'
    }
    setError(errorMsg)
    onVariableSelected({ value: data.name, type: 'variable' }, data)
    closePicker()
  }

  const onClearPress = () => {
    setError(undefined)
    onVariableSelected()
    closePicker()
  }

  const onSelectFlowField = (contentValue: ContentValue, flowVar: { name: string; type: string }) => {
    let errorMsg = undefined
    if (isDatetimePicker && flowVar.type !== 'date') {
      errorMsg = 'variable type is not date'
    }
    setError(errorMsg)
    onVariableSelected(contentValue, flowVar)
    closePicker()
  }
  ////////////////////

  const searchPlaceholder = isDatetimePicker
    ? `Enter ${datetimeFormatPlaceholder} or search resource...`
    : cannotEnterConstantValue
    ? `Search resource...`
    : 'Enter value or search resource...'

  return (
    <>
      <Wrapper {...restProps}>
        <div
          onClick={() => {
            if (disabled) {
              return
            }
            setShowPicker(true)
          }}>
          {children || (
            <InputWrapper className={disabled ? 'is-disabled' : undefined}>
              <Input
                disabled={disabled}
                value={renderValue}
                onChange={noop}
                placeholder={placeholder || 'Search variable'}
              />
            </InputWrapper>
          )}
        </div>
        {(localError || error || caption) && (
          <Caption isError={!!localError || !!error}>{localError || error || caption}</Caption>
        )}
      </Wrapper>
      {showPicker && (
        <NModal
          size="large"
          visible={showPicker}
          setVisible={(newVisible: boolean) => {
            if (newVisible) {
              setShowPicker(true)
              return
            }
            closePicker()
          }}>
          <NModal.Header
            title={label}
            onBack={modalState === FlowBuilderVariableState.Listing ? undefined : changeStateToListing}
            onClose={closePicker}
          />
          {modalState === FlowBuilderVariableState.Listing && (
            <FlowVariableListing
              selectMode
              searchPlaceholder={searchPlaceholder}
              searchError={searchError}
              searchDefaultValue={searchDefaultValue}
              onNewPressed={() => {
                setModalState(FlowBuilderVariableState.Form)
              }}
              onRowSelect={onRowSelect}
              onClearSelect={onClearPress}
              onSearchKeyDown={event => {
                const newValue = (event.target as HTMLInputElement).value

                if (event.key === 'Enter') {
                  onEnterKeyDown(newValue)
                }
              }}
            />
          )}
          {modalState === FlowBuilderVariableState.Form && <FlowVariableForm onGoBack={changeStateToListing} />}
          {modalState === FlowBuilderVariableState.Schema && (
            <FlowVariableSchema selectedVariable={selectedVar} onSelectField={onSelectFlowField} />
          )}
        </NModal>
      )}
    </>
  )
}

const dateFormat = 'dd/MM/yyyy'
const timeFormat = 'HH:mm:ss'

const checkDateInputFormat = (inputValue: string) => {
  const newDate = parse(inputValue, dateFormat, 0)

  return isValid(newDate) ? newDate : undefined
}

const checkTimeInputFormat = (inputValue: string) => {
  const newDate = parse(inputValue, timeFormat, new Date())

  return isValid(newDate) ? newDate : undefined
}

const checkDateTimeInputFormat = (inputValue: string) => {
  const newDate = parse(inputValue, `${dateFormat} ${timeFormat}`, new Date())

  return isValid(newDate) ? newDate : undefined
}
