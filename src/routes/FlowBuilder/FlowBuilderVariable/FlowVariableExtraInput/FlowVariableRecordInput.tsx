import React, { FC } from 'react'
import { Controller, UseFormReturn } from 'react-hook-form'
import { useQuery } from 'react-query'
import { NDivider } from '../../../../components/NDivider'
import { SelectOption } from '../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { useSearchText } from '../../../../hooks/useSearchText'
import { APIServices } from '../../../../services/api'
import { BuilderQueryKey } from '../../../../services/api/endpoints/builder'
import { FlowVariableResponse } from '../../../../services/api/models'

type Props = {
  formMethods: UseFormReturn<FlowVariableResponse>
}
export const FlowVariableRecordInput: FC<Props> = ({ formMethods }) => {
  const { control } = formMethods

  const [{ searchValue, searchText }, handleSearchChange] = useSearchText()

  // get all objects
  const { data = [], isLoading } = useQuery(
    [BuilderQueryKey.getAllObjects, { searchText }],
    APIServices.Builder.getAllObjects,
    {
      keepPreviousData: true,
      select: data => {
        return data.data.data.map(option => {
          const typeName = option.isExternal ? ' (external)' : ' (internal)'
          return {
            value: option.name,
            label: option.displayName + typeName,
          }
        }) as SelectOption[]
      },
    },
  )

  return (
    <>
      <NDivider size="md" />
      <Controller
        name="record_object_name"
        control={control}
        rules={{ required: { value: true, message: 'Required' } }}
        render={({ field, fieldState }) => {
          return (
            <NSingleSelect
              fullWidth
              required
              label="Collection Record Type"
              error={fieldState.error?.message}
              options={data}
              value={field.value}
              onValueChange={newType => {
                field.onChange(newType)
              }}
              isLoading={isLoading}
              isSearchable
              searchValue={searchValue}
              onSearchValueChange={handleSearchChange}
            />
          )
        }}
      />
    </>
  )
}
