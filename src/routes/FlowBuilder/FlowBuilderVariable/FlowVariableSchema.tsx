import React, { FC } from 'react'
import { useQuery } from 'react-query'
import { useParams } from 'react-router'
import styled, { useTheme } from 'styled-components'
import { color, spacing } from '../../../components/GlobalStyle'
import { NButton } from '../../../components/NButton/NButton'
import { NDivider } from '../../../components/NDivider'
import { NSpinner } from '../../../components/NSpinner/NSpinner'
import { typography } from '../../../components/NTypography'
import { APIServices } from '../../../services/api'
import { FlowsQueryKey } from '../../../services/api/endpoints/flows'
import { ContentValue, FlowVariableResponse, FlowVariableSchemaResponse } from '../../../services/api/models'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: ${spacing('xl')};

  .flow-var-btn {
    justify-content: flex-start;
    height: 50px;
    padding: 0 ${spacing('md')};
  }
`

const Title = styled.div`
  margin-bottom: ${spacing('xs')};
  ${typography('h500')};
`

const FlowVarWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`

const FlowVarName = styled.div`
  ${typography('h400')};
  color: ${color('Neutral900')};
`

const FlowVarType = styled.div`
  ${typography('caption')};
`

const Dot = styled.div`
  margin-right: ${spacing('xs')};
  width: ${spacing('xs')};
  height: ${spacing('xs')};
  border-radius: ${spacing('xxs')};
  background-color: ${color('Neutral900')};
`

type Props = {
  selectedVariable?: FlowVariableResponse
  onSelectField?: (selectedVar: ContentValue, flowVar: { name: string; type: string }) => void
}

export const FlowVariableSchema: FC<Props> = ({ selectedVariable, onSelectField }) => {
  const { flowName } = useParams<{ flowName: string }>()
  const theme = useTheme()

  const { data, isLoading } = useQuery(
    [FlowsQueryKey.getFlowVariableSchema, { variable: selectedVariable?.name!, flowName }],
    APIServices.Flows.getFlowVariableSchema,
    {
      enabled: !!selectedVariable,
    },
  )

  if (!selectedVariable) {
    return <Wrapper>No selected flow variable</Wrapper>
  }

  const handleSelectField = (flowField: FlowVariableSchemaResponse) => {
    const value = `${selectedVariable.name}.${flowField.name}`
    onSelectField && onSelectField({ value, type: 'variable' }, { name: value, type: flowField.type })
  }

  return (
    <Wrapper>
      <Title>Flow variable</Title>
      <NButton
        className="flow-var-btn"
        type="ghost"
        icon={<Dot />}
        onClick={() => {
          const value = selectedVariable.name
          onSelectField && onSelectField({ value, type: 'variable' }, { name: value, type: selectedVariable.type })
        }}>
        <FlowVarWrapper>
          <FlowVarName>{selectedVariable.name}</FlowVarName>
          <FlowVarType>{selectedVariable.type}</FlowVarType>
        </FlowVarWrapper>
      </NButton>
      <NDivider size="lg" lineSize={1} lineColor={color('Neutral200')({ theme })} />
      <Title>Flow variable schema</Title>
      {isLoading && <NSpinner size={20} strokeWidth={2} />}
      {!isLoading &&
        data?.data.map(field => {
          return (
            <NButton
              key={`schema_field_${field.name}`}
              className="flow-var-btn"
              type="ghost"
              icon={<Dot />}
              onClick={() => {
                handleSelectField(field)
              }}>
              <FlowVarWrapper>
                <FlowVarName>{field.displayName || field.name}</FlowVarName>
                <FlowVarType>{field.type}</FlowVarType>
              </FlowVarWrapper>
            </NButton>
          )
        })}
    </Wrapper>
  )
}
