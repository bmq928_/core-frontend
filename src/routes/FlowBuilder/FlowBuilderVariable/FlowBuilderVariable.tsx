import React, { FC, useState } from 'react'
import { NModal } from '../../../components/NModal/NModal'
import { FlowVariableResponse } from '../../../services/api/models'
import { FlowVariableForm } from './FlowVariableForm'
import { FlowVariableListing } from './FlowVariableListing'
import { FlowVariableSchema } from './FlowVariableSchema'
import { FlowBuilderVariableState } from './types'

type Props = {
  visible: boolean
  setVisible: (visible: boolean) => void
}

export const FlowBuilderVariable: FC<Props> = ({ visible, setVisible }) => {
  const [modalState, setModalState] = useState(FlowBuilderVariableState.Listing)
  const [selectedVar, setSelectedVar] = useState<FlowVariableResponse | undefined>()

  const changeStateToListing = () => {
    setSelectedVar(undefined)
    setModalState(FlowBuilderVariableState.Listing)
  }

  const showEditForm = (data: FlowVariableResponse) => {
    setSelectedVar(data)
    setModalState(FlowBuilderVariableState.Form)
  }

  return (
    <NModal visible={visible} setVisible={setVisible} size="x-large">
      <NModal.Header
        onBack={modalState !== FlowBuilderVariableState.Listing ? changeStateToListing : undefined}
        onClose={() => {
          setVisible(false)
        }}>
        Variable Manager
      </NModal.Header>
      {modalState === FlowBuilderVariableState.Listing && (
        <FlowVariableListing
          onNewPressed={() => {
            setModalState(FlowBuilderVariableState.Form)
          }}
          onEditClick={showEditForm}
          onRowSelect={flowVar => {
            const isVarWithSchema = flowVar.type === 'record' || flowVar.type === 'object'
            if (isVarWithSchema) {
              setModalState(FlowBuilderVariableState.Schema)
              setSelectedVar(flowVar)
            }
          }}
        />
      )}
      {modalState === FlowBuilderVariableState.Form && (
        <FlowVariableForm defaultValue={selectedVar} onGoBack={changeStateToListing} />
      )}
      {modalState === FlowBuilderVariableState.Schema && <FlowVariableSchema selectedVariable={selectedVar} />}
    </NModal>
  )
}
