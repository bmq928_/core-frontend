import { FC, useState } from 'react'
import { useMutation, useQueryClient } from 'react-query'
import { useParams } from 'react-router'
import { NTableActions } from '../../../components/NTable/NTableActions'
import { NToast } from '../../../components/NToast'
import { APIServices } from '../../../services/api'
import { FlowsQueryKey } from '../../../services/api/endpoints/flows'
import { FlowVariableResponse } from '../../../services/api/models'

type Props = {
  value: string
  data: FlowVariableResponse
  onEditClick?: (data: FlowVariableResponse) => void
}

export const FlowVariableListingActions: FC<Props> = ({ value, data, onEditClick }) => {
  const { flowName } = useParams<{ flowName: string }>()
  const [showDropList, setShowDropList] = useState(false)
  const queryClient = useQueryClient()

  const { mutate: deleteFlowVars, isLoading } = useMutation(APIServices.Flows.deleteFlowVariables)

  const handleEdit = () => {
    onEditClick && onEditClick(data)
    setShowDropList(false)
  }

  const handleDelete = () => {
    deleteFlowVars(
      { flowName, varName: data.name },
      {
        onSuccess: () => {
          queryClient.invalidateQueries(FlowsQueryKey.getFlowVariables)
          setShowDropList(false)
        },
        onError: err => {
          NToast.error({ title: 'Delete object error', subtitle: `Name: ${value}\n${err.response?.data.message}` })
        },
      },
    )
  }

  return (
    <NTableActions
      showDropList={showDropList}
      setShowDropList={setShowDropList}
      options={[
        { title: 'Edit', onClick: handleEdit },
        { title: 'Delete', isLoading, onClick: handleDelete },
      ]}
    />
  )
}
