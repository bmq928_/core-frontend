import React, { FC } from 'react'
import styled from 'styled-components'
import { spacing } from '../../../components/GlobalStyle'
import { GlobalIcons } from '../../../components/Icons'
import { NButton } from '../../../components/NButton/NButton'
import { ContentValue } from '../../../services/api/models'
import { FlowVariablePicker } from './FlowVariablePicker'

const StyledRow = styled.div`
  display: flex;
  margin-bottom: ${spacing('sm')};
`

type Props = {
  flowVariables: ContentValue[]
  setFlowVariables: (newVars: ContentValue[]) => void
}

export const MultiFlowVariablesPicker: FC<Props> = ({ flowVariables, setFlowVariables }) => {
  return (
    <>
      {flowVariables.map((contentVal, index) => {
        return (
          <StyledRow key={`flow_variable_picker_${index}`}>
            <FlowVariablePicker
              label={`Variable ${index}`}
              selectedVariable={contentVal}
              onVariableSelected={varOption => {
                if (!varOption) {
                  const newFlowVariables = [...flowVariables.slice(0, index), ...flowVariables.slice(index + 1)]
                  setFlowVariables(newFlowVariables)
                  return
                }
                const newFlowVariables = [
                  ...flowVariables.slice(0, index),
                  varOption,
                  ...flowVariables.slice(index + 1),
                ]
                setFlowVariables(newFlowVariables)
              }}
            />
            <NButton
              type="ghost"
              icon={<GlobalIcons.Trash />}
              onClick={() => {
                const newFlowVariables = [...flowVariables.slice(0, index), ...flowVariables.slice(index + 1)]
                setFlowVariables(newFlowVariables)
              }}
            />
          </StyledRow>
        )
      })}
      <FlowVariablePicker
        label="New option"
        onVariableSelected={newVar => {
          if (!newVar) {
            return
          }
          const newFlowVariables = [...flowVariables, newVar]
          setFlowVariables(newFlowVariables)
        }}>
        <NButton type="primary" size="small" style={{ width: '56px', height: '24px' }} icon={<GlobalIcons.Plus />} />
      </FlowVariablePicker>
    </>
  )
}
