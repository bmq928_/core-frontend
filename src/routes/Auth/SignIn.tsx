import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useMutation } from 'react-query'
import { useDispatch } from 'react-redux'
import { Redirect, useLocation } from 'react-router-dom'
import { down } from 'styled-breakpoints'
import styled from 'styled-components'
import { ReactComponent as NuclentLogo } from '../../assets/nuclent.svg'
import { color, spacing } from '../../components/GlobalStyle'
import { NButton } from '../../components/NButton/NButton'
import { NTextInput } from '../../components/NTextInput/NTextInput'
import { NToast } from '../../components/NToast'
import { authActions, useAuth } from '../../redux/slices/authSlice'
import { APIServices } from '../../services/api'

type SignInProps = {}

const Wrapper = styled('div')`
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-direction: column;
`
const Content = styled.div`
  flex: 1;
  flex-direction: row;
  display: flex;
  justify-content: space-between;
`

const Footer = styled.div`
  background: ${color('Neutral200')};
  height: 64px;
  width: 100%;
  flex-shrink: 0;
`

const LoginContainer = styled.div`
  width: 320px;
  ${down('md')} {
    margin-left: ${spacing('xl')};
    margin-right: ${spacing('xl')};
  }
  margin-left: ${spacing(173)};
  margin-top: ${spacing(61)};
`

const LoginHeader = styled.p`
  font-weight: bold;
  font-size: 32px;
  line-height: 41px;
  color: ${color('Neutral900')};
  margin-top: ${spacing(76)};
  margin-bottom: ${spacing(18)};
`
const LoginDescription = styled.p`
  font-weight: normal;
  font-size: 16px;
  line-height: 20px;
  color: ${color('Neutral500')};
  margin-bottom: 32px;
`

const SideDecorator = styled.div`
  width: 346px;
  height: 100%;
  background: ${color('Primary700')};
`
const LoginForm = styled.form`
  display: flex;
  flex-direction: column;
  justify-content: center;
`

const StyledNTextInput = styled(NTextInput)`
  margin-bottom: ${spacing('xl')};
`

const ForgotPasswordBtn = styled(NButton)`
  margin-top: ${spacing('xl')};
`
interface ILoginInput {
  username: string
  password: string
}

export function SignIn({}: SignInProps) {
  const { state, search, hash } = useLocation()
  const { accessToken } = useAuth()
  const { handleSubmit, formState, register } = useForm({
    defaultValues: {
      username: '',
      password: '',
    },
  })
  const dispatch = useDispatch()
  const { t } = useTranslation()

  const { mutate: login, isLoading } = useMutation(APIServices.Auth.login, {
    onSuccess: res => {
      dispatch(authActions.addToken({ accessToken: res.data.accessToken }))
    },
    onError: error => {
      NToast.error({
        title: 'Login unsuccessful',
        subtitle: error.response?.data.message,
      })
    },
  })

  const onSubmit = (data: ILoginInput) => {
    login(data)
  }

  if (accessToken) {
    //Redirect to app and handle session there
    return <Redirect to={{ pathname: '/', state, search, hash }} />
  }

  return (
    <Wrapper>
      <Content style={{ flexDirection: 'row' }}>
        <LoginContainer>
          <NuclentLogo />
          <LoginHeader>{t('auth.loginHeader')}</LoginHeader>
          <LoginDescription>{t('auth.loginDesc')}</LoginDescription>
          <LoginForm onSubmit={handleSubmit(onSubmit)}>
            <StyledNTextInput
              placeholder={t('auth.enterUsername')}
              label={t('auth.username')}
              error={formState.errors.username?.message}
              {...register('username', {
                // pattern: {
                //   value: emailRegex,
                //   message: t('common.error.invalidEmail'),
                // },
                required: {
                  value: true,
                  message: t('common.error.required'),
                },
              })}
            />
            <StyledNTextInput
              type="password"
              placeholder={t('auth.enterPassword')}
              label={t('auth.password')}
              error={formState.errors.password?.message}
              {...register('password', {
                required: {
                  value: true,
                  message: t('common.error.required'),
                },
              })}
            />
            <NButton loading={isLoading} type="primary" htmlType="submit" block>
              {t('auth.loginBtn')}
            </NButton>
            <ForgotPasswordBtn type="link">{t('auth.forgotPassword')}</ForgotPasswordBtn>
          </LoginForm>
        </LoginContainer>
        <SideDecorator />
      </Content>
      <Footer />
    </Wrapper>
  )
}
