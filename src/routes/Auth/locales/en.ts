export default {
  loginHeader: 'Login to CORE',
  loginDesc: 'Enter your username to login.',
  username: 'Username',
  password: 'Password',
  loginBtn: 'Log in',
  enterUsername: 'Enter username',
  enterPassword: 'Enter password',
  forgotPassword: 'Forgot password?',
}
