export default {
  loginHeader: 'vi:Login to CORE',
  loginDesc: 'vi:Enter your username to login.',
  username: 'vi:Username',
  password: 'vi:Password',
  loginBtn: 'vi:Log in',
  enterUsername: 'vi:Enter username',
  enterPassword: 'vi:Enter password',
  forgotPassword: 'vi:Forgot password?',
}
