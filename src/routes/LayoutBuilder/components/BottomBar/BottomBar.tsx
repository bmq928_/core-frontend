import * as React from 'react'
import styled from 'styled-components'
import { classnames } from '../../../../common/classnames'
import { color, spacing } from '../../../../components/GlobalStyle'
import { elevation } from '../../../../components/NElevation'

const Wrapper = styled('div')`
  position: fixed;
  left: 0;
  right: 0;
  bottom: 0;
  transform: translateY(calc(100% + 4px));
  background: ${color('white')};
  padding: ${spacing('lg')};
  transition: transform 0.3s ease-in-out;
  ${elevation('Elevation200')};
  &.show {
    transform: translateY(0);
  }
`

type BottomBarProps = {
  visible: boolean
  children?: React.ReactNode
} & React.HTMLAttributes<HTMLDivElement>

export const BottomBar = React.forwardRef<HTMLDivElement, BottomBarProps>(function BottomBar(
  { visible, children, className },
  ref,
) {
  return (
    <Wrapper className={classnames([visible && 'show', className])} ref={ref}>
      {children}
    </Wrapper>
  )
})
