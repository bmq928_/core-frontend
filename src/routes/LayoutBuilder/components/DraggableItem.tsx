import * as React from 'react'
import { useDrag } from 'react-dnd'
import { classnames } from '../../../common/classnames'

type DraggableItemProps = {
  children: React.ReactNode
  id: string
  type: string
  disable?: boolean
  onDrag?: () => void
}

export function DraggableItem({ id, type, children, disable = false, onDrag }: DraggableItemProps) {
  const [{ isDragging }, ref] = useDrag({
    type,
    item: { id },
    canDrag() {
      return !disable
    },
    collect(monitor) {
      if (monitor.isDragging() && onDrag) {
        onDrag()
      }
      return {
        isDragging: monitor.isDragging(),
      }
    },
  })

  if (React.isValidElement(children)) {
    return React.cloneElement(children, {
      ref,
      className: classnames([disable && 'is-disable', isDragging && 'is-dragging']),
    })
  }
  return <>{children}</>
}
