import { ReactComponent as Flow } from '../assets/icons/Component/Flow.svg'
import { ReactComponent as ObjectList } from '../assets/icons/Component/ObjectList.svg'
import { ReactComponent as RecordDetails } from '../assets/icons/Component/RecordDetails.svg'
import { ReactComponent as RelatedList } from '../assets/icons/Component/RelatedList.svg'
import { ReactComponent as Button } from '../assets/icons/Field/Button.svg'
import { ReactComponent as Checkbox } from '../assets/icons/Field/Checkbox.svg'
import { ReactComponent as Dropdown } from '../assets/icons/Field/Dropdown.svg'
import { ReactComponent as Input } from '../assets/icons/Field/Input.svg'
import { ReactComponent as Link } from '../assets/icons/Field/Link.svg'
import { ReactComponent as Paragraph } from '../assets/icons/Field/Paragraph.svg'
import { ReactComponent as Radio } from '../assets/icons/Field/Radio.svg'
import { ReactComponent as Title } from '../assets/icons/Field/Title.svg'
import { ReactComponent as Accordion } from '../assets/icons/Layout/Accordion.svg'
import { ReactComponent as Divider } from '../assets/icons/Layout/Divider.svg'
import { ReactComponent as Section } from '../assets/icons/Layout/Section.svg'
import { ReactComponent as Tab } from '../assets/icons/Layout/Tab.svg'
import { ReactComponent as Preview } from '../assets/icons/Nav/Preview.svg'
import { ReactComponent as Redo } from '../assets/icons/Nav/Redo.svg'
import { ReactComponent as Undo } from '../assets/icons/Nav/Undo.svg'

export const FieldIcons = {
  Button,
  Checkbox,
  Dropdown,
  Input,
  Link,
  Paragraph,
  Radio,
  Title,
}
export const LayoutIcons = {
  Accordion,
  Divider,
  Section,
  Tab,
}
export const NavIcons = {
  Preview,
  Redo,
  Undo,
}

export const ComponentIcons = {
  ObjectList,
  RecordDetails,
  Flow,
  RelatedList,
}
