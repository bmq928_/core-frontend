import * as React from 'react'
import styled from 'styled-components'
import { color } from '../../../components/GlobalStyle'
import { typography } from '../../../components/NTypography'

const Wrapper = styled('div')`
  color: ${color('Neutral400')};
  ${typography('button')};
  padding: 24px;
  display: flex;
  justify-content: center;
  align-items: center;
`

type UnderConstructionProps = {
  children?: React.ReactNode
}

export function UnderConstruction({ children }: UnderConstructionProps) {
  return <Wrapper>Need more 🍕 and ☕️ to build {children} component</Wrapper>
}
