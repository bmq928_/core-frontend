import * as React from 'react'
import styled from 'styled-components'
import { classnames } from '../../../../common/classnames'
import { NButton } from '../../../../components/NButton/NButton'
import { Portal } from '../../../../components/Portal'
import { useLayoutAction, useLayoutBuilder } from '../../contexts/LayoutBuilderContext'
import { useElement } from '../../hooks/useElement'
import { ROOT_ELEMENT } from '../../utils/builder'
import { ZoneProps } from '../../utils/models'
import { sidebarPortalRef } from '../RightSidebar'
import { Actions, LayoutWrapper, Placeholder } from './StyledElements'

const ButtonWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 1rem;
`

type ZoneItemLayoutProps = {
  shouldRenderSidebar?: boolean
  sidebar?: React.ReactNode
  children?: React.ReactNode
} & Partial<ZoneProps>

/*
 * deprecated
 */
export function ZoneItemLayout({
  children,
  sidebar,
  elementId,
  parentId,
  index,
  canDrop,
  canDrag,
  canRemove = true,
}: ZoneItemLayoutProps) {
  const { elements, selectedElement } = useLayoutBuilder()
  const { selectElement, removeElement } = useLayoutAction()

  const element = elements[elementId!]

  const [{ isOver, isMouseOver, mousePosition }, ref] = useElement({
    elementId: elementId!,
    parentId: parentId!,
    index: index!,
    canDrop,
    canDrag,
  })

  const isSelected = selectedElement === elementId

  return (
    <React.Fragment>
      <LayoutWrapper
        onClick={e => {
          e.stopPropagation()
          selectElement(elementId!)
        }}
        className={classnames([
          isOver && 'is-over',
          isMouseOver && 'is-mouse-over',
          mousePosition && `is-sort ${mousePosition}`,
          isSelected && 'is-selected',
        ])}
        ref={ref}>
        {children}
        <Placeholder mousePosition={mousePosition} />
        <Actions className="element-actions">{element.label}</Actions>
      </LayoutWrapper>
      <Portal mountNode={sidebarPortalRef}>
        {isSelected && (
          <div
            onClick={e => {
              e.stopPropagation()
            }}>
            {sidebar}
            {selectedElement !== ROOT_ELEMENT && canRemove && (
              <ButtonWrapper>
                <NButton
                  size="small"
                  type="primary"
                  onClick={() => {
                    selectedElement && removeElement({ elementId: selectedElement })
                  }}>
                  Remove
                </NButton>
              </ButtonWrapper>
            )}
          </div>
        )}
      </Portal>
    </React.Fragment>
  )
}
