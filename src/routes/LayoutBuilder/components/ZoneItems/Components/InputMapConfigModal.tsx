import React, { FC } from 'react'
import { FormProvider, useForm, useFormContext } from 'react-hook-form'
import { useQuery } from 'react-query'
import styled, { useTheme } from 'styled-components'
import { color, spacing } from '../../../../../components/GlobalStyle'
import { NModal } from '../../../../../components/NModal/NModal'
import { NSpinner } from '../../../../../components/NSpinner/NSpinner'
import { NTextInput } from '../../../../../components/NTextInput/NTextInput'
import { APIServices } from '../../../../../services/api'
import { BuilderQueryKey } from '../../../../../services/api/endpoints/builder'
import { FlowsQueryKey } from '../../../../../services/api/endpoints/flows'
import { useLayoutAction } from '../../../contexts/LayoutBuilderContext'
import { Element } from '../../../utils/models'

const Wrapper = styled.div`
  padding: ${spacing('lg')};
`
const LoadingWrapper = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
`

const ScrollWrapper = styled.div`
  max-height: 60vh;
  overflow: auto;
`

const StyledInput = styled(NTextInput)`
  margin-bottom: ${spacing('xs')};
`

type Props = {
  visible: boolean
  setVisible: (visible: boolean) => void
  element: Element
}

export const InputMapConfigModal: FC<Props> = ({ visible, setVisible, element }) => {
  const { configElement } = useLayoutAction()
  const theme = useTheme()
  const formMethods = useForm()
  const { handleSubmit } = formMethods

  const { data: flowInputs, isLoading: isLoadingFlowInputs } = useQuery(
    [FlowsQueryKey.getFlowInputVariables, { flowName: element.props.name }],
    APIServices.Flows.getFlowInputVariables,
    {
      enabled: !!(element.props.type === 'flow' && element.props.name),
    },
  )

  const { data: fieldSchemas, isLoading: isLoadingFieldSchema } = useQuery(
    [BuilderQueryKey.getObjectSchema, { objName: element.props.objName }],
    APIServices.Builder.getObjectSchema,
    {
      enabled: !!(element.props.action === 'new' && element.props.objName),
      select: data => {
        return data.data.fields
      },
    },
  )

  const isAvailableInput = (flowInputs && flowInputs.data.length > 0) || (fieldSchemas && fieldSchemas.length > 0)

  const onSubmit = (data: any) => {
    isAvailableInput &&
      configElement(element.id, {
        inputMap: data,
      })
    setVisible(false)
  }

  const isLoading = isLoadingFlowInputs || isLoadingFieldSchema
  return (
    <NModal visible={visible} setVisible={setVisible}>
      <NModal.Header title="Config input map" onClose={() => setVisible(false)} />
      <NModal.Body>
        <FormProvider {...formMethods}>
          <Wrapper>
            {isLoading ? (
              <LoadingWrapper>
                <NSpinner size={30} strokeWidth={2} color={color('Primary900')({ theme })} />
              </LoadingWrapper>
            ) : (
              <>
                {isAvailableInput ? (
                  <ScrollWrapper>
                    {/* TODO: Check input type for suitable input */}
                    {flowInputs?.data.map(inputMap => (
                      <InputByType
                        key={inputMap.name}
                        name={inputMap.name}
                        label={inputMap.display_name}
                        type={inputMap.type}
                        inputMap={element.props.inputMap}
                      />
                    ))}
                    {fieldSchemas?.map(field => {
                      return (
                        <InputByType
                          key={field.name}
                          name={field.name}
                          label={field.displayName}
                          type={field.typeName}
                          subType={field.subType}
                          inputMap={element.props.inputMap}
                        />
                      )
                    })}
                  </ScrollWrapper>
                ) : (
                  <div>This flow does not contains any input</div>
                )}
              </>
            )}
          </Wrapper>
        </FormProvider>
      </NModal.Body>
      <NModal.Footer onFinish={handleSubmit(onSubmit)} onCancel={() => setVisible(false)} />
    </NModal>
  )
}

type InputByTypeProps = {
  name: string
  label: string
  type: string
  subType?: string
  inputMap?: Record<string, any>
}
const InputByType: FC<InputByTypeProps> = ({ name, label, type, subType, inputMap }) => {
  const formMethods = useFormContext<Record<string, any>>()
  const { register } = formMethods

  /* TODO: Check input type for suitable input */
  if (type === 'generated') {
    return null
  }

  return (
    <StyledInput
      defaultValue={inputMap?.[`${name}`] || ''}
      label={label}
      placeholder={`${type}${subType ? ` (${subType})` : ''}`}
      {...register(name)}
    />
  )
}
