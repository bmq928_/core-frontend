import * as React from 'react'
import { useQuery } from 'react-query'
import { useParams } from 'react-router-dom'
import styled from 'styled-components'
import { NButton } from '../../../../../components/NButton/NButton'
import { SelectOption } from '../../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../../components/NSelect/NSingleSelect'
import { useSearchText } from '../../../../../hooks/useSearchText'
import { APIServices } from '../../../../../services/api'
import { BuilderQueryKey } from '../../../../../services/api/endpoints/builder'
import { TriggerFlowResponse, TriggerObjectResponse } from '../../../../../services/api/models'
import { capitalize } from '../../../../../utils/utils'
import { useLayoutAction, useLayoutBuilder } from '../../../contexts/LayoutBuilderContext'
import { ElementType, ZoneProps } from '../../../utils/models'
import { FieldIcons } from '../../Icons'
import { TextInput } from '../../Inputs/TextInput'
import { Widget } from '../../Sidebar'
import ElementLayout from '../ElementLayout'
import { InputMapConfigModal } from './InputMapConfigModal'

const StyledElementLayout = styled(ElementLayout)`
  min-height: initial;
`

const BtnWrapper = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
`

const BUTTON_OPTIONS: SelectOption[] = [
  {
    label: 'Default',
    value: 'default',
  },
  {
    label: 'Primary',
    value: 'primary',
  },
  {
    label: 'Ghost',
    value: 'ghost',
  },
  {
    label: 'Outline',
    value: 'outline',
  },
  {
    label: 'Link',
    value: 'link',
  },
]

const BUTTON_SIZES: SelectOption[] = [
  {
    label: 'Default',
    value: 'default',
  },
  {
    label: 'Small',
    value: 'small',
  },
]

export const Button = React.forwardRef<HTMLDivElement, ZoneProps>(function Trigger(
  { elementId, parentId, index, ...restOptions },
  ref,
) {
  const { elements, layoutDetails } = useLayoutBuilder()
  const { configElement } = useLayoutAction()
  const [showConfig, setShowConfig] = React.useState(false)

  const element = elements[elementId]

  const { layoutId } = useParams<{ layoutId: string }>() || { layoutId: '' }

  const { isLoading, data } = useQuery(
    [BuilderQueryKey.getTriggers, { id: layoutId }],
    APIServices.Builder.getTriggers,
    {
      select(response) {
        return response.data.reduce(
          (a, i) => {
            const value = i.type === 'object' ? `${i.type}_${i.action}` : `${i.type}_${i.flowId}`
            const label = i.type === 'object' ? `${capitalize(i.displayName)} (Object)` : `${i.displayName} (Flow)`
            return {
              options: a.options.concat({ label, value }),
              actionsById: {
                ...a.actionsById,
                [value]: i,
              },
            }
          },
          {
            options: [] as SelectOption[],
            actionsById: {} as Record<string, TriggerFlowResponse | TriggerObjectResponse>,
          },
        )
      },
    },
  )

  const [{ searchValue, searchText }, handleSearchChange] = useSearchText('')

  const { data: objectOptions, isLoading: isObjectLoading } = useQuery(
    [BuilderQueryKey.getAllObjects, { searchText }],
    APIServices.Builder.getAllObjects,
    {
      keepPreviousData: true,
      select: data => {
        return data.data.data.map(option => {
          const typeName = option.isExternal ? ' (external)' : ' (internal)'
          return {
            value: option.name,
            label: option.displayName + typeName,
          }
        }) as SelectOption[]
      },
    },
  )

  const isDisabled =
    element.props.type === 'object'
      ? !Boolean(element.props.actionId && element.props.objName)
      : !element.props.actionId

  const hasInputMapConfig = element.props.type === 'flow' || element.props.action === 'new'

  return (
    <StyledElementLayout
      label="Button"
      elementId={elementId}
      parentId={parentId}
      index={index}
      {...restOptions}
      ref={ref}
      sidebar={
        <React.Fragment>
          <Widget title="Action">
            <NSingleSelect
              options={data?.options || []}
              value={element.props.actionId}
              onValueChange={actionId => {
                if (!actionId || !data?.actionsById[actionId]) {
                  console.error('Button', `Action Id: ${actionId} is invalid`)
                  return
                }
                configElement(elementId, {
                  actionId,
                  title: element.props.title || data?.actionsById[actionId!].displayName,
                  objName: layoutDetails?.objectName,
                  ...data?.actionsById[actionId!],
                })
              }}
              isLoading={isLoading}
              fullWidth
            />
          </Widget>
          {element.props.type === 'object' && (
            <Widget title="Object">
              <NSingleSelect
                disabled={!Boolean(element.props.actionId)}
                options={objectOptions || []}
                value={element.props.objName}
                onValueChange={objName => {
                  configElement(elementId, {
                    objName,
                  })
                }}
                isLoading={isObjectLoading}
                fullWidth
                isSearchable
                searchValue={searchValue}
                onSearchValueChange={handleSearchChange}
              />
            </Widget>
          )}
          {hasInputMapConfig && (
            <Widget title="Input map">
              <BtnWrapper>
                <NButton onClick={() => setShowConfig(true)}>Show config</NButton>
              </BtnWrapper>
            </Widget>
          )}
          <Widget title="Action Title">
            <TextInput
              disabled={isDisabled}
              value={element.props.title || ''}
              onChange={e => configElement(elementId, { title: e.target.value })}
            />
          </Widget>

          <Widget title="Button Type">
            <NSingleSelect
              disabled={isDisabled}
              options={BUTTON_OPTIONS}
              value={element.props.buttonType}
              onValueChange={buttonType => configElement(elementId, { buttonType })}
              fullWidth
            />
          </Widget>
          <Widget title="Button Size">
            <NSingleSelect
              disabled={isDisabled}
              options={BUTTON_SIZES}
              value={element.props.buttonSize}
              onValueChange={buttonSize => configElement(elementId, { buttonSize })}
              fullWidth
            />
          </Widget>
        </React.Fragment>
      }>
      <NButton type={element.props.buttonType} size={element.props.buttonSize}>
        {element.props.title || 'Action Title'}
      </NButton>
      {showConfig && element && (
        <InputMapConfigModal element={element} visible={showConfig} setVisible={setShowConfig} />
      )}
    </StyledElementLayout>
  )
})

export const widgetButton = {
  icon: <FieldIcons.Button />,
  label: 'Button',
  type: ElementType.Field,
  component: 'Button',
  props: {
    actionId: '',
    title: '',
    buttonType: 'outline',
    buttonSize: 'small',
    inputMap: {},
  },
  children: [],
}
