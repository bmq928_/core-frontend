import { FC } from 'react'
import { useQuery } from 'react-query'
import styled from 'styled-components'
import { spacing } from '../../../../../components/GlobalStyle'
import { SidebarIcons } from '../../../../../components/Icons'
import { NDivider } from '../../../../../components/NDivider'
import { NSingleSelect } from '../../../../../components/NSelect/NSingleSelect'
import { typography } from '../../../../../components/NTypography'
import { useSearchText } from '../../../../../hooks/useSearchText'
import { APIServices } from '../../../../../services/api'
import { FlowsQueryKey } from '../../../../../services/api/endpoints/flows'
import { useLayoutAction, useLayoutBuilder } from '../../../contexts/LayoutBuilderContext'
import { ElementType, ZoneProps } from '../../../utils/models'
import { ComponentIcons } from '../../Icons'
import { Widget } from '../../Sidebar'
import ElementLayout from '../ElementLayout'

const FlowWrapper = styled.div`
  padding: ${spacing('xl')};
`

const FlowHeader = styled.div`
  display: flex;
  align-items: center;
`

const FlowHeaderTxt = styled.p`
  ${typography('s600')}
`

const FlowPlaceholderTxt = styled.p`
  ${typography('x-small-ui-text')}
`

export const Flow: FC<ZoneProps> = ({ elementId, parentId, index, ...restProps }) => {
  const { elements } = useLayoutBuilder()
  const { configElement } = useLayoutAction()

  const element = elements[elementId]
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText()

  const { isFetching: isLoadingFlows, data: screenFlowsData } = useQuery(
    [FlowsQueryKey.getScreenFlows, { searchText }],
    APIServices.Flows.getScreenFlows,
    {
      select: flows => {
        return flows.data.data
          .filter(flow => flow.is_active)
          .map(flow => {
            return {
              value: flow.guid,
              label: flow.display_name,
            }
          })
      },
    },
  )

  const selectedLabel = (screenFlowsData || []).find(flow => flow.value === element.props.flowId)?.label || ''

  return (
    <ElementLayout
      {...restProps}
      label="Flow"
      elementId={elementId}
      parentId={parentId}
      index={index}
      sidebar={
        <Widget title="Flow" required>
          <NSingleSelect
            required
            fullWidth
            isSearchable
            options={screenFlowsData || []}
            searchValue={searchValue}
            onSearchValueChange={handleSearchChange}
            isLoading={isLoadingFlows}
            onValueChange={value => {
              configElement(elementId, {
                flowId: value,
              })
            }}
            value={element.props.flowId}
            placeholder="Select a screen flow"
            caption="Flows don't run in the canvas so that they don't accidentally do something in your org, like create or delete records."
          />
        </Widget>
      }>
      <FlowWrapper>
        <FlowHeader>
          <SidebarIcons.AutomationFilled />
          <NDivider vertical size="md" />
          <div>
            <FlowHeaderTxt>Flow component:</FlowHeaderTxt>
            <NDivider size="xs" />
            <FlowHeaderTxt>{selectedLabel}</FlowHeaderTxt>
          </div>
        </FlowHeader>
        <NDivider size="md" />
        <FlowPlaceholderTxt>This is a placeholder. Flows don't run in builder.</FlowPlaceholderTxt>
      </FlowWrapper>
    </ElementLayout>
  )
}

export const widgetFlow = {
  icon: <ComponentIcons.Flow />,
  label: 'Flow',
  type: ElementType.Field,
  component: 'Flow',
  props: {
    flowId: '',
  },
  children: [],
}
