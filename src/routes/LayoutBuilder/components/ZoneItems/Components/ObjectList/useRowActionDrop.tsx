import { nanoid } from '@reduxjs/toolkit'
import { useDrop } from 'react-dnd'
import { useLayoutAction, useLayoutBuilder } from '../../../../contexts/LayoutBuilderContext'
import { LEFT_SIDEBAR } from '../../../../utils/builder'
import { DraggableElement, Element, ElementType } from '../../../../utils/models'
import { widgetButton } from '../Button'

export const useRowActionDrop = (elementId: string) => {
  const { addSubElement, configElement } = useLayoutAction()
  const { elements } = useLayoutBuilder()
  const element = elements[elementId]

  return useDrop({
    accept: [ElementType.Field],
    canDrop(dragItem: DraggableElement) {
      return !!(dragItem.item && dragItem.item.component === 'Button' && dragItem.parentId === LEFT_SIDEBAR)
    },
    drop(_dragItem: DraggableElement, monitor) {
      if (monitor.didDrop()) {
        return
      }

      const newActionId = nanoid()
      const { icon: _icon, ...newItem } = widgetButton
      const newElement = {
        id: newActionId,
        ...newItem,
      } as Element

      addSubElement({ element: newElement })
      configElement(elementId, { rowActions: [...(element.props.rowActions || []), newActionId] })
    },
    collect(monitor) {
      return {
        isOver: monitor.isOver(),
        canDropRowActions: monitor.canDrop(),
      }
    },
  })
}
