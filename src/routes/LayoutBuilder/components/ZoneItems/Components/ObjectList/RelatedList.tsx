import { nanoid } from 'nanoid'
import * as React from 'react'
import { useQuery } from 'react-query'
import { useTheme } from 'styled-components'
import { classnames } from '../../../../../../common/classnames'
import { color } from '../../../../../../components/GlobalStyle'
import { NButton } from '../../../../../../components/NButton/NButton'
import { NDivider } from '../../../../../../components/NDivider'
import { SelectOption } from '../../../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../../../components/NSelect/NSingleSelect'
import { NSortableList } from '../../../../../../components/NSortableList'
import { NSpinner } from '../../../../../../components/NSpinner/NSpinner'
import { NTableColumnType } from '../../../../../../components/NTable/NTable'
import { APIServices } from '../../../../../../services/api'
import { BuilderQueryKey } from '../../../../../../services/api/endpoints/builder'
import { DataQueryKey } from '../../../../../../services/api/endpoints/data'
import { FieldSchema } from '../../../../../../services/api/models'
import { capitalize } from '../../../../../../utils/utils'
import { useLayoutAction, useLayoutBuilder } from '../../../../contexts/LayoutBuilderContext'
import { ObjectSchema, useLayoutBuilderData } from '../../../../contexts/LayoutBuilderDataContext'
import { useElementRenderer } from '../../../../hooks/useElementRenderer'
import { resolveFilterVariable } from '../../../../utils/builder'
import { Element, ElementType, ZoneProps } from '../../../../utils/models'
import { ComponentIcons } from '../../../Icons'
import { TextArea } from '../../../Inputs/TextArea'
import { TextInput } from '../../../Inputs/TextInput'
import { Widget } from '../../../Sidebar'
import ElementLayout from '../../ElementLayout'
import { Button } from '../Button'
import { ColumnSelectionModal } from './ColumnSelectionModal'
import { FilterSelection } from './FilterSelection'
import {
  ActionsWrapper,
  CustomDroppableZone,
  DropzoneIndicator,
  DropzoneText,
  DropzoneTitle,
  ErrorCell,
  FieldsWrapper,
  ItemDescription,
  ItemHeader,
  ItemName,
  LoadingContainer,
  ObjectDataTable,
  ObjectWrapper,
  SORT_ORDER_OPTIONS,
  TableWrapper,
} from './ObjectList'
import { useRowActionDrop } from './useRowActionDrop'

const DEFAULT_SCHEMA = {
  fields: [],
  fieldsByName: {},
  fieldNameOptions: [],
  sortByOptions: [],
  actions: [],
  related: {},
} as ObjectSchema & { fieldNameOptions: SelectOption[]; sortByOptions: SelectOption[] }

export function RelatedList({ elementId, parentId, index, canDrop = false, ...restProps }: ZoneProps) {
  const theme = useTheme()
  const { elements } = useLayoutBuilder()
  const { configElement, removeSubElement } = useLayoutAction()

  const { previewData, inputMap } = useLayoutBuilderData()
  const actions = useElementRenderer(elementId)

  const element = elements[elementId]

  // can drop row options
  const [{ canDropRowActions, isOver }, drop] = useRowActionDrop(elementId)

  // get scheme for relation object
  const { data: relatedSchema = DEFAULT_SCHEMA } = useQuery(
    [BuilderQueryKey.getObjectSchema, { objName: element.props.objName }],
    APIServices.Builder.getObjectSchema,
    {
      enabled: Boolean(previewData && previewData.guid && previewData.objName && element.props.objName),
      select: response => {
        if (response.data) {
          const { fields, fieldsByName, fieldNameOptions, sortByOptions } = response.data.fields.reduce(
            (allFields, field) => {
              const newSortByOptions = field.isSortable
                ? [...allFields.sortByOptions, { label: field.displayName, value: field.name }]
                : allFields.sortByOptions

              return {
                fields: [...allFields.fields, field],
                fieldsByName: { ...allFields.fieldsByName, [field.name]: field },
                fieldNameOptions: [...allFields.fieldNameOptions, { label: field.displayName, value: field.name }],
                sortByOptions: newSortByOptions,
              }
            },
            { fields: [], fieldsByName: {}, fieldNameOptions: [], sortByOptions: [{ value: '', label: 'None' }] } as {
              fields: FieldSchema[]
              fieldsByName: Record<string, FieldSchema>
              fieldNameOptions: SelectOption[]
              sortByOptions: SelectOption[]
            },
          )

          return {
            fields,
            fieldNameOptions,
            sortByOptions,
            fieldsByName,
            actions: [] as string[],
            related: response.data.related,
          } as ObjectSchema & { fieldNameOptions: SelectOption[]; sortByOptions: SelectOption[] }
        }
        return DEFAULT_SCHEMA
      },
    },
  )

  // get preview data for relation object based on filtered
  const isFiltered = !!element.props.filters && element.props.filters.length > 0

  const mapFilters = resolveFilterVariable(element.props.filters || [], inputMap)

  const { data: relatedData, isLoading: isRelatedDataLoading } = useQuery(
    [
      DataQueryKey.getObjectRelatedData,
      {
        name: previewData.objName,
        guid: previewData!.guid,
        relation: element.props.relationField,
        sortBy: element.props.sortBy,
        sortOrder: element.props.sortBy ? element.props.sortOrder : undefined,
      },
    ],
    APIServices.Data.getObjectRelatedData,
    {
      enabled: Boolean(
        previewData && previewData.guid && previewData.objName && element.props.relationField && !isFiltered,
      ),
    },
  )

  const { data: filteredRelatedData, isLoading: isFilteredRelatedDataLoading } = useQuery(
    [
      DataQueryKey.searchObjectRelatedData,
      {
        name: previewData.objName,
        guid: previewData!.guid,
        relation: element.props.relationField,
        filters: mapFilters,
        sortBy: element.props.sortBy,
        sortOrder: element.props.sortBy ? element.props.sortOrder : undefined,
      },
    ],
    APIServices.Data.searchObjectRelatedData,
    {
      enabled: Boolean(
        previewData && previewData.guid && previewData.objName && element.props.relationField && isFiltered,
      ),
    },
  )

  const isDisabled = !element.props.objName

  const columns = React.useMemo<NTableColumnType<any>>(() => {
    return element.props.fields.map((fieldName: string) => {
      if (!relatedSchema) {
        return {
          Header: '',
          accessor: nanoid(),
        }
      }

      const field = relatedSchema.fields.find(f => f.name === fieldName)

      if (!field) {
        return {
          Header: capitalize(fieldName),
          Cell: () => <ErrorCell>This field may get deleted</ErrorCell>,
        }
      }

      return {
        Header: field.displayName,
        accessor: field.name,
        Cell: (cell: any) => {
          return cell.value !== null && typeof cell.value === 'object' ? 'Cannot preview' : cell.value || null
        },
      }
    })
  }, [element.props.fields, relatedSchema])

  const handleSortRowActions = (fromIndex: number, toIndex: number) => {
    const next = [...element.props.rowActions]
    ;[next[toIndex], next[fromIndex]] = [next[fromIndex], next[toIndex]]
    configElement(elementId, { rowActions: next })
  }

  return (
    <ElementLayout
      {...restProps}
      label="Related List"
      elementId={elementId}
      parentId={parentId}
      index={index}
      canDrop={item => {
        if (item.item) {
          return ['Button'].includes(item.item.component) || false
        }
        if (typeof canDrop === 'function') {
          return canDrop(item)
        }
        return Boolean(canDrop)
      }}
      sidebar={
        <ObjectListSidebar
          // allActions={relatedSchema?.actions || []}
          element={element}
          configElement={configElement}
          isDisabled={isDisabled}
          fieldNameOptions={relatedSchema?.fieldNameOptions || []}
          sortByOptions={relatedSchema?.sortByOptions || []}
          fieldsByName={relatedSchema?.fieldsByName}
        />
      }>
      <ObjectWrapper className={classnames([isDisabled && 'is-disabled'])}>
        {isDisabled && <div>Select an Object to start</div>}
        {!isDisabled && (
          <>
            <ItemHeader>
              <ItemName>{element.props.title || `Related List`}</ItemName>
              <ItemDescription>{element.props.description}</ItemDescription>
            </ItemHeader>
            <ActionsWrapper>{actions}</ActionsWrapper>
            <FieldsWrapper>
              <TableWrapper>
                {isRelatedDataLoading || isFilteredRelatedDataLoading ? (
                  <LoadingContainer>
                    <NSpinner size={20} strokeWidth={4} color={color('Primary700')({ theme })} />
                  </LoadingContainer>
                ) : (
                  <ObjectDataTable
                    colNumber={element.props.fields.length}
                    data={(isFiltered ? filteredRelatedData?.data.data : relatedData?.data.data) || []}
                    columns={columns}
                  />
                )}
              </TableWrapper>
              <CustomDroppableZone
                ref={drop}
                className={classnames([
                  isDisabled && 'is-disabled',
                  canDropRowActions && 'can-drop',
                  isOver && 'is-over',
                  element.props.rowActions && element.props.rowActions.length > 0 && 'has-fields',
                ])}>
                <DropzoneTitle>Row actions</DropzoneTitle>
                <NSortableList onSort={handleSortRowActions}>
                  {(element.props.rowActions || []).map((rowActionId: string) => {
                    return (
                      <Button
                        key={rowActionId}
                        elementId={rowActionId}
                        parentId={elementId}
                        index={index}
                        canDrag={false}
                        canDrop={false}
                        canDuplicate={false}
                        onRemove={() => {
                          const newRowOptions = element.props.rowActions.filter((id: string) => id !== rowActionId)
                          removeSubElement({ elementId: rowActionId, selectElementId: elementId })
                          configElement(elementId, { rowActions: newRowOptions })
                        }}
                      />
                    )
                  })}
                </NSortableList>
                <DropzoneIndicator>
                  <DropzoneText>Drag and Drop 'Button' here</DropzoneText>
                </DropzoneIndicator>
              </CustomDroppableZone>
            </FieldsWrapper>
          </>
        )}
      </ObjectWrapper>
    </ElementLayout>
  )
}

type SidebarProps = {
  // allActions: string[]
  element: Element
  configElement(elementId: string, props: Record<string, any>): void
  isDisabled: boolean
  fieldNameOptions?: SelectOption[]
  sortByOptions?: SelectOption[]
  fieldsByName?: Record<string, FieldSchema>
}

function ObjectListSidebar({
  // allActions,
  element,
  configElement,
  isDisabled,
  fieldNameOptions = [],
  sortByOptions = [],
  fieldsByName,
}: SidebarProps) {
  const [searchValue, setSearchValue] = React.useState('')

  const {
    objectSchema: { related },
  } = useLayoutBuilderData()

  const options = Object.keys(related).map(id => ({ label: related[id].name, value: id }))

  const relatedOptions = searchValue
    ? options.filter(i => i.label.toLowerCase().includes(searchValue.toLowerCase()))
    : options

  const [columnModal, setColumnModal] = React.useState(false)

  // const filteredActions = allActions.filter(
  //   action => !element.props.actions.some((elementAction: any) => elementAction === action),
  // )

  return (
    <>
      <Widget title="Title">
        <TextInput
          value={element.props.title || ''}
          onChange={e => configElement(element.id, { title: e.target.value })}
        />
      </Widget>
      <Widget title="Description">
        <TextArea
          value={element.props.description || ''}
          onChange={e => configElement(element.id, { description: e.target.value })}
        />
      </Widget>
      <Widget title="Object">
        <NSingleSelect
          label="Related Object"
          options={relatedOptions}
          value={element.props.relationField}
          onValueChange={newValue => {
            if (newValue) {
              const selected = related[newValue]
              configElement(element.id, {
                relationField: newValue,
                objName: selected.objName,
                title: selected.name,
                fields: [],
              })
            }
          }}
          fullWidth
          isSearchable
          searchValue={searchValue}
          onSearchValueChange={setSearchValue}
        />
      </Widget>
      <Widget title="Records">
        <TextInput
          label="Display Records"
          type="number"
          value={`${element.props.limit}`}
          onChange={e => configElement(element.id, { limit: +e.target.value })}
        />
        <NDivider size="sm" />
        <NButton disabled={isDisabled} size="small" type="outline-3" block onClick={() => setColumnModal(true)}>
          Column selection
        </NButton>
        <ColumnSelectionModal
          visible={columnModal}
          setVisible={setColumnModal}
          objectName={element.props.objName}
          fields={element.props.fields || []}
          onSubmit={fields => {
            configElement(element.id, { fields })
            setColumnModal(false)
          }}
        />
      </Widget>
      {/* <ActionSelection
        actions={filteredActions}
        onAdd={action => configElement(element.id, { actions: [...element.props.actions, action] })}
        disabled={isDisabled}>
        {element.props.actions.length <= 0 && <EmptyMessage>Select + to add new Action</EmptyMessage>}
        {element.props.actions.length > 0 &&
          element.props.actions.map((action: string) => {
            return (
              <ActionItem
                onClick={() =>
                  configElement(element.id, {
                    actions: element.props.actions.filter((actId: string) => actId !== action),
                  })
                }
                action="Remove"
                key={action}
                style={{ paddingLeft: 0, paddingRight: 0 }}>
                {capitalize(action)}
              </ActionItem>
            )
          })}
      </ActionSelection> */}
      <FilterSelection
        disabled={isDisabled}
        filters={element.props.filters}
        fieldsByName={fieldsByName}
        fieldNameOptions={fieldNameOptions}
        onSubmit={filters => {
          configElement(element.id, { filters })
        }}
      />
      <Widget title="Default Sort">
        <NSingleSelect
          label="Sort by"
          options={sortByOptions || []}
          value={element.props.sortBy}
          onValueChange={newValue => {
            if (newValue === '') {
              configElement(element.id, { sortBy: undefined })
              return
            }
            configElement(element.id, { sortBy: newValue, sortOrder: 'ASC' })
          }}
          fullWidth
        />
        {!!element.props.sortBy && (
          <>
            <NDivider size="sm" />
            <NSingleSelect
              label="Sort order"
              options={SORT_ORDER_OPTIONS}
              value={element.props.sortOrder}
              onValueChange={newValue => {
                configElement(element.id, { sortOrder: newValue })
              }}
              fullWidth
            />
          </>
        )}
      </Widget>
    </>
  )
}

export const widgetRelatedList = {
  icon: <ComponentIcons.RelatedList />,
  label: 'RelatedList',
  type: ElementType.Layout,
  component: 'RelatedList',
  props: {
    limit: 5,
    objName: '', // object related
    relationField: '', // relation field
    title: '',
    description: '',
    fields: [],
    actions: [],
    sortBy: undefined,
    sortOrder: 'ASC',
    rowActions: [],
  },
  children: [],
}
