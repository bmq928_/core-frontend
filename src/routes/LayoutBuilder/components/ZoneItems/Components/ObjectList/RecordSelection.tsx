import * as React from 'react'
import styled from 'styled-components'
import { color, spacing } from '../../../../../../components/GlobalStyle'
import { Icons } from '../../../../../../components/Icons'
import { NButton } from '../../../../../../components/NButton/NButton'
import { NCheckbox } from '../../../../../../components/NCheckbox/NCheckbox'
import { NModal } from '../../../../../../components/NModal/NModal'
import { moveItem, NSortableList } from '../../../../../../components/NSortableList/NSortableList'
import { FieldResponse } from '../../../../../../services/api/models'
import { ReactComponent as IconNumber } from './assets/Number.svg'
import { ReactComponent as IconString } from './assets/String.svg'

function getIcon(type: string) {
  if (type === 'text') {
    return IconString
  }
  return IconNumber
}

const BodyWrapper = styled('div')`
  max-height: 220px;
  overflow: auto;
`

const CountWrapper = styled('div')`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: ${spacing('xs')} ${spacing('md')};
  span {
    font-size: 9px;
    color: ${color('Neutral400')};
  }
`

const ItemWrapper = styled('div')`
  display: flex;
  align-items: center;
  padding: ${spacing('sm')} ${spacing('xl')};
  padding-left: 0;
  border: 1px solid ${color('transparent')};
  cursor: pointer;
  &.no-hover:hover {
    background: ${color('Neutral100')};
  }
  & > svg:first-of-type {
    color: ${color('transparent')};
  }
  &:not(.no-hover) {
    cursor: move;
    &:hover > svg:first-of-type {
      color: ${color('Neutral400')};
    }
  }
  & > svg:nth-of-type(2) {
    margin: 0 ${spacing('xxs')};
  }
  .item-name {
    flex: 1;
    color: ${color('Neutral700')};
  }
`

type RecordSelectionProps = {
  data: FieldResponse[]
  fields?: FieldResponse[]
  onSubmit?(fields: FieldResponse[]): void
  children: JSX.Element
}

export function RecordSelection({ data, fields = [], children, onSubmit }: RecordSelectionProps) {
  const [show, setShow] = React.useState(false)
  const [selected, setSelected] = React.useState<FieldResponse[]>(fields)

  React.useEffect(() => {
    setSelected(fields)
  }, [fields])

  const button = React.cloneElement(children, {
    disabled: data.length <= 0,
    onClick() {
      setShow(true)
    },
  })

  const isSelected = (field: FieldResponse) => selected.some(i => i.id === field.id)

  const handleSort = moveItem(setSelected)

  const handleSelect = (item: FieldResponse) => {
    setSelected(prev => {
      if (isSelected(item)) {
        return prev.filter(i => i.id !== item.id)
      }
      return [...prev, item]
    })
  }

  const handleSelectAll = () => {
    setSelected(prev => {
      if (prev.length === data.length) {
        return []
      }
      return data
    })
  }

  const handleSubmit = () => {
    setShow(false)
    if (onSubmit) {
      onSubmit(selected)
    }
  }

  const items = data.filter(item => !selected.some(i => item.id === i.id))

  return (
    <>
      {button}
      <NModal visible={show} setVisible={setShow}>
        <NModal.Header title="Record Selection" onClose={() => setShow(false)} />
        <NModal.Body>
          <CountWrapper>
            <span>
              {selected.length} of {data.length}
            </span>
            <NButton type="link" size="small" onClick={handleSelectAll}>
              {selected.length === data.length ? 'Unselect All' : 'Select all'}
            </NButton>
          </CountWrapper>
          <BodyWrapper>
            <NSortableList onSort={handleSort}>
              {selected.map(item => {
                const Icon = getIcon(item.dataType.name)
                return (
                  <ItemWrapper onClick={() => handleSelect(item)} key={item.id}>
                    <Icons.Reorder />
                    <Icon />
                    <div className="item-name">{item.displayName}</div>
                    <NCheckbox checked={isSelected(item)} onChange={() => handleSelect(item)} />
                  </ItemWrapper>
                )
              })}
            </NSortableList>
            {items.map(item => {
              const Icon = getIcon(item.dataType.name)
              return (
                <ItemWrapper onClick={() => handleSelect(item)} key={item.id} className="no-hover">
                  <Icons.Reorder />
                  <Icon />
                  <div className="item-name">{item.displayName}</div>
                  <NCheckbox checked={isSelected(item)} onChange={() => handleSelect(item)} />
                </ItemWrapper>
              )
            })}
          </BodyWrapper>
        </NModal.Body>
        <NModal.Footer>
          <NButton
            size="small"
            type="ghost"
            onClick={() => {
              setSelected(fields)
              setShow(false)
            }}>
            Cancel
          </NButton>
          <NButton
            size="small"
            type="primary"
            onClick={() => {
              handleSubmit()
            }}>
            Apply
          </NButton>
        </NModal.Footer>
      </NModal>
    </>
  )
}
