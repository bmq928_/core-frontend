import * as React from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { useQuery } from 'react-query'
import { useParams } from 'react-router-dom'
import styled from 'styled-components'
import { spacing } from '../../../../../../components/GlobalStyle'
import { NButton } from '../../../../../../components/NButton/NButton'
import {
  FilterItem,
  NConditionBuilder,
  transformToFormValue,
  transformToPropsValue,
} from '../../../../../../components/NConditionBuilder'
import { NModal } from '../../../../../../components/NModal/NModal'
import { NPerfectScrollbar } from '../../../../../../components/NPerfectScrollbar'
import { SelectOption } from '../../../../../../components/NSelect/model'
import { APIServices } from '../../../../../../services/api'
import { BuilderQueryKey } from '../../../../../../services/api/endpoints/builder'
import { FieldSchema } from '../../../../../../services/api/models'
import { Widget } from '../../../Sidebar'

const ModalBody = styled(NModal.Body)`
  max-height: 50vh;
`

const Wrapper = styled('div')`
  padding: ${spacing('xl')};
`
type FilterSelectionProps = {
  disabled?: boolean
  filters?: FilterItem[][]
  onSubmit?(filters?: FilterItem[][]): void
  fieldsByName?: Record<string, FieldSchema>
  fieldNameOptions?: SelectOption[]
}

export const FilterSelection = React.memo(function FilterSelection({
  disabled = false,
  filters,
  onSubmit,
  fieldsByName = {},
  fieldNameOptions = [],
}: FilterSelectionProps) {
  const { layoutId } = useParams<{ layoutId: string }>() || { layoutId: '' }
  const [show, setShow] = React.useState(false)

  const formMethods = useForm({
    defaultValues: {
      filters: (filters && transformToFormValue(filters)) || [{ rules: [{ fieldName: '', operator: '', value: '' }] }],
    },
    shouldUnregister: false,
  })

  const { data: operators = {}, isLoading } = useQuery(
    [BuilderQueryKey.getFilterOperators, { id: layoutId }],
    APIServices.Builder.getFilterOperators,
    {
      select(res) {
        return Object.keys(res.data).reduce((a, k) => {
          return {
            ...a,
            [k]: res.data[k].map(i => ({ value: i.value, label: i.displayName })),
          }
        }, {} as Record<string, SelectOption[]>)
      },
      enabled: !!layoutId,
    },
  )

  return (
    <Widget title="Filters">
      <NButton disabled={disabled} size="small" type="outline-3" block onClick={() => setShow(true)}>
        Show filter control
      </NButton>
      <NModal size="large" visible={show} setVisible={setShow}>
        <NModal.Header onClose={() => setShow(false)} title="Filters" />
        <NPerfectScrollbar>
          <ModalBody>
            <Wrapper>
              <FormProvider {...formMethods}>
                <NConditionBuilder
                  name="filters"
                  fieldNames={fieldNameOptions}
                  fields={fieldsByName}
                  operators={operators}
                  isOperatorsLoading={isLoading}
                />
              </FormProvider>
            </Wrapper>
          </ModalBody>
        </NPerfectScrollbar>
        <NModal.Footer
          onCancel={() => {
            if (filters) {
              formMethods.reset({ filters: transformToFormValue(filters) })
            }
            setShow(false)
          }}
          onFinish={formMethods.handleSubmit(v => {
            if (onSubmit) {
              onSubmit(transformToPropsValue(v.filters))
            }
            setShow(false)
          })}
        />
      </NModal>
    </Widget>
  )
})
