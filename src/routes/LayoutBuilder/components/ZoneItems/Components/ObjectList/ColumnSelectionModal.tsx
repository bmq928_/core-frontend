import React, { useEffect, useMemo, useState } from 'react'
import { useQuery } from 'react-query'
import { NModal } from '../../../../../../components/NModal/NModal'
import { NPerfectScrollbar } from '../../../../../../components/NPerfectScrollbar'
import { PickColumn, PickListOption } from '../../../../../../components/PickColumn'
import { APIServices } from '../../../../../../services/api'
import { BuilderQueryKey } from '../../../../../../services/api/endpoints/builder'

type CustomObjectSchemaResponse = {
  columnList: PickListOption[]
  columnByName: Record<string, PickListOption>
}

type Props = {
  visible: boolean
  setVisible: (val: boolean) => void
  objectName?: string
  fields: string[]
  onSubmit?(fields: string[]): void
}
export const ColumnSelectionModal: React.FC<Props> = ({ visible, setVisible, objectName, fields, onSubmit }) => {
  const [selectedColumn, setSelectedColumn] = useState<PickListOption[]>([])
  // const [searchText, setSearchText] = useState('')

  const { data: fieldData } = useQuery(
    [BuilderQueryKey.getObjectSchema, { objName: objectName! }],
    APIServices.Builder.getObjectSchema,
    {
      enabled: !!objectName,
    },
  )

  // transform response outside insteads of directly in query
  const { columnList, columnByName } = useMemo(() => {
    if (!fieldData) {
      return {
        columnList: [] as PickListOption[],
        columnByName: {} as Record<string, PickListOption>,
      }
    }

    return fieldData.data.fields.reduce(
      (allColumns, currentField) => {
        return {
          columnList: [...allColumns.columnList, { value: currentField.name, label: currentField.displayName }],
          columnByName: {
            ...allColumns.columnByName,
            [currentField.name]: { value: currentField.name, label: currentField.displayName },
          },
        }
      },
      { columnList: [], columnByName: {} } as CustomObjectSchemaResponse,
    )
  }, [fieldData])

  useEffect(() => {
    if (columnList.length === 0) {
      return
    }
    const defaultSelectedColumn = fields.map(fieldName => {
      return columnByName[fieldName]
    })
    setSelectedColumn(defaultSelectedColumn)
  }, [fields, columnList, columnByName])

  return (
    <NModal size="large" visible={visible} setVisible={setVisible}>
      <NModal.Header onClose={() => setVisible(false)} title="Column Selection" />
      <NModal.Body>
        <NPerfectScrollbar style={{ maxHeight: '70vh' }}>
          <PickColumn
            options={columnList || []}
            onOptionsChange={setSelectedColumn}
            selectedOptions={selectedColumn}
            leftHeader="Available Records"
            rightHeader="Selected Records"
            sortableSelected
          />
        </NPerfectScrollbar>
      </NModal.Body>
      <NModal.Footer
        onCancel={() => setVisible(false)}
        onFinish={() => {
          onSubmit && onSubmit(selectedColumn.map(col => col.value))
        }}
      />
    </NModal>
  )
}
