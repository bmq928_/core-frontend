import * as React from 'react'
import styled from 'styled-components'
import { rootPortal } from '../../../../../../App'
import { color, spacing } from '../../../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../../../components/Icons'
import { NButton } from '../../../../../../components/NButton/NButton'
import { NDivider } from '../../../../../../components/NDivider'
import { elevation } from '../../../../../../components/NElevation'
import { NTypography, typography } from '../../../../../../components/NTypography'
import { Portal } from '../../../../../../components/Portal'
import { useClickOutside } from '../../../../../../hooks/useClickOutside'
import { capitalize } from '../../../../../../utils/utils'
import { Widget } from '../../../Sidebar'
import { EmptyMessage } from '../../StyledElements'
import { ReactComponent as IconActions } from './assets/IconActions.svg'

type Props = {
  top: number
  left: number
  show?: boolean
}

export const OverlayWrapper = styled('div').attrs(({ top, left, show }: Props) => {
  return {
    style: {
      top,
      left,
      visibility: show ? 'visible' : 'hidden',
      userSelect: show ? 'auto' : 'none',
      opacity: show ? 1 : 0,
    },
  }
})<Props>`
  position: absolute;
  background: ${color('white')};
  min-width: 200px;
  ${elevation('Elevation300')};
  padding-bottom: ${spacing('md')};
`

export const Header = styled('div')`
  padding: ${spacing('sm')} ${spacing('md')};
  display: flex;
  justify-content: space-between;
  align-items: center;
`

type ActionSelectionProps = {
  actions: string[]
  onAdd(action: any): void
  portalNode?: React.RefObject<HTMLDivElement | null>
  children: React.ReactNode
  disabled?: boolean
}

export function ActionSelection({
  actions = [],
  onAdd,
  portalNode = rootPortal,
  children,
  disabled = false,
}: ActionSelectionProps) {
  const containerRef = React.useRef<HTMLDivElement>(null)
  const overlayRef = React.useRef<HTMLDivElement>(null)
  const [show, setShow] = React.useState(false)

  const { top, left } = React.useMemo(() => {
    if (show && containerRef.current && overlayRef.current) {
      const containerBounding = containerRef.current.getBoundingClientRect()

      const overlayBounding = overlayRef.current.getBoundingClientRect()

      return {
        top: containerBounding.top - 24,
        left: containerBounding.left - overlayBounding.width - 4,
      }
    }
    return { top: 0, left: 0 }
  }, [show])

  useClickOutside(() => setShow(false), overlayRef)

  return (
    <Widget
      title="Actions"
      rightElement={
        <NButton
          icon={<GlobalIcons.Plus />}
          type="ghost"
          size="small"
          onClick={() => {
            setShow(prev => (!prev ? true : prev))
          }}
          disabled={disabled}
        />
      }
      ref={containerRef}>
      {children}
      <Portal mountNode={portalNode}>
        <OverlayWrapper ref={overlayRef} top={top} left={left} show={show}>
          <Header>
            <NTypography.H200>Actions</NTypography.H200>
            <NButton type="ghost" size="small" icon={<GlobalIcons.Close />} onClick={() => setShow(false)} />
          </Header>
          {actions.length <= 0 && <EmptyMessage>No Action</EmptyMessage>}
          {actions.length > 0 &&
            actions.map(action => (
              <ActionItem onClick={() => onAdd(action)} action="Add" key={action}>
                {capitalize(action)}
              </ActionItem>
            ))}
        </OverlayWrapper>
      </Portal>
    </Widget>
  )
}

const ActionItemWrapper = styled('div')`
  display: flex;
  align-items: center;
  padding: ${spacing('xxs')} ${spacing('md')};
  &:nth-child(2) {
    flex: 1;
  }
  & > *:last-child {
    display: none;
  }
  &:hover {
    // background: ${color('Neutral100')};
    > *:last-child {
      display: block;
    }
  }
`

const ActionItemName = styled('div')`
  display: flex;
  align-items: center;
  flex: 1;
  min-height: 32px;
  color: ${color('Neutral700')};
  ${typography('button')};
`

type ActionItemProps = {
  action: string
  onClick?(): void
} & React.HTMLAttributes<HTMLDivElement>

export function ActionItem({ action, onClick, children, ...restProps }: ActionItemProps) {
  return (
    <ActionItemWrapper {...restProps}>
      <ActionItemName>
        <IconActions />
        <NDivider vertical size="xs" />
        {children}
      </ActionItemName>
      <NButton type="link" size="small" onClick={onClick}>
        {action}
      </NButton>
    </ActionItemWrapper>
  )
}
