import { nanoid } from 'nanoid'
import * as React from 'react'
import { useQuery } from 'react-query'
import styled, { useTheme } from 'styled-components'
import { classnames } from '../../../../../../common/classnames'
import { color, spacing } from '../../../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../../../components/Icons'
import { NButton } from '../../../../../../components/NButton/NButton'
import { CurrencyData } from '../../../../../../components/NCurrency/CurrencyData'
import { NDivider } from '../../../../../../components/NDivider'
import { NPerfectScrollbar } from '../../../../../../components/NPerfectScrollbar'
import { SelectOption } from '../../../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../../../components/NSelect/NSingleSelect'
import { NSortableList } from '../../../../../../components/NSortableList'
import { NSpinner } from '../../../../../../components/NSpinner/NSpinner'
import { NTable, NTableColumnType } from '../../../../../../components/NTable/NTable'
import { typography } from '../../../../../../components/NTypography'
import { useSearchText } from '../../../../../../hooks/useSearchText'
import { APIServices } from '../../../../../../services/api'
import { BuilderQueryKey } from '../../../../../../services/api/endpoints/builder'
import { DataQueryKey } from '../../../../../../services/api/endpoints/data'
import { FieldSchema, ObjectSchemaResponse } from '../../../../../../services/api/models'
import { capitalize } from '../../../../../../utils/utils'
import { useLayoutAction, useLayoutBuilder } from '../../../../contexts/LayoutBuilderContext'
import { useLayoutBuilderData } from '../../../../contexts/LayoutBuilderDataContext'
import { useElementRenderer } from '../../../../hooks/useElementRenderer'
import { resolveFilterVariable } from '../../../../utils/builder'
import { Element, ElementType, ZoneProps } from '../../../../utils/models'
import { ComponentIcons } from '../../../Icons'
import { TextArea } from '../../../Inputs/TextArea'
import { TextInput } from '../../../Inputs/TextInput'
import { Widget } from '../../../Sidebar'
import ElementLayout from '../../ElementLayout'
import { Button } from '../Button'
import { ColumnSelectionModal } from './ColumnSelectionModal'
import { FilterSelection } from './FilterSelection'
import { useRowActionDrop } from './useRowActionDrop'

export const ObjectWrapper = styled('div')`
  width: 100%;
  min-height: 200px;
  display: grid;
  grid-template: auto minmax(0, 1fr) / auto minmax(0, 1fr);
  grid-template-areas:
    'item-header actions'
    'fields fields';
  grid-gap: ${spacing('xs')};

  &.is-disabled {
    grid-template: 1fr / 1fr;
    grid-template-areas: unset;
    place-items: center;
  }
`

export const ItemHeader = styled('div')`
  grid-area: item-header;
`
export const ItemName = styled('div')`
  ${typography('h300')}
`
export const ItemDescription = styled('div')`
  ${typography('caption')};
  color: ${color('Neutral400')};
`

export const ActionsWrapper = styled('div')`
  display: flex;
  grid-area: actions;
  justify-self: end;
  & > *:not(:last-child) {
    margin-right: ${spacing('xxs')};
  }
`
export const FieldsWrapper = styled('div')`
  grid-area: fields;
  display: flex;
  border: 1px solid #eee;
`
export const TableWrapper = styled(NPerfectScrollbar)`
  flex: 1;
`

export const CustomDroppableZone = styled('div')`
  width: 150px;
  min-height: 200px;
  display: flex;
  flex-direction: column;

  &.is-disabled {
    background: ${color('Neutral400')};
  }
  &.can-drop {
    background: ${color('Primary100')};
  }
  &.has-fields {
    min-height: unset;
  }
`

export const DropzoneTitle = styled('div')`
  ${typography('h300')};
  padding: ${spacing('sm')};
`

export const DropzoneIndicator = styled.div`
  display: flex;
  justify-content: cener;
  align-items: center;
  margin: ${spacing('sm')};
  height: 100px;
  border: 1px dashed ${color('Primary400')};
`

export const DropzoneText = styled('div')`
  ${typography('caption')};
  color: ${color('Neutral400')};
  text-align: center;
`

export const LoadingContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex: 1;
`

export const ErrorCell = styled('div')`
  color: ${color('Red700')};
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
`

export const ObjectDataTable = styled(NTable)<{ colNumber: number }>`
  // so table wont shrink too small, have to set because of table-layout: fixed;
  min-width: ${props => `${props.colNumber * 200}px`};
`

export function ObjectList({ elementId, parentId, index, canDrop = false, ...restProps }: ZoneProps) {
  const { elements } = useLayoutBuilder()
  const { configElement, removeSubElement } = useLayoutAction()
  const { inputMap } = useLayoutBuilderData()
  const element = elements[elementId]
  const theme = useTheme()

  const isDisabled = !element.props.objName
  const isFiltered = !!element.props.filters && element.props.filters.length > 0

  const actions = useElementRenderer(elementId)
  const mapFilters = resolveFilterVariable(element.props.filters || [], inputMap)

  // can drop row options
  const [{ canDropRowActions, isOver }, drop] = useRowActionDrop(elementId)

  // get object data
  const { data: filteredObjectData, isFetching: isLoadingFilteredData } = useQuery(
    [
      DataQueryKey.searchObjectData,
      {
        name: element.props.objName,
        limit: element.props.limit,
        filters: mapFilters,
        sortBy: element.props.sortBy,
        sortOrder: element.props.sortBy ? element.props.sortOrder : undefined,
      },
    ],
    APIServices.Data.searchObjectData,
    {
      keepPreviousData: true,
      enabled: !!element.props.objName && isFiltered,
    },
  )

  // get content list
  const { data: objectData, isFetching: isLoadingObjectsData } = useQuery(
    [
      DataQueryKey.getObjectData,
      {
        name: element.props.objName,
        limit: element.props.limit,
        sortBy: element.props.sortBy,
        sortOrder: element.props.sortBy ? element.props.sortOrder : undefined,
      },
    ],
    APIServices.Data.getObjectData,
    {
      keepPreviousData: true,
      enabled: !!element.props.objName && !isFiltered,
    },
  )

  // get list of fields of the selected field
  const { data: fieldsData, isLoading: isLoadingFields } = useQuery(
    [BuilderQueryKey.getObjectSchema, { objName: element.props.objName }],
    APIServices.Builder.getObjectSchema,
    {
      enabled: !isDisabled,
    },
  )

  const columns = React.useMemo<NTableColumnType<any>>(() => {
    return element.props.fields.map((fieldName: string) => {
      if (!fieldsData) {
        return {
          Header: '',
          accessor: nanoid(),
        }
      }

      const field = fieldsData.data.fields.find(f => f.name === fieldName)

      if (!field) {
        return {
          Header: capitalize(fieldName),
          Cell: () => <ErrorCell>This field may get deleted</ErrorCell>,
        }
      }

      return {
        Header: field.displayName,
        accessor: field.name,
        Cell: (cell: any) => {
          if (field.typeName === 'currency') {
            return (
              <CurrencyData
                dataCurrencyCode={cell.row.original.currencyCode}
                value={cell.value}
                convertedValue={cell.row.original['$metadata']?.currency_values?.[field.name]}
              />
            )
          }
          if (field.typeName === 'boolean') {
            return cell.value ? <GlobalIcons.Check /> : null
          }
          return cell.value !== null && typeof cell.value === 'object' ? 'Cannot preview' : cell.value ?? null
        },
      }
    })
  }, [element.props.fields, fieldsData])

  const handleSortRowActions = (fromIndex: number, toIndex: number) => {
    const next = [...element.props.rowActions]
    ;[next[toIndex], next[fromIndex]] = [next[fromIndex], next[toIndex]]
    configElement(elementId, { rowActions: next })
  }

  const descriptionText =
    element.props.description || `${element.props.fields.length || 0} ${fieldsData?.data.displayName}`

  return (
    <ElementLayout
      {...restProps}
      label="Object List"
      elementId={elementId}
      parentId={parentId}
      index={index}
      canDrop={item => {
        if (item.item) {
          return ['Button'].includes(item.item.component) || false
        }
        if (typeof canDrop === 'function') {
          return canDrop(item)
        }
        return Boolean(canDrop)
      }}
      sidebar={
        <ObjectListSidebar
          element={element}
          configElement={configElement}
          isDisabled={isDisabled}
          fieldsData={fieldsData?.data}
        />
      }>
      <ObjectWrapper className={classnames([isDisabled && 'is-disabled'])}>
        {isDisabled && <div>Select an Object to start</div>}
        {!isDisabled && (
          <>
            <ItemHeader>
              <ItemName>{element.props.title || `${fieldsData?.data.displayName} Listing`}</ItemName>
              <ItemDescription>{descriptionText}</ItemDescription>
            </ItemHeader>
            <ActionsWrapper>{actions}</ActionsWrapper>
            <FieldsWrapper>
              <TableWrapper>
                {isLoadingFields ? (
                  <LoadingContainer>
                    <NSpinner size={20} strokeWidth={4} color={color('Primary700')({ theme })} />
                  </LoadingContainer>
                ) : (
                  <ObjectDataTable
                    colNumber={element.props.fields.length}
                    data={
                      (element.props.fields.length > 0 &&
                        (isFiltered ? filteredObjectData?.data.data : objectData?.data.data)) ||
                      []
                    }
                    columns={columns}
                    isLoading={isLoadingObjectsData || isLoadingFilteredData}
                  />
                )}
              </TableWrapper>
              <CustomDroppableZone
                ref={drop}
                className={classnames([
                  isDisabled && 'is-disabled',
                  canDropRowActions && 'can-drop',
                  isOver && 'is-over',
                  element.props.rowActions && element.props.rowActions.length > 0 && 'has-fields',
                ])}>
                <DropzoneTitle>Row actions</DropzoneTitle>
                <NSortableList onSort={handleSortRowActions}>
                  {(element.props.rowActions || []).map((rowActionId: string) => {
                    return (
                      <Button
                        key={rowActionId}
                        elementId={rowActionId}
                        parentId={elementId}
                        index={index}
                        canDrag={false}
                        canDrop={false}
                        canDuplicate={false}
                        onRemove={() => {
                          const newRowOptions = element.props.rowActions.filter((id: string) => id !== rowActionId)
                          removeSubElement({ elementId: rowActionId, selectElementId: elementId })
                          configElement(elementId, { rowActions: newRowOptions })
                        }}
                      />
                    )
                  })}
                </NSortableList>
                <DropzoneIndicator>
                  <DropzoneText>Drag and Drop 'Button' here</DropzoneText>
                </DropzoneIndicator>
              </CustomDroppableZone>
            </FieldsWrapper>
          </>
        )}
      </ObjectWrapper>
    </ElementLayout>
  )
}

export const SORT_ORDER_OPTIONS: SelectOption[] = [{ value: 'ASC' }, { value: 'DESC' }]

type SidebarProps = {
  // allActions: string[]
  element: Element
  configElement(elementId: string, props: Record<string, any>): void
  isDisabled: boolean
  fieldsData?: ObjectSchemaResponse
}

function ObjectListSidebar({ element, configElement, isDisabled, fieldsData }: SidebarProps) {
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText('')

  // get all objects
  const { data: objectsData, isFetching: isLoadingObjects } = useQuery(
    [BuilderQueryKey.getAllObjects, { searchText }],
    APIServices.Builder.getAllObjects,
    {
      keepPreviousData: true,
      select: data => {
        return data.data.data.map(option => {
          const typeName = option.isExternal ? ' (external)' : ' (internal)'
          return {
            value: option.name,
            label: option.displayName + typeName,
          }
        }) as SelectOption[]
      },
    },
  )

  const { fieldsByName, fieldNameOptions, sortByOptions } = React.useMemo(() => {
    if (fieldsData) {
      return fieldsData.fields.reduce(
        (a, f) => {
          const newSortByOptions = f.isSortable
            ? [...a.sortByOptions, { label: f.displayName, value: f.name }]
            : a.sortByOptions
          return {
            fieldNameOptions: [...a.fieldNameOptions, { label: f.displayName, value: f.name }],
            sortByOptions: newSortByOptions,
            fieldsByName: {
              ...a.fieldsByName,
              [f.name]: f,
            },
          }
        },
        {
          fieldNameOptions: [],
          fieldsByName: {},
          sortByOptions: [{ value: '', label: 'None' }],
        } as {
          fieldNameOptions: SelectOption[]
          sortByOptions: SelectOption[]
          fieldsByName: Record<string, FieldSchema>
        },
      )
    }
    return {
      fieldNameOptions: [],
      sortByOptions: [{ value: '', label: 'None' }],
      fieldsByName: {},
    } as { fieldNameOptions: SelectOption[]; sortByOptions: SelectOption[]; fieldsByName: Record<string, FieldSchema> }
  }, [fieldsData])

  const [columnModal, setColumnModal] = React.useState(false)

  return (
    <>
      <Widget title="Title">
        <TextInput
          value={element.props.title || ''}
          onChange={e => configElement(element.id, { title: e.target.value })}
        />
      </Widget>
      <Widget title="Description">
        <TextArea
          value={element.props.description || ''}
          onChange={e => configElement(element.id, { description: e.target.value })}
        />
      </Widget>
      <Widget title="Object">
        <NSingleSelect
          required
          label="Object"
          options={objectsData || []}
          value={element.props.objName}
          onValueChange={newValue => {
            configElement(element.id, { objName: newValue, fields: [], sortBy: undefined })
          }}
          fullWidth
          isSearchable
          searchValue={searchValue}
          onSearchValueChange={handleSearchChange}
          isLoading={isLoadingObjects}
        />
      </Widget>
      <Widget title="Records">
        <TextInput
          label="Display Records"
          type="number"
          value={`${element.props.limit}`}
          onChange={e => configElement(element.id, { limit: +e.target.value })}
        />
        <NDivider size="sm" />
        <NButton disabled={isDisabled} size="small" type="outline-3" block onClick={() => setColumnModal(true)}>
          Column selection
        </NButton>
        <ColumnSelectionModal
          visible={columnModal}
          setVisible={setColumnModal}
          objectName={element.props.objName}
          fields={element.props.fields || []}
          onSubmit={fields => {
            configElement(element.id, { fields })
            setColumnModal(false)
          }}
        />
      </Widget>
      <FilterSelection
        disabled={isDisabled}
        filters={element.props.filters}
        fieldNameOptions={fieldNameOptions}
        fieldsByName={fieldsByName}
        onSubmit={filters => {
          configElement(element.id, { filters })
        }}
      />
      <Widget title="Default Sort">
        <NSingleSelect
          label="Sort by"
          options={sortByOptions || []}
          value={element.props.sortBy}
          onValueChange={newValue => {
            if (newValue === '') {
              configElement(element.id, { sortBy: undefined })
              return
            }
            configElement(element.id, { sortBy: newValue, sortOrder: 'ASC' })
          }}
          fullWidth
          isLoading={isLoadingObjects}
        />
        {!!element.props.sortBy && (
          <>
            <NDivider size="sm" />
            <NSingleSelect
              label="Sort order"
              options={SORT_ORDER_OPTIONS}
              value={element.props.sortOrder}
              onValueChange={newValue => {
                configElement(element.id, { sortOrder: newValue })
              }}
              fullWidth
            />
          </>
        )}
      </Widget>
    </>
  )
}

export const widgetObjectList = {
  icon: <ComponentIcons.ObjectList />,
  label: 'Object List',
  type: ElementType.Layout,
  component: 'ObjectList',
  props: {
    limit: 10,
    objName: '',
    fields: [],
    actions: [],
    rowActions: [],
    sortBy: undefined,
    sortOrder: 'ASC',
  },
  children: [],
}
