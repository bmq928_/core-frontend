import React from 'react'
import { useDrop } from 'react-dnd'
import styled from 'styled-components'
import { classnames } from '../../../../../../common/classnames'
import { color, spacing } from '../../../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../../../components/Icons'
import { NButton } from '../../../../../../components/NButton/NButton'
import { NCheckbox } from '../../../../../../components/NCheckbox/NCheckbox'
import { NDivider } from '../../../../../../components/NDivider'
import { NSortableList } from '../../../../../../components/NSortableList'
import { NTextInput } from '../../../../../../components/NTextInput/NTextInput'
import { typography } from '../../../../../../components/NTypography'
import { useLayoutAction, useLayoutBuilder } from '../../../../contexts/LayoutBuilderContext'
import { useLayoutBuilderData } from '../../../../contexts/LayoutBuilderDataContext'
import { useElementRenderer } from '../../../../hooks/useElementRenderer'
import { ElementType, ZoneProps } from '../../../../utils/models'
import { DraggableItem } from '../../../DraggableItem'
import { ComponentIcons } from '../../../Icons'
import { TextInput } from '../../../Inputs/TextInput'
import { Widget } from '../../../Sidebar'
import ElementLayout from '../../ElementLayout'
import { FieldAndActionSelection, GridItem } from './FieldAndActionSelection'
import { FieldItem as BaseFieldItem } from './FieldItem'
import { RecordDetailFileDropzone, RecordDetailFileDropzoneSidebar } from './RecordDetailFileDropzone'

const DroppableZone = styled('div')`
  width: 100%;
  padding: ${spacing('xs')};
  display: grid;
  grid-template: auto 1fr / auto 1fr;
  grid-template-areas:
    'item-header actions'
    'fields fields';
  grid-gap: ${spacing('xs')};

  &.is-disabled {
    grid-template: 1fr / 1fr;
    grid-template-areas: unset;
    place-items: center;
  }
  &.can-drop {
    background: ${color('Primary100')};
  }
  min-height: 200px;
  &.has-fields {
    min-height: unset;
  }
`

const ItemHeader = styled('div')`
  grid-area: item-header;
`
const ItemName = styled('div')`
  ${typography('h300')}
`
const ItemDescription = styled('div')`
  ${typography('caption')};
  color: ${color('Neutral400')};
`

const ActionsWrapper = styled('div')`
  display: flex;
  grid-area: actions;
  justify-self: end;
  & > *:not(:last-child) {
    margin-right: ${spacing('xs')};
  }
`
const FieldsWrapper = styled(NSortableList)<{ columns: number }>`
  grid-area: fields;
  border: 1px solid #eee;
  display: grid;
  grid-template-columns: repeat(${props => props?.columns || 1}, minmax(0, 1fr));
  grid-gap: ${spacing('md')};
`

const SearchInput = styled(NTextInput)`
  border: none;
  padding: unset;
`

// const ActionButton = styled(NButton)`
//   position: relative;
//   svg {
//     display: none;
//     position: absolute;
//     top: -12px;
//     right: -12px;
//   }
//   &:hover {
//     svg {
//       display: block;
//     }
//   }
// `

const FieldItem = styled(BaseFieldItem)`
  position: relative;
  .close-icon {
    display: none;
    position: absolute;
    top: 0px;
    right: 0px;
  }
  &:hover {
    .close-icon {
      display: block;
    }
  }
`

export function RecordDetails({ elementId, parentId, index, ...restProps }: ZoneProps) {
  const { layoutDetails } = useLayoutBuilder()
  const objectName = layoutDetails!.objectName

  const {
    isLoading,
    previewData,
    objectSchema: { fields, fieldsByName },
  } = useLayoutBuilderData()

  const { elements } = useLayoutBuilder()
  const { configElement } = useLayoutAction()

  const actions = useElementRenderer(elementId)

  const element = elements[elementId]

  const [searchValue, setSearchValue] = React.useState('')
  const [showFieldSelect, setShowFieldSelect] = React.useState(false)
  const [isFirstRender, setIsFirstRender] = React.useState(true)

  React.useEffect(() => {
    setIsFirstRender(false)
  }, [])

  const [{ canDrop, isOver }, drop] = useDrop({
    accept: [`ACTION_${elementId}`, `FIELD_${elementId}`],
    drop(item: { type: string; id: string }, monitor) {
      if (monitor.didDrop()) {
        return
      }

      if (item.type.includes('ACTION')) {
        configElement(elementId, { actions: [...(element.props.actions || []), item.id] })
        return
      }
      configElement(elementId, { fields: [...(element.props.fields || []), item.id] })
    },
    collect(monitor) {
      // use isFirstRender check to prevent show bottom on init
      if (!monitor.canDrop() && !isFirstRender) {
        setShowFieldSelect(true)
      }
      return {
        isOver: monitor.isOver(),
        canDrop: monitor.canDrop(),
      }
    },
  })

  const isDisabled = !objectName

  // const handleSortActions = (fromIndex: number, toIndex: number) => {
  //   const next = [...element.props.actions]
  //   ;[next[toIndex], next[fromIndex]] = [next[fromIndex], next[toIndex]]
  //   configElement(elementId, { actions: next })
  // }

  const handleSortFields = (fromIndex: number, toIndex: number) => {
    const next = [...element.props.fields]
    ;[next[toIndex], next[fromIndex]] = [next[fromIndex], next[toIndex]]
    configElement(elementId, { fields: next })
  }

  // const filteredActions = searchValue
  //   ? actions.filter(i => i.toLowerCase().includes(searchValue.toLowerCase()))
  //   : actions

  const filteredFields = searchValue
    ? fields.filter(i => i.displayName.toLowerCase().includes(searchValue.toLowerCase()))
    : fields || []

  return (
    <ElementLayout
      {...restProps}
      label="Record Details"
      elementId={elementId}
      parentId={parentId}
      index={index}
      canDrop={item => {
        if (item.item) {
          return ['Button'].includes(item.item.component)
        }
        return false
      }}
      sidebar={
        <>
          <Widget title="Title">
            <TextInput
              value={element.props.title}
              onChange={e => configElement(elementId, { title: e.target.value })}
            />
          </Widget>
          <Widget title="Description">
            <TextInput
              value={element.props.description}
              onChange={e => configElement(elementId, { description: e.target.value })}
            />
          </Widget>
          <Widget>
            <NCheckbox
              title="Is Primary"
              subtitle="Some description about isPrimary"
              checked={element.props.isPrimary}
              onChange={e => {
                configElement(elementId, { isPrimary: e.target.checked })
              }}
            />
          </Widget>
          <Widget title="Columns">
            <TextInput
              type="number"
              min={1}
              value={element.props.numOfCols}
              onChange={e => {
                const col = (e.target.value && +e.target.value) || ''
                configElement(elementId, { numOfCols: col })
              }}
            />
          </Widget>
          <Widget title="Field Mapping">
            <FieldAndActionSelection
              visible={showFieldSelect}
              setVisible={setShowFieldSelect}
              fields={filteredFields.map(field => (
                <DraggableItem
                  onDrag={() => setShowFieldSelect(false)}
                  key={field.name}
                  type={`FIELD_${elementId}`}
                  id={field.name}
                  disable={element.props.fields?.some((i: string) => field.name === i)}>
                  <GridItem>{field.displayName}</GridItem>
                </DraggableItem>
              ))}
              // actions={filteredActions.map(action => (
              //   <DraggableItem
              //     key={action}
              //     type={`ACTION_${elementId}`}
              //     id={action}
              //     disable={element.props.actions?.some((i: string) => action === i)}>
              //     <GridItem>{capitalize(action)}</GridItem>
              //   </DraggableItem>
              // ))}
              actions={[]}
              searchInput={
                <SearchInput
                  value={searchValue}
                  onChange={e => setSearchValue(e.target.value)}
                  left={<GlobalIcons.Search />}
                  placeholder="Search ..."
                />
              }>
              <NButton type="outline-3" block disabled={isDisabled}>
                Select fields / actions
              </NButton>
            </FieldAndActionSelection>
          </Widget>
          {/* TODO: replace this with a common component for sub dropzone right bar widget */}
          <Widget title="Layout">
            <NCheckbox
              title="Hide Back Button"
              checked={!!element.props.hideBackBtn}
              onChange={e => {
                configElement(elementId, { hideBackBtn: e.target.checked })
              }}
            />
            <NDivider size="md" />
            <NCheckbox
              title="Edit attachments"
              checked={!!element.props.showFileDropzones}
              onChange={e => {
                configElement(elementId, { showFileDropzones: e.target.checked })
                if (e.target.checked) {
                  configElement(elementId, { actionMetadata: undefined, inputMap: {}, fileNames: ['file0'] })
                  return
                }
                configElement(elementId, { actionMetadata: undefined, inputMap: {}, fileNames: [] })
              }}
            />
          </Widget>
          {!!element.props.showFileDropzones && <RecordDetailFileDropzoneSidebar elementId={elementId} />}
        </>
      }>
      <DroppableZone
        ref={drop}
        className={classnames([
          isDisabled && 'is-disabled',
          canDrop && 'can-drop',
          isOver && 'is-over',
          element.props.fields.length > 0 && 'has-fields',
        ])}>
        {isDisabled && <div>Record Details only support Layout Type Record Page</div>}
        {!isDisabled && (
          <>
            <ItemHeader>
              <ItemName>{element.props.title}</ItemName>
              <ItemDescription>{element.props.description}</ItemDescription>
            </ItemHeader>
            <ActionsWrapper>
              {/* {element.props.actions?.map((i: any) => (
                <ActionButton size="small" key={i}>
                  {capitalize(i)}
                  <GlobalIcons.Close
                    onClick={e => {
                      e.stopPropagation()
                      configElement(elementId, {
                        actions: (element.props.actions || []).filter((item: string) => item !== i),
                      })
                    }}
                  />
                </ActionButton>
              ))} */}
              {actions}
            </ActionsWrapper>
            {!isLoading && previewData && (
              <FieldsWrapper onSort={handleSortFields} columns={element.props.numOfCols || 1}>
                {element.props.fields.map((field: string) => {
                  const fieldData = (previewData && (previewData as any)[field]) || undefined
                  return (
                    <FieldItem
                      key={field}
                      fieldSchema={fieldsByName[field]}
                      value={fieldData}
                      field={field}
                      extraData={{ currencyCode: previewData.currencyCode, $metadata: previewData['$metadata'] }}>
                      <GlobalIcons.Close
                        className="close-icon"
                        onClick={e => {
                          e.stopPropagation()
                          configElement(elementId, {
                            fields: (element.props.fields || []).filter((item: string) => item !== field),
                          })
                        }}
                      />
                    </FieldItem>
                  )
                })}
              </FieldsWrapper>
            )}
          </>
        )}
      </DroppableZone>
      {/* TODO: replace this with a common component for sub dropzone  */}
      {!!element.props.showFileDropzones && <RecordDetailFileDropzone elementId={elementId} />}
    </ElementLayout>
  )
}

export const widgetRecordDetails = {
  icon: <ComponentIcons.RecordDetails />,
  label: 'Record Details',
  type: ElementType.Layout,
  component: 'RecordDetail',
  props: {
    isPrimary: false,
    title: '',
    description: '',
    fields: [],
    actions: [],
    columns: 2,
    // layout
    hideBackBtn: true,
    // file dropzone
    showFileDropzones: false,
    actionMetadata: undefined,
    inputMap: {},
    fileNames: [],
  },
  children: [],
}
