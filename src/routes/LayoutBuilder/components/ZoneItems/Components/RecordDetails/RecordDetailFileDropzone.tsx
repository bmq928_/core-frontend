import React, { FC, useState } from 'react'
import { useQuery } from 'react-query'
import styled from 'styled-components'
import { ActionMetadataMapInputModal } from '../../../../../../components/Builder/ActionMetadataMapInputModal'
import { color, spacing } from '../../../../../../components/GlobalStyle'
import { GlobalIcons, Icons, SidebarIcons } from '../../../../../../components/Icons'
import { NButton } from '../../../../../../components/NButton/NButton'
import { NDivider } from '../../../../../../components/NDivider'
import { SelectOption } from '../../../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../../../components/NSelect/NSingleSelect'
import { NSortableList } from '../../../../../../components/NSortableList'
import { typography } from '../../../../../../components/NTypography'
import { useSearchText } from '../../../../../../hooks/useSearchText'
import { APIServices } from '../../../../../../services/api'
import { IntegrationsQueryKeys } from '../../../../../../services/api/endpoints/integrations'
import { ActionMetadataResponse } from '../../../../../../services/api/models'
import { useLayoutAction, useLayoutBuilder } from '../../../../contexts/LayoutBuilderContext'
import { TextInput } from '../../../Inputs/TextInput'
import { Widget } from '../../../Sidebar'

const Header = styled.div`
  display: flex;
  align-items: center;
`

const HeaderTxt = styled.p`
  ${typography('s600')}
`

const PlaceholderTxt = styled.p`
  ${typography('x-small-ui-text')}
`

const FileDropzoneGrid = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: ${spacing('sm')};
`

const FileWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: ${spacing('md')};
  border: 1px solid ${color('Primary700')};
  min-height: 150px;
`

type RecordDetailFileDropzoneSidebarProps = {
  elementId: string
}

export const RecordDetailFileDropzoneSidebar: FC<RecordDetailFileDropzoneSidebarProps> = ({ elementId }) => {
  const [{ searchText, searchValue }, handleSearchChange] = useSearchText()
  // get action metadata data
  const { data, isFetching: isLoadingActionMetadata } = useQuery(
    [IntegrationsQueryKeys.getAllActionMetadata, { searchText }],
    APIServices.Integrations.getAllActionMetadata,
    {
      keepPreviousData: true,
      select: data => {
        const { selectOptions, getDataById } = data.data.data.reduce(
          (prev, cur) => {
            return {
              selectOptions: [...prev.selectOptions, { value: cur.name, label: cur.displayName }],
              getDataById: { ...prev.getDataById, [cur.name]: cur },
            }
          },
          {
            selectOptions: [] as SelectOption[],
            getDataById: {} as Record<string, ActionMetadataResponse>,
          },
        )

        return {
          actionMetadataOptions: selectOptions,
          actionMetadataById: getDataById,
        }
      },
    },
  )

  //
  const [showInputMap, setShowInputMap] = useState(false)

  const { elements } = useLayoutBuilder()
  const { configElement } = useLayoutAction()

  const element = elements[elementId]

  return (
    <>
      <Widget title="Action metadata">
        <NSingleSelect
          label="Select Action metadata"
          options={data?.actionMetadataOptions || []}
          value={element.props.actionMetadata}
          onValueChange={newValue => {
            configElement(element.id, { actionMetadata: newValue, inputMap: {} })
          }}
          fullWidth
          isSearchable
          searchValue={searchValue}
          onSearchValueChange={handleSearchChange}
          isLoading={isLoadingActionMetadata}
        />
        <NDivider size="md" />
        <NButton
          disabled={!element.props.actionMetadata}
          size="small"
          type="outline-3"
          block
          onClick={() => setShowInputMap(true)}>
          Map input
        </NButton>
        <ActionMetadataMapInputModal
          visible={showInputMap}
          setVisible={setShowInputMap}
          actionMetadata={
            element.props.actionMetadata ? data?.actionMetadataById[element.props.actionMetadata] : undefined
          }
          defaultInputMap={element.props.inputMap}
          onSubmit={newInputMap => {
            configElement(element.id, { inputMap: newInputMap })
            setShowInputMap(false)
          }}
        />
      </Widget>
      <Widget title="Files">
        <FileInputs elementId={elementId} />
      </Widget>
    </>
  )
}

type RecordDetailFileDropzoneProps = {
  elementId: string
}

export const RecordDetailFileDropzone: FC<RecordDetailFileDropzoneProps> = ({ elementId }) => {
  const { elements } = useLayoutBuilder()
  const element = elements[elementId]
  return (
    <FileDropzoneGrid>
      {element.props.fileNames.map((name: string) => {
        return (
          <FileWrapper>
            <Header>
              <SidebarIcons.AutomationFilled />
              <NDivider vertical size="md" />
              <div>
                <HeaderTxt>{name}</HeaderTxt>
              </div>
            </Header>
            <NDivider size="md" />
            <PlaceholderTxt>This is a placeholder. File Dropzone don't run in builder.</PlaceholderTxt>
          </FileWrapper>
        )
      })}
    </FileDropzoneGrid>
  )
}

const ListItem = styled('div')`
  display: flex;
  align-items: center;
  position: relative;
  cursor: move;
  &:not(:last-child) {
    margin-bottom: 2px;
  }
  & > svg {
    position: absolute;
    right: 100%;
    display: none;
  }
  &:hover > svg {
    display: block;
  }
`

const ListItemAction = styled('div')`
  ${typography('small-overline')};
  color: ${color('Primary700')};
  cursor: pointer;
  margin-left: ${spacing('xs')};
`

type FileInputsProps = {
  elementId: string
}
const FileInputs: FC<FileInputsProps> = ({ elementId }) => {
  const { elements } = useLayoutBuilder()
  const { configElement } = useLayoutAction()

  const element = elements[elementId]
  const fileNames: string[] = element.props.fileNames

  const handleSort = (fromIndex: number, toIndex: number) => {
    const newFileNames = [...fileNames]
    ;[newFileNames[toIndex], newFileNames[fromIndex]] = [newFileNames[fromIndex], newFileNames[toIndex]]
    configElement(elementId, { fileNames: newFileNames })
  }

  const handleAdd = () => {
    configElement(elementId, { fileNames: [...fileNames, `file${fileNames.length}`] })
  }

  return (
    <>
      <NSortableList onSort={handleSort}>
        {fileNames.map((name, idx) => {
          const handleChangeTitle = (e: React.ChangeEvent<HTMLInputElement>) => {
            const newFileNames = [...fileNames]
            newFileNames[idx] = e.target.value
            configElement(elementId, { fileNames: newFileNames })
          }
          const handleRemove = (e: React.MouseEvent<HTMLDivElement>) => {
            e.stopPropagation()
            const newFileNames = [...fileNames.slice(0, idx), ...fileNames.slice(idx + 1)]
            configElement(elementId, { fileNames: newFileNames })
          }
          return (
            <ListItem key={`filename_${idx}`}>
              <Icons.Reorder />
              <TextInput type="text" value={name} onChange={handleChangeTitle} />
              {fileNames.length > 1 && <ListItemAction onClick={handleRemove}>Delete</ListItemAction>}
            </ListItem>
          )
        })}
      </NSortableList>
      <NDivider size="sm" />
      <NButton size="small" type="primary" icon={<GlobalIcons.Plus />} onClick={handleAdd} />
    </>
  )
}
