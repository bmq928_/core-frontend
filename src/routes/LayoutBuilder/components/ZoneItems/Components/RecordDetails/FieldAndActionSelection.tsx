import * as React from 'react'
import styled from 'styled-components'
import { color, spacing } from '../../../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../../../components/Icons'
import { NButton, NButtonProps } from '../../../../../../components/NButton/NButton'
import { NDivider } from '../../../../../../components/NDivider'
import { NTabs } from '../../../../../../components/NTabs/NTabs'
import { NTextInputProps } from '../../../../../../components/NTextInput/NTextInput'
import { BottomBar } from '../../../BottomBar'

const Tab = styled(NTabs.Tab)`
  width: 100%;
  height: 200px;
  margin-top: ${spacing('xs')};
  overflow-y: auto;
`

const Grid = styled('div')`
  display: grid;
  grid-template: 1fr / repeat(5, 1fr);
  grid-gap: ${spacing('xs')};
`

export const GridItem = styled('div')`
  padding: ${spacing('sm')} ${spacing('md')};
  border: 1px solid ${color('Neutral400')};
  border-radius: 3px;
  cursor: move;
  &:not(.is-disable):hover {
    border-color: ${color('Neutral900')};
  }
  &.is-dragging {
    opacity: 0.6;
  }
  &.is-disable {
    cursor: not-allowed;
    opacity: 0.6;
  }
`

type FieldAndActionSelectionProps = {
  actions: React.ReactElement[]
  fields: React.ReactElement[]
  children: React.ReactElement<NButtonProps>
  searchInput: React.ReactElement<NTextInputProps>
  visible: boolean
  setVisible: React.Dispatch<React.SetStateAction<boolean>>
}

export function FieldAndActionSelection({
  actions,
  children,
  searchInput,
  fields,
  visible,
  setVisible,
}: FieldAndActionSelectionProps) {
  const button = React.cloneElement(children, {
    onClick() {
      setVisible(prev => !prev)
    },
  })

  return (
    <>
      {button}
      <BottomBar visible={visible}>
        <NTabs
          leftElement={
            <>
              <NButton icon={<GlobalIcons.Close />} type="ghost" size="small" onClick={() => setVisible(false)} />
              <NDivider vertical size="xs" />
            </>
          }
          rightElement={
            <>
              <NDivider vertical size="xs" />
              {searchInput}
            </>
          }>
          <Tab title="Fields">
            <Grid>{fields}</Grid>
          </Tab>
          <Tab title="Actions">
            <Grid>{actions}</Grid>
          </Tab>
        </NTabs>
      </BottomBar>
    </>
  )
}
