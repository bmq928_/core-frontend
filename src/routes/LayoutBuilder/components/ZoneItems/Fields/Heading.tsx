import * as React from 'react'
import styled from 'styled-components'
import { useLayoutAction, useLayoutBuilder } from '../../../contexts/LayoutBuilderContext'
import { ElementType, ZoneProps } from '../../../utils/models'
import { FieldIcons } from '../../Icons'
import { Select } from '../../Inputs/Select'
import { TextInput } from '../../Inputs/TextInput'
import { Widget } from '../../Sidebar'
import ElementLayout from '../ElementLayout'

const Wrapper = styled(ElementLayout)`
  min-height: unset;
`

const H = styled('h1')``

export function Heading({ elementId, parentId, index, ...restProps }: ZoneProps) {
  const { elements } = useLayoutBuilder()
  const { configElement } = useLayoutAction()
  const element = elements[elementId]
  return (
    <Wrapper
      {...restProps}
      label="Title"
      elementId={elementId}
      parentId={parentId}
      index={index}
      sidebar={
        <React.Fragment>
          <Widget title="Level">
            <Select value={element.props.level} onChange={level => configElement(elementId, { level })}>
              <Select.Option value="h1">H1</Select.Option>
              <Select.Option value="h2">H2</Select.Option>
              <Select.Option value="h3">H3</Select.Option>
              <Select.Option value="h4">H4</Select.Option>
              <Select.Option value="h5">H5</Select.Option>
            </Select>
          </Widget>
          <Widget title="Title">
            <TextInput
              type="text"
              value={element.props.children}
              onChange={e => {
                configElement(elementId, { children: e.target.value })
              }}
            />
          </Widget>
        </React.Fragment>
      }>
      <H as={element.props.level}>{element.props.children}</H>
    </Wrapper>
  )
}

export const widgetHeading = {
  icon: <FieldIcons.Title />,
  label: 'Title',
  type: ElementType.Field,
  component: 'Heading',
  props: {
    level: 'h1',
    children: 'Heading',
  },
  children: [],
}
