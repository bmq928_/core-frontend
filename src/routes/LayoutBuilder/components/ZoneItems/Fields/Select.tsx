import * as React from 'react'
import { useLayoutBuilder } from '../../../contexts/LayoutBuilderContext'
import { ElementType, ZoneProps } from '../../../utils/models'
import { FieldIcons } from '../../Icons'
import { UnderConstruction } from '../../UnderConstruction'
import { ZoneItemLayout } from '../ZoneItemLayout'

export function Select({ elementId }: ZoneProps) {
  const { selectedElement } = useLayoutBuilder()
  return (
    <ZoneItemLayout shouldRenderSidebar={selectedElement === elementId} sidebar={`Select - ${elementId}`}>
      <UnderConstruction />
    </ZoneItemLayout>
  )
}

export const widgetSelect = {
  icon: <FieldIcons.Dropdown />,
  label: 'Select',
  type: ElementType.Field,
  component: 'Select',
  props: {},
  children: [],
}
