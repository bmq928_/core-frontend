import * as React from 'react'
import { classnames } from '../../../../../common/classnames'
import { NCheckbox } from '../../../../../components/NCheckbox/NCheckbox'
import { NRadio } from '../../../../../components/NRadio/NRadio'
import { noop } from '../../../../../utils/utils'
import { useLayoutAction, useLayoutBuilder } from '../../../contexts/LayoutBuilderContext'
import { useElement } from '../../../hooks/useElement'
import { ElementType, ZoneProps } from '../../../utils/models'
import { FieldIcons } from '../../Icons'
import { TextInput } from '../../Inputs/TextInput'
import { Widget } from '../../Sidebar'
import { FieldWrapper as Wrapper, Placeholder } from '../StyledElements'
import { ZoneItemLayout } from '../ZoneItemLayout'

export function Radio({ elementId, parentId, index }: ZoneProps) {
  const { elements, selectedElement } = useLayoutBuilder()
  const { selectElement, configElement } = useLayoutAction()
  const element = elements[elementId]
  const [{ isMouseOver, mousePosition }, ref] = useElement({ elementId, parentId, index })
  const isSelected = selectedElement === elementId
  return (
    <ZoneItemLayout
      shouldRenderSidebar={isSelected}
      sidebar={
        <React.Fragment>
          <Widget title="Name">
            <TextInput
              type="text"
              value={element.props.name}
              onChange={e => {
                configElement(elementId, { name: e.target.value })
              }}
            />
          </Widget>
          <Widget title="Value">
            <TextInput
              type="text"
              value={element.props.value}
              onChange={e => {
                configElement(elementId, { value: e.target.value })
              }}
            />
          </Widget>
          <Widget title="Title">
            <TextInput
              type="text"
              value={element.props.title}
              onChange={e => {
                configElement(elementId, { title: e.target.value })
              }}
            />
          </Widget>
          <Widget title="Subtitle">
            <TextInput
              type="text"
              value={element.props.subtitle}
              onChange={e => {
                configElement(elementId, { subtitle: e.target.value })
              }}
            />
          </Widget>
          <Widget>
            <NCheckbox
              title="Default Checked"
              checked={element.props.defaultChecked}
              onChange={e => {
                configElement(elementId, { defaultChecked: e.target.checked })
              }}
            />
          </Widget>
        </React.Fragment>
      }>
      <Wrapper
        ref={ref}
        className={classnames([
          isMouseOver && 'is-mouse-over',
          mousePosition && `is-sort ${mousePosition}`,
          isSelected && 'is-selected',
        ])}
        onClick={e => {
          e.stopPropagation()
          selectElement(elementId)
        }}>
        <NRadio {...element.props} onChange={noop} />
        <Placeholder mousePosition={mousePosition} />
      </Wrapper>
    </ZoneItemLayout>
  )
}

export const widgetRadio = {
  icon: <FieldIcons.Radio />,
  label: 'Radio',
  type: ElementType.Field,
  component: 'Radio',
  props: {
    title: 'Radio',
    subtitle: '',
    value: 'radio1',
    name: 'radio',
    defaultChecked: false,
  },
  children: [],
}
