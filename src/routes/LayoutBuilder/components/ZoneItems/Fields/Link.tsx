import * as React from 'react'
import { classnames } from '../../../../../common/classnames'
import { NCheckbox } from '../../../../../components/NCheckbox/NCheckbox'
import { NLink } from '../../../../../components/NLink/NLink'
import { useLayoutAction, useLayoutBuilder } from '../../../contexts/LayoutBuilderContext'
import { useElement } from '../../../hooks/useElement'
import { ElementType, ZoneProps } from '../../../utils/models'
import { FieldIcons } from '../../Icons'
import { TextInput } from '../../Inputs/TextInput'
import { Widget } from '../../Sidebar'
import { FieldWrapper as Wrapper, Placeholder } from '../StyledElements'
import { ZoneItemLayout } from '../ZoneItemLayout'

export function Link({ elementId, parentId, index }: ZoneProps) {
  const { elements, selectedElement } = useLayoutBuilder()
  const { selectElement, configElement } = useLayoutAction()
  const element = elements[elementId]
  const [{ isMouseOver, mousePosition }, ref] = useElement({ elementId, parentId, index })
  const isSelected = selectedElement === elementId
  return (
    <ZoneItemLayout
      shouldRenderSidebar={selectedElement === elementId}
      sidebar={
        <React.Fragment>
          <Widget title="Title">
            <TextInput
              type="text"
              value={element.props.title || element.props.pathname}
              onChange={e => {
                configElement(elementId, { title: e.target.value })
              }}
            />
          </Widget>
          <Widget title="href">
            <TextInput
              type="text"
              value={element.props.pathname}
              onChange={e => {
                configElement(elementId, { pathname: e.target.value })
              }}
            />
          </Widget>
          <Widget>
            <NCheckbox
              checked={element.props.target === '_blank'}
              onChange={e => {
                if (e.target.checked) {
                  configElement(elementId, { target: '_blank' })
                  return
                }
                configElement(elementId, { target: '_self' })
              }}
              title="Open in new tab/window"
            />
          </Widget>
        </React.Fragment>
      }>
      <Wrapper
        ref={ref}
        className={classnames([
          isMouseOver && 'is-mouse-over',
          mousePosition && `is-sort ${mousePosition}`,
          isSelected && 'is-selected',
        ])}
        onClick={e => {
          e.stopPropagation()
          selectElement(elementId)
        }}>
        <NLink to={element.props.pathname} target={element.props.target} rel="noreferrer noopener">
          {element.props.title || element.props.pathname}
        </NLink>
        <Placeholder mousePosition={mousePosition} />
      </Wrapper>
    </ZoneItemLayout>
  )
}

export const widgetLink = {
  icon: <FieldIcons.Link />,
  label: 'Link',
  type: ElementType.Field,
  component: 'Link',
  props: {
    title: "Nuclent's Homepage",
    pathname: 'https://nuclent.com',
    target: '_blank',
  },
  children: [],
}
