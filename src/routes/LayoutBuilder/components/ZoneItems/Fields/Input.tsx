import * as React from 'react'
import { classnames } from '../../../../../common/classnames'
import { NTextInput } from '../../../../../components/NTextInput/NTextInput'
import { useLayoutAction, useLayoutBuilder } from '../../../contexts/LayoutBuilderContext'
import { useElement } from '../../../hooks/useElement'
import { ElementType, ZoneProps } from '../../../utils/models'
import { FieldIcons } from '../../Icons'
import { TextInput } from '../../Inputs/TextInput'
import { Widget } from '../../Sidebar'
import { FieldWrapper as Wrapper, Placeholder } from '../StyledElements'
import { ZoneItemLayout } from '../ZoneItemLayout'

export function Input({ elementId, parentId, index }: ZoneProps) {
  const { elements, selectedElement } = useLayoutBuilder()
  const { selectElement, configElement } = useLayoutAction()
  const [{ isMouseOver, mousePosition }, ref] = useElement({ elementId, parentId, index })
  const element = elements[elementId]
  const isSelected = selectedElement === elementId
  return (
    <ZoneItemLayout
      shouldRenderSidebar={isSelected}
      sidebar={
        <React.Fragment>
          <Widget title="Label">
            <TextInput
              type="text"
              value={element.props.label}
              onChange={e => {
                configElement(elementId, { label: e.target.value })
              }}
            />
          </Widget>
          <Widget title="Placeholder">
            <TextInput
              type="text"
              value={element.props.placeholder}
              onChange={e => {
                configElement(elementId, { placeholder: e.target.value })
              }}
            />
          </Widget>
          <Widget title="Caption">
            <TextInput
              type="text"
              value={element.props.caption}
              onChange={e => {
                configElement(elementId, { caption: e.target.value })
              }}
            />
          </Widget>
        </React.Fragment>
      }>
      <Wrapper
        ref={ref}
        className={classnames([
          isMouseOver && 'is-mouse-over',
          mousePosition && `is-sort ${mousePosition}`,
          isSelected && 'is-selected',
        ])}
        onClick={e => {
          e.stopPropagation()
          selectElement(elementId)
        }}>
        <NTextInput disabled {...element.props} />
        <Placeholder mousePosition={mousePosition} />
      </Wrapper>
    </ZoneItemLayout>
  )
}

export const widgetInput = {
  icon: <FieldIcons.Input />,
  label: 'Input',
  type: ElementType.Field,
  component: 'Input',
  props: {
    label: 'Text Input',
  },
  children: [],
}
