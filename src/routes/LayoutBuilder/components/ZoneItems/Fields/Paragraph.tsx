import * as React from 'react'
import styled from 'styled-components'
import { useLayoutAction, useLayoutBuilder } from '../../../contexts/LayoutBuilderContext'
import { ElementType, ZoneProps } from '../../../utils/models'
import { FieldIcons } from '../../Icons'
import { TextArea } from '../../Inputs/TextArea'
import { Widget } from '../../Sidebar'
import ElementLayout from '../ElementLayout'

const Wrapper = styled(ElementLayout)`
  min-height: unset;
`

const P = styled('div')``

export function Paragraph({ elementId, parentId, index, ...restProps }: ZoneProps) {
  const { elements } = useLayoutBuilder()
  const { configElement } = useLayoutAction()
  const element = elements[elementId]
  return (
    <Wrapper
      {...restProps}
      label="Paragraph"
      elementId={elementId}
      parentId={parentId}
      index={index}
      sidebar={
        <React.Fragment>
          <Widget title="Content">
            <TextArea
              value={element.props.children}
              onChange={e => {
                configElement(elementId, { children: e.target.value })
              }}
            />
          </Widget>
        </React.Fragment>
      }>
      <P>{element.props.children}</P>
    </Wrapper>
  )
}

export const widgetParagraph = {
  icon: <FieldIcons.Paragraph />,
  label: 'Paragraph',
  type: ElementType.Field,
  component: 'Paragraph',
  props: {
    children: 'Paragraph',
  },
  children: [],
}
