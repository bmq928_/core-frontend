import styled from 'styled-components'
import { color, spacing } from '../../../../components/GlobalStyle'
import { typography } from '../../../../components/NTypography'
import { MousePosition } from '../../utils/models'

export const Placeholder = styled('div').attrs((props: { mousePosition: MousePosition | null }) => {
  switch (props.mousePosition) {
    case MousePosition.Top:
      return { className: 'element-placeholder', style: { top: -8, height: 2, left: 0, right: 0 } }
    case MousePosition.Right:
      return { className: 'element-placeholder', style: { right: -8, width: 2, top: 0, bottom: 0 } }
    case MousePosition.Bottom:
      return { className: 'element-placeholder', style: { bottom: -8, height: 2, left: 0, right: 0 } }
    case MousePosition.Left:
      return { className: 'element-placeholder', style: { left: -8, width: 2, top: 0, bottom: 0 } }
    default:
      return { className: 'element-placeholder', style: {} }
  }
})<{ mousePosition: MousePosition | null }>`
  position: absolute;
  background: ${color('Primary700')};
  display: none;
  z-index: 9999;
  pointer-events: none;
`
export const LayoutWrapper = styled('div')`
  display: flex;
  align-items: flex-start;
  background: ${color('white')};
  position: relative;
  min-height: 200px;
  padding: ${spacing('md')};
  box-shadow: 0px 1px 2px rgba(13, 24, 38, 0.24), 0px 8px 18px rgba(13, 24, 38, 0.05);
  border-radius: 3px;
  &.is-over:not(.is-sort) {
    background: ${color('Primary200')};
  }
  &:after {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    pointer-events: none;
    border: 1px solid ${color('Neutral200')};
  }
  &.is-selected:after,
  &.is-mouse-over:after {
    border-style: dashed;
    border-color: ${color('Primary700')};
  }
  &.is-sort {
    ${Placeholder} {
      display: block;
    }
  }
  & > *:not(.elemnet-placeholder):not(.element-actions) {
    flex: 1;
  }
`

export const FieldWrapper = styled(LayoutWrapper)`
  min-height: unset;
  width: 100%;
`

export const Actions = styled('div')`
  position: absolute;
  top: 0;
  left: 0;
  background: ${color('Primary700')};
  z-index: 9999;
  display: none;
  padding: 4px 8px;
  color: ${color('white')};
`

export const EmptyMessage = styled('div')`
  text-align: center;
  color: ${color('Neutral400')};
  ${typography('x-small-ui-text')}
`
