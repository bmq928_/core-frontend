import * as React from 'react'
import styled, { css } from 'styled-components'
import { spacing } from '../../../../../components/GlobalStyle'
import { NCheckbox } from '../../../../../components/NCheckbox/NCheckbox'
import { useLayoutAction, useLayoutBuilder } from '../../../contexts/LayoutBuilderContext'
import { ElementType, ZoneProps } from '../../../utils/models'
import { LayoutIcons } from '../../Icons'
import { TextInput } from '../../Inputs/TextInput'
import { Widget } from '../../Sidebar'
import ElementLayout from '../ElementLayout'
import { EmptyMessage as BaseEmptyMessage } from '../StyledElements'
import { ZONE_ELEMENTS } from '../zoneItems'

export const Wrapper = styled('div')<{ direction: 'row' | 'column' }>`
  display: flex;
  width: 100%;
  flex-direction: ${props => props.direction || 'row'};
  & > *:not(:last-child) {
    ${props =>
      props.direction === 'row'
        ? css`
            margin-right: ${spacing('md')};
          `
        : css`
            margin-bottom: ${spacing('md')};
          `}
  }
`

const EmptyMessage = styled(BaseEmptyMessage)`
  border-radius: 3px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px dashed #999;
`

export function Section({ elementId, parentId, index, ...restProps }: ZoneProps) {
  const { elements } = useLayoutBuilder()
  const { configElement } = useLayoutAction()

  const element = elements[elementId]
  const children = element.children || []

  const direction = element.props.columns?.length > 1 ? 'row' : 'column'

  const canDrop =
    element.props.columns && element.props.columns.length > 1 ? children.length < element.props.columns.length : true

  return (
    <ElementLayout
      {...restProps}
      label="Section"
      elementId={elementId}
      parentId={parentId}
      index={index}
      canDrop={canDrop}
      sidebar={
        <React.Fragment>
          <Widget title="Columns">
            <ColumnInput values={element.props.columns} onChange={columns => configElement(elementId, { columns })} />
          </Widget>
          <Widget title="Style">
            <NCheckbox
              title="Show background"
              checked={element.props.showBackground}
              onChange={e => {
                configElement(elementId, { showBackground: e.target.checked })
              }}
            />
          </Widget>
        </React.Fragment>
      }>
      <Wrapper direction={direction}>
        {children.map((id, idx) => {
          const Component = ZONE_ELEMENTS[elements[id].component]
          return (
            <div key={id} style={{ flex: element.props.columns[idx] }}>
              <Component elementId={id} parentId={elementId} index={idx} />
            </div>
          )
        })}
        {element.props.columns?.length > 0 &&
          Array.from({ length: element.props.columns.length - children.length }).map((_, idx) => (
            <EmptyMessage key={`empty-${idx}`} style={{ flex: element.props.columns[children.length + idx] || 1 }}>
              Drop an item here to start
            </EmptyMessage>
          ))}
      </Wrapper>
    </ElementLayout>
  )
}

function ColumnInput({
  values,
  onChange,
}: {
  values: (number | string)[]
  onChange(values?: (number | string)[]): void
}) {
  const MAX_COLUMN = 4
  return (
    <>
      <TextInput
        type="number"
        min="1"
        max={MAX_COLUMN}
        value={values?.length || ''}
        onChange={e => {
          if (e.target.value) {
            const newColumns = Math.max(0, Math.min(MAX_COLUMN, +e.target.value))
            if (values) {
              if (values.length < newColumns) {
                onChange([...values, ...Array.from({ length: newColumns - values.length }, () => 1)])
                return
              }
              onChange(values.slice(0, newColumns))
              return
            }
            onChange(Array.from({ length: newColumns }, () => 1))
            return
          }
          onChange(undefined)
        }}
        onBlur={e => {
          if (!e.target.value) {
            onChange([1])
          }
        }}
      />
      {values && values.length > 1 && (
        <div
          style={{
            marginTop: 2,
            display: 'grid',
            gridTemplate: `1fr / repeat(${values.length}, 1fr)`,
            gridGap: 4,
          }}>
          {values.map((value, index) => (
            <TextInput
              type="number"
              value={value}
              onChange={e => {
                const newValues = [...values]
                newValues[index] = (e.target.value && +e.target.value) || ''
                onChange(newValues)
              }}
              onBlur={e => {
                if (!e.target.value) {
                  const newValues = [...values]
                  newValues[index] = 1
                  onChange(newValues)
                }
              }}
              key={`i-${index}`}
            />
          ))}
        </div>
      )}
    </>
  )
}

export const widgetSection = {
  icon: <LayoutIcons.Section />,
  label: 'Section',
  type: ElementType.Layout,
  component: 'Section',
  props: {
    direction: 'row',
    columns: [1],
    showBackground: false,
  },
  children: [],
}
