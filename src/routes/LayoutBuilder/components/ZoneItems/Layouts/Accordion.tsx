import * as React from 'react'
import styled from 'styled-components'
import { classnames } from '../../../../../common/classnames'
import { color, spacing } from '../../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../../components/Icons'
import { typography } from '../../../../../components/NTypography'
import { useLayoutAction, useLayoutBuilder } from '../../../contexts/LayoutBuilderContext'
import { useElement } from '../../../hooks/useElement'
import { useElementRenderer } from '../../../hooks/useElementRenderer'
import { ElementType, ZoneProps } from '../../../utils/models'
import { LayoutIcons } from '../../Icons'
import { ZoneItemLayout } from '../ZoneItemLayout'

const Wrapper = styled('div')`
  position: relative;
  &.is-over {
    background: ${color('Primary200')};
  }
`

const Header = styled('div')`
  display: flex;
  justify-content: space-between;
  background: ${color('Primary700')};
  height: 32px;
  align-items: center;
  color: ${color('white')};
  padding: 0 ${spacing('sm')} 0 ${spacing('md')};
  & > div {
    &:first-child {
      ${typography('button')};
    }
  }
`

const Content = styled('div')`
  background: ${color('white')};
  position: relative;
  width: 100%;
  min-height: 200px;
  &:after {
    position: absolute;
    content: '';
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    border: 1px solid ${color('Neutral200')};
    pointer-events: none;
  }
`

export function Accordion({ elementId, parentId, index, canDrag = true, canDrop = true }: ZoneProps) {
  const { elements, selectedElement } = useLayoutBuilder()
  const { selectElement } = useLayoutAction()
  const element = elements[elementId]

  const [show, setShow] = React.useState(true)

  const [{ isOver, isMouseOver, mousePosition, isDragging }, ref] = useElement({
    elementId,
    parentId,
    index,
    canDrop: show && canDrop,
    canDrag,
  })
  const children = useElementRenderer(elementId, { canDrop: !isDragging })

  const isSelected = selectedElement === elementId
  return (
    <ZoneItemLayout
      shouldRenderSidebar={isSelected}
      sidebar={
        <React.Fragment>
          <div>Accordion - {element.id}</div>
        </React.Fragment>
      }>
      <Wrapper
        onClick={e => {
          e.stopPropagation()
          selectElement(elementId)
        }}
        className={classnames([
          isOver && 'is-over',
          isMouseOver && 'is-mouse-over',
          mousePosition && `is-sort ${mousePosition}`,
          isSelected && 'is-selected',
        ])}
        ref={ref}>
        <Header
          onClick={e => {
            e.stopPropagation()
            setShow(prev => !prev)
          }}>
          <div className="header-title">Accordion</div>
          {show ? <GlobalIcons.Minus /> : <GlobalIcons.Plus />}
        </Header>
        {show && <Content>{children}</Content>}
      </Wrapper>
    </ZoneItemLayout>
  )
}

export const widgetAccordion = {
  icon: <LayoutIcons.Accordion />,
  label: 'Accordion',
  type: ElementType.Layout,
  component: 'Accordion',
  props: {},
  children: [],
}
