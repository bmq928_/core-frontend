import * as React from 'react'
import { useLayoutBuilder } from '../../../contexts/LayoutBuilderContext'
import { ElementType, ZoneProps } from '../../../utils/models'
import { LayoutIcons } from '../../Icons'
import { UnderConstruction } from '../../UnderConstruction'
import { ZoneItemLayout } from '../ZoneItemLayout'

export function Divider({ elementId }: ZoneProps) {
  const { selectedElement } = useLayoutBuilder()
  return (
    <ZoneItemLayout shouldRenderSidebar={selectedElement === elementId} sidebar={`Divider - ${elementId}`}>
      <UnderConstruction />
    </ZoneItemLayout>
  )
}

export const widgetDivider = {
  icon: <LayoutIcons.Divider />,
  label: 'Divider',
  type: ElementType.Layout,
  component: 'Divider',
  props: {},
  children: [],
}
