import { LayoutResponse } from '../../../../services/api/models'
import { Header, widgetHeader } from '../../demo/Header'
import { ElementType, ZoneProps } from '../../utils/models'
import { Attachments, widgetAttachments } from './Components/Attachments'
import { Button, widgetButton } from './Components/Button'
import { Flow, widgetFlow } from './Components/Flow'
import { ObjectList, widgetObjectList } from './Components/ObjectList/ObjectList'
import { RelatedList, widgetRelatedList } from './Components/ObjectList/RelatedList'
import { RecordDetails as RecordDetail, widgetRecordDetails } from './Components/RecordDetails/RecordDetails'
import { Checkbox, widgetCheckbox } from './Fields/Checkbox'
import { Heading, widgetHeading } from './Fields/Heading'
import { Input, widgetInput } from './Fields/Input'
import { Link, widgetLink } from './Fields/Link'
import { Paragraph, widgetParagraph } from './Fields/Paragraph'
import { Radio, widgetRadio } from './Fields/Radio'
import { Accordion, widgetAccordion } from './Layouts/Accordion'
import { Section, widgetSection } from './Layouts/Section'
import { Tabs, widgetTabs } from './Layouts/Tabs'

type Widget = {
  icon: JSX.Element
  label: string
  component: string
  type: ElementType
  props: Record<string, any>
  children?: string[]
}

export const GROUPS = ['Layout', 'Basic', 'Component'] as const
export type GroupName = typeof GROUPS[number]

export function getWidgetByGroups(layoutType?: LayoutResponse['type']): Record<GroupName, Widget[]> {
  if (!layoutType) {
    return {
      Layout: [],
      Basic: [],
      Component: [],
    }
  }
  if (layoutType === 'app-page') {
    return {
      Layout: [widgetSection, widgetTabs],
      Basic: [widgetHeading, widgetParagraph],
      Component: [widgetObjectList, widgetFlow, widgetButton, widgetHeader],
    }
  }
  return {
    Layout: [widgetSection, widgetTabs],
    Basic: [widgetHeading, widgetParagraph],
    Component: [
      widgetObjectList,
      widgetRecordDetails,
      widgetRelatedList,
      widgetFlow,
      widgetButton,
      widgetHeader,
      widgetAttachments,
    ],
  }
}

export const ZONE_ITEMS = {
  Section: widgetSection,
  Tabs: widgetTabs,
  Accordion: widgetAccordion,
  Input: widgetInput,
  Heading: widgetHeading,
  Paragraph: widgetParagraph,
  Checkbox: widgetCheckbox,
  Link: widgetLink,
  Radio: widgetRadio,
  ObjectList: widgetObjectList,
  RecordDetail: widgetRecordDetails,
  Flow: widgetFlow,
  RelatedList: widgetRelatedList,
  Button: widgetButton,
  DemoHeader: widgetHeader,
  Attachments: widgetAttachments,
} as Record<string, any>

export const ZONE_ELEMENTS = {
  Section,
  Accordion,
  Tabs,
  Input,
  Heading,
  Paragraph,
  Button,
  Checkbox,
  Link,
  Radio,
  Flow,
  ObjectList,
  RecordDetail,
  RelatedList,
  DemoHeader: Header,
  Attachments,
} as Record<string, (props: ZoneProps) => JSX.Element>
