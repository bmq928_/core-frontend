import * as React from 'react'
import styled from 'styled-components'
import { color, spacing } from '../../../../components/GlobalStyle'
import { Icons } from '../../../../components/Icons'
import { elevation } from '../../../../components/NElevation'
import { NPerfectScrollbar } from '../../../../components/NPerfectScrollbar'
import { NTypography } from '../../../../components/NTypography'
import { useClickOutside } from '../../../../hooks/useClickOutside'

const Wrapper = styled('div')`
  position: relative;
`
const ValueWrapper = styled('div')`
  cursor: pointer;
  display: flex;
  align-items: center;
  height: 32px;
  border-radius: 2px;
  &.is-focused,
  &:hover {
    color: ${color('Primary700')};
  }
  svg {
    margin-left: ${spacing('xs')};
    transition: transform 0.4s ease-in-out;
  }
  &.is-focused {
    svg {
      transform: rotate(180deg);
    }
  }
`
const DropdownWrapper = styled('div')`
  padding-top: ${spacing('xxs')};
  padding-bottom: ${spacing('xxs')};
  border-radius: 3px;
  position: absolute;
  top: 100%;
  left: 0;
  width: 180px;
  ${elevation('Elevation300')};
  background: ${color('Neutral900')};
  z-index: 100;
`

const OptionsWrapper = styled('div')`
  position: relative;
  max-height: 200px;
`

const OptionWrapper = styled(NTypography.SmallText)`
  color: ${color('white')};
  cursor: pointer;
  padding: ${spacing('xs')} ${spacing('sm')};
  &:hover,
  &.is-selected {
    background: ${color('Primary700')};
  }
  &:not(:last-child) {
    margin-bottom: 1px;
  }
`

type SelectProps = {
  children: ReturnType<typeof Option>[]
  value: string
  onChange(value: string): void
  placeholder?: string
}

export function Select({ children, value, onChange, placeholder }: SelectProps) {
  const dropdownRef = React.useRef<HTMLDivElement>(null)
  const [show, setShow] = React.useState(false)

  const arrChild = React.Children.toArray(children) as JSX.Element[]

  const { options, selected } = arrChild.reduce(
    (accumulator, child) => {
      // @ts-ignore
      if (React.isValidElement(child) && child.type.displayName === Option.displayName) {
        const props = child.props as OptionProps
        const item = (
          <OptionWrapper
            key={props.value}
            className={value === props.value ? 'is-selected' : undefined}
            onClick={() => {
              if (value !== props.value) {
                onChange(props.value)
              }
              setShow(false)
            }}>
            {props.children}
          </OptionWrapper>
        )
        return {
          options: [...accumulator.options, item],
          selected: value === props.value ? props.children : accumulator.selected,
        } as { options: JSX.Element[]; selected?: JSX.Element }
      }
      return accumulator
    },
    { options: [], selected: undefined } as { options: JSX.Element[]; selected?: JSX.Element },
  )

  useClickOutside(() => setShow(false), dropdownRef)
  return (
    <Wrapper>
      <ValueWrapper className={show ? 'is-focused' : undefined} onClick={() => setShow(true)}>
        {selected || placeholder}
        <Icons.Down />
      </ValueWrapper>
      {show && (
        <DropdownWrapper ref={dropdownRef}>
          <NPerfectScrollbar>
            <OptionsWrapper>{options}</OptionsWrapper>
          </NPerfectScrollbar>
        </DropdownWrapper>
      )}
    </Wrapper>
  )
}

Select.Option = Option

type OptionProps = {
  value: string
  children: React.ReactNode
  className?: string
}

function Option({ className, children }: OptionProps) {
  return <div className={className}>{children}</div>
}
Option.displayName = 'SelectOption'
