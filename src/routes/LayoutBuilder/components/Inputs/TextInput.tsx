import * as React from 'react'
import styled from 'styled-components'
import { color, spacing } from '../../../../components/GlobalStyle'
import { typography } from '../../../../components/NTypography'

const Wrapper = styled('label')`
  display: flex;
  align-items: center;
  span {
    ${typography('x-small-ui-text')}
    display: inline-block;
  }
  & > * {
    flex: 1;
  }
`

const Input = styled('input')`
  border: 1px solid ${color('Neutral200')};
  padding-left: ${spacing('sm')};
  padding-right: ${spacing('sm')};
  height: 32px;
  width: 100%;
  border-radius: 2px;
  &:focus {
    border-color: ${color('Primary700')};
  }
`

type TextInputProps = { label?: string } & React.InputHTMLAttributes<HTMLInputElement>

export function TextInput({ label, ...inputProps }: TextInputProps) {
  return (
    <Wrapper>
      {label && <span>{label}</span>}
      <Input {...inputProps} />
    </Wrapper>
  )
}
