import * as React from 'react'
import styled from 'styled-components'
import { color } from '../../../../components/GlobalStyle'
import { elevation } from '../../../../components/NElevation'
import { useClickOutside } from '../../../../hooks/useClickOutside'
import { UNITS } from '../../utils/builder'
import { SpacingUnit } from '../../utils/models'

export type SpacingValue = {
  value: string
  unit: SpacingUnit
}

const Wrapper = styled('div')`
  display: grid;
  grid-template: repeat(3, 1fr) / repeat(4, 1fr);
  grid-template-areas:
    '. top top .'
    'left left right right'
    '. bottom bottom .';
  grid-gap: 3px;
  & > div {
    display: flex;
    &:nth-child(1) {
      grid-area: top;
    }
    &:nth-child(2) {
      grid-area: left;
    }
    &:nth-child(3) {
      grid-area: right;
    }
    &:nth-child(4) {
      grid-area: bottom;
    }
  }
`

export type SpacingValues = Record<'top' | 'right' | 'bottom' | 'left', SpacingValue>

type SpacingInputProps = {
  values: SpacingValues
  onChangeValue(value: SpacingValues): void
}

export function SpacingInputs({ values, onChangeValue }: SpacingInputProps) {
  return (
    <Wrapper>
      <SpacingInput value={values.top} onChangeValue={top => onChangeValue({ ...values, top })} />
      <SpacingInput value={values.left} onChangeValue={left => onChangeValue({ ...values, left })} />
      <SpacingInput value={values.right} onChangeValue={right => onChangeValue({ ...values, right })} />
      <SpacingInput value={values.bottom} onChangeValue={bottom => onChangeValue({ ...values, bottom })} />
    </Wrapper>
  )
}

const SpacingInputWrapper = styled('div')`
  display: flex;
  border: 1px solid ${color('Neutral200')};
  border-radius: 2px;
  &:hover,
  &:focus-within {
    border-color: ${color('Primary700')};
  }
  input {
    width: 48px;
    border: none;
    flex: 1;
    padding: 0 4px;
    height: 100%;
  }
`

// MIN, MAX, STEP are hard coded for now
// If have any case need custom this value,
// remove them after convert to default props
const MAX = 100
const MIN = -MAX
const STEP = 1

// TODO: handle e character
export function SpacingInput({
  value,
  onChangeValue,
}: {
  value: SpacingValue
  onChangeValue(value: SpacingValue): void
}) {
  const { value: spacing, unit } = value
  return (
    <SpacingInputWrapper>
      <input
        min={MIN}
        max={MAX}
        step={STEP}
        type="number"
        value={`${spacing}`}
        onChange={e => onChangeValue({ value: e.target.value, unit })}
        placeholder="0"
      />
      <SelectUnit value={unit} onChangeValue={unit => onChangeValue({ unit, value: spacing })} />
    </SpacingInputWrapper>
  )
}

const SelectUnitWrapper = styled('div')`
  position: relative;
  background: ${color('Neutral200')};
`

const UnitWrapper = styled('div')`
  width: 32px;
  height: 32px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  color: ${color('Neutral700')};
`

const DropdownWrapper = styled('div')`
  position: absolute;
  top: calc(100% + 2px);
  right: 0;
  display: flex;
  z-index: 10;
  ${elevation('Elevation300')}
  border-radius: 3px;
  overflow: hidden;
`

const Option = styled(UnitWrapper)`
  color: ${color('white')};
  background: ${color('Neutral900')};
  &:hover {
    background: ${color('Neutral700')};
  }
  &.is-selected {
    background: ${color('Primary700')};
  }
`

type SelectUnitProps = {
  value: SpacingUnit
  onChangeValue(value: SpacingUnit): void
}

function SelectUnit({ value, onChangeValue }: SelectUnitProps) {
  const [show, setShow] = React.useState(false)
  const containerRef = React.useRef<HTMLDivElement>(null)

  useClickOutside(() => setShow(false), containerRef)
  return (
    <SelectUnitWrapper ref={containerRef}>
      <UnitWrapper onClick={() => setShow(true)}>{value}</UnitWrapper>
      {show && (
        <DropdownWrapper>
          {UNITS.map(i => (
            <Option
              className={i === value ? 'is-selected' : undefined}
              key={i}
              onClick={() => {
                onChangeValue(i)
                setShow(false)
              }}>
              {i}
            </Option>
          ))}
        </DropdownWrapper>
      )}
    </SelectUnitWrapper>
  )
}
