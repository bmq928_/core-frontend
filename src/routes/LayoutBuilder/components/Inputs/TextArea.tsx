import * as React from 'react'
import styled from 'styled-components'
import { color, spacing } from '../../../../components/GlobalStyle'

const Input = styled('textarea')`
  padding: ${spacing('sm')};
  border: 1px solid ${color('Neutral200')};
  width: 100%;
  border-radius: 2px;
  &:focus {
    border-color: ${color('Primary700')};
  }
`

type TextAreaProps = {} & React.TextareaHTMLAttributes<HTMLTextAreaElement>

export function TextArea(props: TextAreaProps) {
  return <Input {...props} />
}
