import { uniqBy } from 'lodash'
import { FC, useEffect, useState } from 'react'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { useParams } from 'react-router-dom'
import styled, { useTheme } from 'styled-components'
import { color, spacing } from '../../../components/GlobalStyle'
import { GlobalIcons } from '../../../components/Icons'
import { NButton } from '../../../components/NButton/NButton'
import { NCheckbox } from '../../../components/NCheckbox/NCheckbox'
import { NDivider } from '../../../components/NDivider'
import { NColumn, NRow } from '../../../components/NGrid/NGrid'
import { NModal } from '../../../components/NModal/NModal'
import { NPerfectScrollbar } from '../../../components/NPerfectScrollbar'
import { NSpinner } from '../../../components/NSpinner/NSpinner'
import { NTextInput } from '../../../components/NTextInput/NTextInput'
import { NToast } from '../../../components/NToast'
import { typography } from '../../../components/NTypography'
import { useSearchText } from '../../../hooks/useSearchText'
import { APIServices } from '../../../services/api'
import { BuilderQueryKey } from '../../../services/api/endpoints/builder'
import { ProfilesQueryKey } from '../../../services/api/endpoints/profiles'
import {
  AppLayoutResponse,
  ApplicationResponse,
  AppProfileDto,
  ProfileLayoutResponse,
  ProfileResponse,
} from '../../../services/api/models'
import { noop } from '../../../utils/utils'
import { AppPickList } from './AppPickList'

const Wrapper = styled.div`
  padding: ${spacing('xl')};
`

const ProfileWrapper = styled('div')`
  border: 1px solid ${color('Neutral200')};
  border-radius: 3px;
`

const ProfileItem = styled('div')`
  display: flex;
  padding: ${spacing('sm')} ${spacing('md')};
  cursor: pointer;
  &:hover {
    background: ${color('Neutral100')};
  }
`

const ProfileItemName = styled('div')`
  flex: 1;
  margin-left: ${spacing('md')};
  ${typography('small-bold-text')}
`

const PlaceholderWrapper = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: ${spacing('xl')};
`

type Props = {
  setVisible: (visible: boolean) => void
  visible: boolean
  onBack: () => void
}

export const AppsProfilesSelectModal: FC<Props> = ({ visible, setVisible, onBack }) => {
  const [selectedApps, setSelectedApps] = useState<AppLayoutResponse[]>([])
  const [selectedProfiles, setSelectedProfiles] = useState<ProfileLayoutResponse[]>([])
  const theme = useTheme()
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText()
  const params = useParams<{ layoutId: string }>()
  const queryClient = useQueryClient()

  const { data: profilesData, isFetching: isLoadingProfiles } = useQuery(
    [
      ProfilesQueryKey.getProfiles,
      {
        searchText: searchText,
      },
    ],
    APIServices.Profiles.getProfiles,
  )

  const { mutate: assignLayoutAppsProfiles, isLoading: isAssigning } = useMutation(
    APIServices.Builder.assignLayoutAppsProfiles,
    {
      onSuccess: () => {
        queryClient.invalidateQueries([BuilderQueryKey.getLayoutAppsProfiles, { guid: params.layoutId }])
        onBack()
      },
      onError: error => {
        NToast.error({
          title: 'Assign unsuccessful',
          subtitle: error.response?.data.message,
        })
      },
    },
  )

  const { data: layoutAppsProfiles } = useQuery(
    [BuilderQueryKey.getLayoutAppsProfiles, { id: params.layoutId }],
    APIServices.Builder.getLayoutAppsProfiles,
  )

  useEffect(() => {
    if (layoutAppsProfiles?.data) {
      //remove duplicate app, profile
      setSelectedApps(
        uniqBy(
          layoutAppsProfiles?.data.map(appPro => ({ name: appPro.application.name, id: appPro.application.id })),
          'id',
        ),
      )
      setSelectedProfiles(
        uniqBy(
          layoutAppsProfiles?.data.map(appPro => ({ name: appPro.profile.name, id: appPro.profile.id })),
          'id',
        ),
      )
    }
  }, [layoutAppsProfiles?.data])

  const handleChangeApp = (app: ApplicationResponse) => {
    if (selectedApps.some(select => select.id === app.id)) {
      setSelectedApps(prev => prev.filter(i => i.id !== app.id))
      return
    }
    setSelectedApps(prev => [...prev, { id: app.id, name: app.name }])
  }

  const handleChangeProfile = (profile: ProfileResponse) => {
    if (selectedProfiles.some(select => select.id === profile.id)) {
      setSelectedProfiles(prev => prev.filter(i => i.id !== profile.id))
      return
    }
    setSelectedProfiles(prev => [...prev, { id: profile.id, name: profile.name }])
  }

  const publish = () => {
    const appProfileInputs: AppProfileDto[] = []
    selectedApps.forEach(app => {
      selectedProfiles.forEach(profile => {
        appProfileInputs.push({
          appId: app.id,
          profileId: profile.id,
        })
      })
    })
    assignLayoutAppsProfiles({ id: params.layoutId, input: appProfileInputs })
  }

  return (
    <NModal visible={visible} setVisible={setVisible} size="x-large">
      <NModal.Header onBack={onBack} title={'Select apps and profiles'} onClose={() => setVisible(false)} />
      <NModal.Body>
        <Wrapper>
          <NRow>
            <NColumn>
              <AppPickList selected={selectedApps} handleChange={handleChangeApp} />
            </NColumn>
            <NDivider size="sm" vertical />
            <NColumn>
              <NTextInput
                left={<GlobalIcons.Search />}
                placeholder="Search for profile"
                value={searchValue}
                onChange={e => {
                  handleSearchChange(e.target.value)
                }}
              />
              <NDivider size="xs" />
              <ProfileWrapper>
                {isLoadingProfiles ? (
                  <PlaceholderWrapper>
                    <NSpinner size={20} strokeWidth={2} color={color('Primary900')({ theme })} />
                  </PlaceholderWrapper>
                ) : (
                  <NPerfectScrollbar style={{ maxHeight: 400 }}>
                    {profilesData?.data.data && profilesData?.data.data.length > 0 ? (
                      profilesData.data.data.map(profile => (
                        <ProfileItem
                          key={profile.id}
                          onClick={() => {
                            handleChangeProfile(profile)
                          }}>
                          <NCheckbox
                            value={profile.id}
                            checked={selectedProfiles.some(i => i.id === profile.id)}
                            onChange={noop}
                            onClick={e => e.stopPropagation()}
                          />
                          <ProfileItemName>{profile.name}</ProfileItemName>
                        </ProfileItem>
                      ))
                    ) : (
                      <PlaceholderWrapper>No profile found</PlaceholderWrapper>
                    )}
                  </NPerfectScrollbar>
                )}
              </ProfileWrapper>
            </NColumn>
          </NRow>
        </Wrapper>
      </NModal.Body>
      <NModal.Footer>
        <NButton size="small" type="ghost" onClick={() => setVisible(false)}>
          Cancel
        </NButton>
        <NButton loading={isAssigning} onClick={publish} style={{ width: '160px' }} size="small" type="primary">
          Assign & Publish
        </NButton>
      </NModal.Footer>
    </NModal>
  )
}
