import * as React from 'react'
import { FC } from 'react'
import { useMutation, useQueryClient } from 'react-query'
import { useParams } from 'react-router-dom'
import styled from 'styled-components'
import { color, spacing } from '../../../components/GlobalStyle'
import { GlobalIcons } from '../../../components/Icons'
import { NButton } from '../../../components/NButton/NButton'
import { NDivider } from '../../../components/NDivider'
import { NModal, NModalProps } from '../../../components/NModal/NModal'
import { NPagination } from '../../../components/NPagination'
import { NPerfectScrollbar } from '../../../components/NPerfectScrollbar'
import { NTable, NTableColumnType } from '../../../components/NTable/NTable'
import { NToast } from '../../../components/NToast'
import { typography } from '../../../components/NTypography'
import { APIServices } from '../../../services/api'
import { BuilderQueryKey } from '../../../services/api/endpoints/builder'
import { ApplicationResponse } from '../../../services/api/models'

const BodyWrapper = styled('div')`
  padding: ${spacing('xl')};
`

const WarningBanner = styled.div`
  display: flex;
  height: 56px;
  background: ${color('Neutral100')};
  border-radius: 4px;
  padding-left: ${spacing('md')};
  padding-right: ${spacing('md')};
  align-items: center;
`

const WarningText = styled.p`
  ${typography('x-small-ui-text')}
  color: ${color('Neutral400')};
`
const AssignBtn = styled(NButton)`
  width: 160px;
`

type Props = {
  onBack: () => void
  selectedApps: ApplicationResponse[]
} & NModalProps

//TODO: Add data to this when doing NCL-897 cuz I dunno what data looks like now

export const RecordPageAppConfirmModal: FC<Props> = ({ visible, setVisible, onBack, selectedApps }) => {
  const queryClient = useQueryClient()
  const params = useParams<{ layoutId: string }>()

  const { mutate: assignPageToApps, isLoading } = useMutation(APIServices.Builder.assignAppPageToApps, {
    onSuccess: async () => {
      await queryClient.invalidateQueries([BuilderQueryKey.getLayoutApps, { id: params.layoutId }])
      handleClose()
    },
    onError: error => {
      NToast.error({
        title: 'Assign unsuccessful',
        subtitle: error.response?.data.message,
      })
    },
  })
  const handleClose = () => {
    if (setVisible) {
      setVisible(false)
    }
  }
  const columns = React.useMemo<NTableColumnType<any>>(
    () => [
      {
        Header: 'Application',
        accessor: 'name',
      },
      {
        Header: 'Current Assigned',
      },
    ],
    [],
  )

  const handlePublish = () => {
    assignPageToApps({ id: params.layoutId, appIds: selectedApps.map(app => app.id) })
  }

  return (
    <NModal visible={visible} setVisible={setVisible} size="large">
      <NModal.Header onBack={onBack} title="App Default Confirmation" onClose={handleClose} />
      <NModal.Body>
        <BodyWrapper>
          <NPerfectScrollbar style={{ maxHeight: 300 }}>
            <NTable
              data={selectedApps}
              columns={columns}
              pagination={<NPagination total={selectedApps.length} current={1} onChangePage={() => {}} />}
            />
          </NPerfectScrollbar>
          <NDivider size="xl" />
          <WarningBanner>
            <GlobalIcons.Info />
            <NDivider vertical size="md" />
            <WarningText>Current Assign will be overwrite</WarningText>
          </WarningBanner>
        </BodyWrapper>
      </NModal.Body>
      <NModal.Footer>
        <NButton size="small" type="ghost" onClick={handleClose}>
          Cancel
        </NButton>
        <AssignBtn onClick={handlePublish} loading={isLoading} size="small" type="primary">
          Assign & Publish
        </AssignBtn>
      </NModal.Footer>
    </NModal>
  )
}
