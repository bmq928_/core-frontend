import * as React from 'react'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { useParams } from 'react-router-dom'
import styled from 'styled-components'
import { color, spacing } from '../../../components/GlobalStyle'
import { NButton } from '../../../components/NButton/NButton'
import { NDivider } from '../../../components/NDivider'
import { NModal, NModalProps } from '../../../components/NModal/NModal'
import { NToast } from '../../../components/NToast'
import { typography } from '../../../components/NTypography'
import { APIServices } from '../../../services/api'
import { BuilderQueryKey } from '../../../services/api/endpoints/builder'
import { ApplicationResponse } from '../../../services/api/models'
import { AppPickList } from './AppPickList'

const BodyWrapper = styled('div')`
  padding: ${spacing('xl')};
`

const Description = styled.p`
  ${typography('small-bold-text')}
  color: ${color('Neutral400')};
`

type PublishAppPageModalProps = {
  name: string
} & NModalProps

export function PublishAppPageModal({ visible, setVisible, name }: PublishAppPageModalProps) {
  const [selected, setSelected] = React.useState<ApplicationResponse[]>([])
  const queryClient = useQueryClient()
  const params = useParams<{ layoutId: string }>()

  const { data: layoutApps } = useQuery(
    [BuilderQueryKey.getLayoutApps, { id: params.layoutId }],
    APIServices.Builder.getLayoutApps,
  )

  const { mutate: assignAppPageToApps, isLoading } = useMutation(APIServices.Builder.assignAppPageToApps, {
    onSuccess: async () => {
      await queryClient.invalidateQueries([BuilderQueryKey.getLayoutApps, { id: params.layoutId }])
      handleClose()
    },
    onError: error => {
      NToast.error({ title: 'Assign unsuccessful', subtitle: error.response?.data.message })
    },
  })

  React.useEffect(() => {
    if (layoutApps?.data) {
      setSelected(layoutApps?.data)
    }
  }, [layoutApps])

  const handleClose = () => {
    if (setVisible) {
      setVisible(false)
    }
  }

  const handlePublish = () => {
    assignAppPageToApps({ id: params.layoutId, appIds: selected.map(app => app.id) })
  }

  const handleChange = (app: ApplicationResponse) => {
    if (selected.some(select => select.id === app.id)) {
      setSelected(prev => prev.filter(i => i.id !== app.id))
      return
    }
    setSelected(prev => [...prev, app])
  }

  return (
    <NModal visible={visible} setVisible={setVisible} size="large">
      <NModal.Header title={`Publish: ${name}`} onClose={handleClose} />
      <NModal.Body>
        <BodyWrapper>
          <Description>Select app to assign to {name}</Description>
          <NDivider size="xl" />
          <AppPickList selected={selected} handleChange={handleChange} />
        </BodyWrapper>
      </NModal.Body>
      <NModal.Footer>
        <NButton size="small" type="ghost" onClick={handleClose}>
          Cancel
        </NButton>
        <NButton style={{ width: '160px' }} size="small" type="primary" onClick={handlePublish} loading={isLoading}>
          Assign & Publish
        </NButton>
      </NModal.Footer>
    </NModal>
  )
}
