import * as React from 'react'
import styled from 'styled-components'
import { color, spacing } from '../../../components/GlobalStyle'
import { RequiredIndicator, typography } from '../../../components/NTypography'

const Wrapper = styled('div')`
  border-bottom: 1px solid ${color('Neutral200')};
`

const WidgetHeading = styled('div')`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 40px;
  padding: 0 ${spacing('md')};
  ${typography('small-overline')}
  text-transform: uppercase;
`

const WidgetContent = styled('div')<{ hasTitle: boolean }>`
  padding: ${spacing('md')};
  padding-top: ${props => (props.hasTitle ? 0 : spacing('md')(props))};
`

type WidgetProps = {
  required?: boolean
  title?: string
  children: React.ReactNode
  rightElement?: React.ReactNode
}

export const Widget = React.forwardRef<HTMLDivElement, WidgetProps>(function Widget(
  { required, title, children, rightElement },
  ref,
) {
  return (
    <Wrapper ref={ref}>
      {title && (
        <WidgetHeading>
          <span>
            {required && <RequiredIndicator>*</RequiredIndicator>}
            {title}
          </span>
          {rightElement}
        </WidgetHeading>
      )}
      <WidgetContent hasTitle={title !== undefined}>{children}</WidgetContent>
    </Wrapper>
  )
})
