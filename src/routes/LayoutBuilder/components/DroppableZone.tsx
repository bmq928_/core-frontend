import * as React from 'react'
import styled, { useTheme } from 'styled-components'
import { classnames } from '../../../common/classnames'
import { color, spacing } from '../../../components/GlobalStyle'
import { NSpinner } from '../../../components/NSpinner/NSpinner'
import { useLayoutAction, useLayoutBuilder } from '../contexts/LayoutBuilderContext'
import { useElement } from '../hooks/useElement'
import { useElementRenderer } from '../hooks/useElementRenderer'
import { ROOT_ELEMENT } from '../utils/builder'

const Wrapper = styled('div').attrs(props => {
  if (props.className?.includes('is-over')) {
    return {
      style: {
        background: color('Primary200')(props),
      },
    }
  }
})`
  height: 100%;
  overflow-y: auto;
  overflow-x: hidden;
  padding: 32px ${spacing('md')};
  position: relative;
  background: ${color('transparent')};
  &.is-empty {
    display: flex;
    justify-content: center;
    align-items: center;
  }
  &.is-sort.move-left {
    background: #999;
  }
  & > *:not(:last-child) {
    margin-bottom: ${spacing('md')};
  }
`

const LoadingWrapper = styled.div`
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`

type DroppableZoneProps = {}

export function DroppableZone({}: DroppableZoneProps) {
  const { isLoading } = useLayoutBuilder()
  const theme = useTheme()
  const [{ isOver, mousePosition }, ref] = useElement({
    elementId: ROOT_ELEMENT,
    parentId: ROOT_ELEMENT,
    index: 0,
    canDrag: false,
    canDrop: !isLoading,
  })
  const children = useElementRenderer(ROOT_ELEMENT)
  const { elements } = useLayoutBuilder()
  const { selectElement } = useLayoutAction()
  const element = elements[ROOT_ELEMENT]
  const isEmpty = (element.children || []).length <= 0

  return (
    <Wrapper
      className={classnames([isEmpty && 'is-empty', isOver && 'is-over', mousePosition && `is-sort ${mousePosition}`])}
      ref={ref}
      onClick={() => selectElement(ROOT_ELEMENT)}>
      {isLoading && (
        <LoadingWrapper onClick={() => selectElement(ROOT_ELEMENT)}>
          <NSpinner size={20} strokeWidth={4} color={color('Primary700')({ theme })} />
        </LoadingWrapper>
      )}
      {!isLoading && (
        <>
          {isEmpty && <div>Select a layout to start</div>}
          {!isEmpty && children}
        </>
      )}
    </Wrapper>
  )
}
