import { FC } from 'react'
import { useQuery } from 'react-query'
import styled, { useTheme } from 'styled-components'
import { color, spacing } from '../../../components/GlobalStyle'
import { GlobalIcons } from '../../../components/Icons'
import { NCheckbox } from '../../../components/NCheckbox/NCheckbox'
import { NDivider } from '../../../components/NDivider'
import { NPerfectScrollbar } from '../../../components/NPerfectScrollbar'
import { NSpinner } from '../../../components/NSpinner/NSpinner'
import { NTextInput } from '../../../components/NTextInput/NTextInput'
import { typography } from '../../../components/NTypography'
import { useSearchText } from '../../../hooks/useSearchText'
import { APIServices } from '../../../services/api'
import { BuilderQueryKey } from '../../../services/api/endpoints/builder'
import { noop } from '../../../utils/utils'

const ApplicationWrapper = styled('div')`
  border: 1px solid ${color('Neutral200')};
  border-radius: 3px;
`

const ApplicationItem = styled('div')`
  display: flex;
  padding: ${spacing('sm')} ${spacing('md')};
  cursor: pointer;
  &:hover {
    background: ${color('Neutral100')};
  }
`

const ApplicationItemName = styled('div')`
  flex: 1;

  margin-left: ${spacing('md')};
  ${typography('small-bold-text')}
`

const PlaceholderWrapper = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: ${spacing('xl')};
`

type AppPickListProps = {
  selected: any[]
  handleChange: (app: any) => void
}

export const AppPickList: FC<AppPickListProps> = ({ selected, handleChange }) => {
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText()
  const theme = useTheme()

  const { data: appsData, isFetching: isLoadingAppsData } = useQuery(
    [
      BuilderQueryKey.getApps,
      {
        searchText: searchText,
      },
    ],
    APIServices.Builder.getApps,
  )

  return (
    <>
      <NTextInput
        left={<GlobalIcons.Search />}
        placeholder="Search for application"
        value={searchValue}
        onChange={e => {
          handleSearchChange(e.target.value)
        }}
      />
      <NDivider size="xs" />
      <ApplicationWrapper>
        {isLoadingAppsData ? (
          <PlaceholderWrapper>
            <NSpinner size={20} strokeWidth={2} color={color('Primary900')({ theme })} />
          </PlaceholderWrapper>
        ) : (
          <NPerfectScrollbar style={{ maxHeight: 300 }}>
            {appsData?.data.data && appsData?.data.data.length > 0 ? (
              appsData.data.data.map(app => (
                <ApplicationItem
                  key={app.id}
                  onClick={() => {
                    handleChange(app)
                  }}>
                  <NCheckbox
                    value={app.id}
                    checked={selected.some(i => i.id === app.id)}
                    onChange={noop}
                    onClick={e => e.stopPropagation()}
                  />
                  <ApplicationItemName>{app.name}</ApplicationItemName>
                </ApplicationItem>
              ))
            ) : (
              <PlaceholderWrapper>No application found</PlaceholderWrapper>
            )}
          </NPerfectScrollbar>
        )}
      </ApplicationWrapper>
    </>
  )
}
