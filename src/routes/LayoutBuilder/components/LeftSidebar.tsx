import * as React from 'react'
import { useDrag } from 'react-dnd'
import styled from 'styled-components'
import { Item, ItemLabel, Widget, Wrapper } from '../../../components/Builder/LeftSidebar'
import { NSpinner } from '../../../components/NSpinner/NSpinner'
import { useLayoutBuilder } from '../contexts/LayoutBuilderContext'
import { LEFT_SIDEBAR } from '../utils/builder'
import { getWidgetByGroups, GROUPS } from './ZoneItems/zoneItems'

const LoadingWrapper = styled('div')`
  display: flex;
  min-height: 100vh;
  justify-content: center;
  align-items: center;
`

type LeftSidebarProps = {}

export function LeftSidebar({}: LeftSidebarProps) {
  const { isLoading, layoutDetails } = useLayoutBuilder()
  const WIDGET_GROUPS = getWidgetByGroups(layoutDetails?.type)
  return (
    <Wrapper>
      {isLoading && (
        <LoadingWrapper>
          <NSpinner size={16} strokeWidth={2} />
        </LoadingWrapper>
      )}
      {!isLoading &&
        GROUPS.map(group => (
          <Widget title={group} key={group}>
            {WIDGET_GROUPS[group].map(widget => (
              <DraggableItem key={widget.label} item={widget}>
                {widget.icon}
                <ItemLabel>{widget.label}</ItemLabel>
              </DraggableItem>
            ))}
          </Widget>
        ))}
    </Wrapper>
  )
}

type DraggableItemProps = {
  item: any
  children: React.ReactNode
}

function DraggableItem({ item, children }: DraggableItemProps) {
  const [, ref] = useDrag({
    type: item.type,
    item: {
      item,
      parentId: LEFT_SIDEBAR,
    },
    options: { dropEffect: 'copy' },
  })
  return <Item ref={ref}>{children}</Item>
}
