import * as React from 'react'
import { useMutation, useQuery } from 'react-query'
import { useParams } from 'react-router-dom'
import styled, { useTheme } from 'styled-components'
import { color, spacing } from '../../../components/GlobalStyle'
import { GlobalIcons } from '../../../components/Icons'
import { NButton } from '../../../components/NButton/NButton'
import { NDivider } from '../../../components/NDivider'
import { NModal, NModalProps } from '../../../components/NModal/NModal'
import { NPerfectScrollbar } from '../../../components/NPerfectScrollbar'
import { NSpinner } from '../../../components/NSpinner/NSpinner'
import { NTable, NTableColumnType } from '../../../components/NTable/NTable'
import { NTabs } from '../../../components/NTabs/NTabs'
import { NToast } from '../../../components/NToast'
import { typography } from '../../../components/NTypography'
import { APIServices } from '../../../services/api'
import { BuilderQueryKey } from '../../../services/api/endpoints/builder'
import { ApplicationResponse, AppProfileLayoutResponse } from '../../../services/api/models'
import { AppPickList } from './AppPickList'

const BodyWrapper = styled('div')`
  padding: ${spacing('xl')};
`

const TabWrapper = styled.div`
  height: 100%;
  margin-left: ${spacing('lg')};
  .tabbar-underline {
    bottom: -4px;
  }
  .tab-list {
    height: 40px;
  }
`

const Description = styled.p`
  ${typography('small-bold-text')}
  color: ${color('Neutral400')};
`

const AssignBtn = styled(NButton)`
  width: 100%;
`

const WarningBanner = styled.div`
  display: flex;
  height: 56px;
  background: ${color('Neutral100')};
  border-radius: 4px;
  padding-left: ${spacing('md')};
  padding-right: ${spacing('md')};
  align-items: center;
`

const WarningText = styled.p`
  ${typography('x-small-ui-text')}
  color: ${color('Neutral400')};
`

const LoadingContainer = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
`

const COLUMNS: NTableColumnType<AppProfileLayoutResponse> = [
  {
    Header: 'Application',
    accessor: 'application',
    Cell: ({ value }) => {
      return value.name
    },
  },
  {
    Header: 'Profile',
    accessor: 'profile',
    Cell: ({ value }) => {
      return value.name
    },
  },
]

type PublishRecordPageModalProps = {
  name: string
  onAssignToApp: (apps: ApplicationResponse[]) => void
  onAssignToAppsProfiles: () => void
} & NModalProps

export function PublishRecordPageModal({
  visible,
  setVisible,
  name,
  onAssignToApp,
  onAssignToAppsProfiles,
}: PublishRecordPageModalProps) {
  const [selected, setSelected] = React.useState<ApplicationResponse[]>([])
  const params = useParams<{ layoutId: string }>()
  const [activeTab, setActiveTab] = React.useState<number>(0)
  const theme = useTheme()

  const { data: layoutApps } = useQuery(
    [BuilderQueryKey.getLayoutApps, { id: params.layoutId }],
    APIServices.Builder.getLayoutApps,
  )

  const { data: layoutAppsProfiles, isLoading: isLoadingAppsProfiles } = useQuery(
    [BuilderQueryKey.getLayoutAppsProfiles, { id: params.layoutId }],
    APIServices.Builder.getLayoutAppsProfiles,
  )

  const { mutate: assignToOrg, isLoading: isSettingDefault } = useMutation(APIServices.Builder.assignDefaultRecord, {
    onError: error => {
      NToast.error({
        title: 'Assign unsuccessful',
        subtitle: error.response?.data.message,
      })
    },
  })

  React.useEffect(() => {
    if (layoutApps?.data) {
      setSelected(layoutApps?.data)
    }
  }, [layoutApps])

  const handleClose = () => {
    if (setVisible) {
      setVisible(false)
    }
  }

  const handleChange = (app: ApplicationResponse) => {
    if (selected.some(select => select.id === app.id)) {
      setSelected(prev => prev.filter(i => i.id !== app.id))
      return
    }
    setSelected(prev => [...prev, app])
  }

  const handleAssignToOrg = () => {
    assignToOrg({ id: params.layoutId })
  }

  const handleAssignToApp = () => {
    onAssignToApp(selected)
  }

  return (
    <NModal visible={visible} setVisible={setVisible} size="large">
      <NModal.Header title={`Publish: ${name}`} onClose={handleClose}>
        <TabWrapper>
          <NTabs active={activeTab} onChangeTab={setActiveTab}>
            <NTabs.Tab title="App Default" />
            <NTabs.Tab title="Org Default" />
            <NTabs.Tab title="Apps and Profiles" />
          </NTabs>
        </TabWrapper>
      </NModal.Header>
      <NModal.Body>
        <BodyWrapper>
          {activeTab === 0 && (
            <>
              <Description>Select app to assign to {name}.</Description>
              <NDivider size="xl" />
              <AppPickList selected={selected} handleChange={handleChange} />
              <NDivider size="xl" />
              <AssignBtn onClick={handleAssignToApp} type="outline">
                Assign as App
              </AssignBtn>
            </>
          )}
          {activeTab === 1 && (
            <>
              <Description>
                Set this page as the org default it for all “Object” record, except when app default or app, record
                type, or profile specific assignment are defined.
              </Description>
              <NDivider size="xl" />
              <AssignBtn onClick={handleAssignToOrg} type="outline" loading={isSettingDefault}>
                Assign as Organization Default
              </AssignBtn>
              <NDivider size="xl" />
              <WarningBanner>
                <GlobalIcons.Info />
                <NDivider vertical size="md" />
                <WarningText>When assign as Org Default you can unassign this</WarningText>
              </WarningBanner>
            </>
          )}
          {activeTab === 2 && (
            <>
              <Description>
                Set a combination of apps and profiles to display this custom record page. This setting is the most
                specific and allows for fine-grained customization.
              </Description>
              <NDivider size="md" />
              {isLoadingAppsProfiles && (
                <LoadingContainer>
                  <NSpinner size={20} strokeWidth={2} color={color('Primary900')({ theme })} />
                </LoadingContainer>
              )}
              <AssignBtn onClick={onAssignToAppsProfiles} type="outline">
                Assign to Applications and Profiles
              </AssignBtn>
              {layoutAppsProfiles?.data && layoutAppsProfiles.data.length > 0 && (
                <>
                  <NDivider size="md" />
                  <NPerfectScrollbar style={{ maxHeight: 300 }}>
                    <NTable data={layoutAppsProfiles?.data || []} columns={COLUMNS} />
                  </NPerfectScrollbar>
                </>
              )}
            </>
          )}
        </BodyWrapper>
      </NModal.Body>
      <NModal.Footer>
        <NButton size="small" type="ghost" onClick={handleClose}>
          Cancel
        </NButton>
      </NModal.Footer>
    </NModal>
  )
}
