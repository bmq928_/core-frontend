import { get } from 'lodash'
import { FilterItem } from '../../../services/api/models'
import { SpacingValues } from '../components/Inputs/SpacingInput'
import { InputMap, SpacingUnit } from './models'

export const ROOT_ELEMENT = 'BUILDER_ROOT_ELEMENT'
export const LEFT_SIDEBAR = 'BUILDER_LEFT_SIDEBAR'
export const UNITS = ['px', 'em', 'rem', '%'] as SpacingUnit[]

export function getSpacing(values: SpacingValues, prefix: string = 'margin', defaultValue: string = '0px') {
  if (!values) {
    return {}
  }
  const top = `${prefix}Top`
  const right = `${prefix}Right`
  const bottom = `${prefix}Bottom`
  const left = `${prefix}Left`
  return {
    [top]: (values.top.value && `calc(${values.top.value}${values.top.unit} + ${defaultValue})`) || defaultValue,
    [right]:
      (values.right.value && `calc(${values.right.value}${values.right.unit} + ${defaultValue})`) || defaultValue,
    [bottom]:
      (values.bottom.value && `calc(${values.bottom.value}${values.bottom.unit} + ${defaultValue})`) || defaultValue,
    [left]: (values.left.value && `calc(${values.left.value}${values.left.unit} + ${defaultValue})`) || defaultValue,
  }
}

export function resolveFilterVariable(filters: FilterItem[][] = [], inputMap: InputMap) {
  return filters.map(filter =>
    filter.map(f => {
      const value = f.value as any
      const isVariable = value && value.includes('$')
      return {
        ...f,
        value: isVariable ? get(inputMap, value.replace('$', ''), null) : value,
      }
    }),
  )
}
