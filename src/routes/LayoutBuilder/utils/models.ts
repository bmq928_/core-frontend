import { SessionState } from '../../../redux/slices/sessionSlice'
import { DataResponse, ObjectSchemaResponse } from '../../../services/api/models'

export enum LayoutActionType {
  AddElement = 'LAYOUT_ADD_ELEMENT',
  UpdateElement = 'LAYOUT_UPDATE_ELEMENT',
  RemoveElement = 'LAYOUT_REMOVE_ELEMENT',
  SortElement = 'LAYOUT_SORT_ELEMENT',

  SelectElement = 'LAYOUT_SELECT_ELEMENT',
  MoveElement = 'LAYOUT_MOVE_ELEMENT',
}

export enum ElementType {
  Layout = 'ELEMENT_LAYOUT',
  Field = 'ELEMENT_FIELD',
  Component = 'ELEMENT_COMPONENT',
}

export type Element = {
  id: string
  label: string
  type: ElementType
  component: string
  props: Record<string, any>
  children: string[]
  // contentFieldName?: string
  schema?: ObjectSchemaResponse
}

export type DraggableElement = {
  elementId?: string
  type: ElementType
  item?: Omit<Element, 'id'> & { id?: string }
  parentId: string
  index: number
}

export type ZoneProps = {
  elementId: string
  parentId: string
  index: number
  canDrag?: boolean | ((item: DraggableElement) => boolean)
  canDrop?: boolean | ((item: DraggableElement) => boolean)
  canDuplicate?: boolean
  canRemove?: boolean
  onRemove?: () => void
}

export type SpacingUnit = 'px' | 'em' | 'rem' | '%'

// Use Element
export enum MousePosition {
  Top = 'move-top',
  Right = 'move-right',
  Bottom = 'move-bottom',
  Left = 'move-left',
}

export enum Direction {
  Vertical,
  Horizontal,
}

export type InputMap = {
  curUser?: SessionState['user']
  curRecord?: DataResponse
  now: string
}
