import { UnderConstruction } from '../components/UnderConstruction'
import { ZONE_ELEMENTS } from '../components/ZoneItems/zoneItems'
import { useLayoutBuilder } from '../contexts/LayoutBuilderContext'
import { DraggableElement } from '../utils/models'

export function useElementRenderer(
  elementId?: string,
  options?: {
    canDrag?: boolean | ((item: DraggableElement) => boolean)
    canDrop?: boolean | ((item: DraggableElement) => boolean)
  },
) {
  const { elements } = useLayoutBuilder()
  if (!elementId) {
    return null
  }
  const element = elements[elementId]
  if (!element.children || element.children.length < 0) {
    return null
  }
  return element.children.map((id, index) => {
    const Component = ZONE_ELEMENTS[elements[id].component]
    if (Component) {
      return <Component key={id} elementId={id} parentId={elementId} index={index} {...options} />
    }
    return <UnderConstruction key={id}>{element.label}</UnderConstruction>
  })
}
