import throttle from 'lodash/throttle'
import { nanoid } from 'nanoid'
import * as React from 'react'
import { DropTargetMonitor, useDrag, useDrop } from 'react-dnd'
import { hasValue } from '../../../utils/utils'
import { ZONE_ITEMS } from '../components/ZoneItems/zoneItems'
import { useLayoutAction, useLayoutBuilder } from '../contexts/LayoutBuilderContext'
import { LEFT_SIDEBAR } from '../utils/builder'
import { Direction, DraggableElement, Element, ElementType, MousePosition, ZoneProps } from '../utils/models'

type Rect = { top: number; left: number; right: number; bottom: number }
type MouseOffset = { x: number; y: number }

const ACTION_VERTICAL_SIZE = 32
const ACTION_HORIZONTAL_SIZE = 32

function calculateMousePosition(elementRect: Rect, mouseOffset: MouseOffset, direction: Direction) {
  const actionsRect = {
    top: elementRect.top + ACTION_VERTICAL_SIZE,
    right: elementRect.right - ACTION_HORIZONTAL_SIZE,
    bottom: elementRect.bottom - ACTION_VERTICAL_SIZE,
    left: elementRect.left + ACTION_HORIZONTAL_SIZE,
  }

  // last mouseOffsetX <= elementRect.right - 1
  // last mouseOffsetY <= elementRect.bottom - 1
  const { x: mouseOffsetX, y: mouseOffsetY } = mouseOffset

  // move-top || move-bottom
  if (direction === Direction.Vertical) {
    const isInZone = mouseOffsetX > elementRect.left && mouseOffsetX <= elementRect.right - 1
    // move-top
    if (mouseOffsetY > elementRect.top && mouseOffsetY < actionsRect.top && isInZone) {
      return MousePosition.Top
    }
    // move-bottom
    if (mouseOffsetY <= elementRect.bottom - 1 && mouseOffsetY > actionsRect.bottom && isInZone) {
      return MousePosition.Bottom
    }
  }
  if (direction === Direction.Horizontal) {
    const isInZone = mouseOffsetY > elementRect.top && mouseOffsetY <= elementRect.bottom - 1
    // move-left
    if (mouseOffsetX > elementRect.left && mouseOffsetX < actionsRect.left && isInZone) {
      return MousePosition.Left
    }
    // move-right
    if (mouseOffsetX <= elementRect.right - 1 && mouseOffsetX > actionsRect.right && isInZone) {
      return MousePosition.Right
    }
  }
  return null
}

type ReturnUseElement = [
  { isOver: boolean; isDragging: boolean; isMouseOver: boolean; mousePosition: MousePosition | null },
  React.RefObject<HTMLDivElement>,
]

export function useElement({
  elementId,
  parentId,
  index,
  canDrag = true,
  canDrop = true,
}: ZoneProps): ReturnUseElement {
  const elementRef = React.useRef<HTMLDivElement>(null)

  const [mousePosition, setMousePosition] = React.useState<MousePosition | null>(null)
  const [mouseOver, setMouseOver] = React.useState(false)

  React.useEffect(() => {
    if (elementRef.current) {
      const el = elementRef.current
      const handleMouseOver = (e: MouseEvent) => {
        e.stopPropagation()
        setMouseOver(true)
      }
      const handleMouseOut = () => setMouseOver(false)
      el.addEventListener('mouseover', handleMouseOver)
      el.addEventListener('mouseout', handleMouseOut)
      return () => {
        el.removeEventListener('mouseover', handleMouseOver)
        el.removeEventListener('mouseout', handleMouseOut)
      }
    }
  }, [])

  const { elements } = useLayoutBuilder()
  const { addElement, moveElement } = useLayoutAction()

  const element = elements[elementId]
  const parent = elements[parentId]

  const [{ isDragging }, drag] = useDrag({
    type: element.type || ElementType.Layout,
    item: { parentId, elementId, index },
    canDrag(monitor) {
      if (typeof canDrag === 'boolean') {
        return canDrag
      }
      return canDrag(monitor.getItem())
    },
    collect(monitor) {
      return {
        isDragging: monitor.isDragging(),
      }
    },
  })

  const [{ isOver }, drop] = useDrop({
    accept: [ElementType.Field, ElementType.Layout],
    canDrop(item: DraggableElement) {
      const droppable = typeof canDrop === 'boolean' ? canDrop : canDrop(item)
      return (droppable && element.type === ElementType.Layout) || hasValue(mousePosition)
    },
    drop(dragItem: DraggableElement, monitor) {
      if (monitor.didDrop() || dragItem.elementId === elementId) {
        return
      }
      const targetIndex =
        mousePosition === MousePosition.Right || mousePosition === MousePosition.Bottom ? index + 1 : index
      // add new element to layout builder
      if (dragItem.parentId === LEFT_SIDEBAR) {
        const newElementId = nanoid()
        const component = dragItem.item!.component
        const { icon: _icon, ...item } = ZONE_ITEMS[component]
        const newElement = {
          id: newElementId,
          children: [],
          ...item,
        } as Element

        if (hasValue(mousePosition)) {
          addElement({
            element: newElement,
            source: { id: dragItem.parentId },
            target: { id: parentId, index: targetIndex },
          })
          if (component === 'Tabs') {
            const { icon: _icon, ...tab } = ZONE_ITEMS['Section']
            addElement({
              element: {
                id: nanoid(),
                ...tab,
              },
              source: { id: dragItem.parentId },
              target: { id: newElementId },
            })
          }
          setMousePosition(null)
          return
        }
        addElement({ element: newElement, source: { id: dragItem.parentId }, target: { id: elementId } })
        if (component === 'Tabs') {
          const { icon: _icon, ...tab } = ZONE_ITEMS['Section']
          addElement({
            element: {
              id: nanoid(),
              ...tab,
              props: {
                ...tab.props,
                title: 'Tab 1',
              },
            },
            source: { id: dragItem.parentId },
            target: { id: newElementId },
            selectElementId: newElement.id,
          })
        }
        return
      }
      // move element around
      if (hasValue(mousePosition)) {
        moveElement({
          elementId: dragItem.elementId!,
          source: { id: dragItem.parentId },
          target: { id: parentId, index: targetIndex },
        })
        setMousePosition(null)
        return
      }
      moveElement({
        elementId: dragItem.elementId!,
        source: { id: dragItem.parentId },
        target: { id: elementId },
      })
    },
    hover: throttle(function (dragItem: DraggableElement, monitor: DropTargetMonitor) {
      if (!elementRef.current || dragItem.elementId === elementId) {
        return
      }
      const elementRect = elementRef.current.getBoundingClientRect()

      const mouseOffset = monitor.getClientOffset() || { x: 0, y: 0 }
      const direction = parent.props.columns?.length > 1 ? Direction.Horizontal : Direction.Vertical
      const position = calculateMousePosition(elementRect, mouseOffset, direction)
      setMousePosition(position)
    }, 300),
    collect(monitor) {
      return {
        isOver: canDrop && monitor.isOver({ shallow: true }),
      }
    },
  })

  drag(drop(elementRef))

  return [{ isDragging, isOver, isMouseOver: mouseOver, mousePosition: isOver ? mousePosition : null }, elementRef]
}
