import * as React from 'react'
import { useMutation, useQueryClient } from 'react-query'
import { Link, useParams } from 'react-router-dom'
import styled, { useTheme } from 'styled-components'
import { BuilderHeader } from '../../components/Builder/BuilderHeader'
import { color, spacing } from '../../components/GlobalStyle'
import { Icons, SidebarIcons } from '../../components/Icons'
import { NButton } from '../../components/NButton/NButton'
import { NDivider } from '../../components/NDivider'
import { elevation } from '../../components/NElevation'
import { NSpinner } from '../../components/NSpinner/NSpinner'
import { NToast } from '../../components/NToast'
import { NTypography, typography } from '../../components/NTypography'
import { useClickOutside } from '../../hooks/useClickOutside'
import { APIServices } from '../../services/api'
import { AppQueryKey } from '../../services/api/endpoints/app'
import { BuilderQueryKey } from '../../services/api/endpoints/builder'
import { ApplicationResponse, ComponentResponse, LayoutResponse } from '../../services/api/models'
import { AppsProfilesSelectModal } from './components/AppsProfilesSelectModal'
import { AskForPublishModal } from './components/AskForPublishModal'
import { DroppableZone } from './components/DroppableZone'
import { NavIcons } from './components/Icons'
import { LeftSidebar } from './components/LeftSidebar'
import { PublishAppPageModal } from './components/PublishAppPageModal'
import { PublishRecordPageModal } from './components/PublishRecordPageModal'
import { RecordPageAppConfirmModal } from './components/RecordPageAppConfirmModal'
import { RightSidebar } from './components/RightSidebar'
import { ActionsUndoRedo, useLayoutAction, useLayoutBuilder } from './contexts/LayoutBuilderContext'

const Wrapper = styled('div')`
  display: grid;
  grid-template-rows: auto 1fr;
  grid-template-columns: 251px 1fr 250px;
  grid-template-areas:
    'header header header'
    'left-sidebar droppable-zone right-sidebar';
  background: ${color('Neutral200')};
  width: 100vw;
  height: 100vh;
  overflow: hidden;
`

const Breadcrumbs = styled('ul')`
  display: flex;
  height: 100%;
  li {
    position: relative;
    display: inline-flex;
    align-items: center;
    height: 100%;
    ${typography('button')};
    a {
      color: ${color('Neutral400')};
      &:after {
        content: '/';
        margin: 0 4px;
        color: ${color('Neutral400')};
      }
      &:hover {
        color: ${color('Neutral900')};
      }
    }
  }
`

const DropdownWrapper = styled('div')`
  z-index: 1000;
  position: absolute;
  top: calc(${spacing('xs')} + 100%);
  right: 0;
  width: 180px;
  padding-top: ${spacing('xxs')};
  padding-bottom: ${spacing('xxs')};
  border-radius: 3px;
  ${elevation('Elevation300')}
  background: ${color('Neutral900')};
`

const Action = styled(NTypography.SmallText)`
  padding: ${spacing('xs')} ${spacing('sm')};
  color: ${color('white')};
  &:hover {
    background: ${color('Primary700')};
  }
`

const Icon = styled('div')`
  margin-left: cacl(${spacing('xs')} / 2);
  width: 16px;
  height: 16px;
  display: flex;
  align-items: center;
  justify-content: center;
  svg {
    transition: transform 0.3s ease-in-out;
  }
  &.show-dropdown {
    svg {
      transform: rotate(180deg);
    }
  }
`

type LayoutBuilderProps = {
  children?: React.ReactNode
}

export function LayoutBuilder({}: LayoutBuilderProps) {
  const theme = useTheme()
  const queryClient = useQueryClient()

  const dropdownRef = React.useRef<HTMLDivElement>(null)
  const [showDropdown, setShowDropdown] = React.useState(false)

  const params = useParams<{ layoutId: string }>()

  const { undoRedoElement } = useLayoutAction()
  const { layoutDetails: layoutData, elements, indexHistory, elementHistory } = useLayoutBuilder()
  const [selectedAppsForRecord, setSelectedAppsForRecord] = React.useState<ApplicationResponse[]>([])

  const layoutDetails = (layoutData || {}) as LayoutResponse

  const ACTIONS: { onClick(): void; disabled: boolean; label: string }[] = [
    {
      onClick() {
        console.log('thanhtc', 'View History')
      },
      disabled: false,
      label: 'View version history',
    },
    {
      onClick() {
        console.log('thanhtc', 'Duplicate')
      },
      disabled: false,
      label: 'Duplicate',
    },
    {
      onClick() {},
      disabled: false,
      label: 'Rename',
    },
    {
      onClick() {
        console.log('thanhtc', 'Delete')
      },
      disabled: false,
      label: 'Delete',
    },
  ]

  useClickOutside(() => setShowDropdown(false), dropdownRef)

  const [showAskPublish, setShowAskPublish] = React.useState(false)
  const [showPublishAppPage, setShowPublishAppPage] = React.useState(false)
  const [showPublishRecordPage, setShowPublishRecordPage] = React.useState(false)
  const [showRecordAppConfirm, setShowRecordAppConfirm] = React.useState(false)
  const [showAppsProfilesSelect, setShowAppsProfilesSelect] = React.useState(false)

  const handlePublish = () => {
    const layoutType = layoutDetails.type
    if (!layoutType) {
      return
    }
    if (layoutType === 'app-page') {
      setShowPublishAppPage(true)
      return
    }
    setShowPublishRecordPage(true)
  }

  const { mutate: updateLayoutScript, isLoading: isUpdatingScript } = useMutation(APIServices.Builder.putLayoutScript, {
    onSuccess: (_data, variables) => {
      queryClient.invalidateQueries([BuilderQueryKey.getLayoutScript, { id: variables.id }])
      queryClient.invalidateQueries([AppQueryKey.getAppScript, { layoutId: variables.id }])
      NToast.success({ title: 'Layout Builder', subtitle: 'Save script successful' })
    },
    onError: error => {
      NToast.error({ title: 'Update unsuccessful', subtitle: error.response?.data.message })
    },
  })

  const handleSave = () => {
    if (!params.layoutId) {
      setShowAskPublish(true)
    }
    updateLayoutScript({ id: params.layoutId, script: elements as Record<string, ComponentResponse> })
  }

  const handleAssignAppDefault = (apps: ApplicationResponse[]) => {
    setSelectedAppsForRecord(apps)
    //for record modal
    setShowPublishRecordPage(false)
    setShowRecordAppConfirm(true)
  }

  const handleBackAppDefault = () => {
    setShowPublishRecordPage(true)
    setShowRecordAppConfirm(false)
  }

  const handleAssignAppsProfiles = () => {
    setShowPublishRecordPage(false)
    setShowAppsProfilesSelect(true)
  }

  const handleBackAssignAppsProfiles = () => {
    setShowPublishRecordPage(true)
    setShowAppsProfilesSelect(false)
  }

  return (
    <Wrapper>
      <BuilderHeader
        icon={<SidebarIcons.LayoutOutlined />}
        title="Layout Builder"
        rightElement={
          <React.Fragment>
            <NButton
              type="link"
              icon={<NavIcons.Undo />}
              size="small"
              disabled={indexHistory === 0}
              onClick={() => undoRedoElement(ActionsUndoRedo.Undo)}
            />
            <NButton
              type="link"
              icon={<NavIcons.Redo />}
              size="small"
              disabled={indexHistory === elementHistory.length - 1}
              onClick={() => undoRedoElement(ActionsUndoRedo.Redo)}
            />
            <NDivider vertical size="md" lineSize={1} lineColor={color('Neutral200')({ theme })} />
            <NDivider vertical />
            <NButton type="outline" size="small" onClick={handlePublish}>
              Publish
            </NButton>
            <NDivider vertical />
            <NButton type="primary" size="small" loading={isUpdatingScript} onClick={handleSave}>
              Save
            </NButton>
          </React.Fragment>
        }
        style={{ gridArea: 'header' }}>
        <Breadcrumbs>
          <li>
            <Link to="/setup/layout">Layout</Link>
          </li>
          <li onClick={() => setShowDropdown(prev => !prev)}>
            {layoutDetails?.name || <NSpinner size={8} strokeWidth={2} />}
            <Icon className={showDropdown ? 'show-dropdown' : ''}>
              <Icons.Down />
            </Icon>
            {showDropdown && (
              <DropdownWrapper ref={dropdownRef}>
                {ACTIONS.map(action => (
                  <Action key={action.label} onClick={action.onClick}>
                    {action.label}
                  </Action>
                ))}
              </DropdownWrapper>
            )}
          </li>
        </Breadcrumbs>
      </BuilderHeader>
      <LeftSidebar />
      <DroppableZone />
      <RightSidebar />
      {showAskPublish && (
        <AskForPublishModal visible={showAskPublish} setVisible={setShowAskPublish} name={layoutDetails!.name}>
          <NButton
            size="small"
            type="ghost"
            onClick={() => {
              setShowAskPublish(false)
            }}>
            Not yet
          </NButton>
          <NButton
            size="small"
            type="primary"
            onClick={() => {
              setShowAskPublish(false)
              handlePublish()
            }}>
            Publish
          </NButton>
        </AskForPublishModal>
      )}
      {showPublishAppPage && (
        <PublishAppPageModal
          visible={showPublishAppPage}
          setVisible={setShowPublishAppPage}
          name={layoutDetails!.name}
        />
      )}

      {showPublishRecordPage && (
        <PublishRecordPageModal
          visible={showPublishRecordPage}
          setVisible={setShowPublishRecordPage}
          name={layoutDetails!.name}
          onAssignToApp={handleAssignAppDefault}
          onAssignToAppsProfiles={handleAssignAppsProfiles}
        />
      )}

      {showRecordAppConfirm && (
        <RecordPageAppConfirmModal
          onBack={handleBackAppDefault}
          visible={showRecordAppConfirm}
          setVisible={setShowRecordAppConfirm}
          selectedApps={selectedAppsForRecord}
        />
      )}

      {showAppsProfilesSelect && (
        <AppsProfilesSelectModal
          onBack={handleBackAssignAppsProfiles}
          visible={showAppsProfilesSelect}
          setVisible={setShowAppsProfilesSelect}
        />
      )}
    </Wrapper>
  )
}
