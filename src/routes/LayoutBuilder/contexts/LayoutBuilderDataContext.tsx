import * as React from 'react'
import { useQuery } from 'react-query'
import { useSession } from '../../../redux/slices/sessionSlice'
import { APIServices } from '../../../services/api'
import { BuilderQueryKey } from '../../../services/api/endpoints/builder'
import { DataQueryKey } from '../../../services/api/endpoints/data'
import { DataResponse, FieldSchema } from '../../../services/api/models'
import { InputMap } from '../utils/models'
import { useLayoutBuilder } from './LayoutBuilderContext'

export type ObjectSchema = {
  fields: FieldSchema[]
  fieldsByName: Record<string, FieldSchema>
  actions: string[]
  related: Record<string, { name: string; objName: string }>
}

type LayoutBuilderDataContextType = {
  isLoading: boolean
  previewData: DataResponse
  objectSchema: ObjectSchema
  inputMap: InputMap
}

export const LayoutBuilderDataContext = React.createContext<LayoutBuilderDataContextType | string>(
  'useLayoutBuilderData should be used inside LayoutBuilderDataProvider',
)

type Props = {
  children: React.ReactNode
}

export function LayoutBuilderDataProvider({ children }: Props) {
  const { layoutDetails } = useLayoutBuilder()
  const session = useSession()

  const objectName = layoutDetails && layoutDetails.objectName

  // get preview data for record page
  const { data: previewData = {} as DataResponse, isLoading: isPreviewDataLoading } = useQuery(
    [DataQueryKey.getObjectData, { name: objectName!, limit: 1 }],
    APIServices.Data.getObjectData,
    {
      enabled: !!objectName,
      select: data => {
        if (data.data.pageInfo.total > 0) {
          return data.data.data[0]
        }
        return undefined
      },
    },
  )

  // get schema for record page
  const {
    data: objectSchema = { fields: [], fieldsByName: {}, actions: [], related: {} } as ObjectSchema,
    isLoading: isObjectSchemaLoading,
  } = useQuery([BuilderQueryKey.getObjectSchema, { objName: objectName! }], APIServices.Builder.getObjectSchema, {
    enabled: !!objectName,
    select: data => {
      if (data.data) {
        const { fields, fieldsByName } = data.data.fields.reduce(
          (allFields, field) => {
            return {
              fields: [...allFields.fields, field],
              fieldsByName: { ...allFields.fieldsByName, [field.name]: field },
            }
          },
          { fields: [], fieldsByName: {} } as { fields: FieldSchema[]; fieldsByName: Record<string, FieldSchema> },
        )
        return {
          fields,
          fieldsByName,
          actions: [] as string[],
          related: data.data.related,
        } as ObjectSchema
      }
      return { fields: [], fieldsByName: {}, actions: [], related: {} } as ObjectSchema
    },
  })

  const isLoading = isPreviewDataLoading || isObjectSchemaLoading

  const value: LayoutBuilderDataContextType = React.useMemo(
    () => ({
      isLoading,
      previewData,
      objectSchema,
      inputMap: {
        now: new Date().toISOString(),
        curUser: session.user,
        curRecord: previewData,
      },
    }),
    [isLoading, previewData, objectSchema, session.user],
  )

  return <LayoutBuilderDataContext.Provider value={value}>{children}</LayoutBuilderDataContext.Provider>
}

export function useLayoutBuilderData() {
  const c = React.useContext(LayoutBuilderDataContext)
  if (typeof c === 'string') {
    throw new Error(c)
  }
  return c
}
