import * as React from 'react'
import styled from 'styled-components'
import { Icons } from '../../../components/Icons'
import { NCheckbox } from '../../../components/NCheckbox/NCheckbox'
import { NDivider } from '../../../components/NDivider'
import { ComponentIcons } from '../components/Icons'
import { TextArea } from '../components/Inputs/TextArea'
import { TextInput } from '../components/Inputs/TextInput'
import { Widget } from '../components/Sidebar'
import ElementLayout from '../components/ZoneItems/ElementLayout'
import { useLayoutAction, useLayoutBuilder } from '../contexts/LayoutBuilderContext'
import { useElementRenderer } from '../hooks/useElementRenderer'
import { ElementType, ZoneProps } from '../utils/models'

const StyledElementLayout = styled(ElementLayout)`
  min-height: 100px;
`

const Wrapper = styled('div')`
  flex: 1;
  display: flex;
  justify-content: space-between;
  align-items: center;
  > :last-child {
    display: flex;
  }
`

const Title = styled('div')`
  font-size: 16px;
  line-height: 16px;
  font-weight: bold;
  color: #0d1826;
`
const Subtitle = styled('div')`
  font-size: 11px;
  line-height: 11px;
  color: #768493;
  margin-top: 8px;
`

export function Header({ elementId, parentId, index }: ZoneProps) {
  const { elements } = useLayoutBuilder()
  const { configElement } = useLayoutAction()
  const element = elements[elementId]
  const children = useElementRenderer(elementId)
  return (
    <StyledElementLayout
      label="Header"
      elementId={elementId}
      parentId={parentId}
      index={index}
      sidebar={
        <React.Fragment>
          <Widget title="Title">
            <TextInput
              value={element.props.title || ''}
              onChange={e => configElement(element.id, { title: e.target.value })}
            />
            <NDivider />
            <NCheckbox
              title="Show Back Button"
              checked={element.props.hasBack}
              onChange={e => {
                if (e.target.checked) {
                  configElement(element.id, { hasBack: true })
                  return
                }
                configElement(element.id, { hasBack: false })
              }}
            />
          </Widget>
          <Widget title="Description">
            <TextArea
              value={element.props.description || ''}
              onChange={e => configElement(element.id, { description: e.target.value })}
            />
          </Widget>
        </React.Fragment>
      }>
      <Wrapper>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          {element.props.hasBack && (
            <div style={{ width: 32, height: 32, display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
              <Icons.Left />
            </div>
          )}
          <div>
            <Title>{element.props.title}</Title>
            {element.props.description && <Subtitle>{element.props.description}</Subtitle>}
          </div>
        </div>
        <div>
          {children && children.length < 1 && (
            <div style={{ padding: '8px 16px', border: '1px dashed #A3ABB5', borderRadius: 4 }}>
              Drop Trigger here to start
            </div>
          )}
          {children}
        </div>
      </Wrapper>
    </StyledElementLayout>
  )
}

export const widgetHeader = {
  icon: <ComponentIcons.Flow />,
  label: 'Header',
  type: ElementType.Layout,
  component: 'DemoHeader',
  props: {
    title: 'Title',
    description: '',
    columns: [1, 1],
    hasBack: false,
  },
  children: [],
}
