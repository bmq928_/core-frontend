import { format } from 'date-fns'
import { camelCase, capitalize as ldCapitalize, snakeCase } from 'lodash'
import { ROOT_ELEMENT } from '../routes/LayoutBuilder/utils/builder'

export function buildKeys<T extends {}>(prefix: string, data: T) {
  return Object.keys(data).reduce((a, key) => ({ ...a, [key]: `${prefix}.${key}` }), {} as Record<keyof T, string>)
}

export function noop() {}

//
export function hasValue(v: unknown): boolean {
  return v !== null && v !== undefined
}

//
export function isEmptyStr(str?: string): boolean {
  if (!str) {
    return true
  }
  const trimSpaceStr = str.trim()
  return trimSpaceStr.length === 0
}

//
export const parseLayoutScript = (script?: string | object) => {
  if (!script) {
    return undefined
  }

  try {
    const parsedScript = typeof script === 'string' ? JSON.parse(script) : script
    if (!parsedScript[ROOT_ELEMENT]) {
      return undefined
    }

    return parsedScript as Record<string, any>
  } catch (error) {
    return undefined
  }
}

////////////////////////////////
export const transformOptionsToString = (optionsValue?: string): string => {
  try {
    const options = optionsValue && JSON.parse(optionsValue)
    return options && options.length > 0 ? options.join('\n') : ''
  } catch {
    return ''
  }
}

export const transformStringToOptions = (optionsStringValue?: string): string | undefined => {
  return optionsStringValue
    ? JSON.stringify(optionsStringValue.split('\n').filter(stringVal => stringVal.trim() !== ''))
    : undefined
}

////////////////////////////////
const isoDateRegExp = new RegExp(
  /(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z))|(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z))|(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z))/,
)

export function isISODate(str: string) {
  return isoDateRegExp.test(str)
}

type DateTimeType = string | undefined
export const getDateFromIsoString = (iosString: string, type: DateTimeType = 'date-time') => {
  if (isISODate(iosString)) {
    return new Date(iosString)
  }

  if (type === 'date') {
    return getDateFromDateIsoString(iosString)
  }
  if (type === 'time') {
    return getDateFromTimeIsoString(iosString)
  }

  throw new Error('getDateFromIsoString\niosString is not in correct')
}

// translate string format yyyy-mm-dd to date
export const getDateFromDateIsoString = (iosString: string) => {
  const splitTime = iosString.split('-')
  if (splitTime.length !== 3) {
    throw new Error('Not iso string date')
  }
  if (isNaN(+splitTime[0]) || isNaN(+splitTime[1]) || isNaN(+splitTime[2])) {
    throw new Error('Not iso string date')
  }

  const newDate = new Date()
  newDate.setHours(0, 0, 0, 0)
  newDate.setFullYear(+splitTime[0], +splitTime[1] - 1, +splitTime[2])

  return newDate
}
// translate string format hh:mm:ss to date
export const getDateFromTimeIsoString = (iosString: string) => {
  const splitTime = iosString.split(':')
  if (splitTime.length !== 3) {
    throw new Error('Not iso string time')
  }
  if (isNaN(+splitTime[0]) || isNaN(+splitTime[1]) || isNaN(+splitTime[2])) {
    throw new Error('Not iso string time')
  }

  const newDate = new Date()
  newDate.setHours(+splitTime[0], +splitTime[1], +splitTime[2])

  return newDate
}
// get string from date
type getStringFromDateParams = {
  date: Date
  type: DateTimeType
  dateFormat: string
  timeFormat: string
}
export const getStringFromDate = ({ date, type = 'date-time', dateFormat, timeFormat }: getStringFromDateParams) => {
  if (type === 'date') {
    return format(date, dateFormat)
  }
  if (type === 'time') {
    return format(date, timeFormat)
  }
  if (type === 'date-time') {
    return format(date, `${dateFormat} ${timeFormat}`)
  }

  throw new Error('Date time subtype not valid!')
}
// get value string from Date
export const getValueFromDate = (date: Date, type: DateTimeType = 'date-time') => {
  if (type === 'date') {
    return format(date, 'yyyy-MM-dd')
  }
  if (type === 'time') {
    return format(date, 'HH:mm:ss')
  }
  if (type === 'date-time') {
    return date.toISOString()
  }

  throw new Error('Date time subtype not valid!')
}

// get name(id) from display name
export const getNameFromDisplayName = (displayName: string) => {
  return camelCase(displayName)
}

export function capitalize(str?: string) {
  if (!str || typeof str !== 'string') {
    return ''
  }
  return snakeCase(str)
    .split('_')
    .map(word => ldCapitalize(word))
    .join(' ')
}

export const NUMBER_FORMAT_REGEX = /\B(?=(\d{3})+(?!\d))/g

export function numberFormat(rawAmount: number | string) {
  const amount = Number(rawAmount)
  if (Number.isNaN(amount)) {
    return rawAmount
  }
  return `${rawAmount}`.replace(NUMBER_FORMAT_REGEX, ',')
}
