import { render, RenderOptions } from '@testing-library/react'
import * as React from 'react'
import { I18nextProvider } from 'react-i18next'
import { Provider } from 'react-redux'
import { ThemeProvider } from 'styled-components'
import { GlobalStyle, theme } from '../components/GlobalStyle'
import i18n from '../i18n/i18n'
import { store } from '../redux'

type ProviderProps = {
  children?: React.ReactNode
}

function Providers({ children }: ProviderProps) {
  return (
    <I18nextProvider i18n={i18n}>
      <Provider store={store}>
        <ThemeProvider theme={theme}>
          <GlobalStyle />
          {children}
        </ThemeProvider>
      </Provider>
    </I18nextProvider>
  )
}

const customRender = (ui: React.ReactElement, options?: RenderOptions) => {
  return render(ui, { wrapper: Providers, ...options })
}

export * from '@testing-library/react'
export { customRender as render }
