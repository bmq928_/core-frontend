export const formatCurrencyValue = (locale: string, currencyCode: string, amount: number) => {
  return new Intl.NumberFormat(locale, {
    style: 'currency',
    currency: currencyCode,
  }).format(amount)
}
