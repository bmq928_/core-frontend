import { Action, configureStore, ThunkAction } from '@reduxjs/toolkit'
import { rootReducer, RootState } from './rootReducer'

export const store = configureStore({
  reducer: rootReducer,
})
// @ts-ignore
if (process.env.NODE_ENV === 'development' && module.hot) {
  // @ts-ignore
  module.hot.accept('./rootReducer', () => {
    const newRootReducers = require('./rootReducer').default
    store.replaceReducer(newRootReducers)
  })
}

export type AppDispatch = typeof store.dispatch
export type AppThunk = ThunkAction<void, RootState, null, Action<string>>
