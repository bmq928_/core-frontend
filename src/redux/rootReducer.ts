import { combineReducers } from '@reduxjs/toolkit'
import { authKey, authReducer } from './slices/authSlice'
import { sessionKey, sessionReducer } from './slices/sessionSlice'

export const rootReducer = combineReducers({
  [authKey]: authReducer,
  [sessionKey]: sessionReducer,
})

export type RootState = ReturnType<typeof rootReducer>
