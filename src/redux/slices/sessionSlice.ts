import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { useSelector } from 'react-redux'
import { APIServices } from '../../services/api'
import { BundleUserResponse, SessionResponse } from '../../services/api/models'

export const sessionKey = 'session'

const initialState: SessionState = {
  status: 'loading',
}

const getSession = createAsyncThunk('session/getSessions', async () => {
  try {
    const response = await APIServices.Session.getSessions()
    return response.data
  } catch (e) {
    if (e.response) {
      return e.response.data
    }
    return {
      message: 'Something went wrong!',
    }
  }
})

export type SessionState = {
  status: 'idle' | 'loading' | 'error'
  message?: string
  objName?: string
  guid?: string
  lastUsedApp?: string
  lastUrl?: string
  defaultCurrencyCode?: string
  defaultLocale?: string
  currencyLocale?: string
  user_name?: string
  user?: BundleUserResponse
}

const sessionSlide = createSlice({
  name: sessionKey,
  initialState,
  reducers: {
    setSession(state, action: PayloadAction<SessionResponse>) {
      state.guid = action.payload.guid
      state.lastUsedApp = action.payload.last_used_app
      state.lastUrl = action.payload.last_url
      state.defaultCurrencyCode = action.payload.defaultCurrencyCode
      state.defaultLocale = action.payload.defaultLocale
      state.currencyLocale = action.payload.currencyLocale
    },
    clearSession() {
      return initialState
    },
  },
  extraReducers: {
    [getSession.pending.toString()]: state => {
      state.status = 'loading'
    },
    [getSession.fulfilled.toString()]: (state, action) => {
      state.user_name = action.payload.user_name
      state.guid = action.payload.guid
      state.lastUsedApp = action.payload.last_used_app
      state.lastUrl = action.payload.last_url
      state.defaultCurrencyCode = action.payload.defaultCurrencyCode
      state.defaultLocale = action.payload.defaultLocale
      state.currencyLocale = action.payload.currencyLocale
      state.status = 'idle'
      state.user = action.payload.user
    },
    [getSession.rejected.toString()]: (state, action) => {
      state.status = 'error'
      state.message = action.payload.message
    },
  },
})

export const sessionActions = {
  ...sessionSlide.actions,
  getSession,
}
export const sessionReducer = sessionSlide.reducer

export function useSession() {
  return useSelector((state: { [sessionKey]: SessionState }) => state[sessionKey])
}
