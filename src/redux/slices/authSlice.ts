import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { useSelector } from 'react-redux'

export const authKey = 'auth'
const accessTokenKey = 'accessToken'

const initialState = {
  accessToken: localStorage.getItem(accessTokenKey) || '',
  canRedirectToLastUrl: true,
}

export type AuthState = Partial<typeof initialState>

const authSlice = createSlice({
  name: authKey,
  initialState,
  reducers: {
    addToken(state, action: PayloadAction<{ accessToken: string }>) {
      state.accessToken = action.payload.accessToken
      state.canRedirectToLastUrl = true
      localStorage.setItem(accessTokenKey, action.payload.accessToken)
    },
    removeToken(state) {
      state.accessToken = ''
      localStorage.removeItem(accessTokenKey)
    },
    stopRedirectToLastUrl(state) {
      state.canRedirectToLastUrl = false
    },
  },
})

export const authActions = authSlice.actions
export const authReducer = authSlice.reducer

export function useAuth() {
  return useSelector((state: { [authKey]: AuthState }) => state[authKey])
}
