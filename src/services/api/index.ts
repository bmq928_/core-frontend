import { AxiosError } from 'axios'
import { Apps } from './endpoints/app'
import { Auth } from './endpoints/auth'
import { Builder } from './endpoints/builder'
import { Currencies } from './endpoints/currencies'
import { Data } from './endpoints/data'
import { Flows } from './endpoints/flows'
import { Integrations } from './endpoints/integrations'
import { Layout } from './endpoints/layout'
import { Media } from './endpoints/media'
import { Metadata } from './endpoints/metadata'
import { ObjectRecordPages } from './endpoints/objRecordPages'
import { Organization } from './endpoints/organization'
import { PickLists } from './endpoints/pick-lists'
import { Profiles } from './endpoints/profiles'
import { Session } from './endpoints/session'
import { UploadFile } from './endpoints/upload-file'
import { Users } from './endpoints/users'
import { request } from './request'

export type ErrorResponse = AxiosError<{
  error?: string
  message: string
  statusCode: number
}>

export const APIServices = {
  request,
  Builder,
  Metadata,
  Data,
  Apps,
  Auth,
  Session,
  Profiles,
  Flows,
  Layout,
  Users,
  Integrations,
  Currencies,
  Organization,
  ObjectRecordPages,
  UploadFile,
  Media,
  PickLists,
}
