/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface ObjectRecordName {
  label: string;
  type: "text";
}

export interface ObjectResponse {
  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  name: string;
  displayName: string;
  description?: string;
  isSystemDefault: boolean;
  isExternal: boolean;
  recordName: ObjectRecordName;
}

export interface PageInfoType {
  limit: number;
  offset: number;
  total: number;
}

export interface ObjectCollection {
  data: ObjectResponse[];
  pageInfo: PageInfoType;
}

export interface CreateRecordNameDto {
  label: string;
  type: "text";
}

export interface ObjectCreateDto {
  displayName: string;
  recordName: CreateRecordNameDto;
  name: string;
  description?: string;
}

export interface UpdateRecordNameDto {
  label?: string;
  type?: "text";
}

export interface ObjectUpdateDto {
  displayName?: string;
  recordName?: UpdateRecordNameDto;
  description?: string;
}

export interface ObjectDto {
  data?: ObjectCreateDto | ObjectUpdateDto;
  action: "create" | "update" | "delete";
  name?: string;
}

export interface AttributeType {
  subTypes: string[];
}

export interface DataTypeResponse {
  name: string;
  displayName: string;
  description?: string;
  systemType:
    | "numeric"
    | "text"
    | "dateTime"
    | "boolean"
    | "pickList"
    | "json"
    | "generated"
    | "currency"
    | "externalRelation"
    | "indirectRelation"
    | "relation";
  validateScript: string;
  attributes: AttributeType;
}

export interface FieldAttributes {
  /** Use with relation field. 'SET_NULL' requires 'isRequired'=false */
  onDelete?: "noAction" | "setNull" | "cascade";

  /** Currently applied for short text only. 'Partial' will only expose the last 4 characters. 'All' will hide everything & return '****' */
  sensitivity?: "none" | "partial" | "all";
  subType?: string;
  defaultValue?: string;
  isUnique?: boolean;
  isIndexed?: boolean;
  isSearchable?: boolean;
  isSortable?: boolean;
  isReadOnly?: boolean;
}

export interface FieldResponse {
  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  name: string;
  displayName: string;
  description?: string;
  isSystemDefault: boolean;
  id: string;
  isRequired: boolean;
  dataType: DataTypeResponse;
  attributes: FieldAttributes;
  value?: string;
  isExternalId: boolean;
  pickListId?: number;
}

export interface FieldAttributesDto {
  /** Use with relation field. 'SET_NULL' requires 'isRequired'=false */
  onDelete?: "noAction" | "setNull" | "cascade";

  /** Currently applied for short text only. 'Partial' will only expose the last 4 characters. 'All' will hide everything & return '****' */
  sensitivity?: "none" | "partial" | "all";
  subType?: string;
  defaultValue?: string;
  isUnique?: boolean;
  isIndexed?: boolean;
  isSearchable?: boolean;
  isSortable?: boolean;
}

export interface FieldCreateDto {
  typeName:
    | "numeric"
    | "text"
    | "dateTime"
    | "boolean"
    | "pickList"
    | "json"
    | "generated"
    | "currency"
    | "externalRelation"
    | "relation";
  isRequired?: boolean;
  isExternalId?: boolean;

  /**
   * value can be:
   *
   *     For pickList: Json array of options
   *     For relation/externalRelation: targetObjectName
   *     For indirectRelation: targetObjectName.externalFieldName
   *
   */
  value?: string;
  pickListId?: number;
  name: string;
  displayName: string;
  attributes: FieldAttributesDto;
  description?: string;
}

export interface FieldUpdateDto {
  isRequired?: boolean;
  isExternalId?: boolean;

  /**
   * value can be:
   *
   *     For pickList: Json array of options
   *     For relation/externalRelation: targetObjectName
   *     For indirectRelation: targetObjectName.externalFieldName
   *
   */
  value?: string;
  pickListId?: number;
  displayName?: string;
  attributes?: FieldAttributesDto;
  description?: string;
}

export interface LayoutComponent {
  layoutId: string;
  componentIds: string[];
}

export interface FieldDto {
  data?: FieldCreateDto | FieldUpdateDto;
  updateLayouts?: LayoutComponent[];
  action: "create" | "update" | "delete";
  name?: string;
}

export interface RuleItem {
  fieldName: string;
  operator:
    | "==="
    | "!=="
    | "isDefined"
    | "isEmpty"
    | "isNotEmpty"
    | "isIn"
    | "isNotIn"
    | "isNegative"
    | "isPositive"
    | "<="
    | ">="
    | "<"
    | ">"
    | "minDate"
    | "maxDate"
    | "contains"
    | "notContains";
  value?: string;
}

export interface AndLogicRule {
  rules: RuleItem[];
}

export interface ValidationRuleResponse {
  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  name: string;
  displayName: string;
  description?: string;
  isSystemDefault: boolean;
  id: string;
  script: AndLogicRule[];
  execStript?: string;
  errorMessage: string;
}

export interface RuleCreateDto {
  displayName: string;
  script: AndLogicRule[];
  errorMessage: string;
  name: string;
  description?: string;
}

export interface RuleUpdateDto {
  displayName?: string;
  script?: AndLogicRule[];
  errorMessage?: string;
  description?: string;
}

export interface RuleDto {
  data?: RuleCreateDto | RuleUpdateDto;
  action: "create" | "update" | "delete";
  name?: string;
}

export interface PickListItemResponse {
  id: string;
  value: string;
}

export interface PickListResponse {
  items: PickListItemResponse[];

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  name: string;
  displayName: string;
  description?: string;
  isSystemDefault: boolean;
  id: number;
  isActive: boolean;
}

export interface PickListCollection {
  data: PickListResponse[];
  pageInfo: PageInfoType;
}

export interface MutatePickListItemDto {
  action?: "CREATE" | "UPDATE" | "DELETE";
  id?: string;
  value?: string;
}

export interface CreatePickListDto {
  items?: MutatePickListItemDto[];
  name: string;
  displayName: string;
  description?: string;
}

export interface UpdatePickListDto {
  displayName?: string;
  description?: string;
  items?: MutatePickListItemDto[];
}

export interface DataResponseMetadata {
  /** { relationFieldName: relationGuid } */
  relations?: Record<string, string>;

  /** { [currencyFieldName]: [amount in default currency] } */
  currency_values?: Record<string, number>;

  /** { [mediaId]: [DataMediaResponse] } */
  media?: Record<string, DataMediaResponse>;
  createdBy?: Record<string, UserResponse>;
}

export interface DataResponse {
  createdBy: string;
  orgId: number;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  objName: string;
  guid: string;
  currencyCode: string;
  $metadata?: DataResponseMetadata;
}

export interface DataCollection {
  data: DataResponse[];
  pageInfo: PageInfoType;
}

export interface ExtDataResponse {
  orgId: number;
  objName: string;
  externalId: string;
  displayUrl: string;
  $metadata?: DataResponseMetadata;
}

export interface ExtDataCollection {
  data: ExtDataResponse[];
  pageInfo: PageInfoType;
}

export interface MediaDto {
  /** Map of attached media files: {[mediaId]: tagName} */
  save?: Record<string, string>;

  /** Array of tagName to remove from attached files */
  del?: string[];
}

export interface Metadata {
  media?: MediaDto;
}

export interface MetadataDto {
  $metadata?: Metadata;
}

export interface BundleActionResponse {
  name: string;
  displayName: string;
}

export interface MediaResponse {
  actionMetadata: BundleActionResponse;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  name: string;
  displayName: string;
  description?: string;
  id: string;
  size: number;
  url: string;
  mimeType: string;
  referenceKey: string;
  actionMetadataId: string;
  createdBy: string;
}

export interface DataMediaResponse {
  tagName?: string;
  name: string;
  displayName: string;
  description?: string;
  id: string;
  size: number;
  url: string;
  mimeType: string;
  createdBy: string;
}

export interface FilterItem {
  fieldName: string;
  operator: "===" | "!==" | "isIn" | "isNotIn" | "contains" | "notContains";
  value: object;
}

export interface ListRecordsQueryWithFilters {
  /** @min 0 */
  offset?: number;
  limit?: number;
  sortOrder?: "ASC" | "DESC";
  filters?: FilterItem[][];
  sortBy?: string;
  searchText?: string;
}

export interface LoginDto {
  username: string;
  password: string;
}

export interface LoginResponse {
  accessToken: string;
}

export interface BundleUserResponse {
  name: string;
  id: string;
}

export interface SessionResponse {
  guid: string;
  user_name: string;
  user: BundleUserResponse;
  last_used_app: string;
  last_url?: string;
  defaultCurrencyCode: string;
  defaultLocale: string;
  currencyLocale: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
}

export interface UpdateSessionDto {
  last_used_app?: string;
  last_url?: string;
}

export interface UserResponse {
  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  id: string;
  profileId?: string;
  name: string;
  ssoId: string;
  isActive: boolean;
}

export interface UserCollection {
  data: UserResponse[];
  pageInfo: PageInfoType;
}

export interface InviteUserDto {
  name: string;
  profileId: string;
}

export interface UpdateUserDto {
  name?: string;
  profileId?: string;
}

export interface RootProps {
  showHeader: boolean;
  showFooter: boolean;
}

export interface ListProps {
  filters?: FilterItem[][];
  objName: string;
  fields: string[];
  actions: string[];
  pageSize?: number;
  limit?: number;
}

export interface RecordProps {
  isPrimary?: boolean;
  numOfCols?: number;
  fields: string[];
  actions: string[];
  title?: string;
  description?: string;
}

export interface RelatedProps {
  filters?: FilterItem[][];
  objName: string;
  fields: string[];
  actions: string[];
  pageSize?: number;
  limit?: number;

  /**
   * Relation field name: key of 'ObjectSchemaResponse.related' field.
   *
   * To load data for this related list please use '/data/{objName}/{guid}/{relation}' with:
   *
   *   'objName' is record-page's objName
   *
   *   'guid' is current record's guid
   *
   *   'relation' is value of this field
   *
   *
   */
  relationField: string;
  title?: string;
  description?: string;
}

export interface SectionProps {
  columns: number[];
  title?: string;
}

export interface TabProps {
  type: "default" | "filled";
  showUnderlineGg: boolean;
}

export interface TriggerObjectInputMap {
  guid?: string;
  parentGuid?: string;
  parentName?: string;
  relationField?: string;
}

export interface TriggerObjectProps {
  title?: string;
  type: "object";
  action: "new" | "get" | "edit" | "delete";
  objName: string;
  inputMap: TriggerObjectInputMap;
}

export interface TriggerFlowProps {
  title?: string;
  type: "flow";
  flowId: string;
  inputMap: Record<string, string>;
}

export interface RelatedDataField {
  objName: string;

  /** Display content for relation selection box */
  name: string;
}

export interface FieldSchema {
  /** Use with relation field. 'SET_NULL' requires 'isRequired'=false */
  onDelete?: "noAction" | "setNull" | "cascade";

  /** Currently applied for short text only. 'Partial' will only expose the last 4 characters. 'All' will hide everything & return '****' */
  sensitivity?: "none" | "partial" | "all";
  name: string;
  displayName: string;
  description?: string;
  typeName:
    | "numeric"
    | "text"
    | "dateTime"
    | "boolean"
    | "pickList"
    | "json"
    | "generated"
    | "currency"
    | "externalRelation"
    | "indirectRelation"
    | "relation";
  subType?: string;
  value?: string | string[];
  targetObjId?: number;
  isRequired: boolean;
  defaultValue?: string;
  isUnique?: boolean;
  isIndexed?: boolean;
  isSearchable?: boolean;
  isSortable?: boolean;
  isReadOnly?: boolean;
}

export interface ObjectSchemaResponse {
  id: number;
  name: string;
  displayName: string;
  description?: string;
  fields: FieldSchema[];
  related: Record<string, RelatedDataField>;
}

export interface ComponentResponse {
  id: string;
  label: string;
  type: string;
  component: "ObjectList" | "RecordDetail" | "Flow" | "Button" | "RelatedList" | "Section" | "Tab" | string;
  props:
    | RootProps
    | ListProps
    | RecordProps
    | RelatedProps
    | SectionProps
    | TabProps
    | TriggerObjectProps
    | TriggerFlowProps;
  children: string[];
  schema?: ObjectSchemaResponse;
}

export interface ListFilterOperator {
  displayName: string;
  value: "===" | "!==" | "isIn" | "isNotIn" | "contains" | "notContains";
}

export interface TriggerObjectResponse {
  type: "object";
  action: "new" | "get" | "edit" | "delete";
  displayName: string;
}

export interface TriggerFlowResponse {
  type: "flow";
  flowId: string;
  name: string;
  displayName: string;
}

export interface LayoutResponse {
  orgId: number;
  id: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  objectName?: string;
  name: string;
  type: "app-page" | "record-page";
}

export interface LayoutCollection {
  data: LayoutResponse[];
  pageInfo: PageInfoType;
}

export interface CreateLayoutDto {
  /** Only required for record-page */
  objectName?: string;
  name: string;
  type: "app-page" | "record-page";
}

export interface UpdateLayoutDto {
  name?: string;
}

export interface ComponentDto {
  id: string;
  label: string;
  type: string;
  component: "ObjectList" | "RecordDetail" | "Flow" | "Button" | "RelatedList" | "Section" | "Tab" | string;
  props:
    | RootProps
    | ListProps
    | RecordProps
    | RelatedProps
    | SectionProps
    | TabProps
    | TriggerObjectProps
    | TriggerFlowProps;
  children: string[];
}

export interface UpdateScriptDto {
  script: Record<string, ComponentDto>;
}

export interface AppViewerResponse {
  id: string;
  name: string;
  description?: string;
}

export interface AssignAppPageToAppsDto {
  appIds: string[];
}

export interface AppProfileDto {
  appId: string;
  profileId: string;
}

export interface AssignToAppAndProfileDto {
  input: AppProfileDto[];
}

export interface AppLayoutResponse {
  id: string;
  name: string;
}

export interface ProfileLayoutResponse {
  id: string;
  name: string;
}

export interface AppProfileLayoutResponse {
  application: AppLayoutResponse;
  profile: ProfileLayoutResponse;
}

export interface ObjectLayoutResponse {
  script: Record<string, ComponentResponse>;
  content?: object;
  id: string;
  name: string;
  objectName?: string;
}

export interface ProfileObjectAccess {
  create: boolean;
  read: boolean;
  update: boolean;
  delete: boolean;
}

export interface ProfileObjectItemDto {
  objName: string;
  access: ProfileObjectAccess;
}

export interface UpsertProfileAccessDto {
  items: ProfileObjectItemDto[];
}

export interface ApplicationResponse {
  name: string;
  description?: string;
  id: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
}

export interface ApplicationCollection {
  data: ApplicationResponse[];
  pageInfo: PageInfoType;
}

export interface AppBuilderLayoutResponse {
  id: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  objectName?: string;
  name: string;
  type: "app-page" | "record-page";
}

export interface AppBuilderPageResponse {
  app_page: AppBuilderLayoutResponse[];
  record_page: AppBuilderLayoutResponse[];
}

export interface UpdateAppPageOrderDto {
  /** List layoutIds to update, in order */
  layoutIds: string[];
}

export interface CreateApplicationDto {
  name: string;
  description?: string;
}

export interface UpdateApplicationDto {
  name?: string;
  description?: string;
}

export interface AppProfileResponse {
  id: string;
  name: string;
}

export interface AssignProfileToAppDto {
  profileIds: string[];
}

export interface AppViewerCollection {
  data: AppViewerResponse[];
  pageInfo: PageInfoType;
}

export interface AppTabResponse {
  id: string;
  name: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
}

export interface CurrencyEntity {
  name: string;
  currencyCode: string;
  symbol?: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
}

export interface OrganizationResponse {
  name: string;
  description?: string;
  url?: string;
  email?: string;
  phoneNumber?: string;
  contactPerson?: string;
  defaultCurrency: CurrencyEntity;
  defaultLocale: string;
  currencyLocale: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
}

export interface UpdateOrgDto {
  defaultLocale?:
    | "af-NA"
    | "af-ZA"
    | "ak-GH"
    | "am-ET"
    | "ar-DZ"
    | "ar-BH"
    | "ar-EG"
    | "ar-IQ"
    | "ar-JO"
    | "ar-KW"
    | "ar-LB"
    | "ar-LY"
    | "ar-MA"
    | "ar-OM"
    | "ar-QA"
    | "ar-SA"
    | "ar-SD"
    | "ar-SY"
    | "ar-TN"
    | "ar-AE"
    | "ar-YE"
    | "hy-AM"
    | "as-IN"
    | "asa-TZ"
    | "az-Cyrl"
    | "az-Latn"
    | "bm-ML"
    | "eu-ES"
    | "be-BY"
    | "bem-ZM"
    | "bez-TZ"
    | "bn-BD"
    | "bn-IN"
    | "bs-BA"
    | "bg-BG"
    | "my-MM"
    | "yue-Hant-HK"
    | "ca-ES"
    | "tzm-Latn"
    | "chr-US"
    | "cgg-UG"
    | "zh-Hans"
    | "zh-Hant"
    | "kw-GB"
    | "hr-HR"
    | "cs-CZ"
    | "da-DK"
    | "nl-BE"
    | "nl-NL"
    | "ebu-KE"
    | "en-AS"
    | "en-AU"
    | "en-BE"
    | "en-BZ"
    | "en-BW"
    | "en-CA"
    | "en-GU"
    | "en-HK"
    | "en-IN"
    | "en-IE"
    | "en-IL"
    | "en-JM"
    | "en-MT"
    | "en-MH"
    | "en-MU"
    | "en-NA"
    | "en-NZ"
    | "en-MP"
    | "en-PK"
    | "en-PH"
    | "en-SG"
    | "en-ZA"
    | "en-TT"
    | "en-UM"
    | "en-VI"
    | "en-GB"
    | "en-US"
    | "en-ZW"
    | "et-EE"
    | "ee-GH"
    | "ee-TG"
    | "fo-FO"
    | "fil-PH"
    | "fi-FI"
    | "fr-BE"
    | "fr-BJ"
    | "fr-BF"
    | "fr-BI"
    | "fr-CM"
    | "fr-CA"
    | "fr-CF"
    | "fr-TD"
    | "fr-KM"
    | "fr-CG"
    | "fr-CD"
    | "fr-CI"
    | "fr-DJ"
    | "fr-GQ"
    | "fr-FR"
    | "fr-GA"
    | "fr-GP"
    | "fr-GN"
    | "fr-LU"
    | "fr-MG"
    | "fr-ML"
    | "fr-MQ"
    | "fr-MC"
    | "fr-NE"
    | "fr-RW"
    | "fr-RE"
    | "fr-BL"
    | "fr-MF"
    | "fr-SN"
    | "fr-CH"
    | "fr-TG"
    | "ff-SN"
    | "gl-ES"
    | "lg-UG"
    | "ka-GE"
    | "de-AT"
    | "de-BE"
    | "de-DE"
    | "de-LI"
    | "de-LU"
    | "de-CH"
    | "el-CY"
    | "el-GR"
    | "gu-IN"
    | "guz-KE"
    | "ha-Latn"
    | "haw-US"
    | "he-IL"
    | "hi-IN"
    | "hu-HU"
    | "is-IS"
    | "ig-NG"
    | "id-ID"
    | "ga-IE"
    | "it-IT"
    | "it-CH"
    | "ja-JP"
    | "kea-CV"
    | "kab-DZ"
    | "kl-GL"
    | "kln-KE"
    | "kam-KE"
    | "kn-IN"
    | "kk-Cyrl"
    | "km-KH"
    | "ki-KE"
    | "rw-RW"
    | "kok-IN"
    | "ko-KR"
    | "khq-ML"
    | "ses-ML"
    | "lag-TZ"
    | "lv-LV"
    | "lt-LT"
    | "luo-KE"
    | "luy-KE"
    | "mk-MK"
    | "jmc-TZ"
    | "kde-TZ"
    | "mg-MG"
    | "ms-BN"
    | "ms-MY"
    | "ml-IN"
    | "mt-MT"
    | "gv-GB"
    | "mr-IN"
    | "mas-KE"
    | "mas-TZ"
    | "mer-KE"
    | "mfe-MU"
    | "naq-NA"
    | "ne-IN"
    | "ne-NP"
    | "nd-ZW"
    | "nb-NO"
    | "nn-NO"
    | "nyn-UG"
    | "or-IN"
    | "om-ET"
    | "om-KE"
    | "ps-AF"
    | "fa-AF"
    | "fa-IR"
    | "pl-PL"
    | "pt-BR"
    | "pt-GW"
    | "pt-MZ"
    | "pt-PT"
    | "pa-Arab"
    | "ro-MD"
    | "ro-RO"
    | "rm-CH"
    | "rof-TZ"
    | "ru-MD"
    | "ru-RU"
    | "ru-UA"
    | "rwk-TZ"
    | "saq-KE"
    | "sg-CF"
    | "seh-MZ"
    | "sr-Cyrl"
    | "sr-Latn"
    | "sn-ZW"
    | "ii-CN"
    | "si-LK"
    | "sk-SK"
    | "sl-SI"
    | "xog-UG"
    | "so-DJ"
    | "so-ET"
    | "so-KE"
    | "so-SO"
    | "es-AR"
    | "es-BO"
    | "es-CL"
    | "es-CO"
    | "es-CR"
    | "es-DO"
    | "es-EC"
    | "es-SV"
    | "es-GQ"
    | "es-GT"
    | "es-HN"
    | "es-419"
    | "es-MX"
    | "es-NI"
    | "es-PA"
    | "es-PY"
    | "es-PE"
    | "es-PR"
    | "es-ES"
    | "es-US"
    | "es-UY"
    | "es-VE"
    | "sw-KE"
    | "sw-TZ"
    | "sv-FI"
    | "sv-SE"
    | "gsw-CH"
    | "shi-Latn"
    | "shi-Tfng"
    | "dav-KE"
    | "ta-IN"
    | "ta-LK"
    | "te-IN"
    | "teo-KE"
    | "teo-UG"
    | "th-TH"
    | "bo-CN"
    | "bo-IN"
    | "ti-ER"
    | "ti-ET"
    | "to-TO"
    | "tr-TR"
    | "uk-UA"
    | "ur-IN"
    | "ur-PK"
    | "uz-Arab"
    | "uz-Cyrl"
    | "uz-Latn"
    | "vi-VN"
    | "vun-TZ"
    | "cy-GB"
    | "yo-NG"
    | "zu-ZA";
  currencyLocale?:
    | "af-NA"
    | "af-ZA"
    | "ak-GH"
    | "am-ET"
    | "ar-DZ"
    | "ar-BH"
    | "ar-EG"
    | "ar-IQ"
    | "ar-JO"
    | "ar-KW"
    | "ar-LB"
    | "ar-LY"
    | "ar-MA"
    | "ar-OM"
    | "ar-QA"
    | "ar-SA"
    | "ar-SD"
    | "ar-SY"
    | "ar-TN"
    | "ar-AE"
    | "ar-YE"
    | "hy-AM"
    | "as-IN"
    | "asa-TZ"
    | "az-Cyrl"
    | "az-Latn"
    | "bm-ML"
    | "eu-ES"
    | "be-BY"
    | "bem-ZM"
    | "bez-TZ"
    | "bn-BD"
    | "bn-IN"
    | "bs-BA"
    | "bg-BG"
    | "my-MM"
    | "yue-Hant-HK"
    | "ca-ES"
    | "tzm-Latn"
    | "chr-US"
    | "cgg-UG"
    | "zh-Hans"
    | "zh-Hant"
    | "kw-GB"
    | "hr-HR"
    | "cs-CZ"
    | "da-DK"
    | "nl-BE"
    | "nl-NL"
    | "ebu-KE"
    | "en-AS"
    | "en-AU"
    | "en-BE"
    | "en-BZ"
    | "en-BW"
    | "en-CA"
    | "en-GU"
    | "en-HK"
    | "en-IN"
    | "en-IE"
    | "en-IL"
    | "en-JM"
    | "en-MT"
    | "en-MH"
    | "en-MU"
    | "en-NA"
    | "en-NZ"
    | "en-MP"
    | "en-PK"
    | "en-PH"
    | "en-SG"
    | "en-ZA"
    | "en-TT"
    | "en-UM"
    | "en-VI"
    | "en-GB"
    | "en-US"
    | "en-ZW"
    | "et-EE"
    | "ee-GH"
    | "ee-TG"
    | "fo-FO"
    | "fil-PH"
    | "fi-FI"
    | "fr-BE"
    | "fr-BJ"
    | "fr-BF"
    | "fr-BI"
    | "fr-CM"
    | "fr-CA"
    | "fr-CF"
    | "fr-TD"
    | "fr-KM"
    | "fr-CG"
    | "fr-CD"
    | "fr-CI"
    | "fr-DJ"
    | "fr-GQ"
    | "fr-FR"
    | "fr-GA"
    | "fr-GP"
    | "fr-GN"
    | "fr-LU"
    | "fr-MG"
    | "fr-ML"
    | "fr-MQ"
    | "fr-MC"
    | "fr-NE"
    | "fr-RW"
    | "fr-RE"
    | "fr-BL"
    | "fr-MF"
    | "fr-SN"
    | "fr-CH"
    | "fr-TG"
    | "ff-SN"
    | "gl-ES"
    | "lg-UG"
    | "ka-GE"
    | "de-AT"
    | "de-BE"
    | "de-DE"
    | "de-LI"
    | "de-LU"
    | "de-CH"
    | "el-CY"
    | "el-GR"
    | "gu-IN"
    | "guz-KE"
    | "ha-Latn"
    | "haw-US"
    | "he-IL"
    | "hi-IN"
    | "hu-HU"
    | "is-IS"
    | "ig-NG"
    | "id-ID"
    | "ga-IE"
    | "it-IT"
    | "it-CH"
    | "ja-JP"
    | "kea-CV"
    | "kab-DZ"
    | "kl-GL"
    | "kln-KE"
    | "kam-KE"
    | "kn-IN"
    | "kk-Cyrl"
    | "km-KH"
    | "ki-KE"
    | "rw-RW"
    | "kok-IN"
    | "ko-KR"
    | "khq-ML"
    | "ses-ML"
    | "lag-TZ"
    | "lv-LV"
    | "lt-LT"
    | "luo-KE"
    | "luy-KE"
    | "mk-MK"
    | "jmc-TZ"
    | "kde-TZ"
    | "mg-MG"
    | "ms-BN"
    | "ms-MY"
    | "ml-IN"
    | "mt-MT"
    | "gv-GB"
    | "mr-IN"
    | "mas-KE"
    | "mas-TZ"
    | "mer-KE"
    | "mfe-MU"
    | "naq-NA"
    | "ne-IN"
    | "ne-NP"
    | "nd-ZW"
    | "nb-NO"
    | "nn-NO"
    | "nyn-UG"
    | "or-IN"
    | "om-ET"
    | "om-KE"
    | "ps-AF"
    | "fa-AF"
    | "fa-IR"
    | "pl-PL"
    | "pt-BR"
    | "pt-GW"
    | "pt-MZ"
    | "pt-PT"
    | "pa-Arab"
    | "ro-MD"
    | "ro-RO"
    | "rm-CH"
    | "rof-TZ"
    | "ru-MD"
    | "ru-RU"
    | "ru-UA"
    | "rwk-TZ"
    | "saq-KE"
    | "sg-CF"
    | "seh-MZ"
    | "sr-Cyrl"
    | "sr-Latn"
    | "sn-ZW"
    | "ii-CN"
    | "si-LK"
    | "sk-SK"
    | "sl-SI"
    | "xog-UG"
    | "so-DJ"
    | "so-ET"
    | "so-KE"
    | "so-SO"
    | "es-AR"
    | "es-BO"
    | "es-CL"
    | "es-CO"
    | "es-CR"
    | "es-DO"
    | "es-EC"
    | "es-SV"
    | "es-GQ"
    | "es-GT"
    | "es-HN"
    | "es-419"
    | "es-MX"
    | "es-NI"
    | "es-PA"
    | "es-PY"
    | "es-PE"
    | "es-PR"
    | "es-ES"
    | "es-US"
    | "es-UY"
    | "es-VE"
    | "sw-KE"
    | "sw-TZ"
    | "sv-FI"
    | "sv-SE"
    | "gsw-CH"
    | "shi-Latn"
    | "shi-Tfng"
    | "dav-KE"
    | "ta-IN"
    | "ta-LK"
    | "te-IN"
    | "teo-KE"
    | "teo-UG"
    | "th-TH"
    | "bo-CN"
    | "bo-IN"
    | "ti-ER"
    | "ti-ET"
    | "to-TO"
    | "tr-TR"
    | "uk-UA"
    | "ur-IN"
    | "ur-PK"
    | "uz-Arab"
    | "uz-Cyrl"
    | "uz-Latn"
    | "vi-VN"
    | "vun-TZ"
    | "cy-GB"
    | "yo-NG"
    | "zu-ZA";
  name?: string;
  description?: string;
  url?: string;
  email?: string;
  phoneNumber?: string;
  contactPerson?: string;
}

export interface UpdateDefaultCurrencyDto {
  defaultCurrencyCode: string;
}

export interface BaseScreenFIComponent {
  id: string;
  name: string;
  type: string;
  component: "Input" | "Selection" | "DisplayText" | "FileDropzone" | string;
  props: RootProps | InputProps | SelectionProps | TextProps;
  children: string[];
}

export interface ScreenTemplateResponse {
  script: Record<string, BaseScreenFIComponent>;
  content: Record<string, any>;
  displayName: string;
  ctx: Record<string, any>;
}

export interface FlowBodyPayload {
  flowId: string;
}

export interface FlowExecutionResponse {
  ctxId: string;
}

export interface ContentValue {
  type: "variable" | "value" | "script";
  value: string;
}

export interface InputProps {
  label: string;
  isRequired?: boolean;
  inputType: "string" | "integer" | "float" | "date" | "time" | "date-time" | "boolean";
  defaultValue?: ContentValue;
}

export interface SelectionProps {
  label: string;
  isRequired?: boolean;
  selectionType: "single" | "multi";
  isRadio?: boolean;
  options: ContentValue[];
  defaultIndexes?: number[];
}

export interface TextProps {
  content: ContentValue;
}

export interface CtxFlowResponse {
  ctxId: string;
}

export interface DecisionConditionType {
  conditionNumber: number;
  sign:
    | "==="
    | "!=="
    | "<="
    | ">="
    | "<"
    | ">"
    | "isNull"
    | "isNotNull"
    | "startsWith"
    | "endsWith"
    | "contains"
    | "notContains"
    | "wasSet";
  value?: ContentValue;
  field: ContentValue;
}

export interface Outcome {
  name: string;
  displayName: string;
  conditionLogic: string;
  customConditionLogic: string;
  conditions: DecisionConditionType[];
}

export interface ConditionType {
  conditionNumber: number;
  field: string;
  sign:
    | "==="
    | "!=="
    | "<="
    | ">="
    | "<"
    | ">"
    | "isNull"
    | "isNotNull"
    | "startsWith"
    | "endsWith"
    | "contains"
    | "notContains"
    | "wasSet";
  value?: ContentValue;
}

export interface DataFlowCondition {
  conditionLogic: "none" | "and" | "or" | "custom";
  customConditionLogic?: string;
  conditionClauses: ConditionType[];
}

export interface DataFlowResponse {
  name: string;
  description?: string;
  display_name: string;
  is_active?: boolean;
  guid: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  action: "create" | "update" | "delete";
  object_name?: string;
  conditions?: DataFlowCondition;
  flow_type: "data";
}

export interface ScreenFlowResponse {
  name: string;
  description?: string;
  display_name: string;
  is_active?: boolean;
  guid: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  flow_type: "screen";
}

export interface ScheduleFlowResponse {
  name: string;
  description?: string;
  display_name: string;
  is_active?: boolean;
  guid: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  flow_type: "time-based";
  start_date: string;
  start_time: string;
  frequency: "once" | "daily" | "weekly";
}

export interface ActionFlowResponse {
  name: string;
  description?: string;
  display_name: string;
  is_active?: boolean;
  guid: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  flow_type: "action";
}

export interface UpdateDataFlowDto {
  action?: "create" | "update" | "delete";
  object_name?: string;
  conditions?: DataFlowCondition;
  display_name?: string;
  description?: string;
}

export interface UpdateScreenFlowDto {
  display_name?: string;
  description?: string;
}

export interface UpdateScheduleFlowDto {
  /** Format: yyyy-MM-dd */
  start_date: string;

  /** Accept 24-hour clock format: HH:mm, with 15 minute step */
  start_time: string;
  frequency: "once" | "daily" | "weekly";
  display_name?: string;
  description?: string;
}

export interface FlowCollection {
  data: (DataFlowResponse | ScreenFlowResponse | ScheduleFlowResponse | ActionFlowResponse)[];
  pageInfo: PageInfoType;
}

export interface FlowCreateDto {
  name: string;
  display_name: string;
  description?: string;
  flow_type: "screen" | "action" | "time-based" | "data";
}

export interface AssignmentOperator {
  displayName: string;
  value:
    | "assign"
    | "add"
    | "subTract"
    | "addAtEnd"
    | "addAtStart"
    | "removeAll"
    | "removeFirst"
    | "removeLast"
    | "removeAtIndex";
}

export interface FlowVariableSchemaResponse {
  name: string;
  type: string;
  displayName?: string;
}

export interface ActionFlowItemResponse {
  guid: string;
  name: string;
  display_name: string;
  flow_id: string;
  node_attributes: Record<string, any>;
  max_connections: number;
  has_fault_connection: boolean;
  action_metadata_name: string;
  input_map: Record<string, ContentValue>;
  type: "action";
}

export interface AssignmentVariable {
  variable: string;
  sign: string;
  value: ContentValue;
}

export interface AssignmentFlowItemResponse {
  guid: string;
  name: string;
  display_name: string;
  flow_id: string;
  node_attributes: Record<string, any>;
  max_connections: number;
  has_fault_connection: boolean;
  description: string;
  assignment_variables: AssignmentVariable[];
  type: "assignment";
}

export interface DecisionFlowItemResponse {
  guid: string;
  name: string;
  display_name: string;
  flow_id: string;
  node_attributes: Record<string, any>;
  max_connections: number;
  has_fault_connection: boolean;
  outcomes: Record<string, Outcome>;
  order: string[];
  default_outcome_label: string;
  type: "decision";
}

export interface LoopFlowItemResponse {
  guid: string;
  name: string;
  display_name: string;
  flow_id: string;
  node_attributes: Record<string, any>;
  max_connections: number;
  has_fault_connection: boolean;
  collection_variable_name: string;
  collection_variable_type: string;
  collection_record_object_name?: string;
  loop_order: string;
  type: "loop";
}

export interface RecordMediaUpdate {
  mediaId: ContentValue;
  tagName: ContentValue;
}

export interface RecordMediaConfig {
  update?: RecordMediaUpdate[];

  /** Config tagName to remove */
  remove?: ContentValue[];
}

export interface CreateConfigResponse {
  /** { fieldName: ContentValue } */
  payload: Record<string, ContentValue>;
  media?: RecordMediaConfig;
}

export interface RecordCreateFlowItemResponse {
  guid: string;
  name: string;
  display_name: string;
  flow_id: string;
  node_attributes: Record<string, any>;
  max_connections: number;
  has_fault_connection: boolean;
  config: CreateConfigResponse;
  object_name: string;
  type: "create";
}

export interface DeleteConfigResponse {
  /** Guid variable to delete the record */
  guid: ContentValue;
}

export interface RecordDeleteFlowItemResponse {
  guid: string;
  name: string;
  display_name: string;
  flow_id: string;
  node_attributes: Record<string, any>;
  max_connections: number;
  has_fault_connection: boolean;
  config: DeleteConfigResponse;
  object_name: string;
  type: "delete";
}

export interface ReadConditionResponse {
  fieldName: string;
  sign: string;
  value: ContentValue;
}

export interface ReadConfigResponse {
  conditions: ReadConditionResponse[];
  conditionLogic: string;
  sortBy: string;
  sortOrder: string;
  getOptions: string;
}

export interface RecordReadFlowItemResponse {
  guid: string;
  name: string;
  display_name: string;
  flow_id: string;
  node_attributes: Record<string, any>;
  max_connections: number;
  has_fault_connection: boolean;
  config: ReadConfigResponse;
  object_name: string;
  type: "read";
}

export interface UpdateConfigResponse {
  media?: RecordMediaConfig;

  /** { fieldName: ContentValue } */
  payload: Record<string, ContentValue>;

  /** Guid variable to update the record */
  guid: ContentValue;
}

export interface RecordUpdateFlowItemResponse {
  guid: string;
  name: string;
  display_name: string;
  flow_id: string;
  node_attributes: Record<string, any>;
  max_connections: number;
  has_fault_connection: boolean;
  config: UpdateConfigResponse;
  object_name: string;
  type: "update";
}

export interface ScreenFlowItemResponse {
  guid: string;
  name: string;
  display_name: string;
  flow_id: string;
  node_attributes: Record<string, any>;
  max_connections: number;
  has_fault_connection: boolean;
  layout_script: Record<string, BaseScreenFIComponent>;
  type: "screen";
}

export interface SubflowFlowItemResponse {
  guid: string;
  name: string;
  display_name: string;
  flow_id: string;
  node_attributes: Record<string, any>;
  max_connections: number;
  has_fault_connection: boolean;
  input_map: Record<string, ContentValue>;
  subflow_id: string;
  type: "sub-flow";
}

export interface CreateActionFlowItemDto {
  action_metadata_name: string;

  /** key should be <location>.<name> */
  input_map?: Record<string, ContentValue>;
  type: "action";
  name: string;
  display_name: string;
  node_attributes: object;
}

export interface AssignmentVariableDto {
  variable: string;
  sign:
    | "assign"
    | "add"
    | "subTract"
    | "addAtEnd"
    | "addAtStart"
    | "removeAll"
    | "removeFirst"
    | "removeLast"
    | "removeAtIndex";
  value?: ContentValue;
}

export interface CreateAssignmentFlowItemDto {
  assignment_variables: AssignmentVariableDto[];
  type: "assignment";
  description?: string;
  name: string;
  display_name: string;
  node_attributes: object;
}

export interface DecisionOutcomeData {
  /** Api name of the decision outcome */
  name: string;
  displayName: string;

  /** Use AND, OR and parentheses to customize the logic. For example, (1 AND 2 AND 3) OR 4 */
  customConditionLogic?: string;
  conditionLogic: "and" | "or" | "custom";
  conditions: DecisionConditionType[];
}

export type DecisionOutcomesMap = object;

export interface CreateDecisionFlowItemDto {
  default_outcome_label: string;
  order: string[];
  outcomes: DecisionOutcomesMap;
  type: "decision";
  name: string;
  display_name: string;
  node_attributes: object;
}

export interface CreateLoopFlowItemDto {
  type: "loop";
  collection_variable_name: string;
  loop_order: "ASC" | "DESC";
  name: string;
  display_name: string;
  node_attributes: object;
}

export interface RecordCreateConfig {
  /** { fieldName: ContentValue } */
  payload: Record<string, ContentValue>;
  media?: RecordMediaConfig;
}

export interface CreateRecordCreateDto {
  type: "create";
  config: RecordCreateConfig;
  object_name: string;
  name: string;
  display_name: string;
  node_attributes: object;
}

export interface RecordDeleteConfig {
  /** Guid variable to delete the record */
  guid: ContentValue;
}

export interface CreateRecordDeleteDto {
  type: "delete";
  config: RecordDeleteConfig;
  object_name: string;
  name: string;
  display_name: string;
  node_attributes: object;
}

export interface RecordReadCondition {
  fieldName: string;
  sign: "equals" | "notEquals" | "isNull" | "isNotNull";
  value?: ContentValue;
}

export interface RecordReadConfig {
  conditionLogic: "and";

  /** FieldName to sort the result */
  sortBy: string;
  sortOrder: "ASC" | "DESC";

  /** Get and store all or only the first record */
  getOptions: "one" | "all";
  conditions: RecordReadCondition[];
}

export interface CreateRecordReadDto {
  type: "read";
  config: RecordReadConfig;
  object_name: string;
  name: string;
  display_name: string;
  node_attributes: object;
}

export interface RecordUpdateConfig {
  /** { fieldName: ContentValue } */
  payload: Record<string, ContentValue>;
  media?: RecordMediaConfig;

  /** Guid variable to update the record */
  guid: ContentValue;
}

export interface CreateRecordUpdateDto {
  type: "update";
  config: RecordUpdateConfig;
  object_name: string;
  name: string;
  display_name: string;
  node_attributes: object;
}

export interface ScreenFIComponentDto {
  id: string;
  name: string;
  type: string;
  component: "Input" | "Selection" | "DisplayText" | "FileDropzone" | string;
  props: RootProps | InputProps | SelectionProps | TextProps;
  children: string[];
}

export interface CreateScreenFlowItemDto {
  layout_script: Record<string, ScreenFIComponentDto>;
  type: "screen";
  name: string;
  display_name: string;
  node_attributes: object;
}

export interface CreateSubflowItemDto {
  input_map?: Record<string, ContentValue>;
  type: "sub-flow";
  subflow_id: string;
  name: string;
  display_name: string;
  node_attributes: object;
}

export interface UpdateActionFlowItemDto {
  action_metadata_name?: string;

  /** key should be <location>.<name> */
  input_map?: Record<string, ContentValue>;
  display_name?: string;
  node_attributes?: object;
}

export interface UpdateAssignmentFlowItemDto {
  description?: string;
  assignment_variables?: AssignmentVariableDto[];
  display_name?: string;
  node_attributes?: object;
}

export interface UpdateDecisionFlowItemDto {
  default_outcome_label?: string;
  order?: string[];
  outcomes?: DecisionOutcomesMap;
  display_name?: string;
  node_attributes?: object;
}

export interface UpdateLoopFlowItemDto {
  collection_variable_name?: string;
  loop_order?: "ASC" | "DESC";
  display_name?: string;
  node_attributes?: object;
}

export interface UpdateRecordCreateDto {
  object_name?: string;
  config?: RecordCreateConfig;
  display_name?: string;
  node_attributes?: object;
}

export interface UpdateRecordDeleteDto {
  object_name?: string;
  config?: RecordDeleteConfig;
  display_name?: string;
  node_attributes?: object;
}

export interface UpdateRecordReadDto {
  config?: RecordReadConfig;
  object_name?: string;
  display_name?: string;
  node_attributes?: object;
}

export interface UpdateRecordUpdateDto {
  object_name?: string;
  config?: RecordUpdateConfig;
  display_name?: string;
  node_attributes?: object;
}

export interface UpdateScreenFlowItemDto {
  layout_script?: Record<string, ScreenFIComponentDto>;
  display_name?: string;
  node_attributes?: object;
}

export interface UpdateSubflowItemDto {
  input_map?: Record<string, ContentValue>;
  subflow_id?: string;
  display_name?: string;
  node_attributes?: object;
}

export interface FlowItemConnectionResponse {
  name: string;
  src_item_name: string;
  trg_item_name: string;
  src_outcome_name?: string;
  is_fault?: boolean;
  flow_id: string;
  node_attributes?: object;
  orgId: number;
  objName: string;
  guid: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
}

export interface CreateFlowItemConnectionDto {
  src_item_name: string;
  trg_item_name: string;
  is_fault?: boolean;
  src_outcome_name?: string;
  node_attributes?: object;
}

export interface UpdateFlowItemConnectionDto {
  node_attributes?: object;
}

export interface FlowInputVariableListResponse {
  name: string;
  display_name: string;
  type: "string" | "integer" | "float" | "boolean" | "date" | "date-time" | "time" | "record" | "object" | "collection";
  record_object_name?: string;
}

export interface FlowVariableResponse {
  is_constant?: boolean;
  const_value?: string;
  default_value?: string;
  flow_id: string;
  name: string;
  display_name: string;
  type: "string" | "integer" | "float" | "boolean" | "date" | "date-time" | "time" | "record" | "object" | "collection";
  record_object_name?: string;
  is_for_input?: boolean;
  is_for_output?: boolean;
  is_generated?: boolean;
  collection_data_type?:
    | "string"
    | "integer"
    | "float"
    | "boolean"
    | "date"
    | "date-time"
    | "time"
    | "record"
    | "object"
    | "collection";
  guid: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  schema?: FlowVariableSchemaResponse[];
}

export interface CreateFlowVariableDto {
  type: "string" | "integer" | "float" | "boolean" | "date" | "date-time" | "time" | "record" | "collection";
  is_constant?: boolean;
  const_value?: string;
  default_value?: string;
  name: string;
  display_name: string;
  record_object_name?: string;
  is_for_input?: boolean;
  is_for_output?: boolean;
  collection_data_type?:
    | "string"
    | "integer"
    | "float"
    | "boolean"
    | "date"
    | "date-time"
    | "time"
    | "record"
    | "object"
    | "collection";
}

export interface UpdateFlowVariableDto {
  display_name?: string;
  is_for_input?: boolean;
  is_for_output?: boolean;
}

export interface ProfileResponse {
  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  id: string;
  name: string;
  description: string;
}

export interface ProfileCollection {
  data: ProfileResponse[];
  pageInfo: PageInfoType;
}

export interface CreateProfileDto {
  name: string;
}

export interface UpdateProfileDto {
  name?: string;
}

export interface PasswordCredential {
  username: string;
  password: string;
}

export interface CredentialResponse {
  id: string;
  url: string;
  authProtocol?: "password" | "oauth" | "jwt";
  identityType: "anon" | "orgWide" | "perUser";
  name: string;
  displayName: string;
  description?: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
}

export interface CredentialCollection {
  data: CredentialResponse[];
  pageInfo: PageInfoType;
}

export interface CredentialCreateDto {
  credential?: PasswordCredential;
  name: string;
  displayName: string;
  url: string;
  identityType: "anon" | "orgWide" | "perUser";
  authProtocol?: "password" | "oauth" | "jwt";
}

export interface CredentialUpdateDto {
  credential?: PasswordCredential;
  displayName?: string;
  url?: string;
  identityType?: "anon" | "orgWide" | "perUser";
  authProtocol?: "password" | "oauth" | "jwt";
}

export interface ScriptDetailComponent {
  id: string;
  label: string;
  isPrimary: boolean;
  fieldExists: boolean;
}

export interface ObjRecordLayoutResponse {
  id: string;
  name: string;
  components: ScriptDetailComponent[];
}

export interface BundleCredentialResponse {
  name: string;
  displayName: string;
}

export interface HeaderInputType {
  isRequired: boolean;
  default?: string;
  description?: string;
}

export interface RequestInputType {
  type: "string" | "number" | "object" | "array" | "boolean";
  default?: string | number | object | boolean | (string | number | object | boolean)[];
  isRequired: boolean;
  description?: string;
}

export interface InputMetadata {
  header: Record<string, HeaderInputType>;
  query: Record<string, RequestInputType>;
  path: string[];
  body?: RequestInputType;
}

export interface ActionMetadataResponse {
  outputs: Record<string, ResponseOutputType>;
  isStream: boolean;
  credential: BundleCredentialResponse;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  name: string;
  displayName: string;
  description?: string;
  id: string;
  path: string;
  method: "post" | "get" | "put" | "delete";
  inputs: InputMetadata;
}

export interface ActionMetadataCollection {
  data: ActionMetadataResponse[];
  pageInfo: PageInfoType;
}

export interface ResponseOutputType {
  type: "string" | "number" | "object" | "array" | "boolean";
  description?: string;
}

export interface InputMapDto {
  header: Record<string, HeaderInputType>;
  query: Record<string, RequestInputType>;
  body?: RequestInputType;
}

export interface ActionMetadataCreateDto {
  outputs?: Record<string, ResponseOutputType>;
  name: string;
  displayName: string;
  description?: string;
  credentialName: string;
  path: string;
  method: "post" | "get" | "put" | "delete";
  inputs?: InputMapDto;
}

export interface ActionMetadataUpdateDto {
  outputs?: Record<string, ResponseOutputType>;
  displayName?: string;
  description?: string;
  credentialName?: string;
  path?: string;
  method?: "post" | "get" | "put" | "delete";
  inputs?: InputMapDto;
}

export interface OrganizationCurrencyResponse {
  symbol?: string;
  name: string;
  displayName: string;
  currencyCode: string;
  isActive: boolean;
  rate?: number;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
}

export interface OrganizationCurrencyCollection {
  data: OrganizationCurrencyResponse[];
  pageInfo: PageInfoType;
}

export interface UpdateCurrencyDto {
  displayName?: string;
  isActive?: boolean;
}

export interface UpdateCurrencyRateDto {
  rate: number;
}

export interface CurrencyRateEntity {
  fromCurrencyCode: string;
  toCurrencyCode: string;

  /** @format date-time */
  dateFrom: string;
  rate: number;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
}

export interface CurrencyRateCollection {
  data: CurrencyRateEntity[];
  pageInfo: PageInfoType;
}

export interface ExtDataSourceResponseMap {
  total?: string;
  data?: string;
  externalId?: string;
}

export interface ActionConfigurationResponse {
  outputs: Record<string, ResponseOutputType>;
  name: string;
  displayName: string;
  inputs: InputMetadata;
  method: "post" | "get" | "put" | "delete";
}

export interface RequestVarType {
  variable: string;
  isRequired: boolean;
}

export interface ExtObjectDataSourceResponse {
  inputMap: Record<string, ExtDataSourceInputMap>;
  responseMap: ExtDataSourceResponseMap;

  /** Status code for data */
  statusCode: number;
  actionMetadata: ActionConfigurationResponse;
  availableRequestVars: RequestVarType[];
  availableResponseVars: string[];

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
}

export interface ExtDataSourceInputMap {
  /** For template, to using variable, content should look like: {\$variable-name} */
  value: string | number | object | boolean;
  type: "value" | "variable" | "template";
}

export interface UpdateDataSourceDto {
  /** Key should start with path.,body,header.,query. */
  inputMap?: Record<string, ExtDataSourceInputMap>;

  /**
   * Status code for data
   * @min 100
   * @max 599
   */
  statusCode?: number;
  responseMap?: object;
  actionMetadataName?: string;
}

export interface TestDataSourceRequestResponse {
  headers?: Record<string, string>;
  data?: string | number | object | boolean | (string | number | object | boolean)[];
  url?: string;
  error?: string;
  status?: number;
}

export interface ExtDataTypeResponse {
  systemType: object;
  name: string;
  displayName: string;
  description?: string;
  validateScript: string;
  attributes: AttributeType;
}

export interface ExtFieldAttributes {
  subType?: string;
  isSearchable?: boolean;
  isSortable?: boolean;
}

export interface ExtFieldResponse {
  dataType: ExtDataTypeResponse;
  attributes: ExtFieldAttributes;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  name: string;
  displayName: string;
  description?: string;
  isSystemDefault: boolean;
  id: string;
  isRequired: boolean;
  value?: string;
  isExternalId: boolean;
  pickListId?: number;
}

export interface ExtFieldAttributesDto {
  subType?: string;
  isSearchable?: boolean;
  isSortable?: boolean;
}

export interface ExtFieldCreateDto {
  typeName:
    | "numeric"
    | "text"
    | "dateTime"
    | "boolean"
    | "pickList"
    | "json"
    | "currency"
    | "externalRelation"
    | "indirectRelation"
    | "relation";
  isRequired?: boolean;
  isExternalId?: boolean;

  /**
   * value can be:
   *
   *     For pickList: Json array of options
   *     For relation/externalRelation: targetObjectName
   *     For indirectRelation: targetObjectName.externalFieldName
   *
   */
  value?: string;
  pickListId?: number;
  attributes: ExtFieldAttributesDto;
  name: string;
  displayName: string;
  description?: string;
}

export interface ExtFieldUpdateDto {
  isRequired?: boolean;
  isExternalId?: boolean;

  /**
   * value can be:
   *
   *     For pickList: Json array of options
   *     For relation/externalRelation: targetObjectName
   *     For indirectRelation: targetObjectName.externalFieldName
   *
   */
  value?: string;
  pickListId?: number;
  attributes?: ExtFieldAttributes;
  displayName?: string;
  description?: string;
}

export interface ExtFieldDto {
  data?: ExtFieldCreateDto | ExtFieldUpdateDto;
  updateLayouts?: LayoutComponent[];
  action: "create" | "update" | "delete";
  name?: string;
}

export interface ExternalIdFieldResponse {
  name: string;
  displayName: string;
}

export interface IndirectRelationResponse {
  fields: ExternalIdFieldResponse[];
  name: string;
  displayName: string;
}

export interface FieldAttributeMeta {
  displayName: string;
  options?: string[];
}

export interface DataTypeExtendedResponse {
  availableAttributes: Record<string, FieldAttributeMeta>;
  name: string;
  displayName: string;
  description?: string;
  systemType:
    | "numeric"
    | "text"
    | "dateTime"
    | "boolean"
    | "pickList"
    | "json"
    | "generated"
    | "currency"
    | "externalRelation"
    | "indirectRelation"
    | "relation";
  validateScript: string;
  attributes: AttributeType;
}

export interface MediaCollection {
  data: MediaResponse[];
  pageInfo: PageInfoType;
}

export interface UpdateMediaDto {
  displayName?: string;
  description?: string;
}
