import axios from 'axios'
import { QueryFunctionContext } from 'react-query'
import { store } from '../../redux'
import { authActions } from '../../redux/slices/authSlice'

export type GUID = {
  guid: string
}

export type ID = {
  id: string
}

export type SearchParams = {
  searchText?: string
}

export type PaginationParams = {
  limit?: number
  offset?: number
}

export enum SortOrder {
  Asc = 'ASC',
  Desc = 'DESC',
}

export type SortParams = {
  sortBy?: string
  sortOrder?: SortOrder
}

export enum PostAction {
  Create = 'create',
  Update = 'update',
  Delete = 'delete',
}

export type QueryContext<Params> = QueryFunctionContext<[string, Params] | [string, Params, string]>

export const request = axios.create({
  baseURL: process.env.REACT_APP_API_HOST || '',
})

request.interceptors.request.use(configs => {
  return {
    ...configs,
    headers: {
      ...(configs.headers || {}),
      Authorization: `Bearer ${store.getState().auth.accessToken}`,
      'Content-Type': 'application/json; charset=utf-8',
    },
  }
})

request.interceptors.response.use(
  response => response,
  error => {
    if (error) {
      if (error.response && error.response.status === 401) {
        store.dispatch(authActions.removeToken())
        return
      }
      throw error
    }
  },
)
