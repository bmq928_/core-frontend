import { AxiosPromise } from 'axios'
import { buildKeys } from '../../../utils/utils'
import { OrganizationResponse, UpdateDefaultCurrencyDto, UpdateOrgDto } from '../models'
import { request } from '../request'

type LocalesResponse = { [key: string]: string }

export const Organization = {
  getOrganization(): AxiosPromise<OrganizationResponse> {
    return request.get('/v1/organization')
  },
  setDefaultCurrency(body: UpdateDefaultCurrencyDto): AxiosPromise<OrganizationResponse> {
    return request.post('/v1/organization/set-default-currency', body)
  },
  updateOrganization(body: UpdateOrgDto): AxiosPromise<OrganizationResponse> {
    return request.put('/v1/organization', body)
  },
  getLocales(): AxiosPromise<LocalesResponse> {
    return request.get('/v1/organization/locales')
  },
}

export const OrganizationQueryKey = buildKeys('Organization', Organization)
