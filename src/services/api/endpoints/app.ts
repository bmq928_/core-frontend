import { AxiosPromise } from 'axios'
import { buildKeys } from '../../../utils/utils'
import { ApplicationCollection, AppViewerResponse, ComponentResponse, LayoutResponse } from '../models'
import { PaginationParams, QueryContext, request, SortParams } from '../request'

export type AppsArguments = {
  getApps: { searchText: string } & PaginationParams & SortParams
  getAppPages: { id: string }
  getAppScript: { layoutId: string }
  getApp: { id: string }
}

export const Apps = {
  getApps({ queryKey }: QueryContext<Partial<AppsArguments['getApps']>>): AxiosPromise<ApplicationCollection> {
    return request.get('/v1/apps', { params: queryKey[1] })
  },
  getAppPages({ queryKey }: QueryContext<Partial<AppsArguments['getAppPages']>>): AxiosPromise<LayoutResponse[]> {
    const { id } = queryKey[1]
    return request.get(`/v1/apps/${id}/pages`)
  },
  getAppScript({
    queryKey,
  }: QueryContext<Partial<AppsArguments['getAppScript']>>): AxiosPromise<Record<string, ComponentResponse>> {
    const { layoutId } = queryKey[1]
    return request.get(`/v1/app-pages/${layoutId}`)
  },
  getApp({ queryKey }: QueryContext<Partial<AppsArguments['getApp']>>): AxiosPromise<AppViewerResponse> {
    const { id } = queryKey[1]
    return request.get(`/v1/apps/${id}`)
  },
}

export const AppQueryKey = buildKeys('Apps', Apps)
