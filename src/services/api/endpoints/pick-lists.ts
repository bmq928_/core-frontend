import { AxiosPromise } from 'axios'
import { buildKeys } from '../../../utils/utils'
import { CreatePickListDto, PickListCollection, PickListResponse, UpdatePickListDto } from '../models'
import { PaginationParams, QueryContext, request, SearchParams, SortParams } from '../request'
export const PickLists = {
  getAllPickLists({
    queryKey,
  }: QueryContext<
    SearchParams & PaginationParams & SortParams & { status?: 'ALL' | 'INACTIVE' | 'ACTIVE' }
  >): AxiosPromise<PickListCollection> {
    const params = queryKey[1]
    return request.get(`/v1/pick-lists`, { params })
  },
  getPickList({ queryKey }: QueryContext<{ name: string }>): AxiosPromise<PickListResponse> {
    return request.get(`/v1/pick-lists/${queryKey[1].name}`)
  },
  createPickList(body: CreatePickListDto): AxiosPromise<PickListResponse> {
    return request.post(`/v1/pick-lists`, body)
  },
  updatePickList({ name, ...body }: { name: string } & UpdatePickListDto): AxiosPromise<PickListResponse> {
    return request.put(`/v1/pick-lists/${name}`, body)
  },
  activatePickList({ name }: { name: string }): AxiosPromise<PickListResponse> {
    return request.put(`/v1/pick-lists/${name}/activate`)
  },
  deactivatePickList({ name }: { name: string }): AxiosPromise<PickListResponse> {
    return request.put(`/v1/pick-lists/${name}/deactivate`)
  },
}

export const PickListsQueryKey = buildKeys('PickList', PickLists)
