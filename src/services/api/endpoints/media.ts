import { AxiosPromise } from 'axios'
import { buildKeys } from '../../../utils/utils'
import { MediaCollection, MediaResponse, UpdateMediaDto } from '../models'
import { PaginationParams, QueryContext, request, SearchParams, SortParams } from '../request'

export const Media = {
  getAllMedia({ queryKey }: QueryContext<SearchParams & PaginationParams & SortParams>): AxiosPromise<MediaCollection> {
    const params = queryKey[1]
    return request.get(`/v1/media`, { params })
  },
  getMedia({ queryKey }: QueryContext<{ id: string }>): AxiosPromise<MediaResponse> {
    return request.get(`/v1/media/${queryKey[1].id}`)
  },
  updateMedia({ id, ...body }: { id: string } & UpdateMediaDto): AxiosPromise<MediaResponse> {
    return request.put(`/v1/media/${id}`, body)
  },
}

export const MediaQueryKeys = buildKeys('Media', Media)
