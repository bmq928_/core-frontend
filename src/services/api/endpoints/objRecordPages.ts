import { AxiosPromise } from 'axios'
import { buildKeys } from '../../../utils/utils'
import { ObjRecordLayoutResponse } from '../models'
import { QueryContext, request } from '../request'

export const ObjectRecordPages = {
  getObjectRecordPages: ({
    queryKey,
  }: QueryContext<{ objName: string; fieldName: string }>): AxiosPromise<ObjRecordLayoutResponse[]> => {
    const { objName, fieldName } = queryKey[1]
    return request.get(`/v1/obj-record-pages/${objName}/${fieldName}`)
  },
}

export const ObjectRecordPagesQueryKey = buildKeys('ObjectRecordPages', ObjectRecordPages)
