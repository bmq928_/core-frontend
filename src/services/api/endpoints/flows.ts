import { AxiosPromise } from 'axios'
import { buildKeys } from '../../../utils/utils'
import {
  ActionFlowItemResponse,
  ActionFlowResponse,
  AssignmentFlowItemResponse,
  AssignmentOperator,
  CreateActionFlowItemDto,
  CreateAssignmentFlowItemDto,
  CreateDecisionFlowItemDto,
  CreateFlowItemConnectionDto,
  CreateFlowVariableDto,
  CreateLoopFlowItemDto,
  CreateRecordCreateDto,
  CreateRecordDeleteDto,
  CreateRecordReadDto,
  CreateRecordUpdateDto,
  CreateScreenFlowItemDto,
  CreateSubflowItemDto,
  DataFlowResponse,
  DecisionFlowItemResponse,
  FlowCollection,
  FlowCreateDto,
  FlowExecutionResponse,
  FlowInputVariableListResponse,
  FlowItemConnectionResponse,
  FlowVariableResponse,
  FlowVariableSchemaResponse,
  LoopFlowItemResponse,
  RecordCreateFlowItemResponse,
  RecordDeleteFlowItemResponse,
  RecordReadFlowItemResponse,
  RecordUpdateFlowItemResponse,
  ScheduleFlowResponse,
  ScreenFlowItemResponse,
  ScreenFlowResponse,
  ScreenTemplateResponse,
  SubflowFlowItemResponse,
  UpdateActionFlowItemDto,
  UpdateAssignmentFlowItemDto,
  UpdateDataFlowDto,
  UpdateDecisionFlowItemDto,
  UpdateFlowVariableDto,
  UpdateLoopFlowItemDto,
  UpdateRecordCreateDto,
  UpdateRecordDeleteDto,
  UpdateRecordReadDto,
  UpdateRecordUpdateDto,
  UpdateScheduleFlowDto,
  UpdateScreenFlowDto,
  UpdateScreenFlowItemDto,
  UpdateSubflowItemDto,
} from '../models'
import { PaginationParams, QueryContext, request, SearchParams, SortParams } from '../request'

export type BaseCreateFlowItemDto =
  | CreateActionFlowItemDto
  | CreateAssignmentFlowItemDto
  | CreateDecisionFlowItemDto
  | CreateLoopFlowItemDto
  | CreateRecordCreateDto
  | CreateRecordDeleteDto
  | CreateRecordReadDto
  | CreateRecordUpdateDto
  | CreateScreenFlowItemDto
  | CreateSubflowItemDto

export type BaseUpdateFlowItemDto =
  | UpdateActionFlowItemDto
  | UpdateAssignmentFlowItemDto
  | UpdateDecisionFlowItemDto
  | UpdateLoopFlowItemDto
  | UpdateRecordCreateDto
  | UpdateRecordDeleteDto
  | UpdateRecordReadDto
  | UpdateRecordUpdateDto
  | UpdateScreenFlowItemDto
  | UpdateSubflowItemDto

export type FlowResponse = DataFlowResponse | ScreenFlowResponse | ScheduleFlowResponse | ActionFlowResponse
export type FlowType = 'screen' | 'action' | 'time-based' | 'data'

export type FlowItem =
  | ScreenFlowItemResponse
  | SubflowFlowItemResponse
  | DecisionFlowItemResponse
  | AssignmentFlowItemResponse
  | ActionFlowItemResponse
  | LoopFlowItemResponse
  | RecordCreateFlowItemResponse
  | RecordReadFlowItemResponse
  | RecordUpdateFlowItemResponse
  | RecordDeleteFlowItemResponse

export const getScreenFlowSSEUrl = (ctxId: string) => `${process.env.REACT_APP_API_HOST || ''}/v1/sse/flow/${ctxId}`

export const Flows = {
  getScreenFlows: ({
    queryKey,
  }: QueryContext<SearchParams & PaginationParams & SortParams>): AxiosPromise<FlowCollection> => {
    return request.get('/v1/flow-builder/screen-flows', { params: queryKey[1] })
  },
  //
  startScreenFlow: ({ flowId }: { flowId: string }): AxiosPromise<FlowExecutionResponse> => {
    return request.post('/v1/flow-runner/screen', { flowId })
  },
  getScreenFlowLayout: ({ queryKey }: QueryContext<{ ctxId: string }>): AxiosPromise<ScreenTemplateResponse> => {
    const { ctxId } = queryKey[1]
    return request.get(`/v1/flow-runner/screen/${ctxId}`)
  },
  submitScreenFlowData: ({ ctxId, body }: { ctxId: string; body?: Record<string, any> }): AxiosPromise<void> => {
    return request.post(`/v1/flow-runner/screen/${ctxId}`, body)
  },
  //
  getFlowItems: ({ queryKey }: QueryContext<{ flowName: string }>): AxiosPromise<FlowItem[]> => {
    return request.get(`/v1/flow-items/${queryKey[1].flowName}`)
  },
  getFlowItem: ({ queryKey }: QueryContext<{ flowName: string; flowItemName: string }>): AxiosPromise<FlowItem> => {
    return request.get(`/v1/flow-items/${queryKey[1].flowName}/${queryKey[1].flowItemName}`)
  },
  createFlowItem: ({ flowName, data }: { flowName: string; data: BaseCreateFlowItemDto }): AxiosPromise<FlowItem> => {
    return request.post(`/v1/flow-items/${flowName}`, data)
  },
  updateFlowItem: ({
    flowName,
    flowItemName,
    data,
  }: {
    flowName: string
    flowItemName: string
    data: BaseUpdateFlowItemDto
  }): AxiosPromise<FlowItem> => {
    return request.put(`/v1/flow-items/${flowName}/${flowItemName}`, data)
  },
  deleteFlowItem: ({ flowName, flowItemName }: { flowName: string; flowItemName: string }): AxiosPromise<void> => {
    return request.delete(`/v1/flow-items/${flowName}/${flowItemName}`)
  },
  //
  getSubFlows: ({
    queryKey,
  }: QueryContext<
    { flowName: string } & SearchParams & PaginationParams & SortParams
  >): AxiosPromise<FlowCollection> => {
    return request.get(`/v1/flow-builder/${queryKey[1].flowName}/sub-flows`, {
      params: { searchText: queryKey[1].searchText },
    })
  },
  getFlowInputVariables: ({
    queryKey,
  }: QueryContext<{ flowName: string }>): AxiosPromise<FlowInputVariableListResponse[]> => {
    return request.get(`/v1/flows/${queryKey[1].flowName}/input-variables`)
  },

  //
  getFlowsList: ({
    queryKey,
  }: QueryContext<SearchParams & PaginationParams & SortParams>): AxiosPromise<FlowCollection> => {
    return request.get('/v1/flows', { params: queryKey[1] })
  },
  //
  getFlow: ({ queryKey }: QueryContext<{ flowName: string }>): AxiosPromise<FlowResponse> => {
    return request.get(`/v1/flows/${queryKey[1].flowName}`)
  },
  createFlow: (args: FlowCreateDto): AxiosPromise<FlowResponse> => {
    return request.post('/v1/flows', args)
  },
  updateFlow: (params: {
    flowName: string
    data: UpdateDataFlowDto | UpdateScreenFlowDto | UpdateScheduleFlowDto
  }): AxiosPromise<FlowResponse> => {
    const { flowName, data } = params
    return request.put(`/v1/flows/${flowName}`, data)
  },
  //
  activateFlow: ({ flowName }: { flowName: string }): AxiosPromise<FlowResponse> => {
    return request.post(`/v1/flows/${flowName}/activate`)
  },
  deactivateFlow: ({ flowName }: { flowName: string }): AxiosPromise<FlowResponse> => {
    return request.post(`/v1/flows/${flowName}/deactivate`)
  },
  //
  getFlowConnections: ({
    queryKey,
  }: QueryContext<{ flowName: string }>): AxiosPromise<FlowItemConnectionResponse[]> => {
    return request.get(`/v1/flow-connections/${queryKey[1].flowName}`)
  },
  createFlowConnection: ({
    flowName,
    ...body
  }: { flowName: string } & CreateFlowItemConnectionDto): AxiosPromise<FlowItemConnectionResponse> => {
    return request.post(`/v1/flow-connections/${flowName}`, body)
  },
  deleteFlowConnection: ({ flowName, id }: { flowName: string; id: string }): AxiosPromise<void> => {
    return request.delete(`/v1/flow-connections/${flowName}/${id}`)
  },
  //
  getFlowVariables: ({ queryKey }: QueryContext<{ flowName: string }>): AxiosPromise<FlowVariableResponse[]> => {
    const { flowName } = queryKey[1]
    return request.get(`/v1/flows/${flowName}/variables`)
  },
  createFlowVariables: ({
    flowName,
    body,
  }: {
    flowName: string
    body: CreateFlowVariableDto
  }): AxiosPromise<FlowVariableResponse> => {
    return request.post(`/v1/flows/${flowName}/variables`, body)
  },
  updateFlowVariables: ({
    flowName,
    varName,
    body,
  }: {
    flowName: string
    varName: string
    body: UpdateFlowVariableDto
  }): AxiosPromise<FlowVariableResponse> => {
    return request.put(`/v1/flows/${flowName}/variables/${varName}`, body)
  },
  deleteFlowVariables: ({ flowName, varName }: { flowName: string; varName: string }): AxiosPromise<void> => {
    return request.delete(`/v1/flows/${flowName}/variables/${varName}`)
  },
  getFlowVariableSchema: ({
    queryKey,
  }: QueryContext<{ flowName: string; variable: string }>): AxiosPromise<FlowVariableSchemaResponse[]> => {
    const { flowName, variable } = queryKey[1]
    return request.get(`/v1/flow-builder/${flowName}/schema/${variable}`)
  },
  //
  getAssignmentOperations: (): AxiosPromise<Record<string, AssignmentOperator[]>> => {
    return request.get(`/v1/flow-builder/assignment-operators`)
  },
  getConditionOperators: (): AxiosPromise<string[]> => {
    return request.get(`/v1/flow-builder/condition-operators`)
  },
}

export const FlowsQueryKey = buildKeys('Flows', Flows)
