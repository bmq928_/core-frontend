import { AxiosPromise } from 'axios'
import { buildKeys } from '../../../utils/utils'
import {
  AppBuilderLayoutResponse,
  AppBuilderPageResponse,
  ApplicationCollection,
  ApplicationResponse,
  AppProfileLayoutResponse,
  AppProfileResponse,
  AssignAppPageToAppsDto,
  AssignProfileToAppDto,
  AssignToAppAndProfileDto,
  ComponentResponse,
  CreateApplicationDto,
  CreateLayoutDto,
  DataTypeExtendedResponse,
  LayoutCollection,
  LayoutResponse,
  ListFilterOperator,
  ObjectCollection,
  ObjectSchemaResponse,
  TriggerFlowResponse,
  TriggerObjectResponse,
  UpdateApplicationDto,
  UpdateAppPageOrderDto,
  UpdateLayoutDto,
  UpdateScriptDto,
} from '../models'
import { ID, PaginationParams, QueryContext, request, SearchParams, SortParams } from '../request'

type LayoutScript = Record<string, ComponentResponse>

export const Builder = {
  getLayouts({
    queryKey,
  }: QueryContext<Partial<SearchParams & PaginationParams & SortParams>>): AxiosPromise<LayoutCollection> {
    const params = queryKey[1]
    return request.get(`/v1/builder/layouts`, { params })
  },
  getLayout({ queryKey: [, params] }: QueryContext<ID>): AxiosPromise<LayoutResponse> {
    return request.get(`/v1/builder/layouts/${params.id}`)
  },
  postLayout(body: CreateLayoutDto): AxiosPromise<LayoutResponse> {
    return request.post(`/v1/builder/layouts`, body)
  },
  putLayout({ id, ...body }: ID & UpdateLayoutDto): AxiosPromise<LayoutResponse> {
    return request.put(`/v1/builder/layouts/${id}`, body)
  },
  getLayoutScript({ queryKey: [, params] }: QueryContext<ID>): AxiosPromise<LayoutScript> {
    return request.get(`/v1/builder/layouts/${params.id}/script`)
  },
  putLayoutScript({ id, ...body }: ID & UpdateScriptDto): AxiosPromise<LayoutScript> {
    return request.put(`/v1/builder/layouts/${id}/script`, body)
  },
  // assign stuff
  getLayoutApps({ queryKey: [, params] }: QueryContext<ID>): AxiosPromise<ApplicationResponse[]> {
    return request.get(`/v1/builder/layouts/${params.id}/apps`)
  },
  assignAppPageToApps({ id, ...body }: ID & AssignAppPageToAppsDto): AxiosPromise<ApplicationResponse[]> {
    return request.post(`/v1/builder/layouts/${id}/apps`, body)
  },
  assignDefaultRecord({ id }: ID): AxiosPromise<void> {
    return request.post(`/v1/builder/layouts/${id}/object-default`)
  },
  getLayoutAppsProfiles({ queryKey: [, params] }: QueryContext<ID>): AxiosPromise<AppProfileLayoutResponse[]> {
    return request.get(`/v1/builder/layouts/${params.id}/apps-profiles`)
  },
  assignLayoutAppsProfiles({ id, ...body }: ID & AssignToAppAndProfileDto): AxiosPromise<AppProfileLayoutResponse[]> {
    return request.post(`/v1/builder/layouts/${id}/apps-profiles`, body)
  },
  // App builder
  getApps({
    queryKey,
  }: QueryContext<SearchParams & PaginationParams & SortParams>): AxiosPromise<ApplicationCollection> {
    const params = queryKey[1]
    return request.get(`/v1/builder/apps`, { params })
  },
  getApp({ queryKey: [, params] }: QueryContext<{ id: string }>): AxiosPromise<ApplicationResponse> {
    return request.get(`/v1/builder/apps/${params.id}`)
  },
  createApp(body: CreateApplicationDto): AxiosPromise<ApplicationResponse> {
    return request.post(`/v1/builder/apps`, body)
  },
  updateApp({ id, ...body }: { id: string } & UpdateApplicationDto): AxiosPromise<ApplicationResponse> {
    return request.put(`/v1/builder/apps/${id}`, body)
  },
  getAppPages({ queryKey: [, params] }: QueryContext<{ id: string }>): AxiosPromise<AppBuilderPageResponse> {
    return request.get(`/v1/builder/apps/${params.id}/pages`)
  },
  orderPages({ id, ...body }: { id: string } & UpdateAppPageOrderDto): AxiosPromise<AppBuilderLayoutResponse[]> {
    return request.post(`/v1/builder/apps/${id}/order-pages`, body)
  },
  // app profiles
  getAppProfiles({ queryKey: [, params] }: QueryContext<{ id: string }>): AxiosPromise<AppProfileResponse[]> {
    return request.get(`/v1/builder/apps/${params.id}/profiles`)
  },
  updateAppProfiles({ id, ...body }: { id: string } & AssignProfileToAppDto): AxiosPromise<void> {
    return request.post(`/v1/builder/apps/${id}/profiles`, body)
  },
  // all object api
  getAllObjects({ queryKey }: QueryContext<SearchParams & PaginationParams>): AxiosPromise<ObjectCollection> {
    const params = queryKey[1]
    return request.get('/v1/builder/obj', {
      params,
    })
  },
  getObjectSchema({ queryKey }: QueryContext<{ objName: string }>): AxiosPromise<ObjectSchemaResponse> {
    const { objName } = queryKey[1]
    return request.get(`/v1/builder/obj/${objName}/schema`)
  },
  getObjectDataTypes: ({ queryKey }: QueryContext<{ objName: string }>): AxiosPromise<DataTypeExtendedResponse[]> => {
    return request.get(`/v1/builder/data-types/${queryKey[1].objName}`)
  },
  getFilterOperators: ({
    queryKey,
  }: QueryContext<{ id: string }>): AxiosPromise<Record<string, ListFilterOperator[]>> => {
    const { id } = queryKey[1]
    return request.get(`/v1/builder/layouts/${id}/filter-operators`)
  },

  getTriggers({
    queryKey,
  }: QueryContext<{ id: string }>): AxiosPromise<Array<TriggerObjectResponse | TriggerFlowResponse>> {
    const { id } = queryKey[1]
    return request.get(`/v1/builder/layouts/${id}/triggers`)
  },
}

export const BuilderQueryKey = buildKeys('Builder', Builder)
