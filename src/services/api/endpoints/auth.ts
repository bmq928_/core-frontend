import { AxiosPromise } from 'axios'
import { LoginDto, LoginResponse } from '../models'
import { request } from '../request'

export const Auth = {
  login({ username, password }: LoginDto): AxiosPromise<LoginResponse> {
    return request.post('/v1/auth/login', {
      username,
      password,
    })
  },
  logout(): AxiosPromise<void> {
    return request.post('/v1/auth/logout')
  },
}
