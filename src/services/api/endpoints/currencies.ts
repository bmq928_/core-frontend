import { AxiosPromise } from 'axios'
import { buildKeys } from '../../../utils/utils'
import {
  CurrencyRateCollection,
  OrganizationCurrencyCollection,
  OrganizationCurrencyResponse,
  UpdateCurrencyDto,
  UpdateCurrencyRateDto,
} from '../models'
import { PaginationParams, QueryContext, request, SearchParams, SortParams } from '../request'

type CurrencyCode = { currencyCode: string }

export const Currencies = {
  getCurrenciesList({
    queryKey: [, params],
  }: QueryContext<SearchParams & PaginationParams & SortParams>): AxiosPromise<OrganizationCurrencyCollection> {
    return request.get('/v1/currencies', { params })
  },
  getCurrency({ queryKey: [, params] }: QueryContext<CurrencyCode>): AxiosPromise<OrganizationCurrencyResponse> {
    return request.get(`/v1/currencies/${params.currencyCode}`)
  },
  updateCurrency({
    currencyCode,
    ...body
  }: CurrencyCode & UpdateCurrencyDto): AxiosPromise<OrganizationCurrencyResponse> {
    return request.put(`/v1/currencies/${currencyCode}`, body)
  },
  getCurrencyRates({
    queryKey: [, params],
  }: QueryContext<CurrencyCode & PaginationParams & SortParams>): AxiosPromise<CurrencyRateCollection> {
    const { currencyCode, ...body } = params
    return request.get(`/v1/currencies/${currencyCode}/rate`, { params: body })
  },
  updateCurrencyRate({
    currencyCode,
    ...body
  }: CurrencyCode & UpdateCurrencyRateDto): AxiosPromise<OrganizationCurrencyResponse> {
    return request.post(`/v1/currencies/${currencyCode}/rate`, body)
  },
}

export const CurrenciesQueryKey = buildKeys('Currencies', Currencies)
