import { AxiosPromise } from 'axios'
import { buildKeys } from '../../../utils/utils'
import {
  CreateProfileDto,
  ProfileCollection,
  ProfileObjectAccess,
  ProfileResponse,
  UpdateProfileDto,
  UpsertProfileAccessDto,
} from '../models'
import { PaginationParams, QueryContext, request, SearchParams, SortParams } from '../request'

type Id = {
  id: string
}

type ProfilesObject = { [key: string]: ProfileObjectAccess }

export const Profiles = {
  getProfileObjects: ({ queryKey }: QueryContext<Id>): AxiosPromise<ProfilesObject> => {
    const { id } = queryKey[1]
    return request.get(`/v1/profile-object/${id}`)
  },
  updateProfileObjects: ({ id, items }: Id & UpsertProfileAccessDto): AxiosPromise<ProfilesObject> => {
    return request.put(`/v1/profile-object/${id}`, { items })
  },
  getProfiles: ({
    queryKey,
  }: QueryContext<SearchParams & PaginationParams & SortParams>): AxiosPromise<ProfileCollection> => {
    return request.get('/v1/profiles', { params: queryKey[1] })
  },
  getProfile: ({ queryKey }: QueryContext<Id>): AxiosPromise<ProfileResponse> => {
    const { id } = queryKey[1]
    return request.get(`/v1/profiles/${id}`)
  },
  createProfile: (body: CreateProfileDto): AxiosPromise<ProfileResponse> => {
    return request.post(`/v1/profiles`, body)
  },
  updateProfile: ({ id, ...body }: Id & UpdateProfileDto): AxiosPromise<ProfileResponse> => {
    return request.put(`/v1/profiles/${id}`, body)
  },
  deleteProfile: ({ id }: Id): AxiosPromise<ProfileResponse> => {
    return request.delete(`/v1/profiles/${id}`)
  },
}

export const ProfilesQueryKey = buildKeys('Profiles', Profiles)
