import { AxiosPromise } from 'axios'
import { buildKeys } from '../../../utils/utils'
import { DataCollection, DataResponse, ListRecordsQueryWithFilters } from '../models'
import { GUID, PaginationParams, QueryContext, request, SearchParams, SortParams } from '../request'

export const Data = {
  getObjectData: ({
    queryKey,
  }: QueryContext<
    { name: string } & Partial<SearchParams & PaginationParams & SortParams>
  >): AxiosPromise<DataCollection> => {
    const { name, ...params } = queryKey[1]
    return request.get(`/v1/data/${name}`, {
      params,
    })
  },
  getSingleObjectData: ({ queryKey }: QueryContext<{ name: string } & GUID>): AxiosPromise<DataResponse> => {
    const params = queryKey[1]
    return request.get(`/v1/data/${params.name}/${params.guid}`)
  },
  createObjectData: ({ name, body }: { name: string; body: object }): AxiosPromise<DataResponse> => {
    return request.post(`/v1/data/${name}`, body)
  },
  updateObjectData: ({ name, guid, body }: { name: string; body: object } & GUID): AxiosPromise<DataResponse> => {
    return request.put(`/v1/data/${name}/${guid}`, body)
  },
  deleteObjectData: ({ name, guid }: { name: string } & GUID): AxiosPromise<DataResponse> => {
    return request.delete(`/v1/data/${name}/${guid}`)
  },
  getObjectRelatedData: ({
    queryKey,
  }: QueryContext<
    { name: string; relation: string } & GUID & SearchParams & PaginationParams & SortParams
  >): AxiosPromise<DataCollection> => {
    const { name, guid, relation, ...params } = queryKey[1]
    return request.get(`/v1/data/${name}/${guid}/${relation}`, { params })
  },
  searchObjectData: ({
    queryKey,
  }: QueryContext<{ name: string } & ListRecordsQueryWithFilters>): AxiosPromise<DataCollection> => {
    const { name, ...body } = queryKey[1]
    return request.post(`/v1/data/${name}/search`, body)
  },
  searchObjectRelatedData: ({
    queryKey,
  }: QueryContext<
    { name: string; relation: string } & GUID & ListRecordsQueryWithFilters
  >): AxiosPromise<DataCollection> => {
    const { name, guid, relation, ...body } = queryKey[1]
    return request.post(`/v1/data/${name}/${guid}/${relation}/search`, body)
  },
}

export const DataQueryKey = buildKeys('Data', Data)
