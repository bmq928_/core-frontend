import { AxiosPromise } from 'axios'
import { buildKeys } from '../../../utils/utils'
import { MediaResponse } from '../models'
import { request } from '../request'

export const UploadFile = {
  uploadFile({ name, formData }: { name: string; formData: FormData }): AxiosPromise<MediaResponse> {
    return request.post(`/v1/upload-file/${name}`, formData)
  },
}

export const UploadFileKeys = buildKeys('UploadFile', UploadFile)
