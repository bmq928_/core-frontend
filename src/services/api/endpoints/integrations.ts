import { AxiosPromise } from 'axios'
import { buildKeys } from '../../../utils/utils'
import {
  ActionMetadataCollection,
  ActionMetadataCreateDto,
  ActionMetadataResponse,
  ActionMetadataUpdateDto,
  CredentialCollection,
  CredentialCreateDto,
  CredentialResponse,
  CredentialUpdateDto,
  ExtFieldDto,
  ExtFieldResponse,
  ExtObjectDataSourceResponse,
  IndirectRelationResponse,
  ObjectCollection,
  ObjectDto,
  ObjectResponse,
  TestDataSourceRequestResponse,
  UpdateDataSourceDto,
} from '../models'
import { PaginationParams, QueryContext, request, SearchParams, SortParams } from '../request'

export type EndpointAction = 'list' | 'item' | 'create' | 'update' | 'delete'

export const Integrations = {
  getCredentials: ({
    queryKey,
  }: QueryContext<(SearchParams & PaginationParams & SortParams) | undefined>): AxiosPromise<CredentialCollection> => {
    return request.get('/v1/integrations/credentials', { params: queryKey[1] })
  },
  getCredential: ({ queryKey }: QueryContext<{ name: string }>): AxiosPromise<CredentialResponse> => {
    return request.get(`/v1/integrations/credentials/${queryKey[1].name}`)
  },
  createCredential: (body: CredentialCreateDto): AxiosPromise<CredentialResponse> => {
    return request.post('/v1/integrations/credentials', body)
  },
  updateCredential: ({ name, ...body }: { name: string } & CredentialUpdateDto): AxiosPromise<CredentialResponse> => {
    return request.put(`/v1/integrations/credentials/${name}`, body)
  },
  deleteCredential: ({ name }: { name: string }): AxiosPromise<void> => {
    return request.delete(`/v1/integrations/credentials/${name}`)
  },

  getAllActionMetadata: ({
    queryKey,
  }: QueryContext<SearchParams & PaginationParams & SortParams>): AxiosPromise<ActionMetadataCollection> => {
    return request.get(`/v1/integrations/action-metadata`, { params: queryKey[1] })
  },
  getSingleActionMetadata: ({ queryKey }: QueryContext<{ name: string }>): AxiosPromise<ActionMetadataResponse> => {
    return request.get(`/v1/integrations/action-metadata/${queryKey[1].name}`)
  },
  createActionMetadata: (body: ActionMetadataCreateDto): AxiosPromise<ActionMetadataResponse> => {
    return request.post(`/v1/integrations/action-metadata`, body)
  },
  updateActionMetadata: ({
    name,
    ...body
  }: { name: string } & ActionMetadataUpdateDto): AxiosPromise<ActionMetadataResponse> => {
    return request.put(`/v1/integrations/action-metadata/${name}`, body)
  },
  deleteActionMetadata: ({ name }: { name: string }): AxiosPromise<void> => {
    return request.delete(`/v1/integrations/action-metadata/${name}`)
  },

  getExtObjectList: ({
    queryKey,
  }: QueryContext<SearchParams & PaginationParams & SortParams>): AxiosPromise<ObjectCollection> => {
    return request.get(`/v1/integrations/mo`, { params: queryKey[1] })
  },
  getExtObject: ({ queryKey }: QueryContext<{ objName: string }>): AxiosPromise<ObjectResponse> => {
    return request.get(`/v1/integrations/mo/${queryKey[1].objName}`)
  },
  postExtObject: (body: ObjectDto): AxiosPromise<ObjectResponse> => {
    return request.post('/v1/integrations/mo', body)
  },

  getDataSources: ({
    queryKey,
  }: QueryContext<{ objName: string }>): AxiosPromise<Record<string, ExtObjectDataSourceResponse>> => {
    return request.get(`/v1/integrations/mo/${queryKey[1].objName}/data-sources`)
  },
  getDataSourceAction: ({
    queryKey,
  }: QueryContext<{ objName: string; action: EndpointAction }>): AxiosPromise<ExtObjectDataSourceResponse> => {
    return request.get(`/v1/integrations/mo/${queryKey[1].objName}/data-sources/${queryKey[1].action}`)
  },
  updateDataSourceAction: ({
    body,
    objName,
    action,
  }: {
    objName: string
    action: EndpointAction
    body: UpdateDataSourceDto
  }): AxiosPromise<ExtObjectDataSourceResponse> => {
    return request.put(`/v1/integrations/mo/${objName}/data-sources/${action}`, body)
  },

  getExtObjFieldList: ({ queryKey }: QueryContext<{ objName: string }>): AxiosPromise<ExtFieldResponse[]> => {
    const params = queryKey[1]
    return request.get(`/v1/integrations/mo/${params.objName}/fields`)
  },
  getExtObjField: ({ queryKey }: QueryContext<{ objName: string; name: string }>): AxiosPromise<ExtFieldResponse> => {
    const params = queryKey[1]
    return request.get(`/v1/integrations/mo/${params.objName}/fields/${params.name}`)
  },
  postExtObjField: ({ body, objName }: { objName: string; body: ExtFieldDto }): AxiosPromise<ExtFieldResponse> => {
    return request.post(`/v1/integrations/mo/${objName}/fields`, body)
  },

  getIndirectRelations: ({ queryKey }: QueryContext<SearchParams>): AxiosPromise<IndirectRelationResponse[]> => {
    return request.get(`/v1/integrations/indirect-relations`, { params: queryKey[1] })
  },

  testSource: ({
    action,
    data,
    objName,
  }: {
    action: EndpointAction
    objName: string
    data: Record<string, any>
  }): AxiosPromise<TestDataSourceRequestResponse> => {
    return request.post(`/v1/integrations/mo/${objName}/test-source/${action}`, data)
  },
}

export const IntegrationsQueryKeys = buildKeys('Integrations', Integrations)
