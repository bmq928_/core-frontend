import { AxiosPromise, AxiosResponse } from 'axios'
import { QueryFunctionContext } from 'react-query'
import { buildKeys } from '../../../utils/utils'
import {
  FieldDto,
  FieldResponse,
  ObjectCollection,
  ObjectCreateDto,
  ObjectDto,
  ObjectResponse,
  ObjectUpdateDto,
  RuleCreateDto,
  RuleDto,
  RuleUpdateDto,
  ValidationRuleResponse,
} from '../models'
import { PaginationParams, PostAction, QueryContext, request, SearchParams, SortParams } from '../request'

export type MetadataArguments = {
  getObjects: { searchText?: string } & PaginationParams & SortParams
  getObject: { name?: string }
  postObject: { data?: ObjectCreateDto | ObjectUpdateDto; action: PostAction; name?: string }
  getObjectFields: { name?: string }
  postObjectField: { body: FieldDto; objName: string }
  getObjectRules: { name?: string }
  postObjectRule: { data?: RuleCreateDto | RuleUpdateDto; action: PostAction; objName?: string; ruleName?: string }
  getObjectRule: { objName?: string; ruleName?: string }
}

export type MetadataRespones = {
  getObjects: AxiosResponse<ObjectCollection>
  getObject: AxiosResponse<ObjectResponse>
  postObject: AxiosResponse<ObjectResponse>
  getObjectFields: AxiosResponse<FieldResponse[]>
  postObjectField: AxiosResponse<FieldResponse>
  getObjectRules: AxiosResponse<ValidationRuleResponse[]>
  postObjectRule: AxiosResponse<ValidationRuleResponse>
  getObjectRule: AxiosResponse<ValidationRuleResponse>
}

export type MetadataEndPoints = {
  getObjects(args: QueryFunctionContext<MetadataArguments['getObjects'][]>): Promise<MetadataRespones['getObjects']>
  getObject(args: QueryFunctionContext<MetadataArguments['getObject'][]>): Promise<MetadataRespones['getObject']>
  postObjects(args: MetadataArguments['postObject']): Promise<MetadataRespones['postObject']>
  getObjectFields(
    args: QueryFunctionContext<MetadataArguments['getObjectFields'][]>,
  ): Promise<MetadataRespones['getObjectFields']>
  postObjectField(args: MetadataArguments['postObjectField']): Promise<MetadataRespones['postObjectField']>
  getObjectRules(
    args: QueryFunctionContext<MetadataArguments['getObjectRules'][]>,
  ): Promise<MetadataRespones['getObjectRules']>
  postObjectRule(args: MetadataArguments['postObjectRule']): Promise<MetadataRespones['postObjectRule']>
  getObjectRule(
    args: QueryFunctionContext<MetadataArguments['getObjectRule'][]>,
  ): Promise<MetadataRespones['getObjectRule']>
}

export const Metadata = {
  getObjects: ({
    queryKey,
  }: QueryContext<SearchParams & PaginationParams & SortParams>): AxiosPromise<ObjectCollection> => {
    const params = queryKey[1]

    return request.get('/v1/mo', {
      params,
    })
  },
  getObject: ({ queryKey }: QueryContext<{ name: string }>): AxiosPromise<ObjectResponse> => {
    const params = queryKey[1]
    return request.get(`/v1/mo/${params.name}`)
  },
  postObject: ({
    action,
    data,
    name,
  }: {
    data?: ObjectCreateDto | ObjectUpdateDto
    action: PostAction
    name?: string
  }): AxiosPromise<ObjectResponse> => {
    return request.post('/v1/mo', {
      name,
      data,
      action,
    } as ObjectDto)
  },
  getObjectFields: ({ queryKey }: QueryContext<{ name: string }>): AxiosPromise<FieldResponse[]> => {
    const params = queryKey[1]
    return request.get(`/v1/mo/${params.name}/fields`)
  },
  postObjectField: ({ objName, body }: { objName: string; body: FieldDto }): AxiosPromise<FieldResponse> => {
    return request.post(`/v1/mo/${objName}/fields`, body)
  },
  getObjectRules: ({ queryKey }: QueryContext<{ name: string }>): AxiosPromise<ValidationRuleResponse[]> => {
    const params = queryKey[1]
    return request.get(`/v1/mo/${params.name}/rules`)
  },
  postObjectRule: ({
    action,
    data,
    objName,
    ruleName,
  }: {
    data?: RuleCreateDto | RuleUpdateDto
    action: PostAction
    objName?: string
    ruleName?: string
  }): AxiosPromise<ValidationRuleResponse> => {
    return request.post(`/v1/mo/${objName}/rules`, {
      name: ruleName,
      data,
      action,
    } as RuleDto)
  },
  getObjectRule: ({
    queryKey,
  }: QueryContext<{ objName: string; ruleName: string }>): AxiosPromise<ValidationRuleResponse> => {
    const params = queryKey[1]
    return request.get(`/v1/mo/${params.objName}/rules/${params.ruleName}`)
  },
}

export const MetadataQueryKey = buildKeys('Metadata', Metadata)
