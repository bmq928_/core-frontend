import { AxiosPromise } from 'axios'
import { buildKeys } from '../../../utils/utils'
import { SessionResponse, UpdateSessionDto } from '../models'
import { request } from '../request'

export const Session = {
  getSessions(): AxiosPromise<SessionResponse> {
    return request.get('/v1/sessions')
  },
  updateSession(body: UpdateSessionDto): AxiosPromise<SessionResponse> {
    return request.put('/v1/sessions', body)
  },
}

export const SessionKeys = buildKeys('Session', Session)
