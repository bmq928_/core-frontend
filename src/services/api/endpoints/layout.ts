import { AxiosPromise } from 'axios'
import { buildKeys } from '../../../utils/utils'
import { CtxFlowResponse, ObjectLayoutResponse } from '../models'
import { QueryContext, request } from '../request'

export const Layout = {
  getObjectLayout: ({ queryKey }: QueryContext<{ objName: string }>): AxiosPromise<ObjectLayoutResponse> => {
    return request.get(`/v1/layouts/${queryKey[1].objName}`)
  },
  getObjectMutationLayout: ({ queryKey }: QueryContext<{ objName: string }>): AxiosPromise<ObjectLayoutResponse> => {
    return request.get(`/v1/layouts/${queryKey[1].objName}/mutation`)
  },
  triggers({
    layoutId,
    componentId,
    ...body
  }: {
    layoutId: string
    componentId: string
  } & Record<string, any>): AxiosPromise<CtxFlowResponse | ObjectLayoutResponse> {
    return request.post(`/v1/triggers/${layoutId}/${componentId}`, body)
  },
}

export const LayoutQueryKeys = buildKeys('Layout', Layout)
